package com.pagesynopsis.af.proxy.remote;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.ApiConsumerServiceProxy;
import com.pagesynopsis.af.proxy.UserServiceProxy;
import com.pagesynopsis.af.proxy.RobotsTextFileServiceProxy;
import com.pagesynopsis.af.proxy.RobotsTextRefreshServiceProxy;
import com.pagesynopsis.af.proxy.OgProfileServiceProxy;
import com.pagesynopsis.af.proxy.OgWebsiteServiceProxy;
import com.pagesynopsis.af.proxy.OgBlogServiceProxy;
import com.pagesynopsis.af.proxy.OgArticleServiceProxy;
import com.pagesynopsis.af.proxy.OgBookServiceProxy;
import com.pagesynopsis.af.proxy.OgVideoServiceProxy;
import com.pagesynopsis.af.proxy.OgMovieServiceProxy;
import com.pagesynopsis.af.proxy.OgTvShowServiceProxy;
import com.pagesynopsis.af.proxy.OgTvEpisodeServiceProxy;
import com.pagesynopsis.af.proxy.TwitterSummaryCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterPhotoCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterGalleryCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterAppCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterPlayerCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterProductCardServiceProxy;
import com.pagesynopsis.af.proxy.FetchRequestServiceProxy;
import com.pagesynopsis.af.proxy.PageInfoServiceProxy;
import com.pagesynopsis.af.proxy.PageFetchServiceProxy;
import com.pagesynopsis.af.proxy.LinkListServiceProxy;
import com.pagesynopsis.af.proxy.ImageSetServiceProxy;
import com.pagesynopsis.af.proxy.AudioSetServiceProxy;
import com.pagesynopsis.af.proxy.VideoSetServiceProxy;
import com.pagesynopsis.af.proxy.OpenGraphMetaServiceProxy;
import com.pagesynopsis.af.proxy.TwitterCardMetaServiceProxy;
import com.pagesynopsis.af.proxy.DomainInfoServiceProxy;
import com.pagesynopsis.af.proxy.UrlRatingServiceProxy;
import com.pagesynopsis.af.proxy.ServiceInfoServiceProxy;
import com.pagesynopsis.af.proxy.FiveTenServiceProxy;

public class RemoteProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(RemoteProxyFactory.class.getName());

    private RemoteProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class RemoteProxyFactoryHolder
    {
        private static final RemoteProxyFactory INSTANCE = new RemoteProxyFactory();
    }

    // Singleton method
    public static RemoteProxyFactory getInstance()
    {
        return RemoteProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new RemoteApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new RemoteUserServiceProxy();
    }

    @Override
    public RobotsTextFileServiceProxy getRobotsTextFileServiceProxy()
    {
        return new RemoteRobotsTextFileServiceProxy();
    }

    @Override
    public RobotsTextRefreshServiceProxy getRobotsTextRefreshServiceProxy()
    {
        return new RemoteRobotsTextRefreshServiceProxy();
    }

    @Override
    public OgProfileServiceProxy getOgProfileServiceProxy()
    {
        return new RemoteOgProfileServiceProxy();
    }

    @Override
    public OgWebsiteServiceProxy getOgWebsiteServiceProxy()
    {
        return new RemoteOgWebsiteServiceProxy();
    }

    @Override
    public OgBlogServiceProxy getOgBlogServiceProxy()
    {
        return new RemoteOgBlogServiceProxy();
    }

    @Override
    public OgArticleServiceProxy getOgArticleServiceProxy()
    {
        return new RemoteOgArticleServiceProxy();
    }

    @Override
    public OgBookServiceProxy getOgBookServiceProxy()
    {
        return new RemoteOgBookServiceProxy();
    }

    @Override
    public OgVideoServiceProxy getOgVideoServiceProxy()
    {
        return new RemoteOgVideoServiceProxy();
    }

    @Override
    public OgMovieServiceProxy getOgMovieServiceProxy()
    {
        return new RemoteOgMovieServiceProxy();
    }

    @Override
    public OgTvShowServiceProxy getOgTvShowServiceProxy()
    {
        return new RemoteOgTvShowServiceProxy();
    }

    @Override
    public OgTvEpisodeServiceProxy getOgTvEpisodeServiceProxy()
    {
        return new RemoteOgTvEpisodeServiceProxy();
    }

    @Override
    public TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy()
    {
        return new RemoteTwitterSummaryCardServiceProxy();
    }

    @Override
    public TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy()
    {
        return new RemoteTwitterPhotoCardServiceProxy();
    }

    @Override
    public TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy()
    {
        return new RemoteTwitterGalleryCardServiceProxy();
    }

    @Override
    public TwitterAppCardServiceProxy getTwitterAppCardServiceProxy()
    {
        return new RemoteTwitterAppCardServiceProxy();
    }

    @Override
    public TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy()
    {
        return new RemoteTwitterPlayerCardServiceProxy();
    }

    @Override
    public TwitterProductCardServiceProxy getTwitterProductCardServiceProxy()
    {
        return new RemoteTwitterProductCardServiceProxy();
    }

    @Override
    public FetchRequestServiceProxy getFetchRequestServiceProxy()
    {
        return new RemoteFetchRequestServiceProxy();
    }

    @Override
    public PageInfoServiceProxy getPageInfoServiceProxy()
    {
        return new RemotePageInfoServiceProxy();
    }

    @Override
    public PageFetchServiceProxy getPageFetchServiceProxy()
    {
        return new RemotePageFetchServiceProxy();
    }

    @Override
    public LinkListServiceProxy getLinkListServiceProxy()
    {
        return new RemoteLinkListServiceProxy();
    }

    @Override
    public ImageSetServiceProxy getImageSetServiceProxy()
    {
        return new RemoteImageSetServiceProxy();
    }

    @Override
    public AudioSetServiceProxy getAudioSetServiceProxy()
    {
        return new RemoteAudioSetServiceProxy();
    }

    @Override
    public VideoSetServiceProxy getVideoSetServiceProxy()
    {
        return new RemoteVideoSetServiceProxy();
    }

    @Override
    public OpenGraphMetaServiceProxy getOpenGraphMetaServiceProxy()
    {
        return new RemoteOpenGraphMetaServiceProxy();
    }

    @Override
    public TwitterCardMetaServiceProxy getTwitterCardMetaServiceProxy()
    {
        return new RemoteTwitterCardMetaServiceProxy();
    }

    @Override
    public DomainInfoServiceProxy getDomainInfoServiceProxy()
    {
        return new RemoteDomainInfoServiceProxy();
    }

    @Override
    public UrlRatingServiceProxy getUrlRatingServiceProxy()
    {
        return new RemoteUrlRatingServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new RemoteServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new RemoteFiveTenServiceProxy();
    }

}
