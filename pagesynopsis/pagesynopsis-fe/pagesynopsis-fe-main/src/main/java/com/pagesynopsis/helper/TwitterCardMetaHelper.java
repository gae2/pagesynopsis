package com.pagesynopsis.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardMetaJsBean;
import com.pagesynopsis.wa.service.TwitterCardMetaWebService;
import com.pagesynopsis.wa.service.UserWebService;


public class TwitterCardMetaHelper
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaHelper.class.getName());

    // Arbitrary...
    private static final long MAX_AGE_MILLIS = 3600 * 1000L * 24 * 30;   // ~1 month

    private TwitterCardMetaHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private TwitterCardMetaWebService twitterCardMetaWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private TwitterCardMetaWebService getTwitterCardMetaService()
    {
        if(twitterCardMetaWebService == null) {
            twitterCardMetaWebService = new TwitterCardMetaWebService();
        }
        return twitterCardMetaWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class TwitterCardMetaHelperHolder
    {
        private static final TwitterCardMetaHelper INSTANCE = new TwitterCardMetaHelper();
    }

    // Singleton method
    public static TwitterCardMetaHelper getInstance()
    {
        return TwitterCardMetaHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public TwitterCardMetaJsBean getTwitterCardMeta(String guid) 
    {
        TwitterCardMetaJsBean bean = null;
        try {
            bean = getTwitterCardMetaService().getTwitterCardMeta(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }

    public TwitterCardMetaJsBean getTwitterCardMetaByTargetUrl(String targetUrl) 
    {
        // Long cutoff = System.currentTimeMillis() - MAX_AGE_MILLIS;
        Long cutoff = null;
        return getTwitterCardMetaByTargetUrl(targetUrl, cutoff);
    }
    public TwitterCardMetaJsBean getTwitterCardMetaByTargetUrl(String targetUrl, Long timeCutoff) 
    {
        return getTwitterCardMetaByTargetUrl(targetUrl, timeCutoff, false);  // This should really be true.. ????? 
    }
    private TwitterCardMetaJsBean getTwitterCardMetaByTargetUrl(String targetUrl, Long timeCutoff, boolean refreshingRecordOnly) 
    {
        TwitterCardMetaJsBean bean = null;
        try {
            String filter = "targetUrl=='" + targetUrl + "'";
            if(timeCutoff != null && timeCutoff > 0L) {
                filter += " && createdTime >= " + timeCutoff;
            }
            // TBD: This requires new datastore index....
            if(refreshingRecordOnly == true) {
                // We should not return stale record...
                filter += " && refreshStatus != " + RefreshStatus.STATUS_NOREFRESH;
            }
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<TwitterCardMetaJsBean> beans = getTwitterCardMetaService().findTwitterCardMetas(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
                String guid = bean.getGuid();
                try {
                    bean = getTwitterCardMetaService().getTwitterCardMeta(guid);
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
                }
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for targetUrl = " + targetUrl, e);
        }
        return bean;
    }

    

    public List<String> getTwitterCardMetaKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getTwitterCardMetaService().findTwitterCardMetaKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    // TBD: 
    public List<TwitterCardMetaJsBean> getTwitterCardMetasForRefreshProcessing(Integer maxCount)
    {
        List<TwitterCardMetaJsBean> twitterCardMetas = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            twitterCardMetas = getTwitterCardMetaService().findTwitterCardMetas(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return twitterCardMetas;
    }


}
