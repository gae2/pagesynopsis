package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.RobotsTextRefreshService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RobotsTextRefreshServiceImpl implements RobotsTextRefreshService
{
    private static final Logger log = Logger.getLogger(RobotsTextRefreshServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "RobotsTextRefresh-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("RobotsTextRefresh:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public RobotsTextRefreshServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // RobotsTextRefresh related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public RobotsTextRefresh getRobotsTextRefresh(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getRobotsTextRefresh(): guid = " + guid);

        RobotsTextRefreshBean bean = null;
        if(getCache() != null) {
            bean = (RobotsTextRefreshBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (RobotsTextRefreshBean) getProxyFactory().getRobotsTextRefreshServiceProxy().getRobotsTextRefresh(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "RobotsTextRefreshBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RobotsTextRefreshBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getRobotsTextRefresh(String guid, String field) throws BaseException
    {
        RobotsTextRefreshBean bean = null;
        if(getCache() != null) {
            bean = (RobotsTextRefreshBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (RobotsTextRefreshBean) getProxyFactory().getRobotsTextRefreshServiceProxy().getRobotsTextRefresh(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "RobotsTextRefreshBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RobotsTextRefreshBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("robotsTextFile")) {
            return bean.getRobotsTextFile();
        } else if(field.equals("refreshInterval")) {
            return bean.getRefreshInterval();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("refreshStatus")) {
            return bean.getRefreshStatus();
        } else if(field.equals("result")) {
            return bean.getResult();
        } else if(field.equals("lastCheckedTime")) {
            return bean.getLastCheckedTime();
        } else if(field.equals("nextCheckedTime")) {
            return bean.getNextCheckedTime();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<RobotsTextRefresh> getRobotsTextRefreshes(List<String> guids) throws BaseException
    {
        log.fine("getRobotsTextRefreshes()");

        // TBD: Is there a better way????
        List<RobotsTextRefresh> robotsTextRefreshes = getProxyFactory().getRobotsTextRefreshServiceProxy().getRobotsTextRefreshes(guids);
        if(robotsTextRefreshes == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextRefreshBean list.");
        }

        log.finer("END");
        return robotsTextRefreshes;
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes() throws BaseException
    {
        return getAllRobotsTextRefreshes(null, null, null);
    }


    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextRefreshes(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRobotsTextRefreshes(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<RobotsTextRefresh> robotsTextRefreshes = getProxyFactory().getRobotsTextRefreshServiceProxy().getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
        if(robotsTextRefreshes == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextRefreshBean list.");
        }

        log.finer("END");
        return robotsTextRefreshes;
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextRefreshKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRobotsTextRefreshKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getRobotsTextRefreshServiceProxy().getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextRefreshBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty RobotsTextRefreshBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextRefreshServiceImpl.findRobotsTextRefreshes(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<RobotsTextRefresh> robotsTextRefreshes = getProxyFactory().getRobotsTextRefreshServiceProxy().findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(robotsTextRefreshes == null) {
            log.log(Level.WARNING, "Failed to find robotsTextRefreshes for the given criterion.");
        }

        log.finer("END");
        return robotsTextRefreshes;
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextRefreshServiceImpl.findRobotsTextRefreshKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getRobotsTextRefreshServiceProxy().findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find RobotsTextRefresh keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty RobotsTextRefresh key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextRefreshServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getRobotsTextRefreshServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createRobotsTextRefresh(String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        RobotsTextRefreshBean bean = new RobotsTextRefreshBean(null, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
        return createRobotsTextRefresh(bean);
    }

    @Override
    public String createRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //RobotsTextRefresh bean = constructRobotsTextRefresh(robotsTextRefresh);
        //return bean.getGuid();

        // Param robotsTextRefresh cannot be null.....
        if(robotsTextRefresh == null) {
            log.log(Level.INFO, "Param robotsTextRefresh is null!");
            throw new BadRequestException("Param robotsTextRefresh object is null!");
        }
        RobotsTextRefreshBean bean = null;
        if(robotsTextRefresh instanceof RobotsTextRefreshBean) {
            bean = (RobotsTextRefreshBean) robotsTextRefresh;
        } else if(robotsTextRefresh instanceof RobotsTextRefresh) {
            // bean = new RobotsTextRefreshBean(null, robotsTextRefresh.getRobotsTextFile(), robotsTextRefresh.getRefreshInterval(), robotsTextRefresh.getNote(), robotsTextRefresh.getStatus(), robotsTextRefresh.getRefreshStatus(), robotsTextRefresh.getResult(), robotsTextRefresh.getLastCheckedTime(), robotsTextRefresh.getNextCheckedTime(), robotsTextRefresh.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new RobotsTextRefreshBean(robotsTextRefresh.getGuid(), robotsTextRefresh.getRobotsTextFile(), robotsTextRefresh.getRefreshInterval(), robotsTextRefresh.getNote(), robotsTextRefresh.getStatus(), robotsTextRefresh.getRefreshStatus(), robotsTextRefresh.getResult(), robotsTextRefresh.getLastCheckedTime(), robotsTextRefresh.getNextCheckedTime(), robotsTextRefresh.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createRobotsTextRefresh(): Arg robotsTextRefresh is of an unknown type.");
            //bean = new RobotsTextRefreshBean();
            bean = new RobotsTextRefreshBean(robotsTextRefresh.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getRobotsTextRefreshServiceProxy().createRobotsTextRefresh(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public RobotsTextRefresh constructRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextRefresh cannot be null.....
        if(robotsTextRefresh == null) {
            log.log(Level.INFO, "Param robotsTextRefresh is null!");
            throw new BadRequestException("Param robotsTextRefresh object is null!");
        }
        RobotsTextRefreshBean bean = null;
        if(robotsTextRefresh instanceof RobotsTextRefreshBean) {
            bean = (RobotsTextRefreshBean) robotsTextRefresh;
        } else if(robotsTextRefresh instanceof RobotsTextRefresh) {
            // bean = new RobotsTextRefreshBean(null, robotsTextRefresh.getRobotsTextFile(), robotsTextRefresh.getRefreshInterval(), robotsTextRefresh.getNote(), robotsTextRefresh.getStatus(), robotsTextRefresh.getRefreshStatus(), robotsTextRefresh.getResult(), robotsTextRefresh.getLastCheckedTime(), robotsTextRefresh.getNextCheckedTime(), robotsTextRefresh.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new RobotsTextRefreshBean(robotsTextRefresh.getGuid(), robotsTextRefresh.getRobotsTextFile(), robotsTextRefresh.getRefreshInterval(), robotsTextRefresh.getNote(), robotsTextRefresh.getStatus(), robotsTextRefresh.getRefreshStatus(), robotsTextRefresh.getResult(), robotsTextRefresh.getLastCheckedTime(), robotsTextRefresh.getNextCheckedTime(), robotsTextRefresh.getExpirationTime());
        } else {
            log.log(Level.WARNING, "createRobotsTextRefresh(): Arg robotsTextRefresh is of an unknown type.");
            //bean = new RobotsTextRefreshBean();
            bean = new RobotsTextRefreshBean(robotsTextRefresh.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getRobotsTextRefreshServiceProxy().createRobotsTextRefresh(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateRobotsTextRefresh(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        RobotsTextRefreshBean bean = new RobotsTextRefreshBean(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
        return updateRobotsTextRefresh(bean);
    }
        
    @Override
    public Boolean updateRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //RobotsTextRefresh bean = refreshRobotsTextRefresh(robotsTextRefresh);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param robotsTextRefresh cannot be null.....
        if(robotsTextRefresh == null || robotsTextRefresh.getGuid() == null) {
            log.log(Level.INFO, "Param robotsTextRefresh or its guid is null!");
            throw new BadRequestException("Param robotsTextRefresh object or its guid is null!");
        }
        RobotsTextRefreshBean bean = null;
        if(robotsTextRefresh instanceof RobotsTextRefreshBean) {
            bean = (RobotsTextRefreshBean) robotsTextRefresh;
        } else {  // if(robotsTextRefresh instanceof RobotsTextRefresh)
            bean = new RobotsTextRefreshBean(robotsTextRefresh.getGuid(), robotsTextRefresh.getRobotsTextFile(), robotsTextRefresh.getRefreshInterval(), robotsTextRefresh.getNote(), robotsTextRefresh.getStatus(), robotsTextRefresh.getRefreshStatus(), robotsTextRefresh.getResult(), robotsTextRefresh.getLastCheckedTime(), robotsTextRefresh.getNextCheckedTime(), robotsTextRefresh.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getRobotsTextRefreshServiceProxy().updateRobotsTextRefresh(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public RobotsTextRefresh refreshRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextRefresh cannot be null.....
        if(robotsTextRefresh == null || robotsTextRefresh.getGuid() == null) {
            log.log(Level.INFO, "Param robotsTextRefresh or its guid is null!");
            throw new BadRequestException("Param robotsTextRefresh object or its guid is null!");
        }
        RobotsTextRefreshBean bean = null;
        if(robotsTextRefresh instanceof RobotsTextRefreshBean) {
            bean = (RobotsTextRefreshBean) robotsTextRefresh;
        } else {  // if(robotsTextRefresh instanceof RobotsTextRefresh)
            bean = new RobotsTextRefreshBean(robotsTextRefresh.getGuid(), robotsTextRefresh.getRobotsTextFile(), robotsTextRefresh.getRefreshInterval(), robotsTextRefresh.getNote(), robotsTextRefresh.getStatus(), robotsTextRefresh.getRefreshStatus(), robotsTextRefresh.getResult(), robotsTextRefresh.getLastCheckedTime(), robotsTextRefresh.getNextCheckedTime(), robotsTextRefresh.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getRobotsTextRefreshServiceProxy().updateRobotsTextRefresh(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteRobotsTextRefresh(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getRobotsTextRefreshServiceProxy().deleteRobotsTextRefresh(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            RobotsTextRefresh robotsTextRefresh = null;
            try {
                robotsTextRefresh = getProxyFactory().getRobotsTextRefreshServiceProxy().getRobotsTextRefresh(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch robotsTextRefresh with a key, " + guid);
                return false;
            }
            if(robotsTextRefresh != null) {
                String beanGuid = robotsTextRefresh.getGuid();
                Boolean suc1 = getProxyFactory().getRobotsTextRefreshServiceProxy().deleteRobotsTextRefresh(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("robotsTextRefresh with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextRefresh cannot be null.....
        if(robotsTextRefresh == null || robotsTextRefresh.getGuid() == null) {
            log.log(Level.INFO, "Param robotsTextRefresh or its guid is null!");
            throw new BadRequestException("Param robotsTextRefresh object or its guid is null!");
        }
        RobotsTextRefreshBean bean = null;
        if(robotsTextRefresh instanceof RobotsTextRefreshBean) {
            bean = (RobotsTextRefreshBean) robotsTextRefresh;
        } else {  // if(robotsTextRefresh instanceof RobotsTextRefresh)
            // ????
            log.warning("robotsTextRefresh is not an instance of RobotsTextRefreshBean.");
            bean = new RobotsTextRefreshBean(robotsTextRefresh.getGuid(), robotsTextRefresh.getRobotsTextFile(), robotsTextRefresh.getRefreshInterval(), robotsTextRefresh.getNote(), robotsTextRefresh.getStatus(), robotsTextRefresh.getRefreshStatus(), robotsTextRefresh.getResult(), robotsTextRefresh.getLastCheckedTime(), robotsTextRefresh.getNextCheckedTime(), robotsTextRefresh.getExpirationTime());
        }
        Boolean suc = getProxyFactory().getRobotsTextRefreshServiceProxy().deleteRobotsTextRefresh(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getRobotsTextRefreshServiceProxy().deleteRobotsTextRefreshes(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createRobotsTextRefreshes(List<RobotsTextRefresh> robotsTextRefreshes) throws BaseException
    {
        log.finer("BEGIN");

        if(robotsTextRefreshes == null) {
            log.log(Level.WARNING, "createRobotsTextRefreshes() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = robotsTextRefreshes.size();
        if(size == 0) {
            log.log(Level.WARNING, "createRobotsTextRefreshes() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(RobotsTextRefresh robotsTextRefresh : robotsTextRefreshes) {
            String guid = createRobotsTextRefresh(robotsTextRefresh);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createRobotsTextRefreshes() failed for at least one robotsTextRefresh. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateRobotsTextRefreshes(List<RobotsTextRefresh> robotsTextRefreshes) throws BaseException
    //{
    //}

}
