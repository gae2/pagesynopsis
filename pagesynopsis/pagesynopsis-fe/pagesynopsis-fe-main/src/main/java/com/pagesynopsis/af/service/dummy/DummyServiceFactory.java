package com.pagesynopsis.af.service.dummy;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.service.AbstractServiceFactory;
import com.pagesynopsis.af.service.ApiConsumerService;
import com.pagesynopsis.af.service.UserService;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.OgProfileService;
import com.pagesynopsis.af.service.OgWebsiteService;
import com.pagesynopsis.af.service.OgBlogService;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.af.service.OgVideoService;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.af.service.TwitterSummaryCardService;
import com.pagesynopsis.af.service.TwitterPhotoCardService;
import com.pagesynopsis.af.service.TwitterGalleryCardService;
import com.pagesynopsis.af.service.TwitterAppCardService;
import com.pagesynopsis.af.service.TwitterPlayerCardService;
import com.pagesynopsis.af.service.TwitterProductCardService;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.af.service.PageInfoService;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.af.service.ImageSetService;
import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.af.service.OpenGraphMetaService;
import com.pagesynopsis.af.service.TwitterCardMetaService;
import com.pagesynopsis.af.service.DomainInfoService;
import com.pagesynopsis.af.service.UrlRatingService;
import com.pagesynopsis.af.service.ServiceInfoService;
import com.pagesynopsis.af.service.FiveTenService;


// The primary purpose of a dummy service is to fake the service api.
// The caller/client can use the same API as other abstract factory-based services,
// but the dummy services do not do anything.
public class DummyServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(DummyServiceFactory.class.getName());

    private DummyServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class DummyServiceFactoryHolder
    {
        private static final DummyServiceFactory INSTANCE = new DummyServiceFactory();
    }

    // Singleton method
    public static DummyServiceFactory getInstance()
    {
        return DummyServiceFactoryHolder.INSTANCE;
    }


    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerDummyService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserDummyService();
    }

    @Override
    public RobotsTextFileService getRobotsTextFileService()
    {
        return new RobotsTextFileDummyService();
    }

    @Override
    public RobotsTextRefreshService getRobotsTextRefreshService()
    {
        return new RobotsTextRefreshDummyService();
    }

    @Override
    public OgProfileService getOgProfileService()
    {
        return new OgProfileDummyService();
    }

    @Override
    public OgWebsiteService getOgWebsiteService()
    {
        return new OgWebsiteDummyService();
    }

    @Override
    public OgBlogService getOgBlogService()
    {
        return new OgBlogDummyService();
    }

    @Override
    public OgArticleService getOgArticleService()
    {
        return new OgArticleDummyService();
    }

    @Override
    public OgBookService getOgBookService()
    {
        return new OgBookDummyService();
    }

    @Override
    public OgVideoService getOgVideoService()
    {
        return new OgVideoDummyService();
    }

    @Override
    public OgMovieService getOgMovieService()
    {
        return new OgMovieDummyService();
    }

    @Override
    public OgTvShowService getOgTvShowService()
    {
        return new OgTvShowDummyService();
    }

    @Override
    public OgTvEpisodeService getOgTvEpisodeService()
    {
        return new OgTvEpisodeDummyService();
    }

    @Override
    public TwitterSummaryCardService getTwitterSummaryCardService()
    {
        return new TwitterSummaryCardDummyService();
    }

    @Override
    public TwitterPhotoCardService getTwitterPhotoCardService()
    {
        return new TwitterPhotoCardDummyService();
    }

    @Override
    public TwitterGalleryCardService getTwitterGalleryCardService()
    {
        return new TwitterGalleryCardDummyService();
    }

    @Override
    public TwitterAppCardService getTwitterAppCardService()
    {
        return new TwitterAppCardDummyService();
    }

    @Override
    public TwitterPlayerCardService getTwitterPlayerCardService()
    {
        return new TwitterPlayerCardDummyService();
    }

    @Override
    public TwitterProductCardService getTwitterProductCardService()
    {
        return new TwitterProductCardDummyService();
    }

    @Override
    public FetchRequestService getFetchRequestService()
    {
        return new FetchRequestDummyService();
    }

    @Override
    public PageInfoService getPageInfoService()
    {
        return new PageInfoDummyService();
    }

    @Override
    public PageFetchService getPageFetchService()
    {
        return new PageFetchDummyService();
    }

    @Override
    public LinkListService getLinkListService()
    {
        return new LinkListDummyService();
    }

    @Override
    public ImageSetService getImageSetService()
    {
        return new ImageSetDummyService();
    }

    @Override
    public AudioSetService getAudioSetService()
    {
        return new AudioSetDummyService();
    }

    @Override
    public VideoSetService getVideoSetService()
    {
        return new VideoSetDummyService();
    }

    @Override
    public OpenGraphMetaService getOpenGraphMetaService()
    {
        return new OpenGraphMetaDummyService();
    }

    @Override
    public TwitterCardMetaService getTwitterCardMetaService()
    {
        return new TwitterCardMetaDummyService();
    }

    @Override
    public DomainInfoService getDomainInfoService()
    {
        return new DomainInfoDummyService();
    }

    @Override
    public UrlRatingService getUrlRatingService()
    {
        return new UrlRatingDummyService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoDummyService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenDummyService();
    }


}
