package com.pagesynopsis.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.VideoSetBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.af.service.impl.VideoSetServiceImpl;
import com.pagesynopsis.app.fetch.FetchProcessor;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.VideoSet;


// Updated.
public class VideoSetAppService extends VideoSetServiceImpl implements VideoSetService
{
    private static final Logger log = Logger.getLogger(VideoSetAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public VideoSetAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // VideoSet related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public VideoSet getVideoSet(String guid) throws BaseException
    {
        return super.getVideoSet(guid);
    }

    @Override
    public Object getVideoSet(String guid, String field) throws BaseException
    {
        return super.getVideoSet(guid, field);
    }

    @Override
    public List<VideoSet> getVideoSets(List<String> guids) throws BaseException
    {
        return super.getVideoSets(guids);
    }

    @Override
    public List<VideoSet> getAllVideoSets() throws BaseException
    {
        return super.getAllVideoSets();
    }

    @Override
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllVideoSetKeys(ordering, offset, count);
    }

    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findVideoSets(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("TOP: createVideoSet()");

        // TBD:
        videoSet = validateVideoSet(videoSet);
        videoSet = enhanceVideoSet(videoSet);
        // ...
        
        // ????
        Integer fefreshStatus = ((VideoSetBean) videoSet).getRefreshStatus();
        if(fefreshStatus == null) {
            ((VideoSetBean) videoSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);  // ???
        }
        // ...

        // Note:
        // POST/PUT should not call processVideoSet().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = videoSet.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                videoSet = processVideoSet(videoSet);
                if(videoSet != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: videoSet = " + videoSet);
                    // Need to to do this to avoid double processing.
                    ((VideoSetBean) videoSet).setDeferred(true);
                    // ((VideoSetBean) videoSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((VideoSetBean) videoSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...


        // Save it.
        String guid = super.createVideoSet(videoSet);

        log.finer("BOTTOM: createVideoSet(): guid = " + guid);
        return guid;
    }

    @Override
    public VideoSet constructVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("TOP: constructVideoSet()");

        // TBD:
        videoSet = validateVideoSet(videoSet);
        videoSet = enhanceVideoSet(videoSet);
        // ...
        
        // ????
        Integer fefreshStatus = ((VideoSetBean) videoSet).getRefreshStatus();
        if(fefreshStatus == null) {
            ((VideoSetBean) videoSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);   // ????
        }
        // ...
        
        
        // Note:
        // POST/PUT should not call processVideoSet().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = videoSet.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                videoSet = processVideoSet(videoSet);
                if(videoSet != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: videoSet = " + videoSet);
                    // Need to to do this to avoid double processing.
                    ((VideoSetBean) videoSet).setDeferred(true);
                    // ((VideoSetBean) videoSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((VideoSetBean) videoSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...


        // Update it.
        videoSet = super.constructVideoSet(videoSet);
        
        log.finer("BOTTOM: constructVideoSet(): videoSet = " + videoSet);
        return videoSet;
    }


    @Override
    public Boolean updateVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("TOP: updateVideoSet()");

        // TBD:
        videoSet = validateVideoSet(videoSet);
        Boolean suc = super.updateVideoSet(videoSet);

        log.finer("BOTTOM: createVideoSet(): suc = " + suc);
        return suc;
    }
        
    @Override
    public VideoSet refreshVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("TOP: refreshVideoSet()");

        // TBD:
        videoSet = validateVideoSet(videoSet);
        videoSet = super.refreshVideoSet(videoSet);
        
        log.finer("BOTTOM: refreshVideoSet(): videoSet = " + videoSet);
        return videoSet;
    }

    
    @Override
    public Boolean deleteVideoSet(String guid) throws BaseException
    {
        return super.deleteVideoSet(guid);
    }

    @Override
    public Boolean deleteVideoSet(VideoSet videoSet) throws BaseException
    {
        return super.deleteVideoSet(videoSet);
    }

    @Override
    public Integer createVideoSets(List<VideoSet> videoSets) throws BaseException
    {
        return super.createVideoSets(videoSets);
    }

    // TBD
    //@Override
    //public Boolean updateVideoSets(List<VideoSet> videoSets) throws BaseException
    //{
    //}

    
    
    // TBD...
    private VideoSetBean validateVideoSet(VideoSet videoSet) throws BaseException
    {
        VideoSetBean bean = null;
        if(videoSet instanceof VideoSetBean) {
            bean = (VideoSetBean) videoSet;
        }
        // else ???
        
        // ...
        String targetUrl =  videoSet.getTargetUrl();
        if(targetUrl == null || targetUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BaseException("Target Url is null or empty. Cannot proceed with fetch.");
        } else if(targetUrl.length() > 500) {
            // ??? Truncate or error?
            // TBD ....
            throw new BaseException("Target Url is too long. Cannot proceed with fetch. targetUrl = " + targetUrl);            
        }
        
        // TBD:
        // If we truncate targetUrl...
        // We need to check pageUrl as well....
        // ...         
        
        
        
        // TBD: 
        String pageUrl = videoSet.getPageUrl();
        String query = videoSet.getQueryString();
        List<KeyValuePairStruct> queryParams = videoSet.getQueryParams();
        // if either of these is null/empty
        // populate them based on targetUrl...
        // --> This should have done earlier (closer to front end) ???
        // ...
        
        
//        // Next, check input...
//        List<String> input = videoSet.getInput();
//        if(input == null || input.isEmpty()) {
//            // ???  --> Note that the request will not even be saved ....
//            throw new BaseException("Input is null or empty. Cannot proceed with fetch.");
//        }

        
        
        // TBD:
        // Minimum value for refresh interval???
        // etc....
        
        // ...

        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private VideoSet enhanceVideoSet(VideoSet videoSet) throws BaseException
    {        
        // TBD:
        // Create shortlink, etc...
        // ....
        return videoSet;
    }        

    private VideoSet processVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("TOP: processVideoSet()");
        // ...
        videoSet = FetchProcessor.getInstance().processVideoFetch(videoSet);
        // ...   

        log.finer("BOTTOM: processVideoSet()");
        return videoSet;
    }


    
    
}
