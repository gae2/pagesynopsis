package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.TwitterAppCardListStub;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.resource.TwitterAppCardResource;
import com.pagesynopsis.af.resource.util.TwitterCardAppInfoResourceUtil;
import com.pagesynopsis.af.resource.util.TwitterCardProductDataResourceUtil;


// MockTwitterAppCardResource is a decorator.
// It can be used as a base class to mock TwitterAppCardResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/twitterAppCards/")
public abstract class MockTwitterAppCardResource implements TwitterAppCardResource
{
    private static final Logger log = Logger.getLogger(MockTwitterAppCardResource.class.getName());

    // MockTwitterAppCardResource uses the decorator design pattern.
    private TwitterAppCardResource decoratedResource;

    public MockTwitterAppCardResource(TwitterAppCardResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected TwitterAppCardResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(TwitterAppCardResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllTwitterAppCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterAppCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllTwitterAppCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterAppCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterAppCardsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findTwitterAppCardsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getTwitterAppCardAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getTwitterAppCardAsHtml(guid);
//    }

    @Override
    public Response getTwitterAppCard(String guid) throws BaseResourceException
    {
        return decoratedResource.getTwitterAppCard(guid);
    }

    @Override
    public Response getTwitterAppCardAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getTwitterAppCardAsJsonp(guid, callback);
    }

    @Override
    public Response getTwitterAppCard(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getTwitterAppCard(guid, field);
    }

    // TBD
    @Override
    public Response constructTwitterAppCard(TwitterAppCardStub twitterAppCard) throws BaseResourceException
    {
        return decoratedResource.constructTwitterAppCard(twitterAppCard);
    }

    @Override
    public Response createTwitterAppCard(TwitterAppCardStub twitterAppCard) throws BaseResourceException
    {
        return decoratedResource.createTwitterAppCard(twitterAppCard);
    }

//    @Override
//    public Response createTwitterAppCard(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createTwitterAppCard(formParams);
//    }

    // TBD
    @Override
    public Response refreshTwitterAppCard(String guid, TwitterAppCardStub twitterAppCard) throws BaseResourceException
    {
        return decoratedResource.refreshTwitterAppCard(guid, twitterAppCard);
    }

    @Override
    public Response updateTwitterAppCard(String guid, TwitterAppCardStub twitterAppCard) throws BaseResourceException
    {
        return decoratedResource.updateTwitterAppCard(guid, twitterAppCard);
    }

    @Override
    public Response updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String iphoneApp, String ipadApp, String googlePlayApp)
    {
        return decoratedResource.updateTwitterAppCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp);
    }

//    @Override
//    public Response updateTwitterAppCard(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateTwitterAppCard(guid, formParams);
//    }

    @Override
    public Response deleteTwitterAppCard(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterAppCard(guid);
    }

    @Override
    public Response deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterAppCards(filter, params, values);
    }


// TBD ....
    @Override
    public Response createTwitterAppCards(TwitterAppCardListStub twitterAppCards) throws BaseResourceException
    {
        return decoratedResource.createTwitterAppCards(twitterAppCards);
    }


}
