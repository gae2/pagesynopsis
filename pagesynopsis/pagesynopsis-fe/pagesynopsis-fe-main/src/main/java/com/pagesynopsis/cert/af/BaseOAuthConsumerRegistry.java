package com.pagesynopsis.cert.af;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseOAuthConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseOAuthConsumerRegistry.class.getName());

    // Consumer key-secret map.
    private Map<String, String> consumerSecretMap;

    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD:
        consumerSecretMap.put("93e755c1-704a-4762-bddb-bf0c6fdfe359", "30e8e266-42ca-4983-bab5-894c676f44fe");  // PageSynopsisApp
        consumerSecretMap.put("bc63df4a-941a-4d09-bb31-9397e854d09e", "04cdde11-c125-4a68-89c5-e0e3d60808f7");  // PageSynopsisApp + PageSynopsisWeb
        consumerSecretMap.put("f4d148d4-f6ca-4c3c-8443-1cf365672a60", "f8835ab5-4a45-433a-a595-e5a5c6498086");  // PageSynopsisApp + QueryClient
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
