package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.stub.LinkListStub;
import com.pagesynopsis.ws.stub.LinkListListStub;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.LinkListBean;
import com.pagesynopsis.af.resource.LinkListResource;
import com.pagesynopsis.af.resource.util.UrlStructResourceUtil;
import com.pagesynopsis.af.resource.util.ImageStructResourceUtil;
import com.pagesynopsis.af.resource.util.AnchorStructResourceUtil;
import com.pagesynopsis.af.resource.util.KeyValuePairStructResourceUtil;
import com.pagesynopsis.af.resource.util.AudioStructResourceUtil;
import com.pagesynopsis.af.resource.util.VideoStructResourceUtil;


// MockLinkListResource is a decorator.
// It can be used as a base class to mock LinkListResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/linkLists/")
public abstract class MockLinkListResource implements LinkListResource
{
    private static final Logger log = Logger.getLogger(MockLinkListResource.class.getName());

    // MockLinkListResource uses the decorator design pattern.
    private LinkListResource decoratedResource;

    public MockLinkListResource(LinkListResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected LinkListResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(LinkListResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllLinkLists(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllLinkLists(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllLinkListKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllLinkListKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findLinkListsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findLinkListsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getLinkListAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getLinkListAsHtml(guid);
//    }

    @Override
    public Response getLinkList(String guid) throws BaseResourceException
    {
        return decoratedResource.getLinkList(guid);
    }

    @Override
    public Response getLinkListAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getLinkListAsJsonp(guid, callback);
    }

    @Override
    public Response getLinkList(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getLinkList(guid, field);
    }

    // TBD
    @Override
    public Response constructLinkList(LinkListStub linkList) throws BaseResourceException
    {
        return decoratedResource.constructLinkList(linkList);
    }

    @Override
    public Response createLinkList(LinkListStub linkList) throws BaseResourceException
    {
        return decoratedResource.createLinkList(linkList);
    }

//    @Override
//    public Response createLinkList(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createLinkList(formParams);
//    }

    // TBD
    @Override
    public Response refreshLinkList(String guid, LinkListStub linkList) throws BaseResourceException
    {
        return decoratedResource.refreshLinkList(guid, linkList);
    }

    @Override
    public Response updateLinkList(String guid, LinkListStub linkList) throws BaseResourceException
    {
        return decoratedResource.updateLinkList(guid, linkList);
    }

    @Override
    public Response updateLinkList(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<String> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<String> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls)
    {
        return decoratedResource.updateLinkList(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
    }

//    @Override
//    public Response updateLinkList(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateLinkList(guid, formParams);
//    }

    @Override
    public Response deleteLinkList(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteLinkList(guid);
    }

    @Override
    public Response deleteLinkLists(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteLinkLists(filter, params, values);
    }


// TBD ....
    @Override
    public Response createLinkLists(LinkListListStub linkLists) throws BaseResourceException
    {
        return decoratedResource.createLinkLists(linkLists);
    }


}
