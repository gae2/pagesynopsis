package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.ImageSetBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.ImageSetService;
import com.pagesynopsis.af.proxy.ImageSetServiceProxy;


// MockImageSetServiceProxy is a decorator.
// It can be used as a base class to mock ImageSetServiceProxy objects.
public abstract class MockImageSetServiceProxy implements ImageSetServiceProxy
{
    private static final Logger log = Logger.getLogger(MockImageSetServiceProxy.class.getName());

    // MockImageSetServiceProxy uses the decorator design pattern.
    private ImageSetServiceProxy decoratedProxy;

    public MockImageSetServiceProxy(ImageSetServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected ImageSetServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(ImageSetServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public ImageSet getImageSet(String guid) throws BaseException
    {
        return decoratedProxy.getImageSet(guid);
    }

    @Override
    public Object getImageSet(String guid, String field) throws BaseException
    {
        return decoratedProxy.getImageSet(guid, field);       
    }

    @Override
    public List<ImageSet> getImageSets(List<String> guids) throws BaseException
    {
        return decoratedProxy.getImageSets(guids);
    }

    @Override
    public List<ImageSet> getAllImageSets() throws BaseException
    {
        return getAllImageSets(null, null, null);
    }

    @Override
    public List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllImageSets(ordering, offset, count);
        return getAllImageSets(ordering, offset, count, null);
    }

    @Override
    public List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllImageSets(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllImageSetKeys(ordering, offset, count);
        return getAllImageSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllImageSetKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findImageSets(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findImageSets(filter, ordering, params, values, grouping, unique, offset, count);
        return findImageSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findImageSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createImageSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException
    {
        return decoratedProxy.createImageSet(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageImages);
    }

    @Override
    public String createImageSet(ImageSet imageSet) throws BaseException
    {
        return decoratedProxy.createImageSet(imageSet);
    }

    @Override
    public Boolean updateImageSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException
    {
        return decoratedProxy.updateImageSet(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageImages);
    }

    @Override
    public Boolean updateImageSet(ImageSet imageSet) throws BaseException
    {
        return decoratedProxy.updateImageSet(imageSet);
    }

    @Override
    public Boolean deleteImageSet(String guid) throws BaseException
    {
        return decoratedProxy.deleteImageSet(guid);
    }

    @Override
    public Boolean deleteImageSet(ImageSet imageSet) throws BaseException
    {
        String guid = imageSet.getGuid();
        return deleteImageSet(guid);
    }

    @Override
    public Long deleteImageSets(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteImageSets(filter, params, values);
    }

}
