package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OpenGraphMetaBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.OpenGraphMetaService;
import com.pagesynopsis.af.proxy.OpenGraphMetaServiceProxy;


// MockOpenGraphMetaServiceProxy is a decorator.
// It can be used as a base class to mock OpenGraphMetaServiceProxy objects.
public abstract class MockOpenGraphMetaServiceProxy implements OpenGraphMetaServiceProxy
{
    private static final Logger log = Logger.getLogger(MockOpenGraphMetaServiceProxy.class.getName());

    // MockOpenGraphMetaServiceProxy uses the decorator design pattern.
    private OpenGraphMetaServiceProxy decoratedProxy;

    public MockOpenGraphMetaServiceProxy(OpenGraphMetaServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected OpenGraphMetaServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(OpenGraphMetaServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public OpenGraphMeta getOpenGraphMeta(String guid) throws BaseException
    {
        return decoratedProxy.getOpenGraphMeta(guid);
    }

    @Override
    public Object getOpenGraphMeta(String guid, String field) throws BaseException
    {
        return decoratedProxy.getOpenGraphMeta(guid, field);       
    }

    @Override
    public List<OpenGraphMeta> getOpenGraphMetas(List<String> guids) throws BaseException
    {
        return decoratedProxy.getOpenGraphMetas(guids);
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas() throws BaseException
    {
        return getAllOpenGraphMetas(null, null, null);
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOpenGraphMetas(ordering, offset, count);
        return getAllOpenGraphMetas(ordering, offset, count, null);
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOpenGraphMetas(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOpenGraphMetaKeys(ordering, offset, count);
        return getAllOpenGraphMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOpenGraphMetaKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOpenGraphMetas(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count);
        return findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOpenGraphMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        return decoratedProxy.createOpenGraphMeta(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfile, ogWebsite, ogBlog, ogArticle, ogBook, ogVideo, ogMovie, ogTvShow, ogTvEpisode);
    }

    @Override
    public String createOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        return decoratedProxy.createOpenGraphMeta(openGraphMeta);
    }

    @Override
    public Boolean updateOpenGraphMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        return decoratedProxy.updateOpenGraphMeta(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfile, ogWebsite, ogBlog, ogArticle, ogBook, ogVideo, ogMovie, ogTvShow, ogTvEpisode);
    }

    @Override
    public Boolean updateOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        return decoratedProxy.updateOpenGraphMeta(openGraphMeta);
    }

    @Override
    public Boolean deleteOpenGraphMeta(String guid) throws BaseException
    {
        return decoratedProxy.deleteOpenGraphMeta(guid);
    }

    @Override
    public Boolean deleteOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        String guid = openGraphMeta.getGuid();
        return deleteOpenGraphMeta(guid);
    }

    @Override
    public Long deleteOpenGraphMetas(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteOpenGraphMetas(filter, params, values);
    }

}
