package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.stub.OgVideoStub;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.OgBlogStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.stub.UrlStructStub;
import com.pagesynopsis.ws.stub.ImageStructStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.AnchorStructStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.AudioStructStub;
import com.pagesynopsis.ws.stub.VideoStructStub;
import com.pagesynopsis.ws.stub.OgProfileStub;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.stub.PageBaseStub;
import com.pagesynopsis.ws.stub.OpenGraphMetaStub;


// Wrapper class + bean combo.
public class OpenGraphMetaBean extends PageBaseBean implements OpenGraphMeta, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OpenGraphMetaBean.class.getName());


    // [2] Or, without an embedded object.
    private String ogType;
    private OgProfileBean ogProfile;
    private OgWebsiteBean ogWebsite;
    private OgBlogBean ogBlog;
    private OgArticleBean ogArticle;
    private OgBookBean ogBook;
    private OgVideoBean ogVideo;
    private OgMovieBean ogMovie;
    private OgTvShowBean ogTvShow;
    private OgTvEpisodeBean ogTvEpisode;

    // Ctors.
    public OpenGraphMetaBean()
    {
        //this((String) null);
    }
    public OpenGraphMetaBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OpenGraphMetaBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfileBean ogProfile, OgWebsiteBean ogWebsite, OgBlogBean ogBlog, OgArticleBean ogArticle, OgBookBean ogBook, OgVideoBean ogVideo, OgMovieBean ogMovie, OgTvShowBean ogTvShow, OgTvEpisodeBean ogTvEpisode)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfile, ogWebsite, ogBlog, ogArticle, ogBook, ogVideo, ogMovie, ogTvShow, ogTvEpisode, null, null);
    }
    public OpenGraphMetaBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfileBean ogProfile, OgWebsiteBean ogWebsite, OgBlogBean ogBlog, OgArticleBean ogArticle, OgBookBean ogBook, OgVideoBean ogVideo, OgMovieBean ogMovie, OgTvShowBean ogTvShow, OgTvEpisodeBean ogTvEpisode, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);

        this.ogType = ogType;
        this.ogProfile = ogProfile;
        this.ogWebsite = ogWebsite;
        this.ogBlog = ogBlog;
        this.ogArticle = ogArticle;
        this.ogBook = ogBook;
        this.ogVideo = ogVideo;
        this.ogMovie = ogMovie;
        this.ogTvShow = ogTvShow;
        this.ogTvEpisode = ogTvEpisode;
    }
    public OpenGraphMetaBean(OpenGraphMeta stub)
    {
        if(stub instanceof OpenGraphMetaStub) {
            super.setStub((PageBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setFetchRequest(stub.getFetchRequest());   
            setTargetUrl(stub.getTargetUrl());   
            setPageUrl(stub.getPageUrl());   
            setQueryString(stub.getQueryString());   
            setQueryParams(stub.getQueryParams());   
            setLastFetchResult(stub.getLastFetchResult());   
            setResponseCode(stub.getResponseCode());   
            setContentType(stub.getContentType());   
            setContentLength(stub.getContentLength());   
            setLanguage(stub.getLanguage());   
            setRedirect(stub.getRedirect());   
            setLocation(stub.getLocation());   
            setPageTitle(stub.getPageTitle());   
            setNote(stub.getNote());   
            setDeferred(stub.isDeferred());   
            setStatus(stub.getStatus());   
            setRefreshStatus(stub.getRefreshStatus());   
            setRefreshInterval(stub.getRefreshInterval());   
            setNextRefreshTime(stub.getNextRefreshTime());   
            setLastCheckedTime(stub.getLastCheckedTime());   
            setLastUpdatedTime(stub.getLastUpdatedTime());   
            setOgType(stub.getOgType());   
            OgProfile ogProfile = stub.getOgProfile();
            if(ogProfile instanceof OgProfileBean) {
                setOgProfile((OgProfileBean) ogProfile);   
            } else {
                setOgProfile(new OgProfileBean(ogProfile));   
            }
            OgWebsite ogWebsite = stub.getOgWebsite();
            if(ogWebsite instanceof OgWebsiteBean) {
                setOgWebsite((OgWebsiteBean) ogWebsite);   
            } else {
                setOgWebsite(new OgWebsiteBean(ogWebsite));   
            }
            OgBlog ogBlog = stub.getOgBlog();
            if(ogBlog instanceof OgBlogBean) {
                setOgBlog((OgBlogBean) ogBlog);   
            } else {
                setOgBlog(new OgBlogBean(ogBlog));   
            }
            OgArticle ogArticle = stub.getOgArticle();
            if(ogArticle instanceof OgArticleBean) {
                setOgArticle((OgArticleBean) ogArticle);   
            } else {
                setOgArticle(new OgArticleBean(ogArticle));   
            }
            OgBook ogBook = stub.getOgBook();
            if(ogBook instanceof OgBookBean) {
                setOgBook((OgBookBean) ogBook);   
            } else {
                setOgBook(new OgBookBean(ogBook));   
            }
            OgVideo ogVideo = stub.getOgVideo();
            if(ogVideo instanceof OgVideoBean) {
                setOgVideo((OgVideoBean) ogVideo);   
            } else {
                setOgVideo(new OgVideoBean(ogVideo));   
            }
            OgMovie ogMovie = stub.getOgMovie();
            if(ogMovie instanceof OgMovieBean) {
                setOgMovie((OgMovieBean) ogMovie);   
            } else {
                setOgMovie(new OgMovieBean(ogMovie));   
            }
            OgTvShow ogTvShow = stub.getOgTvShow();
            if(ogTvShow instanceof OgTvShowBean) {
                setOgTvShow((OgTvShowBean) ogTvShow);   
            } else {
                setOgTvShow(new OgTvShowBean(ogTvShow));   
            }
            OgTvEpisode ogTvEpisode = stub.getOgTvEpisode();
            if(ogTvEpisode instanceof OgTvEpisodeBean) {
                setOgTvEpisode((OgTvEpisodeBean) ogTvEpisode);   
            } else {
                setOgTvEpisode(new OgTvEpisodeBean(ogTvEpisode));   
            }
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getFetchRequest()
    {
        return super.getFetchRequest();
    }
    public void setFetchRequest(String fetchRequest)
    {
        super.setFetchRequest(fetchRequest);
    }

    public String getTargetUrl()
    {
        return super.getTargetUrl();
    }
    public void setTargetUrl(String targetUrl)
    {
        super.setTargetUrl(targetUrl);
    }

    public String getPageUrl()
    {
        return super.getPageUrl();
    }
    public void setPageUrl(String pageUrl)
    {
        super.setPageUrl(pageUrl);
    }

    public String getQueryString()
    {
        return super.getQueryString();
    }
    public void setQueryString(String queryString)
    {
        super.setQueryString(queryString);
    }

    public List<KeyValuePairStruct> getQueryParams()
    {
        return super.getQueryParams();
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        super.setQueryParams(queryParams);
    }

    public String getLastFetchResult()
    {
        return super.getLastFetchResult();
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        super.setLastFetchResult(lastFetchResult);
    }

    public Integer getResponseCode()
    {
        return super.getResponseCode();
    }
    public void setResponseCode(Integer responseCode)
    {
        super.setResponseCode(responseCode);
    }

    public String getContentType()
    {
        return super.getContentType();
    }
    public void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }

    public Integer getContentLength()
    {
        return super.getContentLength();
    }
    public void setContentLength(Integer contentLength)
    {
        super.setContentLength(contentLength);
    }

    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    public String getRedirect()
    {
        return super.getRedirect();
    }
    public void setRedirect(String redirect)
    {
        super.setRedirect(redirect);
    }

    public String getLocation()
    {
        return super.getLocation();
    }
    public void setLocation(String location)
    {
        super.setLocation(location);
    }

    public String getPageTitle()
    {
        return super.getPageTitle();
    }
    public void setPageTitle(String pageTitle)
    {
        super.setPageTitle(pageTitle);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public Boolean isDeferred()
    {
        return super.isDeferred();
    }
    public void setDeferred(Boolean deferred)
    {
        super.setDeferred(deferred);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public Integer getRefreshStatus()
    {
        return super.getRefreshStatus();
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        super.setRefreshStatus(refreshStatus);
    }

    public Long getRefreshInterval()
    {
        return super.getRefreshInterval();
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        super.setRefreshInterval(refreshInterval);
    }

    public Long getNextRefreshTime()
    {
        return super.getNextRefreshTime();
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        super.setNextRefreshTime(nextRefreshTime);
    }

    public Long getLastCheckedTime()
    {
        return super.getLastCheckedTime();
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        super.setLastCheckedTime(lastCheckedTime);
    }

    public Long getLastUpdatedTime()
    {
        return super.getLastUpdatedTime();
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        super.setLastUpdatedTime(lastUpdatedTime);
    }

    public String getOgType()
    {
        if(getStub() != null) {
            return getStub().getOgType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ogType;
        }
    }
    public void setOgType(String ogType)
    {
        if(getStub() != null) {
            getStub().setOgType(ogType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ogType = ogType;
        }
    }

    public OgProfile getOgProfile()
    {  
        if(getStub() != null) {
            // Note the object type.
            OgProfile _stub_field = getStub().getOgProfile();
            if(_stub_field == null) {
                return null;
            } else {
                return new OgProfileBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ogProfile;
        }
    }
    public void setOgProfile(OgProfile ogProfile)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setOgProfile(ogProfile);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(ogProfile == null) {
                this.ogProfile = null;
            } else {
                if(ogProfile instanceof OgProfileBean) {
                    this.ogProfile = (OgProfileBean) ogProfile;
                } else {
                    this.ogProfile = new OgProfileBean(ogProfile);
                }
            }
        }
    }

    public OgWebsite getOgWebsite()
    {  
        if(getStub() != null) {
            // Note the object type.
            OgWebsite _stub_field = getStub().getOgWebsite();
            if(_stub_field == null) {
                return null;
            } else {
                return new OgWebsiteBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ogWebsite;
        }
    }
    public void setOgWebsite(OgWebsite ogWebsite)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setOgWebsite(ogWebsite);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(ogWebsite == null) {
                this.ogWebsite = null;
            } else {
                if(ogWebsite instanceof OgWebsiteBean) {
                    this.ogWebsite = (OgWebsiteBean) ogWebsite;
                } else {
                    this.ogWebsite = new OgWebsiteBean(ogWebsite);
                }
            }
        }
    }

    public OgBlog getOgBlog()
    {  
        if(getStub() != null) {
            // Note the object type.
            OgBlog _stub_field = getStub().getOgBlog();
            if(_stub_field == null) {
                return null;
            } else {
                return new OgBlogBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ogBlog;
        }
    }
    public void setOgBlog(OgBlog ogBlog)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setOgBlog(ogBlog);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(ogBlog == null) {
                this.ogBlog = null;
            } else {
                if(ogBlog instanceof OgBlogBean) {
                    this.ogBlog = (OgBlogBean) ogBlog;
                } else {
                    this.ogBlog = new OgBlogBean(ogBlog);
                }
            }
        }
    }

    public OgArticle getOgArticle()
    {  
        if(getStub() != null) {
            // Note the object type.
            OgArticle _stub_field = getStub().getOgArticle();
            if(_stub_field == null) {
                return null;
            } else {
                return new OgArticleBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ogArticle;
        }
    }
    public void setOgArticle(OgArticle ogArticle)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setOgArticle(ogArticle);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(ogArticle == null) {
                this.ogArticle = null;
            } else {
                if(ogArticle instanceof OgArticleBean) {
                    this.ogArticle = (OgArticleBean) ogArticle;
                } else {
                    this.ogArticle = new OgArticleBean(ogArticle);
                }
            }
        }
    }

    public OgBook getOgBook()
    {  
        if(getStub() != null) {
            // Note the object type.
            OgBook _stub_field = getStub().getOgBook();
            if(_stub_field == null) {
                return null;
            } else {
                return new OgBookBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ogBook;
        }
    }
    public void setOgBook(OgBook ogBook)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setOgBook(ogBook);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(ogBook == null) {
                this.ogBook = null;
            } else {
                if(ogBook instanceof OgBookBean) {
                    this.ogBook = (OgBookBean) ogBook;
                } else {
                    this.ogBook = new OgBookBean(ogBook);
                }
            }
        }
    }

    public OgVideo getOgVideo()
    {  
        if(getStub() != null) {
            // Note the object type.
            OgVideo _stub_field = getStub().getOgVideo();
            if(_stub_field == null) {
                return null;
            } else {
                return new OgVideoBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ogVideo;
        }
    }
    public void setOgVideo(OgVideo ogVideo)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setOgVideo(ogVideo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(ogVideo == null) {
                this.ogVideo = null;
            } else {
                if(ogVideo instanceof OgVideoBean) {
                    this.ogVideo = (OgVideoBean) ogVideo;
                } else {
                    this.ogVideo = new OgVideoBean(ogVideo);
                }
            }
        }
    }

    public OgMovie getOgMovie()
    {  
        if(getStub() != null) {
            // Note the object type.
            OgMovie _stub_field = getStub().getOgMovie();
            if(_stub_field == null) {
                return null;
            } else {
                return new OgMovieBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ogMovie;
        }
    }
    public void setOgMovie(OgMovie ogMovie)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setOgMovie(ogMovie);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(ogMovie == null) {
                this.ogMovie = null;
            } else {
                if(ogMovie instanceof OgMovieBean) {
                    this.ogMovie = (OgMovieBean) ogMovie;
                } else {
                    this.ogMovie = new OgMovieBean(ogMovie);
                }
            }
        }
    }

    public OgTvShow getOgTvShow()
    {  
        if(getStub() != null) {
            // Note the object type.
            OgTvShow _stub_field = getStub().getOgTvShow();
            if(_stub_field == null) {
                return null;
            } else {
                return new OgTvShowBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ogTvShow;
        }
    }
    public void setOgTvShow(OgTvShow ogTvShow)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setOgTvShow(ogTvShow);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(ogTvShow == null) {
                this.ogTvShow = null;
            } else {
                if(ogTvShow instanceof OgTvShowBean) {
                    this.ogTvShow = (OgTvShowBean) ogTvShow;
                } else {
                    this.ogTvShow = new OgTvShowBean(ogTvShow);
                }
            }
        }
    }

    public OgTvEpisode getOgTvEpisode()
    {  
        if(getStub() != null) {
            // Note the object type.
            OgTvEpisode _stub_field = getStub().getOgTvEpisode();
            if(_stub_field == null) {
                return null;
            } else {
                return new OgTvEpisodeBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ogTvEpisode;
        }
    }
    public void setOgTvEpisode(OgTvEpisode ogTvEpisode)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setOgTvEpisode(ogTvEpisode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(ogTvEpisode == null) {
                this.ogTvEpisode = null;
            } else {
                if(ogTvEpisode instanceof OgTvEpisodeBean) {
                    this.ogTvEpisode = (OgTvEpisodeBean) ogTvEpisode;
                } else {
                    this.ogTvEpisode = new OgTvEpisodeBean(ogTvEpisode);
                }
            }
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public OpenGraphMetaStub getStub()
    {
        return (OpenGraphMetaStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("ogType = " + this.ogType).append(";");
            sb.append("ogProfile = " + this.ogProfile).append(";");
            sb.append("ogWebsite = " + this.ogWebsite).append(";");
            sb.append("ogBlog = " + this.ogBlog).append(";");
            sb.append("ogArticle = " + this.ogArticle).append(";");
            sb.append("ogBook = " + this.ogBook).append(";");
            sb.append("ogVideo = " + this.ogVideo).append(";");
            sb.append("ogMovie = " + this.ogMovie).append(";");
            sb.append("ogTvShow = " + this.ogTvShow).append(";");
            sb.append("ogTvEpisode = " + this.ogTvEpisode).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = ogType == null ? 0 : ogType.hashCode();
            _hash = 31 * _hash + delta;
            delta = ogProfile == null ? 0 : ogProfile.hashCode();
            _hash = 31 * _hash + delta;
            delta = ogWebsite == null ? 0 : ogWebsite.hashCode();
            _hash = 31 * _hash + delta;
            delta = ogBlog == null ? 0 : ogBlog.hashCode();
            _hash = 31 * _hash + delta;
            delta = ogArticle == null ? 0 : ogArticle.hashCode();
            _hash = 31 * _hash + delta;
            delta = ogBook == null ? 0 : ogBook.hashCode();
            _hash = 31 * _hash + delta;
            delta = ogVideo == null ? 0 : ogVideo.hashCode();
            _hash = 31 * _hash + delta;
            delta = ogMovie == null ? 0 : ogMovie.hashCode();
            _hash = 31 * _hash + delta;
            delta = ogTvShow == null ? 0 : ogTvShow.hashCode();
            _hash = 31 * _hash + delta;
            delta = ogTvEpisode == null ? 0 : ogTvEpisode.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
