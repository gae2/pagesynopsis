package com.pagesynopsis.af.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgBookListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.proxy.OgBookServiceProxy;
import com.pagesynopsis.af.proxy.remote.RemoteProxyFactory;
import com.pagesynopsis.af.proxy.remote.RemoteOgBookServiceProxy;
import com.pagesynopsis.af.resource.OgBookResource;
import com.pagesynopsis.af.resource.util.OgAudioStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgImageStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgActorStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgVideoStructResourceUtil;


@Path("/_task/r/ogBooks/")
public class AsyncOgBookResource extends BaseAsyncResource implements OgBookResource
{
    private static final Logger log = Logger.getLogger(AsyncOgBookResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncOgBookResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getOgBookList(List<OgBook> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllOgBooks(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllOgBookKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findOgBooksAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
//    public Response getOgBookAsHtml(String guid) throws BaseResourceException
//    {
//        // Note: This method should never be called.
//        throw new NotImplementedRsException(resourceUri);
//    }

    @Override
    public Response getOgBook(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getOgBookAsJsonp(String guid, String callback) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getOgBook(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response constructOgBook(OgBookStub ogBook) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructOgBook(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            OgBookBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                OgBookStub realStub = (OgBookStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertOgBookStubToBean(realStub);
            } else {
                bean = convertOgBookStubToBean(ogBook);
            }
            //bean = (OgBookBean) RemoteProxyFactory.getInstance().getOgBookServiceProxy().constructOgBook(bean);
            //ogBook = OgBookStub.convertBeanToStub(bean);
            //String guid = ogBook.getGuid();
            // TBD: createOgBook() or constructOgBook()???  (constructOgBook() currently not implemented)
            String guid = RemoteProxyFactory.getInstance().getOgBookServiceProxy().createOgBook(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructOgBook(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(ogBook).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createOgBook(OgBookStub ogBook) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createOgBook(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            OgBookBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                OgBookStub realStub = (OgBookStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertOgBookStubToBean(realStub);
            } else {
                bean = convertOgBookStubToBean(ogBook);
            }
            String guid = RemoteProxyFactory.getInstance().getOgBookServiceProxy().createOgBook(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createOgBook(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createOgBook(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response refreshOgBook(String guid, OgBookStub ogBook) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshOgBook(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(ogBook == null || !guid.equals(ogBook.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from ogBook guid = " + ogBook.getGuid());
                throw new RequestForbiddenRsException("Failed to refresh the ogBook with guid = " + guid);
            }
            OgBookBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                OgBookStub realStub = (OgBookStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertOgBookStubToBean(realStub);
            } else {
                bean = convertOgBookStubToBean(ogBook);
            }
            //bean = (OgBookBean) RemoteProxyFactory.getInstance().getOgBookServiceProxy().refreshOgBook(bean);
            //if(bean == null) {
            //    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the ogBook with guid = " + guid);
            //    throw new InternalServerErrorException("Failed to refresh the ogBook with guid = " + guid);
            //}
            //ogBook = OgBookStub.convertBeanToStub(bean);
            // TBD: updateOgBook() or refreshOgBook()???  (refreshOgBook() currently not implemented)
            boolean suc = RemoteProxyFactory.getInstance().getOgBookServiceProxy().updateOgBook(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refrefsh the ogBook with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the ogBook with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshOgBook(): Successfully processed the request: guid = " + guid);
            return Response.ok(ogBook).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateOgBook(String guid, OgBookStub ogBook) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateOgBook(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(ogBook == null || !guid.equals(ogBook.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from ogBook guid = " + ogBook.getGuid());
                throw new RequestForbiddenRsException("Failed to update the ogBook with guid = " + guid);
            }
            OgBookBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                OgBookStub realStub = (OgBookStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertOgBookStubToBean(realStub);
            } else {
                bean = convertOgBookStubToBean(ogBook);
            }
            boolean suc = RemoteProxyFactory.getInstance().getOgBookServiceProxy().updateOgBook(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the ogBook with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the ogBook with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateOgBook(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateOgBook(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<String> image, List<String> audio, List<String> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
    public Response updateOgBook(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteOgBook(String guid) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteOgBook(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            boolean suc = RemoteProxyFactory.getInstance().getOgBookServiceProxy().deleteOgBook(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the ogBook with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the ogBook with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteOgBook(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteOgBooks(String filter, String params, List<String> values) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteOgBooks(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            Long count = RemoteProxyFactory.getInstance().getOgBookServiceProxy().deleteOgBooks(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


// TBD ....
    @Override
    public Response createOgBooks(OgBookListStub ogBooks) throws BaseResourceException
    {
        // TBD: Do we need this method????
        throw new NotImplementedRsException(resourceUri);
/*
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createOgBooks(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            OgBookListStub stubs;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                OgBookListStub realStubs = (OgBookListStub) getCache().get(taskName);
                if(realStubs == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub list retrieved from memCache. realStubs = " + realStubs);
                stubs = realStubs;
            } else {
                stubs = ogBooks;
            }

            List<OgBookStub> stubList = ogBooks.getList();
            List<VisitorSetting> beans = new ArrayList<OgBook>();
            for(OgBookStub stub : stubList) {
                OgBookBean bean = convertOgBookStubToBean(stub);
                beans.add(bean);
            }
            Integer count = RemoteProxyFactory.getInstance().getOgBookServiceProxy().createOgBooks(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
*/
    }


    public static OgBookBean convertOgBookStubToBean(OgBook stub)
    {
        OgBookBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new OgBookBean();
            bean.setGuid(stub.getGuid());
            bean.setUrl(stub.getUrl());
            bean.setType(stub.getType());
            bean.setSiteName(stub.getSiteName());
            bean.setTitle(stub.getTitle());
            bean.setDescription(stub.getDescription());
            bean.setFbAdmins(stub.getFbAdmins());
            bean.setFbAppId(stub.getFbAppId());
            bean.setImage(stub.getImage());
            bean.setAudio(stub.getAudio());
            bean.setVideo(stub.getVideo());
            bean.setLocale(stub.getLocale());
            bean.setLocaleAlternate(stub.getLocaleAlternate());
            bean.setAuthor(stub.getAuthor());
            bean.setIsbn(stub.getIsbn());
            bean.setTag(stub.getTag());
            bean.setReleaseDate(stub.getReleaseDate());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
