package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.af.proxy.OgTvShowServiceProxy;


// MockOgTvShowServiceProxy is a decorator.
// It can be used as a base class to mock OgTvShowServiceProxy objects.
public abstract class MockOgTvShowServiceProxy implements OgTvShowServiceProxy
{
    private static final Logger log = Logger.getLogger(MockOgTvShowServiceProxy.class.getName());

    // MockOgTvShowServiceProxy uses the decorator design pattern.
    private OgTvShowServiceProxy decoratedProxy;

    public MockOgTvShowServiceProxy(OgTvShowServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected OgTvShowServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(OgTvShowServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public OgTvShow getOgTvShow(String guid) throws BaseException
    {
        return decoratedProxy.getOgTvShow(guid);
    }

    @Override
    public Object getOgTvShow(String guid, String field) throws BaseException
    {
        return decoratedProxy.getOgTvShow(guid, field);       
    }

    @Override
    public List<OgTvShow> getOgTvShows(List<String> guids) throws BaseException
    {
        return decoratedProxy.getOgTvShows(guids);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows() throws BaseException
    {
        return getAllOgTvShows(null, null, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgTvShows(ordering, offset, count);
        return getAllOgTvShows(ordering, offset, count, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgTvShows(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgTvShowKeys(ordering, offset, count);
        return getAllOgTvShowKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgTvShowKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgTvShow(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedProxy.createOgTvShow(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public String createOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        return decoratedProxy.createOgTvShow(ogTvShow);
    }

    @Override
    public Boolean updateOgTvShow(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedProxy.updateOgTvShow(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public Boolean updateOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        return decoratedProxy.updateOgTvShow(ogTvShow);
    }

    @Override
    public Boolean deleteOgTvShow(String guid) throws BaseException
    {
        return decoratedProxy.deleteOgTvShow(guid);
    }

    @Override
    public Boolean deleteOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        String guid = ogTvShow.getGuid();
        return deleteOgTvShow(guid);
    }

    @Override
    public Long deleteOgTvShows(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteOgTvShows(filter, params, values);
    }

}
