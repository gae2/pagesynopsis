package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.ws.service.RobotsTextRefreshService;
import com.pagesynopsis.af.proxy.RobotsTextRefreshServiceProxy;


// MockRobotsTextRefreshServiceProxy is a decorator.
// It can be used as a base class to mock RobotsTextRefreshServiceProxy objects.
public abstract class MockRobotsTextRefreshServiceProxy implements RobotsTextRefreshServiceProxy
{
    private static final Logger log = Logger.getLogger(MockRobotsTextRefreshServiceProxy.class.getName());

    // MockRobotsTextRefreshServiceProxy uses the decorator design pattern.
    private RobotsTextRefreshServiceProxy decoratedProxy;

    public MockRobotsTextRefreshServiceProxy(RobotsTextRefreshServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected RobotsTextRefreshServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(RobotsTextRefreshServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public RobotsTextRefresh getRobotsTextRefresh(String guid) throws BaseException
    {
        return decoratedProxy.getRobotsTextRefresh(guid);
    }

    @Override
    public Object getRobotsTextRefresh(String guid, String field) throws BaseException
    {
        return decoratedProxy.getRobotsTextRefresh(guid, field);       
    }

    @Override
    public List<RobotsTextRefresh> getRobotsTextRefreshes(List<String> guids) throws BaseException
    {
        return decoratedProxy.getRobotsTextRefreshes(guids);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes() throws BaseException
    {
        return getAllRobotsTextRefreshes(null, null, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllRobotsTextRefreshes(ordering, offset, count);
        return getAllRobotsTextRefreshes(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllRobotsTextRefreshKeys(ordering, offset, count);
        return getAllRobotsTextRefreshKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createRobotsTextRefresh(String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        return decoratedProxy.createRobotsTextRefresh(robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
    }

    @Override
    public String createRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        return decoratedProxy.createRobotsTextRefresh(robotsTextRefresh);
    }

    @Override
    public Boolean updateRobotsTextRefresh(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        return decoratedProxy.updateRobotsTextRefresh(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
    }

    @Override
    public Boolean updateRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        return decoratedProxy.updateRobotsTextRefresh(robotsTextRefresh);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(String guid) throws BaseException
    {
        return decoratedProxy.deleteRobotsTextRefresh(guid);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        String guid = robotsTextRefresh.getGuid();
        return deleteRobotsTextRefresh(guid);
    }

    @Override
    public Long deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteRobotsTextRefreshes(filter, params, values);
    }

}
