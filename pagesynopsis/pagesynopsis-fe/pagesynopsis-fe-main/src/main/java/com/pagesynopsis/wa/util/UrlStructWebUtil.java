package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.fe.bean.UrlStructJsBean;


public class UrlStructWebUtil
{
    private static final Logger log = Logger.getLogger(UrlStructWebUtil.class.getName());

    // Static methods only.
    private UrlStructWebUtil() {}
    

    public static UrlStructJsBean convertUrlStructToJsBean(UrlStruct urlStruct)
    {
        UrlStructJsBean jsBean = null;
        if(urlStruct != null) {
            jsBean = new UrlStructJsBean();
            jsBean.setUuid(urlStruct.getUuid());
            jsBean.setStatusCode(urlStruct.getStatusCode());
            jsBean.setRedirectUrl(urlStruct.getRedirectUrl());
            jsBean.setAbsoluteUrl(urlStruct.getAbsoluteUrl());
            jsBean.setHashFragment(urlStruct.getHashFragment());
            jsBean.setNote(urlStruct.getNote());
        }
        return jsBean;
    }

    public static UrlStruct convertUrlStructJsBeanToBean(UrlStructJsBean jsBean)
    {
        UrlStructBean urlStruct = null;
        if(jsBean != null) {
            urlStruct = new UrlStructBean();
            urlStruct.setUuid(jsBean.getUuid());
            urlStruct.setStatusCode(jsBean.getStatusCode());
            urlStruct.setRedirectUrl(jsBean.getRedirectUrl());
            urlStruct.setAbsoluteUrl(jsBean.getAbsoluteUrl());
            urlStruct.setHashFragment(jsBean.getHashFragment());
            urlStruct.setNote(jsBean.getNote());
        }
        return urlStruct;
    }

}
