package com.pagesynopsis.af.service;

import java.util.List;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.UrlRating;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UrlRatingService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    UrlRating getUrlRating(String guid) throws BaseException;
    Object getUrlRating(String guid, String field) throws BaseException;
    List<UrlRating> getUrlRatings(List<String> guids) throws BaseException;
    List<UrlRating> getAllUrlRatings() throws BaseException;
    /* @Deprecated */ List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count) throws BaseException;
    List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUrlRating(String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException;
    //String createUrlRating(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UrlRating?)
    String createUrlRating(UrlRating urlRating) throws BaseException;
    UrlRating constructUrlRating(UrlRating urlRating) throws BaseException;
    Boolean updateUrlRating(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException;
    //Boolean updateUrlRating(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUrlRating(UrlRating urlRating) throws BaseException;
    UrlRating refreshUrlRating(UrlRating urlRating) throws BaseException;
    Boolean deleteUrlRating(String guid) throws BaseException;
    Boolean deleteUrlRating(UrlRating urlRating) throws BaseException;
    Long deleteUrlRatings(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createUrlRatings(List<UrlRating> urlRatings) throws BaseException;
//    Boolean updateUrlRatings(List<UrlRating> urlRatings) throws BaseException;

}
