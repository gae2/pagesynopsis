package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.stub.FetchRequestStub;
import com.pagesynopsis.ws.stub.FetchRequestListStub;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.FetchRequestBean;
import com.pagesynopsis.af.resource.FetchRequestResource;
import com.pagesynopsis.af.resource.util.NotificationStructResourceUtil;
import com.pagesynopsis.af.resource.util.KeyValuePairStructResourceUtil;
import com.pagesynopsis.af.resource.util.ReferrerInfoStructResourceUtil;


// MockFetchRequestResource is a decorator.
// It can be used as a base class to mock FetchRequestResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/fetchRequests/")
public abstract class MockFetchRequestResource implements FetchRequestResource
{
    private static final Logger log = Logger.getLogger(MockFetchRequestResource.class.getName());

    // MockFetchRequestResource uses the decorator design pattern.
    private FetchRequestResource decoratedResource;

    public MockFetchRequestResource(FetchRequestResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected FetchRequestResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(FetchRequestResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllFetchRequests(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findFetchRequestsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findFetchRequestsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getFetchRequestAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getFetchRequestAsHtml(guid);
//    }

    @Override
    public Response getFetchRequest(String guid) throws BaseResourceException
    {
        return decoratedResource.getFetchRequest(guid);
    }

    @Override
    public Response getFetchRequestAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getFetchRequestAsJsonp(guid, callback);
    }

    @Override
    public Response getFetchRequest(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getFetchRequest(guid, field);
    }

    // TBD
    @Override
    public Response constructFetchRequest(FetchRequestStub fetchRequest) throws BaseResourceException
    {
        return decoratedResource.constructFetchRequest(fetchRequest);
    }

    @Override
    public Response createFetchRequest(FetchRequestStub fetchRequest) throws BaseResourceException
    {
        return decoratedResource.createFetchRequest(fetchRequest);
    }

//    @Override
//    public Response createFetchRequest(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createFetchRequest(formParams);
//    }

    // TBD
    @Override
    public Response refreshFetchRequest(String guid, FetchRequestStub fetchRequest) throws BaseResourceException
    {
        return decoratedResource.refreshFetchRequest(guid, fetchRequest);
    }

    @Override
    public Response updateFetchRequest(String guid, FetchRequestStub fetchRequest) throws BaseResourceException
    {
        return decoratedResource.updateFetchRequest(guid, fetchRequest);
    }

    @Override
    public Response updateFetchRequest(String guid, String user, String targetUrl, String pageUrl, String queryString, List<String> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, String referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, String notificationPref)
    {
        return decoratedResource.updateFetchRequest(guid, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfo, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPref);
    }

//    @Override
//    public Response updateFetchRequest(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateFetchRequest(guid, formParams);
//    }

    @Override
    public Response deleteFetchRequest(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteFetchRequest(guid);
    }

    @Override
    public Response deleteFetchRequests(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteFetchRequests(filter, params, values);
    }


// TBD ....
    @Override
    public Response createFetchRequests(FetchRequestListStub fetchRequests) throws BaseResourceException
    {
        return decoratedResource.createFetchRequests(fetchRequests);
    }


}
