package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.stub.RobotsTextGroupStub;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.stub.RobotsTextFileStub;


// Wrapper class + bean combo.
public class RobotsTextFileBean implements RobotsTextFile, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextFileBean.class.getName());

    // [1] With an embedded object.
    private RobotsTextFileStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String siteUrl;
    private List<RobotsTextGroup> groups;
    private List<String> sitemaps;
    private String content;
    private String contentHash;
    private Long lastCheckedTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public RobotsTextFileBean()
    {
        //this((String) null);
    }
    public RobotsTextFileBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public RobotsTextFileBean(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime)
    {
        this(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime, null, null);
    }
    public RobotsTextFileBean(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.siteUrl = siteUrl;
        this.groups = groups;
        this.sitemaps = sitemaps;
        this.content = content;
        this.contentHash = contentHash;
        this.lastCheckedTime = lastCheckedTime;
        this.lastUpdatedTime = lastUpdatedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public RobotsTextFileBean(RobotsTextFile stub)
    {
        if(stub instanceof RobotsTextFileStub) {
            this.stub = (RobotsTextFileStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setSiteUrl(stub.getSiteUrl());   
            setGroups(stub.getGroups());   
            setSitemaps(stub.getSitemaps());   
            setContent(stub.getContent());   
            setContentHash(stub.getContentHash());   
            setLastCheckedTime(stub.getLastCheckedTime());   
            setLastUpdatedTime(stub.getLastUpdatedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getSiteUrl()
    {
        if(getStub() != null) {
            return getStub().getSiteUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.siteUrl;
        }
    }
    public void setSiteUrl(String siteUrl)
    {
        if(getStub() != null) {
            getStub().setSiteUrl(siteUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.siteUrl = siteUrl;
        }
    }

    public List<RobotsTextGroup> getGroups()
    {
        if(getStub() != null) {
            List<RobotsTextGroup> list = getStub().getGroups();
            if(list != null) {
                List<RobotsTextGroup> bean = new ArrayList<RobotsTextGroup>();
                for(RobotsTextGroup robotsTextGroup : list) {
                    RobotsTextGroupBean elem = null;
                    if(robotsTextGroup instanceof RobotsTextGroupBean) {
                        elem = (RobotsTextGroupBean) robotsTextGroup;
                    } else if(robotsTextGroup instanceof RobotsTextGroupStub) {
                        elem = new RobotsTextGroupBean(robotsTextGroup);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.groups;
        }
    }
    public void setGroups(List<RobotsTextGroup> groups)
    {
        if(groups != null) {
            if(getStub() != null) {
                List<RobotsTextGroup> stub = new ArrayList<RobotsTextGroup>();
                for(RobotsTextGroup robotsTextGroup : groups) {
                    RobotsTextGroupStub elem = null;
                    if(robotsTextGroup instanceof RobotsTextGroupStub) {
                        elem = (RobotsTextGroupStub) robotsTextGroup;
                    } else if(robotsTextGroup instanceof RobotsTextGroupBean) {
                        elem = ((RobotsTextGroupBean) robotsTextGroup).getStub();
                    } else if(robotsTextGroup instanceof RobotsTextGroup) {
                        elem = new RobotsTextGroupStub(robotsTextGroup);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setGroups(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.groups = groups;  // ???

                List<RobotsTextGroup> beans = new ArrayList<RobotsTextGroup>();
                for(RobotsTextGroup robotsTextGroup : groups) {
                    RobotsTextGroupBean elem = null;
                    if(robotsTextGroup != null) {
                        if(robotsTextGroup instanceof RobotsTextGroupBean) {
                            elem = (RobotsTextGroupBean) robotsTextGroup;
                        } else {
                            elem = new RobotsTextGroupBean(robotsTextGroup);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.groups = beans;
            }
        } else {
            this.groups = null;
        }
    }

    public List<String> getSitemaps()
    {
        if(getStub() != null) {
            return getStub().getSitemaps();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.sitemaps;
        }
    }
    public void setSitemaps(List<String> sitemaps)
    {
        if(getStub() != null) {
            getStub().setSitemaps(sitemaps);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.sitemaps = sitemaps;
        }
    }

    public String getContent()
    {
        if(getStub() != null) {
            return getStub().getContent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.content;
        }
    }
    public void setContent(String content)
    {
        if(getStub() != null) {
            getStub().setContent(content);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.content = content;
        }
    }

    public String getContentHash()
    {
        if(getStub() != null) {
            return getStub().getContentHash();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.contentHash;
        }
    }
    public void setContentHash(String contentHash)
    {
        if(getStub() != null) {
            getStub().setContentHash(contentHash);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.contentHash = contentHash;
        }
    }

    public Long getLastCheckedTime()
    {
        if(getStub() != null) {
            return getStub().getLastCheckedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastCheckedTime;
        }
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        if(getStub() != null) {
            getStub().setLastCheckedTime(lastCheckedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastCheckedTime = lastCheckedTime;
        }
    }

    public Long getLastUpdatedTime()
    {
        if(getStub() != null) {
            return getStub().getLastUpdatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastUpdatedTime;
        }
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        if(getStub() != null) {
            getStub().setLastUpdatedTime(lastUpdatedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastUpdatedTime = lastUpdatedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public RobotsTextFileStub getStub()
    {
        return this.stub;
    }
    protected void setStub(RobotsTextFileStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("siteUrl = " + this.siteUrl).append(";");
            sb.append("groups = " + this.groups).append(";");
            sb.append("sitemaps = " + this.sitemaps).append(";");
            sb.append("content = " + this.content).append(";");
            sb.append("contentHash = " + this.contentHash).append(";");
            sb.append("lastCheckedTime = " + this.lastCheckedTime).append(";");
            sb.append("lastUpdatedTime = " + this.lastUpdatedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = siteUrl == null ? 0 : siteUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = groups == null ? 0 : groups.hashCode();
            _hash = 31 * _hash + delta;
            delta = sitemaps == null ? 0 : sitemaps.hashCode();
            _hash = 31 * _hash + delta;
            delta = content == null ? 0 : content.hashCode();
            _hash = 31 * _hash + delta;
            delta = contentHash == null ? 0 : contentHash.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastCheckedTime == null ? 0 : lastCheckedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
