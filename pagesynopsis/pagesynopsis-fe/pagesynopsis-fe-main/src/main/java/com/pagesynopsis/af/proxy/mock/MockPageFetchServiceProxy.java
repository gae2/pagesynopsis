package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.PageFetchBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.PageFetchService;
import com.pagesynopsis.af.proxy.PageFetchServiceProxy;


// MockPageFetchServiceProxy is a decorator.
// It can be used as a base class to mock PageFetchServiceProxy objects.
public abstract class MockPageFetchServiceProxy implements PageFetchServiceProxy
{
    private static final Logger log = Logger.getLogger(MockPageFetchServiceProxy.class.getName());

    // MockPageFetchServiceProxy uses the decorator design pattern.
    private PageFetchServiceProxy decoratedProxy;

    public MockPageFetchServiceProxy(PageFetchServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected PageFetchServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(PageFetchServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public PageFetch getPageFetch(String guid) throws BaseException
    {
        return decoratedProxy.getPageFetch(guid);
    }

    @Override
    public Object getPageFetch(String guid, String field) throws BaseException
    {
        return decoratedProxy.getPageFetch(guid, field);       
    }

    @Override
    public List<PageFetch> getPageFetches(List<String> guids) throws BaseException
    {
        return decoratedProxy.getPageFetches(guids);
    }

    @Override
    public List<PageFetch> getAllPageFetches() throws BaseException
    {
        return getAllPageFetches(null, null, null);
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllPageFetches(ordering, offset, count);
        return getAllPageFetches(ordering, offset, count, null);
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllPageFetches(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllPageFetchKeys(ordering, offset, count);
        return getAllPageFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllPageFetchKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findPageFetches(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findPageFetches(filter, ordering, params, values, grouping, unique, offset, count);
        return findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createPageFetch(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {
        return decoratedProxy.createPageFetch(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
    }

    @Override
    public String createPageFetch(PageFetch pageFetch) throws BaseException
    {
        return decoratedProxy.createPageFetch(pageFetch);
    }

    @Override
    public Boolean updatePageFetch(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {
        return decoratedProxy.updatePageFetch(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
    }

    @Override
    public Boolean updatePageFetch(PageFetch pageFetch) throws BaseException
    {
        return decoratedProxy.updatePageFetch(pageFetch);
    }

    @Override
    public Boolean deletePageFetch(String guid) throws BaseException
    {
        return decoratedProxy.deletePageFetch(guid);
    }

    @Override
    public Boolean deletePageFetch(PageFetch pageFetch) throws BaseException
    {
        String guid = pageFetch.getGuid();
        return deletePageFetch(guid);
    }

    @Override
    public Long deletePageFetches(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deletePageFetches(filter, params, values);
    }

}
