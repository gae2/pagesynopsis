package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class VideoStructJsBean implements Serializable, Cloneable  //, VideoStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(VideoStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String id;
    private Integer width;
    private Integer height;
    private String controls;
    private Boolean autoplayEnabled;
    private Boolean loopEnabled;
    private Boolean preloadEnabled;
    private Boolean muted;
    private String remark;
    private MediaSourceStructJsBean source;
    private MediaSourceStructJsBean source1;
    private MediaSourceStructJsBean source2;
    private MediaSourceStructJsBean source3;
    private MediaSourceStructJsBean source4;
    private MediaSourceStructJsBean source5;
    private String note;

    // Ctors.
    public VideoStructJsBean()
    {
        //this((String) null);
    }
    public VideoStructJsBean(String uuid, String id, Integer width, Integer height, String controls, Boolean autoplayEnabled, Boolean loopEnabled, Boolean preloadEnabled, Boolean muted, String remark, MediaSourceStructJsBean source, MediaSourceStructJsBean source1, MediaSourceStructJsBean source2, MediaSourceStructJsBean source3, MediaSourceStructJsBean source4, MediaSourceStructJsBean source5, String note)
    {
        this.uuid = uuid;
        this.id = id;
        this.width = width;
        this.height = height;
        this.controls = controls;
        this.autoplayEnabled = autoplayEnabled;
        this.loopEnabled = loopEnabled;
        this.preloadEnabled = preloadEnabled;
        this.muted = muted;
        this.remark = remark;
        this.source = source;
        this.source1 = source1;
        this.source2 = source2;
        this.source3 = source3;
        this.source4 = source4;
        this.source5 = source5;
        this.note = note;
    }
    public VideoStructJsBean(VideoStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setId(bean.getId());
            setWidth(bean.getWidth());
            setHeight(bean.getHeight());
            setControls(bean.getControls());
            setAutoplayEnabled(bean.isAutoplayEnabled());
            setLoopEnabled(bean.isLoopEnabled());
            setPreloadEnabled(bean.isPreloadEnabled());
            setMuted(bean.isMuted());
            setRemark(bean.getRemark());
            setSource(bean.getSource());
            setSource1(bean.getSource1());
            setSource2(bean.getSource2());
            setSource3(bean.getSource3());
            setSource4(bean.getSource4());
            setSource5(bean.getSource5());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static VideoStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        VideoStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(VideoStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, VideoStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    public Integer getWidth()
    {
        return this.width;
    }
    public void setWidth(Integer width)
    {
        this.width = width;
    }

    public Integer getHeight()
    {
        return this.height;
    }
    public void setHeight(Integer height)
    {
        this.height = height;
    }

    public String getControls()
    {
        return this.controls;
    }
    public void setControls(String controls)
    {
        this.controls = controls;
    }

    public Boolean isAutoplayEnabled()
    {
        return this.autoplayEnabled;
    }
    public void setAutoplayEnabled(Boolean autoplayEnabled)
    {
        this.autoplayEnabled = autoplayEnabled;
    }

    public Boolean isLoopEnabled()
    {
        return this.loopEnabled;
    }
    public void setLoopEnabled(Boolean loopEnabled)
    {
        this.loopEnabled = loopEnabled;
    }

    public Boolean isPreloadEnabled()
    {
        return this.preloadEnabled;
    }
    public void setPreloadEnabled(Boolean preloadEnabled)
    {
        this.preloadEnabled = preloadEnabled;
    }

    public Boolean isMuted()
    {
        return this.muted;
    }
    public void setMuted(Boolean muted)
    {
        this.muted = muted;
    }

    public String getRemark()
    {
        return this.remark;
    }
    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    public MediaSourceStructJsBean getSource()
    {  
        return this.source;
    }
    public void setSource(MediaSourceStructJsBean source)
    {
        this.source = source;
    }

    public MediaSourceStructJsBean getSource1()
    {  
        return this.source1;
    }
    public void setSource1(MediaSourceStructJsBean source1)
    {
        this.source1 = source1;
    }

    public MediaSourceStructJsBean getSource2()
    {  
        return this.source2;
    }
    public void setSource2(MediaSourceStructJsBean source2)
    {
        this.source2 = source2;
    }

    public MediaSourceStructJsBean getSource3()
    {  
        return this.source3;
    }
    public void setSource3(MediaSourceStructJsBean source3)
    {
        this.source3 = source3;
    }

    public MediaSourceStructJsBean getSource4()
    {  
        return this.source4;
    }
    public void setSource4(MediaSourceStructJsBean source4)
    {
        this.source4 = source4;
    }

    public MediaSourceStructJsBean getSource5()
    {  
        return this.source5;
    }
    public void setSource5(MediaSourceStructJsBean source5)
    {
        this.source5 = source5;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getControls() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isAutoplayEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isLoopEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isPreloadEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isMuted() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRemark() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource3() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource4() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource5() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("id:null, ");
        sb.append("width:0, ");
        sb.append("height:0, ");
        sb.append("controls:null, ");
        sb.append("autoplayEnabled:false, ");
        sb.append("loopEnabled:false, ");
        sb.append("preloadEnabled:false, ");
        sb.append("muted:false, ");
        sb.append("remark:null, ");
        sb.append("source:{}, ");
        sb.append("source1:{}, ");
        sb.append("source2:{}, ");
        sb.append("source3:{}, ");
        sb.append("source4:{}, ");
        sb.append("source5:{}, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("id:");
        if(this.getId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getId()).append("\", ");
        }
        sb.append("width:" + this.getWidth()).append(", ");
        sb.append("height:" + this.getHeight()).append(", ");
        sb.append("controls:");
        if(this.getControls() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getControls()).append("\", ");
        }
        sb.append("autoplayEnabled:" + this.isAutoplayEnabled()).append(", ");
        sb.append("loopEnabled:" + this.isLoopEnabled()).append(", ");
        sb.append("preloadEnabled:" + this.isPreloadEnabled()).append(", ");
        sb.append("muted:" + this.isMuted()).append(", ");
        sb.append("remark:");
        if(this.getRemark() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRemark()).append("\", ");
        }
        sb.append("source:");
        if(this.getSource() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSource()).append("\", ");
        }
        sb.append("source1:");
        if(this.getSource1() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSource1()).append("\", ");
        }
        sb.append("source2:");
        if(this.getSource2() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSource2()).append("\", ");
        }
        sb.append("source3:");
        if(this.getSource3() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSource3()).append("\", ");
        }
        sb.append("source4:");
        if(this.getSource4() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSource4()).append("\", ");
        }
        sb.append("source5:");
        if(this.getSource5() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSource5()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getId() != null) {
            sb.append("\"id\":").append("\"").append(this.getId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"id\":").append("null, ");
        }
        if(this.getWidth() != null) {
            sb.append("\"width\":").append("").append(this.getWidth()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"width\":").append("null, ");
        }
        if(this.getHeight() != null) {
            sb.append("\"height\":").append("").append(this.getHeight()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"height\":").append("null, ");
        }
        if(this.getControls() != null) {
            sb.append("\"controls\":").append("\"").append(this.getControls()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"controls\":").append("null, ");
        }
        if(this.isAutoplayEnabled() != null) {
            sb.append("\"autoplayEnabled\":").append("").append(this.isAutoplayEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"autoplayEnabled\":").append("null, ");
        }
        if(this.isLoopEnabled() != null) {
            sb.append("\"loopEnabled\":").append("").append(this.isLoopEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"loopEnabled\":").append("null, ");
        }
        if(this.isPreloadEnabled() != null) {
            sb.append("\"preloadEnabled\":").append("").append(this.isPreloadEnabled()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"preloadEnabled\":").append("null, ");
        }
        if(this.isMuted() != null) {
            sb.append("\"muted\":").append("").append(this.isMuted()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"muted\":").append("null, ");
        }
        if(this.getRemark() != null) {
            sb.append("\"remark\":").append("\"").append(this.getRemark()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"remark\":").append("null, ");
        }
        sb.append("\"source\":").append(this.source.toJsonString()).append(", ");
        sb.append("\"source1\":").append(this.source1.toJsonString()).append(", ");
        sb.append("\"source2\":").append(this.source2.toJsonString()).append(", ");
        sb.append("\"source3\":").append(this.source3.toJsonString()).append(", ");
        sb.append("\"source4\":").append(this.source4.toJsonString()).append(", ");
        sb.append("\"source5\":").append(this.source5.toJsonString()).append(", ");
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("id = " + this.id).append(";");
        sb.append("width = " + this.width).append(";");
        sb.append("height = " + this.height).append(";");
        sb.append("controls = " + this.controls).append(";");
        sb.append("autoplayEnabled = " + this.autoplayEnabled).append(";");
        sb.append("loopEnabled = " + this.loopEnabled).append(";");
        sb.append("preloadEnabled = " + this.preloadEnabled).append(";");
        sb.append("muted = " + this.muted).append(";");
        sb.append("remark = " + this.remark).append(";");
        sb.append("source = " + this.source).append(";");
        sb.append("source1 = " + this.source1).append(";");
        sb.append("source2 = " + this.source2).append(";");
        sb.append("source3 = " + this.source3).append(";");
        sb.append("source4 = " + this.source4).append(";");
        sb.append("source5 = " + this.source5).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        VideoStructJsBean cloned = new VideoStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setId(this.getId());   
        cloned.setWidth(this.getWidth());   
        cloned.setHeight(this.getHeight());   
        cloned.setControls(this.getControls());   
        cloned.setAutoplayEnabled(this.isAutoplayEnabled());   
        cloned.setLoopEnabled(this.isLoopEnabled());   
        cloned.setPreloadEnabled(this.isPreloadEnabled());   
        cloned.setMuted(this.isMuted());   
        cloned.setRemark(this.getRemark());   
        cloned.setSource( (MediaSourceStructJsBean) this.getSource().clone() );
        cloned.setSource1( (MediaSourceStructJsBean) this.getSource1().clone() );
        cloned.setSource2( (MediaSourceStructJsBean) this.getSource2().clone() );
        cloned.setSource3( (MediaSourceStructJsBean) this.getSource3().clone() );
        cloned.setSource4( (MediaSourceStructJsBean) this.getSource4().clone() );
        cloned.setSource5( (MediaSourceStructJsBean) this.getSource5().clone() );
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
