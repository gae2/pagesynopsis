package com.pagesynopsis.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.af.bean.OgActorStructBean;


public class OgActorStructResourceUtil
{
    private static final Logger log = Logger.getLogger(OgActorStructResourceUtil.class.getName());

    // Static methods only.
    private OgActorStructResourceUtil() {}

    public static OgActorStructBean convertOgActorStructStubToBean(OgActorStruct stub)
    {
        OgActorStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new OgActorStructBean();
            bean.setUuid(stub.getUuid());
            bean.setProfile(stub.getProfile());
            bean.setRole(stub.getRole());
        }
        return bean;
    }

}
