package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.stub.OgBlogStub;
import com.pagesynopsis.ws.stub.OgBlogListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.resource.OgBlogResource;
import com.pagesynopsis.af.resource.util.OgAudioStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgImageStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgActorStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgVideoStructResourceUtil;


// MockOgBlogResource is a decorator.
// It can be used as a base class to mock OgBlogResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/ogBlogs/")
public abstract class MockOgBlogResource implements OgBlogResource
{
    private static final Logger log = Logger.getLogger(MockOgBlogResource.class.getName());

    // MockOgBlogResource uses the decorator design pattern.
    private OgBlogResource decoratedResource;

    public MockOgBlogResource(OgBlogResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected OgBlogResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(OgBlogResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllOgBlogs(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgBlogs(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllOgBlogKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgBlogKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgBlogsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findOgBlogsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getOgBlogAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getOgBlogAsHtml(guid);
//    }

    @Override
    public Response getOgBlog(String guid) throws BaseResourceException
    {
        return decoratedResource.getOgBlog(guid);
    }

    @Override
    public Response getOgBlogAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getOgBlogAsJsonp(guid, callback);
    }

    @Override
    public Response getOgBlog(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getOgBlog(guid, field);
    }

    // TBD
    @Override
    public Response constructOgBlog(OgBlogStub ogBlog) throws BaseResourceException
    {
        return decoratedResource.constructOgBlog(ogBlog);
    }

    @Override
    public Response createOgBlog(OgBlogStub ogBlog) throws BaseResourceException
    {
        return decoratedResource.createOgBlog(ogBlog);
    }

//    @Override
//    public Response createOgBlog(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createOgBlog(formParams);
//    }

    // TBD
    @Override
    public Response refreshOgBlog(String guid, OgBlogStub ogBlog) throws BaseResourceException
    {
        return decoratedResource.refreshOgBlog(guid, ogBlog);
    }

    @Override
    public Response updateOgBlog(String guid, OgBlogStub ogBlog) throws BaseResourceException
    {
        return decoratedResource.updateOgBlog(guid, ogBlog);
    }

    @Override
    public Response updateOgBlog(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<String> image, List<String> audio, List<String> video, String locale, List<String> localeAlternate, String note)
    {
        return decoratedResource.updateOgBlog(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

//    @Override
//    public Response updateOgBlog(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateOgBlog(guid, formParams);
//    }

    @Override
    public Response deleteOgBlog(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteOgBlog(guid);
    }

    @Override
    public Response deleteOgBlogs(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteOgBlogs(filter, params, values);
    }


// TBD ....
    @Override
    public Response createOgBlogs(OgBlogListStub ogBlogs) throws BaseResourceException
    {
        return decoratedResource.createOgBlogs(ogBlogs);
    }


}
