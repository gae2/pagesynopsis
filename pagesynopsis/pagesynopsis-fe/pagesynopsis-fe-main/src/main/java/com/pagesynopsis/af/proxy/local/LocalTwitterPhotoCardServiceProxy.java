package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPhotoCard;
// import com.pagesynopsis.ws.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.proxy.TwitterPhotoCardServiceProxy;


public class LocalTwitterPhotoCardServiceProxy extends BaseLocalServiceProxy implements TwitterPhotoCardServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterPhotoCardServiceProxy.class.getName());

    public LocalTwitterPhotoCardServiceProxy()
    {
    }

    @Override
    public TwitterPhotoCard getTwitterPhotoCard(String guid) throws BaseException
    {
        TwitterPhotoCard serverBean = getTwitterPhotoCardService().getTwitterPhotoCard(guid);
        TwitterPhotoCard appBean = convertServerTwitterPhotoCardBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getTwitterPhotoCard(String guid, String field) throws BaseException
    {
        return getTwitterPhotoCardService().getTwitterPhotoCard(guid, field);       
    }

    @Override
    public List<TwitterPhotoCard> getTwitterPhotoCards(List<String> guids) throws BaseException
    {
        List<TwitterPhotoCard> serverBeanList = getTwitterPhotoCardService().getTwitterPhotoCards(guids);
        List<TwitterPhotoCard> appBeanList = convertServerTwitterPhotoCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards() throws BaseException
    {
        return getAllTwitterPhotoCards(null, null, null);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterPhotoCardService().getAllTwitterPhotoCards(ordering, offset, count);
        return getAllTwitterPhotoCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterPhotoCard> serverBeanList = getTwitterPhotoCardService().getAllTwitterPhotoCards(ordering, offset, count, forwardCursor);
        List<TwitterPhotoCard> appBeanList = convertServerTwitterPhotoCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterPhotoCardService().getAllTwitterPhotoCardKeys(ordering, offset, count);
        return getAllTwitterPhotoCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterPhotoCardService().getAllTwitterPhotoCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterPhotoCardService().findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterPhotoCard> serverBeanList = getTwitterPhotoCardService().findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<TwitterPhotoCard> appBeanList = convertServerTwitterPhotoCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterPhotoCardService().findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterPhotoCardService().findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterPhotoCardService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterPhotoCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return getTwitterPhotoCardService().createTwitterPhotoCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public String createTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterPhotoCardBean serverBean =  convertAppTwitterPhotoCardBeanToServerBean(twitterPhotoCard);
        return getTwitterPhotoCardService().createTwitterPhotoCard(serverBean);
    }

    @Override
    public Boolean updateTwitterPhotoCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return getTwitterPhotoCardService().updateTwitterPhotoCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public Boolean updateTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterPhotoCardBean serverBean =  convertAppTwitterPhotoCardBeanToServerBean(twitterPhotoCard);
        return getTwitterPhotoCardService().updateTwitterPhotoCard(serverBean);
    }

    @Override
    public Boolean deleteTwitterPhotoCard(String guid) throws BaseException
    {
        return getTwitterPhotoCardService().deleteTwitterPhotoCard(guid);
    }

    @Override
    public Boolean deleteTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterPhotoCardBean serverBean =  convertAppTwitterPhotoCardBeanToServerBean(twitterPhotoCard);
        return getTwitterPhotoCardService().deleteTwitterPhotoCard(serverBean);
    }

    @Override
    public Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterPhotoCardService().deleteTwitterPhotoCards(filter, params, values);
    }




    public static TwitterPhotoCardBean convertServerTwitterPhotoCardBeanToAppBean(TwitterPhotoCard serverBean)
    {
        TwitterPhotoCardBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new TwitterPhotoCardBean();
            bean.setGuid(serverBean.getGuid());
            bean.setCard(serverBean.getCard());
            bean.setUrl(serverBean.getUrl());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setSite(serverBean.getSite());
            bean.setSiteId(serverBean.getSiteId());
            bean.setCreator(serverBean.getCreator());
            bean.setCreatorId(serverBean.getCreatorId());
            bean.setImage(serverBean.getImage());
            bean.setImageWidth(serverBean.getImageWidth());
            bean.setImageHeight(serverBean.getImageHeight());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterPhotoCard> convertServerTwitterPhotoCardBeanListToAppBeanList(List<TwitterPhotoCard> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterPhotoCard> beanList = new ArrayList<TwitterPhotoCard>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(TwitterPhotoCard sb : serverBeanList) {
                TwitterPhotoCardBean bean = convertServerTwitterPhotoCardBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.TwitterPhotoCardBean convertAppTwitterPhotoCardBeanToServerBean(TwitterPhotoCard appBean)
    {
        com.pagesynopsis.ws.bean.TwitterPhotoCardBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.TwitterPhotoCardBean();
            bean.setGuid(appBean.getGuid());
            bean.setCard(appBean.getCard());
            bean.setUrl(appBean.getUrl());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setSite(appBean.getSite());
            bean.setSiteId(appBean.getSiteId());
            bean.setCreator(appBean.getCreator());
            bean.setCreatorId(appBean.getCreatorId());
            bean.setImage(appBean.getImage());
            bean.setImageWidth(appBean.getImageWidth());
            bean.setImageHeight(appBean.getImageHeight());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterPhotoCard> convertAppTwitterPhotoCardBeanListToServerBeanList(List<TwitterPhotoCard> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterPhotoCard> beanList = new ArrayList<TwitterPhotoCard>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(TwitterPhotoCard sb : appBeanList) {
                com.pagesynopsis.ws.bean.TwitterPhotoCardBean bean = convertAppTwitterPhotoCardBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
