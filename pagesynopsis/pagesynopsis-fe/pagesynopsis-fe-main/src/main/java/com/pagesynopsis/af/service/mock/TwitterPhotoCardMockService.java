package com.pagesynopsis.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterPhotoCardService;


// TwitterPhotoCardMockService is a decorator.
// It can be used as a base class to mock TwitterPhotoCardService objects.
public abstract class TwitterPhotoCardMockService implements TwitterPhotoCardService
{
    private static final Logger log = Logger.getLogger(TwitterPhotoCardMockService.class.getName());

    // TwitterPhotoCardMockService uses the decorator design pattern.
    private TwitterPhotoCardService decoratedService;

    public TwitterPhotoCardMockService(TwitterPhotoCardService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected TwitterPhotoCardService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(TwitterPhotoCardService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // TwitterPhotoCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterPhotoCard getTwitterPhotoCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterPhotoCard(): guid = " + guid);
        TwitterPhotoCard bean = decoratedService.getTwitterPhotoCard(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterPhotoCard(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getTwitterPhotoCard(guid, field);
        return obj;
    }

    @Override
    public List<TwitterPhotoCard> getTwitterPhotoCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterPhotoCards()");
        List<TwitterPhotoCard> twitterPhotoCards = decoratedService.getTwitterPhotoCards(guids);
        log.finer("END");
        return twitterPhotoCards;
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards() throws BaseException
    {
        return getAllTwitterPhotoCards(null, null, null);
    }


    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPhotoCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterPhotoCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<TwitterPhotoCard> twitterPhotoCards = decoratedService.getAllTwitterPhotoCards(ordering, offset, count, forwardCursor);
        log.finer("END");
        return twitterPhotoCards;
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPhotoCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterPhotoCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllTwitterPhotoCardKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPhotoCardMockService.findTwitterPhotoCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<TwitterPhotoCard> twitterPhotoCards = decoratedService.findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return twitterPhotoCards;
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPhotoCardMockService.findTwitterPhotoCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPhotoCardMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterPhotoCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        TwitterPhotoCardBean bean = new TwitterPhotoCardBean(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        return createTwitterPhotoCard(bean);
    }

    @Override
    public String createTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createTwitterPhotoCard(twitterPhotoCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TwitterPhotoCard constructTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("BEGIN");
        TwitterPhotoCard bean = decoratedService.constructTwitterPhotoCard(twitterPhotoCard);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTwitterPhotoCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterPhotoCardBean bean = new TwitterPhotoCardBean(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        return updateTwitterPhotoCard(bean);
    }
        
    @Override
    public Boolean updateTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateTwitterPhotoCard(twitterPhotoCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TwitterPhotoCard refreshTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("BEGIN");
        TwitterPhotoCard bean = decoratedService.refreshTwitterPhotoCard(twitterPhotoCard);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTwitterPhotoCard(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterPhotoCard(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterPhotoCard(twitterPhotoCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteTwitterPhotoCards(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterPhotoCards(List<TwitterPhotoCard> twitterPhotoCards) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createTwitterPhotoCards(twitterPhotoCards);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterPhotoCards(List<TwitterPhotoCard> twitterPhotoCards) throws BaseException
    //{
    //}

}
