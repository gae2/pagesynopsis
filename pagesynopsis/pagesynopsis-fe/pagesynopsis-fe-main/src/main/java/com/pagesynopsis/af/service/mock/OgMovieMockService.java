package com.pagesynopsis.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgMovieService;


// OgMovieMockService is a decorator.
// It can be used as a base class to mock OgMovieService objects.
public abstract class OgMovieMockService implements OgMovieService
{
    private static final Logger log = Logger.getLogger(OgMovieMockService.class.getName());

    // OgMovieMockService uses the decorator design pattern.
    private OgMovieService decoratedService;

    public OgMovieMockService(OgMovieService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected OgMovieService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(OgMovieService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // OgMovie related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgMovie getOgMovie(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgMovie(): guid = " + guid);
        OgMovie bean = decoratedService.getOgMovie(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgMovie(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getOgMovie(guid, field);
        return obj;
    }

    @Override
    public List<OgMovie> getOgMovies(List<String> guids) throws BaseException
    {
        log.fine("getOgMovies()");
        List<OgMovie> ogMovies = decoratedService.getOgMovies(guids);
        log.finer("END");
        return ogMovies;
    }

    @Override
    public List<OgMovie> getAllOgMovies() throws BaseException
    {
        return getAllOgMovies(null, null, null);
    }


    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgMovies(ordering, offset, count, null);
    }

    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgMovies(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<OgMovie> ogMovies = decoratedService.getAllOgMovies(ordering, offset, count, forwardCursor);
        log.finer("END");
        return ogMovies;
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgMovieKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgMovieKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllOgMovieKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgMovieMockService.findOgMovies(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<OgMovie> ogMovies = decoratedService.findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return ogMovies;
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgMovieMockService.findOgMovieKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgMovieMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgMovie(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        OgMovieBean bean = new OgMovieBean(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return createOgMovie(bean);
    }

    @Override
    public String createOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createOgMovie(ogMovie);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public OgMovie constructOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");
        OgMovie bean = decoratedService.constructOgMovie(ogMovie);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateOgMovie(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgMovieBean bean = new OgMovieBean(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return updateOgMovie(bean);
    }
        
    @Override
    public Boolean updateOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateOgMovie(ogMovie);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public OgMovie refreshOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");
        OgMovie bean = decoratedService.refreshOgMovie(ogMovie);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteOgMovie(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgMovie(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgMovie(ogMovie);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOgMovies(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteOgMovies(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgMovies(List<OgMovie> ogMovies) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createOgMovies(ogMovies);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateOgMovies(List<OgMovie> ogMovies) throws BaseException
    //{
    //}

}
