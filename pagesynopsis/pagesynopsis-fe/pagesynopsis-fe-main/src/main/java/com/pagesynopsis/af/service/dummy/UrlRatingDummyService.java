package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.UrlRatingService;


// The primary purpose of UrlRatingDummyService is to fake the service api, UrlRatingService.
// It has no real implementation.
public class UrlRatingDummyService implements UrlRatingService
{
    private static final Logger log = Logger.getLogger(UrlRatingDummyService.class.getName());

    public UrlRatingDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // UrlRating related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UrlRating getUrlRating(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUrlRating(): guid = " + guid);
        return null;
    }

    @Override
    public Object getUrlRating(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUrlRating(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<UrlRating> getUrlRatings(List<String> guids) throws BaseException
    {
        log.fine("getUrlRatings()");
        return null;
    }

    @Override
    public List<UrlRating> getAllUrlRatings() throws BaseException
    {
        return getAllUrlRatings(null, null, null);
    }


    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUrlRatings(ordering, offset, count, null);
    }

    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUrlRatings(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUrlRatingKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUrlRatingKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUrlRatings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UrlRatingDummyService.findUrlRatings(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UrlRatingDummyService.findUrlRatingKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UrlRatingDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createUrlRating(String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException
    {
        log.finer("createUrlRating()");
        return null;
    }

    @Override
    public String createUrlRating(UrlRating urlRating) throws BaseException
    {
        log.finer("createUrlRating()");
        return null;
    }

    @Override
    public UrlRating constructUrlRating(UrlRating urlRating) throws BaseException
    {
        log.finer("constructUrlRating()");
        return null;
    }

    @Override
    public Boolean updateUrlRating(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException
    {
        log.finer("updateUrlRating()");
        return null;
    }
        
    @Override
    public Boolean updateUrlRating(UrlRating urlRating) throws BaseException
    {
        log.finer("updateUrlRating()");
        return null;
    }

    @Override
    public UrlRating refreshUrlRating(UrlRating urlRating) throws BaseException
    {
        log.finer("refreshUrlRating()");
        return null;
    }

    @Override
    public Boolean deleteUrlRating(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteUrlRating(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteUrlRating(UrlRating urlRating) throws BaseException
    {
        log.finer("deleteUrlRating()");
        return null;
    }

    // TBD
    @Override
    public Long deleteUrlRatings(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteUrlRating(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUrlRatings(List<UrlRating> urlRatings) throws BaseException
    {
        log.finer("createUrlRatings()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateUrlRatings(List<UrlRating> urlRatings) throws BaseException
    //{
    //}

}
