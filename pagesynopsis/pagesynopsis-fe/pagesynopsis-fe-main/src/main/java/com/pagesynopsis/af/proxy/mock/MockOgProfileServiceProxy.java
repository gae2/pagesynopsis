package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.af.proxy.OgProfileServiceProxy;


// MockOgProfileServiceProxy is a decorator.
// It can be used as a base class to mock OgProfileServiceProxy objects.
public abstract class MockOgProfileServiceProxy implements OgProfileServiceProxy
{
    private static final Logger log = Logger.getLogger(MockOgProfileServiceProxy.class.getName());

    // MockOgProfileServiceProxy uses the decorator design pattern.
    private OgProfileServiceProxy decoratedProxy;

    public MockOgProfileServiceProxy(OgProfileServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected OgProfileServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(OgProfileServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public OgProfile getOgProfile(String guid) throws BaseException
    {
        return decoratedProxy.getOgProfile(guid);
    }

    @Override
    public Object getOgProfile(String guid, String field) throws BaseException
    {
        return decoratedProxy.getOgProfile(guid, field);       
    }

    @Override
    public List<OgProfile> getOgProfiles(List<String> guids) throws BaseException
    {
        return decoratedProxy.getOgProfiles(guids);
    }

    @Override
    public List<OgProfile> getAllOgProfiles() throws BaseException
    {
        return getAllOgProfiles(null, null, null);
    }

    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgProfiles(ordering, offset, count);
        return getAllOgProfiles(ordering, offset, count, null);
    }

    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgProfiles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgProfileKeys(ordering, offset, count);
        return getAllOgProfileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgProfileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgProfiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgProfile(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        return decoratedProxy.createOgProfile(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
    }

    @Override
    public String createOgProfile(OgProfile ogProfile) throws BaseException
    {
        return decoratedProxy.createOgProfile(ogProfile);
    }

    @Override
    public Boolean updateOgProfile(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        return decoratedProxy.updateOgProfile(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
    }

    @Override
    public Boolean updateOgProfile(OgProfile ogProfile) throws BaseException
    {
        return decoratedProxy.updateOgProfile(ogProfile);
    }

    @Override
    public Boolean deleteOgProfile(String guid) throws BaseException
    {
        return decoratedProxy.deleteOgProfile(guid);
    }

    @Override
    public Boolean deleteOgProfile(OgProfile ogProfile) throws BaseException
    {
        String guid = ogProfile.getGuid();
        return deleteOgProfile(guid);
    }

    @Override
    public Long deleteOgProfiles(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteOgProfiles(filter, params, values);
    }

}
