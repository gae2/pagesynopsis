package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardListStub;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.resource.TwitterSummaryCardResource;
import com.pagesynopsis.af.resource.util.TwitterCardAppInfoResourceUtil;
import com.pagesynopsis.af.resource.util.TwitterCardProductDataResourceUtil;


// MockTwitterSummaryCardResource is a decorator.
// It can be used as a base class to mock TwitterSummaryCardResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/twitterSummaryCards/")
public abstract class MockTwitterSummaryCardResource implements TwitterSummaryCardResource
{
    private static final Logger log = Logger.getLogger(MockTwitterSummaryCardResource.class.getName());

    // MockTwitterSummaryCardResource uses the decorator design pattern.
    private TwitterSummaryCardResource decoratedResource;

    public MockTwitterSummaryCardResource(TwitterSummaryCardResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected TwitterSummaryCardResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(TwitterSummaryCardResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllTwitterSummaryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterSummaryCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterSummaryCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterSummaryCardsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findTwitterSummaryCardsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getTwitterSummaryCardAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getTwitterSummaryCardAsHtml(guid);
//    }

    @Override
    public Response getTwitterSummaryCard(String guid) throws BaseResourceException
    {
        return decoratedResource.getTwitterSummaryCard(guid);
    }

    @Override
    public Response getTwitterSummaryCardAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getTwitterSummaryCardAsJsonp(guid, callback);
    }

    @Override
    public Response getTwitterSummaryCard(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getTwitterSummaryCard(guid, field);
    }

    // TBD
    @Override
    public Response constructTwitterSummaryCard(TwitterSummaryCardStub twitterSummaryCard) throws BaseResourceException
    {
        return decoratedResource.constructTwitterSummaryCard(twitterSummaryCard);
    }

    @Override
    public Response createTwitterSummaryCard(TwitterSummaryCardStub twitterSummaryCard) throws BaseResourceException
    {
        return decoratedResource.createTwitterSummaryCard(twitterSummaryCard);
    }

//    @Override
//    public Response createTwitterSummaryCard(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createTwitterSummaryCard(formParams);
//    }

    // TBD
    @Override
    public Response refreshTwitterSummaryCard(String guid, TwitterSummaryCardStub twitterSummaryCard) throws BaseResourceException
    {
        return decoratedResource.refreshTwitterSummaryCard(guid, twitterSummaryCard);
    }

    @Override
    public Response updateTwitterSummaryCard(String guid, TwitterSummaryCardStub twitterSummaryCard) throws BaseResourceException
    {
        return decoratedResource.updateTwitterSummaryCard(guid, twitterSummaryCard);
    }

    @Override
    public Response updateTwitterSummaryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight)
    {
        return decoratedResource.updateTwitterSummaryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

//    @Override
//    public Response updateTwitterSummaryCard(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateTwitterSummaryCard(guid, formParams);
//    }

    @Override
    public Response deleteTwitterSummaryCard(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterSummaryCard(guid);
    }

    @Override
    public Response deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterSummaryCards(filter, params, values);
    }


// TBD ....
    @Override
    public Response createTwitterSummaryCards(TwitterSummaryCardListStub twitterSummaryCards) throws BaseResourceException
    {
        return decoratedResource.createTwitterSummaryCards(twitterSummaryCards);
    }


}
