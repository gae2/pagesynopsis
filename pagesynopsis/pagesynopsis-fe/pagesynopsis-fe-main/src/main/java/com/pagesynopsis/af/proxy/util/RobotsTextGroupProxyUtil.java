package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.RobotsTextGroup;
// import com.pagesynopsis.ws.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;


public class RobotsTextGroupProxyUtil
{
    private static final Logger log = Logger.getLogger(RobotsTextGroupProxyUtil.class.getName());

    // Static methods only.
    private RobotsTextGroupProxyUtil() {}

    public static RobotsTextGroupBean convertServerRobotsTextGroupBeanToAppBean(RobotsTextGroup serverBean)
    {
        RobotsTextGroupBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new RobotsTextGroupBean();
            bean.setUuid(serverBean.getUuid());
            bean.setUserAgent(serverBean.getUserAgent());
            bean.setAllows(serverBean.getAllows());
            bean.setDisallows(serverBean.getDisallows());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.RobotsTextGroupBean convertAppRobotsTextGroupBeanToServerBean(RobotsTextGroup appBean)
    {
        com.pagesynopsis.ws.bean.RobotsTextGroupBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.RobotsTextGroupBean();
            bean.setUuid(appBean.getUuid());
            bean.setUserAgent(appBean.getUserAgent());
            bean.setAllows(appBean.getAllows());
            bean.setDisallows(appBean.getDisallows());
        }
        return bean;
    }

}
