package com.pagesynopsis.af.resource.impl;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.DatatypeConverter;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.json.JSONWithPadding;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.af.config.Config;
import com.pagesynopsis.af.auth.TwoLeggedOAuthProvider;
import com.pagesynopsis.af.auth.common.AuthMethod;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;
import com.pagesynopsis.ws.resource.exception.UnauthorizedRsException;

import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.RobotsTextRefreshStub;
import com.pagesynopsis.ws.stub.RobotsTextRefreshListStub;
import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.af.service.manager.ServiceManager;
import com.pagesynopsis.af.resource.RobotsTextRefreshResource;


@Path("/robotsTextRefreshes/")
public class RobotsTextRefreshResourceImpl extends BaseResourceImpl implements RobotsTextRefreshResource
{
    private static final Logger log = Logger.getLogger(RobotsTextRefreshResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public RobotsTextRefreshResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;

        // TBD:
        // The auth-related checking should be done here rather than putting the same code in every method... ???
        // ...
    }

    // Throws exception if auth check fails
    private void doAuthCheck() throws BaseResourceException
    {
        // if(isRunningOnDevel && isIgnoreAuth()) {
        if(isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, then it could be a serious error.");
        } else {
            String authMethod = getAuthMethod();
            if(authMethod.equals(AuthMethod.NONE)) {
                // Auth ignored.
            } else if(authMethod.equals(AuthMethod.BASIC)) {
                String authType = null;
                String encodedAuthToken = null;
                if(httpHeaders != null) {
                    List<String> authList = httpHeaders.getRequestHeader("Authorization");
                    if(authList != null && !authList.isEmpty()) {
                        String authLine = authList.get(0);    // Use the first value.
                        String[] parts = authLine.split("\\s+", 2);
                        authType = parts[0];
                        encodedAuthToken = parts[1];
                    }
                }
                if(authType == null || ! authType.equalsIgnoreCase("Basic")) {
                    log.warning("HTTP Basic authentication failed. Request authType = " + authType);
                    throw new UnauthorizedRsException("HTTP Basic authentication failed. Request authType = " + authType, resourceUri);
                }
                String authToken = null;
                if(encodedAuthToken != null) {
                    byte[] decodedBytes = DatatypeConverter.parseBase64Binary(encodedAuthToken);
                    authToken = new String(decodedBytes);
                }
                String serverUsernamePasswordPair = getHttpBasicAuthUsernameAndPassword(null);
                if(authToken == null || ! authToken.equals(serverUsernamePasswordPair)) {
                    log.warning("HTTP Basic authentication failed: Invalid username/password.");
                    throw new UnauthorizedRsException("HTTP Basic authentication failed: Invalid username/password.", resourceUri);                        
                }
            } else if(authMethod.equals(AuthMethod.BEARER)) {
                String tokenType = null;
                String bearerToken = null;
                if(httpHeaders != null) {
                    List<String> authList = httpHeaders.getRequestHeader("Authorization");
                    if(authList != null && !authList.isEmpty()) {
                        String authLine = authList.get(0);    // Use the first value.
                        String[] parts = authLine.split("\\s+", 2);
                        tokenType = parts[0];
                        bearerToken = parts[1];
                    }
                }
                if(tokenType == null || ! tokenType.equalsIgnoreCase("Bearer")) {
                    log.warning("OAuth2 authorization failed. Request tokenType = " + tokenType);
                    throw new UnauthorizedRsException("OAuth2 authorization failed. Request tokenType = " + tokenType, resourceUri);
                }
                String serverUserAccessToken = getUserOAuth2AccessToken(null);
                if(bearerToken == null || ! bearerToken.equals(serverUserAccessToken)) {
                    log.warning("OAuth2 authorization failed: Invalid bearer token");
                    throw new UnauthorizedRsException("OAuth2 authorization failed: Invalid bearer token", resourceUri);                        
                }
            } else if(authMethod.equals(AuthMethod.TWOLEGGED)) {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            } else {
                // Unsupported auth method.
                log.warning("AuthMethod currently unsupported: " + authMethod);
                throw new UnauthorizedRsException("AuthMethod currently unsupported: " + authMethod, resourceUri);
            }
        }
    }


    private Response getRobotsTextRefreshList(List<RobotsTextRefresh> beans, StringCursor forwardCursor) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }

        RobotsTextRefreshListStub listStub = null;
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            listStub = new RobotsTextRefreshListStub();
        } else {
            List<RobotsTextRefreshStub> stubs = new ArrayList<RobotsTextRefreshStub>();
            Iterator<RobotsTextRefresh> it = beans.iterator();
            while(it.hasNext()) {
                RobotsTextRefresh bean = (RobotsTextRefresh) it.next();
                stubs.add(RobotsTextRefreshStub.convertBeanToStub(bean));
            }
            listStub = new RobotsTextRefreshListStub(stubs);
        }
        String cursorString = null;
        if(forwardCursor != null) {
            cursorString = forwardCursor.getWebSafeString();
        }
        listStub.setForwardCursor(cursorString);
        return Response.ok(listStub).build();        
    }

    // TBD
    private Response getRobotsTextRefreshListAsJsonp(List<RobotsTextRefresh> beans, StringCursor forwardCursor, String callback) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }

        RobotsTextRefreshListStub listStub = null;
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            listStub = new RobotsTextRefreshListStub();
        } else {
            List<RobotsTextRefreshStub> stubs = new ArrayList<RobotsTextRefreshStub>();
            Iterator<RobotsTextRefresh> it = beans.iterator();
            while(it.hasNext()) {
                RobotsTextRefresh bean = (RobotsTextRefresh) it.next();
                stubs.add(RobotsTextRefreshStub.convertBeanToStub(bean));
            }
            listStub = new RobotsTextRefreshListStub(stubs);
        }
        String cursorString = null;
        if(forwardCursor != null) {
            cursorString = forwardCursor.getWebSafeString();
        }
        listStub.setForwardCursor(cursorString);
        return Response.ok(new JSONWithPadding(listStub, callback)).build();        
    }

    @Override
    public Response getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<RobotsTextRefresh> beans = ServiceManager.getRobotsTextRefreshService().getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
            return getRobotsTextRefreshList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<String> keys = ServiceManager.getRobotsTextRefreshService().getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<String> keys = ServiceManager.getRobotsTextRefreshService().findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<RobotsTextRefresh> beans = ServiceManager.getRobotsTextRefreshService().findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return getRobotsTextRefreshList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findRobotsTextRefreshesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<RobotsTextRefresh> beans = ServiceManager.getRobotsTextRefreshService().findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return getRobotsTextRefreshListAsJsonp(beans, forwardCursor, callback);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            Long count = ServiceManager.getRobotsTextRefreshService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
//    public Response getRobotsTextRefreshAsHtml(String guid) throws BaseResourceException
//    {
//        // TBD
//        throw new NotImplementedRsException("Html format currently not supported.", resourceUri);
//    }

    @Override
    public Response getRobotsTextRefresh(String guid) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            RobotsTextRefresh bean = ServiceManager.getRobotsTextRefreshService().getRobotsTextRefresh(guid);
            RobotsTextRefreshStub stub = RobotsTextRefreshStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("RobotsTextRefresh stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full RobotsTextRefresh stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getRobotsTextRefreshAsJsonp(String guid, String callback) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            RobotsTextRefresh bean = ServiceManager.getRobotsTextRefreshService().getRobotsTextRefresh(guid);
            RobotsTextRefreshStub stub = RobotsTextRefreshStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("RobotsTextRefresh stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full RobotsTextRefresh stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getRobotsTextRefresh(String guid, String field) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            if(guid != null) {
                guid = guid.toLowerCase();  // TBD: Validate/normalize guid....
            }
            if(field == null || field.trim().length() == 0) {
                return getRobotsTextRefresh(guid);
            }
            RobotsTextRefresh bean = ServiceManager.getRobotsTextRefreshService().getRobotsTextRefresh(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("robotsTextFile")) {
                    String fval = bean.getRobotsTextFile();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("refreshInterval")) {
                    Integer fval = bean.getRefreshInterval();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("note")) {
                    String fval = bean.getNote();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("status")) {
                    String fval = bean.getStatus();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("refreshStatus")) {
                    Integer fval = bean.getRefreshStatus();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("result")) {
                    String fval = bean.getResult();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("lastCheckedTime")) {
                    Long fval = bean.getLastCheckedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("nextCheckedTime")) {
                    Long fval = bean.getNextCheckedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("expirationTime")) {
                    Long fval = bean.getExpirationTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    // TBD
    @Override
    public Response constructRobotsTextRefresh(RobotsTextRefreshStub robotsTextRefresh) throws BaseResourceException
    {
        doAuthCheck();

        try {
            RobotsTextRefreshBean bean = convertRobotsTextRefreshStubToBean(robotsTextRefresh);
            bean = (RobotsTextRefreshBean) ServiceManager.getRobotsTextRefreshService().constructRobotsTextRefresh(bean);
            robotsTextRefresh = RobotsTextRefreshStub.convertBeanToStub(bean);
            String guid = robotsTextRefresh.getGuid();
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(robotsTextRefresh).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createRobotsTextRefresh(RobotsTextRefreshStub robotsTextRefresh) throws BaseResourceException
    {
        doAuthCheck();

        try {
            RobotsTextRefreshBean bean = convertRobotsTextRefreshStubToBean(robotsTextRefresh);
            String guid = ServiceManager.getRobotsTextRefreshService().createRobotsTextRefresh(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createRobotsTextRefresh(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        doAuthCheck();

        try {
            RobotsTextRefreshBean bean = convertFormParamsToBean(formParams);
            String guid = ServiceManager.getRobotsTextRefreshService().createRobotsTextRefresh(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    // TBD
    @Override
    public Response refreshRobotsTextRefresh(String guid, RobotsTextRefreshStub robotsTextRefresh) throws BaseResourceException
    {
        doAuthCheck();

        try {
            if(robotsTextRefresh == null || !guid.equals(robotsTextRefresh.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from robotsTextRefresh guid = " + robotsTextRefresh.getGuid());
                throw new RequestForbiddenException("Failed to refresh the robotsTextRefresh with guid = " + guid);
            }
            RobotsTextRefreshBean bean = convertRobotsTextRefreshStubToBean(robotsTextRefresh);
            bean = (RobotsTextRefreshBean) ServiceManager.getRobotsTextRefreshService().refreshRobotsTextRefresh(bean);
            if(bean == null) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the robotsTextRefresh with guid = " + guid);
                throw new InternalServerErrorException("Failed to refresh the robotsTextRefresh with guid = " + guid);
            }
            robotsTextRefresh = RobotsTextRefreshStub.convertBeanToStub(bean);
            return Response.ok(robotsTextRefresh).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateRobotsTextRefresh(String guid, RobotsTextRefreshStub robotsTextRefresh) throws BaseResourceException
    {
        doAuthCheck();

        try {
            if(robotsTextRefresh == null || !guid.equals(robotsTextRefresh.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from robotsTextRefresh guid = " + robotsTextRefresh.getGuid());
                throw new RequestForbiddenException("Failed to update the robotsTextRefresh with guid = " + guid);
            }
            RobotsTextRefreshBean bean = convertRobotsTextRefreshStubToBean(robotsTextRefresh);
            boolean suc = ServiceManager.getRobotsTextRefreshService().updateRobotsTextRefresh(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the robotsTextRefresh with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the robotsTextRefresh with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateRobotsTextRefresh(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime)
    {
        doAuthCheck();

        try {
            /*
            boolean suc = ServiceManager.getRobotsTextRefreshService().updateRobotsTextRefresh(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the robotsTextRefresh with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the robotsTextRefresh with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
        //} catch(BadRequestException ex) {
        //    throw new BadRequestRsException(ex, resourceUri);
        //} catch(ResourceNotFoundException ex) {
        //    throw new ResourceNotFoundRsException(ex, resourceUri);
        //} catch(ResourceGoneException ex) {
        //    throw new ResourceGoneRsException(ex, resourceUri);
        //} catch(RequestForbiddenException ex) {
        //    throw new RequestForbiddenRsException(ex, resourceUri);
        //} catch(RequestConflictException ex) {
        //    throw new RequestConflictRsException(ex, resourceUri);
        //} catch(ServiceUnavailableException ex) {
        //    throw new ServiceUnavailableRsException(ex, resourceUri);
        //} catch(InternalServerErrorException ex) {
        //    throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response updateRobotsTextRefresh(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        doAuthCheck();

        try {
            RobotsTextRefreshBean bean = convertFormParamsToBean(formParams);
            boolean suc = ServiceManager.getRobotsTextRefreshService().updateRobotsTextRefresh(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the robotsTextRefresh with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the robotsTextRefresh with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteRobotsTextRefresh(String guid) throws BaseResourceException
    {
        doAuthCheck();

        try {
            boolean suc = ServiceManager.getRobotsTextRefreshService().deleteRobotsTextRefresh(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the robotsTextRefresh with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the robotsTextRefresh with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseResourceException
    {
        doAuthCheck();

        try {
            Long count = ServiceManager.getRobotsTextRefreshService().deleteRobotsTextRefreshes(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    @Override
    public Response createRobotsTextRefreshes(RobotsTextRefreshListStub robotsTextRefreshes) throws BaseResourceException
    {
        doAuthCheck();

        try {
            List<RobotsTextRefreshStub> stubs = robotsTextRefreshes.getList();
            List<RobotsTextRefresh> beans = new ArrayList<RobotsTextRefresh>();
            for(RobotsTextRefreshStub stub : stubs) {
                RobotsTextRefreshBean bean = convertRobotsTextRefreshStubToBean(stub);
                beans.add(bean);
            }
            Integer count = ServiceManager.getRobotsTextRefreshService().createRobotsTextRefreshes(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    public static RobotsTextRefreshBean convertRobotsTextRefreshStubToBean(RobotsTextRefresh stub)
    {
        RobotsTextRefreshBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new RobotsTextRefreshBean();
            bean.setGuid(stub.getGuid());
            bean.setRobotsTextFile(stub.getRobotsTextFile());
            bean.setRefreshInterval(stub.getRefreshInterval());
            bean.setNote(stub.getNote());
            bean.setStatus(stub.getStatus());
            bean.setRefreshStatus(stub.getRefreshStatus());
            bean.setResult(stub.getResult());
            bean.setLastCheckedTime(stub.getLastCheckedTime());
            bean.setNextCheckedTime(stub.getNextCheckedTime());
            bean.setExpirationTime(stub.getExpirationTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<RobotsTextRefreshBean> convertRobotsTextRefreshListStubToBeanList(RobotsTextRefreshListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<RobotsTextRefreshStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<RobotsTextRefreshBean> beanList = new ArrayList<RobotsTextRefreshBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(RobotsTextRefreshStub stub : stubList) {
                    RobotsTextRefreshBean bean = convertRobotsTextRefreshStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }


    // TBD
    // This needs to be implemented before url-encoded form create/update can be supported 
    public static RobotsTextRefreshBean convertFormParamsToBean(MultivaluedMap<String, String> formParams)
    {
        RobotsTextRefreshBean bean = new RobotsTextRefreshBean();
        if(formParams == null) {
            log.log(Level.INFO, "FormParams is null. Empty bean is returned.");
        } else {
            Iterator<MultivaluedMap.Entry<String,List<String>>> it = formParams.entrySet().iterator();
            while(it.hasNext()) {
                MultivaluedMap.Entry<String,List<String>> m =(MultivaluedMap.Entry<String,List<String>>) it.next();
                String key = (String) m.getKey();
                String val = null;
                List<String> list = m.getValue();
                if(list != null && list.size() > 0) {
                    val = list.get(0);
                    if(key.equals("guid")) {
                        bean.setGuid(val);                        
                    } else if(key.equals("robotsTextFile")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setRobotsTextFile(v);
                    } else if(key.equals("refreshInterval")) {
                        // TBD: Needs to use "parse()" methods.
                        //Integer v = (Integer) val;
                        //bean.setRefreshInterval(v);
                    } else if(key.equals("note")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setNote(v);
                    } else if(key.equals("status")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setStatus(v);
                    } else if(key.equals("refreshStatus")) {
                        // TBD: Needs to use "parse()" methods.
                        //Integer v = (Integer) val;
                        //bean.setRefreshStatus(v);
                    } else if(key.equals("result")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setResult(v);
                    } else if(key.equals("lastCheckedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setLastCheckedTime(v);
                    } else if(key.equals("nextCheckedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setNextCheckedTime(v);
                    } else if(key.equals("expirationTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setExpirationTime(v);
                    } else if(key.equals("createdTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setCreatedTime(v);
                    } else if(key.equals("modifiedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setModifiedTime(v);
                    }
                }
            }
        }
        return bean;
    }

}
