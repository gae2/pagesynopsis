package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.fe.bean.RobotsTextGroupJsBean;


public class RobotsTextGroupWebUtil
{
    private static final Logger log = Logger.getLogger(RobotsTextGroupWebUtil.class.getName());

    // Static methods only.
    private RobotsTextGroupWebUtil() {}
    

    public static RobotsTextGroupJsBean convertRobotsTextGroupToJsBean(RobotsTextGroup robotsTextGroup)
    {
        RobotsTextGroupJsBean jsBean = null;
        if(robotsTextGroup != null) {
            jsBean = new RobotsTextGroupJsBean();
            jsBean.setUuid(robotsTextGroup.getUuid());
            jsBean.setUserAgent(robotsTextGroup.getUserAgent());
            jsBean.setAllows(robotsTextGroup.getAllows());
            jsBean.setDisallows(robotsTextGroup.getDisallows());
        }
        return jsBean;
    }

    public static RobotsTextGroup convertRobotsTextGroupJsBeanToBean(RobotsTextGroupJsBean jsBean)
    {
        RobotsTextGroupBean robotsTextGroup = null;
        if(jsBean != null) {
            robotsTextGroup = new RobotsTextGroupBean();
            robotsTextGroup.setUuid(jsBean.getUuid());
            robotsTextGroup.setUserAgent(jsBean.getUserAgent());
            robotsTextGroup.setAllows(jsBean.getAllows());
            robotsTextGroup.setDisallows(jsBean.getDisallows());
        }
        return robotsTextGroup;
    }

}
