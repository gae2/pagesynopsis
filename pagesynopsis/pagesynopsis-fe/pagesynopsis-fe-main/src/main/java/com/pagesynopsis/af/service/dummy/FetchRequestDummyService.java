package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.FetchRequestService;


// The primary purpose of FetchRequestDummyService is to fake the service api, FetchRequestService.
// It has no real implementation.
public class FetchRequestDummyService implements FetchRequestService
{
    private static final Logger log = Logger.getLogger(FetchRequestDummyService.class.getName());

    public FetchRequestDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // FetchRequest related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFetchRequest(): guid = " + guid);
        return null;
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFetchRequest(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        log.fine("getFetchRequests()");
        return null;
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return getAllFetchRequests(null, null, null);
    }


    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequests(ordering, offset, count, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFetchRequests(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFetchRequestKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FetchRequestDummyService.findFetchRequests(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FetchRequestDummyService.findFetchRequestKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FetchRequestDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createFetchRequest(String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        log.finer("createFetchRequest()");
        return null;
    }

    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("createFetchRequest()");
        return null;
    }

    @Override
    public FetchRequest constructFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("constructFetchRequest()");
        return null;
    }

    @Override
    public Boolean updateFetchRequest(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        log.finer("updateFetchRequest()");
        return null;
    }
        
    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("updateFetchRequest()");
        return null;
    }

    @Override
    public FetchRequest refreshFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("refreshFetchRequest()");
        return null;
    }

    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteFetchRequest(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("deleteFetchRequest()");
        return null;
    }

    // TBD
    @Override
    public Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteFetchRequest(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createFetchRequests(List<FetchRequest> fetchRequests) throws BaseException
    {
        log.finer("createFetchRequests()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateFetchRequests(List<FetchRequest> fetchRequests) throws BaseException
    //{
    //}

}
