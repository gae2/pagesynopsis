package com.pagesynopsis.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.fe.Validateable;
import com.pagesynopsis.fe.core.StringEscapeUtil;
import com.pagesynopsis.fe.bean.RobotsTextGroupJsBean;
import com.pagesynopsis.fe.bean.RobotsTextFileJsBean;


// Place holder...
public class RobotsTextFileFormBean extends RobotsTextFileJsBean implements Serializable, Cloneable, Validateable  //, RobotsTextFile
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextFileFormBean.class.getName());

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public RobotsTextFileFormBean()
    {
        super();
    }
    public RobotsTextFileFormBean(String guid)
    {
       super(guid);
    }
    public RobotsTextFileFormBean(String guid, String siteUrl, List<RobotsTextGroupJsBean> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime)
    {
        super(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
    }
    public RobotsTextFileFormBean(String guid, String siteUrl, List<RobotsTextGroupJsBean> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);
    }
    public RobotsTextFileFormBean(RobotsTextFileJsBean bean)
    {
        super(bean);
    }

    public static RobotsTextFileFormBean fromJsonString(String jsonStr)
    {
        RobotsTextFileFormBean bean = null;
        try {
            // TBD:
            ObjectMapper mapper = new ObjectMapper();   // can reuse, share globally
            bean = mapper.readValue(jsonStr, RobotsTextFileFormBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getGuid() == null) {
//            addError("guid", "guid is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSiteUrl() == null) {
//            addError("siteUrl", "siteUrl is null");
//            allOK = false;
//        }
//        // TBD
//        if(getGroups() == null) {
//            addError("groups", "groups is null");
//            allOK = false;
//        }
//        // TBD
//        if(getSitemaps() == null) {
//            addError("sitemaps", "sitemaps is null");
//            allOK = false;
//        }
//        // TBD
//        if(getContent() == null) {
//            addError("content", "content is null");
//            allOK = false;
//        }
//        // TBD
//        if(getContentHash() == null) {
//            addError("contentHash", "contentHash is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLastCheckedTime() == null) {
//            addError("lastCheckedTime", "lastCheckedTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getLastUpdatedTime() == null) {
//            addError("lastUpdatedTime", "lastUpdatedTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getCreatedTime() == null) {
//            addError("createdTime", "createdTime is null");
//            allOK = false;
//        }
//        // TBD
//        if(getModifiedTime() == null) {
//            addError("modifiedTime", "modifiedTime is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        RobotsTextFileFormBean cloned = new RobotsTextFileFormBean((RobotsTextFileJsBean) super.clone());
        return cloned;
    }

}
