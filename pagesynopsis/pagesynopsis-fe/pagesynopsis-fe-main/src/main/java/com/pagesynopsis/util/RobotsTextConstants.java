package com.pagesynopsis.util;


// TBD
public class RobotsTextConstants
{
    private RobotsTextConstants() {}

    // If the age is older than this, fetch new one...
    // One day is allowed value for robots.txt.
    // We can go a bit longer..
    // This value should be longer than cron refresh interval....
    public static final long MAX_AGE_MILLIS = 2 * 24 * 3600 * 1000L;
    
    // Any robot.txt will be refreshed through cron since the last checked time, which is later than MAX_AGE_MILLIS,
    // until EXPIRATION_TIME_MILLIS - MAX_AGE_MILLIS
    // 10 days for now...
    public static final long EXPIRATION_TIME_MILLIS = 10 * 24 * 3600 * 1000L;

    // Cron interval. Should be < MAX_AGE_MILLIS
    // Note this is seconds, not in milli seconds...
    public static final int REFRESH_INTERVAL = 12 * 3600;

    // Etc...
    

}
