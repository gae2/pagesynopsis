package com.pagesynopsis.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.ImageSetBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ImageSetService;
import com.pagesynopsis.af.service.impl.ImageSetServiceImpl;
import com.pagesynopsis.app.fetch.FetchProcessor;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ImageSet;


// Updated.
public class ImageSetAppService extends ImageSetServiceImpl implements ImageSetService
{
    private static final Logger log = Logger.getLogger(ImageSetAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public ImageSetAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // ImageSet related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public ImageSet getImageSet(String guid) throws BaseException
    {
        return super.getImageSet(guid);
    }

    @Override
    public Object getImageSet(String guid, String field) throws BaseException
    {
        return super.getImageSet(guid, field);
    }

    @Override
    public List<ImageSet> getImageSets(List<String> guids) throws BaseException
    {
        return super.getImageSets(guids);
    }

    @Override
    public List<ImageSet> getAllImageSets() throws BaseException
    {
        return super.getAllImageSets();
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllImageSetKeys(ordering, offset, count);
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findImageSets(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("TOP: createImageSet()");

        // TBD:
        imageSet = validateImageSet(imageSet);
        imageSet = enhanceImageSet(imageSet);
        // ...
        
        // ????
        Integer fefreshStatus = ((ImageSetBean) imageSet).getRefreshStatus();
        if(fefreshStatus == null) {
            ((ImageSetBean) imageSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);  // ???
        }
        // ...

        // Note:
        // POST/PUT should not call processImageSet().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = imageSet.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                imageSet = processImageSet(imageSet);
                if(imageSet != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: imageSet = " + imageSet);
                    // Need to to do this to avoid double processing.
                    ((ImageSetBean) imageSet).setDeferred(true);
                    // ((ImageSetBean) imageSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((ImageSetBean) imageSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...


        // Save it.
        String guid = super.createImageSet(imageSet);

        log.finer("BOTTOM: createImageSet(): guid = " + guid);
        return guid;
    }

    @Override
    public ImageSet constructImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("TOP: constructImageSet()");

        // TBD:
        imageSet = validateImageSet(imageSet);
        imageSet = enhanceImageSet(imageSet);
        // ...
        
        // ????
        Integer fefreshStatus = ((ImageSetBean) imageSet).getRefreshStatus();
        if(fefreshStatus == null) {
            ((ImageSetBean) imageSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);   // ????
        }
        // ...
        
        
        // Note:
        // POST/PUT should not call processImageSet().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = imageSet.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                imageSet = processImageSet(imageSet);
                if(imageSet != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: imageSet = " + imageSet);
                    // Need to to do this to avoid double processing.
                    ((ImageSetBean) imageSet).setDeferred(true);
                    // ((ImageSetBean) imageSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((ImageSetBean) imageSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...


        // Update it.
        imageSet = super.constructImageSet(imageSet);
        
        log.finer("BOTTOM: constructImageSet(): imageSet = " + imageSet);
        return imageSet;
    }


    @Override
    public Boolean updateImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("TOP: updateImageSet()");

        // TBD:
        imageSet = validateImageSet(imageSet);
        Boolean suc = super.updateImageSet(imageSet);

        log.finer("BOTTOM: createImageSet(): suc = " + suc);
        return suc;
    }
        
    @Override
    public ImageSet refreshImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("TOP: refreshImageSet()");

        // TBD:
        imageSet = validateImageSet(imageSet);
        imageSet = super.refreshImageSet(imageSet);
        
        log.finer("BOTTOM: refreshImageSet(): imageSet = " + imageSet);
        return imageSet;
    }

    
    @Override
    public Boolean deleteImageSet(String guid) throws BaseException
    {
        return super.deleteImageSet(guid);
    }

    @Override
    public Boolean deleteImageSet(ImageSet imageSet) throws BaseException
    {
        return super.deleteImageSet(imageSet);
    }

    @Override
    public Integer createImageSets(List<ImageSet> imageSets) throws BaseException
    {
        return super.createImageSets(imageSets);
    }

    // TBD
    //@Override
    //public Boolean updateImageSets(List<ImageSet> imageSets) throws BaseException
    //{
    //}

    
    
    // TBD...
    private ImageSetBean validateImageSet(ImageSet imageSet) throws BaseException
    {
        ImageSetBean bean = null;
        if(imageSet instanceof ImageSetBean) {
            bean = (ImageSetBean) imageSet;
        }
        // else ???
        
        // ...
        String targetUrl =  imageSet.getTargetUrl();
        if(targetUrl == null || targetUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BaseException("Target Url is null or empty. Cannot proceed with fetch.");
        } else if(targetUrl.length() > 500) {
            // ??? Truncate or error?
            // TBD ....
            throw new BaseException("Target Url is too long. Cannot proceed with fetch. targetUrl = " + targetUrl);            
        }
        
        // TBD:
        // If we truncate targetUrl...
        // We need to check pageUrl as well....
        // ...         
        
        
        
        // TBD: 
        String pageUrl = imageSet.getPageUrl();
        String query = imageSet.getQueryString();
        List<KeyValuePairStruct> queryParams = imageSet.getQueryParams();
        // if either of these is null/empty
        // populate them based on targetUrl...
        // --> This should have done earlier (closer to front end) ???
        // ...
        
        
//        // Next, check input...
//        List<String> input = imageSet.getInput();
//        if(input == null || input.isEmpty()) {
//            // ???  --> Note that the request will not even be saved ....
//            throw new BaseException("Input is null or empty. Cannot proceed with fetch.");
//        }

        
        
        // TBD:
        // Minimum value for refresh interval???
        // etc....
        
        // ...

        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private ImageSet enhanceImageSet(ImageSet imageSet) throws BaseException
    {        
        // TBD:
        // Create shortlink, etc...
        // ....
        return imageSet;
    }        

    private ImageSet processImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("TOP: processImageSet()");
        // ...
        imageSet = FetchProcessor.getInstance().processImageFetch(imageSet);
        // ...   

        log.finer("BOTTOM: processImageSet()");
        return imageSet;
    }


    
    
}
