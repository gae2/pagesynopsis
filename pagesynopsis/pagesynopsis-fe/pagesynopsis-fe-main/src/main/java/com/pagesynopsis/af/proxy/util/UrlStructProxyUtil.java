package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.UrlStruct;
// import com.pagesynopsis.ws.bean.UrlStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;


public class UrlStructProxyUtil
{
    private static final Logger log = Logger.getLogger(UrlStructProxyUtil.class.getName());

    // Static methods only.
    private UrlStructProxyUtil() {}

    public static UrlStructBean convertServerUrlStructBeanToAppBean(UrlStruct serverBean)
    {
        UrlStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new UrlStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setStatusCode(serverBean.getStatusCode());
            bean.setRedirectUrl(serverBean.getRedirectUrl());
            bean.setAbsoluteUrl(serverBean.getAbsoluteUrl());
            bean.setHashFragment(serverBean.getHashFragment());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.UrlStructBean convertAppUrlStructBeanToServerBean(UrlStruct appBean)
    {
        com.pagesynopsis.ws.bean.UrlStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.UrlStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setStatusCode(appBean.getStatusCode());
            bean.setRedirectUrl(appBean.getRedirectUrl());
            bean.setAbsoluteUrl(appBean.getAbsoluteUrl());
            bean.setHashFragment(appBean.getHashFragment());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
