package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.VideoSetService;


// The primary purpose of VideoSetDummyService is to fake the service api, VideoSetService.
// It has no real implementation.
public class VideoSetDummyService implements VideoSetService
{
    private static final Logger log = Logger.getLogger(VideoSetDummyService.class.getName());

    public VideoSetDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // VideoSet related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public VideoSet getVideoSet(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getVideoSet(): guid = " + guid);
        return null;
    }

    @Override
    public Object getVideoSet(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getVideoSet(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<VideoSet> getVideoSets(List<String> guids) throws BaseException
    {
        log.fine("getVideoSets()");
        return null;
    }

    @Override
    public List<VideoSet> getAllVideoSets() throws BaseException
    {
        return getAllVideoSets(null, null, null);
    }


    @Override
    public List<VideoSet> getAllVideoSets(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllVideoSets(ordering, offset, count, null);
    }

    @Override
    public List<VideoSet> getAllVideoSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllVideoSets(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllVideoSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllVideoSetKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findVideoSets(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findVideoSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("VideoSetDummyService.findVideoSets(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("VideoSetDummyService.findVideoSetKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("VideoSetDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createVideoSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<VideoStruct> pageVideos) throws BaseException
    {
        log.finer("createVideoSet()");
        return null;
    }

    @Override
    public String createVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("createVideoSet()");
        return null;
    }

    @Override
    public VideoSet constructVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("constructVideoSet()");
        return null;
    }

    @Override
    public Boolean updateVideoSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<VideoStruct> pageVideos) throws BaseException
    {
        log.finer("updateVideoSet()");
        return null;
    }
        
    @Override
    public Boolean updateVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("updateVideoSet()");
        return null;
    }

    @Override
    public VideoSet refreshVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("refreshVideoSet()");
        return null;
    }

    @Override
    public Boolean deleteVideoSet(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteVideoSet(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("deleteVideoSet()");
        return null;
    }

    // TBD
    @Override
    public Long deleteVideoSets(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteVideoSet(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createVideoSets(List<VideoSet> videoSets) throws BaseException
    {
        log.finer("createVideoSets()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateVideoSets(List<VideoSet> videoSets) throws BaseException
    //{
    //}

}
