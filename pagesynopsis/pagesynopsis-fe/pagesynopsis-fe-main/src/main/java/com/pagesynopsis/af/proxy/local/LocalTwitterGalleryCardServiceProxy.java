package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterGalleryCard;
// import com.pagesynopsis.ws.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.proxy.TwitterGalleryCardServiceProxy;


public class LocalTwitterGalleryCardServiceProxy extends BaseLocalServiceProxy implements TwitterGalleryCardServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterGalleryCardServiceProxy.class.getName());

    public LocalTwitterGalleryCardServiceProxy()
    {
    }

    @Override
    public TwitterGalleryCard getTwitterGalleryCard(String guid) throws BaseException
    {
        TwitterGalleryCard serverBean = getTwitterGalleryCardService().getTwitterGalleryCard(guid);
        TwitterGalleryCard appBean = convertServerTwitterGalleryCardBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getTwitterGalleryCard(String guid, String field) throws BaseException
    {
        return getTwitterGalleryCardService().getTwitterGalleryCard(guid, field);       
    }

    @Override
    public List<TwitterGalleryCard> getTwitterGalleryCards(List<String> guids) throws BaseException
    {
        List<TwitterGalleryCard> serverBeanList = getTwitterGalleryCardService().getTwitterGalleryCards(guids);
        List<TwitterGalleryCard> appBeanList = convertServerTwitterGalleryCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards() throws BaseException
    {
        return getAllTwitterGalleryCards(null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterGalleryCardService().getAllTwitterGalleryCards(ordering, offset, count);
        return getAllTwitterGalleryCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterGalleryCard> serverBeanList = getTwitterGalleryCardService().getAllTwitterGalleryCards(ordering, offset, count, forwardCursor);
        List<TwitterGalleryCard> appBeanList = convertServerTwitterGalleryCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterGalleryCardService().getAllTwitterGalleryCardKeys(ordering, offset, count);
        return getAllTwitterGalleryCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterGalleryCardService().getAllTwitterGalleryCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterGalleryCardService().findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterGalleryCard> serverBeanList = getTwitterGalleryCardService().findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<TwitterGalleryCard> appBeanList = convertServerTwitterGalleryCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterGalleryCardService().findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterGalleryCardService().findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterGalleryCardService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterGalleryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        return getTwitterGalleryCardService().createTwitterGalleryCard(card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
    }

    @Override
    public String createTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterGalleryCardBean serverBean =  convertAppTwitterGalleryCardBeanToServerBean(twitterGalleryCard);
        return getTwitterGalleryCardService().createTwitterGalleryCard(serverBean);
    }

    @Override
    public Boolean updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        return getTwitterGalleryCardService().updateTwitterGalleryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
    }

    @Override
    public Boolean updateTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterGalleryCardBean serverBean =  convertAppTwitterGalleryCardBeanToServerBean(twitterGalleryCard);
        return getTwitterGalleryCardService().updateTwitterGalleryCard(serverBean);
    }

    @Override
    public Boolean deleteTwitterGalleryCard(String guid) throws BaseException
    {
        return getTwitterGalleryCardService().deleteTwitterGalleryCard(guid);
    }

    @Override
    public Boolean deleteTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterGalleryCardBean serverBean =  convertAppTwitterGalleryCardBeanToServerBean(twitterGalleryCard);
        return getTwitterGalleryCardService().deleteTwitterGalleryCard(serverBean);
    }

    @Override
    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterGalleryCardService().deleteTwitterGalleryCards(filter, params, values);
    }




    public static TwitterGalleryCardBean convertServerTwitterGalleryCardBeanToAppBean(TwitterGalleryCard serverBean)
    {
        TwitterGalleryCardBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new TwitterGalleryCardBean();
            bean.setGuid(serverBean.getGuid());
            bean.setCard(serverBean.getCard());
            bean.setUrl(serverBean.getUrl());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setSite(serverBean.getSite());
            bean.setSiteId(serverBean.getSiteId());
            bean.setCreator(serverBean.getCreator());
            bean.setCreatorId(serverBean.getCreatorId());
            bean.setImage0(serverBean.getImage0());
            bean.setImage1(serverBean.getImage1());
            bean.setImage2(serverBean.getImage2());
            bean.setImage3(serverBean.getImage3());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterGalleryCard> convertServerTwitterGalleryCardBeanListToAppBeanList(List<TwitterGalleryCard> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterGalleryCard> beanList = new ArrayList<TwitterGalleryCard>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(TwitterGalleryCard sb : serverBeanList) {
                TwitterGalleryCardBean bean = convertServerTwitterGalleryCardBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.TwitterGalleryCardBean convertAppTwitterGalleryCardBeanToServerBean(TwitterGalleryCard appBean)
    {
        com.pagesynopsis.ws.bean.TwitterGalleryCardBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.TwitterGalleryCardBean();
            bean.setGuid(appBean.getGuid());
            bean.setCard(appBean.getCard());
            bean.setUrl(appBean.getUrl());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setSite(appBean.getSite());
            bean.setSiteId(appBean.getSiteId());
            bean.setCreator(appBean.getCreator());
            bean.setCreatorId(appBean.getCreatorId());
            bean.setImage0(appBean.getImage0());
            bean.setImage1(appBean.getImage1());
            bean.setImage2(appBean.getImage2());
            bean.setImage3(appBean.getImage3());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterGalleryCard> convertAppTwitterGalleryCardBeanListToServerBeanList(List<TwitterGalleryCard> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterGalleryCard> beanList = new ArrayList<TwitterGalleryCard>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(TwitterGalleryCard sb : appBeanList) {
                com.pagesynopsis.ws.bean.TwitterGalleryCardBean bean = convertAppTwitterGalleryCardBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
