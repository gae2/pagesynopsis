package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
// import com.pagesynopsis.ws.bean.FetchRequestBean;
import com.pagesynopsis.ws.service.FetchRequestService;
import com.pagesynopsis.af.bean.FetchRequestBean;
import com.pagesynopsis.af.proxy.FetchRequestServiceProxy;
import com.pagesynopsis.af.proxy.util.ReferrerInfoStructProxyUtil;
import com.pagesynopsis.af.proxy.util.NotificationStructProxyUtil;


public class LocalFetchRequestServiceProxy extends BaseLocalServiceProxy implements FetchRequestServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalFetchRequestServiceProxy.class.getName());

    public LocalFetchRequestServiceProxy()
    {
    }

    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        FetchRequest serverBean = getFetchRequestService().getFetchRequest(guid);
        FetchRequest appBean = convertServerFetchRequestBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        return getFetchRequestService().getFetchRequest(guid, field);       
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        List<FetchRequest> serverBeanList = getFetchRequestService().getFetchRequests(guids);
        List<FetchRequest> appBeanList = convertServerFetchRequestBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return getAllFetchRequests(null, null, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().getAllFetchRequests(ordering, offset, count);
        return getAllFetchRequests(ordering, offset, count, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<FetchRequest> serverBeanList = getFetchRequestService().getAllFetchRequests(ordering, offset, count, forwardCursor);
        List<FetchRequest> appBeanList = convertServerFetchRequestBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().getAllFetchRequestKeys(ordering, offset, count);
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFetchRequestService().getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count);
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<FetchRequest> serverBeanList = getFetchRequestService().findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<FetchRequest> appBeanList = convertServerFetchRequestBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFetchRequestService().findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getFetchRequestService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFetchRequest(String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        return getFetchRequestService().createFetchRequest(user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfo, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPref);
    }

    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        com.pagesynopsis.ws.bean.FetchRequestBean serverBean =  convertAppFetchRequestBeanToServerBean(fetchRequest);
        return getFetchRequestService().createFetchRequest(serverBean);
    }

    @Override
    public Boolean updateFetchRequest(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        return getFetchRequestService().updateFetchRequest(guid, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfo, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPref);
    }

    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        com.pagesynopsis.ws.bean.FetchRequestBean serverBean =  convertAppFetchRequestBeanToServerBean(fetchRequest);
        return getFetchRequestService().updateFetchRequest(serverBean);
    }

    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        return getFetchRequestService().deleteFetchRequest(guid);
    }

    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        com.pagesynopsis.ws.bean.FetchRequestBean serverBean =  convertAppFetchRequestBeanToServerBean(fetchRequest);
        return getFetchRequestService().deleteFetchRequest(serverBean);
    }

    @Override
    public Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException
    {
        return getFetchRequestService().deleteFetchRequests(filter, params, values);
    }




    public static FetchRequestBean convertServerFetchRequestBeanToAppBean(FetchRequest serverBean)
    {
        FetchRequestBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new FetchRequestBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setTargetUrl(serverBean.getTargetUrl());
            bean.setPageUrl(serverBean.getPageUrl());
            bean.setQueryString(serverBean.getQueryString());
            bean.setQueryParams(serverBean.getQueryParams());
            bean.setVersion(serverBean.getVersion());
            bean.setFetchObject(serverBean.getFetchObject());
            bean.setFetchStatus(serverBean.getFetchStatus());
            bean.setResult(serverBean.getResult());
            bean.setNote(serverBean.getNote());
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertServerReferrerInfoStructBeanToAppBean(serverBean.getReferrerInfo()));
            bean.setStatus(serverBean.getStatus());
            bean.setNextPageUrl(serverBean.getNextPageUrl());
            bean.setFollowPages(serverBean.getFollowPages());
            bean.setFollowDepth(serverBean.getFollowDepth());
            bean.setCreateVersion(serverBean.isCreateVersion());
            bean.setDeferred(serverBean.isDeferred());
            bean.setAlert(serverBean.isAlert());
            bean.setNotificationPref(NotificationStructProxyUtil.convertServerNotificationStructBeanToAppBean(serverBean.getNotificationPref()));
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<FetchRequest> convertServerFetchRequestBeanListToAppBeanList(List<FetchRequest> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<FetchRequest> beanList = new ArrayList<FetchRequest>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(FetchRequest sb : serverBeanList) {
                FetchRequestBean bean = convertServerFetchRequestBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.FetchRequestBean convertAppFetchRequestBeanToServerBean(FetchRequest appBean)
    {
        com.pagesynopsis.ws.bean.FetchRequestBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.FetchRequestBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setTargetUrl(appBean.getTargetUrl());
            bean.setPageUrl(appBean.getPageUrl());
            bean.setQueryString(appBean.getQueryString());
            bean.setQueryParams(appBean.getQueryParams());
            bean.setVersion(appBean.getVersion());
            bean.setFetchObject(appBean.getFetchObject());
            bean.setFetchStatus(appBean.getFetchStatus());
            bean.setResult(appBean.getResult());
            bean.setNote(appBean.getNote());
            bean.setReferrerInfo(ReferrerInfoStructProxyUtil.convertAppReferrerInfoStructBeanToServerBean(appBean.getReferrerInfo()));
            bean.setStatus(appBean.getStatus());
            bean.setNextPageUrl(appBean.getNextPageUrl());
            bean.setFollowPages(appBean.getFollowPages());
            bean.setFollowDepth(appBean.getFollowDepth());
            bean.setCreateVersion(appBean.isCreateVersion());
            bean.setDeferred(appBean.isDeferred());
            bean.setAlert(appBean.isAlert());
            bean.setNotificationPref(NotificationStructProxyUtil.convertAppNotificationStructBeanToServerBean(appBean.getNotificationPref()));
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<FetchRequest> convertAppFetchRequestBeanListToServerBeanList(List<FetchRequest> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<FetchRequest> beanList = new ArrayList<FetchRequest>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(FetchRequest sb : appBeanList) {
                com.pagesynopsis.ws.bean.FetchRequestBean bean = convertAppFetchRequestBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
