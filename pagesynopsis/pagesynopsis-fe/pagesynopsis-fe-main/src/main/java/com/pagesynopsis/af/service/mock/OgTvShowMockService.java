package com.pagesynopsis.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgTvShowService;


// OgTvShowMockService is a decorator.
// It can be used as a base class to mock OgTvShowService objects.
public abstract class OgTvShowMockService implements OgTvShowService
{
    private static final Logger log = Logger.getLogger(OgTvShowMockService.class.getName());

    // OgTvShowMockService uses the decorator design pattern.
    private OgTvShowService decoratedService;

    public OgTvShowMockService(OgTvShowService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected OgTvShowService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(OgTvShowService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // OgTvShow related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgTvShow getOgTvShow(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgTvShow(): guid = " + guid);
        OgTvShow bean = decoratedService.getOgTvShow(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgTvShow(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getOgTvShow(guid, field);
        return obj;
    }

    @Override
    public List<OgTvShow> getOgTvShows(List<String> guids) throws BaseException
    {
        log.fine("getOgTvShows()");
        List<OgTvShow> ogTvShows = decoratedService.getOgTvShows(guids);
        log.finer("END");
        return ogTvShows;
    }

    @Override
    public List<OgTvShow> getAllOgTvShows() throws BaseException
    {
        return getAllOgTvShows(null, null, null);
    }


    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShows(ordering, offset, count, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgTvShows(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<OgTvShow> ogTvShows = decoratedService.getAllOgTvShows(ordering, offset, count, forwardCursor);
        log.finer("END");
        return ogTvShows;
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShowKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgTvShowKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllOgTvShowKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvShowMockService.findOgTvShows(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<OgTvShow> ogTvShows = decoratedService.findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return ogTvShows;
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvShowMockService.findOgTvShowKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvShowMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgTvShow(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        OgTvShowBean bean = new OgTvShowBean(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return createOgTvShow(bean);
    }

    @Override
    public String createOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createOgTvShow(ogTvShow);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public OgTvShow constructOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("BEGIN");
        OgTvShow bean = decoratedService.constructOgTvShow(ogTvShow);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateOgTvShow(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgTvShowBean bean = new OgTvShowBean(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return updateOgTvShow(bean);
    }
        
    @Override
    public Boolean updateOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateOgTvShow(ogTvShow);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public OgTvShow refreshOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("BEGIN");
        OgTvShow bean = decoratedService.refreshOgTvShow(ogTvShow);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteOgTvShow(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgTvShow(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgTvShow(ogTvShow);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOgTvShows(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteOgTvShows(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgTvShows(List<OgTvShow> ogTvShows) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createOgTvShows(ogTvShows);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateOgTvShows(List<OgTvShow> ogTvShows) throws BaseException
    //{
    //}

}
