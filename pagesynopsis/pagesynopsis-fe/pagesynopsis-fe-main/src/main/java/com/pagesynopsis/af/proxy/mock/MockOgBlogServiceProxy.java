package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.af.proxy.OgBlogServiceProxy;


// MockOgBlogServiceProxy is a decorator.
// It can be used as a base class to mock OgBlogServiceProxy objects.
public abstract class MockOgBlogServiceProxy implements OgBlogServiceProxy
{
    private static final Logger log = Logger.getLogger(MockOgBlogServiceProxy.class.getName());

    // MockOgBlogServiceProxy uses the decorator design pattern.
    private OgBlogServiceProxy decoratedProxy;

    public MockOgBlogServiceProxy(OgBlogServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected OgBlogServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(OgBlogServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public OgBlog getOgBlog(String guid) throws BaseException
    {
        return decoratedProxy.getOgBlog(guid);
    }

    @Override
    public Object getOgBlog(String guid, String field) throws BaseException
    {
        return decoratedProxy.getOgBlog(guid, field);       
    }

    @Override
    public List<OgBlog> getOgBlogs(List<String> guids) throws BaseException
    {
        return decoratedProxy.getOgBlogs(guids);
    }

    @Override
    public List<OgBlog> getAllOgBlogs() throws BaseException
    {
        return getAllOgBlogs(null, null, null);
    }

    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgBlogs(ordering, offset, count);
        return getAllOgBlogs(ordering, offset, count, null);
    }

    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgBlogs(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgBlogKeys(ordering, offset, count);
        return getAllOgBlogKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgBlogKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgBlogs(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgBlog(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return decoratedProxy.createOgBlog(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public String createOgBlog(OgBlog ogBlog) throws BaseException
    {
        return decoratedProxy.createOgBlog(ogBlog);
    }

    @Override
    public Boolean updateOgBlog(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return decoratedProxy.updateOgBlog(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public Boolean updateOgBlog(OgBlog ogBlog) throws BaseException
    {
        return decoratedProxy.updateOgBlog(ogBlog);
    }

    @Override
    public Boolean deleteOgBlog(String guid) throws BaseException
    {
        return decoratedProxy.deleteOgBlog(guid);
    }

    @Override
    public Boolean deleteOgBlog(OgBlog ogBlog) throws BaseException
    {
        String guid = ogBlog.getGuid();
        return deleteOgBlog(guid);
    }

    @Override
    public Long deleteOgBlogs(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteOgBlogs(filter, params, values);
    }

}
