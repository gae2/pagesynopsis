package com.pagesynopsis.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.AudioSetJsBean;
import com.pagesynopsis.wa.service.AudioSetWebService;
import com.pagesynopsis.wa.service.UserWebService;


public class AudioSetHelper
{
    private static final Logger log = Logger.getLogger(AudioSetHelper.class.getName());

    // Arbitrary...
    private static final long MAX_AGE_MILLIS = 3600 * 1000L * 24 * 30;   // ~1 month

    private AudioSetHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private AudioSetWebService fetchRequestWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private AudioSetWebService getAudioSetService()
    {
        if(fetchRequestWebService == null) {
            fetchRequestWebService = new AudioSetWebService();
        }
        return fetchRequestWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class AudioSetHelperHolder
    {
        private static final AudioSetHelper INSTANCE = new AudioSetHelper();
    }

    // Singleton method
    public static AudioSetHelper getInstance()
    {
        return AudioSetHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public AudioSetJsBean getAudioSet(String guid) 
    {
        AudioSetJsBean bean = null;
        try {
            bean = getAudioSetService().getAudioSet(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }
    
    public AudioSetJsBean getAudioSetByTargetUrl(String targetUrl) 
    {
        // Long cutoff = System.currentTimeMillis() - MAX_AGE_MILLIS;
        Long cutoff = null;
        return getAudioSetByTargetUrl(targetUrl, cutoff);
    }
    public AudioSetJsBean getAudioSetByTargetUrl(String targetUrl, Long timeCutoff) 
    {
        return getAudioSetByTargetUrl(targetUrl, timeCutoff, false);  // This should really be true.. ????? 
    }
    private AudioSetJsBean getAudioSetByTargetUrl(String targetUrl, Long timeCutoff, boolean refreshingRecordOnly) 
    {
        AudioSetJsBean bean = null;
        try {
            String filter = "targetUrl=='" + targetUrl + "'";
            if(timeCutoff != null && timeCutoff > 0L) {
                filter += " && createdTime >= " + timeCutoff;
            }
            // TBD: This requires new datastore index....
            if(refreshingRecordOnly == true) {
                // We should not return stale record...
                filter += " && refreshStatus != " + RefreshStatus.STATUS_NOREFRESH;
            }
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<AudioSetJsBean> beans = getAudioSetService().findAudioSets(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
                String guid = bean.getGuid();
                try {
                    bean = getAudioSetService().getAudioSet(guid);
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
                }
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for targetUrl = " + targetUrl, e);
        }
        return bean;
    }
    

    public List<String> getAudioSetKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getAudioSetService().findAudioSetKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    // TBD: 
    public List<AudioSetJsBean> getAudioSetsForRefreshProcessing(Integer maxCount)
    {
        List<AudioSetJsBean> audioSets = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            audioSets = getAudioSetService().findAudioSets(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return audioSets;
    }


}
