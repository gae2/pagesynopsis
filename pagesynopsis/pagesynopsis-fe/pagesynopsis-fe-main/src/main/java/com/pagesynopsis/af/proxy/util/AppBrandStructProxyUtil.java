package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.AppBrandStruct;
// import com.pagesynopsis.ws.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;


public class AppBrandStructProxyUtil
{
    private static final Logger log = Logger.getLogger(AppBrandStructProxyUtil.class.getName());

    // Static methods only.
    private AppBrandStructProxyUtil() {}

    public static AppBrandStructBean convertServerAppBrandStructBeanToAppBean(AppBrandStruct serverBean)
    {
        AppBrandStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new AppBrandStructBean();
            bean.setBrand(serverBean.getBrand());
            bean.setName(serverBean.getName());
            bean.setDescription(serverBean.getDescription());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.AppBrandStructBean convertAppAppBrandStructBeanToServerBean(AppBrandStruct appBean)
    {
        com.pagesynopsis.ws.bean.AppBrandStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.AppBrandStructBean();
            bean.setBrand(appBean.getBrand());
            bean.setName(appBean.getName());
            bean.setDescription(appBean.getDescription());
        }
        return bean;
    }

}
