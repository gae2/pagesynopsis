package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgBlogService;


// The primary purpose of OgBlogDummyService is to fake the service api, OgBlogService.
// It has no real implementation.
public class OgBlogDummyService implements OgBlogService
{
    private static final Logger log = Logger.getLogger(OgBlogDummyService.class.getName());

    public OgBlogDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // OgBlog related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgBlog getOgBlog(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgBlog(): guid = " + guid);
        return null;
    }

    @Override
    public Object getOgBlog(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgBlog(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<OgBlog> getOgBlogs(List<String> guids) throws BaseException
    {
        log.fine("getOgBlogs()");
        return null;
    }

    @Override
    public List<OgBlog> getAllOgBlogs() throws BaseException
    {
        return getAllOgBlogs(null, null, null);
    }


    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBlogs(ordering, offset, count, null);
    }

    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgBlogs(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBlogKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgBlogKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgBlogs(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgBlogDummyService.findOgBlogs(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgBlogDummyService.findOgBlogKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgBlogDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createOgBlog(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        log.finer("createOgBlog()");
        return null;
    }

    @Override
    public String createOgBlog(OgBlog ogBlog) throws BaseException
    {
        log.finer("createOgBlog()");
        return null;
    }

    @Override
    public OgBlog constructOgBlog(OgBlog ogBlog) throws BaseException
    {
        log.finer("constructOgBlog()");
        return null;
    }

    @Override
    public Boolean updateOgBlog(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        log.finer("updateOgBlog()");
        return null;
    }
        
    @Override
    public Boolean updateOgBlog(OgBlog ogBlog) throws BaseException
    {
        log.finer("updateOgBlog()");
        return null;
    }

    @Override
    public OgBlog refreshOgBlog(OgBlog ogBlog) throws BaseException
    {
        log.finer("refreshOgBlog()");
        return null;
    }

    @Override
    public Boolean deleteOgBlog(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgBlog(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteOgBlog(OgBlog ogBlog) throws BaseException
    {
        log.finer("deleteOgBlog()");
        return null;
    }

    // TBD
    @Override
    public Long deleteOgBlogs(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgBlog(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgBlogs(List<OgBlog> ogBlogs) throws BaseException
    {
        log.finer("createOgBlogs()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateOgBlogs(List<OgBlog> ogBlogs) throws BaseException
    //{
    //}

}
