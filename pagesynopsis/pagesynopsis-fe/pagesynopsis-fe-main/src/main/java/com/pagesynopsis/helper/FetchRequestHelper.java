package com.pagesynopsis.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.FetchRequestJsBean;
import com.pagesynopsis.wa.service.FetchRequestWebService;
import com.pagesynopsis.wa.service.UserWebService;


public class FetchRequestHelper
{
    private static final Logger log = Logger.getLogger(FetchRequestHelper.class.getName());
    
    // temporary
    private static final int MAX_MEMO_COUNT = 5000;
    private static final int DEFAULT_MAX_MEMO_COUNT = 2500;
    // temporary

    private UserWebService userWebService = null;
    private FetchRequestWebService pingBlastWebService = null;
    // ...

    private FetchRequestHelper() {}

    // Initialization-on-demand holder.
    private static final class FetchRequestHelperHolder
    {
        private static final FetchRequestHelper INSTANCE = new FetchRequestHelper();
    }

    // Singleton method
    public static FetchRequestHelper getInstance()
    {
        return FetchRequestHelperHolder.INSTANCE;
    }
    
    
    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private FetchRequestWebService getFetchRequestWebService()
    {
        if(pingBlastWebService == null) {
            pingBlastWebService = new FetchRequestWebService();
        }
        return pingBlastWebService;
    }

    
    public List<FetchRequestJsBean> findRecentFetchRequests()
    {
        return findRecentFetchRequests(DEFAULT_MAX_MEMO_COUNT);
    }

    // TBD:
    // TBD: filter -> Target site and/or pingUrl ???
    public List<FetchRequestJsBean> findRecentFetchRequests(int maxCount)
    {
        List<FetchRequestJsBean> pingBlasts = null;
        try {
            String filter = null;
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = maxCount;   // TBD: Validation??? ( maxCount < MAX_MEMO_COUNT ???? )
            pingBlasts = getFetchRequestWebService().findFetchRequests(filter, ordering, null, null, null, null, offset, count);    
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find pingBlasts.", e);
            return null;
        }

        return pingBlasts;
    }

    
    // Format the timestamp to W3C date format: "yyyy-mm-dd".
    public static String formatDate(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date(time));
        return date;
    }
    
}
