package com.pagesynopsis.app.cron;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.fetch.FetchProcessor;
import com.pagesynopsis.app.helper.AudioSetHelper;
import com.pagesynopsis.app.helper.FetchRequestHelper;
import com.pagesynopsis.app.helper.ImageSetHelper;
import com.pagesynopsis.app.helper.LinkListHelper;
import com.pagesynopsis.app.helper.PageFetchHelper;
import com.pagesynopsis.app.helper.PageInfoHelper;
import com.pagesynopsis.app.helper.VideoSetHelper;
import com.pagesynopsis.app.service.AudioSetAppService;
import com.pagesynopsis.app.service.FetchRequestAppService;
import com.pagesynopsis.app.service.ImageSetAppService;
import com.pagesynopsis.app.service.LinkListAppService;
import com.pagesynopsis.app.service.PageFetchAppService;
import com.pagesynopsis.app.service.PageInfoAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.app.service.VideoSetAppService;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.ws.VideoSet;


public class FetchCronManager
{
    private static final Logger log = Logger.getLogger(FetchCronManager.class.getName());   

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private FetchRequestAppService fetchRequestAppService = null;
    private PageInfoAppService pageInfoAppService = null;
    private PageFetchAppService pageFetchAppService = null;
    private LinkListAppService linkListAppService = null;
    private ImageSetAppService imageSetAppService = null;
    private AudioSetAppService audioSetAppService = null;
    private VideoSetAppService videoSetAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private FetchRequestAppService getFetchRequestService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new FetchRequestAppService();
        }
        return fetchRequestAppService;
    }
    private PageInfoAppService getPageInfoService()
    {
        if(pageInfoAppService == null) {
            pageInfoAppService = new PageInfoAppService();
        }
        return pageInfoAppService;
    }
    private PageFetchAppService getPageFetchService()
    {
        if(pageFetchAppService == null) {
            pageFetchAppService = new PageFetchAppService();
        }
        return pageFetchAppService;
    }
    private LinkListAppService getLinkListService()
    {
        if(linkListAppService == null) {
            linkListAppService = new LinkListAppService();
        }
        return linkListAppService;
    }
    private ImageSetAppService getImageSetService()
    {
        if(imageSetAppService == null) {
            imageSetAppService = new ImageSetAppService();
        }
        return imageSetAppService;
    }
    private AudioSetAppService getAudioSetService()
    {
        if(audioSetAppService == null) {
            audioSetAppService = new AudioSetAppService();
        }
        return audioSetAppService;
    }
    private VideoSetAppService getVideoSetService()
    {
        if(videoSetAppService == null) {
            videoSetAppService = new VideoSetAppService();
        }
        return videoSetAppService;
    }
  
        // etc. ...

    
    private FetchCronManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class FetchCronManagerHolder
    {
        private static final FetchCronManager INSTANCE = new FetchCronManager();
    }

    // Singleton method
    public static FetchCronManager getInstance()
    {
        return FetchCronManagerHolder.INSTANCE;
    }

    private void init()
    {
        // TBD: ...
    }
    
    
    
    public int processPageInfoRefresh()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = PageInfoHelper.getInstance().getPageInfoKeysForRefreshProcessing(maxCount);
        return processPageInfos(keys);
    }

    private int processPageInfos(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    PageInfo pageInfo = PageInfoHelper.getInstance().getPageInfo(key);
                    log.fine("PageInfo retrieved for processing: pageInfo = " + pageInfo);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    pageInfo = FetchProcessor.getInstance().processPageInfoFetch(pageInfo);
                    log.fine("PageInfo processed: pageInfo = " + pageInfo);
                   
                    // ????
                    // ...
                    Boolean suc = getPageInfoService().updatePageInfo(pageInfo);
                    log.info("Refresh request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated fetch record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
    

    
    public int processPageFetchRefresh()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = PageFetchHelper.getInstance().getPageFetchKeysForRefreshProcessing(maxCount);
        return processPageFetches(keys);
    }

    private int processPageFetches(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    PageFetch pageFetch = PageFetchHelper.getInstance().getPageFetch(key);
                    log.fine("PageFetch retrieved for processing: pageFetch = " + pageFetch);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    pageFetch = FetchProcessor.getInstance().processPageFetchFetch(pageFetch);
                    log.fine("PageFetch processed: pageFetch = " + pageFetch);
                   
                    // ????
                    // ...
                    Boolean suc = getPageFetchService().updatePageFetch(pageFetch);
                    log.info("Refresh request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated fetch record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
    

    
    public int processLinkRefresh()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = LinkListHelper.getInstance().getLinkListKeysForRefreshProcessing(maxCount);
        return processLinkLists(keys);
    }

    private int processLinkLists(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    LinkList linkList = LinkListHelper.getInstance().getLinkList(key);
                    log.fine("LinkList retrieved for processing: linkList = " + linkList);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    linkList = FetchProcessor.getInstance().processLinkFetch(linkList);
                    log.fine("LinkList processed: linkList = " + linkList);
                   
                    // ????
                    // ...
                    Boolean suc = getLinkListService().updateLinkList(linkList);
                    log.info("Refresh request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated fetch record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
        
    
    
    public int processImageRefresh()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = ImageSetHelper.getInstance().getImageSetKeysForRefreshProcessing(maxCount);
        return processImageSets(keys);
    }

    private int processImageSets(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    ImageSet imageSet = ImageSetHelper.getInstance().getImageSet(key);
                    log.fine("ImageSet retrieved for processing: imageSet = " + imageSet);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    imageSet = FetchProcessor.getInstance().processImageFetch(imageSet);
                    log.fine("ImageSet processed: imageSet = " + imageSet);
                   
                    // ????
                    // ...
                    Boolean suc = getImageSetService().updateImageSet(imageSet);
                    log.info("Refresh request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated fetch record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
        



    
    public int processAudioRefresh()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = AudioSetHelper.getInstance().getAudioSetKeysForRefreshProcessing(maxCount);
        return processAudioSets(keys);
    }

    private int processAudioSets(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    AudioSet audioSet = AudioSetHelper.getInstance().getAudioSet(key);
                    log.fine("AudioSet retrieved for processing: audioSet = " + audioSet);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    audioSet = FetchProcessor.getInstance().processAudioFetch(audioSet);
                    log.fine("AudioSet processed: audioSet = " + audioSet);
                   
                    // ????
                    // ...
                    Boolean suc = getAudioSetService().updateAudioSet(audioSet);
                    log.info("Refresh request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated fetch record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
        


    
    public int processVideoRefresh()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = VideoSetHelper.getInstance().getVideoSetKeysForRefreshProcessing(maxCount);
        return processVideoSets(keys);
    }

    private int processVideoSets(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    VideoSet videoSet = VideoSetHelper.getInstance().getVideoSet(key);
                    log.fine("VideoSet retrieved for processing: videoSet = " + videoSet);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    videoSet = FetchProcessor.getInstance().processVideoFetch(videoSet);
                    log.fine("VideoSet processed: videoSet = " + videoSet);
                   
                    // ????
                    // ...
                    Boolean suc = getVideoSetService().updateVideoSet(videoSet);
                    log.info("Refresh request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated fetch record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
        

    
    
    
    // TBD:
    
    public int processPageInfoFetch()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = FetchRequestHelper.getInstance().getFetchRequestKeysForProcessing(maxCount, "pageinfo");
        return processFetchRequests(keys);
    }

    public int processPageFetchFetch()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = FetchRequestHelper.getInstance().getFetchRequestKeysForProcessing(maxCount, "pagefetch");
        return processFetchRequests(keys);
    }

    public int processLinkFetch()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = FetchRequestHelper.getInstance().getFetchRequestKeysForProcessing(maxCount, "linklist");
        return processFetchRequests(keys);
    }

    public int processImageFetch()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = FetchRequestHelper.getInstance().getFetchRequestKeysForProcessing(maxCount, "imageset");
        return processFetchRequests(keys);
    }

    public int processAudioFetch()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = FetchRequestHelper.getInstance().getFetchRequestKeysForProcessing(maxCount, "audioset");
        return processFetchRequests(keys);
    }

    public int processVideoFetch()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = FetchRequestHelper.getInstance().getFetchRequestKeysForProcessing(maxCount, "videoset");
        return processFetchRequests(keys);
    }


    private int processFetchRequests(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    FetchRequest fetchRequest = FetchRequestHelper.getInstance().getFetchRequest(key);
                    log.fine("FetchRequest retrieved for processing: fetchRequest = " + fetchRequest);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    
                    // TBD: target
                    // PageInfo vs. LinkList
                    // ...
                    // ...
                    
                    
                    fetchRequest = FetchProcessor.getInstance().processPageFetch(fetchRequest);  // ???
                    log.fine("FetchRequest processed: fetchRequest = " + fetchRequest);
                    
                    // ????
                    // ...
                    // TBD:
                    // Create or update pageInfo as well...
                    // ...
                    
                    Boolean suc = getFetchRequestService().updateFetchRequest(fetchRequest);
                    log.info("Fetch request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated fetch record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
    

}
