package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.TwitterCardAppInfo;
// import com.pagesynopsis.ws.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;


public class TwitterCardAppInfoProxyUtil
{
    private static final Logger log = Logger.getLogger(TwitterCardAppInfoProxyUtil.class.getName());

    // Static methods only.
    private TwitterCardAppInfoProxyUtil() {}

    public static TwitterCardAppInfoBean convertServerTwitterCardAppInfoBeanToAppBean(TwitterCardAppInfo serverBean)
    {
        TwitterCardAppInfoBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new TwitterCardAppInfoBean();
            bean.setName(serverBean.getName());
            bean.setId(serverBean.getId());
            bean.setUrl(serverBean.getUrl());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.TwitterCardAppInfoBean convertAppTwitterCardAppInfoBeanToServerBean(TwitterCardAppInfo appBean)
    {
        com.pagesynopsis.ws.bean.TwitterCardAppInfoBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.TwitterCardAppInfoBean();
            bean.setName(appBean.getName());
            bean.setId(appBean.getId());
            bean.setUrl(appBean.getUrl());
        }
        return bean;
    }

}
