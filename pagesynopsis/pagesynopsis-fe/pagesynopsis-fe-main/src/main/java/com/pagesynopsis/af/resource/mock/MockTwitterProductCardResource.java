package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.TwitterProductCardListStub;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.resource.TwitterProductCardResource;
import com.pagesynopsis.af.resource.util.TwitterCardAppInfoResourceUtil;
import com.pagesynopsis.af.resource.util.TwitterCardProductDataResourceUtil;


// MockTwitterProductCardResource is a decorator.
// It can be used as a base class to mock TwitterProductCardResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/twitterProductCards/")
public abstract class MockTwitterProductCardResource implements TwitterProductCardResource
{
    private static final Logger log = Logger.getLogger(MockTwitterProductCardResource.class.getName());

    // MockTwitterProductCardResource uses the decorator design pattern.
    private TwitterProductCardResource decoratedResource;

    public MockTwitterProductCardResource(TwitterProductCardResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected TwitterProductCardResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(TwitterProductCardResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllTwitterProductCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterProductCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllTwitterProductCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterProductCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterProductCardsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findTwitterProductCardsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getTwitterProductCardAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getTwitterProductCardAsHtml(guid);
//    }

    @Override
    public Response getTwitterProductCard(String guid) throws BaseResourceException
    {
        return decoratedResource.getTwitterProductCard(guid);
    }

    @Override
    public Response getTwitterProductCardAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getTwitterProductCardAsJsonp(guid, callback);
    }

    @Override
    public Response getTwitterProductCard(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getTwitterProductCard(guid, field);
    }

    // TBD
    @Override
    public Response constructTwitterProductCard(TwitterProductCardStub twitterProductCard) throws BaseResourceException
    {
        return decoratedResource.constructTwitterProductCard(twitterProductCard);
    }

    @Override
    public Response createTwitterProductCard(TwitterProductCardStub twitterProductCard) throws BaseResourceException
    {
        return decoratedResource.createTwitterProductCard(twitterProductCard);
    }

//    @Override
//    public Response createTwitterProductCard(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createTwitterProductCard(formParams);
//    }

    // TBD
    @Override
    public Response refreshTwitterProductCard(String guid, TwitterProductCardStub twitterProductCard) throws BaseResourceException
    {
        return decoratedResource.refreshTwitterProductCard(guid, twitterProductCard);
    }

    @Override
    public Response updateTwitterProductCard(String guid, TwitterProductCardStub twitterProductCard) throws BaseResourceException
    {
        return decoratedResource.updateTwitterProductCard(guid, twitterProductCard);
    }

    @Override
    public Response updateTwitterProductCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String data1, String data2)
    {
        return decoratedResource.updateTwitterProductCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1, data2);
    }

//    @Override
//    public Response updateTwitterProductCard(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateTwitterProductCard(guid, formParams);
//    }

    @Override
    public Response deleteTwitterProductCard(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterProductCard(guid);
    }

    @Override
    public Response deleteTwitterProductCards(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterProductCards(filter, params, values);
    }


// TBD ....
    @Override
    public Response createTwitterProductCards(TwitterProductCardListStub twitterProductCards) throws BaseResourceException
    {
        return decoratedResource.createTwitterProductCards(twitterProductCards);
    }


}
