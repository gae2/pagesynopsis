package com.pagesynopsis.af.cron.servlet;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.af.service.ApiConsumerService;
import com.pagesynopsis.af.service.UserService;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.OgProfileService;
import com.pagesynopsis.af.service.OgWebsiteService;
import com.pagesynopsis.af.service.OgBlogService;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.af.service.OgVideoService;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.af.service.TwitterSummaryCardService;
import com.pagesynopsis.af.service.TwitterPhotoCardService;
import com.pagesynopsis.af.service.TwitterGalleryCardService;
import com.pagesynopsis.af.service.TwitterAppCardService;
import com.pagesynopsis.af.service.TwitterPlayerCardService;
import com.pagesynopsis.af.service.TwitterProductCardService;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.af.service.PageInfoService;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.af.service.ImageSetService;
import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.af.service.OpenGraphMetaService;
import com.pagesynopsis.af.service.TwitterCardMetaService;
import com.pagesynopsis.af.service.DomainInfoService;
import com.pagesynopsis.af.service.UrlRatingService;
import com.pagesynopsis.af.service.ServiceInfoService;
import com.pagesynopsis.af.service.FiveTenService;
import com.pagesynopsis.af.service.impl.ApiConsumerServiceImpl;
import com.pagesynopsis.af.service.impl.UserServiceImpl;
import com.pagesynopsis.af.service.impl.RobotsTextFileServiceImpl;
import com.pagesynopsis.af.service.impl.RobotsTextRefreshServiceImpl;
import com.pagesynopsis.af.service.impl.OgProfileServiceImpl;
import com.pagesynopsis.af.service.impl.OgWebsiteServiceImpl;
import com.pagesynopsis.af.service.impl.OgBlogServiceImpl;
import com.pagesynopsis.af.service.impl.OgArticleServiceImpl;
import com.pagesynopsis.af.service.impl.OgBookServiceImpl;
import com.pagesynopsis.af.service.impl.OgVideoServiceImpl;
import com.pagesynopsis.af.service.impl.OgMovieServiceImpl;
import com.pagesynopsis.af.service.impl.OgTvShowServiceImpl;
import com.pagesynopsis.af.service.impl.OgTvEpisodeServiceImpl;
import com.pagesynopsis.af.service.impl.TwitterSummaryCardServiceImpl;
import com.pagesynopsis.af.service.impl.TwitterPhotoCardServiceImpl;
import com.pagesynopsis.af.service.impl.TwitterGalleryCardServiceImpl;
import com.pagesynopsis.af.service.impl.TwitterAppCardServiceImpl;
import com.pagesynopsis.af.service.impl.TwitterPlayerCardServiceImpl;
import com.pagesynopsis.af.service.impl.TwitterProductCardServiceImpl;
import com.pagesynopsis.af.service.impl.FetchRequestServiceImpl;
import com.pagesynopsis.af.service.impl.PageInfoServiceImpl;
import com.pagesynopsis.af.service.impl.PageFetchServiceImpl;
import com.pagesynopsis.af.service.impl.LinkListServiceImpl;
import com.pagesynopsis.af.service.impl.ImageSetServiceImpl;
import com.pagesynopsis.af.service.impl.AudioSetServiceImpl;
import com.pagesynopsis.af.service.impl.VideoSetServiceImpl;
import com.pagesynopsis.af.service.impl.OpenGraphMetaServiceImpl;
import com.pagesynopsis.af.service.impl.TwitterCardMetaServiceImpl;
import com.pagesynopsis.af.service.impl.DomainInfoServiceImpl;
import com.pagesynopsis.af.service.impl.UrlRatingServiceImpl;
import com.pagesynopsis.af.service.impl.ServiceInfoServiceImpl;
import com.pagesynopsis.af.service.impl.FiveTenServiceImpl;


public final class DataPurgeCronManager
{
    private static final Logger log = Logger.getLogger(DataPurgeCronManager.class.getName());   

    // TBD...
    // Count is per entity, not per cron run.
    public static final int DEFAULT_MAX_COUNT = 10;
    public static final int MAXIMUM_MAX_COUNT = 1000;   // ????
    // ...
    public static final int DEFAULT_DELTA_HOURS = 168;    // 1 week.
    public static final int MAXIMUM_DELTA_HOURS = 8760;   // 1 year.
    // ...
    

    // TBD: Is this safe for concurrent calls??
    private ApiConsumerService apiConsumerService = null;
    private UserService userService = null;
    private RobotsTextFileService robotsTextFileService = null;
    private RobotsTextRefreshService robotsTextRefreshService = null;
    private OgProfileService ogProfileService = null;
    private OgWebsiteService ogWebsiteService = null;
    private OgBlogService ogBlogService = null;
    private OgArticleService ogArticleService = null;
    private OgBookService ogBookService = null;
    private OgVideoService ogVideoService = null;
    private OgMovieService ogMovieService = null;
    private OgTvShowService ogTvShowService = null;
    private OgTvEpisodeService ogTvEpisodeService = null;
    private TwitterSummaryCardService twitterSummaryCardService = null;
    private TwitterPhotoCardService twitterPhotoCardService = null;
    private TwitterGalleryCardService twitterGalleryCardService = null;
    private TwitterAppCardService twitterAppCardService = null;
    private TwitterPlayerCardService twitterPlayerCardService = null;
    private TwitterProductCardService twitterProductCardService = null;
    private FetchRequestService fetchRequestService = null;
    private PageInfoService pageInfoService = null;
    private PageFetchService pageFetchService = null;
    private LinkListService linkListService = null;
    private ImageSetService imageSetService = null;
    private AudioSetService audioSetService = null;
    private VideoSetService videoSetService = null;
    private OpenGraphMetaService openGraphMetaService = null;
    private TwitterCardMetaService twitterCardMetaService = null;
    private DomainInfoService domainInfoService = null;
    private UrlRatingService urlRatingService = null;
    private ServiceInfoService serviceInfoService = null;
    private FiveTenService fiveTenService = null;
    // etc...

    // Lazy initialized.
    private ApiConsumerService getApiConsumerService()
    {
        if(apiConsumerService == null) {
            apiConsumerService = new ApiConsumerServiceImpl();
        }
        return apiConsumerService;
    }
    private UserService getUserService()
    {
        if(userService == null) {
            userService = new UserServiceImpl();
        }
        return userService;
    }
    private RobotsTextFileService getRobotsTextFileService()
    {
        if(robotsTextFileService == null) {
            robotsTextFileService = new RobotsTextFileServiceImpl();
        }
        return robotsTextFileService;
    }
    private RobotsTextRefreshService getRobotsTextRefreshService()
    {
        if(robotsTextRefreshService == null) {
            robotsTextRefreshService = new RobotsTextRefreshServiceImpl();
        }
        return robotsTextRefreshService;
    }
    private OgProfileService getOgProfileService()
    {
        if(ogProfileService == null) {
            ogProfileService = new OgProfileServiceImpl();
        }
        return ogProfileService;
    }
    private OgWebsiteService getOgWebsiteService()
    {
        if(ogWebsiteService == null) {
            ogWebsiteService = new OgWebsiteServiceImpl();
        }
        return ogWebsiteService;
    }
    private OgBlogService getOgBlogService()
    {
        if(ogBlogService == null) {
            ogBlogService = new OgBlogServiceImpl();
        }
        return ogBlogService;
    }
    private OgArticleService getOgArticleService()
    {
        if(ogArticleService == null) {
            ogArticleService = new OgArticleServiceImpl();
        }
        return ogArticleService;
    }
    private OgBookService getOgBookService()
    {
        if(ogBookService == null) {
            ogBookService = new OgBookServiceImpl();
        }
        return ogBookService;
    }
    private OgVideoService getOgVideoService()
    {
        if(ogVideoService == null) {
            ogVideoService = new OgVideoServiceImpl();
        }
        return ogVideoService;
    }
    private OgMovieService getOgMovieService()
    {
        if(ogMovieService == null) {
            ogMovieService = new OgMovieServiceImpl();
        }
        return ogMovieService;
    }
    private OgTvShowService getOgTvShowService()
    {
        if(ogTvShowService == null) {
            ogTvShowService = new OgTvShowServiceImpl();
        }
        return ogTvShowService;
    }
    private OgTvEpisodeService getOgTvEpisodeService()
    {
        if(ogTvEpisodeService == null) {
            ogTvEpisodeService = new OgTvEpisodeServiceImpl();
        }
        return ogTvEpisodeService;
    }
    private TwitterSummaryCardService getTwitterSummaryCardService()
    {
        if(twitterSummaryCardService == null) {
            twitterSummaryCardService = new TwitterSummaryCardServiceImpl();
        }
        return twitterSummaryCardService;
    }
    private TwitterPhotoCardService getTwitterPhotoCardService()
    {
        if(twitterPhotoCardService == null) {
            twitterPhotoCardService = new TwitterPhotoCardServiceImpl();
        }
        return twitterPhotoCardService;
    }
    private TwitterGalleryCardService getTwitterGalleryCardService()
    {
        if(twitterGalleryCardService == null) {
            twitterGalleryCardService = new TwitterGalleryCardServiceImpl();
        }
        return twitterGalleryCardService;
    }
    private TwitterAppCardService getTwitterAppCardService()
    {
        if(twitterAppCardService == null) {
            twitterAppCardService = new TwitterAppCardServiceImpl();
        }
        return twitterAppCardService;
    }
    private TwitterPlayerCardService getTwitterPlayerCardService()
    {
        if(twitterPlayerCardService == null) {
            twitterPlayerCardService = new TwitterPlayerCardServiceImpl();
        }
        return twitterPlayerCardService;
    }
    private TwitterProductCardService getTwitterProductCardService()
    {
        if(twitterProductCardService == null) {
            twitterProductCardService = new TwitterProductCardServiceImpl();
        }
        return twitterProductCardService;
    }
    private FetchRequestService getFetchRequestService()
    {
        if(fetchRequestService == null) {
            fetchRequestService = new FetchRequestServiceImpl();
        }
        return fetchRequestService;
    }
    private PageInfoService getPageInfoService()
    {
        if(pageInfoService == null) {
            pageInfoService = new PageInfoServiceImpl();
        }
        return pageInfoService;
    }
    private PageFetchService getPageFetchService()
    {
        if(pageFetchService == null) {
            pageFetchService = new PageFetchServiceImpl();
        }
        return pageFetchService;
    }
    private LinkListService getLinkListService()
    {
        if(linkListService == null) {
            linkListService = new LinkListServiceImpl();
        }
        return linkListService;
    }
    private ImageSetService getImageSetService()
    {
        if(imageSetService == null) {
            imageSetService = new ImageSetServiceImpl();
        }
        return imageSetService;
    }
    private AudioSetService getAudioSetService()
    {
        if(audioSetService == null) {
            audioSetService = new AudioSetServiceImpl();
        }
        return audioSetService;
    }
    private VideoSetService getVideoSetService()
    {
        if(videoSetService == null) {
            videoSetService = new VideoSetServiceImpl();
        }
        return videoSetService;
    }
    private OpenGraphMetaService getOpenGraphMetaService()
    {
        if(openGraphMetaService == null) {
            openGraphMetaService = new OpenGraphMetaServiceImpl();
        }
        return openGraphMetaService;
    }
    private TwitterCardMetaService getTwitterCardMetaService()
    {
        if(twitterCardMetaService == null) {
            twitterCardMetaService = new TwitterCardMetaServiceImpl();
        }
        return twitterCardMetaService;
    }
    private DomainInfoService getDomainInfoService()
    {
        if(domainInfoService == null) {
            domainInfoService = new DomainInfoServiceImpl();
        }
        return domainInfoService;
    }
    private UrlRatingService getUrlRatingService()
    {
        if(urlRatingService == null) {
            urlRatingService = new UrlRatingServiceImpl();
        }
        return urlRatingService;
    }
    private ServiceInfoService getServiceInfoService()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new ServiceInfoServiceImpl();
        }
        return serviceInfoService;
    }
    private FiveTenService getFiveTenService()
    {
        if(fiveTenService == null) {
            fiveTenService = new FiveTenServiceImpl();
        }
        return fiveTenService;
    }
    // etc. ...

    
    private DataPurgeCronManager()
    {
        init();
    }
    private void init()
    {
        // TBD: ...
    }

    // Initialization-on-demand holder.
    private static class RefreshCronManagerHolder
    {
        private static final DataPurgeCronManager INSTANCE = new DataPurgeCronManager();
    }
    // Singleton method
    public static DataPurgeCronManager getInstance()
    {
        return RefreshCronManagerHolder.INSTANCE;
    }
    
    
    public int processDeletion(String entity, int deltaHours, int maxCount)
    {
        if(deltaHours <= 0) {
            deltaHours = DEFAULT_DELTA_HOURS;
        } else if(deltaHours > MAXIMUM_DELTA_HOURS) {
            deltaHours = MAXIMUM_DELTA_HOURS;
        }
        if(maxCount <= 0) {
            maxCount = DEFAULT_MAX_COUNT;            
        } else if(maxCount > MAXIMUM_MAX_COUNT) {
            maxCount = MAXIMUM_MAX_COUNT;            
        }

        // temporary
        int cnt = 0;
        switch(entity) {
        case "ApiConsumer":
            cnt = deleteApiConsumers(entity, deltaHours, maxCount);
            break;
        case "User":
            cnt = deleteUsers(entity, deltaHours, maxCount);
            break;
        case "RobotsTextFile":
            cnt = deleteRobotsTextFiles(entity, deltaHours, maxCount);
            break;
        case "RobotsTextRefresh":
            cnt = deleteRobotsTextRefreshes(entity, deltaHours, maxCount);
            break;
        case "OgProfile":
            cnt = deleteOgProfiles(entity, deltaHours, maxCount);
            break;
        case "OgWebsite":
            cnt = deleteOgWebsites(entity, deltaHours, maxCount);
            break;
        case "OgBlog":
            cnt = deleteOgBlogs(entity, deltaHours, maxCount);
            break;
        case "OgArticle":
            cnt = deleteOgArticles(entity, deltaHours, maxCount);
            break;
        case "OgBook":
            cnt = deleteOgBooks(entity, deltaHours, maxCount);
            break;
        case "OgVideo":
            cnt = deleteOgVideos(entity, deltaHours, maxCount);
            break;
        case "OgMovie":
            cnt = deleteOgMovies(entity, deltaHours, maxCount);
            break;
        case "OgTvShow":
            cnt = deleteOgTvShows(entity, deltaHours, maxCount);
            break;
        case "OgTvEpisode":
            cnt = deleteOgTvEpisodes(entity, deltaHours, maxCount);
            break;
        case "TwitterSummaryCard":
            cnt = deleteTwitterSummaryCards(entity, deltaHours, maxCount);
            break;
        case "TwitterPhotoCard":
            cnt = deleteTwitterPhotoCards(entity, deltaHours, maxCount);
            break;
        case "TwitterGalleryCard":
            cnt = deleteTwitterGalleryCards(entity, deltaHours, maxCount);
            break;
        case "TwitterAppCard":
            cnt = deleteTwitterAppCards(entity, deltaHours, maxCount);
            break;
        case "TwitterPlayerCard":
            cnt = deleteTwitterPlayerCards(entity, deltaHours, maxCount);
            break;
        case "TwitterProductCard":
            cnt = deleteTwitterProductCards(entity, deltaHours, maxCount);
            break;
        case "FetchRequest":
            cnt = deleteFetchRequests(entity, deltaHours, maxCount);
            break;
        case "PageInfo":
            cnt = deletePageInfos(entity, deltaHours, maxCount);
            break;
        case "PageFetch":
            cnt = deletePageFetches(entity, deltaHours, maxCount);
            break;
        case "LinkList":
            cnt = deleteLinkLists(entity, deltaHours, maxCount);
            break;
        case "ImageSet":
            cnt = deleteImageSets(entity, deltaHours, maxCount);
            break;
        case "AudioSet":
            cnt = deleteAudioSets(entity, deltaHours, maxCount);
            break;
        case "VideoSet":
            cnt = deleteVideoSets(entity, deltaHours, maxCount);
            break;
        case "OpenGraphMeta":
            cnt = deleteOpenGraphMetas(entity, deltaHours, maxCount);
            break;
        case "TwitterCardMeta":
            cnt = deleteTwitterCardMetas(entity, deltaHours, maxCount);
            break;
        case "DomainInfo":
            cnt = deleteDomainInfos(entity, deltaHours, maxCount);
            break;
        case "UrlRating":
            cnt = deleteUrlRatings(entity, deltaHours, maxCount);
            break;
        case "ServiceInfo":
            cnt = deleteServiceInfos(entity, deltaHours, maxCount);
            break;
        case "FiveTen":
            cnt = deleteFiveTens(entity, deltaHours, maxCount);
            break;
        default:
            log.warning("Unsupported entity type for bulk deletion: " + entity);
        }
        return cnt;
    }


    // Purge based on createdTime.
    // Note that for many entity types, createdTime may not be the best choice.
    // TBD: Pass order/filtering as args?


    public int deleteApiConsumers(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getApiConsumerService().deleteApiConsumers(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getApiConsumerService().findApiConsumerKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getApiConsumerService().deleteApiConsumer(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete ApiConsumer: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete ApiConsumers.", e);
        }
        return cnt;
    }

    public int deleteUsers(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getUserService().deleteUsers(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getUserService().findUserKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getUserService().deleteUser(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete User: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete Users.", e);
        }
        return cnt;
    }

    public int deleteRobotsTextFiles(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getRobotsTextFileService().deleteRobotsTextFiles(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getRobotsTextFileService().findRobotsTextFileKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getRobotsTextFileService().deleteRobotsTextFile(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete RobotsTextFile: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete RobotsTextFiles.", e);
        }
        return cnt;
    }

    public int deleteRobotsTextRefreshes(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getRobotsTextRefreshService().deleteRobotsTextRefreshes(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getRobotsTextRefreshService().findRobotsTextRefreshKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getRobotsTextRefreshService().deleteRobotsTextRefresh(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete RobotsTextRefresh: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete RobotsTextRefreshes.", e);
        }
        return cnt;
    }

    public int deleteOgProfiles(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getOgProfileService().deleteOgProfiles(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getOgProfileService().findOgProfileKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getOgProfileService().deleteOgProfile(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete OgProfile: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete OgProfiles.", e);
        }
        return cnt;
    }

    public int deleteOgWebsites(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getOgWebsiteService().deleteOgWebsites(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getOgWebsiteService().findOgWebsiteKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getOgWebsiteService().deleteOgWebsite(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete OgWebsite: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete OgWebsites.", e);
        }
        return cnt;
    }

    public int deleteOgBlogs(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getOgBlogService().deleteOgBlogs(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getOgBlogService().findOgBlogKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getOgBlogService().deleteOgBlog(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete OgBlog: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete OgBlogs.", e);
        }
        return cnt;
    }

    public int deleteOgArticles(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getOgArticleService().deleteOgArticles(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getOgArticleService().findOgArticleKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getOgArticleService().deleteOgArticle(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete OgArticle: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete OgArticles.", e);
        }
        return cnt;
    }

    public int deleteOgBooks(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getOgBookService().deleteOgBooks(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getOgBookService().findOgBookKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getOgBookService().deleteOgBook(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete OgBook: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete OgBooks.", e);
        }
        return cnt;
    }

    public int deleteOgVideos(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getOgVideoService().deleteOgVideos(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getOgVideoService().findOgVideoKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getOgVideoService().deleteOgVideo(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete OgVideo: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete OgVideos.", e);
        }
        return cnt;
    }

    public int deleteOgMovies(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getOgMovieService().deleteOgMovies(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getOgMovieService().findOgMovieKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getOgMovieService().deleteOgMovie(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete OgMovie: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete OgMovies.", e);
        }
        return cnt;
    }

    public int deleteOgTvShows(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getOgTvShowService().deleteOgTvShows(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getOgTvShowService().findOgTvShowKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getOgTvShowService().deleteOgTvShow(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete OgTvShow: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete OgTvShows.", e);
        }
        return cnt;
    }

    public int deleteOgTvEpisodes(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getOgTvEpisodeService().deleteOgTvEpisodes(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getOgTvEpisodeService().findOgTvEpisodeKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getOgTvEpisodeService().deleteOgTvEpisode(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete OgTvEpisode: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete OgTvEpisodes.", e);
        }
        return cnt;
    }

    public int deleteTwitterSummaryCards(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getTwitterSummaryCardService().deleteTwitterSummaryCards(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getTwitterSummaryCardService().findTwitterSummaryCardKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getTwitterSummaryCardService().deleteTwitterSummaryCard(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete TwitterSummaryCard: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete TwitterSummaryCards.", e);
        }
        return cnt;
    }

    public int deleteTwitterPhotoCards(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getTwitterPhotoCardService().deleteTwitterPhotoCards(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getTwitterPhotoCardService().findTwitterPhotoCardKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getTwitterPhotoCardService().deleteTwitterPhotoCard(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete TwitterPhotoCard: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete TwitterPhotoCards.", e);
        }
        return cnt;
    }

    public int deleteTwitterGalleryCards(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getTwitterGalleryCardService().deleteTwitterGalleryCards(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getTwitterGalleryCardService().findTwitterGalleryCardKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getTwitterGalleryCardService().deleteTwitterGalleryCard(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete TwitterGalleryCard: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete TwitterGalleryCards.", e);
        }
        return cnt;
    }

    public int deleteTwitterAppCards(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getTwitterAppCardService().deleteTwitterAppCards(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getTwitterAppCardService().findTwitterAppCardKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getTwitterAppCardService().deleteTwitterAppCard(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete TwitterAppCard: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete TwitterAppCards.", e);
        }
        return cnt;
    }

    public int deleteTwitterPlayerCards(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getTwitterPlayerCardService().deleteTwitterPlayerCards(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getTwitterPlayerCardService().findTwitterPlayerCardKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getTwitterPlayerCardService().deleteTwitterPlayerCard(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete TwitterPlayerCard: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete TwitterPlayerCards.", e);
        }
        return cnt;
    }

    public int deleteTwitterProductCards(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getTwitterProductCardService().deleteTwitterProductCards(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getTwitterProductCardService().findTwitterProductCardKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getTwitterProductCardService().deleteTwitterProductCard(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete TwitterProductCard: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete TwitterProductCards.", e);
        }
        return cnt;
    }

    public int deleteFetchRequests(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getFetchRequestService().deleteFetchRequests(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getFetchRequestService().findFetchRequestKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getFetchRequestService().deleteFetchRequest(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete FetchRequest: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete FetchRequests.", e);
        }
        return cnt;
    }

    public int deletePageInfos(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getPageInfoService().deletePageInfos(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getPageInfoService().findPageInfoKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getPageInfoService().deletePageInfo(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete PageInfo: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete PageInfos.", e);
        }
        return cnt;
    }

    public int deletePageFetches(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getPageFetchService().deletePageFetches(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getPageFetchService().findPageFetchKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getPageFetchService().deletePageFetch(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete PageFetch: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete PageFetches.", e);
        }
        return cnt;
    }

    public int deleteLinkLists(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getLinkListService().deleteLinkLists(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getLinkListService().findLinkListKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getLinkListService().deleteLinkList(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete LinkList: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete LinkLists.", e);
        }
        return cnt;
    }

    public int deleteImageSets(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getImageSetService().deleteImageSets(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getImageSetService().findImageSetKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getImageSetService().deleteImageSet(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete ImageSet: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete ImageSets.", e);
        }
        return cnt;
    }

    public int deleteAudioSets(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getAudioSetService().deleteAudioSets(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getAudioSetService().findAudioSetKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getAudioSetService().deleteAudioSet(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete AudioSet: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete AudioSets.", e);
        }
        return cnt;
    }

    public int deleteVideoSets(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getVideoSetService().deleteVideoSets(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getVideoSetService().findVideoSetKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getVideoSetService().deleteVideoSet(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete VideoSet: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete VideoSets.", e);
        }
        return cnt;
    }

    public int deleteOpenGraphMetas(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getOpenGraphMetaService().deleteOpenGraphMetas(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getOpenGraphMetaService().findOpenGraphMetaKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getOpenGraphMetaService().deleteOpenGraphMeta(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete OpenGraphMeta: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete OpenGraphMetas.", e);
        }
        return cnt;
    }

    public int deleteTwitterCardMetas(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getTwitterCardMetaService().deleteTwitterCardMetas(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getTwitterCardMetaService().findTwitterCardMetaKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getTwitterCardMetaService().deleteTwitterCardMeta(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete TwitterCardMeta: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete TwitterCardMetas.", e);
        }
        return cnt;
    }

    public int deleteDomainInfos(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getDomainInfoService().deleteDomainInfos(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getDomainInfoService().findDomainInfoKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getDomainInfoService().deleteDomainInfo(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete DomainInfo: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete DomainInfos.", e);
        }
        return cnt;
    }

    public int deleteUrlRatings(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getUrlRatingService().deleteUrlRatings(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getUrlRatingService().findUrlRatingKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getUrlRatingService().deleteUrlRating(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete UrlRating: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete UrlRatings.", e);
        }
        return cnt;
    }

    public int deleteServiceInfos(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getServiceInfoService().deleteServiceInfos(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getServiceInfoService().findServiceInfoKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getServiceInfoService().deleteServiceInfo(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete ServiceInfo: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete ServiceInfos.", e);
        }
        return cnt;
    }

    public int deleteFiveTens(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getFiveTenService().deleteFiveTens(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getFiveTenService().findFiveTenKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getFiveTenService().deleteFiveTen(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete FiveTen: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete FiveTens.", e);
        }
        return cnt;
    }


}
