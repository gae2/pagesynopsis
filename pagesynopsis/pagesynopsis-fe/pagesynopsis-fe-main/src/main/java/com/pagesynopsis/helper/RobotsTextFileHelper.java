package com.pagesynopsis.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.RobotsTextFileJsBean;
import com.pagesynopsis.wa.service.RobotsTextFileWebService;
import com.pagesynopsis.wa.service.UserWebService;


public class RobotsTextFileHelper
{
    private static final Logger log = Logger.getLogger(RobotsTextFileHelper.class.getName());

    // Arbitrary...
    private static final long MAX_AGE_MILLIS = 3600 * 1000L * 24 * 30;   // ~1 month

    private RobotsTextFileHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private RobotsTextFileWebService robotsTextFileWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private RobotsTextFileWebService getRobotsTextFileService()
    {
        if(robotsTextFileWebService == null) {
            robotsTextFileWebService = new RobotsTextFileWebService();
        }
        return robotsTextFileWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class RobotsTextFileHelperHolder
    {
        private static final RobotsTextFileHelper INSTANCE = new RobotsTextFileHelper();
    }

    // Singleton method
    public static RobotsTextFileHelper getInstance()
    {
        return RobotsTextFileHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public RobotsTextFileJsBean getRobotsTextFile(String guid) 
    {
        RobotsTextFileJsBean bean = null;
        try {
            bean = getRobotsTextFileService().getRobotsTextFile(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }

    public RobotsTextFileJsBean getRobotsTextFileBySiteUrl(String siteUrl) 
    {
        // Long cutoff = System.currentTimeMillis() - MAX_AGE_MILLIS;
        Long cutoff = null;
        return getRobotsTextFileBySiteUrl(siteUrl, cutoff);
    }
    public RobotsTextFileJsBean getRobotsTextFileBySiteUrl(String siteUrl, Long timeCutoff) 
    {
        return getRobotsTextFileBySiteUrl(siteUrl, timeCutoff, false);  // This should really be true.. ????? 
    }
    private RobotsTextFileJsBean getRobotsTextFileBySiteUrl(String siteUrl, Long timeCutoff, boolean refreshingRecordOnly) 
    {
        RobotsTextFileJsBean bean = null;
        try {
            String filter = "siteUrl=='" + siteUrl + "'";
            if(timeCutoff != null && timeCutoff > 0L) {
//              filter += " && createdTime >= " + timeCutoff;
              filter += " && lastCheckedTime >= " + timeCutoff;
            }
            // TBD: This requires new datastore index....
            if(refreshingRecordOnly == true) {
                // We should not return stale record...
                filter += " && refreshStatus != " + RefreshStatus.STATUS_NOREFRESH;
            }
//          String ordering = "createdTime desc";
          String ordering = "lastCheckedTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<RobotsTextFileJsBean> beans = getRobotsTextFileService().findRobotsTextFiles(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
                String guid = bean.getGuid();
                try {
                    bean = getRobotsTextFileService().getRobotsTextFile(guid);
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
                }
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for siteUrl = " + siteUrl, e);
        }
        return bean;
    }

    

    public List<String> getRobotsTextFileKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getRobotsTextFileService().findRobotsTextFileKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    // TBD: 
    public List<RobotsTextFileJsBean> getRobotsTextFilesForRefreshProcessing(Integer maxCount)
    {
        List<RobotsTextFileJsBean> robotsTextFiles = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            robotsTextFiles = getRobotsTextFileService().findRobotsTextFiles(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return robotsTextFiles;
    }


}
