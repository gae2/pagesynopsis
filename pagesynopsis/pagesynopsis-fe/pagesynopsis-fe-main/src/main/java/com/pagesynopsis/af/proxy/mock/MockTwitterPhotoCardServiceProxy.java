package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.af.proxy.TwitterPhotoCardServiceProxy;


// MockTwitterPhotoCardServiceProxy is a decorator.
// It can be used as a base class to mock TwitterPhotoCardServiceProxy objects.
public abstract class MockTwitterPhotoCardServiceProxy implements TwitterPhotoCardServiceProxy
{
    private static final Logger log = Logger.getLogger(MockTwitterPhotoCardServiceProxy.class.getName());

    // MockTwitterPhotoCardServiceProxy uses the decorator design pattern.
    private TwitterPhotoCardServiceProxy decoratedProxy;

    public MockTwitterPhotoCardServiceProxy(TwitterPhotoCardServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected TwitterPhotoCardServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(TwitterPhotoCardServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public TwitterPhotoCard getTwitterPhotoCard(String guid) throws BaseException
    {
        return decoratedProxy.getTwitterPhotoCard(guid);
    }

    @Override
    public Object getTwitterPhotoCard(String guid, String field) throws BaseException
    {
        return decoratedProxy.getTwitterPhotoCard(guid, field);       
    }

    @Override
    public List<TwitterPhotoCard> getTwitterPhotoCards(List<String> guids) throws BaseException
    {
        return decoratedProxy.getTwitterPhotoCards(guids);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards() throws BaseException
    {
        return getAllTwitterPhotoCards(null, null, null);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllTwitterPhotoCards(ordering, offset, count);
        return getAllTwitterPhotoCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterPhotoCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllTwitterPhotoCardKeys(ordering, offset, count);
        return getAllTwitterPhotoCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterPhotoCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterPhotoCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return decoratedProxy.createTwitterPhotoCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public String createTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        return decoratedProxy.createTwitterPhotoCard(twitterPhotoCard);
    }

    @Override
    public Boolean updateTwitterPhotoCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return decoratedProxy.updateTwitterPhotoCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public Boolean updateTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        return decoratedProxy.updateTwitterPhotoCard(twitterPhotoCard);
    }

    @Override
    public Boolean deleteTwitterPhotoCard(String guid) throws BaseException
    {
        return decoratedProxy.deleteTwitterPhotoCard(guid);
    }

    @Override
    public Boolean deleteTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        String guid = twitterPhotoCard.getGuid();
        return deleteTwitterPhotoCard(guid);
    }

    @Override
    public Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteTwitterPhotoCards(filter, params, values);
    }

}
