package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;


public class TwitterCardProductDataWebUtil
{
    private static final Logger log = Logger.getLogger(TwitterCardProductDataWebUtil.class.getName());

    // Static methods only.
    private TwitterCardProductDataWebUtil() {}
    

    public static TwitterCardProductDataJsBean convertTwitterCardProductDataToJsBean(TwitterCardProductData twitterCardProductData)
    {
        TwitterCardProductDataJsBean jsBean = null;
        if(twitterCardProductData != null) {
            jsBean = new TwitterCardProductDataJsBean();
            jsBean.setData(twitterCardProductData.getData());
            jsBean.setLabel(twitterCardProductData.getLabel());
        }
        return jsBean;
    }

    public static TwitterCardProductData convertTwitterCardProductDataJsBeanToBean(TwitterCardProductDataJsBean jsBean)
    {
        TwitterCardProductDataBean twitterCardProductData = null;
        if(jsBean != null) {
            twitterCardProductData = new TwitterCardProductDataBean();
            twitterCardProductData.setData(jsBean.getData());
            twitterCardProductData.setLabel(jsBean.getLabel());
        }
        return twitterCardProductData;
    }

}
