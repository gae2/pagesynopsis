package com.pagesynopsis.fe.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.KeyValueRelationStructJsBean;
import com.pagesynopsis.fe.bean.ExternalServiceApiKeyStructJsBean;
import com.pagesynopsis.fe.bean.GeoPointStructJsBean;
import com.pagesynopsis.fe.bean.GeoCoordinateStructJsBean;
import com.pagesynopsis.fe.bean.StreetAddressStructJsBean;
import com.pagesynopsis.fe.bean.FullNameStructJsBean;
import com.pagesynopsis.fe.bean.UserWebsiteStructJsBean;
import com.pagesynopsis.fe.bean.ContactInfoStructJsBean;
import com.pagesynopsis.fe.bean.ReferrerInfoStructJsBean;
import com.pagesynopsis.fe.bean.GaeAppStructJsBean;
import com.pagesynopsis.fe.bean.GaeUserStructJsBean;
import com.pagesynopsis.fe.bean.NotificationStructJsBean;
import com.pagesynopsis.fe.bean.PagerStateStructJsBean;
import com.pagesynopsis.fe.bean.AppBrandStructJsBean;
import com.pagesynopsis.fe.bean.ApiConsumerJsBean;
import com.pagesynopsis.fe.bean.UserJsBean;
import com.pagesynopsis.fe.bean.EncodedQueryParamStructJsBean;
import com.pagesynopsis.fe.bean.DecodedQueryParamStructJsBean;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.fe.bean.MediaSourceStructJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.fe.bean.RobotsTextGroupJsBean;
import com.pagesynopsis.fe.bean.RobotsTextFileJsBean;
import com.pagesynopsis.fe.bean.RobotsTextRefreshJsBean;
import com.pagesynopsis.fe.bean.OgContactInfoStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgGeoPointStructJsBean;
import com.pagesynopsis.fe.bean.OgQuantityStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.fe.bean.TwitterPlayerCardJsBean;
import com.pagesynopsis.fe.bean.TwitterProductCardJsBean;
import com.pagesynopsis.fe.bean.FetchRequestJsBean;
import com.pagesynopsis.fe.bean.PageInfoJsBean;
import com.pagesynopsis.fe.bean.PageFetchJsBean;
import com.pagesynopsis.fe.bean.LinkListJsBean;
import com.pagesynopsis.fe.bean.ImageSetJsBean;
import com.pagesynopsis.fe.bean.AudioSetJsBean;
import com.pagesynopsis.fe.bean.VideoSetJsBean;
import com.pagesynopsis.fe.bean.OpenGraphMetaJsBean;
import com.pagesynopsis.fe.bean.TwitterCardMetaJsBean;
import com.pagesynopsis.fe.bean.DomainInfoJsBean;
import com.pagesynopsis.fe.bean.UrlRatingJsBean;
import com.pagesynopsis.fe.bean.HelpNoticeJsBean;
import com.pagesynopsis.fe.bean.ServiceInfoJsBean;
import com.pagesynopsis.fe.bean.FiveTenJsBean;


// Utility functions for combining/decomposing into json string segments.
public class JsonJsBeanUtil
{
    private static final Logger log = Logger.getLogger(JsonJsBeanUtil.class.getName());

    private JsonJsBeanUtil() {}

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> objJsonString
    public static Map<String, String> parseJsonObjectMapString(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, String> jsonObjectMap = new HashMap<String, String>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            factory.setCodec(getObjectMapper());
            JsonParser parser = factory.createJsonParser(jsonStr);

            JsonNode topNode = parser.readValueAsTree();
            Iterator<String> fieldNames = topNode.getFieldNames();
            
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                JsonNode leafNode = topNode.get(name);
                if(! leafNode.isNull()) {
                    // ???
                    // String value = leafNode.asText();
                    String value = leafNode.toString();
                    // ...
                    jsonObjectMap.put(name, value);
                    if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: name = " + name + "; value = " + value);
                } else {
                    if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: Empty node skipped. name = " + name);
                }
            }
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> objJsonString
    public static String generateJsonObjectMapString(Map<String, String> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for(String key : jsonObjectMap.keySet()) {
            String json = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(json == null) {
                sb.append("null");  // ????
            } else {
                sb.append(json);
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        if(log.isLoggable(Level.INFO)) log.info("generateJsonObjectMapString(): jsonStr = " + jsonStr);

        return jsonStr;
    }


    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> jsBean
    public static Map<String, Object> parseJsonObjectMap(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, Object> jsonObjectMap = new HashMap<String, Object>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            factory.setCodec(getObjectMapper());
            JsonParser parser = factory.createJsonParser(jsonStr);
            JsonNode topNode = parser.readValueAsTree();

/*
            // TBD: Remove the loop.
            Iterator<String> fieldNames = topNode.getFieldNames();
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                String value = topNode.get(name).asText();
                if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: name = " + name + "; value = " + value);

                if(name.equals("keyValuePairStruct")) {
                    KeyValuePairStructJsBean bean = KeyValuePairStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("keyValueRelationStruct")) {
                    KeyValueRelationStructJsBean bean = KeyValueRelationStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("externalServiceApiKeyStruct")) {
                    ExternalServiceApiKeyStructJsBean bean = ExternalServiceApiKeyStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("geoPointStruct")) {
                    GeoPointStructJsBean bean = GeoPointStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("geoCoordinateStruct")) {
                    GeoCoordinateStructJsBean bean = GeoCoordinateStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("streetAddressStruct")) {
                    StreetAddressStructJsBean bean = StreetAddressStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fullNameStruct")) {
                    FullNameStructJsBean bean = FullNameStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("userWebsiteStruct")) {
                    UserWebsiteStructJsBean bean = UserWebsiteStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("contactInfoStruct")) {
                    ContactInfoStructJsBean bean = ContactInfoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("referrerInfoStruct")) {
                    ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeAppStruct")) {
                    GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeUserStruct")) {
                    GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("notificationStruct")) {
                    NotificationStructJsBean bean = NotificationStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("pagerStateStruct")) {
                    PagerStateStructJsBean bean = PagerStateStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("appBrandStruct")) {
                    AppBrandStructJsBean bean = AppBrandStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("apiConsumer")) {
                    ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("user")) {
                    UserJsBean bean = UserJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("encodedQueryParamStruct")) {
                    EncodedQueryParamStructJsBean bean = EncodedQueryParamStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("decodedQueryParamStruct")) {
                    DecodedQueryParamStructJsBean bean = DecodedQueryParamStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("urlStruct")) {
                    UrlStructJsBean bean = UrlStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("anchorStruct")) {
                    AnchorStructJsBean bean = AnchorStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("imageStruct")) {
                    ImageStructJsBean bean = ImageStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("mediaSourceStruct")) {
                    MediaSourceStructJsBean bean = MediaSourceStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("audioStruct")) {
                    AudioStructJsBean bean = AudioStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("videoStruct")) {
                    VideoStructJsBean bean = VideoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("robotsTextGroup")) {
                    RobotsTextGroupJsBean bean = RobotsTextGroupJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("robotsTextFile")) {
                    RobotsTextFileJsBean bean = RobotsTextFileJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("robotsTextRefresh")) {
                    RobotsTextRefreshJsBean bean = RobotsTextRefreshJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogContactInfoStruct")) {
                    OgContactInfoStructJsBean bean = OgContactInfoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogImageStruct")) {
                    OgImageStructJsBean bean = OgImageStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogAudioStruct")) {
                    OgAudioStructJsBean bean = OgAudioStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogVideoStruct")) {
                    OgVideoStructJsBean bean = OgVideoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogGeoPointStruct")) {
                    OgGeoPointStructJsBean bean = OgGeoPointStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogQuantityStruct")) {
                    OgQuantityStructJsBean bean = OgQuantityStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogActorStruct")) {
                    OgActorStructJsBean bean = OgActorStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogProfile")) {
                    OgProfileJsBean bean = OgProfileJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogWebsite")) {
                    OgWebsiteJsBean bean = OgWebsiteJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogBlog")) {
                    OgBlogJsBean bean = OgBlogJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogArticle")) {
                    OgArticleJsBean bean = OgArticleJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogBook")) {
                    OgBookJsBean bean = OgBookJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogVideo")) {
                    OgVideoJsBean bean = OgVideoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogMovie")) {
                    OgMovieJsBean bean = OgMovieJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogTvShow")) {
                    OgTvShowJsBean bean = OgTvShowJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("ogTvEpisode")) {
                    OgTvEpisodeJsBean bean = OgTvEpisodeJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterCardAppInfo")) {
                    TwitterCardAppInfoJsBean bean = TwitterCardAppInfoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterCardProductData")) {
                    TwitterCardProductDataJsBean bean = TwitterCardProductDataJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterSummaryCard")) {
                    TwitterSummaryCardJsBean bean = TwitterSummaryCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterPhotoCard")) {
                    TwitterPhotoCardJsBean bean = TwitterPhotoCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterGalleryCard")) {
                    TwitterGalleryCardJsBean bean = TwitterGalleryCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterAppCard")) {
                    TwitterAppCardJsBean bean = TwitterAppCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterPlayerCard")) {
                    TwitterPlayerCardJsBean bean = TwitterPlayerCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterProductCard")) {
                    TwitterProductCardJsBean bean = TwitterProductCardJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fetchRequest")) {
                    FetchRequestJsBean bean = FetchRequestJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("pageInfo")) {
                    PageInfoJsBean bean = PageInfoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("pageFetch")) {
                    PageFetchJsBean bean = PageFetchJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("linkList")) {
                    LinkListJsBean bean = LinkListJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("imageSet")) {
                    ImageSetJsBean bean = ImageSetJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("audioSet")) {
                    AudioSetJsBean bean = AudioSetJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("videoSet")) {
                    VideoSetJsBean bean = VideoSetJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("openGraphMeta")) {
                    OpenGraphMetaJsBean bean = OpenGraphMetaJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("twitterCardMeta")) {
                    TwitterCardMetaJsBean bean = TwitterCardMetaJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("domainInfo")) {
                    DomainInfoJsBean bean = DomainInfoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("urlRating")) {
                    UrlRatingJsBean bean = UrlRatingJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("helpNotice")) {
                    HelpNoticeJsBean bean = HelpNoticeJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("serviceInfo")) {
                    ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fiveTen")) {
                    FiveTenJsBean bean = FiveTenJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
            }
*/

            JsonNode objNode = null;
            String objValueStr = null;
            objNode = topNode.findValue("keyValuePairStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeyValuePairStructJsBean bean = KeyValuePairStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keyValuePairStruct", bean);               
            }
            objNode = topNode.findValue("keyValueRelationStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeyValueRelationStructJsBean bean = KeyValueRelationStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keyValueRelationStruct", bean);               
            }
            objNode = topNode.findValue("externalServiceApiKeyStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ExternalServiceApiKeyStructJsBean bean = ExternalServiceApiKeyStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("externalServiceApiKeyStruct", bean);               
            }
            objNode = topNode.findValue("geoPointStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GeoPointStructJsBean bean = GeoPointStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("geoPointStruct", bean);               
            }
            objNode = topNode.findValue("geoCoordinateStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GeoCoordinateStructJsBean bean = GeoCoordinateStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("geoCoordinateStruct", bean);               
            }
            objNode = topNode.findValue("streetAddressStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                StreetAddressStructJsBean bean = StreetAddressStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("streetAddressStruct", bean);               
            }
            objNode = topNode.findValue("fullNameStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FullNameStructJsBean bean = FullNameStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fullNameStruct", bean);               
            }
            objNode = topNode.findValue("userWebsiteStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserWebsiteStructJsBean bean = UserWebsiteStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("userWebsiteStruct", bean);               
            }
            objNode = topNode.findValue("contactInfoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ContactInfoStructJsBean bean = ContactInfoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("contactInfoStruct", bean);               
            }
            objNode = topNode.findValue("referrerInfoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("referrerInfoStruct", bean);               
            }
            objNode = topNode.findValue("gaeAppStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeAppStruct", bean);               
            }
            objNode = topNode.findValue("gaeUserStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeUserStruct", bean);               
            }
            objNode = topNode.findValue("notificationStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                NotificationStructJsBean bean = NotificationStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("notificationStruct", bean);               
            }
            objNode = topNode.findValue("pagerStateStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                PagerStateStructJsBean bean = PagerStateStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("pagerStateStruct", bean);               
            }
            objNode = topNode.findValue("appBrandStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AppBrandStructJsBean bean = AppBrandStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("appBrandStruct", bean);               
            }
            objNode = topNode.findValue("apiConsumer");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("apiConsumer", bean);               
            }
            objNode = topNode.findValue("user");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserJsBean bean = UserJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("user", bean);               
            }
            objNode = topNode.findValue("encodedQueryParamStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                EncodedQueryParamStructJsBean bean = EncodedQueryParamStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("encodedQueryParamStruct", bean);               
            }
            objNode = topNode.findValue("decodedQueryParamStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                DecodedQueryParamStructJsBean bean = DecodedQueryParamStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("decodedQueryParamStruct", bean);               
            }
            objNode = topNode.findValue("urlStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UrlStructJsBean bean = UrlStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("urlStruct", bean);               
            }
            objNode = topNode.findValue("anchorStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AnchorStructJsBean bean = AnchorStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("anchorStruct", bean);               
            }
            objNode = topNode.findValue("imageStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ImageStructJsBean bean = ImageStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("imageStruct", bean);               
            }
            objNode = topNode.findValue("mediaSourceStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                MediaSourceStructJsBean bean = MediaSourceStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("mediaSourceStruct", bean);               
            }
            objNode = topNode.findValue("audioStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AudioStructJsBean bean = AudioStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("audioStruct", bean);               
            }
            objNode = topNode.findValue("videoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                VideoStructJsBean bean = VideoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("videoStruct", bean);               
            }
            objNode = topNode.findValue("robotsTextGroup");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                RobotsTextGroupJsBean bean = RobotsTextGroupJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("robotsTextGroup", bean);               
            }
            objNode = topNode.findValue("robotsTextFile");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                RobotsTextFileJsBean bean = RobotsTextFileJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("robotsTextFile", bean);               
            }
            objNode = topNode.findValue("robotsTextRefresh");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                RobotsTextRefreshJsBean bean = RobotsTextRefreshJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("robotsTextRefresh", bean);               
            }
            objNode = topNode.findValue("ogContactInfoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgContactInfoStructJsBean bean = OgContactInfoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogContactInfoStruct", bean);               
            }
            objNode = topNode.findValue("ogImageStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgImageStructJsBean bean = OgImageStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogImageStruct", bean);               
            }
            objNode = topNode.findValue("ogAudioStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgAudioStructJsBean bean = OgAudioStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogAudioStruct", bean);               
            }
            objNode = topNode.findValue("ogVideoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgVideoStructJsBean bean = OgVideoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogVideoStruct", bean);               
            }
            objNode = topNode.findValue("ogGeoPointStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgGeoPointStructJsBean bean = OgGeoPointStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogGeoPointStruct", bean);               
            }
            objNode = topNode.findValue("ogQuantityStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgQuantityStructJsBean bean = OgQuantityStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogQuantityStruct", bean);               
            }
            objNode = topNode.findValue("ogActorStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgActorStructJsBean bean = OgActorStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogActorStruct", bean);               
            }
            objNode = topNode.findValue("ogProfile");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgProfileJsBean bean = OgProfileJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogProfile", bean);               
            }
            objNode = topNode.findValue("ogWebsite");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgWebsiteJsBean bean = OgWebsiteJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogWebsite", bean);               
            }
            objNode = topNode.findValue("ogBlog");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgBlogJsBean bean = OgBlogJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogBlog", bean);               
            }
            objNode = topNode.findValue("ogArticle");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgArticleJsBean bean = OgArticleJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogArticle", bean);               
            }
            objNode = topNode.findValue("ogBook");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgBookJsBean bean = OgBookJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogBook", bean);               
            }
            objNode = topNode.findValue("ogVideo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgVideoJsBean bean = OgVideoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogVideo", bean);               
            }
            objNode = topNode.findValue("ogMovie");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgMovieJsBean bean = OgMovieJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogMovie", bean);               
            }
            objNode = topNode.findValue("ogTvShow");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgTvShowJsBean bean = OgTvShowJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogTvShow", bean);               
            }
            objNode = topNode.findValue("ogTvEpisode");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OgTvEpisodeJsBean bean = OgTvEpisodeJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("ogTvEpisode", bean);               
            }
            objNode = topNode.findValue("twitterCardAppInfo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterCardAppInfoJsBean bean = TwitterCardAppInfoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterCardAppInfo", bean);               
            }
            objNode = topNode.findValue("twitterCardProductData");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterCardProductDataJsBean bean = TwitterCardProductDataJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterCardProductData", bean);               
            }
            objNode = topNode.findValue("twitterSummaryCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterSummaryCardJsBean bean = TwitterSummaryCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterSummaryCard", bean);               
            }
            objNode = topNode.findValue("twitterPhotoCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterPhotoCardJsBean bean = TwitterPhotoCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterPhotoCard", bean);               
            }
            objNode = topNode.findValue("twitterGalleryCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterGalleryCardJsBean bean = TwitterGalleryCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterGalleryCard", bean);               
            }
            objNode = topNode.findValue("twitterAppCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterAppCardJsBean bean = TwitterAppCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterAppCard", bean);               
            }
            objNode = topNode.findValue("twitterPlayerCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterPlayerCardJsBean bean = TwitterPlayerCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterPlayerCard", bean);               
            }
            objNode = topNode.findValue("twitterProductCard");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterProductCardJsBean bean = TwitterProductCardJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterProductCard", bean);               
            }
            objNode = topNode.findValue("fetchRequest");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FetchRequestJsBean bean = FetchRequestJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fetchRequest", bean);               
            }
            objNode = topNode.findValue("pageInfo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                PageInfoJsBean bean = PageInfoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("pageInfo", bean);               
            }
            objNode = topNode.findValue("pageFetch");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                PageFetchJsBean bean = PageFetchJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("pageFetch", bean);               
            }
            objNode = topNode.findValue("linkList");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                LinkListJsBean bean = LinkListJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("linkList", bean);               
            }
            objNode = topNode.findValue("imageSet");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ImageSetJsBean bean = ImageSetJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("imageSet", bean);               
            }
            objNode = topNode.findValue("audioSet");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AudioSetJsBean bean = AudioSetJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("audioSet", bean);               
            }
            objNode = topNode.findValue("videoSet");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                VideoSetJsBean bean = VideoSetJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("videoSet", bean);               
            }
            objNode = topNode.findValue("openGraphMeta");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                OpenGraphMetaJsBean bean = OpenGraphMetaJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("openGraphMeta", bean);               
            }
            objNode = topNode.findValue("twitterCardMeta");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TwitterCardMetaJsBean bean = TwitterCardMetaJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("twitterCardMeta", bean);               
            }
            objNode = topNode.findValue("domainInfo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                DomainInfoJsBean bean = DomainInfoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("domainInfo", bean);               
            }
            objNode = topNode.findValue("urlRating");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UrlRatingJsBean bean = UrlRatingJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("urlRating", bean);               
            }
            objNode = topNode.findValue("helpNotice");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                HelpNoticeJsBean bean = HelpNoticeJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("helpNotice", bean);               
            }
            objNode = topNode.findValue("serviceInfo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("serviceInfo", bean);               
            }
            objNode = topNode.findValue("fiveTen");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FiveTenJsBean bean = FiveTenJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fiveTen", bean);               
            }
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> jsBean
    public static String generateJsonObjectMap(Map<String, Object> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        // TBD: Remove the loop.
        for(String key : jsonObjectMap.keySet()) {
            Object jsonObj = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(jsonObj == null) {
                sb.append("null");  // ????
            } else {
                if(jsonObj instanceof KeyValuePairStructJsBean) {
                    sb.append(((KeyValuePairStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof KeyValueRelationStructJsBean) {
                    sb.append(((KeyValueRelationStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ExternalServiceApiKeyStructJsBean) {
                    sb.append(((ExternalServiceApiKeyStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GeoPointStructJsBean) {
                    sb.append(((GeoPointStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GeoCoordinateStructJsBean) {
                    sb.append(((GeoCoordinateStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof StreetAddressStructJsBean) {
                    sb.append(((StreetAddressStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FullNameStructJsBean) {
                    sb.append(((FullNameStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserWebsiteStructJsBean) {
                    sb.append(((UserWebsiteStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ContactInfoStructJsBean) {
                    sb.append(((ContactInfoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ReferrerInfoStructJsBean) {
                    sb.append(((ReferrerInfoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeAppStructJsBean) {
                    sb.append(((GaeAppStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeUserStructJsBean) {
                    sb.append(((GaeUserStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof NotificationStructJsBean) {
                    sb.append(((NotificationStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof PagerStateStructJsBean) {
                    sb.append(((PagerStateStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AppBrandStructJsBean) {
                    sb.append(((AppBrandStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ApiConsumerJsBean) {
                    sb.append(((ApiConsumerJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserJsBean) {
                    sb.append(((UserJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof EncodedQueryParamStructJsBean) {
                    sb.append(((EncodedQueryParamStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof DecodedQueryParamStructJsBean) {
                    sb.append(((DecodedQueryParamStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UrlStructJsBean) {
                    sb.append(((UrlStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AnchorStructJsBean) {
                    sb.append(((AnchorStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ImageStructJsBean) {
                    sb.append(((ImageStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof MediaSourceStructJsBean) {
                    sb.append(((MediaSourceStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AudioStructJsBean) {
                    sb.append(((AudioStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof VideoStructJsBean) {
                    sb.append(((VideoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof RobotsTextGroupJsBean) {
                    sb.append(((RobotsTextGroupJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof RobotsTextFileJsBean) {
                    sb.append(((RobotsTextFileJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof RobotsTextRefreshJsBean) {
                    sb.append(((RobotsTextRefreshJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgContactInfoStructJsBean) {
                    sb.append(((OgContactInfoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgImageStructJsBean) {
                    sb.append(((OgImageStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgAudioStructJsBean) {
                    sb.append(((OgAudioStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgVideoStructJsBean) {
                    sb.append(((OgVideoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgGeoPointStructJsBean) {
                    sb.append(((OgGeoPointStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgQuantityStructJsBean) {
                    sb.append(((OgQuantityStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgActorStructJsBean) {
                    sb.append(((OgActorStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgProfileJsBean) {
                    sb.append(((OgProfileJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgWebsiteJsBean) {
                    sb.append(((OgWebsiteJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgBlogJsBean) {
                    sb.append(((OgBlogJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgArticleJsBean) {
                    sb.append(((OgArticleJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgBookJsBean) {
                    sb.append(((OgBookJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgVideoJsBean) {
                    sb.append(((OgVideoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgMovieJsBean) {
                    sb.append(((OgMovieJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgTvShowJsBean) {
                    sb.append(((OgTvShowJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OgTvEpisodeJsBean) {
                    sb.append(((OgTvEpisodeJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterCardAppInfoJsBean) {
                    sb.append(((TwitterCardAppInfoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterCardProductDataJsBean) {
                    sb.append(((TwitterCardProductDataJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterSummaryCardJsBean) {
                    sb.append(((TwitterSummaryCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterPhotoCardJsBean) {
                    sb.append(((TwitterPhotoCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterGalleryCardJsBean) {
                    sb.append(((TwitterGalleryCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterAppCardJsBean) {
                    sb.append(((TwitterAppCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterPlayerCardJsBean) {
                    sb.append(((TwitterPlayerCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterProductCardJsBean) {
                    sb.append(((TwitterProductCardJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FetchRequestJsBean) {
                    sb.append(((FetchRequestJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof PageInfoJsBean) {
                    sb.append(((PageInfoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof PageFetchJsBean) {
                    sb.append(((PageFetchJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof LinkListJsBean) {
                    sb.append(((LinkListJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ImageSetJsBean) {
                    sb.append(((ImageSetJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AudioSetJsBean) {
                    sb.append(((AudioSetJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof VideoSetJsBean) {
                    sb.append(((VideoSetJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof OpenGraphMetaJsBean) {
                    sb.append(((OpenGraphMetaJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TwitterCardMetaJsBean) {
                    sb.append(((TwitterCardMetaJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof DomainInfoJsBean) {
                    sb.append(((DomainInfoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UrlRatingJsBean) {
                    sb.append(((UrlRatingJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof HelpNoticeJsBean) {
                    sb.append(((HelpNoticeJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ServiceInfoJsBean) {
                    sb.append(((ServiceInfoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FiveTenJsBean) {
                    sb.append(((FiveTenJsBean) jsonObj).toJsonString());
                }
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        if(log.isLoggable(Level.INFO)) log.info("generateJsonObjectMap(): jsonStr = " + jsonStr);

        return jsonStr;
    }
    
    
}
