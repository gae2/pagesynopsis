package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.ws.stub.DomainInfoStub;
import com.pagesynopsis.ws.stub.DomainInfoListStub;
import com.pagesynopsis.af.bean.DomainInfoBean;
import com.pagesynopsis.af.resource.DomainInfoResource;


// MockDomainInfoResource is a decorator.
// It can be used as a base class to mock DomainInfoResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/domainInfos/")
public abstract class MockDomainInfoResource implements DomainInfoResource
{
    private static final Logger log = Logger.getLogger(MockDomainInfoResource.class.getName());

    // MockDomainInfoResource uses the decorator design pattern.
    private DomainInfoResource decoratedResource;

    public MockDomainInfoResource(DomainInfoResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected DomainInfoResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(DomainInfoResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllDomainInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDomainInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllDomainInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDomainInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findDomainInfosAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findDomainInfosAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getDomainInfoAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getDomainInfoAsHtml(guid);
//    }

    @Override
    public Response getDomainInfo(String guid) throws BaseResourceException
    {
        return decoratedResource.getDomainInfo(guid);
    }

    @Override
    public Response getDomainInfoAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getDomainInfoAsJsonp(guid, callback);
    }

    @Override
    public Response getDomainInfo(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getDomainInfo(guid, field);
    }

    // TBD
    @Override
    public Response constructDomainInfo(DomainInfoStub domainInfo) throws BaseResourceException
    {
        return decoratedResource.constructDomainInfo(domainInfo);
    }

    @Override
    public Response createDomainInfo(DomainInfoStub domainInfo) throws BaseResourceException
    {
        return decoratedResource.createDomainInfo(domainInfo);
    }

//    @Override
//    public Response createDomainInfo(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createDomainInfo(formParams);
//    }

    // TBD
    @Override
    public Response refreshDomainInfo(String guid, DomainInfoStub domainInfo) throws BaseResourceException
    {
        return decoratedResource.refreshDomainInfo(guid, domainInfo);
    }

    @Override
    public Response updateDomainInfo(String guid, DomainInfoStub domainInfo) throws BaseResourceException
    {
        return decoratedResource.updateDomainInfo(guid, domainInfo);
    }

    @Override
    public Response updateDomainInfo(String guid, String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime)
    {
        return decoratedResource.updateDomainInfo(guid, domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
    }

//    @Override
//    public Response updateDomainInfo(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateDomainInfo(guid, formParams);
//    }

    @Override
    public Response deleteDomainInfo(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteDomainInfo(guid);
    }

    @Override
    public Response deleteDomainInfos(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteDomainInfos(filter, params, values);
    }


// TBD ....
    @Override
    public Response createDomainInfos(DomainInfoListStub domainInfos) throws BaseResourceException
    {
        return decoratedResource.createDomainInfos(domainInfos);
    }


}
