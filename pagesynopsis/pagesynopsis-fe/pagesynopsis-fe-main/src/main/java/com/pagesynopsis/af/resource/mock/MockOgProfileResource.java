package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.stub.OgProfileStub;
import com.pagesynopsis.ws.stub.OgProfileListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.resource.OgProfileResource;
import com.pagesynopsis.af.resource.util.OgAudioStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgImageStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgActorStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgVideoStructResourceUtil;


// MockOgProfileResource is a decorator.
// It can be used as a base class to mock OgProfileResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/ogProfiles/")
public abstract class MockOgProfileResource implements OgProfileResource
{
    private static final Logger log = Logger.getLogger(MockOgProfileResource.class.getName());

    // MockOgProfileResource uses the decorator design pattern.
    private OgProfileResource decoratedResource;

    public MockOgProfileResource(OgProfileResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected OgProfileResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(OgProfileResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgProfiles(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgProfileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgProfilesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findOgProfilesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getOgProfileAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getOgProfileAsHtml(guid);
//    }

    @Override
    public Response getOgProfile(String guid) throws BaseResourceException
    {
        return decoratedResource.getOgProfile(guid);
    }

    @Override
    public Response getOgProfileAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getOgProfileAsJsonp(guid, callback);
    }

    @Override
    public Response getOgProfile(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getOgProfile(guid, field);
    }

    // TBD
    @Override
    public Response constructOgProfile(OgProfileStub ogProfile) throws BaseResourceException
    {
        return decoratedResource.constructOgProfile(ogProfile);
    }

    @Override
    public Response createOgProfile(OgProfileStub ogProfile) throws BaseResourceException
    {
        return decoratedResource.createOgProfile(ogProfile);
    }

//    @Override
//    public Response createOgProfile(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createOgProfile(formParams);
//    }

    // TBD
    @Override
    public Response refreshOgProfile(String guid, OgProfileStub ogProfile) throws BaseResourceException
    {
        return decoratedResource.refreshOgProfile(guid, ogProfile);
    }

    @Override
    public Response updateOgProfile(String guid, OgProfileStub ogProfile) throws BaseResourceException
    {
        return decoratedResource.updateOgProfile(guid, ogProfile);
    }

    @Override
    public Response updateOgProfile(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<String> image, List<String> audio, List<String> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender)
    {
        return decoratedResource.updateOgProfile(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
    }

//    @Override
//    public Response updateOgProfile(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateOgProfile(guid, formParams);
//    }

    @Override
    public Response deleteOgProfile(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteOgProfile(guid);
    }

    @Override
    public Response deleteOgProfiles(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteOgProfiles(filter, params, values);
    }


// TBD ....
    @Override
    public Response createOgProfiles(OgProfileListStub ogProfiles) throws BaseResourceException
    {
        return decoratedResource.createOgProfiles(ogProfiles);
    }


}
