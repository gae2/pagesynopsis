package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.AnchorStruct;
// import com.pagesynopsis.ws.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;


public class AnchorStructProxyUtil
{
    private static final Logger log = Logger.getLogger(AnchorStructProxyUtil.class.getName());

    // Static methods only.
    private AnchorStructProxyUtil() {}

    public static AnchorStructBean convertServerAnchorStructBeanToAppBean(AnchorStruct serverBean)
    {
        AnchorStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new AnchorStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setId(serverBean.getId());
            bean.setName(serverBean.getName());
            bean.setHref(serverBean.getHref());
            bean.setHrefUrl(serverBean.getHrefUrl());
            bean.setUrlScheme(serverBean.getUrlScheme());
            bean.setRel(serverBean.getRel());
            bean.setTarget(serverBean.getTarget());
            bean.setInnerHtml(serverBean.getInnerHtml());
            bean.setAnchorText(serverBean.getAnchorText());
            bean.setAnchorTitle(serverBean.getAnchorTitle());
            bean.setAnchorImage(serverBean.getAnchorImage());
            bean.setAnchorLegend(serverBean.getAnchorLegend());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.AnchorStructBean convertAppAnchorStructBeanToServerBean(AnchorStruct appBean)
    {
        com.pagesynopsis.ws.bean.AnchorStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.AnchorStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setId(appBean.getId());
            bean.setName(appBean.getName());
            bean.setHref(appBean.getHref());
            bean.setHrefUrl(appBean.getHrefUrl());
            bean.setUrlScheme(appBean.getUrlScheme());
            bean.setRel(appBean.getRel());
            bean.setTarget(appBean.getTarget());
            bean.setInnerHtml(appBean.getInnerHtml());
            bean.setAnchorText(appBean.getAnchorText());
            bean.setAnchorTitle(appBean.getAnchorTitle());
            bean.setAnchorImage(appBean.getAnchorImage());
            bean.setAnchorLegend(appBean.getAnchorLegend());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
