package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.OgObjectBase;
import com.pagesynopsis.ws.stub.OgObjectBaseStub;


// Wrapper class + bean combo.
public abstract class OgObjectBaseBean implements OgObjectBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgObjectBaseBean.class.getName());

    // [1] With an embedded object.
    private OgObjectBaseStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String url;
    private String type;
    private String siteName;
    private String title;
    private String description;
    private List<String> fbAdmins;
    private List<String> fbAppId;
    private List<OgImageStruct> image;
    private List<OgAudioStruct> audio;
    private List<OgVideoStruct> video;
    private String locale;
    private List<String> localeAlternate;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public OgObjectBaseBean()
    {
        //this((String) null);
    }
    public OgObjectBaseBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OgObjectBaseBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate)
    {
        this(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, null, null);
    }
    public OgObjectBaseBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.url = url;
        this.type = type;
        this.siteName = siteName;
        this.title = title;
        this.description = description;
        this.fbAdmins = fbAdmins;
        this.fbAppId = fbAppId;
        this.image = image;
        this.audio = audio;
        this.video = video;
        this.locale = locale;
        this.localeAlternate = localeAlternate;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public OgObjectBaseBean(OgObjectBase stub)
    {
        if(stub instanceof OgObjectBaseStub) {
            this.stub = (OgObjectBaseStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUrl(stub.getUrl());   
            setType(stub.getType());   
            setSiteName(stub.getSiteName());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setFbAdmins(stub.getFbAdmins());   
            setFbAppId(stub.getFbAppId());   
            setImage(stub.getImage());   
            setAudio(stub.getAudio());   
            setVideo(stub.getVideo());   
            setLocale(stub.getLocale());   
            setLocaleAlternate(stub.getLocaleAlternate());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUrl()
    {
        if(getStub() != null) {
            return getStub().getUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.url;
        }
    }
    public void setUrl(String url)
    {
        if(getStub() != null) {
            getStub().setUrl(url);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.url = url;
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }

    public String getSiteName()
    {
        if(getStub() != null) {
            return getStub().getSiteName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.siteName;
        }
    }
    public void setSiteName(String siteName)
    {
        if(getStub() != null) {
            getStub().setSiteName(siteName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.siteName = siteName;
        }
    }

    public String getTitle()
    {
        if(getStub() != null) {
            return getStub().getTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.title;
        }
    }
    public void setTitle(String title)
    {
        if(getStub() != null) {
            getStub().setTitle(title);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.title = title;
        }
    }

    public String getDescription()
    {
        if(getStub() != null) {
            return getStub().getDescription();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.description;
        }
    }
    public void setDescription(String description)
    {
        if(getStub() != null) {
            getStub().setDescription(description);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.description = description;
        }
    }

    public List<String> getFbAdmins()
    {
        if(getStub() != null) {
            return getStub().getFbAdmins();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fbAdmins;
        }
    }
    public void setFbAdmins(List<String> fbAdmins)
    {
        if(getStub() != null) {
            getStub().setFbAdmins(fbAdmins);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fbAdmins = fbAdmins;
        }
    }

    public List<String> getFbAppId()
    {
        if(getStub() != null) {
            return getStub().getFbAppId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fbAppId;
        }
    }
    public void setFbAppId(List<String> fbAppId)
    {
        if(getStub() != null) {
            getStub().setFbAppId(fbAppId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fbAppId = fbAppId;
        }
    }

    public List<OgImageStruct> getImage()
    {
        if(getStub() != null) {
            List<OgImageStruct> list = getStub().getImage();
            if(list != null) {
                List<OgImageStruct> bean = new ArrayList<OgImageStruct>();
                for(OgImageStruct ogImageStruct : list) {
                    OgImageStructBean elem = null;
                    if(ogImageStruct instanceof OgImageStructBean) {
                        elem = (OgImageStructBean) ogImageStruct;
                    } else if(ogImageStruct instanceof OgImageStructStub) {
                        elem = new OgImageStructBean(ogImageStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.image;
        }
    }
    public void setImage(List<OgImageStruct> image)
    {
        if(image != null) {
            if(getStub() != null) {
                List<OgImageStruct> stub = new ArrayList<OgImageStruct>();
                for(OgImageStruct ogImageStruct : image) {
                    OgImageStructStub elem = null;
                    if(ogImageStruct instanceof OgImageStructStub) {
                        elem = (OgImageStructStub) ogImageStruct;
                    } else if(ogImageStruct instanceof OgImageStructBean) {
                        elem = ((OgImageStructBean) ogImageStruct).getStub();
                    } else if(ogImageStruct instanceof OgImageStruct) {
                        elem = new OgImageStructStub(ogImageStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setImage(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.image = image;  // ???

                List<OgImageStruct> beans = new ArrayList<OgImageStruct>();
                for(OgImageStruct ogImageStruct : image) {
                    OgImageStructBean elem = null;
                    if(ogImageStruct != null) {
                        if(ogImageStruct instanceof OgImageStructBean) {
                            elem = (OgImageStructBean) ogImageStruct;
                        } else {
                            elem = new OgImageStructBean(ogImageStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.image = beans;
            }
        } else {
            this.image = null;
        }
    }

    public List<OgAudioStruct> getAudio()
    {
        if(getStub() != null) {
            List<OgAudioStruct> list = getStub().getAudio();
            if(list != null) {
                List<OgAudioStruct> bean = new ArrayList<OgAudioStruct>();
                for(OgAudioStruct ogAudioStruct : list) {
                    OgAudioStructBean elem = null;
                    if(ogAudioStruct instanceof OgAudioStructBean) {
                        elem = (OgAudioStructBean) ogAudioStruct;
                    } else if(ogAudioStruct instanceof OgAudioStructStub) {
                        elem = new OgAudioStructBean(ogAudioStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.audio;
        }
    }
    public void setAudio(List<OgAudioStruct> audio)
    {
        if(audio != null) {
            if(getStub() != null) {
                List<OgAudioStruct> stub = new ArrayList<OgAudioStruct>();
                for(OgAudioStruct ogAudioStruct : audio) {
                    OgAudioStructStub elem = null;
                    if(ogAudioStruct instanceof OgAudioStructStub) {
                        elem = (OgAudioStructStub) ogAudioStruct;
                    } else if(ogAudioStruct instanceof OgAudioStructBean) {
                        elem = ((OgAudioStructBean) ogAudioStruct).getStub();
                    } else if(ogAudioStruct instanceof OgAudioStruct) {
                        elem = new OgAudioStructStub(ogAudioStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setAudio(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.audio = audio;  // ???

                List<OgAudioStruct> beans = new ArrayList<OgAudioStruct>();
                for(OgAudioStruct ogAudioStruct : audio) {
                    OgAudioStructBean elem = null;
                    if(ogAudioStruct != null) {
                        if(ogAudioStruct instanceof OgAudioStructBean) {
                            elem = (OgAudioStructBean) ogAudioStruct;
                        } else {
                            elem = new OgAudioStructBean(ogAudioStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.audio = beans;
            }
        } else {
            this.audio = null;
        }
    }

    public List<OgVideoStruct> getVideo()
    {
        if(getStub() != null) {
            List<OgVideoStruct> list = getStub().getVideo();
            if(list != null) {
                List<OgVideoStruct> bean = new ArrayList<OgVideoStruct>();
                for(OgVideoStruct ogVideoStruct : list) {
                    OgVideoStructBean elem = null;
                    if(ogVideoStruct instanceof OgVideoStructBean) {
                        elem = (OgVideoStructBean) ogVideoStruct;
                    } else if(ogVideoStruct instanceof OgVideoStructStub) {
                        elem = new OgVideoStructBean(ogVideoStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.video;
        }
    }
    public void setVideo(List<OgVideoStruct> video)
    {
        if(video != null) {
            if(getStub() != null) {
                List<OgVideoStruct> stub = new ArrayList<OgVideoStruct>();
                for(OgVideoStruct ogVideoStruct : video) {
                    OgVideoStructStub elem = null;
                    if(ogVideoStruct instanceof OgVideoStructStub) {
                        elem = (OgVideoStructStub) ogVideoStruct;
                    } else if(ogVideoStruct instanceof OgVideoStructBean) {
                        elem = ((OgVideoStructBean) ogVideoStruct).getStub();
                    } else if(ogVideoStruct instanceof OgVideoStruct) {
                        elem = new OgVideoStructStub(ogVideoStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setVideo(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.video = video;  // ???

                List<OgVideoStruct> beans = new ArrayList<OgVideoStruct>();
                for(OgVideoStruct ogVideoStruct : video) {
                    OgVideoStructBean elem = null;
                    if(ogVideoStruct != null) {
                        if(ogVideoStruct instanceof OgVideoStructBean) {
                            elem = (OgVideoStructBean) ogVideoStruct;
                        } else {
                            elem = new OgVideoStructBean(ogVideoStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.video = beans;
            }
        } else {
            this.video = null;
        }
    }

    public String getLocale()
    {
        if(getStub() != null) {
            return getStub().getLocale();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.locale;
        }
    }
    public void setLocale(String locale)
    {
        if(getStub() != null) {
            getStub().setLocale(locale);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.locale = locale;
        }
    }

    public List<String> getLocaleAlternate()
    {
        if(getStub() != null) {
            return getStub().getLocaleAlternate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.localeAlternate;
        }
    }
    public void setLocaleAlternate(List<String> localeAlternate)
    {
        if(getStub() != null) {
            getStub().setLocaleAlternate(localeAlternate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.localeAlternate = localeAlternate;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public OgObjectBaseStub getStub()
    {
        return this.stub;
    }
    protected void setStub(OgObjectBaseStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("url = " + this.url).append(";");
            sb.append("type = " + this.type).append(";");
            sb.append("siteName = " + this.siteName).append(";");
            sb.append("title = " + this.title).append(";");
            sb.append("description = " + this.description).append(";");
            sb.append("fbAdmins = " + this.fbAdmins).append(";");
            sb.append("fbAppId = " + this.fbAppId).append(";");
            sb.append("image = " + this.image).append(";");
            sb.append("audio = " + this.audio).append(";");
            sb.append("video = " + this.video).append(";");
            sb.append("locale = " + this.locale).append(";");
            sb.append("localeAlternate = " + this.localeAlternate).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = url == null ? 0 : url.hashCode();
            _hash = 31 * _hash + delta;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            delta = siteName == null ? 0 : siteName.hashCode();
            _hash = 31 * _hash + delta;
            delta = title == null ? 0 : title.hashCode();
            _hash = 31 * _hash + delta;
            delta = description == null ? 0 : description.hashCode();
            _hash = 31 * _hash + delta;
            delta = fbAdmins == null ? 0 : fbAdmins.hashCode();
            _hash = 31 * _hash + delta;
            delta = fbAppId == null ? 0 : fbAppId.hashCode();
            _hash = 31 * _hash + delta;
            delta = image == null ? 0 : image.hashCode();
            _hash = 31 * _hash + delta;
            delta = audio == null ? 0 : audio.hashCode();
            _hash = 31 * _hash + delta;
            delta = video == null ? 0 : video.hashCode();
            _hash = 31 * _hash + delta;
            delta = locale == null ? 0 : locale.hashCode();
            _hash = 31 * _hash + delta;
            delta = localeAlternate == null ? 0 : localeAlternate.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
