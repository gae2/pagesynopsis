package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.ws.stub.PageInfoStub;
import com.pagesynopsis.ws.stub.PageInfoListStub;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.PageInfoBean;
import com.pagesynopsis.af.resource.PageInfoResource;
import com.pagesynopsis.af.resource.util.UrlStructResourceUtil;
import com.pagesynopsis.af.resource.util.ImageStructResourceUtil;
import com.pagesynopsis.af.resource.util.AnchorStructResourceUtil;
import com.pagesynopsis.af.resource.util.KeyValuePairStructResourceUtil;
import com.pagesynopsis.af.resource.util.AudioStructResourceUtil;
import com.pagesynopsis.af.resource.util.VideoStructResourceUtil;


// MockPageInfoResource is a decorator.
// It can be used as a base class to mock PageInfoResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/pageInfos/")
public abstract class MockPageInfoResource implements PageInfoResource
{
    private static final Logger log = Logger.getLogger(MockPageInfoResource.class.getName());

    // MockPageInfoResource uses the decorator design pattern.
    private PageInfoResource decoratedResource;

    public MockPageInfoResource(PageInfoResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected PageInfoResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(PageInfoResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllPageInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllPageInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllPageInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllPageInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findPageInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findPageInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findPageInfosAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findPageInfosAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getPageInfoAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getPageInfoAsHtml(guid);
//    }

    @Override
    public Response getPageInfo(String guid) throws BaseResourceException
    {
        return decoratedResource.getPageInfo(guid);
    }

    @Override
    public Response getPageInfoAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getPageInfoAsJsonp(guid, callback);
    }

    @Override
    public Response getPageInfo(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getPageInfo(guid, field);
    }

    // TBD
    @Override
    public Response constructPageInfo(PageInfoStub pageInfo) throws BaseResourceException
    {
        return decoratedResource.constructPageInfo(pageInfo);
    }

    @Override
    public Response createPageInfo(PageInfoStub pageInfo) throws BaseResourceException
    {
        return decoratedResource.createPageInfo(pageInfo);
    }

//    @Override
//    public Response createPageInfo(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createPageInfo(formParams);
//    }

    // TBD
    @Override
    public Response refreshPageInfo(String guid, PageInfoStub pageInfo) throws BaseResourceException
    {
        return decoratedResource.refreshPageInfo(guid, pageInfo);
    }

    @Override
    public Response updatePageInfo(String guid, PageInfoStub pageInfo) throws BaseResourceException
    {
        return decoratedResource.updatePageInfo(guid, pageInfo);
    }

    @Override
    public Response updatePageInfo(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<String> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String pageAuthor, String pageDescription, String favicon, String faviconUrl)
    {
        return decoratedResource.updatePageInfo(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, pageAuthor, pageDescription, favicon, faviconUrl);
    }

//    @Override
//    public Response updatePageInfo(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updatePageInfo(guid, formParams);
//    }

    @Override
    public Response deletePageInfo(String guid) throws BaseResourceException
    {
        return decoratedResource.deletePageInfo(guid);
    }

    @Override
    public Response deletePageInfos(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deletePageInfos(filter, params, values);
    }


// TBD ....
    @Override
    public Response createPageInfos(PageInfoListStub pageInfos) throws BaseResourceException
    {
        return decoratedResource.createPageInfos(pageInfos);
    }


}
