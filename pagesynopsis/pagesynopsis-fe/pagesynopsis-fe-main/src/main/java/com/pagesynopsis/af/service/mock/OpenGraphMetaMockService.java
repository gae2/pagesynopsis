package com.pagesynopsis.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.OpenGraphMetaBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OpenGraphMetaService;


// OpenGraphMetaMockService is a decorator.
// It can be used as a base class to mock OpenGraphMetaService objects.
public abstract class OpenGraphMetaMockService implements OpenGraphMetaService
{
    private static final Logger log = Logger.getLogger(OpenGraphMetaMockService.class.getName());

    // OpenGraphMetaMockService uses the decorator design pattern.
    private OpenGraphMetaService decoratedService;

    public OpenGraphMetaMockService(OpenGraphMetaService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected OpenGraphMetaService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(OpenGraphMetaService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // OpenGraphMeta related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OpenGraphMeta getOpenGraphMeta(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOpenGraphMeta(): guid = " + guid);
        OpenGraphMeta bean = decoratedService.getOpenGraphMeta(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getOpenGraphMeta(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getOpenGraphMeta(guid, field);
        return obj;
    }

    @Override
    public List<OpenGraphMeta> getOpenGraphMetas(List<String> guids) throws BaseException
    {
        log.fine("getOpenGraphMetas()");
        List<OpenGraphMeta> openGraphMetas = decoratedService.getOpenGraphMetas(guids);
        log.finer("END");
        return openGraphMetas;
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas() throws BaseException
    {
        return getAllOpenGraphMetas(null, null, null);
    }


    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOpenGraphMetas(ordering, offset, count, null);
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOpenGraphMetas(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<OpenGraphMeta> openGraphMetas = decoratedService.getAllOpenGraphMetas(ordering, offset, count, forwardCursor);
        log.finer("END");
        return openGraphMetas;
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOpenGraphMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOpenGraphMetaKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllOpenGraphMetaKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOpenGraphMetas(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OpenGraphMetaMockService.findOpenGraphMetas(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<OpenGraphMeta> openGraphMetas = decoratedService.findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return openGraphMetas;
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OpenGraphMetaMockService.findOpenGraphMetaKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OpenGraphMetaMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOpenGraphMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        OgProfileBean ogProfileBean = null;
        if(ogProfile instanceof OgProfileBean) {
            ogProfileBean = (OgProfileBean) ogProfile;
        } else if(ogProfile instanceof OgProfile) {
            ogProfileBean = new OgProfileBean(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender(), ogProfile.getCreatedTime(), ogProfile.getModifiedTime());
        } else {
            ogProfileBean = null;   // ????
        }
        OgWebsiteBean ogWebsiteBean = null;
        if(ogWebsite instanceof OgWebsiteBean) {
            ogWebsiteBean = (OgWebsiteBean) ogWebsite;
        } else if(ogWebsite instanceof OgWebsite) {
            ogWebsiteBean = new OgWebsiteBean(ogWebsite.getGuid(), ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote(), ogWebsite.getCreatedTime(), ogWebsite.getModifiedTime());
        } else {
            ogWebsiteBean = null;   // ????
        }
        OgBlogBean ogBlogBean = null;
        if(ogBlog instanceof OgBlogBean) {
            ogBlogBean = (OgBlogBean) ogBlog;
        } else if(ogBlog instanceof OgBlog) {
            ogBlogBean = new OgBlogBean(ogBlog.getGuid(), ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote(), ogBlog.getCreatedTime(), ogBlog.getModifiedTime());
        } else {
            ogBlogBean = null;   // ????
        }
        OgArticleBean ogArticleBean = null;
        if(ogArticle instanceof OgArticleBean) {
            ogArticleBean = (OgArticleBean) ogArticle;
        } else if(ogArticle instanceof OgArticle) {
            ogArticleBean = new OgArticleBean(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate(), ogArticle.getCreatedTime(), ogArticle.getModifiedTime());
        } else {
            ogArticleBean = null;   // ????
        }
        OgBookBean ogBookBean = null;
        if(ogBook instanceof OgBookBean) {
            ogBookBean = (OgBookBean) ogBook;
        } else if(ogBook instanceof OgBook) {
            ogBookBean = new OgBookBean(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate(), ogBook.getCreatedTime(), ogBook.getModifiedTime());
        } else {
            ogBookBean = null;   // ????
        }
        OgVideoBean ogVideoBean = null;
        if(ogVideo instanceof OgVideoBean) {
            ogVideoBean = (OgVideoBean) ogVideo;
        } else if(ogVideo instanceof OgVideo) {
            ogVideoBean = new OgVideoBean(ogVideo.getGuid(), ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate(), ogVideo.getCreatedTime(), ogVideo.getModifiedTime());
        } else {
            ogVideoBean = null;   // ????
        }
        OgMovieBean ogMovieBean = null;
        if(ogMovie instanceof OgMovieBean) {
            ogMovieBean = (OgMovieBean) ogMovie;
        } else if(ogMovie instanceof OgMovie) {
            ogMovieBean = new OgMovieBean(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate(), ogMovie.getCreatedTime(), ogMovie.getModifiedTime());
        } else {
            ogMovieBean = null;   // ????
        }
        OgTvShowBean ogTvShowBean = null;
        if(ogTvShow instanceof OgTvShowBean) {
            ogTvShowBean = (OgTvShowBean) ogTvShow;
        } else if(ogTvShow instanceof OgTvShow) {
            ogTvShowBean = new OgTvShowBean(ogTvShow.getGuid(), ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate(), ogTvShow.getCreatedTime(), ogTvShow.getModifiedTime());
        } else {
            ogTvShowBean = null;   // ????
        }
        OgTvEpisodeBean ogTvEpisodeBean = null;
        if(ogTvEpisode instanceof OgTvEpisodeBean) {
            ogTvEpisodeBean = (OgTvEpisodeBean) ogTvEpisode;
        } else if(ogTvEpisode instanceof OgTvEpisode) {
            ogTvEpisodeBean = new OgTvEpisodeBean(ogTvEpisode.getGuid(), ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate(), ogTvEpisode.getCreatedTime(), ogTvEpisode.getModifiedTime());
        } else {
            ogTvEpisodeBean = null;   // ????
        }
        OpenGraphMetaBean bean = new OpenGraphMetaBean(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfileBean, ogWebsiteBean, ogBlogBean, ogArticleBean, ogBookBean, ogVideoBean, ogMovieBean, ogTvShowBean, ogTvEpisodeBean);
        return createOpenGraphMeta(bean);
    }

    @Override
    public String createOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createOpenGraphMeta(openGraphMeta);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public OpenGraphMeta constructOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");
        OpenGraphMeta bean = decoratedService.constructOpenGraphMeta(openGraphMeta);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateOpenGraphMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgProfileBean ogProfileBean = null;
        if(ogProfile instanceof OgProfileBean) {
            ogProfileBean = (OgProfileBean) ogProfile;
        } else if(ogProfile instanceof OgProfile) {
            ogProfileBean = new OgProfileBean(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender(), ogProfile.getCreatedTime(), ogProfile.getModifiedTime());
        } else {
            ogProfileBean = null;   // ????
        }
        OgWebsiteBean ogWebsiteBean = null;
        if(ogWebsite instanceof OgWebsiteBean) {
            ogWebsiteBean = (OgWebsiteBean) ogWebsite;
        } else if(ogWebsite instanceof OgWebsite) {
            ogWebsiteBean = new OgWebsiteBean(ogWebsite.getGuid(), ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote(), ogWebsite.getCreatedTime(), ogWebsite.getModifiedTime());
        } else {
            ogWebsiteBean = null;   // ????
        }
        OgBlogBean ogBlogBean = null;
        if(ogBlog instanceof OgBlogBean) {
            ogBlogBean = (OgBlogBean) ogBlog;
        } else if(ogBlog instanceof OgBlog) {
            ogBlogBean = new OgBlogBean(ogBlog.getGuid(), ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote(), ogBlog.getCreatedTime(), ogBlog.getModifiedTime());
        } else {
            ogBlogBean = null;   // ????
        }
        OgArticleBean ogArticleBean = null;
        if(ogArticle instanceof OgArticleBean) {
            ogArticleBean = (OgArticleBean) ogArticle;
        } else if(ogArticle instanceof OgArticle) {
            ogArticleBean = new OgArticleBean(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate(), ogArticle.getCreatedTime(), ogArticle.getModifiedTime());
        } else {
            ogArticleBean = null;   // ????
        }
        OgBookBean ogBookBean = null;
        if(ogBook instanceof OgBookBean) {
            ogBookBean = (OgBookBean) ogBook;
        } else if(ogBook instanceof OgBook) {
            ogBookBean = new OgBookBean(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate(), ogBook.getCreatedTime(), ogBook.getModifiedTime());
        } else {
            ogBookBean = null;   // ????
        }
        OgVideoBean ogVideoBean = null;
        if(ogVideo instanceof OgVideoBean) {
            ogVideoBean = (OgVideoBean) ogVideo;
        } else if(ogVideo instanceof OgVideo) {
            ogVideoBean = new OgVideoBean(ogVideo.getGuid(), ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate(), ogVideo.getCreatedTime(), ogVideo.getModifiedTime());
        } else {
            ogVideoBean = null;   // ????
        }
        OgMovieBean ogMovieBean = null;
        if(ogMovie instanceof OgMovieBean) {
            ogMovieBean = (OgMovieBean) ogMovie;
        } else if(ogMovie instanceof OgMovie) {
            ogMovieBean = new OgMovieBean(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate(), ogMovie.getCreatedTime(), ogMovie.getModifiedTime());
        } else {
            ogMovieBean = null;   // ????
        }
        OgTvShowBean ogTvShowBean = null;
        if(ogTvShow instanceof OgTvShowBean) {
            ogTvShowBean = (OgTvShowBean) ogTvShow;
        } else if(ogTvShow instanceof OgTvShow) {
            ogTvShowBean = new OgTvShowBean(ogTvShow.getGuid(), ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate(), ogTvShow.getCreatedTime(), ogTvShow.getModifiedTime());
        } else {
            ogTvShowBean = null;   // ????
        }
        OgTvEpisodeBean ogTvEpisodeBean = null;
        if(ogTvEpisode instanceof OgTvEpisodeBean) {
            ogTvEpisodeBean = (OgTvEpisodeBean) ogTvEpisode;
        } else if(ogTvEpisode instanceof OgTvEpisode) {
            ogTvEpisodeBean = new OgTvEpisodeBean(ogTvEpisode.getGuid(), ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate(), ogTvEpisode.getCreatedTime(), ogTvEpisode.getModifiedTime());
        } else {
            ogTvEpisodeBean = null;   // ????
        }
        OpenGraphMetaBean bean = new OpenGraphMetaBean(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfileBean, ogWebsiteBean, ogBlogBean, ogArticleBean, ogBookBean, ogVideoBean, ogMovieBean, ogTvShowBean, ogTvEpisodeBean);
        return updateOpenGraphMeta(bean);
    }
        
    @Override
    public Boolean updateOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateOpenGraphMeta(openGraphMeta);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public OpenGraphMeta refreshOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");
        OpenGraphMeta bean = decoratedService.refreshOpenGraphMeta(openGraphMeta);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteOpenGraphMeta(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOpenGraphMeta(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOpenGraphMeta(openGraphMeta);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOpenGraphMetas(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteOpenGraphMetas(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOpenGraphMetas(List<OpenGraphMeta> openGraphMetas) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createOpenGraphMetas(openGraphMetas);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateOpenGraphMetas(List<OpenGraphMeta> openGraphMetas) throws BaseException
    //{
    //}

}
