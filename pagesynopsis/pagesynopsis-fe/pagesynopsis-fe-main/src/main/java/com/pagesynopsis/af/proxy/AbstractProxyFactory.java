package com.pagesynopsis.af.proxy;

public abstract class AbstractProxyFactory
{
    public abstract ApiConsumerServiceProxy getApiConsumerServiceProxy();
    public abstract UserServiceProxy getUserServiceProxy();
    public abstract RobotsTextFileServiceProxy getRobotsTextFileServiceProxy();
    public abstract RobotsTextRefreshServiceProxy getRobotsTextRefreshServiceProxy();
    public abstract OgProfileServiceProxy getOgProfileServiceProxy();
    public abstract OgWebsiteServiceProxy getOgWebsiteServiceProxy();
    public abstract OgBlogServiceProxy getOgBlogServiceProxy();
    public abstract OgArticleServiceProxy getOgArticleServiceProxy();
    public abstract OgBookServiceProxy getOgBookServiceProxy();
    public abstract OgVideoServiceProxy getOgVideoServiceProxy();
    public abstract OgMovieServiceProxy getOgMovieServiceProxy();
    public abstract OgTvShowServiceProxy getOgTvShowServiceProxy();
    public abstract OgTvEpisodeServiceProxy getOgTvEpisodeServiceProxy();
    public abstract TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy();
    public abstract TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy();
    public abstract TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy();
    public abstract TwitterAppCardServiceProxy getTwitterAppCardServiceProxy();
    public abstract TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy();
    public abstract TwitterProductCardServiceProxy getTwitterProductCardServiceProxy();
    public abstract FetchRequestServiceProxy getFetchRequestServiceProxy();
    public abstract PageInfoServiceProxy getPageInfoServiceProxy();
    public abstract PageFetchServiceProxy getPageFetchServiceProxy();
    public abstract LinkListServiceProxy getLinkListServiceProxy();
    public abstract ImageSetServiceProxy getImageSetServiceProxy();
    public abstract AudioSetServiceProxy getAudioSetServiceProxy();
    public abstract VideoSetServiceProxy getVideoSetServiceProxy();
    public abstract OpenGraphMetaServiceProxy getOpenGraphMetaServiceProxy();
    public abstract TwitterCardMetaServiceProxy getTwitterCardMetaServiceProxy();
    public abstract DomainInfoServiceProxy getDomainInfoServiceProxy();
    public abstract UrlRatingServiceProxy getUrlRatingServiceProxy();
    public abstract ServiceInfoServiceProxy getServiceInfoServiceProxy();
    public abstract FiveTenServiceProxy getFiveTenServiceProxy();
}
