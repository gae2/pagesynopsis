package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgImageStruct;
// import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;


public class OgImageStructProxyUtil
{
    private static final Logger log = Logger.getLogger(OgImageStructProxyUtil.class.getName());

    // Static methods only.
    private OgImageStructProxyUtil() {}

    public static OgImageStructBean convertServerOgImageStructBeanToAppBean(OgImageStruct serverBean)
    {
        OgImageStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new OgImageStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setUrl(serverBean.getUrl());
            bean.setSecureUrl(serverBean.getSecureUrl());
            bean.setType(serverBean.getType());
            bean.setWidth(serverBean.getWidth());
            bean.setHeight(serverBean.getHeight());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.OgImageStructBean convertAppOgImageStructBeanToServerBean(OgImageStruct appBean)
    {
        com.pagesynopsis.ws.bean.OgImageStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgImageStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setUrl(appBean.getUrl());
            bean.setSecureUrl(appBean.getSecureUrl());
            bean.setType(appBean.getType());
            bean.setWidth(appBean.getWidth());
            bean.setHeight(appBean.getHeight());
        }
        return bean;
    }

}
