package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.ImageStruct;
// import com.pagesynopsis.ws.bean.ImageStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;


public class ImageStructProxyUtil
{
    private static final Logger log = Logger.getLogger(ImageStructProxyUtil.class.getName());

    // Static methods only.
    private ImageStructProxyUtil() {}

    public static ImageStructBean convertServerImageStructBeanToAppBean(ImageStruct serverBean)
    {
        ImageStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new ImageStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setId(serverBean.getId());
            bean.setAlt(serverBean.getAlt());
            bean.setSrc(serverBean.getSrc());
            bean.setSrcUrl(serverBean.getSrcUrl());
            bean.setMediaType(serverBean.getMediaType());
            bean.setWidthAttr(serverBean.getWidthAttr());
            bean.setWidth(serverBean.getWidth());
            bean.setHeightAttr(serverBean.getHeightAttr());
            bean.setHeight(serverBean.getHeight());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.ImageStructBean convertAppImageStructBeanToServerBean(ImageStruct appBean)
    {
        com.pagesynopsis.ws.bean.ImageStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.ImageStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setId(appBean.getId());
            bean.setAlt(appBean.getAlt());
            bean.setSrc(appBean.getSrc());
            bean.setSrcUrl(appBean.getSrcUrl());
            bean.setMediaType(appBean.getMediaType());
            bean.setWidthAttr(appBean.getWidthAttr());
            bean.setWidth(appBean.getWidth());
            bean.setHeightAttr(appBean.getHeightAttr());
            bean.setHeight(appBean.getHeight());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
