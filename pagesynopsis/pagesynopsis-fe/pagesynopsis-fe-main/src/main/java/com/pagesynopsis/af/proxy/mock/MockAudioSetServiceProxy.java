package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.AudioSetBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.AudioSetService;
import com.pagesynopsis.af.proxy.AudioSetServiceProxy;


// MockAudioSetServiceProxy is a decorator.
// It can be used as a base class to mock AudioSetServiceProxy objects.
public abstract class MockAudioSetServiceProxy implements AudioSetServiceProxy
{
    private static final Logger log = Logger.getLogger(MockAudioSetServiceProxy.class.getName());

    // MockAudioSetServiceProxy uses the decorator design pattern.
    private AudioSetServiceProxy decoratedProxy;

    public MockAudioSetServiceProxy(AudioSetServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected AudioSetServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(AudioSetServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public AudioSet getAudioSet(String guid) throws BaseException
    {
        return decoratedProxy.getAudioSet(guid);
    }

    @Override
    public Object getAudioSet(String guid, String field) throws BaseException
    {
        return decoratedProxy.getAudioSet(guid, field);       
    }

    @Override
    public List<AudioSet> getAudioSets(List<String> guids) throws BaseException
    {
        return decoratedProxy.getAudioSets(guids);
    }

    @Override
    public List<AudioSet> getAllAudioSets() throws BaseException
    {
        return getAllAudioSets(null, null, null);
    }

    @Override
    public List<AudioSet> getAllAudioSets(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllAudioSets(ordering, offset, count);
        return getAllAudioSets(ordering, offset, count, null);
    }

    @Override
    public List<AudioSet> getAllAudioSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllAudioSets(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllAudioSetKeys(ordering, offset, count);
        return getAllAudioSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllAudioSetKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<AudioSet> findAudioSets(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAudioSets(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AudioSet> findAudioSets(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findAudioSets(filter, ordering, params, values, grouping, unique, offset, count);
        return findAudioSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AudioSet> findAudioSets(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findAudioSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAudioSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<AudioStruct> pageAudios) throws BaseException
    {
        return decoratedProxy.createAudioSet(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageAudios);
    }

    @Override
    public String createAudioSet(AudioSet audioSet) throws BaseException
    {
        return decoratedProxy.createAudioSet(audioSet);
    }

    @Override
    public Boolean updateAudioSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<AudioStruct> pageAudios) throws BaseException
    {
        return decoratedProxy.updateAudioSet(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageAudios);
    }

    @Override
    public Boolean updateAudioSet(AudioSet audioSet) throws BaseException
    {
        return decoratedProxy.updateAudioSet(audioSet);
    }

    @Override
    public Boolean deleteAudioSet(String guid) throws BaseException
    {
        return decoratedProxy.deleteAudioSet(guid);
    }

    @Override
    public Boolean deleteAudioSet(AudioSet audioSet) throws BaseException
    {
        String guid = audioSet.getGuid();
        return deleteAudioSet(guid);
    }

    @Override
    public Long deleteAudioSets(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteAudioSets(filter, params, values);
    }

}
