package com.pagesynopsis.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterGalleryCardService;


// TwitterGalleryCardMockService is a decorator.
// It can be used as a base class to mock TwitterGalleryCardService objects.
public abstract class TwitterGalleryCardMockService implements TwitterGalleryCardService
{
    private static final Logger log = Logger.getLogger(TwitterGalleryCardMockService.class.getName());

    // TwitterGalleryCardMockService uses the decorator design pattern.
    private TwitterGalleryCardService decoratedService;

    public TwitterGalleryCardMockService(TwitterGalleryCardService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected TwitterGalleryCardService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(TwitterGalleryCardService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // TwitterGalleryCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterGalleryCard getTwitterGalleryCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterGalleryCard(): guid = " + guid);
        TwitterGalleryCard bean = decoratedService.getTwitterGalleryCard(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterGalleryCard(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getTwitterGalleryCard(guid, field);
        return obj;
    }

    @Override
    public List<TwitterGalleryCard> getTwitterGalleryCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterGalleryCards()");
        List<TwitterGalleryCard> twitterGalleryCards = decoratedService.getTwitterGalleryCards(guids);
        log.finer("END");
        return twitterGalleryCards;
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards() throws BaseException
    {
        return getAllTwitterGalleryCards(null, null, null);
    }


    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterGalleryCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterGalleryCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<TwitterGalleryCard> twitterGalleryCards = decoratedService.getAllTwitterGalleryCards(ordering, offset, count, forwardCursor);
        log.finer("END");
        return twitterGalleryCards;
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterGalleryCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterGalleryCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllTwitterGalleryCardKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterGalleryCardMockService.findTwitterGalleryCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<TwitterGalleryCard> twitterGalleryCards = decoratedService.findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return twitterGalleryCards;
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterGalleryCardMockService.findTwitterGalleryCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterGalleryCardMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterGalleryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        TwitterGalleryCardBean bean = new TwitterGalleryCardBean(null, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
        return createTwitterGalleryCard(bean);
    }

    @Override
    public String createTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createTwitterGalleryCard(twitterGalleryCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TwitterGalleryCard constructTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");
        TwitterGalleryCard bean = decoratedService.constructTwitterGalleryCard(twitterGalleryCard);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterGalleryCardBean bean = new TwitterGalleryCardBean(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
        return updateTwitterGalleryCard(bean);
    }
        
    @Override
    public Boolean updateTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateTwitterGalleryCard(twitterGalleryCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TwitterGalleryCard refreshTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");
        TwitterGalleryCard bean = decoratedService.refreshTwitterGalleryCard(twitterGalleryCard);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTwitterGalleryCard(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterGalleryCard(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterGalleryCard(twitterGalleryCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteTwitterGalleryCards(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createTwitterGalleryCards(twitterGalleryCards);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException
    //{
    //}

}
