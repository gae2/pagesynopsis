package com.pagesynopsis.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.DomainInfoBean;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.DomainInfoService;


// DomainInfoMockService is a decorator.
// It can be used as a base class to mock DomainInfoService objects.
public abstract class DomainInfoMockService implements DomainInfoService
{
    private static final Logger log = Logger.getLogger(DomainInfoMockService.class.getName());

    // DomainInfoMockService uses the decorator design pattern.
    private DomainInfoService decoratedService;

    public DomainInfoMockService(DomainInfoService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected DomainInfoService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(DomainInfoService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // DomainInfo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DomainInfo getDomainInfo(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDomainInfo(): guid = " + guid);
        DomainInfo bean = decoratedService.getDomainInfo(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getDomainInfo(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getDomainInfo(guid, field);
        return obj;
    }

    @Override
    public List<DomainInfo> getDomainInfos(List<String> guids) throws BaseException
    {
        log.fine("getDomainInfos()");
        List<DomainInfo> domainInfos = decoratedService.getDomainInfos(guids);
        log.finer("END");
        return domainInfos;
    }

    @Override
    public List<DomainInfo> getAllDomainInfos() throws BaseException
    {
        return getAllDomainInfos(null, null, null);
    }


    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDomainInfos(ordering, offset, count, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDomainInfos(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<DomainInfo> domainInfos = decoratedService.getAllDomainInfos(ordering, offset, count, forwardCursor);
        log.finer("END");
        return domainInfos;
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDomainInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDomainInfoKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllDomainInfoKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDomainInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DomainInfoMockService.findDomainInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<DomainInfo> domainInfos = decoratedService.findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return domainInfos;
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DomainInfoMockService.findDomainInfoKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DomainInfoMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createDomainInfo(String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        DomainInfoBean bean = new DomainInfoBean(null, domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
        return createDomainInfo(bean);
    }

    @Override
    public String createDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createDomainInfo(domainInfo);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public DomainInfo constructDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");
        DomainInfo bean = decoratedService.constructDomainInfo(domainInfo);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateDomainInfo(String guid, String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        DomainInfoBean bean = new DomainInfoBean(guid, domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
        return updateDomainInfo(bean);
    }
        
    @Override
    public Boolean updateDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateDomainInfo(domainInfo);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public DomainInfo refreshDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");
        DomainInfo bean = decoratedService.refreshDomainInfo(domainInfo);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDomainInfo(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDomainInfo(domainInfo);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteDomainInfos(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDomainInfos(List<DomainInfo> domainInfos) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createDomainInfos(domainInfos);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateDomainInfos(List<DomainInfo> domainInfos) throws BaseException
    //{
    //}

}
