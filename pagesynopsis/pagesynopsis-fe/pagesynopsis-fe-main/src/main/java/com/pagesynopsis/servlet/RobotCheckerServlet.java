package com.pagesynopsis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pagesynopsis.app.robotcheck.RobotChecker;
import com.pagesynopsis.fe.bean.FiveTenJsBean;
import com.pagesynopsis.wa.service.FiveTenWebService;
import com.pagesynopsis.ws.core.StatusCode;


// Mainly, for ajax calls....
public class RobotCheckerServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotCheckerServlet.class.getName());

    // temporary
    private static final String QUERY_PARAM_TARGET_URL = "targetUrl";
    private static final String QUERY_PARAM_ROBOT = "robot";

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        String targetUrl = null;
        String[] targetUrls = req.getParameterValues(QUERY_PARAM_TARGET_URL);
        if(targetUrls != null && targetUrls.length > 0) {
            // TBD: Support multiple target urls???
            
            // TBD: the query param is already url decoded!!!
            // ...
            // TBD: comment this out on the next deployment
            // ....
            targetUrl = URLDecoder.decode(targetUrls[0], "UTF-8");  // ?????
            // TBD:
            // "canonicalize" the targetUrl???
            // ....
        } else {
            // error...
            log.warning("Required arg, targetUrl, is missing");
            //throw new ResourceNotFoundRsException("");
            //throw new BadRequestRsException("Query parameter is missing, " + QUERY_PARAM_TARGET_URL);
            //throw new ServletException("Query parameter is missing, " + QUERY_PARAM_TARGET_URL);
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        if(log.isLoggable(Level.FINE)) log.fine("targetUrl = " + targetUrl);

        String robot = null;
        String[] robots = req.getParameterValues(QUERY_PARAM_ROBOT);
        if(robots != null && robots.length > 0) {
            robot = URLDecoder.decode(robots[0], "UTF-8");  // ?????
        }
        if(log.isLoggable(Level.FINE)) log.fine("robot = " + robot);

        String permission = RobotChecker.getInstance().isRobotAllowed(targetUrl, robot);
        if(log.isLoggable(Level.INFO)) log.info("permission = " + permission + " for targetUrl = " + targetUrl + "; robot = " + robot);

        if(permission != null) {
            resp.setContentType("text/plain;charset=UTF-8");  // ??? 
            PrintWriter writer = resp.getWriter();
            writer.print(permission);
            resp.setStatus(StatusCode.OK);
        } else {
            // Arbitrary. Can this happen???
            resp.setStatus(StatusCode.NOT_FOUND);
        }
    }

}
