package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.af.bean.DomainInfoBean;
import com.pagesynopsis.ws.service.DomainInfoService;
import com.pagesynopsis.af.proxy.DomainInfoServiceProxy;


// MockDomainInfoServiceProxy is a decorator.
// It can be used as a base class to mock DomainInfoServiceProxy objects.
public abstract class MockDomainInfoServiceProxy implements DomainInfoServiceProxy
{
    private static final Logger log = Logger.getLogger(MockDomainInfoServiceProxy.class.getName());

    // MockDomainInfoServiceProxy uses the decorator design pattern.
    private DomainInfoServiceProxy decoratedProxy;

    public MockDomainInfoServiceProxy(DomainInfoServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected DomainInfoServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(DomainInfoServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public DomainInfo getDomainInfo(String guid) throws BaseException
    {
        return decoratedProxy.getDomainInfo(guid);
    }

    @Override
    public Object getDomainInfo(String guid, String field) throws BaseException
    {
        return decoratedProxy.getDomainInfo(guid, field);       
    }

    @Override
    public List<DomainInfo> getDomainInfos(List<String> guids) throws BaseException
    {
        return decoratedProxy.getDomainInfos(guids);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos() throws BaseException
    {
        return getAllDomainInfos(null, null, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllDomainInfos(ordering, offset, count);
        return getAllDomainInfos(ordering, offset, count, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDomainInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllDomainInfoKeys(ordering, offset, count);
        return getAllDomainInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDomainInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDomainInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count);
        return findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDomainInfo(String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return decoratedProxy.createDomainInfo(domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public String createDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return decoratedProxy.createDomainInfo(domainInfo);
    }

    @Override
    public Boolean updateDomainInfo(String guid, String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return decoratedProxy.updateDomainInfo(guid, domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public Boolean updateDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return decoratedProxy.updateDomainInfo(domainInfo);
    }

    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        return decoratedProxy.deleteDomainInfo(guid);
    }

    @Override
    public Boolean deleteDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        String guid = domainInfo.getGuid();
        return deleteDomainInfo(guid);
    }

    @Override
    public Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteDomainInfos(filter, params, values);
    }

}
