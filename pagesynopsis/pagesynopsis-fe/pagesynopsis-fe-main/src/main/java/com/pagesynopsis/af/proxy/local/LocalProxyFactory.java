package com.pagesynopsis.af.proxy.local;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.ApiConsumerServiceProxy;
import com.pagesynopsis.af.proxy.UserServiceProxy;
import com.pagesynopsis.af.proxy.RobotsTextFileServiceProxy;
import com.pagesynopsis.af.proxy.RobotsTextRefreshServiceProxy;
import com.pagesynopsis.af.proxy.OgProfileServiceProxy;
import com.pagesynopsis.af.proxy.OgWebsiteServiceProxy;
import com.pagesynopsis.af.proxy.OgBlogServiceProxy;
import com.pagesynopsis.af.proxy.OgArticleServiceProxy;
import com.pagesynopsis.af.proxy.OgBookServiceProxy;
import com.pagesynopsis.af.proxy.OgVideoServiceProxy;
import com.pagesynopsis.af.proxy.OgMovieServiceProxy;
import com.pagesynopsis.af.proxy.OgTvShowServiceProxy;
import com.pagesynopsis.af.proxy.OgTvEpisodeServiceProxy;
import com.pagesynopsis.af.proxy.TwitterSummaryCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterPhotoCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterGalleryCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterAppCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterPlayerCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterProductCardServiceProxy;
import com.pagesynopsis.af.proxy.FetchRequestServiceProxy;
import com.pagesynopsis.af.proxy.PageInfoServiceProxy;
import com.pagesynopsis.af.proxy.PageFetchServiceProxy;
import com.pagesynopsis.af.proxy.LinkListServiceProxy;
import com.pagesynopsis.af.proxy.ImageSetServiceProxy;
import com.pagesynopsis.af.proxy.AudioSetServiceProxy;
import com.pagesynopsis.af.proxy.VideoSetServiceProxy;
import com.pagesynopsis.af.proxy.OpenGraphMetaServiceProxy;
import com.pagesynopsis.af.proxy.TwitterCardMetaServiceProxy;
import com.pagesynopsis.af.proxy.DomainInfoServiceProxy;
import com.pagesynopsis.af.proxy.UrlRatingServiceProxy;
import com.pagesynopsis.af.proxy.ServiceInfoServiceProxy;
import com.pagesynopsis.af.proxy.FiveTenServiceProxy;
import com.pagesynopsis.ws.service.ApiConsumerService;
import com.pagesynopsis.ws.service.UserService;
import com.pagesynopsis.ws.service.RobotsTextFileService;
import com.pagesynopsis.ws.service.RobotsTextRefreshService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.FetchRequestService;
import com.pagesynopsis.ws.service.PageInfoService;
import com.pagesynopsis.ws.service.PageFetchService;
import com.pagesynopsis.ws.service.LinkListService;
import com.pagesynopsis.ws.service.ImageSetService;
import com.pagesynopsis.ws.service.AudioSetService;
import com.pagesynopsis.ws.service.VideoSetService;
import com.pagesynopsis.ws.service.OpenGraphMetaService;
import com.pagesynopsis.ws.service.TwitterCardMetaService;
import com.pagesynopsis.ws.service.DomainInfoService;
import com.pagesynopsis.ws.service.UrlRatingService;
import com.pagesynopsis.ws.service.ServiceInfoService;
import com.pagesynopsis.ws.service.FiveTenService;
import com.pagesynopsis.ws.service.impl.ApiConsumerServiceImpl;
import com.pagesynopsis.ws.service.impl.UserServiceImpl;
import com.pagesynopsis.ws.service.impl.RobotsTextFileServiceImpl;
import com.pagesynopsis.ws.service.impl.RobotsTextRefreshServiceImpl;
import com.pagesynopsis.ws.service.impl.OgProfileServiceImpl;
import com.pagesynopsis.ws.service.impl.OgWebsiteServiceImpl;
import com.pagesynopsis.ws.service.impl.OgBlogServiceImpl;
import com.pagesynopsis.ws.service.impl.OgArticleServiceImpl;
import com.pagesynopsis.ws.service.impl.OgBookServiceImpl;
import com.pagesynopsis.ws.service.impl.OgVideoServiceImpl;
import com.pagesynopsis.ws.service.impl.OgMovieServiceImpl;
import com.pagesynopsis.ws.service.impl.OgTvShowServiceImpl;
import com.pagesynopsis.ws.service.impl.OgTvEpisodeServiceImpl;
import com.pagesynopsis.ws.service.impl.TwitterSummaryCardServiceImpl;
import com.pagesynopsis.ws.service.impl.TwitterPhotoCardServiceImpl;
import com.pagesynopsis.ws.service.impl.TwitterGalleryCardServiceImpl;
import com.pagesynopsis.ws.service.impl.TwitterAppCardServiceImpl;
import com.pagesynopsis.ws.service.impl.TwitterPlayerCardServiceImpl;
import com.pagesynopsis.ws.service.impl.TwitterProductCardServiceImpl;
import com.pagesynopsis.ws.service.impl.FetchRequestServiceImpl;
import com.pagesynopsis.ws.service.impl.PageInfoServiceImpl;
import com.pagesynopsis.ws.service.impl.PageFetchServiceImpl;
import com.pagesynopsis.ws.service.impl.LinkListServiceImpl;
import com.pagesynopsis.ws.service.impl.ImageSetServiceImpl;
import com.pagesynopsis.ws.service.impl.AudioSetServiceImpl;
import com.pagesynopsis.ws.service.impl.VideoSetServiceImpl;
import com.pagesynopsis.ws.service.impl.OpenGraphMetaServiceImpl;
import com.pagesynopsis.ws.service.impl.TwitterCardMetaServiceImpl;
import com.pagesynopsis.ws.service.impl.DomainInfoServiceImpl;
import com.pagesynopsis.ws.service.impl.UrlRatingServiceImpl;
import com.pagesynopsis.ws.service.impl.ServiceInfoServiceImpl;
import com.pagesynopsis.ws.service.impl.FiveTenServiceImpl;


// TBD: How to best inject the ws service instances?
public class LocalProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(LocalProxyFactory.class.getName());

    // Lazy initialization.
    private ApiConsumerServiceProxy apiConsumerServiceProxy = null;
    private UserServiceProxy userServiceProxy = null;
    private RobotsTextFileServiceProxy robotsTextFileServiceProxy = null;
    private RobotsTextRefreshServiceProxy robotsTextRefreshServiceProxy = null;
    private OgProfileServiceProxy ogProfileServiceProxy = null;
    private OgWebsiteServiceProxy ogWebsiteServiceProxy = null;
    private OgBlogServiceProxy ogBlogServiceProxy = null;
    private OgArticleServiceProxy ogArticleServiceProxy = null;
    private OgBookServiceProxy ogBookServiceProxy = null;
    private OgVideoServiceProxy ogVideoServiceProxy = null;
    private OgMovieServiceProxy ogMovieServiceProxy = null;
    private OgTvShowServiceProxy ogTvShowServiceProxy = null;
    private OgTvEpisodeServiceProxy ogTvEpisodeServiceProxy = null;
    private TwitterSummaryCardServiceProxy twitterSummaryCardServiceProxy = null;
    private TwitterPhotoCardServiceProxy twitterPhotoCardServiceProxy = null;
    private TwitterGalleryCardServiceProxy twitterGalleryCardServiceProxy = null;
    private TwitterAppCardServiceProxy twitterAppCardServiceProxy = null;
    private TwitterPlayerCardServiceProxy twitterPlayerCardServiceProxy = null;
    private TwitterProductCardServiceProxy twitterProductCardServiceProxy = null;
    private FetchRequestServiceProxy fetchRequestServiceProxy = null;
    private PageInfoServiceProxy pageInfoServiceProxy = null;
    private PageFetchServiceProxy pageFetchServiceProxy = null;
    private LinkListServiceProxy linkListServiceProxy = null;
    private ImageSetServiceProxy imageSetServiceProxy = null;
    private AudioSetServiceProxy audioSetServiceProxy = null;
    private VideoSetServiceProxy videoSetServiceProxy = null;
    private OpenGraphMetaServiceProxy openGraphMetaServiceProxy = null;
    private TwitterCardMetaServiceProxy twitterCardMetaServiceProxy = null;
    private DomainInfoServiceProxy domainInfoServiceProxy = null;
    private UrlRatingServiceProxy urlRatingServiceProxy = null;
    private ServiceInfoServiceProxy serviceInfoServiceProxy = null;
    private FiveTenServiceProxy fiveTenServiceProxy = null;


    private LocalProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class LocalProxyFactoryHolder
    {
        private static final LocalProxyFactory INSTANCE = new LocalProxyFactory();
    }

    // Singleton method
    public static LocalProxyFactory getInstance()
    {
        return LocalProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        if(apiConsumerServiceProxy == null) {
            apiConsumerServiceProxy = new LocalApiConsumerServiceProxy();
            ApiConsumerService apiConsumerService = new ApiConsumerServiceImpl();
            ((LocalApiConsumerServiceProxy) apiConsumerServiceProxy).setApiConsumerService(apiConsumerService);
        }
        return apiConsumerServiceProxy;
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        if(userServiceProxy == null) {
            userServiceProxy = new LocalUserServiceProxy();
            UserService userService = new UserServiceImpl();
            ((LocalUserServiceProxy) userServiceProxy).setUserService(userService);
        }
        return userServiceProxy;
    }

    @Override
    public RobotsTextFileServiceProxy getRobotsTextFileServiceProxy()
    {
        if(robotsTextFileServiceProxy == null) {
            robotsTextFileServiceProxy = new LocalRobotsTextFileServiceProxy();
            RobotsTextFileService robotsTextFileService = new RobotsTextFileServiceImpl();
            ((LocalRobotsTextFileServiceProxy) robotsTextFileServiceProxy).setRobotsTextFileService(robotsTextFileService);
        }
        return robotsTextFileServiceProxy;
    }

    @Override
    public RobotsTextRefreshServiceProxy getRobotsTextRefreshServiceProxy()
    {
        if(robotsTextRefreshServiceProxy == null) {
            robotsTextRefreshServiceProxy = new LocalRobotsTextRefreshServiceProxy();
            RobotsTextRefreshService robotsTextRefreshService = new RobotsTextRefreshServiceImpl();
            ((LocalRobotsTextRefreshServiceProxy) robotsTextRefreshServiceProxy).setRobotsTextRefreshService(robotsTextRefreshService);
        }
        return robotsTextRefreshServiceProxy;
    }

    @Override
    public OgProfileServiceProxy getOgProfileServiceProxy()
    {
        if(ogProfileServiceProxy == null) {
            ogProfileServiceProxy = new LocalOgProfileServiceProxy();
            OgProfileService ogProfileService = new OgProfileServiceImpl();
            ((LocalOgProfileServiceProxy) ogProfileServiceProxy).setOgProfileService(ogProfileService);
        }
        return ogProfileServiceProxy;
    }

    @Override
    public OgWebsiteServiceProxy getOgWebsiteServiceProxy()
    {
        if(ogWebsiteServiceProxy == null) {
            ogWebsiteServiceProxy = new LocalOgWebsiteServiceProxy();
            OgWebsiteService ogWebsiteService = new OgWebsiteServiceImpl();
            ((LocalOgWebsiteServiceProxy) ogWebsiteServiceProxy).setOgWebsiteService(ogWebsiteService);
        }
        return ogWebsiteServiceProxy;
    }

    @Override
    public OgBlogServiceProxy getOgBlogServiceProxy()
    {
        if(ogBlogServiceProxy == null) {
            ogBlogServiceProxy = new LocalOgBlogServiceProxy();
            OgBlogService ogBlogService = new OgBlogServiceImpl();
            ((LocalOgBlogServiceProxy) ogBlogServiceProxy).setOgBlogService(ogBlogService);
        }
        return ogBlogServiceProxy;
    }

    @Override
    public OgArticleServiceProxy getOgArticleServiceProxy()
    {
        if(ogArticleServiceProxy == null) {
            ogArticleServiceProxy = new LocalOgArticleServiceProxy();
            OgArticleService ogArticleService = new OgArticleServiceImpl();
            ((LocalOgArticleServiceProxy) ogArticleServiceProxy).setOgArticleService(ogArticleService);
        }
        return ogArticleServiceProxy;
    }

    @Override
    public OgBookServiceProxy getOgBookServiceProxy()
    {
        if(ogBookServiceProxy == null) {
            ogBookServiceProxy = new LocalOgBookServiceProxy();
            OgBookService ogBookService = new OgBookServiceImpl();
            ((LocalOgBookServiceProxy) ogBookServiceProxy).setOgBookService(ogBookService);
        }
        return ogBookServiceProxy;
    }

    @Override
    public OgVideoServiceProxy getOgVideoServiceProxy()
    {
        if(ogVideoServiceProxy == null) {
            ogVideoServiceProxy = new LocalOgVideoServiceProxy();
            OgVideoService ogVideoService = new OgVideoServiceImpl();
            ((LocalOgVideoServiceProxy) ogVideoServiceProxy).setOgVideoService(ogVideoService);
        }
        return ogVideoServiceProxy;
    }

    @Override
    public OgMovieServiceProxy getOgMovieServiceProxy()
    {
        if(ogMovieServiceProxy == null) {
            ogMovieServiceProxy = new LocalOgMovieServiceProxy();
            OgMovieService ogMovieService = new OgMovieServiceImpl();
            ((LocalOgMovieServiceProxy) ogMovieServiceProxy).setOgMovieService(ogMovieService);
        }
        return ogMovieServiceProxy;
    }

    @Override
    public OgTvShowServiceProxy getOgTvShowServiceProxy()
    {
        if(ogTvShowServiceProxy == null) {
            ogTvShowServiceProxy = new LocalOgTvShowServiceProxy();
            OgTvShowService ogTvShowService = new OgTvShowServiceImpl();
            ((LocalOgTvShowServiceProxy) ogTvShowServiceProxy).setOgTvShowService(ogTvShowService);
        }
        return ogTvShowServiceProxy;
    }

    @Override
    public OgTvEpisodeServiceProxy getOgTvEpisodeServiceProxy()
    {
        if(ogTvEpisodeServiceProxy == null) {
            ogTvEpisodeServiceProxy = new LocalOgTvEpisodeServiceProxy();
            OgTvEpisodeService ogTvEpisodeService = new OgTvEpisodeServiceImpl();
            ((LocalOgTvEpisodeServiceProxy) ogTvEpisodeServiceProxy).setOgTvEpisodeService(ogTvEpisodeService);
        }
        return ogTvEpisodeServiceProxy;
    }

    @Override
    public TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy()
    {
        if(twitterSummaryCardServiceProxy == null) {
            twitterSummaryCardServiceProxy = new LocalTwitterSummaryCardServiceProxy();
            TwitterSummaryCardService twitterSummaryCardService = new TwitterSummaryCardServiceImpl();
            ((LocalTwitterSummaryCardServiceProxy) twitterSummaryCardServiceProxy).setTwitterSummaryCardService(twitterSummaryCardService);
        }
        return twitterSummaryCardServiceProxy;
    }

    @Override
    public TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy()
    {
        if(twitterPhotoCardServiceProxy == null) {
            twitterPhotoCardServiceProxy = new LocalTwitterPhotoCardServiceProxy();
            TwitterPhotoCardService twitterPhotoCardService = new TwitterPhotoCardServiceImpl();
            ((LocalTwitterPhotoCardServiceProxy) twitterPhotoCardServiceProxy).setTwitterPhotoCardService(twitterPhotoCardService);
        }
        return twitterPhotoCardServiceProxy;
    }

    @Override
    public TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy()
    {
        if(twitterGalleryCardServiceProxy == null) {
            twitterGalleryCardServiceProxy = new LocalTwitterGalleryCardServiceProxy();
            TwitterGalleryCardService twitterGalleryCardService = new TwitterGalleryCardServiceImpl();
            ((LocalTwitterGalleryCardServiceProxy) twitterGalleryCardServiceProxy).setTwitterGalleryCardService(twitterGalleryCardService);
        }
        return twitterGalleryCardServiceProxy;
    }

    @Override
    public TwitterAppCardServiceProxy getTwitterAppCardServiceProxy()
    {
        if(twitterAppCardServiceProxy == null) {
            twitterAppCardServiceProxy = new LocalTwitterAppCardServiceProxy();
            TwitterAppCardService twitterAppCardService = new TwitterAppCardServiceImpl();
            ((LocalTwitterAppCardServiceProxy) twitterAppCardServiceProxy).setTwitterAppCardService(twitterAppCardService);
        }
        return twitterAppCardServiceProxy;
    }

    @Override
    public TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy()
    {
        if(twitterPlayerCardServiceProxy == null) {
            twitterPlayerCardServiceProxy = new LocalTwitterPlayerCardServiceProxy();
            TwitterPlayerCardService twitterPlayerCardService = new TwitterPlayerCardServiceImpl();
            ((LocalTwitterPlayerCardServiceProxy) twitterPlayerCardServiceProxy).setTwitterPlayerCardService(twitterPlayerCardService);
        }
        return twitterPlayerCardServiceProxy;
    }

    @Override
    public TwitterProductCardServiceProxy getTwitterProductCardServiceProxy()
    {
        if(twitterProductCardServiceProxy == null) {
            twitterProductCardServiceProxy = new LocalTwitterProductCardServiceProxy();
            TwitterProductCardService twitterProductCardService = new TwitterProductCardServiceImpl();
            ((LocalTwitterProductCardServiceProxy) twitterProductCardServiceProxy).setTwitterProductCardService(twitterProductCardService);
        }
        return twitterProductCardServiceProxy;
    }

    @Override
    public FetchRequestServiceProxy getFetchRequestServiceProxy()
    {
        if(fetchRequestServiceProxy == null) {
            fetchRequestServiceProxy = new LocalFetchRequestServiceProxy();
            FetchRequestService fetchRequestService = new FetchRequestServiceImpl();
            ((LocalFetchRequestServiceProxy) fetchRequestServiceProxy).setFetchRequestService(fetchRequestService);
        }
        return fetchRequestServiceProxy;
    }

    @Override
    public PageInfoServiceProxy getPageInfoServiceProxy()
    {
        if(pageInfoServiceProxy == null) {
            pageInfoServiceProxy = new LocalPageInfoServiceProxy();
            PageInfoService pageInfoService = new PageInfoServiceImpl();
            ((LocalPageInfoServiceProxy) pageInfoServiceProxy).setPageInfoService(pageInfoService);
        }
        return pageInfoServiceProxy;
    }

    @Override
    public PageFetchServiceProxy getPageFetchServiceProxy()
    {
        if(pageFetchServiceProxy == null) {
            pageFetchServiceProxy = new LocalPageFetchServiceProxy();
            PageFetchService pageFetchService = new PageFetchServiceImpl();
            ((LocalPageFetchServiceProxy) pageFetchServiceProxy).setPageFetchService(pageFetchService);
        }
        return pageFetchServiceProxy;
    }

    @Override
    public LinkListServiceProxy getLinkListServiceProxy()
    {
        if(linkListServiceProxy == null) {
            linkListServiceProxy = new LocalLinkListServiceProxy();
            LinkListService linkListService = new LinkListServiceImpl();
            ((LocalLinkListServiceProxy) linkListServiceProxy).setLinkListService(linkListService);
        }
        return linkListServiceProxy;
    }

    @Override
    public ImageSetServiceProxy getImageSetServiceProxy()
    {
        if(imageSetServiceProxy == null) {
            imageSetServiceProxy = new LocalImageSetServiceProxy();
            ImageSetService imageSetService = new ImageSetServiceImpl();
            ((LocalImageSetServiceProxy) imageSetServiceProxy).setImageSetService(imageSetService);
        }
        return imageSetServiceProxy;
    }

    @Override
    public AudioSetServiceProxy getAudioSetServiceProxy()
    {
        if(audioSetServiceProxy == null) {
            audioSetServiceProxy = new LocalAudioSetServiceProxy();
            AudioSetService audioSetService = new AudioSetServiceImpl();
            ((LocalAudioSetServiceProxy) audioSetServiceProxy).setAudioSetService(audioSetService);
        }
        return audioSetServiceProxy;
    }

    @Override
    public VideoSetServiceProxy getVideoSetServiceProxy()
    {
        if(videoSetServiceProxy == null) {
            videoSetServiceProxy = new LocalVideoSetServiceProxy();
            VideoSetService videoSetService = new VideoSetServiceImpl();
            ((LocalVideoSetServiceProxy) videoSetServiceProxy).setVideoSetService(videoSetService);
        }
        return videoSetServiceProxy;
    }

    @Override
    public OpenGraphMetaServiceProxy getOpenGraphMetaServiceProxy()
    {
        if(openGraphMetaServiceProxy == null) {
            openGraphMetaServiceProxy = new LocalOpenGraphMetaServiceProxy();
            OpenGraphMetaService openGraphMetaService = new OpenGraphMetaServiceImpl();
            ((LocalOpenGraphMetaServiceProxy) openGraphMetaServiceProxy).setOpenGraphMetaService(openGraphMetaService);
        }
        return openGraphMetaServiceProxy;
    }

    @Override
    public TwitterCardMetaServiceProxy getTwitterCardMetaServiceProxy()
    {
        if(twitterCardMetaServiceProxy == null) {
            twitterCardMetaServiceProxy = new LocalTwitterCardMetaServiceProxy();
            TwitterCardMetaService twitterCardMetaService = new TwitterCardMetaServiceImpl();
            ((LocalTwitterCardMetaServiceProxy) twitterCardMetaServiceProxy).setTwitterCardMetaService(twitterCardMetaService);
        }
        return twitterCardMetaServiceProxy;
    }

    @Override
    public DomainInfoServiceProxy getDomainInfoServiceProxy()
    {
        if(domainInfoServiceProxy == null) {
            domainInfoServiceProxy = new LocalDomainInfoServiceProxy();
            DomainInfoService domainInfoService = new DomainInfoServiceImpl();
            ((LocalDomainInfoServiceProxy) domainInfoServiceProxy).setDomainInfoService(domainInfoService);
        }
        return domainInfoServiceProxy;
    }

    @Override
    public UrlRatingServiceProxy getUrlRatingServiceProxy()
    {
        if(urlRatingServiceProxy == null) {
            urlRatingServiceProxy = new LocalUrlRatingServiceProxy();
            UrlRatingService urlRatingService = new UrlRatingServiceImpl();
            ((LocalUrlRatingServiceProxy) urlRatingServiceProxy).setUrlRatingService(urlRatingService);
        }
        return urlRatingServiceProxy;
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoServiceProxy == null) {
            serviceInfoServiceProxy = new LocalServiceInfoServiceProxy();
            ServiceInfoService serviceInfoService = new ServiceInfoServiceImpl();
            ((LocalServiceInfoServiceProxy) serviceInfoServiceProxy).setServiceInfoService(serviceInfoService);
        }
        return serviceInfoServiceProxy;
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenServiceProxy == null) {
            fiveTenServiceProxy = new LocalFiveTenServiceProxy();
            FiveTenService fiveTenService = new FiveTenServiceImpl();
            ((LocalFiveTenServiceProxy) fiveTenServiceProxy).setFiveTenService(fiveTenService);
        }
        return fiveTenServiceProxy;
    }

}
