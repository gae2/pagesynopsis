package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.fe.bean.MediaSourceStructJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;


public class AudioStructWebUtil
{
    private static final Logger log = Logger.getLogger(AudioStructWebUtil.class.getName());

    // Static methods only.
    private AudioStructWebUtil() {}
    

    public static AudioStructJsBean convertAudioStructToJsBean(AudioStruct audioStruct)
    {
        AudioStructJsBean jsBean = null;
        if(audioStruct != null) {
            jsBean = new AudioStructJsBean();
            jsBean.setUuid(audioStruct.getUuid());
            jsBean.setId(audioStruct.getId());
            jsBean.setControls(audioStruct.getControls());
            jsBean.setAutoplayEnabled(audioStruct.isAutoplayEnabled());
            jsBean.setLoopEnabled(audioStruct.isLoopEnabled());
            jsBean.setPreloadEnabled(audioStruct.isPreloadEnabled());
            jsBean.setRemark(audioStruct.getRemark());
            jsBean.setSource(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(audioStruct.getSource()));
            jsBean.setSource1(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(audioStruct.getSource1()));
            jsBean.setSource2(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(audioStruct.getSource2()));
            jsBean.setSource3(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(audioStruct.getSource3()));
            jsBean.setSource4(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(audioStruct.getSource4()));
            jsBean.setSource5(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(audioStruct.getSource5()));
            jsBean.setNote(audioStruct.getNote());
        }
        return jsBean;
    }

    public static AudioStruct convertAudioStructJsBeanToBean(AudioStructJsBean jsBean)
    {
        AudioStructBean audioStruct = null;
        if(jsBean != null) {
            audioStruct = new AudioStructBean();
            audioStruct.setUuid(jsBean.getUuid());
            audioStruct.setId(jsBean.getId());
            audioStruct.setControls(jsBean.getControls());
            audioStruct.setAutoplayEnabled(jsBean.isAutoplayEnabled());
            audioStruct.setLoopEnabled(jsBean.isLoopEnabled());
            audioStruct.setPreloadEnabled(jsBean.isPreloadEnabled());
            audioStruct.setRemark(jsBean.getRemark());
            audioStruct.setSource(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource()));

            audioStruct.setSource1(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource1()));

            audioStruct.setSource2(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource2()));

            audioStruct.setSource3(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource3()));

            audioStruct.setSource4(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource4()));

            audioStruct.setSource5(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource5()));

            audioStruct.setNote(jsBean.getNote());
        }
        return audioStruct;
    }

}
