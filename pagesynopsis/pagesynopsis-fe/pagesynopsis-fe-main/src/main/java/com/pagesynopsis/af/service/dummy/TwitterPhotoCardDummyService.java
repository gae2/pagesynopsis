package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterPhotoCardService;


// The primary purpose of TwitterPhotoCardDummyService is to fake the service api, TwitterPhotoCardService.
// It has no real implementation.
public class TwitterPhotoCardDummyService implements TwitterPhotoCardService
{
    private static final Logger log = Logger.getLogger(TwitterPhotoCardDummyService.class.getName());

    public TwitterPhotoCardDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterPhotoCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterPhotoCard getTwitterPhotoCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterPhotoCard(): guid = " + guid);
        return null;
    }

    @Override
    public Object getTwitterPhotoCard(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterPhotoCard(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<TwitterPhotoCard> getTwitterPhotoCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterPhotoCards()");
        return null;
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards() throws BaseException
    {
        return getAllTwitterPhotoCards(null, null, null);
    }


    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPhotoCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterPhotoCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPhotoCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterPhotoCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPhotoCardDummyService.findTwitterPhotoCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPhotoCardDummyService.findTwitterPhotoCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPhotoCardDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createTwitterPhotoCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        log.finer("createTwitterPhotoCard()");
        return null;
    }

    @Override
    public String createTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("createTwitterPhotoCard()");
        return null;
    }

    @Override
    public TwitterPhotoCard constructTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("constructTwitterPhotoCard()");
        return null;
    }

    @Override
    public Boolean updateTwitterPhotoCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        log.finer("updateTwitterPhotoCard()");
        return null;
    }
        
    @Override
    public Boolean updateTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("updateTwitterPhotoCard()");
        return null;
    }

    @Override
    public TwitterPhotoCard refreshTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("refreshTwitterPhotoCard()");
        return null;
    }

    @Override
    public Boolean deleteTwitterPhotoCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTwitterPhotoCard(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard) throws BaseException
    {
        log.finer("deleteTwitterPhotoCard()");
        return null;
    }

    // TBD
    @Override
    public Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTwitterPhotoCard(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterPhotoCards(List<TwitterPhotoCard> twitterPhotoCards) throws BaseException
    {
        log.finer("createTwitterPhotoCards()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterPhotoCards(List<TwitterPhotoCard> twitterPhotoCards) throws BaseException
    //{
    //}

}
