package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
// import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.proxy.OgArticleServiceProxy;


public class LocalOgArticleServiceProxy extends BaseLocalServiceProxy implements OgArticleServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgArticleServiceProxy.class.getName());

    public LocalOgArticleServiceProxy()
    {
    }

    @Override
    public OgArticle getOgArticle(String guid) throws BaseException
    {
        OgArticle serverBean = getOgArticleService().getOgArticle(guid);
        OgArticle appBean = convertServerOgArticleBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getOgArticle(String guid, String field) throws BaseException
    {
        return getOgArticleService().getOgArticle(guid, field);       
    }

    @Override
    public List<OgArticle> getOgArticles(List<String> guids) throws BaseException
    {
        List<OgArticle> serverBeanList = getOgArticleService().getOgArticles(guids);
        List<OgArticle> appBeanList = convertServerOgArticleBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<OgArticle> getAllOgArticles() throws BaseException
    {
        return getAllOgArticles(null, null, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgArticleService().getAllOgArticles(ordering, offset, count);
        return getAllOgArticles(ordering, offset, count, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgArticle> serverBeanList = getOgArticleService().getAllOgArticles(ordering, offset, count, forwardCursor);
        List<OgArticle> appBeanList = convertServerOgArticleBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgArticleService().getAllOgArticleKeys(ordering, offset, count);
        return getAllOgArticleKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgArticleService().getAllOgArticleKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgArticleService().findOgArticles(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgArticle> serverBeanList = getOgArticleService().findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<OgArticle> appBeanList = convertServerOgArticleBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgArticleService().findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgArticleService().findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgArticleService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgArticle(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        return getOgArticleService().createOgArticle(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
    }

    @Override
    public String createOgArticle(OgArticle ogArticle) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgArticleBean serverBean =  convertAppOgArticleBeanToServerBean(ogArticle);
        return getOgArticleService().createOgArticle(serverBean);
    }

    @Override
    public Boolean updateOgArticle(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        return getOgArticleService().updateOgArticle(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
    }

    @Override
    public Boolean updateOgArticle(OgArticle ogArticle) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgArticleBean serverBean =  convertAppOgArticleBeanToServerBean(ogArticle);
        return getOgArticleService().updateOgArticle(serverBean);
    }

    @Override
    public Boolean deleteOgArticle(String guid) throws BaseException
    {
        return getOgArticleService().deleteOgArticle(guid);
    }

    @Override
    public Boolean deleteOgArticle(OgArticle ogArticle) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgArticleBean serverBean =  convertAppOgArticleBeanToServerBean(ogArticle);
        return getOgArticleService().deleteOgArticle(serverBean);
    }

    @Override
    public Long deleteOgArticles(String filter, String params, List<String> values) throws BaseException
    {
        return getOgArticleService().deleteOgArticles(filter, params, values);
    }




    public static OgArticleBean convertServerOgArticleBeanToAppBean(OgArticle serverBean)
    {
        OgArticleBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new OgArticleBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUrl(serverBean.getUrl());
            bean.setType(serverBean.getType());
            bean.setSiteName(serverBean.getSiteName());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setFbAdmins(serverBean.getFbAdmins());
            bean.setFbAppId(serverBean.getFbAppId());
            bean.setImage(serverBean.getImage());
            bean.setAudio(serverBean.getAudio());
            bean.setVideo(serverBean.getVideo());
            bean.setLocale(serverBean.getLocale());
            bean.setLocaleAlternate(serverBean.getLocaleAlternate());
            bean.setAuthor(serverBean.getAuthor());
            bean.setSection(serverBean.getSection());
            bean.setTag(serverBean.getTag());
            bean.setPublishedDate(serverBean.getPublishedDate());
            bean.setModifiedDate(serverBean.getModifiedDate());
            bean.setExpirationDate(serverBean.getExpirationDate());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgArticle> convertServerOgArticleBeanListToAppBeanList(List<OgArticle> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<OgArticle> beanList = new ArrayList<OgArticle>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(OgArticle sb : serverBeanList) {
                OgArticleBean bean = convertServerOgArticleBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.OgArticleBean convertAppOgArticleBeanToServerBean(OgArticle appBean)
    {
        com.pagesynopsis.ws.bean.OgArticleBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgArticleBean();
            bean.setGuid(appBean.getGuid());
            bean.setUrl(appBean.getUrl());
            bean.setType(appBean.getType());
            bean.setSiteName(appBean.getSiteName());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setFbAdmins(appBean.getFbAdmins());
            bean.setFbAppId(appBean.getFbAppId());
            bean.setImage(appBean.getImage());
            bean.setAudio(appBean.getAudio());
            bean.setVideo(appBean.getVideo());
            bean.setLocale(appBean.getLocale());
            bean.setLocaleAlternate(appBean.getLocaleAlternate());
            bean.setAuthor(appBean.getAuthor());
            bean.setSection(appBean.getSection());
            bean.setTag(appBean.getTag());
            bean.setPublishedDate(appBean.getPublishedDate());
            bean.setModifiedDate(appBean.getModifiedDate());
            bean.setExpirationDate(appBean.getExpirationDate());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgArticle> convertAppOgArticleBeanListToServerBeanList(List<OgArticle> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<OgArticle> beanList = new ArrayList<OgArticle>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(OgArticle sb : appBeanList) {
                com.pagesynopsis.ws.bean.OgArticleBean bean = convertAppOgArticleBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
