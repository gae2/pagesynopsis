package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgMovieService;


// The primary purpose of OgMovieDummyService is to fake the service api, OgMovieService.
// It has no real implementation.
public class OgMovieDummyService implements OgMovieService
{
    private static final Logger log = Logger.getLogger(OgMovieDummyService.class.getName());

    public OgMovieDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // OgMovie related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgMovie getOgMovie(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgMovie(): guid = " + guid);
        return null;
    }

    @Override
    public Object getOgMovie(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgMovie(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<OgMovie> getOgMovies(List<String> guids) throws BaseException
    {
        log.fine("getOgMovies()");
        return null;
    }

    @Override
    public List<OgMovie> getAllOgMovies() throws BaseException
    {
        return getAllOgMovies(null, null, null);
    }


    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgMovies(ordering, offset, count, null);
    }

    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgMovies(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgMovieKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgMovieKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgMovieDummyService.findOgMovies(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgMovieDummyService.findOgMovieKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgMovieDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createOgMovie(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        log.finer("createOgMovie()");
        return null;
    }

    @Override
    public String createOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("createOgMovie()");
        return null;
    }

    @Override
    public OgMovie constructOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("constructOgMovie()");
        return null;
    }

    @Override
    public Boolean updateOgMovie(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        log.finer("updateOgMovie()");
        return null;
    }
        
    @Override
    public Boolean updateOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("updateOgMovie()");
        return null;
    }

    @Override
    public OgMovie refreshOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("refreshOgMovie()");
        return null;
    }

    @Override
    public Boolean deleteOgMovie(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgMovie(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("deleteOgMovie()");
        return null;
    }

    // TBD
    @Override
    public Long deleteOgMovies(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgMovie(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgMovies(List<OgMovie> ogMovies) throws BaseException
    {
        log.finer("createOgMovies()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateOgMovies(List<OgMovie> ogMovies) throws BaseException
    //{
    //}

}
