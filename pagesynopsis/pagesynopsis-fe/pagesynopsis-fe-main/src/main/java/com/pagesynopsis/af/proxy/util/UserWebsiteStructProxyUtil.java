package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.UserWebsiteStruct;
// import com.pagesynopsis.ws.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;


public class UserWebsiteStructProxyUtil
{
    private static final Logger log = Logger.getLogger(UserWebsiteStructProxyUtil.class.getName());

    // Static methods only.
    private UserWebsiteStructProxyUtil() {}

    public static UserWebsiteStructBean convertServerUserWebsiteStructBeanToAppBean(UserWebsiteStruct serverBean)
    {
        UserWebsiteStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new UserWebsiteStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setPrimarySite(serverBean.getPrimarySite());
            bean.setHomePage(serverBean.getHomePage());
            bean.setBlogSite(serverBean.getBlogSite());
            bean.setPortfolioSite(serverBean.getPortfolioSite());
            bean.setTwitterProfile(serverBean.getTwitterProfile());
            bean.setFacebookProfile(serverBean.getFacebookProfile());
            bean.setGooglePlusProfile(serverBean.getGooglePlusProfile());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.UserWebsiteStructBean convertAppUserWebsiteStructBeanToServerBean(UserWebsiteStruct appBean)
    {
        com.pagesynopsis.ws.bean.UserWebsiteStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.UserWebsiteStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setPrimarySite(appBean.getPrimarySite());
            bean.setHomePage(appBean.getHomePage());
            bean.setBlogSite(appBean.getBlogSite());
            bean.setPortfolioSite(appBean.getPortfolioSite());
            bean.setTwitterProfile(appBean.getTwitterProfile());
            bean.setFacebookProfile(appBean.getFacebookProfile());
            bean.setGooglePlusProfile(appBean.getGooglePlusProfile());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
