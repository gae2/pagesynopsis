package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgWebsiteListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.resource.OgWebsiteResource;
import com.pagesynopsis.af.resource.util.OgAudioStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgImageStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgActorStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgVideoStructResourceUtil;


// MockOgWebsiteResource is a decorator.
// It can be used as a base class to mock OgWebsiteResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/ogWebsites/")
public abstract class MockOgWebsiteResource implements OgWebsiteResource
{
    private static final Logger log = Logger.getLogger(MockOgWebsiteResource.class.getName());

    // MockOgWebsiteResource uses the decorator design pattern.
    private OgWebsiteResource decoratedResource;

    public MockOgWebsiteResource(OgWebsiteResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected OgWebsiteResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(OgWebsiteResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllOgWebsites(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgWebsites(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllOgWebsiteKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgWebsiteKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgWebsitesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findOgWebsitesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getOgWebsiteAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getOgWebsiteAsHtml(guid);
//    }

    @Override
    public Response getOgWebsite(String guid) throws BaseResourceException
    {
        return decoratedResource.getOgWebsite(guid);
    }

    @Override
    public Response getOgWebsiteAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getOgWebsiteAsJsonp(guid, callback);
    }

    @Override
    public Response getOgWebsite(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getOgWebsite(guid, field);
    }

    // TBD
    @Override
    public Response constructOgWebsite(OgWebsiteStub ogWebsite) throws BaseResourceException
    {
        return decoratedResource.constructOgWebsite(ogWebsite);
    }

    @Override
    public Response createOgWebsite(OgWebsiteStub ogWebsite) throws BaseResourceException
    {
        return decoratedResource.createOgWebsite(ogWebsite);
    }

//    @Override
//    public Response createOgWebsite(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createOgWebsite(formParams);
//    }

    // TBD
    @Override
    public Response refreshOgWebsite(String guid, OgWebsiteStub ogWebsite) throws BaseResourceException
    {
        return decoratedResource.refreshOgWebsite(guid, ogWebsite);
    }

    @Override
    public Response updateOgWebsite(String guid, OgWebsiteStub ogWebsite) throws BaseResourceException
    {
        return decoratedResource.updateOgWebsite(guid, ogWebsite);
    }

    @Override
    public Response updateOgWebsite(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<String> image, List<String> audio, List<String> video, String locale, List<String> localeAlternate, String note)
    {
        return decoratedResource.updateOgWebsite(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

//    @Override
//    public Response updateOgWebsite(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateOgWebsite(guid, formParams);
//    }

    @Override
    public Response deleteOgWebsite(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteOgWebsite(guid);
    }

    @Override
    public Response deleteOgWebsites(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteOgWebsites(filter, params, values);
    }


// TBD ....
    @Override
    public Response createOgWebsites(OgWebsiteListStub ogWebsites) throws BaseResourceException
    {
        return decoratedResource.createOgWebsites(ogWebsites);
    }


}
