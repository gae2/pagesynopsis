package com.pagesynopsis.af.resource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeListStub;

public interface OgTvEpisodeResource
{

    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllOgTvEpisodes(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("allkeys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllOgTvEpisodeKeys(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("keys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findOgTvEpisodeKeys(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findOgTvEpisodes(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Produces({ "application/x-javascript" })
    Response findOgTvEpisodesAsJsonp(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor, @DefaultValue("callback") @QueryParam("callback") String callback) throws BaseResourceException;

    @GET
    @Path("count")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCount(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("aggregate") String aggregate) throws BaseResourceException;

//    @GET
//    @Path("{guid : [0-9a-fA-F\\-]+}")
//    @Produces({MediaType.TEXT_HTML})
//    Response getOgTvEpisodeAsHtml(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    // @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Support both guid and key ???  (Note: We have to be consistent!)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getOgTvEpisode(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Produces({ "application/x-javascript" })
    Response getOgTvEpisodeAsJsonp(@PathParam("guid") String guid, @DefaultValue("callback") @QueryParam("callback") String callback) throws BaseResourceException;

    @GET
    //@Path("{guid : [0-9a-fA-F\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Path("{guid : [0-9a-f\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getOgTvEpisode(@PathParam("guid") String guid, @PathParam("field") String field) throws BaseResourceException;

    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response constructOgTvEpisode(OgTvEpisodeStub ogTvEpisode) throws BaseResourceException;

    @POST
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createOgTvEpisode(OgTvEpisodeStub ogTvEpisode) throws BaseResourceException;

//    @POST
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    Response createOgTvEpisode(MultivaluedMap<String, String> formParams) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response refreshOgTvEpisode(@PathParam("guid") String guid, OgTvEpisodeStub ogTvEpisode) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    //@Produces({MediaType.TEXT_PLAIN})    // ??? updateOgTvEpisode() returns 204 (No Content)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateOgTvEpisode(@PathParam("guid") String guid, OgTvEpisodeStub ogTvEpisode) throws BaseResourceException;

    //@PUT
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    //@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    //Response updateOgTvEpisode(@PathParam("guid") String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException;

    //@PUT  ???
    @POST   // We can adhere to semantics of PUT=Replace, POST=update. PUT is supported in HTML form in HTML5 only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateOgTvEpisode(@PathParam("guid") String guid, @QueryParam("url") String url, @QueryParam("type") String type, @QueryParam("siteName") String siteName, @QueryParam("title") String title, @QueryParam("description") String description, @QueryParam("fbAdmins") List<String> fbAdmins, @QueryParam("fbAppId") List<String> fbAppId, @QueryParam("image") List<String> image, @QueryParam("audio") List<String> audio, @QueryParam("video") List<String> video, @QueryParam("locale") String locale, @QueryParam("localeAlternate") List<String> localeAlternate, @QueryParam("director") List<String> director, @QueryParam("writer") List<String> writer, @QueryParam("actor") List<String> actor, @QueryParam("series") String series, @QueryParam("duration") Integer duration, @QueryParam("tag") List<String> tag, @QueryParam("releaseDate") String releaseDate) throws BaseResourceException;

//    @POST
//    //@Path("{guid : [0-9a-fA-F\\-]+}")
//    @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    Response updateOgTvEpisode(@PathParam("guid") String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException;

    @DELETE
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    Response deleteOgTvEpisode(@PathParam("guid") String guid) throws BaseResourceException;

    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response deleteOgTvEpisodes(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseResourceException;

    @POST
    @Path("bulk")
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createOgTvEpisodes(OgTvEpisodeListStub ogTvEpisodes) throws BaseResourceException;

//    @PUT
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response updateeOgTvEpisodes(OgTvEpisodeListStub ogTvEpisodes) throws BaseResourceException;

}
