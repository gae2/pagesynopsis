package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.TwitterCardProductData;
// import com.pagesynopsis.ws.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;


public class TwitterCardProductDataProxyUtil
{
    private static final Logger log = Logger.getLogger(TwitterCardProductDataProxyUtil.class.getName());

    // Static methods only.
    private TwitterCardProductDataProxyUtil() {}

    public static TwitterCardProductDataBean convertServerTwitterCardProductDataBeanToAppBean(TwitterCardProductData serverBean)
    {
        TwitterCardProductDataBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new TwitterCardProductDataBean();
            bean.setData(serverBean.getData());
            bean.setLabel(serverBean.getLabel());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.TwitterCardProductDataBean convertAppTwitterCardProductDataBeanToServerBean(TwitterCardProductData appBean)
    {
        com.pagesynopsis.ws.bean.TwitterCardProductDataBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.TwitterCardProductDataBean();
            bean.setData(appBean.getData());
            bean.setLabel(appBean.getLabel());
        }
        return bean;
    }

}
