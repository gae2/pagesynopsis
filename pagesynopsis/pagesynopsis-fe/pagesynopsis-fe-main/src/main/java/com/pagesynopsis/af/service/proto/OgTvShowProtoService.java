package com.pagesynopsis.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.af.service.impl.OgTvShowServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class OgTvShowProtoService extends OgTvShowServiceImpl implements OgTvShowService
{
    private static final Logger log = Logger.getLogger(OgTvShowProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public OgTvShowProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgTvShow related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public OgTvShow getOgTvShow(String guid) throws BaseException
    {
        return super.getOgTvShow(guid);
    }

    @Override
    public Object getOgTvShow(String guid, String field) throws BaseException
    {
        return super.getOgTvShow(guid, field);
    }

    @Override
    public List<OgTvShow> getOgTvShows(List<String> guids) throws BaseException
    {
        return super.getOgTvShows(guids);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows() throws BaseException
    {
        return super.getAllOgTvShows();
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShows(ordering, offset, count, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllOgTvShows(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShowKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllOgTvShowKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        return super.createOgTvShow(ogTvShow);
    }

    @Override
    public OgTvShow constructOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        return super.constructOgTvShow(ogTvShow);
    }


    @Override
    public Boolean updateOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        return super.updateOgTvShow(ogTvShow);
    }
        
    @Override
    public OgTvShow refreshOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        return super.refreshOgTvShow(ogTvShow);
    }

    @Override
    public Boolean deleteOgTvShow(String guid) throws BaseException
    {
        return super.deleteOgTvShow(guid);
    }

    @Override
    public Boolean deleteOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        return super.deleteOgTvShow(ogTvShow);
    }

    @Override
    public Integer createOgTvShows(List<OgTvShow> ogTvShows) throws BaseException
    {
        return super.createOgTvShows(ogTvShows);
    }

    // TBD
    //@Override
    //public Boolean updateOgTvShows(List<OgTvShow> ogTvShows) throws BaseException
    //{
    //}

}
