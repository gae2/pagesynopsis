package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.GeoPointStruct;
import com.pagesynopsis.ws.StreetAddressStruct;
import com.pagesynopsis.ws.GaeAppStruct;
import com.pagesynopsis.ws.FullNameStruct;
import com.pagesynopsis.ws.GaeUserStruct;
import com.pagesynopsis.ws.User;
// import com.pagesynopsis.ws.bean.UserBean;
import com.pagesynopsis.ws.service.UserService;
import com.pagesynopsis.af.bean.UserBean;
import com.pagesynopsis.af.proxy.UserServiceProxy;
import com.pagesynopsis.af.proxy.util.GaeAppStructProxyUtil;
import com.pagesynopsis.af.proxy.util.FullNameStructProxyUtil;
import com.pagesynopsis.af.proxy.util.GaeUserStructProxyUtil;
import com.pagesynopsis.af.proxy.util.StreetAddressStructProxyUtil;
import com.pagesynopsis.af.proxy.util.GeoPointStructProxyUtil;


public class LocalUserServiceProxy extends BaseLocalServiceProxy implements UserServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserServiceProxy.class.getName());

    public LocalUserServiceProxy()
    {
    }

    @Override
    public User getUser(String guid) throws BaseException
    {
        User serverBean = getUserService().getUser(guid);
        User appBean = convertServerUserBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        return getUserService().getUser(guid, field);       
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        List<User> serverBeanList = getUserService().getUsers(guids);
        List<User> appBeanList = convertServerUserBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUserService().getAllUsers(ordering, offset, count);
        return getAllUsers(ordering, offset, count, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<User> serverBeanList = getUserService().getAllUsers(ordering, offset, count, forwardCursor);
        List<User> appBeanList = convertServerUserBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUserService().getAllUserKeys(ordering, offset, count);
        return getAllUserKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserService().getAllUserKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUserService().findUsers(filter, ordering, params, values, grouping, unique, offset, count);
        return findUsers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<User> serverBeanList = getUserService().findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<User> appBeanList = convertServerUserBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUserService().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserService().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return getUserService().createUser(managerApp, appAcl, gaeApp, aeryId, sessionId, ancestorGuid, name, usercode, username, nickname, avatar, email, openId, gaeUser, entityType, surrogate, obsolete, timeZone, location, streetAddress, geoPoint, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        com.pagesynopsis.ws.bean.UserBean serverBean =  convertAppUserBeanToServerBean(user);
        return getUserService().createUser(serverBean);
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return getUserService().updateUser(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, ancestorGuid, name, usercode, username, nickname, avatar, email, openId, gaeUser, entityType, surrogate, obsolete, timeZone, location, streetAddress, geoPoint, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        com.pagesynopsis.ws.bean.UserBean serverBean =  convertAppUserBeanToServerBean(user);
        return getUserService().updateUser(serverBean);
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        return getUserService().deleteUser(guid);
    }

    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        com.pagesynopsis.ws.bean.UserBean serverBean =  convertAppUserBeanToServerBean(user);
        return getUserService().deleteUser(serverBean);
    }

    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        return getUserService().deleteUsers(filter, params, values);
    }




    public static UserBean convertServerUserBeanToAppBean(User serverBean)
    {
        UserBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new UserBean();
            bean.setGuid(serverBean.getGuid());
            bean.setManagerApp(serverBean.getManagerApp());
            bean.setAppAcl(serverBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertServerGaeAppStructBeanToAppBean(serverBean.getGaeApp()));
            bean.setAeryId(serverBean.getAeryId());
            bean.setSessionId(serverBean.getSessionId());
            bean.setAncestorGuid(serverBean.getAncestorGuid());
            bean.setName(FullNameStructProxyUtil.convertServerFullNameStructBeanToAppBean(serverBean.getName()));
            bean.setUsercode(serverBean.getUsercode());
            bean.setUsername(serverBean.getUsername());
            bean.setNickname(serverBean.getNickname());
            bean.setAvatar(serverBean.getAvatar());
            bean.setEmail(serverBean.getEmail());
            bean.setOpenId(serverBean.getOpenId());
            bean.setGaeUser(GaeUserStructProxyUtil.convertServerGaeUserStructBeanToAppBean(serverBean.getGaeUser()));
            bean.setEntityType(serverBean.getEntityType());
            bean.setSurrogate(serverBean.isSurrogate());
            bean.setObsolete(serverBean.isObsolete());
            bean.setTimeZone(serverBean.getTimeZone());
            bean.setLocation(serverBean.getLocation());
            bean.setStreetAddress(StreetAddressStructProxyUtil.convertServerStreetAddressStructBeanToAppBean(serverBean.getStreetAddress()));
            bean.setGeoPoint(GeoPointStructProxyUtil.convertServerGeoPointStructBeanToAppBean(serverBean.getGeoPoint()));
            bean.setIpAddress(serverBean.getIpAddress());
            bean.setReferer(serverBean.getReferer());
            bean.setStatus(serverBean.getStatus());
            bean.setEmailVerifiedTime(serverBean.getEmailVerifiedTime());
            bean.setOpenIdVerifiedTime(serverBean.getOpenIdVerifiedTime());
            bean.setAuthenticatedTime(serverBean.getAuthenticatedTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<User> convertServerUserBeanListToAppBeanList(List<User> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<User> beanList = new ArrayList<User>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(User sb : serverBeanList) {
                UserBean bean = convertServerUserBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.UserBean convertAppUserBeanToServerBean(User appBean)
    {
        com.pagesynopsis.ws.bean.UserBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.UserBean();
            bean.setGuid(appBean.getGuid());
            bean.setManagerApp(appBean.getManagerApp());
            bean.setAppAcl(appBean.getAppAcl());
            bean.setGaeApp(GaeAppStructProxyUtil.convertAppGaeAppStructBeanToServerBean(appBean.getGaeApp()));
            bean.setAeryId(appBean.getAeryId());
            bean.setSessionId(appBean.getSessionId());
            bean.setAncestorGuid(appBean.getAncestorGuid());
            bean.setName(FullNameStructProxyUtil.convertAppFullNameStructBeanToServerBean(appBean.getName()));
            bean.setUsercode(appBean.getUsercode());
            bean.setUsername(appBean.getUsername());
            bean.setNickname(appBean.getNickname());
            bean.setAvatar(appBean.getAvatar());
            bean.setEmail(appBean.getEmail());
            bean.setOpenId(appBean.getOpenId());
            bean.setGaeUser(GaeUserStructProxyUtil.convertAppGaeUserStructBeanToServerBean(appBean.getGaeUser()));
            bean.setEntityType(appBean.getEntityType());
            bean.setSurrogate(appBean.isSurrogate());
            bean.setObsolete(appBean.isObsolete());
            bean.setTimeZone(appBean.getTimeZone());
            bean.setLocation(appBean.getLocation());
            bean.setStreetAddress(StreetAddressStructProxyUtil.convertAppStreetAddressStructBeanToServerBean(appBean.getStreetAddress()));
            bean.setGeoPoint(GeoPointStructProxyUtil.convertAppGeoPointStructBeanToServerBean(appBean.getGeoPoint()));
            bean.setIpAddress(appBean.getIpAddress());
            bean.setReferer(appBean.getReferer());
            bean.setStatus(appBean.getStatus());
            bean.setEmailVerifiedTime(appBean.getEmailVerifiedTime());
            bean.setOpenIdVerifiedTime(appBean.getOpenIdVerifiedTime());
            bean.setAuthenticatedTime(appBean.getAuthenticatedTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<User> convertAppUserBeanListToServerBeanList(List<User> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<User> beanList = new ArrayList<User>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(User sb : appBeanList) {
                com.pagesynopsis.ws.bean.UserBean bean = convertAppUserBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
