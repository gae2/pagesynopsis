package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.MediaSourceStruct;
// import com.pagesynopsis.ws.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;


public class MediaSourceStructProxyUtil
{
    private static final Logger log = Logger.getLogger(MediaSourceStructProxyUtil.class.getName());

    // Static methods only.
    private MediaSourceStructProxyUtil() {}

    public static MediaSourceStructBean convertServerMediaSourceStructBeanToAppBean(MediaSourceStruct serverBean)
    {
        MediaSourceStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new MediaSourceStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setSrc(serverBean.getSrc());
            bean.setSrcUrl(serverBean.getSrcUrl());
            bean.setType(serverBean.getType());
            bean.setRemark(serverBean.getRemark());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.MediaSourceStructBean convertAppMediaSourceStructBeanToServerBean(MediaSourceStruct appBean)
    {
        com.pagesynopsis.ws.bean.MediaSourceStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.MediaSourceStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setSrc(appBean.getSrc());
            bean.setSrcUrl(appBean.getSrcUrl());
            bean.setType(appBean.getType());
            bean.setRemark(appBean.getRemark());
        }
        return bean;
    }

}
