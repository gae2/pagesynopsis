package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
// import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.proxy.OgMovieServiceProxy;


public class LocalOgMovieServiceProxy extends BaseLocalServiceProxy implements OgMovieServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgMovieServiceProxy.class.getName());

    public LocalOgMovieServiceProxy()
    {
    }

    @Override
    public OgMovie getOgMovie(String guid) throws BaseException
    {
        OgMovie serverBean = getOgMovieService().getOgMovie(guid);
        OgMovie appBean = convertServerOgMovieBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getOgMovie(String guid, String field) throws BaseException
    {
        return getOgMovieService().getOgMovie(guid, field);       
    }

    @Override
    public List<OgMovie> getOgMovies(List<String> guids) throws BaseException
    {
        List<OgMovie> serverBeanList = getOgMovieService().getOgMovies(guids);
        List<OgMovie> appBeanList = convertServerOgMovieBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<OgMovie> getAllOgMovies() throws BaseException
    {
        return getAllOgMovies(null, null, null);
    }

    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgMovieService().getAllOgMovies(ordering, offset, count);
        return getAllOgMovies(ordering, offset, count, null);
    }

    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgMovie> serverBeanList = getOgMovieService().getAllOgMovies(ordering, offset, count, forwardCursor);
        List<OgMovie> appBeanList = convertServerOgMovieBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgMovieService().getAllOgMovieKeys(ordering, offset, count);
        return getAllOgMovieKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgMovieService().getAllOgMovieKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgMovieService().findOgMovies(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgMovie> serverBeanList = getOgMovieService().findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<OgMovie> appBeanList = convertServerOgMovieBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgMovieService().findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgMovieService().findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgMovieService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgMovie(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgMovieService().createOgMovie(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public String createOgMovie(OgMovie ogMovie) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgMovieBean serverBean =  convertAppOgMovieBeanToServerBean(ogMovie);
        return getOgMovieService().createOgMovie(serverBean);
    }

    @Override
    public Boolean updateOgMovie(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgMovieService().updateOgMovie(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public Boolean updateOgMovie(OgMovie ogMovie) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgMovieBean serverBean =  convertAppOgMovieBeanToServerBean(ogMovie);
        return getOgMovieService().updateOgMovie(serverBean);
    }

    @Override
    public Boolean deleteOgMovie(String guid) throws BaseException
    {
        return getOgMovieService().deleteOgMovie(guid);
    }

    @Override
    public Boolean deleteOgMovie(OgMovie ogMovie) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgMovieBean serverBean =  convertAppOgMovieBeanToServerBean(ogMovie);
        return getOgMovieService().deleteOgMovie(serverBean);
    }

    @Override
    public Long deleteOgMovies(String filter, String params, List<String> values) throws BaseException
    {
        return getOgMovieService().deleteOgMovies(filter, params, values);
    }




    public static OgMovieBean convertServerOgMovieBeanToAppBean(OgMovie serverBean)
    {
        OgMovieBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new OgMovieBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUrl(serverBean.getUrl());
            bean.setType(serverBean.getType());
            bean.setSiteName(serverBean.getSiteName());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setFbAdmins(serverBean.getFbAdmins());
            bean.setFbAppId(serverBean.getFbAppId());
            bean.setImage(serverBean.getImage());
            bean.setAudio(serverBean.getAudio());
            bean.setVideo(serverBean.getVideo());
            bean.setLocale(serverBean.getLocale());
            bean.setLocaleAlternate(serverBean.getLocaleAlternate());
            bean.setDirector(serverBean.getDirector());
            bean.setWriter(serverBean.getWriter());
            bean.setActor(serverBean.getActor());
            bean.setDuration(serverBean.getDuration());
            bean.setTag(serverBean.getTag());
            bean.setReleaseDate(serverBean.getReleaseDate());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgMovie> convertServerOgMovieBeanListToAppBeanList(List<OgMovie> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<OgMovie> beanList = new ArrayList<OgMovie>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(OgMovie sb : serverBeanList) {
                OgMovieBean bean = convertServerOgMovieBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.OgMovieBean convertAppOgMovieBeanToServerBean(OgMovie appBean)
    {
        com.pagesynopsis.ws.bean.OgMovieBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgMovieBean();
            bean.setGuid(appBean.getGuid());
            bean.setUrl(appBean.getUrl());
            bean.setType(appBean.getType());
            bean.setSiteName(appBean.getSiteName());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setFbAdmins(appBean.getFbAdmins());
            bean.setFbAppId(appBean.getFbAppId());
            bean.setImage(appBean.getImage());
            bean.setAudio(appBean.getAudio());
            bean.setVideo(appBean.getVideo());
            bean.setLocale(appBean.getLocale());
            bean.setLocaleAlternate(appBean.getLocaleAlternate());
            bean.setDirector(appBean.getDirector());
            bean.setWriter(appBean.getWriter());
            bean.setActor(appBean.getActor());
            bean.setDuration(appBean.getDuration());
            bean.setTag(appBean.getTag());
            bean.setReleaseDate(appBean.getReleaseDate());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgMovie> convertAppOgMovieBeanListToServerBeanList(List<OgMovie> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<OgMovie> beanList = new ArrayList<OgMovie>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(OgMovie sb : appBeanList) {
                com.pagesynopsis.ws.bean.OgMovieBean bean = convertAppOgMovieBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
