package com.pagesynopsis.common;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// Poor man's branding...
public class AppBrand
{
    private static final Logger log = Logger.getLogger(AppBrand.class.getName());

    private AppBrand() {}


    // TBD:
    public static final String BRAND_UNKNOWN = "unknown";  // ???
    public static final String BRAND_PAGESYNOPSIS = "pagesynopsis";
    public static final String BRAND_PAGESUM = "pagesum";
    public static final String BRAND_PAGEROBOTS = "pagerobots";
    public static final String BRAND_PAGEMETAS = "pagemetas";
    // ...
    
    
    // Brand key -> App display name
    private static final Map<String, String> sBrandMap = new HashMap<String, String>();
    static {
        sBrandMap.put(BRAND_PAGESYNOPSIS, "Page Synopsis");
        sBrandMap.put(BRAND_PAGESUM, "Page Sum");
        sBrandMap.put(BRAND_PAGEROBOTS, "Page Robots");
        sBrandMap.put(BRAND_PAGEMETAS, "Page Metas");
    }
    
    
    // temporary
    public static String getDefaultBrand()
    {
        return BRAND_PAGESYNOPSIS;
    }
    
    // temporary
    public static boolean isValidBrand(String brand)
    {
        return sBrandMap.containsKey(brand);
    }
    
    // temporary
    public static String getDisplayName(String brand)
    {
        if(isValidBrand(brand)) {
            return sBrandMap.get(brand);
        } else {
            // ????
            return "";
        }
    }

    // temporary
    public static String getBrandDescription(String brand)
    {
        if(BRAND_PAGESYNOPSIS.equals(brand)) {
            return "Page Synopsis, a Web service for fetching page title and description.";
        } else if(BRAND_PAGESUM.equals(brand)) {
            return "Page Sum, a Web service for page summaries";
        } else if(BRAND_PAGEROBOTS.equals(brand)) {
            return "Page Robots, a Web service for fetching robots.txt";
        } else if(BRAND_PAGEMETAS.equals(brand)) {
            return "Page Metas, a Web service for page metadata.";
        } else {
            // ????
            return "";
        }
    }

}
