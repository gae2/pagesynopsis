package com.pagesynopsis.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.service.VideoSetAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.VideoSet;


public class VideoSetHelper
{
    private static final Logger log = Logger.getLogger(VideoSetHelper.class.getName());

    private VideoSetHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private VideoSetAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private VideoSetAppService getVideoSetService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new VideoSetAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class VideoSetHelperHolder
    {
        private static final VideoSetHelper INSTANCE = new VideoSetHelper();
    }

    // Singleton method
    public static VideoSetHelper getInstance()
    {
        return VideoSetHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public VideoSet getVideoSet(String guid) 
    {
        VideoSet message = null;
        try {
            message = getVideoSetService().getVideoSet(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
    
    // TBD:
    // Check the pageTitle field as well....  ????
    // ...
    

    public List<String> getVideoSetKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getVideoSetService().findVideoSetKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    public List<VideoSet> getVideoSetsForRefreshProcessing(Integer maxCount)
    {
        List<VideoSet> videoSets = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            videoSets = getVideoSetService().findVideoSets(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return videoSets;
    }


}
