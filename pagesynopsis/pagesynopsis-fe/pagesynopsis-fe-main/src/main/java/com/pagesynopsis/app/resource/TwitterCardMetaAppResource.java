package com.pagesynopsis.app.resource;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import com.sun.jersey.api.core.HttpContext;

import com.pagesynopsis.af.auth.TwoLeggedOAuthProvider;
import com.pagesynopsis.af.bean.TwitterCardMetaBean;
import com.pagesynopsis.af.resource.impl.BaseResourceImpl;
import com.pagesynopsis.af.resource.impl.TwitterCardMetaResourceImpl;
import com.pagesynopsis.app.fetch.MetadataProcessor;
import com.pagesynopsis.app.service.TwitterCardMetaAppService;
import com.pagesynopsis.app.util.QueryStringUtil;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;
import com.pagesynopsis.ws.resource.exception.UnauthorizedRsException;
import com.pagesynopsis.ws.stub.TwitterCardMetaStub;


@Path("/twittercardmeta/")
public class TwitterCardMetaAppResource extends BaseResourceImpl
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaAppResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    private TwitterCardMetaAppService twitterCardMetaService = null;
    
    public TwitterCardMetaAppResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }

        this.httpContext = httpContext;
    }

    private TwitterCardMetaAppService getTwitterCardMetaAppService()
    {
        if(twitterCardMetaService == null) {
            twitterCardMetaService = new TwitterCardMetaAppService();
        }
        return twitterCardMetaService;
    }


    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findTwitterCardMetaByTargetUrl(@QueryParam("targetUrl") String targetUrl, @QueryParam("fetch") Boolean fetch, @QueryParam("refresh") Integer refresh)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            if(targetUrl == null || targetUrl.isEmpty()) {
                throw new BadRequestRsException("Arg, targetUrl, is missing.", resourceUri);
            }

            // refresh==0: refreshFirst, but do not update refreshInterval
            // refresh==-1: refreshFirst, but do not save (create/update)...
            boolean refreshFirst = false;
            Long refreshInterval = null;
            if(refresh != null) {
                refreshFirst = true;
                if(refresh > 0) {
                    refreshInterval = refresh * 1000L;
                }
            }
            log.fine("refreshFirst = " + refreshFirst + "; refreshInterval = " + refreshInterval);
            
            // Existing record for targetUrl in DB 
            TwitterCardMetaStub stub = null;
            if(refresh == null || refresh > -1) {
                String filter = "targetUrl=='" + targetUrl + "'";
                String ordering = "createdTime desc";
                Long offset = 0L;
                Integer count = 1;
                List<String> beanKeys = getTwitterCardMetaAppService().findTwitterCardMetaKeys(filter, ordering, null, null, null, null, offset, count);
                if(beanKeys != null && !beanKeys.isEmpty()) {
                    String beanKey = beanKeys.get(0);
                    TwitterCardMeta fullBean = getTwitterCardMetaAppService().getTwitterCardMeta(beanKey); 
                    stub = TwitterCardMetaStub.convertBeanToStub(fullBean);
                }
            }

            // Whether to create/update records in the DB...
            boolean update = false;   // Do not update by default...
            boolean create = true;    // Create, if update==false, by default... If update==true, this flag is not used...

            if(stub != null) {
                create = false;
                if(refreshFirst == true && refreshInterval != null) {
                    update = true;
                } else {
                    // update = false;  // default...
                }
            } else {
                if(refreshFirst == true && refresh == -1) {
                    create = false;
                    // update = false;  // default...
                } else {
                    // create = true;   // default....
                }
            }

            // Fetch/refresh if necessary...
            if(refreshFirst==true || (stub == null && (fetch == null || fetch == true))) {
                if(stub == null) {
                    stub = new TwitterCardMetaStub();   // ????
                    String guid = GUID.generate();
                    stub.setGuid(guid);
                    stub.setTargetUrl(targetUrl);
                    try {
                        URL url = new URL(targetUrl);  // ???
                        String query = url.getQuery();
                        String pageUrl = targetUrl;
                        if(query != null) {
                            int qidx = targetUrl.indexOf("?");
                            if(qidx > 0) {
                                pageUrl = targetUrl.substring(0, qidx);
                            }
                        }
                        stub.setPageUrl(pageUrl);
                        if(query != null && !query.isEmpty()) {
                            stub.setQueryString(query); 
                            List<KeyValuePairStruct> parameters = QueryStringUtil.parseQueryParameters(query);
                            stub.setQueryParams(parameters);
                        }
                    } catch(MalformedURLException e) {
                        log.log(Level.WARNING, "targetUrl is malformed. targetUrl = " + targetUrl, e);
                        // ignore and continue???? or, bail out???
                        throw new BadRequestException("targetUrl is malformed. targetUrl = " + targetUrl, e);
                    }
                }
                stub.setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ???
                if(refreshInterval != null) {
                    stub.setRefreshInterval(refreshInterval);
                }

                // ...
                TwitterCardMeta bean = TwitterCardMetaResourceImpl.convertTwitterCardMetaStubToBean(stub);
                bean = MetadataProcessor.getInstance().processTwitterCardMetaFetch(bean);
                // ...
                if(bean != null) {
                    // Need to to do this to avoid double processing.
                    ((TwitterCardMetaBean) bean).setDeferred(true);
                    // ....
                    if(update) {
                        bean = getTwitterCardMetaAppService().refreshTwitterCardMeta(bean);
                    } else {
                        if(create) {
                            bean = getTwitterCardMetaAppService().constructTwitterCardMeta(bean);
                        }
                    }
                }
                if(bean == null) {
                    throw new InternalServerErrorRsException("Failed to create/update a twitterCardMeta due to unknown error: targetUrl = " + targetUrl, resourceUri);                        
                }
                stub = TwitterCardMetaStub.convertBeanToStub(bean);
            }

            if(stub != null) {
                if(log.isLoggable(Level.FINE)) log.fine("Returning stub = " + stub);
                return Response.ok(stub).build();
            } else {
                // TBD: Throw 404 ??? Or, just return en empty/null object???
                throw new ResourceNotFoundRsException("TwitterCardMeta not found for the given targetUrl, " + targetUrl, resourceUri);
            }
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    

}
