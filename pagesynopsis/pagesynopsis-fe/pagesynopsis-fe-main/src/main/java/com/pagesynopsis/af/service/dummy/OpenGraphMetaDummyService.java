package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OpenGraphMetaService;


// The primary purpose of OpenGraphMetaDummyService is to fake the service api, OpenGraphMetaService.
// It has no real implementation.
public class OpenGraphMetaDummyService implements OpenGraphMetaService
{
    private static final Logger log = Logger.getLogger(OpenGraphMetaDummyService.class.getName());

    public OpenGraphMetaDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // OpenGraphMeta related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OpenGraphMeta getOpenGraphMeta(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOpenGraphMeta(): guid = " + guid);
        return null;
    }

    @Override
    public Object getOpenGraphMeta(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOpenGraphMeta(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<OpenGraphMeta> getOpenGraphMetas(List<String> guids) throws BaseException
    {
        log.fine("getOpenGraphMetas()");
        return null;
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas() throws BaseException
    {
        return getAllOpenGraphMetas(null, null, null);
    }


    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOpenGraphMetas(ordering, offset, count, null);
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOpenGraphMetas(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOpenGraphMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOpenGraphMetaKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOpenGraphMetas(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OpenGraphMetaDummyService.findOpenGraphMetas(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OpenGraphMetaDummyService.findOpenGraphMetaKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OpenGraphMetaDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createOpenGraphMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        log.finer("createOpenGraphMeta()");
        return null;
    }

    @Override
    public String createOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("createOpenGraphMeta()");
        return null;
    }

    @Override
    public OpenGraphMeta constructOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("constructOpenGraphMeta()");
        return null;
    }

    @Override
    public Boolean updateOpenGraphMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        log.finer("updateOpenGraphMeta()");
        return null;
    }
        
    @Override
    public Boolean updateOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("updateOpenGraphMeta()");
        return null;
    }

    @Override
    public OpenGraphMeta refreshOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("refreshOpenGraphMeta()");
        return null;
    }

    @Override
    public Boolean deleteOpenGraphMeta(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOpenGraphMeta(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("deleteOpenGraphMeta()");
        return null;
    }

    // TBD
    @Override
    public Long deleteOpenGraphMetas(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOpenGraphMeta(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOpenGraphMetas(List<OpenGraphMeta> openGraphMetas) throws BaseException
    {
        log.finer("createOpenGraphMetas()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateOpenGraphMetas(List<OpenGraphMeta> openGraphMetas) throws BaseException
    //{
    //}

}
