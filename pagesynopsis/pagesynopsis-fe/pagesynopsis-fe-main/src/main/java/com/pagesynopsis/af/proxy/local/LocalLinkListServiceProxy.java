package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.LinkList;
// import com.pagesynopsis.ws.bean.LinkListBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.LinkListService;
import com.pagesynopsis.af.bean.LinkListBean;
import com.pagesynopsis.af.proxy.LinkListServiceProxy;


public class LocalLinkListServiceProxy extends BaseLocalServiceProxy implements LinkListServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalLinkListServiceProxy.class.getName());

    public LocalLinkListServiceProxy()
    {
    }

    @Override
    public LinkList getLinkList(String guid) throws BaseException
    {
        LinkList serverBean = getLinkListService().getLinkList(guid);
        LinkList appBean = convertServerLinkListBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getLinkList(String guid, String field) throws BaseException
    {
        return getLinkListService().getLinkList(guid, field);       
    }

    @Override
    public List<LinkList> getLinkLists(List<String> guids) throws BaseException
    {
        List<LinkList> serverBeanList = getLinkListService().getLinkLists(guids);
        List<LinkList> appBeanList = convertServerLinkListBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<LinkList> getAllLinkLists() throws BaseException
    {
        return getAllLinkLists(null, null, null);
    }

    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getLinkListService().getAllLinkLists(ordering, offset, count);
        return getAllLinkLists(ordering, offset, count, null);
    }

    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<LinkList> serverBeanList = getLinkListService().getAllLinkLists(ordering, offset, count, forwardCursor);
        List<LinkList> appBeanList = convertServerLinkListBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getLinkListService().getAllLinkListKeys(ordering, offset, count);
        return getAllLinkListKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getLinkListService().getAllLinkListKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getLinkListService().findLinkLists(filter, ordering, params, values, grouping, unique, offset, count);
        return findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<LinkList> serverBeanList = getLinkListService().findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<LinkList> appBeanList = convertServerLinkListBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getLinkListService().findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getLinkListService().findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getLinkListService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createLinkList(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        return getLinkListService().createLinkList(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
    }

    @Override
    public String createLinkList(LinkList linkList) throws BaseException
    {
        com.pagesynopsis.ws.bean.LinkListBean serverBean =  convertAppLinkListBeanToServerBean(linkList);
        return getLinkListService().createLinkList(serverBean);
    }

    @Override
    public Boolean updateLinkList(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        return getLinkListService().updateLinkList(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
    }

    @Override
    public Boolean updateLinkList(LinkList linkList) throws BaseException
    {
        com.pagesynopsis.ws.bean.LinkListBean serverBean =  convertAppLinkListBeanToServerBean(linkList);
        return getLinkListService().updateLinkList(serverBean);
    }

    @Override
    public Boolean deleteLinkList(String guid) throws BaseException
    {
        return getLinkListService().deleteLinkList(guid);
    }

    @Override
    public Boolean deleteLinkList(LinkList linkList) throws BaseException
    {
        com.pagesynopsis.ws.bean.LinkListBean serverBean =  convertAppLinkListBeanToServerBean(linkList);
        return getLinkListService().deleteLinkList(serverBean);
    }

    @Override
    public Long deleteLinkLists(String filter, String params, List<String> values) throws BaseException
    {
        return getLinkListService().deleteLinkLists(filter, params, values);
    }




    public static LinkListBean convertServerLinkListBeanToAppBean(LinkList serverBean)
    {
        LinkListBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new LinkListBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setFetchRequest(serverBean.getFetchRequest());
            bean.setTargetUrl(serverBean.getTargetUrl());
            bean.setPageUrl(serverBean.getPageUrl());
            bean.setQueryString(serverBean.getQueryString());
            bean.setQueryParams(serverBean.getQueryParams());
            bean.setLastFetchResult(serverBean.getLastFetchResult());
            bean.setResponseCode(serverBean.getResponseCode());
            bean.setContentType(serverBean.getContentType());
            bean.setContentLength(serverBean.getContentLength());
            bean.setLanguage(serverBean.getLanguage());
            bean.setRedirect(serverBean.getRedirect());
            bean.setLocation(serverBean.getLocation());
            bean.setPageTitle(serverBean.getPageTitle());
            bean.setNote(serverBean.getNote());
            bean.setDeferred(serverBean.isDeferred());
            bean.setStatus(serverBean.getStatus());
            bean.setRefreshStatus(serverBean.getRefreshStatus());
            bean.setRefreshInterval(serverBean.getRefreshInterval());
            bean.setNextRefreshTime(serverBean.getNextRefreshTime());
            bean.setLastCheckedTime(serverBean.getLastCheckedTime());
            bean.setLastUpdatedTime(serverBean.getLastUpdatedTime());
            bean.setUrlSchemeFilter(serverBean.getUrlSchemeFilter());
            bean.setPageAnchors(serverBean.getPageAnchors());
            bean.setExcludeRelativeUrls(serverBean.isExcludeRelativeUrls());
            bean.setExcludedBaseUrls(serverBean.getExcludedBaseUrls());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<LinkList> convertServerLinkListBeanListToAppBeanList(List<LinkList> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<LinkList> beanList = new ArrayList<LinkList>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(LinkList sb : serverBeanList) {
                LinkListBean bean = convertServerLinkListBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.LinkListBean convertAppLinkListBeanToServerBean(LinkList appBean)
    {
        com.pagesynopsis.ws.bean.LinkListBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.LinkListBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setFetchRequest(appBean.getFetchRequest());
            bean.setTargetUrl(appBean.getTargetUrl());
            bean.setPageUrl(appBean.getPageUrl());
            bean.setQueryString(appBean.getQueryString());
            bean.setQueryParams(appBean.getQueryParams());
            bean.setLastFetchResult(appBean.getLastFetchResult());
            bean.setResponseCode(appBean.getResponseCode());
            bean.setContentType(appBean.getContentType());
            bean.setContentLength(appBean.getContentLength());
            bean.setLanguage(appBean.getLanguage());
            bean.setRedirect(appBean.getRedirect());
            bean.setLocation(appBean.getLocation());
            bean.setPageTitle(appBean.getPageTitle());
            bean.setNote(appBean.getNote());
            bean.setDeferred(appBean.isDeferred());
            bean.setStatus(appBean.getStatus());
            bean.setRefreshStatus(appBean.getRefreshStatus());
            bean.setRefreshInterval(appBean.getRefreshInterval());
            bean.setNextRefreshTime(appBean.getNextRefreshTime());
            bean.setLastCheckedTime(appBean.getLastCheckedTime());
            bean.setLastUpdatedTime(appBean.getLastUpdatedTime());
            bean.setUrlSchemeFilter(appBean.getUrlSchemeFilter());
            bean.setPageAnchors(appBean.getPageAnchors());
            bean.setExcludeRelativeUrls(appBean.isExcludeRelativeUrls());
            bean.setExcludedBaseUrls(appBean.getExcludedBaseUrls());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<LinkList> convertAppLinkListBeanListToServerBeanList(List<LinkList> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<LinkList> beanList = new ArrayList<LinkList>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(LinkList sb : appBeanList) {
                com.pagesynopsis.ws.bean.LinkListBean bean = convertAppLinkListBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
