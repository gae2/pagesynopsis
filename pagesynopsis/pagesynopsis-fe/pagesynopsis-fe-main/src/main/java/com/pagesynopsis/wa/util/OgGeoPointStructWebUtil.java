package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.OgGeoPointStruct;
import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.fe.bean.OgGeoPointStructJsBean;


public class OgGeoPointStructWebUtil
{
    private static final Logger log = Logger.getLogger(OgGeoPointStructWebUtil.class.getName());

    // Static methods only.
    private OgGeoPointStructWebUtil() {}
    

    public static OgGeoPointStructJsBean convertOgGeoPointStructToJsBean(OgGeoPointStruct ogGeoPointStruct)
    {
        OgGeoPointStructJsBean jsBean = null;
        if(ogGeoPointStruct != null) {
            jsBean = new OgGeoPointStructJsBean();
            jsBean.setUuid(ogGeoPointStruct.getUuid());
            jsBean.setLatitude(ogGeoPointStruct.getLatitude());
            jsBean.setLongitude(ogGeoPointStruct.getLongitude());
            jsBean.setAltitude(ogGeoPointStruct.getAltitude());
        }
        return jsBean;
    }

    public static OgGeoPointStruct convertOgGeoPointStructJsBeanToBean(OgGeoPointStructJsBean jsBean)
    {
        OgGeoPointStructBean ogGeoPointStruct = null;
        if(jsBean != null) {
            ogGeoPointStruct = new OgGeoPointStructBean();
            ogGeoPointStruct.setUuid(jsBean.getUuid());
            ogGeoPointStruct.setLatitude(jsBean.getLatitude());
            ogGeoPointStruct.setLongitude(jsBean.getLongitude());
            ogGeoPointStruct.setAltitude(jsBean.getAltitude());
        }
        return ogGeoPointStruct;
    }

}
