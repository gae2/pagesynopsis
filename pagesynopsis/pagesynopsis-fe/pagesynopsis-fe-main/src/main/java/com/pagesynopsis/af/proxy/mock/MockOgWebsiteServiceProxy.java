package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.af.proxy.OgWebsiteServiceProxy;


// MockOgWebsiteServiceProxy is a decorator.
// It can be used as a base class to mock OgWebsiteServiceProxy objects.
public abstract class MockOgWebsiteServiceProxy implements OgWebsiteServiceProxy
{
    private static final Logger log = Logger.getLogger(MockOgWebsiteServiceProxy.class.getName());

    // MockOgWebsiteServiceProxy uses the decorator design pattern.
    private OgWebsiteServiceProxy decoratedProxy;

    public MockOgWebsiteServiceProxy(OgWebsiteServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected OgWebsiteServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(OgWebsiteServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public OgWebsite getOgWebsite(String guid) throws BaseException
    {
        return decoratedProxy.getOgWebsite(guid);
    }

    @Override
    public Object getOgWebsite(String guid, String field) throws BaseException
    {
        return decoratedProxy.getOgWebsite(guid, field);       
    }

    @Override
    public List<OgWebsite> getOgWebsites(List<String> guids) throws BaseException
    {
        return decoratedProxy.getOgWebsites(guids);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites() throws BaseException
    {
        return getAllOgWebsites(null, null, null);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgWebsites(ordering, offset, count);
        return getAllOgWebsites(ordering, offset, count, null);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgWebsites(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgWebsiteKeys(ordering, offset, count);
        return getAllOgWebsiteKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgWebsiteKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgWebsites(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgWebsite(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return decoratedProxy.createOgWebsite(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public String createOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        return decoratedProxy.createOgWebsite(ogWebsite);
    }

    @Override
    public Boolean updateOgWebsite(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return decoratedProxy.updateOgWebsite(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public Boolean updateOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        return decoratedProxy.updateOgWebsite(ogWebsite);
    }

    @Override
    public Boolean deleteOgWebsite(String guid) throws BaseException
    {
        return decoratedProxy.deleteOgWebsite(guid);
    }

    @Override
    public Boolean deleteOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        String guid = ogWebsite.getGuid();
        return deleteOgWebsite(guid);
    }

    @Override
    public Long deleteOgWebsites(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteOgWebsites(filter, params, values);
    }

}
