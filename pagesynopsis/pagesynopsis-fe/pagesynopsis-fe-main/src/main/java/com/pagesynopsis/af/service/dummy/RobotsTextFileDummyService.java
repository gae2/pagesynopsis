package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.RobotsTextFileService;


// The primary purpose of RobotsTextFileDummyService is to fake the service api, RobotsTextFileService.
// It has no real implementation.
public class RobotsTextFileDummyService implements RobotsTextFileService
{
    private static final Logger log = Logger.getLogger(RobotsTextFileDummyService.class.getName());

    public RobotsTextFileDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // RobotsTextFile related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public RobotsTextFile getRobotsTextFile(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getRobotsTextFile(): guid = " + guid);
        return null;
    }

    @Override
    public Object getRobotsTextFile(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getRobotsTextFile(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<RobotsTextFile> getRobotsTextFiles(List<String> guids) throws BaseException
    {
        log.fine("getRobotsTextFiles()");
        return null;
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles() throws BaseException
    {
        return getAllRobotsTextFiles(null, null, null);
    }


    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextFiles(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRobotsTextFiles(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextFileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRobotsTextFileKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findRobotsTextFiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextFileDummyService.findRobotsTextFiles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextFileDummyService.findRobotsTextFileKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextFileDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createRobotsTextFile(String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        log.finer("createRobotsTextFile()");
        return null;
    }

    @Override
    public String createRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("createRobotsTextFile()");
        return null;
    }

    @Override
    public RobotsTextFile constructRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("constructRobotsTextFile()");
        return null;
    }

    @Override
    public Boolean updateRobotsTextFile(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        log.finer("updateRobotsTextFile()");
        return null;
    }
        
    @Override
    public Boolean updateRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("updateRobotsTextFile()");
        return null;
    }

    @Override
    public RobotsTextFile refreshRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("refreshRobotsTextFile()");
        return null;
    }

    @Override
    public Boolean deleteRobotsTextFile(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteRobotsTextFile(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("deleteRobotsTextFile()");
        return null;
    }

    // TBD
    @Override
    public Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteRobotsTextFile(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createRobotsTextFiles(List<RobotsTextFile> robotsTextFiles) throws BaseException
    {
        log.finer("createRobotsTextFiles()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateRobotsTextFiles(List<RobotsTextFile> robotsTextFiles) throws BaseException
    //{
    //}

}
