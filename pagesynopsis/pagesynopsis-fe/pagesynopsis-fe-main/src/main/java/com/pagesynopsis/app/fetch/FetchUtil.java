package com.pagesynopsis.app.fetch;

import java.util.logging.Logger;

import com.pagesynopsis.af.util.URLUtil;
import com.pagesynopsis.helper.URLHelper;

public class FetchUtil
{
    private static final Logger log = Logger.getLogger(FetchUtil.class.getName());

    // Static methods only.
    private FetchUtil() {}


    // TBD:
    // targetUrl is assumed to be an absolute URL
    // otherwise, it won't work.
    // TBD
    // This implementation won't always work with a relative url that does not start with "/"...
    // ....
    public static String buildAbsoluteUrl(String strUrl, String targetUrl)
    {
        if(strUrl == null) {
            return null;
        }

        // Note:
        // Read the implementation carefully in liu of comments...
        String absoluteUrl = null;
        if(URLUtil.isValidUrl(strUrl)) {
            absoluteUrl = strUrl;
        } else {
            if(URLUtil.isValidUrl(targetUrl)) {
                if(strUrl.startsWith("/")) {
                    absoluteUrl = URLHelper.getInstance().getTopLevelURL(targetUrl);
                    if(absoluteUrl.endsWith("/")) {
                        absoluteUrl += strUrl.substring(1);
                    } else {
                        absoluteUrl += strUrl;                                        
                    }
                } else {
                    // TBD: It does not in general work with a relative url that does not start with "/"... ???
//                    if(URLUtil.hasQueryString(targetUrl)) {
//                        // ???
//                        String basePathUrl = URLHelper.getInstance().getTopLevelURL(targetUrl, true);
//                        //if(basePathUrl != null && !basePathUrl.isEmpty()) {
//                            absoluteUrl = basePathUrl;
//                        //} else {
//                        //    // Can this happen ???
//                        //    // Just ignore, for now???
//                        //    absoluteUrl = targetUrl;
//                        //}
//                    } else {
//                        // What happens if targetUrl has "#" tag???
//                        absoluteUrl = targetUrl;
//                    }
                    absoluteUrl = URLHelper.getInstance().getTopLevelURL(targetUrl, true, true);  // "Current folder" url...
                    if(absoluteUrl.endsWith("/")) {
                        absoluteUrl += strUrl;
                    } else {   // This should not happen...
                        absoluteUrl += "/" + strUrl;                                        
                    }
                }
            }
            // For performance, skip this for now...
            // if(!URLUtil.isValidUrl(absoluteUrl)) {
            //     absoluteUrl = null;
            //     log.info("Failed to compute the absolute URL for strUrl = " + strUrl);
            // }
        }
        
        return absoluteUrl;
    }
    
}
