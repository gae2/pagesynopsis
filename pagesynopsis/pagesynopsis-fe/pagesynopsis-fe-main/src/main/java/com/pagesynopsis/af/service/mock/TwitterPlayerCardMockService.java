package com.pagesynopsis.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterPlayerCardService;


// TwitterPlayerCardMockService is a decorator.
// It can be used as a base class to mock TwitterPlayerCardService objects.
public abstract class TwitterPlayerCardMockService implements TwitterPlayerCardService
{
    private static final Logger log = Logger.getLogger(TwitterPlayerCardMockService.class.getName());

    // TwitterPlayerCardMockService uses the decorator design pattern.
    private TwitterPlayerCardService decoratedService;

    public TwitterPlayerCardMockService(TwitterPlayerCardService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected TwitterPlayerCardService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(TwitterPlayerCardService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // TwitterPlayerCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterPlayerCard getTwitterPlayerCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterPlayerCard(): guid = " + guid);
        TwitterPlayerCard bean = decoratedService.getTwitterPlayerCard(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterPlayerCard(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getTwitterPlayerCard(guid, field);
        return obj;
    }

    @Override
    public List<TwitterPlayerCard> getTwitterPlayerCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterPlayerCards()");
        List<TwitterPlayerCard> twitterPlayerCards = decoratedService.getTwitterPlayerCards(guids);
        log.finer("END");
        return twitterPlayerCards;
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards() throws BaseException
    {
        return getAllTwitterPlayerCards(null, null, null);
    }


    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPlayerCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterPlayerCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<TwitterPlayerCard> twitterPlayerCards = decoratedService.getAllTwitterPlayerCards(ordering, offset, count, forwardCursor);
        log.finer("END");
        return twitterPlayerCards;
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPlayerCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterPlayerCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllTwitterPlayerCardKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPlayerCardMockService.findTwitterPlayerCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<TwitterPlayerCard> twitterPlayerCards = decoratedService.findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return twitterPlayerCards;
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPlayerCardMockService.findTwitterPlayerCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPlayerCardMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterPlayerCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        TwitterPlayerCardBean bean = new TwitterPlayerCardBean(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
        return createTwitterPlayerCard(bean);
    }

    @Override
    public String createTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createTwitterPlayerCard(twitterPlayerCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TwitterPlayerCard constructTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");
        TwitterPlayerCard bean = decoratedService.constructTwitterPlayerCard(twitterPlayerCard);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterPlayerCardBean bean = new TwitterPlayerCardBean(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
        return updateTwitterPlayerCard(bean);
    }
        
    @Override
    public Boolean updateTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateTwitterPlayerCard(twitterPlayerCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TwitterPlayerCard refreshTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");
        TwitterPlayerCard bean = decoratedService.refreshTwitterPlayerCard(twitterPlayerCard);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTwitterPlayerCard(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterPlayerCard(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterPlayerCard(twitterPlayerCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteTwitterPlayerCards(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createTwitterPlayerCards(twitterPlayerCards);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException
    //{
    //}

}
