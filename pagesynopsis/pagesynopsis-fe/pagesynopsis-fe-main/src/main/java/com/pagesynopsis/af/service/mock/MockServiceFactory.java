package com.pagesynopsis.af.service.mock;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.service.AbstractServiceFactory;
import com.pagesynopsis.af.service.ApiConsumerService;
import com.pagesynopsis.af.service.UserService;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.OgProfileService;
import com.pagesynopsis.af.service.OgWebsiteService;
import com.pagesynopsis.af.service.OgBlogService;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.af.service.OgVideoService;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.af.service.TwitterSummaryCardService;
import com.pagesynopsis.af.service.TwitterPhotoCardService;
import com.pagesynopsis.af.service.TwitterGalleryCardService;
import com.pagesynopsis.af.service.TwitterAppCardService;
import com.pagesynopsis.af.service.TwitterPlayerCardService;
import com.pagesynopsis.af.service.TwitterProductCardService;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.af.service.PageInfoService;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.af.service.ImageSetService;
import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.af.service.OpenGraphMetaService;
import com.pagesynopsis.af.service.TwitterCardMetaService;
import com.pagesynopsis.af.service.DomainInfoService;
import com.pagesynopsis.af.service.UrlRatingService;
import com.pagesynopsis.af.service.ServiceInfoService;
import com.pagesynopsis.af.service.FiveTenService;


// Create your own mock object factory using MockServiceFactory as a template.
public class MockServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(MockServiceFactory.class.getName());

    // Using the Decorator pattern.
    private AbstractServiceFactory decoratedServiceFactory;
    private MockServiceFactory()
    {
        this(null);   // ????
    }
    private MockServiceFactory(AbstractServiceFactory decoratedServiceFactory)
    {
        this.decoratedServiceFactory = decoratedServiceFactory;
    }

    // Initialization-on-demand holder.
    private static class MockServiceFactoryHolder
    {
        private static final MockServiceFactory INSTANCE = new MockServiceFactory();
    }

    // Singleton method
    public static MockServiceFactory getInstance()
    {
        return MockServiceFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public AbstractServiceFactory getDecoratedServiceFactory()
    {
        return decoratedServiceFactory;
    }
    public void setDecoratedServiceFactory(AbstractServiceFactory decoratedServiceFactory)
    {
        this.decoratedServiceFactory = decoratedServiceFactory;
    }


    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerMockService(decoratedServiceFactory.getApiConsumerService()) {};
    }

    @Override
    public UserService getUserService()
    {
        return new UserMockService(decoratedServiceFactory.getUserService()) {};
    }

    @Override
    public RobotsTextFileService getRobotsTextFileService()
    {
        return new RobotsTextFileMockService(decoratedServiceFactory.getRobotsTextFileService()) {};
    }

    @Override
    public RobotsTextRefreshService getRobotsTextRefreshService()
    {
        return new RobotsTextRefreshMockService(decoratedServiceFactory.getRobotsTextRefreshService()) {};
    }

    @Override
    public OgProfileService getOgProfileService()
    {
        return new OgProfileMockService(decoratedServiceFactory.getOgProfileService()) {};
    }

    @Override
    public OgWebsiteService getOgWebsiteService()
    {
        return new OgWebsiteMockService(decoratedServiceFactory.getOgWebsiteService()) {};
    }

    @Override
    public OgBlogService getOgBlogService()
    {
        return new OgBlogMockService(decoratedServiceFactory.getOgBlogService()) {};
    }

    @Override
    public OgArticleService getOgArticleService()
    {
        return new OgArticleMockService(decoratedServiceFactory.getOgArticleService()) {};
    }

    @Override
    public OgBookService getOgBookService()
    {
        return new OgBookMockService(decoratedServiceFactory.getOgBookService()) {};
    }

    @Override
    public OgVideoService getOgVideoService()
    {
        return new OgVideoMockService(decoratedServiceFactory.getOgVideoService()) {};
    }

    @Override
    public OgMovieService getOgMovieService()
    {
        return new OgMovieMockService(decoratedServiceFactory.getOgMovieService()) {};
    }

    @Override
    public OgTvShowService getOgTvShowService()
    {
        return new OgTvShowMockService(decoratedServiceFactory.getOgTvShowService()) {};
    }

    @Override
    public OgTvEpisodeService getOgTvEpisodeService()
    {
        return new OgTvEpisodeMockService(decoratedServiceFactory.getOgTvEpisodeService()) {};
    }

    @Override
    public TwitterSummaryCardService getTwitterSummaryCardService()
    {
        return new TwitterSummaryCardMockService(decoratedServiceFactory.getTwitterSummaryCardService()) {};
    }

    @Override
    public TwitterPhotoCardService getTwitterPhotoCardService()
    {
        return new TwitterPhotoCardMockService(decoratedServiceFactory.getTwitterPhotoCardService()) {};
    }

    @Override
    public TwitterGalleryCardService getTwitterGalleryCardService()
    {
        return new TwitterGalleryCardMockService(decoratedServiceFactory.getTwitterGalleryCardService()) {};
    }

    @Override
    public TwitterAppCardService getTwitterAppCardService()
    {
        return new TwitterAppCardMockService(decoratedServiceFactory.getTwitterAppCardService()) {};
    }

    @Override
    public TwitterPlayerCardService getTwitterPlayerCardService()
    {
        return new TwitterPlayerCardMockService(decoratedServiceFactory.getTwitterPlayerCardService()) {};
    }

    @Override
    public TwitterProductCardService getTwitterProductCardService()
    {
        return new TwitterProductCardMockService(decoratedServiceFactory.getTwitterProductCardService()) {};
    }

    @Override
    public FetchRequestService getFetchRequestService()
    {
        return new FetchRequestMockService(decoratedServiceFactory.getFetchRequestService()) {};
    }

    @Override
    public PageInfoService getPageInfoService()
    {
        return new PageInfoMockService(decoratedServiceFactory.getPageInfoService()) {};
    }

    @Override
    public PageFetchService getPageFetchService()
    {
        return new PageFetchMockService(decoratedServiceFactory.getPageFetchService()) {};
    }

    @Override
    public LinkListService getLinkListService()
    {
        return new LinkListMockService(decoratedServiceFactory.getLinkListService()) {};
    }

    @Override
    public ImageSetService getImageSetService()
    {
        return new ImageSetMockService(decoratedServiceFactory.getImageSetService()) {};
    }

    @Override
    public AudioSetService getAudioSetService()
    {
        return new AudioSetMockService(decoratedServiceFactory.getAudioSetService()) {};
    }

    @Override
    public VideoSetService getVideoSetService()
    {
        return new VideoSetMockService(decoratedServiceFactory.getVideoSetService()) {};
    }

    @Override
    public OpenGraphMetaService getOpenGraphMetaService()
    {
        return new OpenGraphMetaMockService(decoratedServiceFactory.getOpenGraphMetaService()) {};
    }

    @Override
    public TwitterCardMetaService getTwitterCardMetaService()
    {
        return new TwitterCardMetaMockService(decoratedServiceFactory.getTwitterCardMetaService()) {};
    }

    @Override
    public DomainInfoService getDomainInfoService()
    {
        return new DomainInfoMockService(decoratedServiceFactory.getDomainInfoService()) {};
    }

    @Override
    public UrlRatingService getUrlRatingService()
    {
        return new UrlRatingMockService(decoratedServiceFactory.getUrlRatingService()) {};
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoMockService(decoratedServiceFactory.getServiceInfoService()) {};
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenMockService(decoratedServiceFactory.getFiveTenService()) {};
    }


}
