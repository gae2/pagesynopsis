package com.pagesynopsis.af.proxy.mock;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.ApiConsumerServiceProxy;
import com.pagesynopsis.af.proxy.UserServiceProxy;
import com.pagesynopsis.af.proxy.RobotsTextFileServiceProxy;
import com.pagesynopsis.af.proxy.RobotsTextRefreshServiceProxy;
import com.pagesynopsis.af.proxy.OgProfileServiceProxy;
import com.pagesynopsis.af.proxy.OgWebsiteServiceProxy;
import com.pagesynopsis.af.proxy.OgBlogServiceProxy;
import com.pagesynopsis.af.proxy.OgArticleServiceProxy;
import com.pagesynopsis.af.proxy.OgBookServiceProxy;
import com.pagesynopsis.af.proxy.OgVideoServiceProxy;
import com.pagesynopsis.af.proxy.OgMovieServiceProxy;
import com.pagesynopsis.af.proxy.OgTvShowServiceProxy;
import com.pagesynopsis.af.proxy.OgTvEpisodeServiceProxy;
import com.pagesynopsis.af.proxy.TwitterSummaryCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterPhotoCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterGalleryCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterAppCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterPlayerCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterProductCardServiceProxy;
import com.pagesynopsis.af.proxy.FetchRequestServiceProxy;
import com.pagesynopsis.af.proxy.PageInfoServiceProxy;
import com.pagesynopsis.af.proxy.PageFetchServiceProxy;
import com.pagesynopsis.af.proxy.LinkListServiceProxy;
import com.pagesynopsis.af.proxy.ImageSetServiceProxy;
import com.pagesynopsis.af.proxy.AudioSetServiceProxy;
import com.pagesynopsis.af.proxy.VideoSetServiceProxy;
import com.pagesynopsis.af.proxy.OpenGraphMetaServiceProxy;
import com.pagesynopsis.af.proxy.TwitterCardMetaServiceProxy;
import com.pagesynopsis.af.proxy.DomainInfoServiceProxy;
import com.pagesynopsis.af.proxy.UrlRatingServiceProxy;
import com.pagesynopsis.af.proxy.ServiceInfoServiceProxy;
import com.pagesynopsis.af.proxy.FiveTenServiceProxy;


// Create your own mock object factory using MockProxyFactory as a template.
public class MockProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(MockProxyFactory.class.getName());

    // Using the Decorator pattern.
    private AbstractProxyFactory decoratedProxyFactory;
    private MockProxyFactory()
    {
        this(null);   // ????
    }
    private MockProxyFactory(AbstractProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }

    // Initialization-on-demand holder.
    private static class MockProxyFactoryHolder
    {
        private static final MockProxyFactory INSTANCE = new MockProxyFactory();
    }

    // Singleton method
    public static MockProxyFactory getInstance()
    {
        return MockProxyFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public AbstractProxyFactory getDecoratedProxyFactory()
    {
        return decoratedProxyFactory;
    }
    public void setDecoratedProxyFactory(AbstractProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }


    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new MockApiConsumerServiceProxy(decoratedProxyFactory.getApiConsumerServiceProxy()) {};
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new MockUserServiceProxy(decoratedProxyFactory.getUserServiceProxy()) {};
    }

    @Override
    public RobotsTextFileServiceProxy getRobotsTextFileServiceProxy()
    {
        return new MockRobotsTextFileServiceProxy(decoratedProxyFactory.getRobotsTextFileServiceProxy()) {};
    }

    @Override
    public RobotsTextRefreshServiceProxy getRobotsTextRefreshServiceProxy()
    {
        return new MockRobotsTextRefreshServiceProxy(decoratedProxyFactory.getRobotsTextRefreshServiceProxy()) {};
    }

    @Override
    public OgProfileServiceProxy getOgProfileServiceProxy()
    {
        return new MockOgProfileServiceProxy(decoratedProxyFactory.getOgProfileServiceProxy()) {};
    }

    @Override
    public OgWebsiteServiceProxy getOgWebsiteServiceProxy()
    {
        return new MockOgWebsiteServiceProxy(decoratedProxyFactory.getOgWebsiteServiceProxy()) {};
    }

    @Override
    public OgBlogServiceProxy getOgBlogServiceProxy()
    {
        return new MockOgBlogServiceProxy(decoratedProxyFactory.getOgBlogServiceProxy()) {};
    }

    @Override
    public OgArticleServiceProxy getOgArticleServiceProxy()
    {
        return new MockOgArticleServiceProxy(decoratedProxyFactory.getOgArticleServiceProxy()) {};
    }

    @Override
    public OgBookServiceProxy getOgBookServiceProxy()
    {
        return new MockOgBookServiceProxy(decoratedProxyFactory.getOgBookServiceProxy()) {};
    }

    @Override
    public OgVideoServiceProxy getOgVideoServiceProxy()
    {
        return new MockOgVideoServiceProxy(decoratedProxyFactory.getOgVideoServiceProxy()) {};
    }

    @Override
    public OgMovieServiceProxy getOgMovieServiceProxy()
    {
        return new MockOgMovieServiceProxy(decoratedProxyFactory.getOgMovieServiceProxy()) {};
    }

    @Override
    public OgTvShowServiceProxy getOgTvShowServiceProxy()
    {
        return new MockOgTvShowServiceProxy(decoratedProxyFactory.getOgTvShowServiceProxy()) {};
    }

    @Override
    public OgTvEpisodeServiceProxy getOgTvEpisodeServiceProxy()
    {
        return new MockOgTvEpisodeServiceProxy(decoratedProxyFactory.getOgTvEpisodeServiceProxy()) {};
    }

    @Override
    public TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy()
    {
        return new MockTwitterSummaryCardServiceProxy(decoratedProxyFactory.getTwitterSummaryCardServiceProxy()) {};
    }

    @Override
    public TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy()
    {
        return new MockTwitterPhotoCardServiceProxy(decoratedProxyFactory.getTwitterPhotoCardServiceProxy()) {};
    }

    @Override
    public TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy()
    {
        return new MockTwitterGalleryCardServiceProxy(decoratedProxyFactory.getTwitterGalleryCardServiceProxy()) {};
    }

    @Override
    public TwitterAppCardServiceProxy getTwitterAppCardServiceProxy()
    {
        return new MockTwitterAppCardServiceProxy(decoratedProxyFactory.getTwitterAppCardServiceProxy()) {};
    }

    @Override
    public TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy()
    {
        return new MockTwitterPlayerCardServiceProxy(decoratedProxyFactory.getTwitterPlayerCardServiceProxy()) {};
    }

    @Override
    public TwitterProductCardServiceProxy getTwitterProductCardServiceProxy()
    {
        return new MockTwitterProductCardServiceProxy(decoratedProxyFactory.getTwitterProductCardServiceProxy()) {};
    }

    @Override
    public FetchRequestServiceProxy getFetchRequestServiceProxy()
    {
        return new MockFetchRequestServiceProxy(decoratedProxyFactory.getFetchRequestServiceProxy()) {};
    }

    @Override
    public PageInfoServiceProxy getPageInfoServiceProxy()
    {
        return new MockPageInfoServiceProxy(decoratedProxyFactory.getPageInfoServiceProxy()) {};
    }

    @Override
    public PageFetchServiceProxy getPageFetchServiceProxy()
    {
        return new MockPageFetchServiceProxy(decoratedProxyFactory.getPageFetchServiceProxy()) {};
    }

    @Override
    public LinkListServiceProxy getLinkListServiceProxy()
    {
        return new MockLinkListServiceProxy(decoratedProxyFactory.getLinkListServiceProxy()) {};
    }

    @Override
    public ImageSetServiceProxy getImageSetServiceProxy()
    {
        return new MockImageSetServiceProxy(decoratedProxyFactory.getImageSetServiceProxy()) {};
    }

    @Override
    public AudioSetServiceProxy getAudioSetServiceProxy()
    {
        return new MockAudioSetServiceProxy(decoratedProxyFactory.getAudioSetServiceProxy()) {};
    }

    @Override
    public VideoSetServiceProxy getVideoSetServiceProxy()
    {
        return new MockVideoSetServiceProxy(decoratedProxyFactory.getVideoSetServiceProxy()) {};
    }

    @Override
    public OpenGraphMetaServiceProxy getOpenGraphMetaServiceProxy()
    {
        return new MockOpenGraphMetaServiceProxy(decoratedProxyFactory.getOpenGraphMetaServiceProxy()) {};
    }

    @Override
    public TwitterCardMetaServiceProxy getTwitterCardMetaServiceProxy()
    {
        return new MockTwitterCardMetaServiceProxy(decoratedProxyFactory.getTwitterCardMetaServiceProxy()) {};
    }

    @Override
    public DomainInfoServiceProxy getDomainInfoServiceProxy()
    {
        return new MockDomainInfoServiceProxy(decoratedProxyFactory.getDomainInfoServiceProxy()) {};
    }

    @Override
    public UrlRatingServiceProxy getUrlRatingServiceProxy()
    {
        return new MockUrlRatingServiceProxy(decoratedProxyFactory.getUrlRatingServiceProxy()) {};
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new MockServiceInfoServiceProxy(decoratedProxyFactory.getServiceInfoServiceProxy()) {};
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new MockFiveTenServiceProxy(decoratedProxyFactory.getFiveTenServiceProxy()) {};
    }

}
