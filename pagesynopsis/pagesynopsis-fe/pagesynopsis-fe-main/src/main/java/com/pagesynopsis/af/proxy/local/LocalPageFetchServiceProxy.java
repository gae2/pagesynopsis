package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
// import com.pagesynopsis.ws.bean.PageFetchBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.PageFetchService;
import com.pagesynopsis.af.bean.PageFetchBean;
import com.pagesynopsis.af.proxy.PageFetchServiceProxy;


public class LocalPageFetchServiceProxy extends BaseLocalServiceProxy implements PageFetchServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalPageFetchServiceProxy.class.getName());

    public LocalPageFetchServiceProxy()
    {
    }

    @Override
    public PageFetch getPageFetch(String guid) throws BaseException
    {
        PageFetch serverBean = getPageFetchService().getPageFetch(guid);
        PageFetch appBean = convertServerPageFetchBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getPageFetch(String guid, String field) throws BaseException
    {
        return getPageFetchService().getPageFetch(guid, field);       
    }

    @Override
    public List<PageFetch> getPageFetches(List<String> guids) throws BaseException
    {
        List<PageFetch> serverBeanList = getPageFetchService().getPageFetches(guids);
        List<PageFetch> appBeanList = convertServerPageFetchBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<PageFetch> getAllPageFetches() throws BaseException
    {
        return getAllPageFetches(null, null, null);
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getPageFetchService().getAllPageFetches(ordering, offset, count);
        return getAllPageFetches(ordering, offset, count, null);
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<PageFetch> serverBeanList = getPageFetchService().getAllPageFetches(ordering, offset, count, forwardCursor);
        List<PageFetch> appBeanList = convertServerPageFetchBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getPageFetchService().getAllPageFetchKeys(ordering, offset, count);
        return getAllPageFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getPageFetchService().getAllPageFetchKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findPageFetches(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getPageFetchService().findPageFetches(filter, ordering, params, values, grouping, unique, offset, count);
        return findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<PageFetch> serverBeanList = getPageFetchService().findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<PageFetch> appBeanList = convertServerPageFetchBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getPageFetchService().findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getPageFetchService().findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getPageFetchService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createPageFetch(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {
        return getPageFetchService().createPageFetch(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
    }

    @Override
    public String createPageFetch(PageFetch pageFetch) throws BaseException
    {
        com.pagesynopsis.ws.bean.PageFetchBean serverBean =  convertAppPageFetchBeanToServerBean(pageFetch);
        return getPageFetchService().createPageFetch(serverBean);
    }

    @Override
    public Boolean updatePageFetch(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {
        return getPageFetchService().updatePageFetch(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
    }

    @Override
    public Boolean updatePageFetch(PageFetch pageFetch) throws BaseException
    {
        com.pagesynopsis.ws.bean.PageFetchBean serverBean =  convertAppPageFetchBeanToServerBean(pageFetch);
        return getPageFetchService().updatePageFetch(serverBean);
    }

    @Override
    public Boolean deletePageFetch(String guid) throws BaseException
    {
        return getPageFetchService().deletePageFetch(guid);
    }

    @Override
    public Boolean deletePageFetch(PageFetch pageFetch) throws BaseException
    {
        com.pagesynopsis.ws.bean.PageFetchBean serverBean =  convertAppPageFetchBeanToServerBean(pageFetch);
        return getPageFetchService().deletePageFetch(serverBean);
    }

    @Override
    public Long deletePageFetches(String filter, String params, List<String> values) throws BaseException
    {
        return getPageFetchService().deletePageFetches(filter, params, values);
    }




    public static PageFetchBean convertServerPageFetchBeanToAppBean(PageFetch serverBean)
    {
        PageFetchBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new PageFetchBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setFetchRequest(serverBean.getFetchRequest());
            bean.setTargetUrl(serverBean.getTargetUrl());
            bean.setPageUrl(serverBean.getPageUrl());
            bean.setQueryString(serverBean.getQueryString());
            bean.setQueryParams(serverBean.getQueryParams());
            bean.setLastFetchResult(serverBean.getLastFetchResult());
            bean.setResponseCode(serverBean.getResponseCode());
            bean.setContentType(serverBean.getContentType());
            bean.setContentLength(serverBean.getContentLength());
            bean.setLanguage(serverBean.getLanguage());
            bean.setRedirect(serverBean.getRedirect());
            bean.setLocation(serverBean.getLocation());
            bean.setPageTitle(serverBean.getPageTitle());
            bean.setNote(serverBean.getNote());
            bean.setDeferred(serverBean.isDeferred());
            bean.setStatus(serverBean.getStatus());
            bean.setRefreshStatus(serverBean.getRefreshStatus());
            bean.setRefreshInterval(serverBean.getRefreshInterval());
            bean.setNextRefreshTime(serverBean.getNextRefreshTime());
            bean.setLastCheckedTime(serverBean.getLastCheckedTime());
            bean.setLastUpdatedTime(serverBean.getLastUpdatedTime());
            bean.setInputMaxRedirects(serverBean.getInputMaxRedirects());
            bean.setResultRedirectCount(serverBean.getResultRedirectCount());
            bean.setRedirectPages(serverBean.getRedirectPages());
            bean.setDestinationUrl(serverBean.getDestinationUrl());
            bean.setPageAuthor(serverBean.getPageAuthor());
            bean.setPageSummary(serverBean.getPageSummary());
            bean.setFavicon(serverBean.getFavicon());
            bean.setFaviconUrl(serverBean.getFaviconUrl());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<PageFetch> convertServerPageFetchBeanListToAppBeanList(List<PageFetch> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<PageFetch> beanList = new ArrayList<PageFetch>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(PageFetch sb : serverBeanList) {
                PageFetchBean bean = convertServerPageFetchBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.PageFetchBean convertAppPageFetchBeanToServerBean(PageFetch appBean)
    {
        com.pagesynopsis.ws.bean.PageFetchBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.PageFetchBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setFetchRequest(appBean.getFetchRequest());
            bean.setTargetUrl(appBean.getTargetUrl());
            bean.setPageUrl(appBean.getPageUrl());
            bean.setQueryString(appBean.getQueryString());
            bean.setQueryParams(appBean.getQueryParams());
            bean.setLastFetchResult(appBean.getLastFetchResult());
            bean.setResponseCode(appBean.getResponseCode());
            bean.setContentType(appBean.getContentType());
            bean.setContentLength(appBean.getContentLength());
            bean.setLanguage(appBean.getLanguage());
            bean.setRedirect(appBean.getRedirect());
            bean.setLocation(appBean.getLocation());
            bean.setPageTitle(appBean.getPageTitle());
            bean.setNote(appBean.getNote());
            bean.setDeferred(appBean.isDeferred());
            bean.setStatus(appBean.getStatus());
            bean.setRefreshStatus(appBean.getRefreshStatus());
            bean.setRefreshInterval(appBean.getRefreshInterval());
            bean.setNextRefreshTime(appBean.getNextRefreshTime());
            bean.setLastCheckedTime(appBean.getLastCheckedTime());
            bean.setLastUpdatedTime(appBean.getLastUpdatedTime());
            bean.setInputMaxRedirects(appBean.getInputMaxRedirects());
            bean.setResultRedirectCount(appBean.getResultRedirectCount());
            bean.setRedirectPages(appBean.getRedirectPages());
            bean.setDestinationUrl(appBean.getDestinationUrl());
            bean.setPageAuthor(appBean.getPageAuthor());
            bean.setPageSummary(appBean.getPageSummary());
            bean.setFavicon(appBean.getFavicon());
            bean.setFaviconUrl(appBean.getFaviconUrl());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<PageFetch> convertAppPageFetchBeanListToServerBeanList(List<PageFetch> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<PageFetch> beanList = new ArrayList<PageFetch>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(PageFetch sb : appBeanList) {
                com.pagesynopsis.ws.bean.PageFetchBean bean = convertAppPageFetchBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
