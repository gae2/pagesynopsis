package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.ImageSet;
// import com.pagesynopsis.ws.bean.ImageSetBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.ImageSetService;
import com.pagesynopsis.af.bean.ImageSetBean;
import com.pagesynopsis.af.proxy.ImageSetServiceProxy;


public class LocalImageSetServiceProxy extends BaseLocalServiceProxy implements ImageSetServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalImageSetServiceProxy.class.getName());

    public LocalImageSetServiceProxy()
    {
    }

    @Override
    public ImageSet getImageSet(String guid) throws BaseException
    {
        ImageSet serverBean = getImageSetService().getImageSet(guid);
        ImageSet appBean = convertServerImageSetBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getImageSet(String guid, String field) throws BaseException
    {
        return getImageSetService().getImageSet(guid, field);       
    }

    @Override
    public List<ImageSet> getImageSets(List<String> guids) throws BaseException
    {
        List<ImageSet> serverBeanList = getImageSetService().getImageSets(guids);
        List<ImageSet> appBeanList = convertServerImageSetBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<ImageSet> getAllImageSets() throws BaseException
    {
        return getAllImageSets(null, null, null);
    }

    @Override
    public List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getImageSetService().getAllImageSets(ordering, offset, count);
        return getAllImageSets(ordering, offset, count, null);
    }

    @Override
    public List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ImageSet> serverBeanList = getImageSetService().getAllImageSets(ordering, offset, count, forwardCursor);
        List<ImageSet> appBeanList = convertServerImageSetBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getImageSetService().getAllImageSetKeys(ordering, offset, count);
        return getAllImageSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getImageSetService().getAllImageSetKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findImageSets(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getImageSetService().findImageSets(filter, ordering, params, values, grouping, unique, offset, count);
        return findImageSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<ImageSet> serverBeanList = getImageSetService().findImageSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<ImageSet> appBeanList = convertServerImageSetBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getImageSetService().findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getImageSetService().findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getImageSetService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createImageSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException
    {
        return getImageSetService().createImageSet(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageImages);
    }

    @Override
    public String createImageSet(ImageSet imageSet) throws BaseException
    {
        com.pagesynopsis.ws.bean.ImageSetBean serverBean =  convertAppImageSetBeanToServerBean(imageSet);
        return getImageSetService().createImageSet(serverBean);
    }

    @Override
    public Boolean updateImageSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException
    {
        return getImageSetService().updateImageSet(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageImages);
    }

    @Override
    public Boolean updateImageSet(ImageSet imageSet) throws BaseException
    {
        com.pagesynopsis.ws.bean.ImageSetBean serverBean =  convertAppImageSetBeanToServerBean(imageSet);
        return getImageSetService().updateImageSet(serverBean);
    }

    @Override
    public Boolean deleteImageSet(String guid) throws BaseException
    {
        return getImageSetService().deleteImageSet(guid);
    }

    @Override
    public Boolean deleteImageSet(ImageSet imageSet) throws BaseException
    {
        com.pagesynopsis.ws.bean.ImageSetBean serverBean =  convertAppImageSetBeanToServerBean(imageSet);
        return getImageSetService().deleteImageSet(serverBean);
    }

    @Override
    public Long deleteImageSets(String filter, String params, List<String> values) throws BaseException
    {
        return getImageSetService().deleteImageSets(filter, params, values);
    }




    public static ImageSetBean convertServerImageSetBeanToAppBean(ImageSet serverBean)
    {
        ImageSetBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new ImageSetBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setFetchRequest(serverBean.getFetchRequest());
            bean.setTargetUrl(serverBean.getTargetUrl());
            bean.setPageUrl(serverBean.getPageUrl());
            bean.setQueryString(serverBean.getQueryString());
            bean.setQueryParams(serverBean.getQueryParams());
            bean.setLastFetchResult(serverBean.getLastFetchResult());
            bean.setResponseCode(serverBean.getResponseCode());
            bean.setContentType(serverBean.getContentType());
            bean.setContentLength(serverBean.getContentLength());
            bean.setLanguage(serverBean.getLanguage());
            bean.setRedirect(serverBean.getRedirect());
            bean.setLocation(serverBean.getLocation());
            bean.setPageTitle(serverBean.getPageTitle());
            bean.setNote(serverBean.getNote());
            bean.setDeferred(serverBean.isDeferred());
            bean.setStatus(serverBean.getStatus());
            bean.setRefreshStatus(serverBean.getRefreshStatus());
            bean.setRefreshInterval(serverBean.getRefreshInterval());
            bean.setNextRefreshTime(serverBean.getNextRefreshTime());
            bean.setLastCheckedTime(serverBean.getLastCheckedTime());
            bean.setLastUpdatedTime(serverBean.getLastUpdatedTime());
            bean.setMediaTypeFilter(serverBean.getMediaTypeFilter());
            bean.setPageImages(serverBean.getPageImages());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ImageSet> convertServerImageSetBeanListToAppBeanList(List<ImageSet> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<ImageSet> beanList = new ArrayList<ImageSet>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(ImageSet sb : serverBeanList) {
                ImageSetBean bean = convertServerImageSetBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.ImageSetBean convertAppImageSetBeanToServerBean(ImageSet appBean)
    {
        com.pagesynopsis.ws.bean.ImageSetBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.ImageSetBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setFetchRequest(appBean.getFetchRequest());
            bean.setTargetUrl(appBean.getTargetUrl());
            bean.setPageUrl(appBean.getPageUrl());
            bean.setQueryString(appBean.getQueryString());
            bean.setQueryParams(appBean.getQueryParams());
            bean.setLastFetchResult(appBean.getLastFetchResult());
            bean.setResponseCode(appBean.getResponseCode());
            bean.setContentType(appBean.getContentType());
            bean.setContentLength(appBean.getContentLength());
            bean.setLanguage(appBean.getLanguage());
            bean.setRedirect(appBean.getRedirect());
            bean.setLocation(appBean.getLocation());
            bean.setPageTitle(appBean.getPageTitle());
            bean.setNote(appBean.getNote());
            bean.setDeferred(appBean.isDeferred());
            bean.setStatus(appBean.getStatus());
            bean.setRefreshStatus(appBean.getRefreshStatus());
            bean.setRefreshInterval(appBean.getRefreshInterval());
            bean.setNextRefreshTime(appBean.getNextRefreshTime());
            bean.setLastCheckedTime(appBean.getLastCheckedTime());
            bean.setLastUpdatedTime(appBean.getLastUpdatedTime());
            bean.setMediaTypeFilter(appBean.getMediaTypeFilter());
            bean.setPageImages(appBean.getPageImages());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<ImageSet> convertAppImageSetBeanListToServerBeanList(List<ImageSet> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<ImageSet> beanList = new ArrayList<ImageSet>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(ImageSet sb : appBeanList) {
                com.pagesynopsis.ws.bean.ImageSetBean bean = convertAppImageSetBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
