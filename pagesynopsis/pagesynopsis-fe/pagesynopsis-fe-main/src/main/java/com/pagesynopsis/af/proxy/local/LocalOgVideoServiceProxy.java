package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgVideo;
// import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.proxy.OgVideoServiceProxy;


public class LocalOgVideoServiceProxy extends BaseLocalServiceProxy implements OgVideoServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgVideoServiceProxy.class.getName());

    public LocalOgVideoServiceProxy()
    {
    }

    @Override
    public OgVideo getOgVideo(String guid) throws BaseException
    {
        OgVideo serverBean = getOgVideoService().getOgVideo(guid);
        OgVideo appBean = convertServerOgVideoBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getOgVideo(String guid, String field) throws BaseException
    {
        return getOgVideoService().getOgVideo(guid, field);       
    }

    @Override
    public List<OgVideo> getOgVideos(List<String> guids) throws BaseException
    {
        List<OgVideo> serverBeanList = getOgVideoService().getOgVideos(guids);
        List<OgVideo> appBeanList = convertServerOgVideoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<OgVideo> getAllOgVideos() throws BaseException
    {
        return getAllOgVideos(null, null, null);
    }

    @Override
    public List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgVideoService().getAllOgVideos(ordering, offset, count);
        return getAllOgVideos(ordering, offset, count, null);
    }

    @Override
    public List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgVideo> serverBeanList = getOgVideoService().getAllOgVideos(ordering, offset, count, forwardCursor);
        List<OgVideo> appBeanList = convertServerOgVideoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgVideoService().getAllOgVideoKeys(ordering, offset, count);
        return getAllOgVideoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgVideoService().getAllOgVideoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgVideos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgVideoService().findOgVideos(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgVideo> serverBeanList = getOgVideoService().findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<OgVideo> appBeanList = convertServerOgVideoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgVideoService().findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgVideoService().findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgVideoService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgVideo(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgVideoService().createOgVideo(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public String createOgVideo(OgVideo ogVideo) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgVideoBean serverBean =  convertAppOgVideoBeanToServerBean(ogVideo);
        return getOgVideoService().createOgVideo(serverBean);
    }

    @Override
    public Boolean updateOgVideo(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgVideoService().updateOgVideo(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public Boolean updateOgVideo(OgVideo ogVideo) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgVideoBean serverBean =  convertAppOgVideoBeanToServerBean(ogVideo);
        return getOgVideoService().updateOgVideo(serverBean);
    }

    @Override
    public Boolean deleteOgVideo(String guid) throws BaseException
    {
        return getOgVideoService().deleteOgVideo(guid);
    }

    @Override
    public Boolean deleteOgVideo(OgVideo ogVideo) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgVideoBean serverBean =  convertAppOgVideoBeanToServerBean(ogVideo);
        return getOgVideoService().deleteOgVideo(serverBean);
    }

    @Override
    public Long deleteOgVideos(String filter, String params, List<String> values) throws BaseException
    {
        return getOgVideoService().deleteOgVideos(filter, params, values);
    }




    public static OgVideoBean convertServerOgVideoBeanToAppBean(OgVideo serverBean)
    {
        OgVideoBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new OgVideoBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUrl(serverBean.getUrl());
            bean.setType(serverBean.getType());
            bean.setSiteName(serverBean.getSiteName());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setFbAdmins(serverBean.getFbAdmins());
            bean.setFbAppId(serverBean.getFbAppId());
            bean.setImage(serverBean.getImage());
            bean.setAudio(serverBean.getAudio());
            bean.setVideo(serverBean.getVideo());
            bean.setLocale(serverBean.getLocale());
            bean.setLocaleAlternate(serverBean.getLocaleAlternate());
            bean.setDirector(serverBean.getDirector());
            bean.setWriter(serverBean.getWriter());
            bean.setActor(serverBean.getActor());
            bean.setDuration(serverBean.getDuration());
            bean.setTag(serverBean.getTag());
            bean.setReleaseDate(serverBean.getReleaseDate());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgVideo> convertServerOgVideoBeanListToAppBeanList(List<OgVideo> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<OgVideo> beanList = new ArrayList<OgVideo>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(OgVideo sb : serverBeanList) {
                OgVideoBean bean = convertServerOgVideoBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.OgVideoBean convertAppOgVideoBeanToServerBean(OgVideo appBean)
    {
        com.pagesynopsis.ws.bean.OgVideoBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgVideoBean();
            bean.setGuid(appBean.getGuid());
            bean.setUrl(appBean.getUrl());
            bean.setType(appBean.getType());
            bean.setSiteName(appBean.getSiteName());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setFbAdmins(appBean.getFbAdmins());
            bean.setFbAppId(appBean.getFbAppId());
            bean.setImage(appBean.getImage());
            bean.setAudio(appBean.getAudio());
            bean.setVideo(appBean.getVideo());
            bean.setLocale(appBean.getLocale());
            bean.setLocaleAlternate(appBean.getLocaleAlternate());
            bean.setDirector(appBean.getDirector());
            bean.setWriter(appBean.getWriter());
            bean.setActor(appBean.getActor());
            bean.setDuration(appBean.getDuration());
            bean.setTag(appBean.getTag());
            bean.setReleaseDate(appBean.getReleaseDate());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgVideo> convertAppOgVideoBeanListToServerBeanList(List<OgVideo> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<OgVideo> beanList = new ArrayList<OgVideo>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(OgVideo sb : appBeanList) {
                com.pagesynopsis.ws.bean.OgVideoBean bean = convertAppOgVideoBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
