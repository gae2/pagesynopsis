package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgVideoService;


// The primary purpose of OgVideoDummyService is to fake the service api, OgVideoService.
// It has no real implementation.
public class OgVideoDummyService implements OgVideoService
{
    private static final Logger log = Logger.getLogger(OgVideoDummyService.class.getName());

    public OgVideoDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // OgVideo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgVideo getOgVideo(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgVideo(): guid = " + guid);
        return null;
    }

    @Override
    public Object getOgVideo(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgVideo(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<OgVideo> getOgVideos(List<String> guids) throws BaseException
    {
        log.fine("getOgVideos()");
        return null;
    }

    @Override
    public List<OgVideo> getAllOgVideos() throws BaseException
    {
        return getAllOgVideos(null, null, null);
    }


    @Override
    public List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgVideos(ordering, offset, count, null);
    }

    @Override
    public List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgVideos(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgVideoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgVideoKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgVideos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgVideoDummyService.findOgVideos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgVideoDummyService.findOgVideoKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgVideoDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createOgVideo(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        log.finer("createOgVideo()");
        return null;
    }

    @Override
    public String createOgVideo(OgVideo ogVideo) throws BaseException
    {
        log.finer("createOgVideo()");
        return null;
    }

    @Override
    public OgVideo constructOgVideo(OgVideo ogVideo) throws BaseException
    {
        log.finer("constructOgVideo()");
        return null;
    }

    @Override
    public Boolean updateOgVideo(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        log.finer("updateOgVideo()");
        return null;
    }
        
    @Override
    public Boolean updateOgVideo(OgVideo ogVideo) throws BaseException
    {
        log.finer("updateOgVideo()");
        return null;
    }

    @Override
    public OgVideo refreshOgVideo(OgVideo ogVideo) throws BaseException
    {
        log.finer("refreshOgVideo()");
        return null;
    }

    @Override
    public Boolean deleteOgVideo(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgVideo(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteOgVideo(OgVideo ogVideo) throws BaseException
    {
        log.finer("deleteOgVideo()");
        return null;
    }

    // TBD
    @Override
    public Long deleteOgVideos(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgVideo(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgVideos(List<OgVideo> ogVideos) throws BaseException
    {
        log.finer("createOgVideos()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateOgVideos(List<OgVideo> ogVideos) throws BaseException
    //{
    //}

}
