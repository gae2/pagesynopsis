package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
// import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.proxy.OgBookServiceProxy;


public class LocalOgBookServiceProxy extends BaseLocalServiceProxy implements OgBookServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgBookServiceProxy.class.getName());

    public LocalOgBookServiceProxy()
    {
    }

    @Override
    public OgBook getOgBook(String guid) throws BaseException
    {
        OgBook serverBean = getOgBookService().getOgBook(guid);
        OgBook appBean = convertServerOgBookBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getOgBook(String guid, String field) throws BaseException
    {
        return getOgBookService().getOgBook(guid, field);       
    }

    @Override
    public List<OgBook> getOgBooks(List<String> guids) throws BaseException
    {
        List<OgBook> serverBeanList = getOgBookService().getOgBooks(guids);
        List<OgBook> appBeanList = convertServerOgBookBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<OgBook> getAllOgBooks() throws BaseException
    {
        return getAllOgBooks(null, null, null);
    }

    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgBookService().getAllOgBooks(ordering, offset, count);
        return getAllOgBooks(ordering, offset, count, null);
    }

    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgBook> serverBeanList = getOgBookService().getAllOgBooks(ordering, offset, count, forwardCursor);
        List<OgBook> appBeanList = convertServerOgBookBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgBookService().getAllOgBookKeys(ordering, offset, count);
        return getAllOgBookKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgBookService().getAllOgBookKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgBooks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgBookService().findOgBooks(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgBook> serverBeanList = getOgBookService().findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<OgBook> appBeanList = convertServerOgBookBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgBookService().findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgBookService().findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgBookService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgBook(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgBookService().createOgBook(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
    }

    @Override
    public String createOgBook(OgBook ogBook) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgBookBean serverBean =  convertAppOgBookBeanToServerBean(ogBook);
        return getOgBookService().createOgBook(serverBean);
    }

    @Override
    public Boolean updateOgBook(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgBookService().updateOgBook(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
    }

    @Override
    public Boolean updateOgBook(OgBook ogBook) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgBookBean serverBean =  convertAppOgBookBeanToServerBean(ogBook);
        return getOgBookService().updateOgBook(serverBean);
    }

    @Override
    public Boolean deleteOgBook(String guid) throws BaseException
    {
        return getOgBookService().deleteOgBook(guid);
    }

    @Override
    public Boolean deleteOgBook(OgBook ogBook) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgBookBean serverBean =  convertAppOgBookBeanToServerBean(ogBook);
        return getOgBookService().deleteOgBook(serverBean);
    }

    @Override
    public Long deleteOgBooks(String filter, String params, List<String> values) throws BaseException
    {
        return getOgBookService().deleteOgBooks(filter, params, values);
    }




    public static OgBookBean convertServerOgBookBeanToAppBean(OgBook serverBean)
    {
        OgBookBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new OgBookBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUrl(serverBean.getUrl());
            bean.setType(serverBean.getType());
            bean.setSiteName(serverBean.getSiteName());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setFbAdmins(serverBean.getFbAdmins());
            bean.setFbAppId(serverBean.getFbAppId());
            bean.setImage(serverBean.getImage());
            bean.setAudio(serverBean.getAudio());
            bean.setVideo(serverBean.getVideo());
            bean.setLocale(serverBean.getLocale());
            bean.setLocaleAlternate(serverBean.getLocaleAlternate());
            bean.setAuthor(serverBean.getAuthor());
            bean.setIsbn(serverBean.getIsbn());
            bean.setTag(serverBean.getTag());
            bean.setReleaseDate(serverBean.getReleaseDate());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgBook> convertServerOgBookBeanListToAppBeanList(List<OgBook> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<OgBook> beanList = new ArrayList<OgBook>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(OgBook sb : serverBeanList) {
                OgBookBean bean = convertServerOgBookBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.OgBookBean convertAppOgBookBeanToServerBean(OgBook appBean)
    {
        com.pagesynopsis.ws.bean.OgBookBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgBookBean();
            bean.setGuid(appBean.getGuid());
            bean.setUrl(appBean.getUrl());
            bean.setType(appBean.getType());
            bean.setSiteName(appBean.getSiteName());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setFbAdmins(appBean.getFbAdmins());
            bean.setFbAppId(appBean.getFbAppId());
            bean.setImage(appBean.getImage());
            bean.setAudio(appBean.getAudio());
            bean.setVideo(appBean.getVideo());
            bean.setLocale(appBean.getLocale());
            bean.setLocaleAlternate(appBean.getLocaleAlternate());
            bean.setAuthor(appBean.getAuthor());
            bean.setIsbn(appBean.getIsbn());
            bean.setTag(appBean.getTag());
            bean.setReleaseDate(appBean.getReleaseDate());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgBook> convertAppOgBookBeanListToServerBeanList(List<OgBook> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<OgBook> beanList = new ArrayList<OgBook>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(OgBook sb : appBeanList) {
                com.pagesynopsis.ws.bean.OgBookBean bean = convertAppOgBookBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
