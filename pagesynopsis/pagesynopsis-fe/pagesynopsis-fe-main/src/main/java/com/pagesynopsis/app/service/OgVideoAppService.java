package com.pagesynopsis.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgVideoService;
import com.pagesynopsis.af.service.impl.OgVideoServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class OgVideoAppService extends OgVideoServiceImpl implements OgVideoService
{
    private static final Logger log = Logger.getLogger(OgVideoAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public OgVideoAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgVideo related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public OgVideo getOgVideo(String guid) throws BaseException
    {
        return super.getOgVideo(guid);
    }

    @Override
    public Object getOgVideo(String guid, String field) throws BaseException
    {
        return super.getOgVideo(guid, field);
    }

    @Override
    public List<OgVideo> getOgVideos(List<String> guids) throws BaseException
    {
        return super.getOgVideos(guids);
    }

    @Override
    public List<OgVideo> getAllOgVideos() throws BaseException
    {
        return super.getAllOgVideos();
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllOgVideoKeys(ordering, offset, count);
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findOgVideos(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgVideo(OgVideo ogVideo) throws BaseException
    {
        return super.createOgVideo(ogVideo);
    }

    @Override
    public OgVideo constructOgVideo(OgVideo ogVideo) throws BaseException
    {
        return super.constructOgVideo(ogVideo);
    }


    @Override
    public Boolean updateOgVideo(OgVideo ogVideo) throws BaseException
    {
        return super.updateOgVideo(ogVideo);
    }
        
    @Override
    public OgVideo refreshOgVideo(OgVideo ogVideo) throws BaseException
    {
        return super.refreshOgVideo(ogVideo);
    }

    @Override
    public Boolean deleteOgVideo(String guid) throws BaseException
    {
        return super.deleteOgVideo(guid);
    }

    @Override
    public Boolean deleteOgVideo(OgVideo ogVideo) throws BaseException
    {
        return super.deleteOgVideo(ogVideo);
    }

    @Override
    public Integer createOgVideos(List<OgVideo> ogVideos) throws BaseException
    {
        return super.createOgVideos(ogVideos);
    }

    // TBD
    //@Override
    //public Boolean updateOgVideos(List<OgVideo> ogVideos) throws BaseException
    //{
    //}

}
