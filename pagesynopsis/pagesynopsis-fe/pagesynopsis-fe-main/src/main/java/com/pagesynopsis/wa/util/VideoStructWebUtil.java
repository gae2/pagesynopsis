package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.fe.bean.MediaSourceStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;


public class VideoStructWebUtil
{
    private static final Logger log = Logger.getLogger(VideoStructWebUtil.class.getName());

    // Static methods only.
    private VideoStructWebUtil() {}
    

    public static VideoStructJsBean convertVideoStructToJsBean(VideoStruct videoStruct)
    {
        VideoStructJsBean jsBean = null;
        if(videoStruct != null) {
            jsBean = new VideoStructJsBean();
            jsBean.setUuid(videoStruct.getUuid());
            jsBean.setId(videoStruct.getId());
            jsBean.setWidth(videoStruct.getWidth());
            jsBean.setHeight(videoStruct.getHeight());
            jsBean.setControls(videoStruct.getControls());
            jsBean.setAutoplayEnabled(videoStruct.isAutoplayEnabled());
            jsBean.setLoopEnabled(videoStruct.isLoopEnabled());
            jsBean.setPreloadEnabled(videoStruct.isPreloadEnabled());
            jsBean.setMuted(videoStruct.isMuted());
            jsBean.setRemark(videoStruct.getRemark());
            jsBean.setSource(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(videoStruct.getSource()));
            jsBean.setSource1(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(videoStruct.getSource1()));
            jsBean.setSource2(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(videoStruct.getSource2()));
            jsBean.setSource3(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(videoStruct.getSource3()));
            jsBean.setSource4(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(videoStruct.getSource4()));
            jsBean.setSource5(MediaSourceStructWebUtil.convertMediaSourceStructToJsBean(videoStruct.getSource5()));
            jsBean.setNote(videoStruct.getNote());
        }
        return jsBean;
    }

    public static VideoStruct convertVideoStructJsBeanToBean(VideoStructJsBean jsBean)
    {
        VideoStructBean videoStruct = null;
        if(jsBean != null) {
            videoStruct = new VideoStructBean();
            videoStruct.setUuid(jsBean.getUuid());
            videoStruct.setId(jsBean.getId());
            videoStruct.setWidth(jsBean.getWidth());
            videoStruct.setHeight(jsBean.getHeight());
            videoStruct.setControls(jsBean.getControls());
            videoStruct.setAutoplayEnabled(jsBean.isAutoplayEnabled());
            videoStruct.setLoopEnabled(jsBean.isLoopEnabled());
            videoStruct.setPreloadEnabled(jsBean.isPreloadEnabled());
            videoStruct.setMuted(jsBean.isMuted());
            videoStruct.setRemark(jsBean.getRemark());
            videoStruct.setSource(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource()));

            videoStruct.setSource1(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource1()));

            videoStruct.setSource2(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource2()));

            videoStruct.setSource3(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource3()));

            videoStruct.setSource4(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource4()));

            videoStruct.setSource5(MediaSourceStructWebUtil.convertMediaSourceStructJsBeanToBean(jsBean.getSource5()));

            videoStruct.setNote(jsBean.getNote());
        }
        return videoStruct;
    }

}
