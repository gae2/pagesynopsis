package com.pagesynopsis.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OpenGraphMetaJsBean;
import com.pagesynopsis.wa.service.OpenGraphMetaWebService;
import com.pagesynopsis.wa.service.UserWebService;


public class OpenGraphMetaHelper
{
    private static final Logger log = Logger.getLogger(OpenGraphMetaHelper.class.getName());

    // Arbitrary...
    private static final long MAX_AGE_MILLIS = 3600 * 1000L * 24 * 30;   // ~1 month

    private OpenGraphMetaHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private OpenGraphMetaWebService openGraphMetaWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private OpenGraphMetaWebService getOpenGraphMetaService()
    {
        if(openGraphMetaWebService == null) {
            openGraphMetaWebService = new OpenGraphMetaWebService();
        }
        return openGraphMetaWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class OpenGraphMetaHelperHolder
    {
        private static final OpenGraphMetaHelper INSTANCE = new OpenGraphMetaHelper();
    }

    // Singleton method
    public static OpenGraphMetaHelper getInstance()
    {
        return OpenGraphMetaHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public OpenGraphMetaJsBean getOpenGraphMeta(String guid) 
    {
        OpenGraphMetaJsBean bean = null;
        try {
            bean = getOpenGraphMetaService().getOpenGraphMeta(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }

    public OpenGraphMetaJsBean getOpenGraphMetaByTargetUrl(String targetUrl) 
    {
        // Long cutoff = System.currentTimeMillis() - MAX_AGE_MILLIS;
        Long cutoff = null;
        return getOpenGraphMetaByTargetUrl(targetUrl, cutoff);
    }
    public OpenGraphMetaJsBean getOpenGraphMetaByTargetUrl(String targetUrl, Long timeCutoff) 
    {
        return getOpenGraphMetaByTargetUrl(targetUrl, timeCutoff, false);  // This should really be true.. ????? 
    }
    private OpenGraphMetaJsBean getOpenGraphMetaByTargetUrl(String targetUrl, Long timeCutoff, boolean refreshingRecordOnly) 
    {
        OpenGraphMetaJsBean bean = null;
        try {
            String filter = "targetUrl=='" + targetUrl + "'";
            if(timeCutoff != null && timeCutoff > 0L) {
                filter += " && createdTime >= " + timeCutoff;
            }
            // TBD: This requires new datastore index....
            if(refreshingRecordOnly == true) {
                // We should not return stale record...
                filter += " && refreshStatus != " + RefreshStatus.STATUS_NOREFRESH;
            }
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<OpenGraphMetaJsBean> beans = getOpenGraphMetaService().findOpenGraphMetas(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
                String guid = bean.getGuid();
                try {
                    bean = getOpenGraphMetaService().getOpenGraphMeta(guid);
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
                }
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for targetUrl = " + targetUrl, e);
        }
        return bean;
    }

    

    public List<String> getOpenGraphMetaKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getOpenGraphMetaService().findOpenGraphMetaKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    // TBD: 
    public List<OpenGraphMetaJsBean> getOpenGraphMetasForRefreshProcessing(Integer maxCount)
    {
        List<OpenGraphMetaJsBean> openGraphMetas = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            openGraphMetas = getOpenGraphMetaService().findOpenGraphMetas(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return openGraphMetas;
    }


}
