package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
// import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.proxy.OgTvEpisodeServiceProxy;


public class LocalOgTvEpisodeServiceProxy extends BaseLocalServiceProxy implements OgTvEpisodeServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgTvEpisodeServiceProxy.class.getName());

    public LocalOgTvEpisodeServiceProxy()
    {
    }

    @Override
    public OgTvEpisode getOgTvEpisode(String guid) throws BaseException
    {
        OgTvEpisode serverBean = getOgTvEpisodeService().getOgTvEpisode(guid);
        OgTvEpisode appBean = convertServerOgTvEpisodeBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getOgTvEpisode(String guid, String field) throws BaseException
    {
        return getOgTvEpisodeService().getOgTvEpisode(guid, field);       
    }

    @Override
    public List<OgTvEpisode> getOgTvEpisodes(List<String> guids) throws BaseException
    {
        List<OgTvEpisode> serverBeanList = getOgTvEpisodeService().getOgTvEpisodes(guids);
        List<OgTvEpisode> appBeanList = convertServerOgTvEpisodeBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes() throws BaseException
    {
        return getAllOgTvEpisodes(null, null, null);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgTvEpisodeService().getAllOgTvEpisodes(ordering, offset, count);
        return getAllOgTvEpisodes(ordering, offset, count, null);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgTvEpisode> serverBeanList = getOgTvEpisodeService().getAllOgTvEpisodes(ordering, offset, count, forwardCursor);
        List<OgTvEpisode> appBeanList = convertServerOgTvEpisodeBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgTvEpisodeService().getAllOgTvEpisodeKeys(ordering, offset, count);
        return getAllOgTvEpisodeKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgTvEpisodeService().getAllOgTvEpisodeKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgTvEpisodes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgTvEpisodeService().findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgTvEpisode> serverBeanList = getOgTvEpisodeService().findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<OgTvEpisode> appBeanList = convertServerOgTvEpisodeBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgTvEpisodeService().findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgTvEpisodeService().findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgTvEpisodeService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgTvEpisode(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgTvEpisodeService().createOgTvEpisode(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
    }

    @Override
    public String createOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgTvEpisodeBean serverBean =  convertAppOgTvEpisodeBeanToServerBean(ogTvEpisode);
        return getOgTvEpisodeService().createOgTvEpisode(serverBean);
    }

    @Override
    public Boolean updateOgTvEpisode(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgTvEpisodeService().updateOgTvEpisode(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
    }

    @Override
    public Boolean updateOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgTvEpisodeBean serverBean =  convertAppOgTvEpisodeBeanToServerBean(ogTvEpisode);
        return getOgTvEpisodeService().updateOgTvEpisode(serverBean);
    }

    @Override
    public Boolean deleteOgTvEpisode(String guid) throws BaseException
    {
        return getOgTvEpisodeService().deleteOgTvEpisode(guid);
    }

    @Override
    public Boolean deleteOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgTvEpisodeBean serverBean =  convertAppOgTvEpisodeBeanToServerBean(ogTvEpisode);
        return getOgTvEpisodeService().deleteOgTvEpisode(serverBean);
    }

    @Override
    public Long deleteOgTvEpisodes(String filter, String params, List<String> values) throws BaseException
    {
        return getOgTvEpisodeService().deleteOgTvEpisodes(filter, params, values);
    }




    public static OgTvEpisodeBean convertServerOgTvEpisodeBeanToAppBean(OgTvEpisode serverBean)
    {
        OgTvEpisodeBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new OgTvEpisodeBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUrl(serverBean.getUrl());
            bean.setType(serverBean.getType());
            bean.setSiteName(serverBean.getSiteName());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setFbAdmins(serverBean.getFbAdmins());
            bean.setFbAppId(serverBean.getFbAppId());
            bean.setImage(serverBean.getImage());
            bean.setAudio(serverBean.getAudio());
            bean.setVideo(serverBean.getVideo());
            bean.setLocale(serverBean.getLocale());
            bean.setLocaleAlternate(serverBean.getLocaleAlternate());
            bean.setDirector(serverBean.getDirector());
            bean.setWriter(serverBean.getWriter());
            bean.setActor(serverBean.getActor());
            bean.setSeries(serverBean.getSeries());
            bean.setDuration(serverBean.getDuration());
            bean.setTag(serverBean.getTag());
            bean.setReleaseDate(serverBean.getReleaseDate());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgTvEpisode> convertServerOgTvEpisodeBeanListToAppBeanList(List<OgTvEpisode> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<OgTvEpisode> beanList = new ArrayList<OgTvEpisode>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(OgTvEpisode sb : serverBeanList) {
                OgTvEpisodeBean bean = convertServerOgTvEpisodeBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.OgTvEpisodeBean convertAppOgTvEpisodeBeanToServerBean(OgTvEpisode appBean)
    {
        com.pagesynopsis.ws.bean.OgTvEpisodeBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgTvEpisodeBean();
            bean.setGuid(appBean.getGuid());
            bean.setUrl(appBean.getUrl());
            bean.setType(appBean.getType());
            bean.setSiteName(appBean.getSiteName());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setFbAdmins(appBean.getFbAdmins());
            bean.setFbAppId(appBean.getFbAppId());
            bean.setImage(appBean.getImage());
            bean.setAudio(appBean.getAudio());
            bean.setVideo(appBean.getVideo());
            bean.setLocale(appBean.getLocale());
            bean.setLocaleAlternate(appBean.getLocaleAlternate());
            bean.setDirector(appBean.getDirector());
            bean.setWriter(appBean.getWriter());
            bean.setActor(appBean.getActor());
            bean.setSeries(appBean.getSeries());
            bean.setDuration(appBean.getDuration());
            bean.setTag(appBean.getTag());
            bean.setReleaseDate(appBean.getReleaseDate());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgTvEpisode> convertAppOgTvEpisodeBeanListToServerBeanList(List<OgTvEpisode> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<OgTvEpisode> beanList = new ArrayList<OgTvEpisode>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(OgTvEpisode sb : appBeanList) {
                com.pagesynopsis.ws.bean.OgTvEpisodeBean bean = convertAppOgTvEpisodeBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
