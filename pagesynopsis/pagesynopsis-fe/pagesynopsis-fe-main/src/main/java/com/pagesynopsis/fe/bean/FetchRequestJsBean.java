package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class FetchRequestJsBean implements Serializable, Cloneable  //, FetchRequest
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FetchRequestJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String user;
    private String targetUrl;
    private String pageUrl;
    private String queryString;
    private List<KeyValuePairStructJsBean> queryParams;
    private String version;
    private String fetchObject;
    private Integer fetchStatus;
    private String result;
    private String note;
    private ReferrerInfoStructJsBean referrerInfo;
    private String status;
    private String nextPageUrl;
    private Integer followPages;
    private Integer followDepth;
    private Boolean createVersion;
    private Boolean deferred;
    private Boolean alert;
    private NotificationStructJsBean notificationPref;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FetchRequestJsBean()
    {
        //this((String) null);
    }
    public FetchRequestJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FetchRequestJsBean(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStructJsBean> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStructJsBean referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStructJsBean notificationPref)
    {
        this(guid, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfo, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPref, null, null);
    }
    public FetchRequestJsBean(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStructJsBean> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStructJsBean referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStructJsBean notificationPref, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.targetUrl = targetUrl;
        this.pageUrl = pageUrl;
        this.queryString = queryString;
        setQueryParams(getQueryParams());
        this.version = version;
        this.fetchObject = fetchObject;
        this.fetchStatus = fetchStatus;
        this.result = result;
        this.note = note;
        this.referrerInfo = referrerInfo;
        this.status = status;
        this.nextPageUrl = nextPageUrl;
        this.followPages = followPages;
        this.followDepth = followDepth;
        this.createVersion = createVersion;
        this.deferred = deferred;
        this.alert = alert;
        this.notificationPref = notificationPref;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FetchRequestJsBean(FetchRequestJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setTargetUrl(bean.getTargetUrl());
            setPageUrl(bean.getPageUrl());
            setQueryString(bean.getQueryString());
            setQueryParams(bean.getQueryParams());
            setVersion(bean.getVersion());
            setFetchObject(bean.getFetchObject());
            setFetchStatus(bean.getFetchStatus());
            setResult(bean.getResult());
            setNote(bean.getNote());
            setReferrerInfo(bean.getReferrerInfo());
            setStatus(bean.getStatus());
            setNextPageUrl(bean.getNextPageUrl());
            setFollowPages(bean.getFollowPages());
            setFollowDepth(bean.getFollowDepth());
            setCreateVersion(bean.isCreateVersion());
            setDeferred(bean.isDeferred());
            setAlert(bean.isAlert());
            setNotificationPref(bean.getNotificationPref());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static FetchRequestJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        FetchRequestJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(FetchRequestJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, FetchRequestJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getTargetUrl()
    {
        return this.targetUrl;
    }
    public void setTargetUrl(String targetUrl)
    {
        this.targetUrl = targetUrl;
    }

    public String getPageUrl()
    {
        return this.pageUrl;
    }
    public void setPageUrl(String pageUrl)
    {
        this.pageUrl = pageUrl;
    }

    public String getQueryString()
    {
        return this.queryString;
    }
    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    public List<KeyValuePairStructJsBean> getQueryParams()
    {  
        return this.queryParams;
    }
    public void setQueryParams(List<KeyValuePairStructJsBean> queryParams)
    {
        this.queryParams = queryParams;
    }

    public String getVersion()
    {
        return this.version;
    }
    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getFetchObject()
    {
        return this.fetchObject;
    }
    public void setFetchObject(String fetchObject)
    {
        this.fetchObject = fetchObject;
    }

    public Integer getFetchStatus()
    {
        return this.fetchStatus;
    }
    public void setFetchStatus(Integer fetchStatus)
    {
        this.fetchStatus = fetchStatus;
    }

    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public ReferrerInfoStructJsBean getReferrerInfo()
    {  
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStructJsBean referrerInfo)
    {
        this.referrerInfo = referrerInfo;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNextPageUrl()
    {
        return this.nextPageUrl;
    }
    public void setNextPageUrl(String nextPageUrl)
    {
        this.nextPageUrl = nextPageUrl;
    }

    public Integer getFollowPages()
    {
        return this.followPages;
    }
    public void setFollowPages(Integer followPages)
    {
        this.followPages = followPages;
    }

    public Integer getFollowDepth()
    {
        return this.followDepth;
    }
    public void setFollowDepth(Integer followDepth)
    {
        this.followDepth = followDepth;
    }

    public Boolean isCreateVersion()
    {
        return this.createVersion;
    }
    public void setCreateVersion(Boolean createVersion)
    {
        this.createVersion = createVersion;
    }

    public Boolean isDeferred()
    {
        return this.deferred;
    }
    public void setDeferred(Boolean deferred)
    {
        this.deferred = deferred;
    }

    public Boolean isAlert()
    {
        return this.alert;
    }
    public void setAlert(Boolean alert)
    {
        this.alert = alert;
    }

    public NotificationStructJsBean getNotificationPref()
    {  
        return this.notificationPref;
    }
    public void setNotificationPref(NotificationStructJsBean notificationPref)
    {
        this.notificationPref = notificationPref;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("targetUrl:null, ");
        sb.append("pageUrl:null, ");
        sb.append("queryString:null, ");
        sb.append("queryParams:null, ");
        sb.append("version:null, ");
        sb.append("fetchObject:null, ");
        sb.append("fetchStatus:0, ");
        sb.append("result:null, ");
        sb.append("note:null, ");
        sb.append("referrerInfo:{}, ");
        sb.append("status:null, ");
        sb.append("nextPageUrl:null, ");
        sb.append("followPages:0, ");
        sb.append("followDepth:0, ");
        sb.append("createVersion:false, ");
        sb.append("deferred:false, ");
        sb.append("alert:false, ");
        sb.append("notificationPref:{}, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("targetUrl:");
        if(this.getTargetUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTargetUrl()).append("\", ");
        }
        sb.append("pageUrl:");
        if(this.getPageUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPageUrl()).append("\", ");
        }
        sb.append("queryString:");
        if(this.getQueryString() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQueryString()).append("\", ");
        }
        sb.append("queryParams:");
        if(this.getQueryParams() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQueryParams()).append("\", ");
        }
        sb.append("version:");
        if(this.getVersion() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getVersion()).append("\", ");
        }
        sb.append("fetchObject:");
        if(this.getFetchObject() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFetchObject()).append("\", ");
        }
        sb.append("fetchStatus:" + this.getFetchStatus()).append(", ");
        sb.append("result:");
        if(this.getResult() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getResult()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("referrerInfo:");
        if(this.getReferrerInfo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReferrerInfo()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("nextPageUrl:");
        if(this.getNextPageUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNextPageUrl()).append("\", ");
        }
        sb.append("followPages:" + this.getFollowPages()).append(", ");
        sb.append("followDepth:" + this.getFollowDepth()).append(", ");
        sb.append("createVersion:" + this.isCreateVersion()).append(", ");
        sb.append("deferred:" + this.isDeferred()).append(", ");
        sb.append("alert:" + this.isAlert()).append(", ");
        sb.append("notificationPref:");
        if(this.getNotificationPref() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNotificationPref()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getTargetUrl() != null) {
            sb.append("\"targetUrl\":").append("\"").append(this.getTargetUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"targetUrl\":").append("null, ");
        }
        if(this.getPageUrl() != null) {
            sb.append("\"pageUrl\":").append("\"").append(this.getPageUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pageUrl\":").append("null, ");
        }
        if(this.getQueryString() != null) {
            sb.append("\"queryString\":").append("\"").append(this.getQueryString()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"queryString\":").append("null, ");
        }
        if(this.getQueryParams() != null) {
            sb.append("\"queryParams\":").append("\"").append(this.getQueryParams()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"queryParams\":").append("null, ");
        }
        if(this.getVersion() != null) {
            sb.append("\"version\":").append("\"").append(this.getVersion()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"version\":").append("null, ");
        }
        if(this.getFetchObject() != null) {
            sb.append("\"fetchObject\":").append("\"").append(this.getFetchObject()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fetchObject\":").append("null, ");
        }
        if(this.getFetchStatus() != null) {
            sb.append("\"fetchStatus\":").append("").append(this.getFetchStatus()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fetchStatus\":").append("null, ");
        }
        if(this.getResult() != null) {
            sb.append("\"result\":").append("\"").append(this.getResult()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"result\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        sb.append("\"referrerInfo\":").append(this.referrerInfo.toJsonString()).append(", ");
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getNextPageUrl() != null) {
            sb.append("\"nextPageUrl\":").append("\"").append(this.getNextPageUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"nextPageUrl\":").append("null, ");
        }
        if(this.getFollowPages() != null) {
            sb.append("\"followPages\":").append("").append(this.getFollowPages()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"followPages\":").append("null, ");
        }
        if(this.getFollowDepth() != null) {
            sb.append("\"followDepth\":").append("").append(this.getFollowDepth()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"followDepth\":").append("null, ");
        }
        if(this.isCreateVersion() != null) {
            sb.append("\"createVersion\":").append("").append(this.isCreateVersion()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createVersion\":").append("null, ");
        }
        if(this.isDeferred() != null) {
            sb.append("\"deferred\":").append("").append(this.isDeferred()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"deferred\":").append("null, ");
        }
        if(this.isAlert() != null) {
            sb.append("\"alert\":").append("").append(this.isAlert()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"alert\":").append("null, ");
        }
        sb.append("\"notificationPref\":").append(this.notificationPref.toJsonString()).append(", ");
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("targetUrl = " + this.targetUrl).append(";");
        sb.append("pageUrl = " + this.pageUrl).append(";");
        sb.append("queryString = " + this.queryString).append(";");
        sb.append("queryParams = " + this.queryParams).append(";");
        sb.append("version = " + this.version).append(";");
        sb.append("fetchObject = " + this.fetchObject).append(";");
        sb.append("fetchStatus = " + this.fetchStatus).append(";");
        sb.append("result = " + this.result).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("referrerInfo = " + this.referrerInfo).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("nextPageUrl = " + this.nextPageUrl).append(";");
        sb.append("followPages = " + this.followPages).append(";");
        sb.append("followDepth = " + this.followDepth).append(";");
        sb.append("createVersion = " + this.createVersion).append(";");
        sb.append("deferred = " + this.deferred).append(";");
        sb.append("alert = " + this.alert).append(";");
        sb.append("notificationPref = " + this.notificationPref).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        FetchRequestJsBean cloned = new FetchRequestJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setTargetUrl(this.getTargetUrl());   
        cloned.setPageUrl(this.getPageUrl());   
        cloned.setQueryString(this.getQueryString());   
        cloned.setQueryParams(this.getQueryParams());   
        cloned.setVersion(this.getVersion());   
        cloned.setFetchObject(this.getFetchObject());   
        cloned.setFetchStatus(this.getFetchStatus());   
        cloned.setResult(this.getResult());   
        cloned.setNote(this.getNote());   
        cloned.setReferrerInfo( (ReferrerInfoStructJsBean) this.getReferrerInfo().clone() );
        cloned.setStatus(this.getStatus());   
        cloned.setNextPageUrl(this.getNextPageUrl());   
        cloned.setFollowPages(this.getFollowPages());   
        cloned.setFollowDepth(this.getFollowDepth());   
        cloned.setCreateVersion(this.isCreateVersion());   
        cloned.setDeferred(this.isDeferred());   
        cloned.setAlert(this.isAlert());   
        cloned.setNotificationPref( (NotificationStructJsBean) this.getNotificationPref().clone() );
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
