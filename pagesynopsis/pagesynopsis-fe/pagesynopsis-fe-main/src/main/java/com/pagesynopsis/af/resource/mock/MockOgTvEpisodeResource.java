package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.resource.OgTvEpisodeResource;
import com.pagesynopsis.af.resource.util.OgAudioStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgImageStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgActorStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgVideoStructResourceUtil;


// MockOgTvEpisodeResource is a decorator.
// It can be used as a base class to mock OgTvEpisodeResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/ogTvEpisodes/")
public abstract class MockOgTvEpisodeResource implements OgTvEpisodeResource
{
    private static final Logger log = Logger.getLogger(MockOgTvEpisodeResource.class.getName());

    // MockOgTvEpisodeResource uses the decorator design pattern.
    private OgTvEpisodeResource decoratedResource;

    public MockOgTvEpisodeResource(OgTvEpisodeResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected OgTvEpisodeResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(OgTvEpisodeResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgTvEpisodes(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgTvEpisodeKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgTvEpisodesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findOgTvEpisodesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getOgTvEpisodeAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getOgTvEpisodeAsHtml(guid);
//    }

    @Override
    public Response getOgTvEpisode(String guid) throws BaseResourceException
    {
        return decoratedResource.getOgTvEpisode(guid);
    }

    @Override
    public Response getOgTvEpisodeAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getOgTvEpisodeAsJsonp(guid, callback);
    }

    @Override
    public Response getOgTvEpisode(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getOgTvEpisode(guid, field);
    }

    // TBD
    @Override
    public Response constructOgTvEpisode(OgTvEpisodeStub ogTvEpisode) throws BaseResourceException
    {
        return decoratedResource.constructOgTvEpisode(ogTvEpisode);
    }

    @Override
    public Response createOgTvEpisode(OgTvEpisodeStub ogTvEpisode) throws BaseResourceException
    {
        return decoratedResource.createOgTvEpisode(ogTvEpisode);
    }

//    @Override
//    public Response createOgTvEpisode(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createOgTvEpisode(formParams);
//    }

    // TBD
    @Override
    public Response refreshOgTvEpisode(String guid, OgTvEpisodeStub ogTvEpisode) throws BaseResourceException
    {
        return decoratedResource.refreshOgTvEpisode(guid, ogTvEpisode);
    }

    @Override
    public Response updateOgTvEpisode(String guid, OgTvEpisodeStub ogTvEpisode) throws BaseResourceException
    {
        return decoratedResource.updateOgTvEpisode(guid, ogTvEpisode);
    }

    @Override
    public Response updateOgTvEpisode(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<String> image, List<String> audio, List<String> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<String> actor, String series, Integer duration, List<String> tag, String releaseDate)
    {
        return decoratedResource.updateOgTvEpisode(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
    }

//    @Override
//    public Response updateOgTvEpisode(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateOgTvEpisode(guid, formParams);
//    }

    @Override
    public Response deleteOgTvEpisode(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteOgTvEpisode(guid);
    }

    @Override
    public Response deleteOgTvEpisodes(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteOgTvEpisodes(filter, params, values);
    }


// TBD ....
    @Override
    public Response createOgTvEpisodes(OgTvEpisodeListStub ogTvEpisodes) throws BaseResourceException
    {
        return decoratedResource.createOgTvEpisodes(ogTvEpisodes);
    }


}
