package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterAppCardService;


// The primary purpose of TwitterAppCardDummyService is to fake the service api, TwitterAppCardService.
// It has no real implementation.
public class TwitterAppCardDummyService implements TwitterAppCardService
{
    private static final Logger log = Logger.getLogger(TwitterAppCardDummyService.class.getName());

    public TwitterAppCardDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterAppCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterAppCard getTwitterAppCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterAppCard(): guid = " + guid);
        return null;
    }

    @Override
    public Object getTwitterAppCard(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterAppCard(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<TwitterAppCard> getTwitterAppCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterAppCards()");
        return null;
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards() throws BaseException
    {
        return getAllTwitterAppCards(null, null, null);
    }


    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterAppCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterAppCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterAppCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterAppCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardDummyService.findTwitterAppCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardDummyService.findTwitterAppCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createTwitterAppCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        log.finer("createTwitterAppCard()");
        return null;
    }

    @Override
    public String createTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("createTwitterAppCard()");
        return null;
    }

    @Override
    public TwitterAppCard constructTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("constructTwitterAppCard()");
        return null;
    }

    @Override
    public Boolean updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        log.finer("updateTwitterAppCard()");
        return null;
    }
        
    @Override
    public Boolean updateTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("updateTwitterAppCard()");
        return null;
    }

    @Override
    public TwitterAppCard refreshTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("refreshTwitterAppCard()");
        return null;
    }

    @Override
    public Boolean deleteTwitterAppCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTwitterAppCard(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("deleteTwitterAppCard()");
        return null;
    }

    // TBD
    @Override
    public Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTwitterAppCard(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException
    {
        log.finer("createTwitterAppCards()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException
    //{
    //}

}
