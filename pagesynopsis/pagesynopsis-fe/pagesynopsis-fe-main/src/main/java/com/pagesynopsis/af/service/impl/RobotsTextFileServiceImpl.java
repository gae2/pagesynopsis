package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.RobotsTextFileService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RobotsTextFileServiceImpl implements RobotsTextFileService
{
    private static final Logger log = Logger.getLogger(RobotsTextFileServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "RobotsTextFile-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("RobotsTextFile:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public RobotsTextFileServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // RobotsTextFile related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public RobotsTextFile getRobotsTextFile(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getRobotsTextFile(): guid = " + guid);

        RobotsTextFileBean bean = null;
        if(getCache() != null) {
            bean = (RobotsTextFileBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (RobotsTextFileBean) getProxyFactory().getRobotsTextFileServiceProxy().getRobotsTextFile(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "RobotsTextFileBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RobotsTextFileBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getRobotsTextFile(String guid, String field) throws BaseException
    {
        RobotsTextFileBean bean = null;
        if(getCache() != null) {
            bean = (RobotsTextFileBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (RobotsTextFileBean) getProxyFactory().getRobotsTextFileServiceProxy().getRobotsTextFile(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "RobotsTextFileBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RobotsTextFileBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("siteUrl")) {
            return bean.getSiteUrl();
        } else if(field.equals("groups")) {
            return bean.getGroups();
        } else if(field.equals("sitemaps")) {
            return bean.getSitemaps();
        } else if(field.equals("content")) {
            return bean.getContent();
        } else if(field.equals("contentHash")) {
            return bean.getContentHash();
        } else if(field.equals("lastCheckedTime")) {
            return bean.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return bean.getLastUpdatedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<RobotsTextFile> getRobotsTextFiles(List<String> guids) throws BaseException
    {
        log.fine("getRobotsTextFiles()");

        // TBD: Is there a better way????
        List<RobotsTextFile> robotsTextFiles = getProxyFactory().getRobotsTextFileServiceProxy().getRobotsTextFiles(guids);
        if(robotsTextFiles == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextFileBean list.");
        }

        log.finer("END");
        return robotsTextFiles;
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles() throws BaseException
    {
        return getAllRobotsTextFiles(null, null, null);
    }


    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextFiles(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRobotsTextFiles(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<RobotsTextFile> robotsTextFiles = getProxyFactory().getRobotsTextFileServiceProxy().getAllRobotsTextFiles(ordering, offset, count, forwardCursor);
        if(robotsTextFiles == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextFileBean list.");
        }

        log.finer("END");
        return robotsTextFiles;
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextFileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRobotsTextFileKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getRobotsTextFileServiceProxy().getAllRobotsTextFileKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextFileBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty RobotsTextFileBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findRobotsTextFiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextFileServiceImpl.findRobotsTextFiles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<RobotsTextFile> robotsTextFiles = getProxyFactory().getRobotsTextFileServiceProxy().findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(robotsTextFiles == null) {
            log.log(Level.WARNING, "Failed to find robotsTextFiles for the given criterion.");
        }

        log.finer("END");
        return robotsTextFiles;
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextFileServiceImpl.findRobotsTextFileKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getRobotsTextFileServiceProxy().findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find RobotsTextFile keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty RobotsTextFile key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextFileServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getRobotsTextFileServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createRobotsTextFile(String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        RobotsTextFileBean bean = new RobotsTextFileBean(null, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
        return createRobotsTextFile(bean);
    }

    @Override
    public String createRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //RobotsTextFile bean = constructRobotsTextFile(robotsTextFile);
        //return bean.getGuid();

        // Param robotsTextFile cannot be null.....
        if(robotsTextFile == null) {
            log.log(Level.INFO, "Param robotsTextFile is null!");
            throw new BadRequestException("Param robotsTextFile object is null!");
        }
        RobotsTextFileBean bean = null;
        if(robotsTextFile instanceof RobotsTextFileBean) {
            bean = (RobotsTextFileBean) robotsTextFile;
        } else if(robotsTextFile instanceof RobotsTextFile) {
            // bean = new RobotsTextFileBean(null, robotsTextFile.getSiteUrl(), robotsTextFile.getGroups(), robotsTextFile.getSitemaps(), robotsTextFile.getContent(), robotsTextFile.getContentHash(), robotsTextFile.getLastCheckedTime(), robotsTextFile.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new RobotsTextFileBean(robotsTextFile.getGuid(), robotsTextFile.getSiteUrl(), robotsTextFile.getGroups(), robotsTextFile.getSitemaps(), robotsTextFile.getContent(), robotsTextFile.getContentHash(), robotsTextFile.getLastCheckedTime(), robotsTextFile.getLastUpdatedTime());
        } else {
            log.log(Level.WARNING, "createRobotsTextFile(): Arg robotsTextFile is of an unknown type.");
            //bean = new RobotsTextFileBean();
            bean = new RobotsTextFileBean(robotsTextFile.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getRobotsTextFileServiceProxy().createRobotsTextFile(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public RobotsTextFile constructRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextFile cannot be null.....
        if(robotsTextFile == null) {
            log.log(Level.INFO, "Param robotsTextFile is null!");
            throw new BadRequestException("Param robotsTextFile object is null!");
        }
        RobotsTextFileBean bean = null;
        if(robotsTextFile instanceof RobotsTextFileBean) {
            bean = (RobotsTextFileBean) robotsTextFile;
        } else if(robotsTextFile instanceof RobotsTextFile) {
            // bean = new RobotsTextFileBean(null, robotsTextFile.getSiteUrl(), robotsTextFile.getGroups(), robotsTextFile.getSitemaps(), robotsTextFile.getContent(), robotsTextFile.getContentHash(), robotsTextFile.getLastCheckedTime(), robotsTextFile.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new RobotsTextFileBean(robotsTextFile.getGuid(), robotsTextFile.getSiteUrl(), robotsTextFile.getGroups(), robotsTextFile.getSitemaps(), robotsTextFile.getContent(), robotsTextFile.getContentHash(), robotsTextFile.getLastCheckedTime(), robotsTextFile.getLastUpdatedTime());
        } else {
            log.log(Level.WARNING, "createRobotsTextFile(): Arg robotsTextFile is of an unknown type.");
            //bean = new RobotsTextFileBean();
            bean = new RobotsTextFileBean(robotsTextFile.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getRobotsTextFileServiceProxy().createRobotsTextFile(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateRobotsTextFile(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        RobotsTextFileBean bean = new RobotsTextFileBean(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
        return updateRobotsTextFile(bean);
    }
        
    @Override
    public Boolean updateRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //RobotsTextFile bean = refreshRobotsTextFile(robotsTextFile);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param robotsTextFile cannot be null.....
        if(robotsTextFile == null || robotsTextFile.getGuid() == null) {
            log.log(Level.INFO, "Param robotsTextFile or its guid is null!");
            throw new BadRequestException("Param robotsTextFile object or its guid is null!");
        }
        RobotsTextFileBean bean = null;
        if(robotsTextFile instanceof RobotsTextFileBean) {
            bean = (RobotsTextFileBean) robotsTextFile;
        } else {  // if(robotsTextFile instanceof RobotsTextFile)
            bean = new RobotsTextFileBean(robotsTextFile.getGuid(), robotsTextFile.getSiteUrl(), robotsTextFile.getGroups(), robotsTextFile.getSitemaps(), robotsTextFile.getContent(), robotsTextFile.getContentHash(), robotsTextFile.getLastCheckedTime(), robotsTextFile.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getRobotsTextFileServiceProxy().updateRobotsTextFile(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public RobotsTextFile refreshRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextFile cannot be null.....
        if(robotsTextFile == null || robotsTextFile.getGuid() == null) {
            log.log(Level.INFO, "Param robotsTextFile or its guid is null!");
            throw new BadRequestException("Param robotsTextFile object or its guid is null!");
        }
        RobotsTextFileBean bean = null;
        if(robotsTextFile instanceof RobotsTextFileBean) {
            bean = (RobotsTextFileBean) robotsTextFile;
        } else {  // if(robotsTextFile instanceof RobotsTextFile)
            bean = new RobotsTextFileBean(robotsTextFile.getGuid(), robotsTextFile.getSiteUrl(), robotsTextFile.getGroups(), robotsTextFile.getSitemaps(), robotsTextFile.getContent(), robotsTextFile.getContentHash(), robotsTextFile.getLastCheckedTime(), robotsTextFile.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getRobotsTextFileServiceProxy().updateRobotsTextFile(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteRobotsTextFile(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getRobotsTextFileServiceProxy().deleteRobotsTextFile(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            RobotsTextFile robotsTextFile = null;
            try {
                robotsTextFile = getProxyFactory().getRobotsTextFileServiceProxy().getRobotsTextFile(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch robotsTextFile with a key, " + guid);
                return false;
            }
            if(robotsTextFile != null) {
                String beanGuid = robotsTextFile.getGuid();
                Boolean suc1 = getProxyFactory().getRobotsTextFileServiceProxy().deleteRobotsTextFile(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("robotsTextFile with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextFile cannot be null.....
        if(robotsTextFile == null || robotsTextFile.getGuid() == null) {
            log.log(Level.INFO, "Param robotsTextFile or its guid is null!");
            throw new BadRequestException("Param robotsTextFile object or its guid is null!");
        }
        RobotsTextFileBean bean = null;
        if(robotsTextFile instanceof RobotsTextFileBean) {
            bean = (RobotsTextFileBean) robotsTextFile;
        } else {  // if(robotsTextFile instanceof RobotsTextFile)
            // ????
            log.warning("robotsTextFile is not an instance of RobotsTextFileBean.");
            bean = new RobotsTextFileBean(robotsTextFile.getGuid(), robotsTextFile.getSiteUrl(), robotsTextFile.getGroups(), robotsTextFile.getSitemaps(), robotsTextFile.getContent(), robotsTextFile.getContentHash(), robotsTextFile.getLastCheckedTime(), robotsTextFile.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getRobotsTextFileServiceProxy().deleteRobotsTextFile(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getRobotsTextFileServiceProxy().deleteRobotsTextFiles(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createRobotsTextFiles(List<RobotsTextFile> robotsTextFiles) throws BaseException
    {
        log.finer("BEGIN");

        if(robotsTextFiles == null) {
            log.log(Level.WARNING, "createRobotsTextFiles() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = robotsTextFiles.size();
        if(size == 0) {
            log.log(Level.WARNING, "createRobotsTextFiles() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(RobotsTextFile robotsTextFile : robotsTextFiles) {
            String guid = createRobotsTextFile(robotsTextFile);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createRobotsTextFiles() failed for at least one robotsTextFile. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateRobotsTextFiles(List<RobotsTextFile> robotsTextFiles) throws BaseException
    //{
    //}

}
