package com.pagesynopsis.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgGeoPointStruct;
import com.pagesynopsis.ws.stub.OgGeoPointStructStub;
import com.pagesynopsis.af.bean.OgGeoPointStructBean;


public class OgGeoPointStructResourceUtil
{
    private static final Logger log = Logger.getLogger(OgGeoPointStructResourceUtil.class.getName());

    // Static methods only.
    private OgGeoPointStructResourceUtil() {}

    public static OgGeoPointStructBean convertOgGeoPointStructStubToBean(OgGeoPointStruct stub)
    {
        OgGeoPointStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new OgGeoPointStructBean();
            bean.setUuid(stub.getUuid());
            bean.setLatitude(stub.getLatitude());
            bean.setLongitude(stub.getLongitude());
            bean.setAltitude(stub.getAltitude());
        }
        return bean;
    }

}
