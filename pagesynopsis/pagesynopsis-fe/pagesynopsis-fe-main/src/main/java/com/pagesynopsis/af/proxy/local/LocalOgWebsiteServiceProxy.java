package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgWebsite;
// import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.proxy.OgWebsiteServiceProxy;


public class LocalOgWebsiteServiceProxy extends BaseLocalServiceProxy implements OgWebsiteServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgWebsiteServiceProxy.class.getName());

    public LocalOgWebsiteServiceProxy()
    {
    }

    @Override
    public OgWebsite getOgWebsite(String guid) throws BaseException
    {
        OgWebsite serverBean = getOgWebsiteService().getOgWebsite(guid);
        OgWebsite appBean = convertServerOgWebsiteBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getOgWebsite(String guid, String field) throws BaseException
    {
        return getOgWebsiteService().getOgWebsite(guid, field);       
    }

    @Override
    public List<OgWebsite> getOgWebsites(List<String> guids) throws BaseException
    {
        List<OgWebsite> serverBeanList = getOgWebsiteService().getOgWebsites(guids);
        List<OgWebsite> appBeanList = convertServerOgWebsiteBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<OgWebsite> getAllOgWebsites() throws BaseException
    {
        return getAllOgWebsites(null, null, null);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgWebsiteService().getAllOgWebsites(ordering, offset, count);
        return getAllOgWebsites(ordering, offset, count, null);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgWebsite> serverBeanList = getOgWebsiteService().getAllOgWebsites(ordering, offset, count, forwardCursor);
        List<OgWebsite> appBeanList = convertServerOgWebsiteBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgWebsiteService().getAllOgWebsiteKeys(ordering, offset, count);
        return getAllOgWebsiteKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgWebsiteService().getAllOgWebsiteKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgWebsites(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgWebsiteService().findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgWebsite> serverBeanList = getOgWebsiteService().findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<OgWebsite> appBeanList = convertServerOgWebsiteBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgWebsiteService().findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgWebsiteService().findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgWebsiteService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgWebsite(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return getOgWebsiteService().createOgWebsite(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public String createOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgWebsiteBean serverBean =  convertAppOgWebsiteBeanToServerBean(ogWebsite);
        return getOgWebsiteService().createOgWebsite(serverBean);
    }

    @Override
    public Boolean updateOgWebsite(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return getOgWebsiteService().updateOgWebsite(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public Boolean updateOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgWebsiteBean serverBean =  convertAppOgWebsiteBeanToServerBean(ogWebsite);
        return getOgWebsiteService().updateOgWebsite(serverBean);
    }

    @Override
    public Boolean deleteOgWebsite(String guid) throws BaseException
    {
        return getOgWebsiteService().deleteOgWebsite(guid);
    }

    @Override
    public Boolean deleteOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgWebsiteBean serverBean =  convertAppOgWebsiteBeanToServerBean(ogWebsite);
        return getOgWebsiteService().deleteOgWebsite(serverBean);
    }

    @Override
    public Long deleteOgWebsites(String filter, String params, List<String> values) throws BaseException
    {
        return getOgWebsiteService().deleteOgWebsites(filter, params, values);
    }




    public static OgWebsiteBean convertServerOgWebsiteBeanToAppBean(OgWebsite serverBean)
    {
        OgWebsiteBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new OgWebsiteBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUrl(serverBean.getUrl());
            bean.setType(serverBean.getType());
            bean.setSiteName(serverBean.getSiteName());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setFbAdmins(serverBean.getFbAdmins());
            bean.setFbAppId(serverBean.getFbAppId());
            bean.setImage(serverBean.getImage());
            bean.setAudio(serverBean.getAudio());
            bean.setVideo(serverBean.getVideo());
            bean.setLocale(serverBean.getLocale());
            bean.setLocaleAlternate(serverBean.getLocaleAlternate());
            bean.setNote(serverBean.getNote());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgWebsite> convertServerOgWebsiteBeanListToAppBeanList(List<OgWebsite> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<OgWebsite> beanList = new ArrayList<OgWebsite>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(OgWebsite sb : serverBeanList) {
                OgWebsiteBean bean = convertServerOgWebsiteBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.OgWebsiteBean convertAppOgWebsiteBeanToServerBean(OgWebsite appBean)
    {
        com.pagesynopsis.ws.bean.OgWebsiteBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgWebsiteBean();
            bean.setGuid(appBean.getGuid());
            bean.setUrl(appBean.getUrl());
            bean.setType(appBean.getType());
            bean.setSiteName(appBean.getSiteName());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setFbAdmins(appBean.getFbAdmins());
            bean.setFbAppId(appBean.getFbAppId());
            bean.setImage(appBean.getImage());
            bean.setAudio(appBean.getAudio());
            bean.setVideo(appBean.getVideo());
            bean.setLocale(appBean.getLocale());
            bean.setLocaleAlternate(appBean.getLocaleAlternate());
            bean.setNote(appBean.getNote());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgWebsite> convertAppOgWebsiteBeanListToServerBeanList(List<OgWebsite> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<OgWebsite> beanList = new ArrayList<OgWebsite>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(OgWebsite sb : appBeanList) {
                com.pagesynopsis.ws.bean.OgWebsiteBean bean = convertAppOgWebsiteBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
