package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.UrlRating;
// import com.pagesynopsis.ws.bean.UrlRatingBean;
import com.pagesynopsis.ws.service.UrlRatingService;
import com.pagesynopsis.af.bean.UrlRatingBean;
import com.pagesynopsis.af.proxy.UrlRatingServiceProxy;


public class LocalUrlRatingServiceProxy extends BaseLocalServiceProxy implements UrlRatingServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUrlRatingServiceProxy.class.getName());

    public LocalUrlRatingServiceProxy()
    {
    }

    @Override
    public UrlRating getUrlRating(String guid) throws BaseException
    {
        UrlRating serverBean = getUrlRatingService().getUrlRating(guid);
        UrlRating appBean = convertServerUrlRatingBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getUrlRating(String guid, String field) throws BaseException
    {
        return getUrlRatingService().getUrlRating(guid, field);       
    }

    @Override
    public List<UrlRating> getUrlRatings(List<String> guids) throws BaseException
    {
        List<UrlRating> serverBeanList = getUrlRatingService().getUrlRatings(guids);
        List<UrlRating> appBeanList = convertServerUrlRatingBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<UrlRating> getAllUrlRatings() throws BaseException
    {
        return getAllUrlRatings(null, null, null);
    }

    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUrlRatingService().getAllUrlRatings(ordering, offset, count);
        return getAllUrlRatings(ordering, offset, count, null);
    }

    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<UrlRating> serverBeanList = getUrlRatingService().getAllUrlRatings(ordering, offset, count, forwardCursor);
        List<UrlRating> appBeanList = convertServerUrlRatingBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUrlRatingService().getAllUrlRatingKeys(ordering, offset, count);
        return getAllUrlRatingKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUrlRatingService().getAllUrlRatingKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUrlRatings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUrlRatingService().findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count);
        return findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<UrlRating> serverBeanList = getUrlRatingService().findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<UrlRating> appBeanList = convertServerUrlRatingBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUrlRatingService().findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUrlRatingService().findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUrlRatingService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUrlRating(String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException
    {
        return getUrlRatingService().createUrlRating(domain, pageUrl, preview, flag, rating, note, status, ratedTime);
    }

    @Override
    public String createUrlRating(UrlRating urlRating) throws BaseException
    {
        com.pagesynopsis.ws.bean.UrlRatingBean serverBean =  convertAppUrlRatingBeanToServerBean(urlRating);
        return getUrlRatingService().createUrlRating(serverBean);
    }

    @Override
    public Boolean updateUrlRating(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException
    {
        return getUrlRatingService().updateUrlRating(guid, domain, pageUrl, preview, flag, rating, note, status, ratedTime);
    }

    @Override
    public Boolean updateUrlRating(UrlRating urlRating) throws BaseException
    {
        com.pagesynopsis.ws.bean.UrlRatingBean serverBean =  convertAppUrlRatingBeanToServerBean(urlRating);
        return getUrlRatingService().updateUrlRating(serverBean);
    }

    @Override
    public Boolean deleteUrlRating(String guid) throws BaseException
    {
        return getUrlRatingService().deleteUrlRating(guid);
    }

    @Override
    public Boolean deleteUrlRating(UrlRating urlRating) throws BaseException
    {
        com.pagesynopsis.ws.bean.UrlRatingBean serverBean =  convertAppUrlRatingBeanToServerBean(urlRating);
        return getUrlRatingService().deleteUrlRating(serverBean);
    }

    @Override
    public Long deleteUrlRatings(String filter, String params, List<String> values) throws BaseException
    {
        return getUrlRatingService().deleteUrlRatings(filter, params, values);
    }




    public static UrlRatingBean convertServerUrlRatingBeanToAppBean(UrlRating serverBean)
    {
        UrlRatingBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new UrlRatingBean();
            bean.setGuid(serverBean.getGuid());
            bean.setDomain(serverBean.getDomain());
            bean.setPageUrl(serverBean.getPageUrl());
            bean.setPreview(serverBean.getPreview());
            bean.setFlag(serverBean.getFlag());
            bean.setRating(serverBean.getRating());
            bean.setNote(serverBean.getNote());
            bean.setStatus(serverBean.getStatus());
            bean.setRatedTime(serverBean.getRatedTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<UrlRating> convertServerUrlRatingBeanListToAppBeanList(List<UrlRating> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<UrlRating> beanList = new ArrayList<UrlRating>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(UrlRating sb : serverBeanList) {
                UrlRatingBean bean = convertServerUrlRatingBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.UrlRatingBean convertAppUrlRatingBeanToServerBean(UrlRating appBean)
    {
        com.pagesynopsis.ws.bean.UrlRatingBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.UrlRatingBean();
            bean.setGuid(appBean.getGuid());
            bean.setDomain(appBean.getDomain());
            bean.setPageUrl(appBean.getPageUrl());
            bean.setPreview(appBean.getPreview());
            bean.setFlag(appBean.getFlag());
            bean.setRating(appBean.getRating());
            bean.setNote(appBean.getNote());
            bean.setStatus(appBean.getStatus());
            bean.setRatedTime(appBean.getRatedTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<UrlRating> convertAppUrlRatingBeanListToServerBeanList(List<UrlRating> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<UrlRating> beanList = new ArrayList<UrlRating>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(UrlRating sb : appBeanList) {
                com.pagesynopsis.ws.bean.UrlRatingBean bean = convertAppUrlRatingBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
