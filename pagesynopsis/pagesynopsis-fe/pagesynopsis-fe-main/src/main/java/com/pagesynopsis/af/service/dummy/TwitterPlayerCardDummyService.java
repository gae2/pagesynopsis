package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterPlayerCardService;


// The primary purpose of TwitterPlayerCardDummyService is to fake the service api, TwitterPlayerCardService.
// It has no real implementation.
public class TwitterPlayerCardDummyService implements TwitterPlayerCardService
{
    private static final Logger log = Logger.getLogger(TwitterPlayerCardDummyService.class.getName());

    public TwitterPlayerCardDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterPlayerCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterPlayerCard getTwitterPlayerCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterPlayerCard(): guid = " + guid);
        return null;
    }

    @Override
    public Object getTwitterPlayerCard(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterPlayerCard(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<TwitterPlayerCard> getTwitterPlayerCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterPlayerCards()");
        return null;
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards() throws BaseException
    {
        return getAllTwitterPlayerCards(null, null, null);
    }


    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPlayerCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterPlayerCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPlayerCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterPlayerCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPlayerCardDummyService.findTwitterPlayerCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPlayerCardDummyService.findTwitterPlayerCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterPlayerCardDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createTwitterPlayerCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        log.finer("createTwitterPlayerCard()");
        return null;
    }

    @Override
    public String createTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("createTwitterPlayerCard()");
        return null;
    }

    @Override
    public TwitterPlayerCard constructTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("constructTwitterPlayerCard()");
        return null;
    }

    @Override
    public Boolean updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        log.finer("updateTwitterPlayerCard()");
        return null;
    }
        
    @Override
    public Boolean updateTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("updateTwitterPlayerCard()");
        return null;
    }

    @Override
    public TwitterPlayerCard refreshTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("refreshTwitterPlayerCard()");
        return null;
    }

    @Override
    public Boolean deleteTwitterPlayerCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTwitterPlayerCard(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        log.finer("deleteTwitterPlayerCard()");
        return null;
    }

    // TBD
    @Override
    public Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTwitterPlayerCard(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException
    {
        log.finer("createTwitterPlayerCards()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException
    //{
    //}

}
