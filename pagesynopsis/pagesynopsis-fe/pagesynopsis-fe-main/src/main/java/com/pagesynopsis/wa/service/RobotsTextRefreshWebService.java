package com.pagesynopsis.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.manager.ServiceManager;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.RobotsTextRefreshJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RobotsTextRefreshWebService // implements RobotsTextRefreshService
{
    private static final Logger log = Logger.getLogger(RobotsTextRefreshWebService.class.getName());
     
    // Af service interface.
    private RobotsTextRefreshService mService = null;

    public RobotsTextRefreshWebService()
    {
        this(ServiceManager.getRobotsTextRefreshService());
    }
    public RobotsTextRefreshWebService(RobotsTextRefreshService service)
    {
        mService = service;
    }
    
    private RobotsTextRefreshService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getRobotsTextRefreshService();
        }
        return mService;
    }
    
    
    public RobotsTextRefreshJsBean getRobotsTextRefresh(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            RobotsTextRefresh robotsTextRefresh = getService().getRobotsTextRefresh(guid);
            RobotsTextRefreshJsBean bean = convertRobotsTextRefreshToJsBean(robotsTextRefresh);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getRobotsTextRefresh(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getRobotsTextRefresh(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<RobotsTextRefreshJsBean> getRobotsTextRefreshes(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<RobotsTextRefreshJsBean> jsBeans = new ArrayList<RobotsTextRefreshJsBean>();
            List<RobotsTextRefresh> robotsTextRefreshes = getService().getRobotsTextRefreshes(guids);
            if(robotsTextRefreshes != null) {
                for(RobotsTextRefresh robotsTextRefresh : robotsTextRefreshes) {
                    jsBeans.add(convertRobotsTextRefreshToJsBean(robotsTextRefresh));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<RobotsTextRefreshJsBean> getAllRobotsTextRefreshes() throws WebException
    {
        return getAllRobotsTextRefreshes(null, null, null);
    }

    // @Deprecated
    public List<RobotsTextRefreshJsBean> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllRobotsTextRefreshes(ordering, offset, count, null);
    }

    public List<RobotsTextRefreshJsBean> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<RobotsTextRefreshJsBean> jsBeans = new ArrayList<RobotsTextRefreshJsBean>();
            List<RobotsTextRefresh> robotsTextRefreshes = getService().getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
            if(robotsTextRefreshes != null) {
                for(RobotsTextRefresh robotsTextRefresh : robotsTextRefreshes) {
                    jsBeans.add(convertRobotsTextRefreshToJsBean(robotsTextRefresh));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllRobotsTextRefreshKeys(ordering, offset, count, null);
    }

    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<RobotsTextRefreshJsBean> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<RobotsTextRefreshJsBean> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<RobotsTextRefreshJsBean> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<RobotsTextRefreshJsBean> jsBeans = new ArrayList<RobotsTextRefreshJsBean>();
            List<RobotsTextRefresh> robotsTextRefreshes = getService().findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(robotsTextRefreshes != null) {
                for(RobotsTextRefresh robotsTextRefresh : robotsTextRefreshes) {
                    jsBeans.add(convertRobotsTextRefreshToJsBean(robotsTextRefresh));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createRobotsTextRefresh(String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws WebException
    {
        try {
            return getService().createRobotsTextRefresh(robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createRobotsTextRefresh(String jsonStr) throws WebException
    {
        return createRobotsTextRefresh(RobotsTextRefreshJsBean.fromJsonString(jsonStr));
    }

    public String createRobotsTextRefresh(RobotsTextRefreshJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            RobotsTextRefresh robotsTextRefresh = convertRobotsTextRefreshJsBeanToBean(jsBean);
            return getService().createRobotsTextRefresh(robotsTextRefresh);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public RobotsTextRefreshJsBean constructRobotsTextRefresh(String jsonStr) throws WebException
    {
        return constructRobotsTextRefresh(RobotsTextRefreshJsBean.fromJsonString(jsonStr));
    }

    public RobotsTextRefreshJsBean constructRobotsTextRefresh(RobotsTextRefreshJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            RobotsTextRefresh robotsTextRefresh = convertRobotsTextRefreshJsBeanToBean(jsBean);
            robotsTextRefresh = getService().constructRobotsTextRefresh(robotsTextRefresh);
            jsBean = convertRobotsTextRefreshToJsBean(robotsTextRefresh);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateRobotsTextRefresh(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws WebException
    {
        try {
            return getService().updateRobotsTextRefresh(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateRobotsTextRefresh(String jsonStr) throws WebException
    {
        return updateRobotsTextRefresh(RobotsTextRefreshJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateRobotsTextRefresh(RobotsTextRefreshJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            RobotsTextRefresh robotsTextRefresh = convertRobotsTextRefreshJsBeanToBean(jsBean);
            return getService().updateRobotsTextRefresh(robotsTextRefresh);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public RobotsTextRefreshJsBean refreshRobotsTextRefresh(String jsonStr) throws WebException
    {
        return refreshRobotsTextRefresh(RobotsTextRefreshJsBean.fromJsonString(jsonStr));
    }

    public RobotsTextRefreshJsBean refreshRobotsTextRefresh(RobotsTextRefreshJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            RobotsTextRefresh robotsTextRefresh = convertRobotsTextRefreshJsBeanToBean(jsBean);
            robotsTextRefresh = getService().refreshRobotsTextRefresh(robotsTextRefresh);
            jsBean = convertRobotsTextRefreshToJsBean(robotsTextRefresh);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteRobotsTextRefresh(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteRobotsTextRefresh(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteRobotsTextRefresh(RobotsTextRefreshJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            RobotsTextRefresh robotsTextRefresh = convertRobotsTextRefreshJsBeanToBean(jsBean);
            return getService().deleteRobotsTextRefresh(robotsTextRefresh);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteRobotsTextRefreshes(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static RobotsTextRefreshJsBean convertRobotsTextRefreshToJsBean(RobotsTextRefresh robotsTextRefresh)
    {
        RobotsTextRefreshJsBean jsBean = null;
        if(robotsTextRefresh != null) {
            jsBean = new RobotsTextRefreshJsBean();
            jsBean.setGuid(robotsTextRefresh.getGuid());
            jsBean.setRobotsTextFile(robotsTextRefresh.getRobotsTextFile());
            jsBean.setRefreshInterval(robotsTextRefresh.getRefreshInterval());
            jsBean.setNote(robotsTextRefresh.getNote());
            jsBean.setStatus(robotsTextRefresh.getStatus());
            jsBean.setRefreshStatus(robotsTextRefresh.getRefreshStatus());
            jsBean.setResult(robotsTextRefresh.getResult());
            jsBean.setLastCheckedTime(robotsTextRefresh.getLastCheckedTime());
            jsBean.setNextCheckedTime(robotsTextRefresh.getNextCheckedTime());
            jsBean.setExpirationTime(robotsTextRefresh.getExpirationTime());
            jsBean.setCreatedTime(robotsTextRefresh.getCreatedTime());
            jsBean.setModifiedTime(robotsTextRefresh.getModifiedTime());
        }
        return jsBean;
    }

    public static RobotsTextRefresh convertRobotsTextRefreshJsBeanToBean(RobotsTextRefreshJsBean jsBean)
    {
        RobotsTextRefreshBean robotsTextRefresh = null;
        if(jsBean != null) {
            robotsTextRefresh = new RobotsTextRefreshBean();
            robotsTextRefresh.setGuid(jsBean.getGuid());
            robotsTextRefresh.setRobotsTextFile(jsBean.getRobotsTextFile());
            robotsTextRefresh.setRefreshInterval(jsBean.getRefreshInterval());
            robotsTextRefresh.setNote(jsBean.getNote());
            robotsTextRefresh.setStatus(jsBean.getStatus());
            robotsTextRefresh.setRefreshStatus(jsBean.getRefreshStatus());
            robotsTextRefresh.setResult(jsBean.getResult());
            robotsTextRefresh.setLastCheckedTime(jsBean.getLastCheckedTime());
            robotsTextRefresh.setNextCheckedTime(jsBean.getNextCheckedTime());
            robotsTextRefresh.setExpirationTime(jsBean.getExpirationTime());
            robotsTextRefresh.setCreatedTime(jsBean.getCreatedTime());
            robotsTextRefresh.setModifiedTime(jsBean.getModifiedTime());
        }
        return robotsTextRefresh;
    }

}
