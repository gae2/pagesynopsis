package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.af.bean.UrlRatingBean;
import com.pagesynopsis.ws.service.UrlRatingService;
import com.pagesynopsis.af.proxy.UrlRatingServiceProxy;


// MockUrlRatingServiceProxy is a decorator.
// It can be used as a base class to mock UrlRatingServiceProxy objects.
public abstract class MockUrlRatingServiceProxy implements UrlRatingServiceProxy
{
    private static final Logger log = Logger.getLogger(MockUrlRatingServiceProxy.class.getName());

    // MockUrlRatingServiceProxy uses the decorator design pattern.
    private UrlRatingServiceProxy decoratedProxy;

    public MockUrlRatingServiceProxy(UrlRatingServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected UrlRatingServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(UrlRatingServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public UrlRating getUrlRating(String guid) throws BaseException
    {
        return decoratedProxy.getUrlRating(guid);
    }

    @Override
    public Object getUrlRating(String guid, String field) throws BaseException
    {
        return decoratedProxy.getUrlRating(guid, field);       
    }

    @Override
    public List<UrlRating> getUrlRatings(List<String> guids) throws BaseException
    {
        return decoratedProxy.getUrlRatings(guids);
    }

    @Override
    public List<UrlRating> getAllUrlRatings() throws BaseException
    {
        return getAllUrlRatings(null, null, null);
    }

    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllUrlRatings(ordering, offset, count);
        return getAllUrlRatings(ordering, offset, count, null);
    }

    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUrlRatings(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllUrlRatingKeys(ordering, offset, count);
        return getAllUrlRatingKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUrlRatingKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUrlRatings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count);
        return findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUrlRating(String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException
    {
        return decoratedProxy.createUrlRating(domain, pageUrl, preview, flag, rating, note, status, ratedTime);
    }

    @Override
    public String createUrlRating(UrlRating urlRating) throws BaseException
    {
        return decoratedProxy.createUrlRating(urlRating);
    }

    @Override
    public Boolean updateUrlRating(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException
    {
        return decoratedProxy.updateUrlRating(guid, domain, pageUrl, preview, flag, rating, note, status, ratedTime);
    }

    @Override
    public Boolean updateUrlRating(UrlRating urlRating) throws BaseException
    {
        return decoratedProxy.updateUrlRating(urlRating);
    }

    @Override
    public Boolean deleteUrlRating(String guid) throws BaseException
    {
        return decoratedProxy.deleteUrlRating(guid);
    }

    @Override
    public Boolean deleteUrlRating(UrlRating urlRating) throws BaseException
    {
        String guid = urlRating.getGuid();
        return deleteUrlRating(guid);
    }

    @Override
    public Long deleteUrlRatings(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteUrlRatings(filter, params, values);
    }

}
