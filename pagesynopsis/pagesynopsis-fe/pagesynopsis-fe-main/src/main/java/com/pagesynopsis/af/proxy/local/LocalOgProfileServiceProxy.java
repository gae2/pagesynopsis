package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
// import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.proxy.OgProfileServiceProxy;


public class LocalOgProfileServiceProxy extends BaseLocalServiceProxy implements OgProfileServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgProfileServiceProxy.class.getName());

    public LocalOgProfileServiceProxy()
    {
    }

    @Override
    public OgProfile getOgProfile(String guid) throws BaseException
    {
        OgProfile serverBean = getOgProfileService().getOgProfile(guid);
        OgProfile appBean = convertServerOgProfileBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getOgProfile(String guid, String field) throws BaseException
    {
        return getOgProfileService().getOgProfile(guid, field);       
    }

    @Override
    public List<OgProfile> getOgProfiles(List<String> guids) throws BaseException
    {
        List<OgProfile> serverBeanList = getOgProfileService().getOgProfiles(guids);
        List<OgProfile> appBeanList = convertServerOgProfileBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<OgProfile> getAllOgProfiles() throws BaseException
    {
        return getAllOgProfiles(null, null, null);
    }

    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgProfileService().getAllOgProfiles(ordering, offset, count);
        return getAllOgProfiles(ordering, offset, count, null);
    }

    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgProfile> serverBeanList = getOgProfileService().getAllOgProfiles(ordering, offset, count, forwardCursor);
        List<OgProfile> appBeanList = convertServerOgProfileBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgProfileService().getAllOgProfileKeys(ordering, offset, count);
        return getAllOgProfileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgProfileService().getAllOgProfileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgProfiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgProfileService().findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgProfile> serverBeanList = getOgProfileService().findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<OgProfile> appBeanList = convertServerOgProfileBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgProfileService().findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgProfileService().findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgProfileService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgProfile(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        return getOgProfileService().createOgProfile(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
    }

    @Override
    public String createOgProfile(OgProfile ogProfile) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgProfileBean serverBean =  convertAppOgProfileBeanToServerBean(ogProfile);
        return getOgProfileService().createOgProfile(serverBean);
    }

    @Override
    public Boolean updateOgProfile(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        return getOgProfileService().updateOgProfile(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
    }

    @Override
    public Boolean updateOgProfile(OgProfile ogProfile) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgProfileBean serverBean =  convertAppOgProfileBeanToServerBean(ogProfile);
        return getOgProfileService().updateOgProfile(serverBean);
    }

    @Override
    public Boolean deleteOgProfile(String guid) throws BaseException
    {
        return getOgProfileService().deleteOgProfile(guid);
    }

    @Override
    public Boolean deleteOgProfile(OgProfile ogProfile) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgProfileBean serverBean =  convertAppOgProfileBeanToServerBean(ogProfile);
        return getOgProfileService().deleteOgProfile(serverBean);
    }

    @Override
    public Long deleteOgProfiles(String filter, String params, List<String> values) throws BaseException
    {
        return getOgProfileService().deleteOgProfiles(filter, params, values);
    }




    public static OgProfileBean convertServerOgProfileBeanToAppBean(OgProfile serverBean)
    {
        OgProfileBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new OgProfileBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUrl(serverBean.getUrl());
            bean.setType(serverBean.getType());
            bean.setSiteName(serverBean.getSiteName());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setFbAdmins(serverBean.getFbAdmins());
            bean.setFbAppId(serverBean.getFbAppId());
            bean.setImage(serverBean.getImage());
            bean.setAudio(serverBean.getAudio());
            bean.setVideo(serverBean.getVideo());
            bean.setLocale(serverBean.getLocale());
            bean.setLocaleAlternate(serverBean.getLocaleAlternate());
            bean.setProfileId(serverBean.getProfileId());
            bean.setFirstName(serverBean.getFirstName());
            bean.setLastName(serverBean.getLastName());
            bean.setUsername(serverBean.getUsername());
            bean.setGender(serverBean.getGender());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgProfile> convertServerOgProfileBeanListToAppBeanList(List<OgProfile> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<OgProfile> beanList = new ArrayList<OgProfile>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(OgProfile sb : serverBeanList) {
                OgProfileBean bean = convertServerOgProfileBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.OgProfileBean convertAppOgProfileBeanToServerBean(OgProfile appBean)
    {
        com.pagesynopsis.ws.bean.OgProfileBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgProfileBean();
            bean.setGuid(appBean.getGuid());
            bean.setUrl(appBean.getUrl());
            bean.setType(appBean.getType());
            bean.setSiteName(appBean.getSiteName());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setFbAdmins(appBean.getFbAdmins());
            bean.setFbAppId(appBean.getFbAppId());
            bean.setImage(appBean.getImage());
            bean.setAudio(appBean.getAudio());
            bean.setVideo(appBean.getVideo());
            bean.setLocale(appBean.getLocale());
            bean.setLocaleAlternate(appBean.getLocaleAlternate());
            bean.setProfileId(appBean.getProfileId());
            bean.setFirstName(appBean.getFirstName());
            bean.setLastName(appBean.getLastName());
            bean.setUsername(appBean.getUsername());
            bean.setGender(appBean.getGender());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgProfile> convertAppOgProfileBeanListToServerBeanList(List<OgProfile> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<OgProfile> beanList = new ArrayList<OgProfile>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(OgProfile sb : appBeanList) {
                com.pagesynopsis.ws.bean.OgProfileBean bean = convertAppOgProfileBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
