package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgTvShowService;


// The primary purpose of OgTvShowDummyService is to fake the service api, OgTvShowService.
// It has no real implementation.
public class OgTvShowDummyService implements OgTvShowService
{
    private static final Logger log = Logger.getLogger(OgTvShowDummyService.class.getName());

    public OgTvShowDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // OgTvShow related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgTvShow getOgTvShow(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgTvShow(): guid = " + guid);
        return null;
    }

    @Override
    public Object getOgTvShow(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgTvShow(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<OgTvShow> getOgTvShows(List<String> guids) throws BaseException
    {
        log.fine("getOgTvShows()");
        return null;
    }

    @Override
    public List<OgTvShow> getAllOgTvShows() throws BaseException
    {
        return getAllOgTvShows(null, null, null);
    }


    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShows(ordering, offset, count, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgTvShows(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShowKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgTvShowKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvShowDummyService.findOgTvShows(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvShowDummyService.findOgTvShowKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvShowDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createOgTvShow(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        log.finer("createOgTvShow()");
        return null;
    }

    @Override
    public String createOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("createOgTvShow()");
        return null;
    }

    @Override
    public OgTvShow constructOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("constructOgTvShow()");
        return null;
    }

    @Override
    public Boolean updateOgTvShow(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        log.finer("updateOgTvShow()");
        return null;
    }
        
    @Override
    public Boolean updateOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("updateOgTvShow()");
        return null;
    }

    @Override
    public OgTvShow refreshOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("refreshOgTvShow()");
        return null;
    }

    @Override
    public Boolean deleteOgTvShow(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgTvShow(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("deleteOgTvShow()");
        return null;
    }

    // TBD
    @Override
    public Long deleteOgTvShows(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgTvShow(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgTvShows(List<OgTvShow> ogTvShows) throws BaseException
    {
        log.finer("createOgTvShows()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateOgTvShows(List<OgTvShow> ogTvShows) throws BaseException
    //{
    //}

}
