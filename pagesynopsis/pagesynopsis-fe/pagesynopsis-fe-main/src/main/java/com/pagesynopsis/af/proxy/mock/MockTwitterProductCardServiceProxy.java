package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.af.proxy.TwitterProductCardServiceProxy;


// MockTwitterProductCardServiceProxy is a decorator.
// It can be used as a base class to mock TwitterProductCardServiceProxy objects.
public abstract class MockTwitterProductCardServiceProxy implements TwitterProductCardServiceProxy
{
    private static final Logger log = Logger.getLogger(MockTwitterProductCardServiceProxy.class.getName());

    // MockTwitterProductCardServiceProxy uses the decorator design pattern.
    private TwitterProductCardServiceProxy decoratedProxy;

    public MockTwitterProductCardServiceProxy(TwitterProductCardServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected TwitterProductCardServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(TwitterProductCardServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public TwitterProductCard getTwitterProductCard(String guid) throws BaseException
    {
        return decoratedProxy.getTwitterProductCard(guid);
    }

    @Override
    public Object getTwitterProductCard(String guid, String field) throws BaseException
    {
        return decoratedProxy.getTwitterProductCard(guid, field);       
    }

    @Override
    public List<TwitterProductCard> getTwitterProductCards(List<String> guids) throws BaseException
    {
        return decoratedProxy.getTwitterProductCards(guids);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards() throws BaseException
    {
        return getAllTwitterProductCards(null, null, null);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllTwitterProductCards(ordering, offset, count);
        return getAllTwitterProductCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterProductCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllTwitterProductCardKeys(ordering, offset, count);
        return getAllTwitterProductCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterProductCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterProductCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterProductCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException
    {
        return decoratedProxy.createTwitterProductCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1, data2);
    }

    @Override
    public String createTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        return decoratedProxy.createTwitterProductCard(twitterProductCard);
    }

    @Override
    public Boolean updateTwitterProductCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException
    {
        return decoratedProxy.updateTwitterProductCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1, data2);
    }

    @Override
    public Boolean updateTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        return decoratedProxy.updateTwitterProductCard(twitterProductCard);
    }

    @Override
    public Boolean deleteTwitterProductCard(String guid) throws BaseException
    {
        return decoratedProxy.deleteTwitterProductCard(guid);
    }

    @Override
    public Boolean deleteTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        String guid = twitterProductCard.getGuid();
        return deleteTwitterProductCard(guid);
    }

    @Override
    public Long deleteTwitterProductCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteTwitterProductCards(filter, params, values);
    }

}
