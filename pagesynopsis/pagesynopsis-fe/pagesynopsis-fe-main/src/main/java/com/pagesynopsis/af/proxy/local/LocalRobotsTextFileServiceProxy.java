package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
// import com.pagesynopsis.ws.bean.RobotsTextFileBean;
import com.pagesynopsis.ws.service.RobotsTextFileService;
import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.proxy.RobotsTextFileServiceProxy;


public class LocalRobotsTextFileServiceProxy extends BaseLocalServiceProxy implements RobotsTextFileServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalRobotsTextFileServiceProxy.class.getName());

    public LocalRobotsTextFileServiceProxy()
    {
    }

    @Override
    public RobotsTextFile getRobotsTextFile(String guid) throws BaseException
    {
        RobotsTextFile serverBean = getRobotsTextFileService().getRobotsTextFile(guid);
        RobotsTextFile appBean = convertServerRobotsTextFileBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getRobotsTextFile(String guid, String field) throws BaseException
    {
        return getRobotsTextFileService().getRobotsTextFile(guid, field);       
    }

    @Override
    public List<RobotsTextFile> getRobotsTextFiles(List<String> guids) throws BaseException
    {
        List<RobotsTextFile> serverBeanList = getRobotsTextFileService().getRobotsTextFiles(guids);
        List<RobotsTextFile> appBeanList = convertServerRobotsTextFileBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles() throws BaseException
    {
        return getAllRobotsTextFiles(null, null, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextFileService().getAllRobotsTextFiles(ordering, offset, count);
        return getAllRobotsTextFiles(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<RobotsTextFile> serverBeanList = getRobotsTextFileService().getAllRobotsTextFiles(ordering, offset, count, forwardCursor);
        List<RobotsTextFile> appBeanList = convertServerRobotsTextFileBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextFileService().getAllRobotsTextFileKeys(ordering, offset, count);
        return getAllRobotsTextFileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextFileService().getAllRobotsTextFileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findRobotsTextFiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextFileService().findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<RobotsTextFile> serverBeanList = getRobotsTextFileService().findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<RobotsTextFile> appBeanList = convertServerRobotsTextFileBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextFileService().findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextFileService().findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getRobotsTextFileService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createRobotsTextFile(String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return getRobotsTextFileService().createRobotsTextFile(siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public String createRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        com.pagesynopsis.ws.bean.RobotsTextFileBean serverBean =  convertAppRobotsTextFileBeanToServerBean(robotsTextFile);
        return getRobotsTextFileService().createRobotsTextFile(serverBean);
    }

    @Override
    public Boolean updateRobotsTextFile(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return getRobotsTextFileService().updateRobotsTextFile(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public Boolean updateRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        com.pagesynopsis.ws.bean.RobotsTextFileBean serverBean =  convertAppRobotsTextFileBeanToServerBean(robotsTextFile);
        return getRobotsTextFileService().updateRobotsTextFile(serverBean);
    }

    @Override
    public Boolean deleteRobotsTextFile(String guid) throws BaseException
    {
        return getRobotsTextFileService().deleteRobotsTextFile(guid);
    }

    @Override
    public Boolean deleteRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        com.pagesynopsis.ws.bean.RobotsTextFileBean serverBean =  convertAppRobotsTextFileBeanToServerBean(robotsTextFile);
        return getRobotsTextFileService().deleteRobotsTextFile(serverBean);
    }

    @Override
    public Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseException
    {
        return getRobotsTextFileService().deleteRobotsTextFiles(filter, params, values);
    }




    public static RobotsTextFileBean convertServerRobotsTextFileBeanToAppBean(RobotsTextFile serverBean)
    {
        RobotsTextFileBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new RobotsTextFileBean();
            bean.setGuid(serverBean.getGuid());
            bean.setSiteUrl(serverBean.getSiteUrl());
            bean.setGroups(serverBean.getGroups());
            bean.setSitemaps(serverBean.getSitemaps());
            bean.setContent(serverBean.getContent());
            bean.setContentHash(serverBean.getContentHash());
            bean.setLastCheckedTime(serverBean.getLastCheckedTime());
            bean.setLastUpdatedTime(serverBean.getLastUpdatedTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<RobotsTextFile> convertServerRobotsTextFileBeanListToAppBeanList(List<RobotsTextFile> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<RobotsTextFile> beanList = new ArrayList<RobotsTextFile>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(RobotsTextFile sb : serverBeanList) {
                RobotsTextFileBean bean = convertServerRobotsTextFileBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.RobotsTextFileBean convertAppRobotsTextFileBeanToServerBean(RobotsTextFile appBean)
    {
        com.pagesynopsis.ws.bean.RobotsTextFileBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.RobotsTextFileBean();
            bean.setGuid(appBean.getGuid());
            bean.setSiteUrl(appBean.getSiteUrl());
            bean.setGroups(appBean.getGroups());
            bean.setSitemaps(appBean.getSitemaps());
            bean.setContent(appBean.getContent());
            bean.setContentHash(appBean.getContentHash());
            bean.setLastCheckedTime(appBean.getLastCheckedTime());
            bean.setLastUpdatedTime(appBean.getLastUpdatedTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<RobotsTextFile> convertAppRobotsTextFileBeanListToServerBeanList(List<RobotsTextFile> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<RobotsTextFile> beanList = new ArrayList<RobotsTextFile>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(RobotsTextFile sb : appBeanList) {
                com.pagesynopsis.ws.bean.RobotsTextFileBean bean = convertAppRobotsTextFileBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
