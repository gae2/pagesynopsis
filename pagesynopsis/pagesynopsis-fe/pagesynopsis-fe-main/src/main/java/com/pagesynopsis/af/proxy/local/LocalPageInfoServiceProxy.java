package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageInfo;
// import com.pagesynopsis.ws.bean.PageInfoBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.PageInfoService;
import com.pagesynopsis.af.bean.PageInfoBean;
import com.pagesynopsis.af.proxy.PageInfoServiceProxy;


public class LocalPageInfoServiceProxy extends BaseLocalServiceProxy implements PageInfoServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalPageInfoServiceProxy.class.getName());

    public LocalPageInfoServiceProxy()
    {
    }

    @Override
    public PageInfo getPageInfo(String guid) throws BaseException
    {
        PageInfo serverBean = getPageInfoService().getPageInfo(guid);
        PageInfo appBean = convertServerPageInfoBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getPageInfo(String guid, String field) throws BaseException
    {
        return getPageInfoService().getPageInfo(guid, field);       
    }

    @Override
    public List<PageInfo> getPageInfos(List<String> guids) throws BaseException
    {
        List<PageInfo> serverBeanList = getPageInfoService().getPageInfos(guids);
        List<PageInfo> appBeanList = convertServerPageInfoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<PageInfo> getAllPageInfos() throws BaseException
    {
        return getAllPageInfos(null, null, null);
    }

    @Override
    public List<PageInfo> getAllPageInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getPageInfoService().getAllPageInfos(ordering, offset, count);
        return getAllPageInfos(ordering, offset, count, null);
    }

    @Override
    public List<PageInfo> getAllPageInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<PageInfo> serverBeanList = getPageInfoService().getAllPageInfos(ordering, offset, count, forwardCursor);
        List<PageInfo> appBeanList = convertServerPageInfoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getPageInfoService().getAllPageInfoKeys(ordering, offset, count);
        return getAllPageInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getPageInfoService().getAllPageInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<PageInfo> findPageInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findPageInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<PageInfo> findPageInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getPageInfoService().findPageInfos(filter, ordering, params, values, grouping, unique, offset, count);
        return findPageInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<PageInfo> findPageInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<PageInfo> serverBeanList = getPageInfoService().findPageInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<PageInfo> appBeanList = convertServerPageInfoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getPageInfoService().findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getPageInfoService().findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getPageInfoService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createPageInfo(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String pageAuthor, String pageDescription, String favicon, String faviconUrl) throws BaseException
    {
        return getPageInfoService().createPageInfo(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, pageAuthor, pageDescription, favicon, faviconUrl);
    }

    @Override
    public String createPageInfo(PageInfo pageInfo) throws BaseException
    {
        com.pagesynopsis.ws.bean.PageInfoBean serverBean =  convertAppPageInfoBeanToServerBean(pageInfo);
        return getPageInfoService().createPageInfo(serverBean);
    }

    @Override
    public Boolean updatePageInfo(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String pageAuthor, String pageDescription, String favicon, String faviconUrl) throws BaseException
    {
        return getPageInfoService().updatePageInfo(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, pageAuthor, pageDescription, favicon, faviconUrl);
    }

    @Override
    public Boolean updatePageInfo(PageInfo pageInfo) throws BaseException
    {
        com.pagesynopsis.ws.bean.PageInfoBean serverBean =  convertAppPageInfoBeanToServerBean(pageInfo);
        return getPageInfoService().updatePageInfo(serverBean);
    }

    @Override
    public Boolean deletePageInfo(String guid) throws BaseException
    {
        return getPageInfoService().deletePageInfo(guid);
    }

    @Override
    public Boolean deletePageInfo(PageInfo pageInfo) throws BaseException
    {
        com.pagesynopsis.ws.bean.PageInfoBean serverBean =  convertAppPageInfoBeanToServerBean(pageInfo);
        return getPageInfoService().deletePageInfo(serverBean);
    }

    @Override
    public Long deletePageInfos(String filter, String params, List<String> values) throws BaseException
    {
        return getPageInfoService().deletePageInfos(filter, params, values);
    }




    public static PageInfoBean convertServerPageInfoBeanToAppBean(PageInfo serverBean)
    {
        PageInfoBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new PageInfoBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setFetchRequest(serverBean.getFetchRequest());
            bean.setTargetUrl(serverBean.getTargetUrl());
            bean.setPageUrl(serverBean.getPageUrl());
            bean.setQueryString(serverBean.getQueryString());
            bean.setQueryParams(serverBean.getQueryParams());
            bean.setLastFetchResult(serverBean.getLastFetchResult());
            bean.setResponseCode(serverBean.getResponseCode());
            bean.setContentType(serverBean.getContentType());
            bean.setContentLength(serverBean.getContentLength());
            bean.setLanguage(serverBean.getLanguage());
            bean.setRedirect(serverBean.getRedirect());
            bean.setLocation(serverBean.getLocation());
            bean.setPageTitle(serverBean.getPageTitle());
            bean.setNote(serverBean.getNote());
            bean.setDeferred(serverBean.isDeferred());
            bean.setStatus(serverBean.getStatus());
            bean.setRefreshStatus(serverBean.getRefreshStatus());
            bean.setRefreshInterval(serverBean.getRefreshInterval());
            bean.setNextRefreshTime(serverBean.getNextRefreshTime());
            bean.setLastCheckedTime(serverBean.getLastCheckedTime());
            bean.setLastUpdatedTime(serverBean.getLastUpdatedTime());
            bean.setPageAuthor(serverBean.getPageAuthor());
            bean.setPageDescription(serverBean.getPageDescription());
            bean.setFavicon(serverBean.getFavicon());
            bean.setFaviconUrl(serverBean.getFaviconUrl());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<PageInfo> convertServerPageInfoBeanListToAppBeanList(List<PageInfo> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<PageInfo> beanList = new ArrayList<PageInfo>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(PageInfo sb : serverBeanList) {
                PageInfoBean bean = convertServerPageInfoBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.PageInfoBean convertAppPageInfoBeanToServerBean(PageInfo appBean)
    {
        com.pagesynopsis.ws.bean.PageInfoBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.PageInfoBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setFetchRequest(appBean.getFetchRequest());
            bean.setTargetUrl(appBean.getTargetUrl());
            bean.setPageUrl(appBean.getPageUrl());
            bean.setQueryString(appBean.getQueryString());
            bean.setQueryParams(appBean.getQueryParams());
            bean.setLastFetchResult(appBean.getLastFetchResult());
            bean.setResponseCode(appBean.getResponseCode());
            bean.setContentType(appBean.getContentType());
            bean.setContentLength(appBean.getContentLength());
            bean.setLanguage(appBean.getLanguage());
            bean.setRedirect(appBean.getRedirect());
            bean.setLocation(appBean.getLocation());
            bean.setPageTitle(appBean.getPageTitle());
            bean.setNote(appBean.getNote());
            bean.setDeferred(appBean.isDeferred());
            bean.setStatus(appBean.getStatus());
            bean.setRefreshStatus(appBean.getRefreshStatus());
            bean.setRefreshInterval(appBean.getRefreshInterval());
            bean.setNextRefreshTime(appBean.getNextRefreshTime());
            bean.setLastCheckedTime(appBean.getLastCheckedTime());
            bean.setLastUpdatedTime(appBean.getLastUpdatedTime());
            bean.setPageAuthor(appBean.getPageAuthor());
            bean.setPageDescription(appBean.getPageDescription());
            bean.setFavicon(appBean.getFavicon());
            bean.setFaviconUrl(appBean.getFaviconUrl());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<PageInfo> convertAppPageInfoBeanListToServerBeanList(List<PageInfo> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<PageInfo> beanList = new ArrayList<PageInfo>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(PageInfo sb : appBeanList) {
                com.pagesynopsis.ws.bean.PageInfoBean bean = convertAppPageInfoBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
