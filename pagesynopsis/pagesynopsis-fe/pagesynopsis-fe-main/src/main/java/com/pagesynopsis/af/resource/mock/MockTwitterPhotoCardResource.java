package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardListStub;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.resource.TwitterPhotoCardResource;
import com.pagesynopsis.af.resource.util.TwitterCardAppInfoResourceUtil;
import com.pagesynopsis.af.resource.util.TwitterCardProductDataResourceUtil;


// MockTwitterPhotoCardResource is a decorator.
// It can be used as a base class to mock TwitterPhotoCardResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/twitterPhotoCards/")
public abstract class MockTwitterPhotoCardResource implements TwitterPhotoCardResource
{
    private static final Logger log = Logger.getLogger(MockTwitterPhotoCardResource.class.getName());

    // MockTwitterPhotoCardResource uses the decorator design pattern.
    private TwitterPhotoCardResource decoratedResource;

    public MockTwitterPhotoCardResource(TwitterPhotoCardResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected TwitterPhotoCardResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(TwitterPhotoCardResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllTwitterPhotoCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterPhotoCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterPhotoCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterPhotoCardsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findTwitterPhotoCardsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getTwitterPhotoCardAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getTwitterPhotoCardAsHtml(guid);
//    }

    @Override
    public Response getTwitterPhotoCard(String guid) throws BaseResourceException
    {
        return decoratedResource.getTwitterPhotoCard(guid);
    }

    @Override
    public Response getTwitterPhotoCardAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getTwitterPhotoCardAsJsonp(guid, callback);
    }

    @Override
    public Response getTwitterPhotoCard(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getTwitterPhotoCard(guid, field);
    }

    // TBD
    @Override
    public Response constructTwitterPhotoCard(TwitterPhotoCardStub twitterPhotoCard) throws BaseResourceException
    {
        return decoratedResource.constructTwitterPhotoCard(twitterPhotoCard);
    }

    @Override
    public Response createTwitterPhotoCard(TwitterPhotoCardStub twitterPhotoCard) throws BaseResourceException
    {
        return decoratedResource.createTwitterPhotoCard(twitterPhotoCard);
    }

//    @Override
//    public Response createTwitterPhotoCard(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createTwitterPhotoCard(formParams);
//    }

    // TBD
    @Override
    public Response refreshTwitterPhotoCard(String guid, TwitterPhotoCardStub twitterPhotoCard) throws BaseResourceException
    {
        return decoratedResource.refreshTwitterPhotoCard(guid, twitterPhotoCard);
    }

    @Override
    public Response updateTwitterPhotoCard(String guid, TwitterPhotoCardStub twitterPhotoCard) throws BaseResourceException
    {
        return decoratedResource.updateTwitterPhotoCard(guid, twitterPhotoCard);
    }

    @Override
    public Response updateTwitterPhotoCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight)
    {
        return decoratedResource.updateTwitterPhotoCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

//    @Override
//    public Response updateTwitterPhotoCard(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateTwitterPhotoCard(guid, formParams);
//    }

    @Override
    public Response deleteTwitterPhotoCard(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterPhotoCard(guid);
    }

    @Override
    public Response deleteTwitterPhotoCards(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterPhotoCards(filter, params, values);
    }


// TBD ....
    @Override
    public Response createTwitterPhotoCards(TwitterPhotoCardListStub twitterPhotoCards) throws BaseResourceException
    {
        return decoratedResource.createTwitterPhotoCards(twitterPhotoCards);
    }


}
