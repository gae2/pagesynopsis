package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBlog;
// import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.proxy.OgBlogServiceProxy;


public class LocalOgBlogServiceProxy extends BaseLocalServiceProxy implements OgBlogServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgBlogServiceProxy.class.getName());

    public LocalOgBlogServiceProxy()
    {
    }

    @Override
    public OgBlog getOgBlog(String guid) throws BaseException
    {
        OgBlog serverBean = getOgBlogService().getOgBlog(guid);
        OgBlog appBean = convertServerOgBlogBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getOgBlog(String guid, String field) throws BaseException
    {
        return getOgBlogService().getOgBlog(guid, field);       
    }

    @Override
    public List<OgBlog> getOgBlogs(List<String> guids) throws BaseException
    {
        List<OgBlog> serverBeanList = getOgBlogService().getOgBlogs(guids);
        List<OgBlog> appBeanList = convertServerOgBlogBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<OgBlog> getAllOgBlogs() throws BaseException
    {
        return getAllOgBlogs(null, null, null);
    }

    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgBlogService().getAllOgBlogs(ordering, offset, count);
        return getAllOgBlogs(ordering, offset, count, null);
    }

    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgBlog> serverBeanList = getOgBlogService().getAllOgBlogs(ordering, offset, count, forwardCursor);
        List<OgBlog> appBeanList = convertServerOgBlogBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgBlogService().getAllOgBlogKeys(ordering, offset, count);
        return getAllOgBlogKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgBlogService().getAllOgBlogKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgBlogs(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgBlogService().findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgBlog> serverBeanList = getOgBlogService().findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<OgBlog> appBeanList = convertServerOgBlogBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgBlogService().findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgBlogService().findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgBlogService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgBlog(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return getOgBlogService().createOgBlog(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public String createOgBlog(OgBlog ogBlog) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgBlogBean serverBean =  convertAppOgBlogBeanToServerBean(ogBlog);
        return getOgBlogService().createOgBlog(serverBean);
    }

    @Override
    public Boolean updateOgBlog(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return getOgBlogService().updateOgBlog(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public Boolean updateOgBlog(OgBlog ogBlog) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgBlogBean serverBean =  convertAppOgBlogBeanToServerBean(ogBlog);
        return getOgBlogService().updateOgBlog(serverBean);
    }

    @Override
    public Boolean deleteOgBlog(String guid) throws BaseException
    {
        return getOgBlogService().deleteOgBlog(guid);
    }

    @Override
    public Boolean deleteOgBlog(OgBlog ogBlog) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgBlogBean serverBean =  convertAppOgBlogBeanToServerBean(ogBlog);
        return getOgBlogService().deleteOgBlog(serverBean);
    }

    @Override
    public Long deleteOgBlogs(String filter, String params, List<String> values) throws BaseException
    {
        return getOgBlogService().deleteOgBlogs(filter, params, values);
    }




    public static OgBlogBean convertServerOgBlogBeanToAppBean(OgBlog serverBean)
    {
        OgBlogBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new OgBlogBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUrl(serverBean.getUrl());
            bean.setType(serverBean.getType());
            bean.setSiteName(serverBean.getSiteName());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setFbAdmins(serverBean.getFbAdmins());
            bean.setFbAppId(serverBean.getFbAppId());
            bean.setImage(serverBean.getImage());
            bean.setAudio(serverBean.getAudio());
            bean.setVideo(serverBean.getVideo());
            bean.setLocale(serverBean.getLocale());
            bean.setLocaleAlternate(serverBean.getLocaleAlternate());
            bean.setNote(serverBean.getNote());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgBlog> convertServerOgBlogBeanListToAppBeanList(List<OgBlog> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<OgBlog> beanList = new ArrayList<OgBlog>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(OgBlog sb : serverBeanList) {
                OgBlogBean bean = convertServerOgBlogBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.OgBlogBean convertAppOgBlogBeanToServerBean(OgBlog appBean)
    {
        com.pagesynopsis.ws.bean.OgBlogBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgBlogBean();
            bean.setGuid(appBean.getGuid());
            bean.setUrl(appBean.getUrl());
            bean.setType(appBean.getType());
            bean.setSiteName(appBean.getSiteName());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setFbAdmins(appBean.getFbAdmins());
            bean.setFbAppId(appBean.getFbAppId());
            bean.setImage(appBean.getImage());
            bean.setAudio(appBean.getAudio());
            bean.setVideo(appBean.getVideo());
            bean.setLocale(appBean.getLocale());
            bean.setLocaleAlternate(appBean.getLocaleAlternate());
            bean.setNote(appBean.getNote());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgBlog> convertAppOgBlogBeanListToServerBeanList(List<OgBlog> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<OgBlog> beanList = new ArrayList<OgBlog>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(OgBlog sb : appBeanList) {
                com.pagesynopsis.ws.bean.OgBlogBean bean = convertAppOgBlogBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
