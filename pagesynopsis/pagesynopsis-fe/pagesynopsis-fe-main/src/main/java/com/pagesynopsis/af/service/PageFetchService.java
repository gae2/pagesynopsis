package com.pagesynopsis.af.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface PageFetchService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    PageFetch getPageFetch(String guid) throws BaseException;
    Object getPageFetch(String guid, String field) throws BaseException;
    List<PageFetch> getPageFetches(List<String> guids) throws BaseException;
    List<PageFetch> getAllPageFetches() throws BaseException;
    /* @Deprecated */ List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count) throws BaseException;
    List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createPageFetch(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException;
    //String createPageFetch(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return PageFetch?)
    String createPageFetch(PageFetch pageFetch) throws BaseException;
    PageFetch constructPageFetch(PageFetch pageFetch) throws BaseException;
    Boolean updatePageFetch(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException;
    //Boolean updatePageFetch(String guid, Map<String, Object> args) throws BaseException;
    Boolean updatePageFetch(PageFetch pageFetch) throws BaseException;
    PageFetch refreshPageFetch(PageFetch pageFetch) throws BaseException;
    Boolean deletePageFetch(String guid) throws BaseException;
    Boolean deletePageFetch(PageFetch pageFetch) throws BaseException;
    Long deletePageFetches(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createPageFetches(List<PageFetch> pageFetches) throws BaseException;
//    Boolean updatePageFetches(List<PageFetch> pageFetches) throws BaseException;

}
