package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgContactInfoStruct;
// import com.pagesynopsis.ws.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;


public class OgContactInfoStructProxyUtil
{
    private static final Logger log = Logger.getLogger(OgContactInfoStructProxyUtil.class.getName());

    // Static methods only.
    private OgContactInfoStructProxyUtil() {}

    public static OgContactInfoStructBean convertServerOgContactInfoStructBeanToAppBean(OgContactInfoStruct serverBean)
    {
        OgContactInfoStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new OgContactInfoStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setStreetAddress(serverBean.getStreetAddress());
            bean.setLocality(serverBean.getLocality());
            bean.setRegion(serverBean.getRegion());
            bean.setPostalCode(serverBean.getPostalCode());
            bean.setCountryName(serverBean.getCountryName());
            bean.setEmailAddress(serverBean.getEmailAddress());
            bean.setPhoneNumber(serverBean.getPhoneNumber());
            bean.setFaxNumber(serverBean.getFaxNumber());
            bean.setWebsite(serverBean.getWebsite());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.OgContactInfoStructBean convertAppOgContactInfoStructBeanToServerBean(OgContactInfoStruct appBean)
    {
        com.pagesynopsis.ws.bean.OgContactInfoStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgContactInfoStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setStreetAddress(appBean.getStreetAddress());
            bean.setLocality(appBean.getLocality());
            bean.setRegion(appBean.getRegion());
            bean.setPostalCode(appBean.getPostalCode());
            bean.setCountryName(appBean.getCountryName());
            bean.setEmailAddress(appBean.getEmailAddress());
            bean.setPhoneNumber(appBean.getPhoneNumber());
            bean.setFaxNumber(appBean.getFaxNumber());
            bean.setWebsite(appBean.getWebsite());
        }
        return bean;
    }

}
