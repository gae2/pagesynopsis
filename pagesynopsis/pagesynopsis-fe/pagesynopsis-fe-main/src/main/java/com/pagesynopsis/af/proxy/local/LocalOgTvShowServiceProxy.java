package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
// import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.proxy.OgTvShowServiceProxy;


public class LocalOgTvShowServiceProxy extends BaseLocalServiceProxy implements OgTvShowServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgTvShowServiceProxy.class.getName());

    public LocalOgTvShowServiceProxy()
    {
    }

    @Override
    public OgTvShow getOgTvShow(String guid) throws BaseException
    {
        OgTvShow serverBean = getOgTvShowService().getOgTvShow(guid);
        OgTvShow appBean = convertServerOgTvShowBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getOgTvShow(String guid, String field) throws BaseException
    {
        return getOgTvShowService().getOgTvShow(guid, field);       
    }

    @Override
    public List<OgTvShow> getOgTvShows(List<String> guids) throws BaseException
    {
        List<OgTvShow> serverBeanList = getOgTvShowService().getOgTvShows(guids);
        List<OgTvShow> appBeanList = convertServerOgTvShowBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<OgTvShow> getAllOgTvShows() throws BaseException
    {
        return getAllOgTvShows(null, null, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgTvShowService().getAllOgTvShows(ordering, offset, count);
        return getAllOgTvShows(ordering, offset, count, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgTvShow> serverBeanList = getOgTvShowService().getAllOgTvShows(ordering, offset, count, forwardCursor);
        List<OgTvShow> appBeanList = convertServerOgTvShowBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgTvShowService().getAllOgTvShowKeys(ordering, offset, count);
        return getAllOgTvShowKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgTvShowService().getAllOgTvShowKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgTvShowService().findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OgTvShow> serverBeanList = getOgTvShowService().findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<OgTvShow> appBeanList = convertServerOgTvShowBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgTvShowService().findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgTvShowService().findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgTvShowService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgTvShow(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgTvShowService().createOgTvShow(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public String createOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgTvShowBean serverBean =  convertAppOgTvShowBeanToServerBean(ogTvShow);
        return getOgTvShowService().createOgTvShow(serverBean);
    }

    @Override
    public Boolean updateOgTvShow(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgTvShowService().updateOgTvShow(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public Boolean updateOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgTvShowBean serverBean =  convertAppOgTvShowBeanToServerBean(ogTvShow);
        return getOgTvShowService().updateOgTvShow(serverBean);
    }

    @Override
    public Boolean deleteOgTvShow(String guid) throws BaseException
    {
        return getOgTvShowService().deleteOgTvShow(guid);
    }

    @Override
    public Boolean deleteOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        com.pagesynopsis.ws.bean.OgTvShowBean serverBean =  convertAppOgTvShowBeanToServerBean(ogTvShow);
        return getOgTvShowService().deleteOgTvShow(serverBean);
    }

    @Override
    public Long deleteOgTvShows(String filter, String params, List<String> values) throws BaseException
    {
        return getOgTvShowService().deleteOgTvShows(filter, params, values);
    }




    public static OgTvShowBean convertServerOgTvShowBeanToAppBean(OgTvShow serverBean)
    {
        OgTvShowBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new OgTvShowBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUrl(serverBean.getUrl());
            bean.setType(serverBean.getType());
            bean.setSiteName(serverBean.getSiteName());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setFbAdmins(serverBean.getFbAdmins());
            bean.setFbAppId(serverBean.getFbAppId());
            bean.setImage(serverBean.getImage());
            bean.setAudio(serverBean.getAudio());
            bean.setVideo(serverBean.getVideo());
            bean.setLocale(serverBean.getLocale());
            bean.setLocaleAlternate(serverBean.getLocaleAlternate());
            bean.setDirector(serverBean.getDirector());
            bean.setWriter(serverBean.getWriter());
            bean.setActor(serverBean.getActor());
            bean.setDuration(serverBean.getDuration());
            bean.setTag(serverBean.getTag());
            bean.setReleaseDate(serverBean.getReleaseDate());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgTvShow> convertServerOgTvShowBeanListToAppBeanList(List<OgTvShow> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<OgTvShow> beanList = new ArrayList<OgTvShow>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(OgTvShow sb : serverBeanList) {
                OgTvShowBean bean = convertServerOgTvShowBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.OgTvShowBean convertAppOgTvShowBeanToServerBean(OgTvShow appBean)
    {
        com.pagesynopsis.ws.bean.OgTvShowBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgTvShowBean();
            bean.setGuid(appBean.getGuid());
            bean.setUrl(appBean.getUrl());
            bean.setType(appBean.getType());
            bean.setSiteName(appBean.getSiteName());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setFbAdmins(appBean.getFbAdmins());
            bean.setFbAppId(appBean.getFbAppId());
            bean.setImage(appBean.getImage());
            bean.setAudio(appBean.getAudio());
            bean.setVideo(appBean.getVideo());
            bean.setLocale(appBean.getLocale());
            bean.setLocaleAlternate(appBean.getLocaleAlternate());
            bean.setDirector(appBean.getDirector());
            bean.setWriter(appBean.getWriter());
            bean.setActor(appBean.getActor());
            bean.setDuration(appBean.getDuration());
            bean.setTag(appBean.getTag());
            bean.setReleaseDate(appBean.getReleaseDate());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OgTvShow> convertAppOgTvShowBeanListToServerBeanList(List<OgTvShow> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<OgTvShow> beanList = new ArrayList<OgTvShow>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(OgTvShow sb : appBeanList) {
                com.pagesynopsis.ws.bean.OgTvShowBean bean = convertAppOgTvShowBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
