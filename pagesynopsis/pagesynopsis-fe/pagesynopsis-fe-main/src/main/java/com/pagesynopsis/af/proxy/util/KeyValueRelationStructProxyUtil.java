package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.KeyValueRelationStruct;
// import com.pagesynopsis.ws.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;


public class KeyValueRelationStructProxyUtil
{
    private static final Logger log = Logger.getLogger(KeyValueRelationStructProxyUtil.class.getName());

    // Static methods only.
    private KeyValueRelationStructProxyUtil() {}

    public static KeyValueRelationStructBean convertServerKeyValueRelationStructBeanToAppBean(KeyValueRelationStruct serverBean)
    {
        KeyValueRelationStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new KeyValueRelationStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setKey(serverBean.getKey());
            bean.setValue(serverBean.getValue());
            bean.setNote(serverBean.getNote());
            bean.setRelation(serverBean.getRelation());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.KeyValueRelationStructBean convertAppKeyValueRelationStructBeanToServerBean(KeyValueRelationStruct appBean)
    {
        com.pagesynopsis.ws.bean.KeyValueRelationStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.KeyValueRelationStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setKey(appBean.getKey());
            bean.setValue(appBean.getValue());
            bean.setNote(appBean.getNote());
            bean.setRelation(appBean.getRelation());
        }
        return bean;
    }

}
