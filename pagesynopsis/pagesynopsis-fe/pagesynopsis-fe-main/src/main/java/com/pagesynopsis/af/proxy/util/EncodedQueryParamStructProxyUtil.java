package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.EncodedQueryParamStruct;
// import com.pagesynopsis.ws.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;


public class EncodedQueryParamStructProxyUtil
{
    private static final Logger log = Logger.getLogger(EncodedQueryParamStructProxyUtil.class.getName());

    // Static methods only.
    private EncodedQueryParamStructProxyUtil() {}

    public static EncodedQueryParamStructBean convertServerEncodedQueryParamStructBeanToAppBean(EncodedQueryParamStruct serverBean)
    {
        EncodedQueryParamStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new EncodedQueryParamStructBean();
            bean.setParamType(serverBean.getParamType());
            bean.setOriginalString(serverBean.getOriginalString());
            bean.setEncodedString(serverBean.getEncodedString());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.EncodedQueryParamStructBean convertAppEncodedQueryParamStructBeanToServerBean(EncodedQueryParamStruct appBean)
    {
        com.pagesynopsis.ws.bean.EncodedQueryParamStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.EncodedQueryParamStructBean();
            bean.setParamType(appBean.getParamType());
            bean.setOriginalString(appBean.getOriginalString());
            bean.setEncodedString(appBean.getEncodedString());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
