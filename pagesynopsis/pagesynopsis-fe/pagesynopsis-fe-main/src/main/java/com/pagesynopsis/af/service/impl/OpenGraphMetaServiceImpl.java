package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.OpenGraphMetaBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OpenGraphMetaService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OpenGraphMetaServiceImpl implements OpenGraphMetaService
{
    private static final Logger log = Logger.getLogger(OpenGraphMetaServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "OpenGraphMeta-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("OpenGraphMeta:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public OpenGraphMetaServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // OpenGraphMeta related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OpenGraphMeta getOpenGraphMeta(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOpenGraphMeta(): guid = " + guid);

        OpenGraphMetaBean bean = null;
        if(getCache() != null) {
            bean = (OpenGraphMetaBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (OpenGraphMetaBean) getProxyFactory().getOpenGraphMetaServiceProxy().getOpenGraphMeta(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "OpenGraphMetaBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OpenGraphMetaBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOpenGraphMeta(String guid, String field) throws BaseException
    {
        OpenGraphMetaBean bean = null;
        if(getCache() != null) {
            bean = (OpenGraphMetaBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (OpenGraphMetaBean) getProxyFactory().getOpenGraphMetaServiceProxy().getOpenGraphMeta(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "OpenGraphMetaBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OpenGraphMetaBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("fetchRequest")) {
            return bean.getFetchRequest();
        } else if(field.equals("targetUrl")) {
            return bean.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return bean.getPageUrl();
        } else if(field.equals("queryString")) {
            return bean.getQueryString();
        } else if(field.equals("queryParams")) {
            return bean.getQueryParams();
        } else if(field.equals("lastFetchResult")) {
            return bean.getLastFetchResult();
        } else if(field.equals("responseCode")) {
            return bean.getResponseCode();
        } else if(field.equals("contentType")) {
            return bean.getContentType();
        } else if(field.equals("contentLength")) {
            return bean.getContentLength();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("redirect")) {
            return bean.getRedirect();
        } else if(field.equals("location")) {
            return bean.getLocation();
        } else if(field.equals("pageTitle")) {
            return bean.getPageTitle();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("deferred")) {
            return bean.isDeferred();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("refreshStatus")) {
            return bean.getRefreshStatus();
        } else if(field.equals("refreshInterval")) {
            return bean.getRefreshInterval();
        } else if(field.equals("nextRefreshTime")) {
            return bean.getNextRefreshTime();
        } else if(field.equals("lastCheckedTime")) {
            return bean.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return bean.getLastUpdatedTime();
        } else if(field.equals("ogType")) {
            return bean.getOgType();
        } else if(field.equals("ogProfile")) {
            return bean.getOgProfile();
        } else if(field.equals("ogWebsite")) {
            return bean.getOgWebsite();
        } else if(field.equals("ogBlog")) {
            return bean.getOgBlog();
        } else if(field.equals("ogArticle")) {
            return bean.getOgArticle();
        } else if(field.equals("ogBook")) {
            return bean.getOgBook();
        } else if(field.equals("ogVideo")) {
            return bean.getOgVideo();
        } else if(field.equals("ogMovie")) {
            return bean.getOgMovie();
        } else if(field.equals("ogTvShow")) {
            return bean.getOgTvShow();
        } else if(field.equals("ogTvEpisode")) {
            return bean.getOgTvEpisode();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OpenGraphMeta> getOpenGraphMetas(List<String> guids) throws BaseException
    {
        log.fine("getOpenGraphMetas()");

        // TBD: Is there a better way????
        List<OpenGraphMeta> openGraphMetas = getProxyFactory().getOpenGraphMetaServiceProxy().getOpenGraphMetas(guids);
        if(openGraphMetas == null) {
            log.log(Level.WARNING, "Failed to retrieve OpenGraphMetaBean list.");
        }

        log.finer("END");
        return openGraphMetas;
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas() throws BaseException
    {
        return getAllOpenGraphMetas(null, null, null);
    }


    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOpenGraphMetas(ordering, offset, count, null);
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOpenGraphMetas(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OpenGraphMeta> openGraphMetas = getProxyFactory().getOpenGraphMetaServiceProxy().getAllOpenGraphMetas(ordering, offset, count, forwardCursor);
        if(openGraphMetas == null) {
            log.log(Level.WARNING, "Failed to retrieve OpenGraphMetaBean list.");
        }

        log.finer("END");
        return openGraphMetas;
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOpenGraphMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOpenGraphMetaKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getOpenGraphMetaServiceProxy().getAllOpenGraphMetaKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OpenGraphMetaBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty OpenGraphMetaBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOpenGraphMetas(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OpenGraphMetaServiceImpl.findOpenGraphMetas(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OpenGraphMeta> openGraphMetas = getProxyFactory().getOpenGraphMetaServiceProxy().findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(openGraphMetas == null) {
            log.log(Level.WARNING, "Failed to find openGraphMetas for the given criterion.");
        }

        log.finer("END");
        return openGraphMetas;
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OpenGraphMetaServiceImpl.findOpenGraphMetaKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getOpenGraphMetaServiceProxy().findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OpenGraphMeta keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty OpenGraphMeta key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OpenGraphMetaServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getOpenGraphMetaServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOpenGraphMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        OgProfileBean ogProfileBean = null;
        if(ogProfile instanceof OgProfileBean) {
            ogProfileBean = (OgProfileBean) ogProfile;
        } else if(ogProfile instanceof OgProfile) {
            ogProfileBean = new OgProfileBean(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender(), ogProfile.getCreatedTime(), ogProfile.getModifiedTime());
        } else {
            ogProfileBean = null;   // ????
        }
        OgWebsiteBean ogWebsiteBean = null;
        if(ogWebsite instanceof OgWebsiteBean) {
            ogWebsiteBean = (OgWebsiteBean) ogWebsite;
        } else if(ogWebsite instanceof OgWebsite) {
            ogWebsiteBean = new OgWebsiteBean(ogWebsite.getGuid(), ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote(), ogWebsite.getCreatedTime(), ogWebsite.getModifiedTime());
        } else {
            ogWebsiteBean = null;   // ????
        }
        OgBlogBean ogBlogBean = null;
        if(ogBlog instanceof OgBlogBean) {
            ogBlogBean = (OgBlogBean) ogBlog;
        } else if(ogBlog instanceof OgBlog) {
            ogBlogBean = new OgBlogBean(ogBlog.getGuid(), ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote(), ogBlog.getCreatedTime(), ogBlog.getModifiedTime());
        } else {
            ogBlogBean = null;   // ????
        }
        OgArticleBean ogArticleBean = null;
        if(ogArticle instanceof OgArticleBean) {
            ogArticleBean = (OgArticleBean) ogArticle;
        } else if(ogArticle instanceof OgArticle) {
            ogArticleBean = new OgArticleBean(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate(), ogArticle.getCreatedTime(), ogArticle.getModifiedTime());
        } else {
            ogArticleBean = null;   // ????
        }
        OgBookBean ogBookBean = null;
        if(ogBook instanceof OgBookBean) {
            ogBookBean = (OgBookBean) ogBook;
        } else if(ogBook instanceof OgBook) {
            ogBookBean = new OgBookBean(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate(), ogBook.getCreatedTime(), ogBook.getModifiedTime());
        } else {
            ogBookBean = null;   // ????
        }
        OgVideoBean ogVideoBean = null;
        if(ogVideo instanceof OgVideoBean) {
            ogVideoBean = (OgVideoBean) ogVideo;
        } else if(ogVideo instanceof OgVideo) {
            ogVideoBean = new OgVideoBean(ogVideo.getGuid(), ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate(), ogVideo.getCreatedTime(), ogVideo.getModifiedTime());
        } else {
            ogVideoBean = null;   // ????
        }
        OgMovieBean ogMovieBean = null;
        if(ogMovie instanceof OgMovieBean) {
            ogMovieBean = (OgMovieBean) ogMovie;
        } else if(ogMovie instanceof OgMovie) {
            ogMovieBean = new OgMovieBean(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate(), ogMovie.getCreatedTime(), ogMovie.getModifiedTime());
        } else {
            ogMovieBean = null;   // ????
        }
        OgTvShowBean ogTvShowBean = null;
        if(ogTvShow instanceof OgTvShowBean) {
            ogTvShowBean = (OgTvShowBean) ogTvShow;
        } else if(ogTvShow instanceof OgTvShow) {
            ogTvShowBean = new OgTvShowBean(ogTvShow.getGuid(), ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate(), ogTvShow.getCreatedTime(), ogTvShow.getModifiedTime());
        } else {
            ogTvShowBean = null;   // ????
        }
        OgTvEpisodeBean ogTvEpisodeBean = null;
        if(ogTvEpisode instanceof OgTvEpisodeBean) {
            ogTvEpisodeBean = (OgTvEpisodeBean) ogTvEpisode;
        } else if(ogTvEpisode instanceof OgTvEpisode) {
            ogTvEpisodeBean = new OgTvEpisodeBean(ogTvEpisode.getGuid(), ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate(), ogTvEpisode.getCreatedTime(), ogTvEpisode.getModifiedTime());
        } else {
            ogTvEpisodeBean = null;   // ????
        }
        OpenGraphMetaBean bean = new OpenGraphMetaBean(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfileBean, ogWebsiteBean, ogBlogBean, ogArticleBean, ogBookBean, ogVideoBean, ogMovieBean, ogTvShowBean, ogTvEpisodeBean);
        return createOpenGraphMeta(bean);
    }

    @Override
    public String createOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //OpenGraphMeta bean = constructOpenGraphMeta(openGraphMeta);
        //return bean.getGuid();

        // Param openGraphMeta cannot be null.....
        if(openGraphMeta == null) {
            log.log(Level.INFO, "Param openGraphMeta is null!");
            throw new BadRequestException("Param openGraphMeta object is null!");
        }
        OpenGraphMetaBean bean = null;
        if(openGraphMeta instanceof OpenGraphMetaBean) {
            bean = (OpenGraphMetaBean) openGraphMeta;
        } else if(openGraphMeta instanceof OpenGraphMeta) {
            // bean = new OpenGraphMetaBean(null, openGraphMeta.getUser(), openGraphMeta.getFetchRequest(), openGraphMeta.getTargetUrl(), openGraphMeta.getPageUrl(), openGraphMeta.getQueryString(), openGraphMeta.getQueryParams(), openGraphMeta.getLastFetchResult(), openGraphMeta.getResponseCode(), openGraphMeta.getContentType(), openGraphMeta.getContentLength(), openGraphMeta.getLanguage(), openGraphMeta.getRedirect(), openGraphMeta.getLocation(), openGraphMeta.getPageTitle(), openGraphMeta.getNote(), openGraphMeta.isDeferred(), openGraphMeta.getStatus(), openGraphMeta.getRefreshStatus(), openGraphMeta.getRefreshInterval(), openGraphMeta.getNextRefreshTime(), openGraphMeta.getLastCheckedTime(), openGraphMeta.getLastUpdatedTime(), openGraphMeta.getOgType(), (OgProfileBean) openGraphMeta.getOgProfile(), (OgWebsiteBean) openGraphMeta.getOgWebsite(), (OgBlogBean) openGraphMeta.getOgBlog(), (OgArticleBean) openGraphMeta.getOgArticle(), (OgBookBean) openGraphMeta.getOgBook(), (OgVideoBean) openGraphMeta.getOgVideo(), (OgMovieBean) openGraphMeta.getOgMovie(), (OgTvShowBean) openGraphMeta.getOgTvShow(), (OgTvEpisodeBean) openGraphMeta.getOgTvEpisode());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new OpenGraphMetaBean(openGraphMeta.getGuid(), openGraphMeta.getUser(), openGraphMeta.getFetchRequest(), openGraphMeta.getTargetUrl(), openGraphMeta.getPageUrl(), openGraphMeta.getQueryString(), openGraphMeta.getQueryParams(), openGraphMeta.getLastFetchResult(), openGraphMeta.getResponseCode(), openGraphMeta.getContentType(), openGraphMeta.getContentLength(), openGraphMeta.getLanguage(), openGraphMeta.getRedirect(), openGraphMeta.getLocation(), openGraphMeta.getPageTitle(), openGraphMeta.getNote(), openGraphMeta.isDeferred(), openGraphMeta.getStatus(), openGraphMeta.getRefreshStatus(), openGraphMeta.getRefreshInterval(), openGraphMeta.getNextRefreshTime(), openGraphMeta.getLastCheckedTime(), openGraphMeta.getLastUpdatedTime(), openGraphMeta.getOgType(), (OgProfileBean) openGraphMeta.getOgProfile(), (OgWebsiteBean) openGraphMeta.getOgWebsite(), (OgBlogBean) openGraphMeta.getOgBlog(), (OgArticleBean) openGraphMeta.getOgArticle(), (OgBookBean) openGraphMeta.getOgBook(), (OgVideoBean) openGraphMeta.getOgVideo(), (OgMovieBean) openGraphMeta.getOgMovie(), (OgTvShowBean) openGraphMeta.getOgTvShow(), (OgTvEpisodeBean) openGraphMeta.getOgTvEpisode());
        } else {
            log.log(Level.WARNING, "createOpenGraphMeta(): Arg openGraphMeta is of an unknown type.");
            //bean = new OpenGraphMetaBean();
            bean = new OpenGraphMetaBean(openGraphMeta.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getOpenGraphMetaServiceProxy().createOpenGraphMeta(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public OpenGraphMeta constructOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param openGraphMeta cannot be null.....
        if(openGraphMeta == null) {
            log.log(Level.INFO, "Param openGraphMeta is null!");
            throw new BadRequestException("Param openGraphMeta object is null!");
        }
        OpenGraphMetaBean bean = null;
        if(openGraphMeta instanceof OpenGraphMetaBean) {
            bean = (OpenGraphMetaBean) openGraphMeta;
        } else if(openGraphMeta instanceof OpenGraphMeta) {
            // bean = new OpenGraphMetaBean(null, openGraphMeta.getUser(), openGraphMeta.getFetchRequest(), openGraphMeta.getTargetUrl(), openGraphMeta.getPageUrl(), openGraphMeta.getQueryString(), openGraphMeta.getQueryParams(), openGraphMeta.getLastFetchResult(), openGraphMeta.getResponseCode(), openGraphMeta.getContentType(), openGraphMeta.getContentLength(), openGraphMeta.getLanguage(), openGraphMeta.getRedirect(), openGraphMeta.getLocation(), openGraphMeta.getPageTitle(), openGraphMeta.getNote(), openGraphMeta.isDeferred(), openGraphMeta.getStatus(), openGraphMeta.getRefreshStatus(), openGraphMeta.getRefreshInterval(), openGraphMeta.getNextRefreshTime(), openGraphMeta.getLastCheckedTime(), openGraphMeta.getLastUpdatedTime(), openGraphMeta.getOgType(), (OgProfileBean) openGraphMeta.getOgProfile(), (OgWebsiteBean) openGraphMeta.getOgWebsite(), (OgBlogBean) openGraphMeta.getOgBlog(), (OgArticleBean) openGraphMeta.getOgArticle(), (OgBookBean) openGraphMeta.getOgBook(), (OgVideoBean) openGraphMeta.getOgVideo(), (OgMovieBean) openGraphMeta.getOgMovie(), (OgTvShowBean) openGraphMeta.getOgTvShow(), (OgTvEpisodeBean) openGraphMeta.getOgTvEpisode());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new OpenGraphMetaBean(openGraphMeta.getGuid(), openGraphMeta.getUser(), openGraphMeta.getFetchRequest(), openGraphMeta.getTargetUrl(), openGraphMeta.getPageUrl(), openGraphMeta.getQueryString(), openGraphMeta.getQueryParams(), openGraphMeta.getLastFetchResult(), openGraphMeta.getResponseCode(), openGraphMeta.getContentType(), openGraphMeta.getContentLength(), openGraphMeta.getLanguage(), openGraphMeta.getRedirect(), openGraphMeta.getLocation(), openGraphMeta.getPageTitle(), openGraphMeta.getNote(), openGraphMeta.isDeferred(), openGraphMeta.getStatus(), openGraphMeta.getRefreshStatus(), openGraphMeta.getRefreshInterval(), openGraphMeta.getNextRefreshTime(), openGraphMeta.getLastCheckedTime(), openGraphMeta.getLastUpdatedTime(), openGraphMeta.getOgType(), (OgProfileBean) openGraphMeta.getOgProfile(), (OgWebsiteBean) openGraphMeta.getOgWebsite(), (OgBlogBean) openGraphMeta.getOgBlog(), (OgArticleBean) openGraphMeta.getOgArticle(), (OgBookBean) openGraphMeta.getOgBook(), (OgVideoBean) openGraphMeta.getOgVideo(), (OgMovieBean) openGraphMeta.getOgMovie(), (OgTvShowBean) openGraphMeta.getOgTvShow(), (OgTvEpisodeBean) openGraphMeta.getOgTvEpisode());
        } else {
            log.log(Level.WARNING, "createOpenGraphMeta(): Arg openGraphMeta is of an unknown type.");
            //bean = new OpenGraphMetaBean();
            bean = new OpenGraphMetaBean(openGraphMeta.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getOpenGraphMetaServiceProxy().createOpenGraphMeta(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateOpenGraphMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgProfileBean ogProfileBean = null;
        if(ogProfile instanceof OgProfileBean) {
            ogProfileBean = (OgProfileBean) ogProfile;
        } else if(ogProfile instanceof OgProfile) {
            ogProfileBean = new OgProfileBean(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender(), ogProfile.getCreatedTime(), ogProfile.getModifiedTime());
        } else {
            ogProfileBean = null;   // ????
        }
        OgWebsiteBean ogWebsiteBean = null;
        if(ogWebsite instanceof OgWebsiteBean) {
            ogWebsiteBean = (OgWebsiteBean) ogWebsite;
        } else if(ogWebsite instanceof OgWebsite) {
            ogWebsiteBean = new OgWebsiteBean(ogWebsite.getGuid(), ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote(), ogWebsite.getCreatedTime(), ogWebsite.getModifiedTime());
        } else {
            ogWebsiteBean = null;   // ????
        }
        OgBlogBean ogBlogBean = null;
        if(ogBlog instanceof OgBlogBean) {
            ogBlogBean = (OgBlogBean) ogBlog;
        } else if(ogBlog instanceof OgBlog) {
            ogBlogBean = new OgBlogBean(ogBlog.getGuid(), ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote(), ogBlog.getCreatedTime(), ogBlog.getModifiedTime());
        } else {
            ogBlogBean = null;   // ????
        }
        OgArticleBean ogArticleBean = null;
        if(ogArticle instanceof OgArticleBean) {
            ogArticleBean = (OgArticleBean) ogArticle;
        } else if(ogArticle instanceof OgArticle) {
            ogArticleBean = new OgArticleBean(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate(), ogArticle.getCreatedTime(), ogArticle.getModifiedTime());
        } else {
            ogArticleBean = null;   // ????
        }
        OgBookBean ogBookBean = null;
        if(ogBook instanceof OgBookBean) {
            ogBookBean = (OgBookBean) ogBook;
        } else if(ogBook instanceof OgBook) {
            ogBookBean = new OgBookBean(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate(), ogBook.getCreatedTime(), ogBook.getModifiedTime());
        } else {
            ogBookBean = null;   // ????
        }
        OgVideoBean ogVideoBean = null;
        if(ogVideo instanceof OgVideoBean) {
            ogVideoBean = (OgVideoBean) ogVideo;
        } else if(ogVideo instanceof OgVideo) {
            ogVideoBean = new OgVideoBean(ogVideo.getGuid(), ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate(), ogVideo.getCreatedTime(), ogVideo.getModifiedTime());
        } else {
            ogVideoBean = null;   // ????
        }
        OgMovieBean ogMovieBean = null;
        if(ogMovie instanceof OgMovieBean) {
            ogMovieBean = (OgMovieBean) ogMovie;
        } else if(ogMovie instanceof OgMovie) {
            ogMovieBean = new OgMovieBean(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate(), ogMovie.getCreatedTime(), ogMovie.getModifiedTime());
        } else {
            ogMovieBean = null;   // ????
        }
        OgTvShowBean ogTvShowBean = null;
        if(ogTvShow instanceof OgTvShowBean) {
            ogTvShowBean = (OgTvShowBean) ogTvShow;
        } else if(ogTvShow instanceof OgTvShow) {
            ogTvShowBean = new OgTvShowBean(ogTvShow.getGuid(), ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate(), ogTvShow.getCreatedTime(), ogTvShow.getModifiedTime());
        } else {
            ogTvShowBean = null;   // ????
        }
        OgTvEpisodeBean ogTvEpisodeBean = null;
        if(ogTvEpisode instanceof OgTvEpisodeBean) {
            ogTvEpisodeBean = (OgTvEpisodeBean) ogTvEpisode;
        } else if(ogTvEpisode instanceof OgTvEpisode) {
            ogTvEpisodeBean = new OgTvEpisodeBean(ogTvEpisode.getGuid(), ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate(), ogTvEpisode.getCreatedTime(), ogTvEpisode.getModifiedTime());
        } else {
            ogTvEpisodeBean = null;   // ????
        }
        OpenGraphMetaBean bean = new OpenGraphMetaBean(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfileBean, ogWebsiteBean, ogBlogBean, ogArticleBean, ogBookBean, ogVideoBean, ogMovieBean, ogTvShowBean, ogTvEpisodeBean);
        return updateOpenGraphMeta(bean);
    }
        
    @Override
    public Boolean updateOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //OpenGraphMeta bean = refreshOpenGraphMeta(openGraphMeta);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param openGraphMeta cannot be null.....
        if(openGraphMeta == null || openGraphMeta.getGuid() == null) {
            log.log(Level.INFO, "Param openGraphMeta or its guid is null!");
            throw new BadRequestException("Param openGraphMeta object or its guid is null!");
        }
        OpenGraphMetaBean bean = null;
        if(openGraphMeta instanceof OpenGraphMetaBean) {
            bean = (OpenGraphMetaBean) openGraphMeta;
        } else {  // if(openGraphMeta instanceof OpenGraphMeta)
            bean = new OpenGraphMetaBean(openGraphMeta.getGuid(), openGraphMeta.getUser(), openGraphMeta.getFetchRequest(), openGraphMeta.getTargetUrl(), openGraphMeta.getPageUrl(), openGraphMeta.getQueryString(), openGraphMeta.getQueryParams(), openGraphMeta.getLastFetchResult(), openGraphMeta.getResponseCode(), openGraphMeta.getContentType(), openGraphMeta.getContentLength(), openGraphMeta.getLanguage(), openGraphMeta.getRedirect(), openGraphMeta.getLocation(), openGraphMeta.getPageTitle(), openGraphMeta.getNote(), openGraphMeta.isDeferred(), openGraphMeta.getStatus(), openGraphMeta.getRefreshStatus(), openGraphMeta.getRefreshInterval(), openGraphMeta.getNextRefreshTime(), openGraphMeta.getLastCheckedTime(), openGraphMeta.getLastUpdatedTime(), openGraphMeta.getOgType(), (OgProfileBean) openGraphMeta.getOgProfile(), (OgWebsiteBean) openGraphMeta.getOgWebsite(), (OgBlogBean) openGraphMeta.getOgBlog(), (OgArticleBean) openGraphMeta.getOgArticle(), (OgBookBean) openGraphMeta.getOgBook(), (OgVideoBean) openGraphMeta.getOgVideo(), (OgMovieBean) openGraphMeta.getOgMovie(), (OgTvShowBean) openGraphMeta.getOgTvShow(), (OgTvEpisodeBean) openGraphMeta.getOgTvEpisode());
        }
        Boolean suc = getProxyFactory().getOpenGraphMetaServiceProxy().updateOpenGraphMeta(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public OpenGraphMeta refreshOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param openGraphMeta cannot be null.....
        if(openGraphMeta == null || openGraphMeta.getGuid() == null) {
            log.log(Level.INFO, "Param openGraphMeta or its guid is null!");
            throw new BadRequestException("Param openGraphMeta object or its guid is null!");
        }
        OpenGraphMetaBean bean = null;
        if(openGraphMeta instanceof OpenGraphMetaBean) {
            bean = (OpenGraphMetaBean) openGraphMeta;
        } else {  // if(openGraphMeta instanceof OpenGraphMeta)
            bean = new OpenGraphMetaBean(openGraphMeta.getGuid(), openGraphMeta.getUser(), openGraphMeta.getFetchRequest(), openGraphMeta.getTargetUrl(), openGraphMeta.getPageUrl(), openGraphMeta.getQueryString(), openGraphMeta.getQueryParams(), openGraphMeta.getLastFetchResult(), openGraphMeta.getResponseCode(), openGraphMeta.getContentType(), openGraphMeta.getContentLength(), openGraphMeta.getLanguage(), openGraphMeta.getRedirect(), openGraphMeta.getLocation(), openGraphMeta.getPageTitle(), openGraphMeta.getNote(), openGraphMeta.isDeferred(), openGraphMeta.getStatus(), openGraphMeta.getRefreshStatus(), openGraphMeta.getRefreshInterval(), openGraphMeta.getNextRefreshTime(), openGraphMeta.getLastCheckedTime(), openGraphMeta.getLastUpdatedTime(), openGraphMeta.getOgType(), (OgProfileBean) openGraphMeta.getOgProfile(), (OgWebsiteBean) openGraphMeta.getOgWebsite(), (OgBlogBean) openGraphMeta.getOgBlog(), (OgArticleBean) openGraphMeta.getOgArticle(), (OgBookBean) openGraphMeta.getOgBook(), (OgVideoBean) openGraphMeta.getOgVideo(), (OgMovieBean) openGraphMeta.getOgMovie(), (OgTvShowBean) openGraphMeta.getOgTvShow(), (OgTvEpisodeBean) openGraphMeta.getOgTvEpisode());
        }
        Boolean suc = getProxyFactory().getOpenGraphMetaServiceProxy().updateOpenGraphMeta(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteOpenGraphMeta(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getOpenGraphMetaServiceProxy().deleteOpenGraphMeta(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            OpenGraphMeta openGraphMeta = null;
            try {
                openGraphMeta = getProxyFactory().getOpenGraphMetaServiceProxy().getOpenGraphMeta(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch openGraphMeta with a key, " + guid);
                return false;
            }
            if(openGraphMeta != null) {
                String beanGuid = openGraphMeta.getGuid();
                Boolean suc1 = getProxyFactory().getOpenGraphMetaServiceProxy().deleteOpenGraphMeta(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("openGraphMeta with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param openGraphMeta cannot be null.....
        if(openGraphMeta == null || openGraphMeta.getGuid() == null) {
            log.log(Level.INFO, "Param openGraphMeta or its guid is null!");
            throw new BadRequestException("Param openGraphMeta object or its guid is null!");
        }
        OpenGraphMetaBean bean = null;
        if(openGraphMeta instanceof OpenGraphMetaBean) {
            bean = (OpenGraphMetaBean) openGraphMeta;
        } else {  // if(openGraphMeta instanceof OpenGraphMeta)
            // ????
            log.warning("openGraphMeta is not an instance of OpenGraphMetaBean.");
            bean = new OpenGraphMetaBean(openGraphMeta.getGuid(), openGraphMeta.getUser(), openGraphMeta.getFetchRequest(), openGraphMeta.getTargetUrl(), openGraphMeta.getPageUrl(), openGraphMeta.getQueryString(), openGraphMeta.getQueryParams(), openGraphMeta.getLastFetchResult(), openGraphMeta.getResponseCode(), openGraphMeta.getContentType(), openGraphMeta.getContentLength(), openGraphMeta.getLanguage(), openGraphMeta.getRedirect(), openGraphMeta.getLocation(), openGraphMeta.getPageTitle(), openGraphMeta.getNote(), openGraphMeta.isDeferred(), openGraphMeta.getStatus(), openGraphMeta.getRefreshStatus(), openGraphMeta.getRefreshInterval(), openGraphMeta.getNextRefreshTime(), openGraphMeta.getLastCheckedTime(), openGraphMeta.getLastUpdatedTime(), openGraphMeta.getOgType(), (OgProfileBean) openGraphMeta.getOgProfile(), (OgWebsiteBean) openGraphMeta.getOgWebsite(), (OgBlogBean) openGraphMeta.getOgBlog(), (OgArticleBean) openGraphMeta.getOgArticle(), (OgBookBean) openGraphMeta.getOgBook(), (OgVideoBean) openGraphMeta.getOgVideo(), (OgMovieBean) openGraphMeta.getOgMovie(), (OgTvShowBean) openGraphMeta.getOgTvShow(), (OgTvEpisodeBean) openGraphMeta.getOgTvEpisode());
        }
        Boolean suc = getProxyFactory().getOpenGraphMetaServiceProxy().deleteOpenGraphMeta(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOpenGraphMetas(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getOpenGraphMetaServiceProxy().deleteOpenGraphMetas(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOpenGraphMetas(List<OpenGraphMeta> openGraphMetas) throws BaseException
    {
        log.finer("BEGIN");

        if(openGraphMetas == null) {
            log.log(Level.WARNING, "createOpenGraphMetas() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = openGraphMetas.size();
        if(size == 0) {
            log.log(Level.WARNING, "createOpenGraphMetas() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(OpenGraphMeta openGraphMeta : openGraphMetas) {
            String guid = createOpenGraphMeta(openGraphMeta);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createOpenGraphMetas() failed for at least one openGraphMeta. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateOpenGraphMetas(List<OpenGraphMeta> openGraphMetas) throws BaseException
    //{
    //}

}
