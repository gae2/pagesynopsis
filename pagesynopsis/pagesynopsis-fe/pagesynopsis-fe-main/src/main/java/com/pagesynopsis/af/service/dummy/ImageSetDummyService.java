package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.ImageSetService;


// The primary purpose of ImageSetDummyService is to fake the service api, ImageSetService.
// It has no real implementation.
public class ImageSetDummyService implements ImageSetService
{
    private static final Logger log = Logger.getLogger(ImageSetDummyService.class.getName());

    public ImageSetDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // ImageSet related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ImageSet getImageSet(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getImageSet(): guid = " + guid);
        return null;
    }

    @Override
    public Object getImageSet(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getImageSet(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<ImageSet> getImageSets(List<String> guids) throws BaseException
    {
        log.fine("getImageSets()");
        return null;
    }

    @Override
    public List<ImageSet> getAllImageSets() throws BaseException
    {
        return getAllImageSets(null, null, null);
    }


    @Override
    public List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllImageSets(ordering, offset, count, null);
    }

    @Override
    public List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllImageSets(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllImageSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllImageSetKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findImageSets(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findImageSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ImageSetDummyService.findImageSets(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ImageSetDummyService.findImageSetKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ImageSetDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createImageSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException
    {
        log.finer("createImageSet()");
        return null;
    }

    @Override
    public String createImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("createImageSet()");
        return null;
    }

    @Override
    public ImageSet constructImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("constructImageSet()");
        return null;
    }

    @Override
    public Boolean updateImageSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException
    {
        log.finer("updateImageSet()");
        return null;
    }
        
    @Override
    public Boolean updateImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("updateImageSet()");
        return null;
    }

    @Override
    public ImageSet refreshImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("refreshImageSet()");
        return null;
    }

    @Override
    public Boolean deleteImageSet(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteImageSet(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("deleteImageSet()");
        return null;
    }

    // TBD
    @Override
    public Long deleteImageSets(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteImageSet(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createImageSets(List<ImageSet> imageSets) throws BaseException
    {
        log.finer("createImageSets()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateImageSets(List<ImageSet> imageSets) throws BaseException
    //{
    //}

}
