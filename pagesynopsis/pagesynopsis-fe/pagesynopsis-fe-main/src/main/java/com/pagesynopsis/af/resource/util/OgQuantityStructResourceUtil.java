package com.pagesynopsis.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgQuantityStruct;
import com.pagesynopsis.ws.stub.OgQuantityStructStub;
import com.pagesynopsis.af.bean.OgQuantityStructBean;


public class OgQuantityStructResourceUtil
{
    private static final Logger log = Logger.getLogger(OgQuantityStructResourceUtil.class.getName());

    // Static methods only.
    private OgQuantityStructResourceUtil() {}

    public static OgQuantityStructBean convertOgQuantityStructStubToBean(OgQuantityStruct stub)
    {
        OgQuantityStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new OgQuantityStructBean();
            bean.setUuid(stub.getUuid());
            bean.setValue(stub.getValue());
            bean.setUnits(stub.getUnits());
        }
        return bean;
    }

}
