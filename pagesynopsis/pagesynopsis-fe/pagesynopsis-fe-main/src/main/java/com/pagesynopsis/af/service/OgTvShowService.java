package com.pagesynopsis.af.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface OgTvShowService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    OgTvShow getOgTvShow(String guid) throws BaseException;
    Object getOgTvShow(String guid, String field) throws BaseException;
    List<OgTvShow> getOgTvShows(List<String> guids) throws BaseException;
    List<OgTvShow> getAllOgTvShows() throws BaseException;
    /* @Deprecated */ List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count) throws BaseException;
    List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createOgTvShow(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException;
    //String createOgTvShow(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgTvShow?)
    String createOgTvShow(OgTvShow ogTvShow) throws BaseException;
    OgTvShow constructOgTvShow(OgTvShow ogTvShow) throws BaseException;
    Boolean updateOgTvShow(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException;
    //Boolean updateOgTvShow(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgTvShow(OgTvShow ogTvShow) throws BaseException;
    OgTvShow refreshOgTvShow(OgTvShow ogTvShow) throws BaseException;
    Boolean deleteOgTvShow(String guid) throws BaseException;
    Boolean deleteOgTvShow(OgTvShow ogTvShow) throws BaseException;
    Long deleteOgTvShows(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createOgTvShows(List<OgTvShow> ogTvShows) throws BaseException;
//    Boolean updateOgTvShows(List<OgTvShow> ogTvShows) throws BaseException;

}
