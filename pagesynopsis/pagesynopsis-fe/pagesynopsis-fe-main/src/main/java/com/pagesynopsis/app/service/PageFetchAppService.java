package com.pagesynopsis.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.PageFetchBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.af.service.impl.PageFetchServiceImpl;
import com.pagesynopsis.app.fetch.FetchProcessor;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.PageFetch;


// Updated.
public class PageFetchAppService extends PageFetchServiceImpl implements PageFetchService
{
    private static final Logger log = Logger.getLogger(PageFetchAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public PageFetchAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // PageFetch related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public PageFetch getPageFetch(String guid) throws BaseException
    {
        return super.getPageFetch(guid);
    }

    @Override
    public Object getPageFetch(String guid, String field) throws BaseException
    {
        return super.getPageFetch(guid, field);
    }

    @Override
    public List<PageFetch> getPageFetches(List<String> guids) throws BaseException
    {
        return super.getPageFetches(guids);
    }

    @Override
    public List<PageFetch> getAllPageFetches() throws BaseException
    {
        return super.getAllPageFetches();
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllPageFetchKeys(ordering, offset, count);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findPageFetches(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createPageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("TOP: createPageFetch()");

        // TBD:
        pageFetch = validatePageFetch(pageFetch);
        pageFetch = enhancePageFetch(pageFetch);
        // ...
        
        // ????
        Integer fefreshStatus = ((PageFetchBean) pageFetch).getRefreshStatus();
        if(fefreshStatus == null) {
            ((PageFetchBean) pageFetch).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);  // ???
        }
        // ...

        // Note:
        // POST/PUT should not call processPageFetch().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = pageFetch.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                pageFetch = processPageFetch(pageFetch);
                if(pageFetch != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: pageFetch = " + pageFetch);
                    // Need to to do this to avoid double processing.
                    ((PageFetchBean) pageFetch).setDeferred(true);
                    // ((PageFetchBean) pageFetch).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((PageFetchBean) pageFetch).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...


        // Save it.
        String guid = super.createPageFetch(pageFetch);

        log.finer("BOTTOM: createPageFetch(): guid = " + guid);
        return guid;
    }

    @Override
    public PageFetch constructPageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("TOP: constructPageFetch()");

        // TBD:
        pageFetch = validatePageFetch(pageFetch);
        pageFetch = enhancePageFetch(pageFetch);
        // ...
        
        // ????
        Integer fefreshStatus = ((PageFetchBean) pageFetch).getRefreshStatus();
        if(fefreshStatus == null) {
            ((PageFetchBean) pageFetch).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);   // ????
        }
        // ...
        
        
        // Note:
        // POST/PUT should not call processPageFetch().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = pageFetch.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                pageFetch = processPageFetch(pageFetch);
                if(pageFetch != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: pageFetch = " + pageFetch);
                    // Need to to do this to avoid double processing.
                    ((PageFetchBean) pageFetch).setDeferred(true);
                    // ((PageFetchBean) pageFetch).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((PageFetchBean) pageFetch).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...


        // Update it.
        pageFetch = super.constructPageFetch(pageFetch);
        
        log.finer("BOTTOM: constructPageFetch(): pageFetch = " + pageFetch);
        return pageFetch;
    }


    @Override
    public Boolean updatePageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("TOP: updatePageFetch()");

        // TBD:
        pageFetch = validatePageFetch(pageFetch);
        Boolean suc = super.updatePageFetch(pageFetch);

        log.finer("BOTTOM: createPageFetch(): suc = " + suc);
        return suc;
    }
        
    @Override
    public PageFetch refreshPageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("TOP: refreshPageFetch()");

        // TBD:
        pageFetch = validatePageFetch(pageFetch);
        pageFetch = super.refreshPageFetch(pageFetch);
        
        log.finer("BOTTOM: refreshPageFetch(): pageFetch = " + pageFetch);
        return pageFetch;
    }

    
    @Override
    public Boolean deletePageFetch(String guid) throws BaseException
    {
        return super.deletePageFetch(guid);
    }

    @Override
    public Boolean deletePageFetch(PageFetch pageFetch) throws BaseException
    {
        return super.deletePageFetch(pageFetch);
    }

    @Override
    public Integer createPageFetches(List<PageFetch> pageFetches) throws BaseException
    {
        return super.createPageFetches(pageFetches);
    }

    // TBD
    //@Override
    //public Boolean updatePageFetches(List<PageFetch> pageFetches) throws BaseException
    //{
    //}

    
    
    // TBD...
    private PageFetchBean validatePageFetch(PageFetch pageFetch) throws BaseException
    {
        PageFetchBean bean = null;
        if(pageFetch instanceof PageFetchBean) {
            bean = (PageFetchBean) pageFetch;
        }
        // else ???
        
        // ...
        String targetUrl =  pageFetch.getTargetUrl();
        if(targetUrl == null || targetUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BaseException("Target Url is null or empty. Cannot proceed with fetch.");
        } else if(targetUrl.length() > 500) {
            // ??? Truncate or error?
            // TBD ....
            throw new BaseException("Target Url is too long. Cannot proceed with fetch. targetUrl = " + targetUrl);            
        }
        
        // TBD:
        // If we truncate targetUrl...
        // We need to check pageUrl as well....
        // ...         
        
        
        
        // TBD: 
        String pageUrl = pageFetch.getPageUrl();
        String query = pageFetch.getQueryString();
        List<KeyValuePairStruct> queryParams = pageFetch.getQueryParams();
        // if either of these is null/empty
        // populate them based on targetUrl...
        // --> This should have done earlier (closer to front end) ???
        // ...
        
        
//        // Next, check input...
//        List<String> input = pageFetch.getInput();
//        if(input == null || input.isEmpty()) {
//            // ???  --> Note that the request will not even be saved ....
//            throw new BaseException("Input is null or empty. Cannot proceed with fetch.");
//        }

        
        
        // TBD:
        // Minimum value for refresh interval???
        // etc....
        
        // ...

        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private PageFetch enhancePageFetch(PageFetch pageFetch) throws BaseException
    {        
        // TBD:
        // Create shortlink, etc...
        // ....
        return pageFetch;
    }        

    private PageFetch processPageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("TOP: processPageFetch()");
        // ...
        pageFetch = FetchProcessor.getInstance().processPageFetchFetch(pageFetch);
        // ...   

        log.finer("BOTTOM: processPageFetch()");
        return pageFetch;
    }


    
    
}
