package com.pagesynopsis.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.service.TwitterCardMetaAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.TwitterCardMeta;


public class TwitterCardMetaHelper
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaHelper.class.getName());

    private TwitterCardMetaHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private TwitterCardMetaAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private TwitterCardMetaAppService getTwitterCardMetaService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new TwitterCardMetaAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class TwitterCardMetaHelperHolder
    {
        private static final TwitterCardMetaHelper INSTANCE = new TwitterCardMetaHelper();
    }

    // Singleton method
    public static TwitterCardMetaHelper getInstance()
    {
        return TwitterCardMetaHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public TwitterCardMeta getTwitterCardMeta(String guid) 
    {
        TwitterCardMeta message = null;
        try {
            message = getTwitterCardMetaService().getTwitterCardMeta(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
    
    // TBD:
    // Check the pageTitle field as well....  ????
    // ...
    

    public List<String> getTwitterCardMetaKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getTwitterCardMetaService().findTwitterCardMetaKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    public List<TwitterCardMeta> getTwitterCardMetasForRefreshProcessing(Integer maxCount)
    {
        List<TwitterCardMeta> twitterCardMetas = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            twitterCardMetas = getTwitterCardMetaService().findTwitterCardMetas(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return twitterCardMetas;
    }


}
