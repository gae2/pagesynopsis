package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.TwitterCardMeta;
// import com.pagesynopsis.ws.bean.TwitterCardMetaBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.TwitterCardMetaService;
import com.pagesynopsis.af.bean.TwitterCardMetaBean;
import com.pagesynopsis.af.proxy.TwitterCardMetaServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalTwitterSummaryCardServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalTwitterPhotoCardServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalTwitterGalleryCardServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalTwitterAppCardServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalTwitterPlayerCardServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalTwitterProductCardServiceProxy;


public class LocalTwitterCardMetaServiceProxy extends BaseLocalServiceProxy implements TwitterCardMetaServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterCardMetaServiceProxy.class.getName());

    public LocalTwitterCardMetaServiceProxy()
    {
    }

    @Override
    public TwitterCardMeta getTwitterCardMeta(String guid) throws BaseException
    {
        TwitterCardMeta serverBean = getTwitterCardMetaService().getTwitterCardMeta(guid);
        TwitterCardMeta appBean = convertServerTwitterCardMetaBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getTwitterCardMeta(String guid, String field) throws BaseException
    {
        return getTwitterCardMetaService().getTwitterCardMeta(guid, field);       
    }

    @Override
    public List<TwitterCardMeta> getTwitterCardMetas(List<String> guids) throws BaseException
    {
        List<TwitterCardMeta> serverBeanList = getTwitterCardMetaService().getTwitterCardMetas(guids);
        List<TwitterCardMeta> appBeanList = convertServerTwitterCardMetaBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas() throws BaseException
    {
        return getAllTwitterCardMetas(null, null, null);
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterCardMetaService().getAllTwitterCardMetas(ordering, offset, count);
        return getAllTwitterCardMetas(ordering, offset, count, null);
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterCardMeta> serverBeanList = getTwitterCardMetaService().getAllTwitterCardMetas(ordering, offset, count, forwardCursor);
        List<TwitterCardMeta> appBeanList = convertServerTwitterCardMetaBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterCardMetaService().getAllTwitterCardMetaKeys(ordering, offset, count);
        return getAllTwitterCardMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterCardMetaService().getAllTwitterCardMetaKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterCardMetas(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterCardMetaService().findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterCardMeta> serverBeanList = getTwitterCardMetaService().findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<TwitterCardMeta> appBeanList = convertServerTwitterCardMetaBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterCardMetaService().findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterCardMetaService().findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterCardMetaService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterCardMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException
    {
        return getTwitterCardMetaService().createTwitterCardMeta(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, summaryCard, photoCard, galleryCard, appCard, playerCard, productCard);
    }

    @Override
    public String createTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterCardMetaBean serverBean =  convertAppTwitterCardMetaBeanToServerBean(twitterCardMeta);
        return getTwitterCardMetaService().createTwitterCardMeta(serverBean);
    }

    @Override
    public Boolean updateTwitterCardMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException
    {
        return getTwitterCardMetaService().updateTwitterCardMeta(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, summaryCard, photoCard, galleryCard, appCard, playerCard, productCard);
    }

    @Override
    public Boolean updateTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterCardMetaBean serverBean =  convertAppTwitterCardMetaBeanToServerBean(twitterCardMeta);
        return getTwitterCardMetaService().updateTwitterCardMeta(serverBean);
    }

    @Override
    public Boolean deleteTwitterCardMeta(String guid) throws BaseException
    {
        return getTwitterCardMetaService().deleteTwitterCardMeta(guid);
    }

    @Override
    public Boolean deleteTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterCardMetaBean serverBean =  convertAppTwitterCardMetaBeanToServerBean(twitterCardMeta);
        return getTwitterCardMetaService().deleteTwitterCardMeta(serverBean);
    }

    @Override
    public Long deleteTwitterCardMetas(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterCardMetaService().deleteTwitterCardMetas(filter, params, values);
    }




    public static TwitterCardMetaBean convertServerTwitterCardMetaBeanToAppBean(TwitterCardMeta serverBean)
    {
        TwitterCardMetaBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new TwitterCardMetaBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setFetchRequest(serverBean.getFetchRequest());
            bean.setTargetUrl(serverBean.getTargetUrl());
            bean.setPageUrl(serverBean.getPageUrl());
            bean.setQueryString(serverBean.getQueryString());
            bean.setQueryParams(serverBean.getQueryParams());
            bean.setLastFetchResult(serverBean.getLastFetchResult());
            bean.setResponseCode(serverBean.getResponseCode());
            bean.setContentType(serverBean.getContentType());
            bean.setContentLength(serverBean.getContentLength());
            bean.setLanguage(serverBean.getLanguage());
            bean.setRedirect(serverBean.getRedirect());
            bean.setLocation(serverBean.getLocation());
            bean.setPageTitle(serverBean.getPageTitle());
            bean.setNote(serverBean.getNote());
            bean.setDeferred(serverBean.isDeferred());
            bean.setStatus(serverBean.getStatus());
            bean.setRefreshStatus(serverBean.getRefreshStatus());
            bean.setRefreshInterval(serverBean.getRefreshInterval());
            bean.setNextRefreshTime(serverBean.getNextRefreshTime());
            bean.setLastCheckedTime(serverBean.getLastCheckedTime());
            bean.setLastUpdatedTime(serverBean.getLastUpdatedTime());
            bean.setCardType(serverBean.getCardType());
            bean.setSummaryCard(LocalTwitterSummaryCardServiceProxy.convertServerTwitterSummaryCardBeanToAppBean(serverBean.getSummaryCard()));
            bean.setPhotoCard(LocalTwitterPhotoCardServiceProxy.convertServerTwitterPhotoCardBeanToAppBean(serverBean.getPhotoCard()));
            bean.setGalleryCard(LocalTwitterGalleryCardServiceProxy.convertServerTwitterGalleryCardBeanToAppBean(serverBean.getGalleryCard()));
            bean.setAppCard(LocalTwitterAppCardServiceProxy.convertServerTwitterAppCardBeanToAppBean(serverBean.getAppCard()));
            bean.setPlayerCard(LocalTwitterPlayerCardServiceProxy.convertServerTwitterPlayerCardBeanToAppBean(serverBean.getPlayerCard()));
            bean.setProductCard(LocalTwitterProductCardServiceProxy.convertServerTwitterProductCardBeanToAppBean(serverBean.getProductCard()));
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterCardMeta> convertServerTwitterCardMetaBeanListToAppBeanList(List<TwitterCardMeta> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterCardMeta> beanList = new ArrayList<TwitterCardMeta>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(TwitterCardMeta sb : serverBeanList) {
                TwitterCardMetaBean bean = convertServerTwitterCardMetaBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.TwitterCardMetaBean convertAppTwitterCardMetaBeanToServerBean(TwitterCardMeta appBean)
    {
        com.pagesynopsis.ws.bean.TwitterCardMetaBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.TwitterCardMetaBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setFetchRequest(appBean.getFetchRequest());
            bean.setTargetUrl(appBean.getTargetUrl());
            bean.setPageUrl(appBean.getPageUrl());
            bean.setQueryString(appBean.getQueryString());
            bean.setQueryParams(appBean.getQueryParams());
            bean.setLastFetchResult(appBean.getLastFetchResult());
            bean.setResponseCode(appBean.getResponseCode());
            bean.setContentType(appBean.getContentType());
            bean.setContentLength(appBean.getContentLength());
            bean.setLanguage(appBean.getLanguage());
            bean.setRedirect(appBean.getRedirect());
            bean.setLocation(appBean.getLocation());
            bean.setPageTitle(appBean.getPageTitle());
            bean.setNote(appBean.getNote());
            bean.setDeferred(appBean.isDeferred());
            bean.setStatus(appBean.getStatus());
            bean.setRefreshStatus(appBean.getRefreshStatus());
            bean.setRefreshInterval(appBean.getRefreshInterval());
            bean.setNextRefreshTime(appBean.getNextRefreshTime());
            bean.setLastCheckedTime(appBean.getLastCheckedTime());
            bean.setLastUpdatedTime(appBean.getLastUpdatedTime());
            bean.setCardType(appBean.getCardType());
            bean.setSummaryCard(LocalTwitterSummaryCardServiceProxy.convertAppTwitterSummaryCardBeanToServerBean(appBean.getSummaryCard()));
            bean.setPhotoCard(LocalTwitterPhotoCardServiceProxy.convertAppTwitterPhotoCardBeanToServerBean(appBean.getPhotoCard()));
            bean.setGalleryCard(LocalTwitterGalleryCardServiceProxy.convertAppTwitterGalleryCardBeanToServerBean(appBean.getGalleryCard()));
            bean.setAppCard(LocalTwitterAppCardServiceProxy.convertAppTwitterAppCardBeanToServerBean(appBean.getAppCard()));
            bean.setPlayerCard(LocalTwitterPlayerCardServiceProxy.convertAppTwitterPlayerCardBeanToServerBean(appBean.getPlayerCard()));
            bean.setProductCard(LocalTwitterProductCardServiceProxy.convertAppTwitterProductCardBeanToServerBean(appBean.getProductCard()));
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterCardMeta> convertAppTwitterCardMetaBeanListToServerBeanList(List<TwitterCardMeta> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterCardMeta> beanList = new ArrayList<TwitterCardMeta>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(TwitterCardMeta sb : appBeanList) {
                com.pagesynopsis.ws.bean.TwitterCardMetaBean bean = convertAppTwitterCardMetaBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
