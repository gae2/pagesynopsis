package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgQuantityStruct;
// import com.pagesynopsis.ws.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;


public class OgQuantityStructProxyUtil
{
    private static final Logger log = Logger.getLogger(OgQuantityStructProxyUtil.class.getName());

    // Static methods only.
    private OgQuantityStructProxyUtil() {}

    public static OgQuantityStructBean convertServerOgQuantityStructBeanToAppBean(OgQuantityStruct serverBean)
    {
        OgQuantityStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new OgQuantityStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setValue(serverBean.getValue());
            bean.setUnits(serverBean.getUnits());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.OgQuantityStructBean convertAppOgQuantityStructBeanToServerBean(OgQuantityStruct appBean)
    {
        com.pagesynopsis.ws.bean.OgQuantityStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgQuantityStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setValue(appBean.getValue());
            bean.setUnits(appBean.getUnits());
        }
        return bean;
    }

}
