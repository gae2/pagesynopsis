package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterProductCard;
// import com.pagesynopsis.ws.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.proxy.TwitterProductCardServiceProxy;
import com.pagesynopsis.af.proxy.util.TwitterCardProductDataProxyUtil;
import com.pagesynopsis.af.proxy.util.TwitterCardProductDataProxyUtil;


public class LocalTwitterProductCardServiceProxy extends BaseLocalServiceProxy implements TwitterProductCardServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterProductCardServiceProxy.class.getName());

    public LocalTwitterProductCardServiceProxy()
    {
    }

    @Override
    public TwitterProductCard getTwitterProductCard(String guid) throws BaseException
    {
        TwitterProductCard serverBean = getTwitterProductCardService().getTwitterProductCard(guid);
        TwitterProductCard appBean = convertServerTwitterProductCardBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getTwitterProductCard(String guid, String field) throws BaseException
    {
        return getTwitterProductCardService().getTwitterProductCard(guid, field);       
    }

    @Override
    public List<TwitterProductCard> getTwitterProductCards(List<String> guids) throws BaseException
    {
        List<TwitterProductCard> serverBeanList = getTwitterProductCardService().getTwitterProductCards(guids);
        List<TwitterProductCard> appBeanList = convertServerTwitterProductCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards() throws BaseException
    {
        return getAllTwitterProductCards(null, null, null);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterProductCardService().getAllTwitterProductCards(ordering, offset, count);
        return getAllTwitterProductCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterProductCard> serverBeanList = getTwitterProductCardService().getAllTwitterProductCards(ordering, offset, count, forwardCursor);
        List<TwitterProductCard> appBeanList = convertServerTwitterProductCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterProductCardService().getAllTwitterProductCardKeys(ordering, offset, count);
        return getAllTwitterProductCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterProductCardService().getAllTwitterProductCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterProductCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterProductCardService().findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterProductCard> serverBeanList = getTwitterProductCardService().findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<TwitterProductCard> appBeanList = convertServerTwitterProductCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterProductCardService().findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterProductCardService().findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterProductCardService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterProductCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException
    {
        return getTwitterProductCardService().createTwitterProductCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1, data2);
    }

    @Override
    public String createTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterProductCardBean serverBean =  convertAppTwitterProductCardBeanToServerBean(twitterProductCard);
        return getTwitterProductCardService().createTwitterProductCard(serverBean);
    }

    @Override
    public Boolean updateTwitterProductCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException
    {
        return getTwitterProductCardService().updateTwitterProductCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1, data2);
    }

    @Override
    public Boolean updateTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterProductCardBean serverBean =  convertAppTwitterProductCardBeanToServerBean(twitterProductCard);
        return getTwitterProductCardService().updateTwitterProductCard(serverBean);
    }

    @Override
    public Boolean deleteTwitterProductCard(String guid) throws BaseException
    {
        return getTwitterProductCardService().deleteTwitterProductCard(guid);
    }

    @Override
    public Boolean deleteTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterProductCardBean serverBean =  convertAppTwitterProductCardBeanToServerBean(twitterProductCard);
        return getTwitterProductCardService().deleteTwitterProductCard(serverBean);
    }

    @Override
    public Long deleteTwitterProductCards(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterProductCardService().deleteTwitterProductCards(filter, params, values);
    }




    public static TwitterProductCardBean convertServerTwitterProductCardBeanToAppBean(TwitterProductCard serverBean)
    {
        TwitterProductCardBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new TwitterProductCardBean();
            bean.setGuid(serverBean.getGuid());
            bean.setCard(serverBean.getCard());
            bean.setUrl(serverBean.getUrl());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setSite(serverBean.getSite());
            bean.setSiteId(serverBean.getSiteId());
            bean.setCreator(serverBean.getCreator());
            bean.setCreatorId(serverBean.getCreatorId());
            bean.setImage(serverBean.getImage());
            bean.setImageWidth(serverBean.getImageWidth());
            bean.setImageHeight(serverBean.getImageHeight());
            bean.setData1(TwitterCardProductDataProxyUtil.convertServerTwitterCardProductDataBeanToAppBean(serverBean.getData1()));
            bean.setData2(TwitterCardProductDataProxyUtil.convertServerTwitterCardProductDataBeanToAppBean(serverBean.getData2()));
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterProductCard> convertServerTwitterProductCardBeanListToAppBeanList(List<TwitterProductCard> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterProductCard> beanList = new ArrayList<TwitterProductCard>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(TwitterProductCard sb : serverBeanList) {
                TwitterProductCardBean bean = convertServerTwitterProductCardBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.TwitterProductCardBean convertAppTwitterProductCardBeanToServerBean(TwitterProductCard appBean)
    {
        com.pagesynopsis.ws.bean.TwitterProductCardBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.TwitterProductCardBean();
            bean.setGuid(appBean.getGuid());
            bean.setCard(appBean.getCard());
            bean.setUrl(appBean.getUrl());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setSite(appBean.getSite());
            bean.setSiteId(appBean.getSiteId());
            bean.setCreator(appBean.getCreator());
            bean.setCreatorId(appBean.getCreatorId());
            bean.setImage(appBean.getImage());
            bean.setImageWidth(appBean.getImageWidth());
            bean.setImageHeight(appBean.getImageHeight());
            bean.setData1(TwitterCardProductDataProxyUtil.convertAppTwitterCardProductDataBeanToServerBean(appBean.getData1()));
            bean.setData2(TwitterCardProductDataProxyUtil.convertAppTwitterCardProductDataBeanToServerBean(appBean.getData2()));
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterProductCard> convertAppTwitterProductCardBeanListToServerBeanList(List<TwitterProductCard> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterProductCard> beanList = new ArrayList<TwitterProductCard>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(TwitterProductCard sb : appBeanList) {
                com.pagesynopsis.ws.bean.TwitterProductCardBean bean = convertAppTwitterProductCardBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
