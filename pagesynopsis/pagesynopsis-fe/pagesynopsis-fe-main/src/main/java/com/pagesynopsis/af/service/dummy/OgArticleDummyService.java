package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgArticleService;


// The primary purpose of OgArticleDummyService is to fake the service api, OgArticleService.
// It has no real implementation.
public class OgArticleDummyService implements OgArticleService
{
    private static final Logger log = Logger.getLogger(OgArticleDummyService.class.getName());

    public OgArticleDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // OgArticle related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgArticle getOgArticle(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgArticle(): guid = " + guid);
        return null;
    }

    @Override
    public Object getOgArticle(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgArticle(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<OgArticle> getOgArticles(List<String> guids) throws BaseException
    {
        log.fine("getOgArticles()");
        return null;
    }

    @Override
    public List<OgArticle> getAllOgArticles() throws BaseException
    {
        return getAllOgArticles(null, null, null);
    }


    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticles(ordering, offset, count, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgArticles(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticleKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgArticleKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgArticleDummyService.findOgArticles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgArticleDummyService.findOgArticleKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgArticleDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createOgArticle(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        log.finer("createOgArticle()");
        return null;
    }

    @Override
    public String createOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("createOgArticle()");
        return null;
    }

    @Override
    public OgArticle constructOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("constructOgArticle()");
        return null;
    }

    @Override
    public Boolean updateOgArticle(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        log.finer("updateOgArticle()");
        return null;
    }
        
    @Override
    public Boolean updateOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("updateOgArticle()");
        return null;
    }

    @Override
    public OgArticle refreshOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("refreshOgArticle()");
        return null;
    }

    @Override
    public Boolean deleteOgArticle(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgArticle(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("deleteOgArticle()");
        return null;
    }

    // TBD
    @Override
    public Long deleteOgArticles(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgArticle(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgArticles(List<OgArticle> ogArticles) throws BaseException
    {
        log.finer("createOgArticles()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateOgArticles(List<OgArticle> ogArticles) throws BaseException
    //{
    //}

}
