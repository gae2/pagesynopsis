package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class OgQuantityStructJsBean implements Serializable, Cloneable  //, OgQuantityStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgQuantityStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private Float value;
    private String units;

    // Ctors.
    public OgQuantityStructJsBean()
    {
        //this((String) null);
    }
    public OgQuantityStructJsBean(String uuid, Float value, String units)
    {
        this.uuid = uuid;
        this.value = value;
        this.units = units;
    }
    public OgQuantityStructJsBean(OgQuantityStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setValue(bean.getValue());
            setUnits(bean.getUnits());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static OgQuantityStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        OgQuantityStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(OgQuantityStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, OgQuantityStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public Float getValue()
    {
        return this.value;
    }
    public void setValue(Float value)
    {
        this.value = value;
    }

    public String getUnits()
    {
        return this.units;
    }
    public void setUnits(String units)
    {
        this.units = units;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getValue() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUnits() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("value:0, ");
        sb.append("units:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("value:" + this.getValue()).append(", ");
        sb.append("units:");
        if(this.getUnits() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUnits()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getValue() != null) {
            sb.append("\"value\":").append("").append(this.getValue()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"value\":").append("null, ");
        }
        if(this.getUnits() != null) {
            sb.append("\"units\":").append("\"").append(this.getUnits()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"units\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("value = " + this.value).append(";");
        sb.append("units = " + this.units).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        OgQuantityStructJsBean cloned = new OgQuantityStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setValue(this.getValue());   
        cloned.setUnits(this.getUnits());   
        return cloned;
    }

}
