package com.pagesynopsis.af.cert.proxy;

public abstract class AbstractProxyFactory
{
    public abstract PublicCertificateInfoServiceProxy getPublicCertificateInfoServiceProxy();
}
