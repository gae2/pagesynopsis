package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.stub.RobotsTextFileStub;
import com.pagesynopsis.ws.stub.RobotsTextFileListStub;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.resource.RobotsTextFileResource;
import com.pagesynopsis.af.resource.util.RobotsTextGroupResourceUtil;


// MockRobotsTextFileResource is a decorator.
// It can be used as a base class to mock RobotsTextFileResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/robotsTextFiles/")
public abstract class MockRobotsTextFileResource implements RobotsTextFileResource
{
    private static final Logger log = Logger.getLogger(MockRobotsTextFileResource.class.getName());

    // MockRobotsTextFileResource uses the decorator design pattern.
    private RobotsTextFileResource decoratedResource;

    public MockRobotsTextFileResource(RobotsTextFileResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected RobotsTextFileResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(RobotsTextFileResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllRobotsTextFiles(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllRobotsTextFileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findRobotsTextFilesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findRobotsTextFilesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getRobotsTextFileAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getRobotsTextFileAsHtml(guid);
//    }

    @Override
    public Response getRobotsTextFile(String guid) throws BaseResourceException
    {
        return decoratedResource.getRobotsTextFile(guid);
    }

    @Override
    public Response getRobotsTextFileAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getRobotsTextFileAsJsonp(guid, callback);
    }

    @Override
    public Response getRobotsTextFile(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getRobotsTextFile(guid, field);
    }

    // TBD
    @Override
    public Response constructRobotsTextFile(RobotsTextFileStub robotsTextFile) throws BaseResourceException
    {
        return decoratedResource.constructRobotsTextFile(robotsTextFile);
    }

    @Override
    public Response createRobotsTextFile(RobotsTextFileStub robotsTextFile) throws BaseResourceException
    {
        return decoratedResource.createRobotsTextFile(robotsTextFile);
    }

//    @Override
//    public Response createRobotsTextFile(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createRobotsTextFile(formParams);
//    }

    // TBD
    @Override
    public Response refreshRobotsTextFile(String guid, RobotsTextFileStub robotsTextFile) throws BaseResourceException
    {
        return decoratedResource.refreshRobotsTextFile(guid, robotsTextFile);
    }

    @Override
    public Response updateRobotsTextFile(String guid, RobotsTextFileStub robotsTextFile) throws BaseResourceException
    {
        return decoratedResource.updateRobotsTextFile(guid, robotsTextFile);
    }

    @Override
    public Response updateRobotsTextFile(String guid, String siteUrl, List<String> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime)
    {
        return decoratedResource.updateRobotsTextFile(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
    }

//    @Override
//    public Response updateRobotsTextFile(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateRobotsTextFile(guid, formParams);
//    }

    @Override
    public Response deleteRobotsTextFile(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteRobotsTextFile(guid);
    }

    @Override
    public Response deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteRobotsTextFiles(filter, params, values);
    }


// TBD ....
    @Override
    public Response createRobotsTextFiles(RobotsTextFileListStub robotsTextFiles) throws BaseResourceException
    {
        return decoratedResource.createRobotsTextFiles(robotsTextFiles);
    }


}
