package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextRefresh;
// import com.pagesynopsis.ws.bean.RobotsTextRefreshBean;
import com.pagesynopsis.ws.service.RobotsTextRefreshService;
import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.af.proxy.RobotsTextRefreshServiceProxy;


public class LocalRobotsTextRefreshServiceProxy extends BaseLocalServiceProxy implements RobotsTextRefreshServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalRobotsTextRefreshServiceProxy.class.getName());

    public LocalRobotsTextRefreshServiceProxy()
    {
    }

    @Override
    public RobotsTextRefresh getRobotsTextRefresh(String guid) throws BaseException
    {
        RobotsTextRefresh serverBean = getRobotsTextRefreshService().getRobotsTextRefresh(guid);
        RobotsTextRefresh appBean = convertServerRobotsTextRefreshBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getRobotsTextRefresh(String guid, String field) throws BaseException
    {
        return getRobotsTextRefreshService().getRobotsTextRefresh(guid, field);       
    }

    @Override
    public List<RobotsTextRefresh> getRobotsTextRefreshes(List<String> guids) throws BaseException
    {
        List<RobotsTextRefresh> serverBeanList = getRobotsTextRefreshService().getRobotsTextRefreshes(guids);
        List<RobotsTextRefresh> appBeanList = convertServerRobotsTextRefreshBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes() throws BaseException
    {
        return getAllRobotsTextRefreshes(null, null, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextRefreshService().getAllRobotsTextRefreshes(ordering, offset, count);
        return getAllRobotsTextRefreshes(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<RobotsTextRefresh> serverBeanList = getRobotsTextRefreshService().getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
        List<RobotsTextRefresh> appBeanList = convertServerRobotsTextRefreshBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextRefreshService().getAllRobotsTextRefreshKeys(ordering, offset, count);
        return getAllRobotsTextRefreshKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextRefreshService().getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextRefreshService().findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<RobotsTextRefresh> serverBeanList = getRobotsTextRefreshService().findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<RobotsTextRefresh> appBeanList = convertServerRobotsTextRefreshBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextRefreshService().findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextRefreshService().findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getRobotsTextRefreshService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createRobotsTextRefresh(String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        return getRobotsTextRefreshService().createRobotsTextRefresh(robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
    }

    @Override
    public String createRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        com.pagesynopsis.ws.bean.RobotsTextRefreshBean serverBean =  convertAppRobotsTextRefreshBeanToServerBean(robotsTextRefresh);
        return getRobotsTextRefreshService().createRobotsTextRefresh(serverBean);
    }

    @Override
    public Boolean updateRobotsTextRefresh(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        return getRobotsTextRefreshService().updateRobotsTextRefresh(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
    }

    @Override
    public Boolean updateRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        com.pagesynopsis.ws.bean.RobotsTextRefreshBean serverBean =  convertAppRobotsTextRefreshBeanToServerBean(robotsTextRefresh);
        return getRobotsTextRefreshService().updateRobotsTextRefresh(serverBean);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(String guid) throws BaseException
    {
        return getRobotsTextRefreshService().deleteRobotsTextRefresh(guid);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        com.pagesynopsis.ws.bean.RobotsTextRefreshBean serverBean =  convertAppRobotsTextRefreshBeanToServerBean(robotsTextRefresh);
        return getRobotsTextRefreshService().deleteRobotsTextRefresh(serverBean);
    }

    @Override
    public Long deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseException
    {
        return getRobotsTextRefreshService().deleteRobotsTextRefreshes(filter, params, values);
    }




    public static RobotsTextRefreshBean convertServerRobotsTextRefreshBeanToAppBean(RobotsTextRefresh serverBean)
    {
        RobotsTextRefreshBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new RobotsTextRefreshBean();
            bean.setGuid(serverBean.getGuid());
            bean.setRobotsTextFile(serverBean.getRobotsTextFile());
            bean.setRefreshInterval(serverBean.getRefreshInterval());
            bean.setNote(serverBean.getNote());
            bean.setStatus(serverBean.getStatus());
            bean.setRefreshStatus(serverBean.getRefreshStatus());
            bean.setResult(serverBean.getResult());
            bean.setLastCheckedTime(serverBean.getLastCheckedTime());
            bean.setNextCheckedTime(serverBean.getNextCheckedTime());
            bean.setExpirationTime(serverBean.getExpirationTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<RobotsTextRefresh> convertServerRobotsTextRefreshBeanListToAppBeanList(List<RobotsTextRefresh> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<RobotsTextRefresh> beanList = new ArrayList<RobotsTextRefresh>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(RobotsTextRefresh sb : serverBeanList) {
                RobotsTextRefreshBean bean = convertServerRobotsTextRefreshBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.RobotsTextRefreshBean convertAppRobotsTextRefreshBeanToServerBean(RobotsTextRefresh appBean)
    {
        com.pagesynopsis.ws.bean.RobotsTextRefreshBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.RobotsTextRefreshBean();
            bean.setGuid(appBean.getGuid());
            bean.setRobotsTextFile(appBean.getRobotsTextFile());
            bean.setRefreshInterval(appBean.getRefreshInterval());
            bean.setNote(appBean.getNote());
            bean.setStatus(appBean.getStatus());
            bean.setRefreshStatus(appBean.getRefreshStatus());
            bean.setResult(appBean.getResult());
            bean.setLastCheckedTime(appBean.getLastCheckedTime());
            bean.setNextCheckedTime(appBean.getNextCheckedTime());
            bean.setExpirationTime(appBean.getExpirationTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<RobotsTextRefresh> convertAppRobotsTextRefreshBeanListToServerBeanList(List<RobotsTextRefresh> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<RobotsTextRefresh> beanList = new ArrayList<RobotsTextRefresh>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(RobotsTextRefresh sb : appBeanList) {
                com.pagesynopsis.ws.bean.RobotsTextRefreshBean bean = convertAppRobotsTextRefreshBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
