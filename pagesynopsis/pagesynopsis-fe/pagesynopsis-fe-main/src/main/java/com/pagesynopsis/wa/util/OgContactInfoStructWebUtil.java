package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.OgContactInfoStruct;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.fe.bean.OgContactInfoStructJsBean;


public class OgContactInfoStructWebUtil
{
    private static final Logger log = Logger.getLogger(OgContactInfoStructWebUtil.class.getName());

    // Static methods only.
    private OgContactInfoStructWebUtil() {}
    

    public static OgContactInfoStructJsBean convertOgContactInfoStructToJsBean(OgContactInfoStruct ogContactInfoStruct)
    {
        OgContactInfoStructJsBean jsBean = null;
        if(ogContactInfoStruct != null) {
            jsBean = new OgContactInfoStructJsBean();
            jsBean.setUuid(ogContactInfoStruct.getUuid());
            jsBean.setStreetAddress(ogContactInfoStruct.getStreetAddress());
            jsBean.setLocality(ogContactInfoStruct.getLocality());
            jsBean.setRegion(ogContactInfoStruct.getRegion());
            jsBean.setPostalCode(ogContactInfoStruct.getPostalCode());
            jsBean.setCountryName(ogContactInfoStruct.getCountryName());
            jsBean.setEmailAddress(ogContactInfoStruct.getEmailAddress());
            jsBean.setPhoneNumber(ogContactInfoStruct.getPhoneNumber());
            jsBean.setFaxNumber(ogContactInfoStruct.getFaxNumber());
            jsBean.setWebsite(ogContactInfoStruct.getWebsite());
        }
        return jsBean;
    }

    public static OgContactInfoStruct convertOgContactInfoStructJsBeanToBean(OgContactInfoStructJsBean jsBean)
    {
        OgContactInfoStructBean ogContactInfoStruct = null;
        if(jsBean != null) {
            ogContactInfoStruct = new OgContactInfoStructBean();
            ogContactInfoStruct.setUuid(jsBean.getUuid());
            ogContactInfoStruct.setStreetAddress(jsBean.getStreetAddress());
            ogContactInfoStruct.setLocality(jsBean.getLocality());
            ogContactInfoStruct.setRegion(jsBean.getRegion());
            ogContactInfoStruct.setPostalCode(jsBean.getPostalCode());
            ogContactInfoStruct.setCountryName(jsBean.getCountryName());
            ogContactInfoStruct.setEmailAddress(jsBean.getEmailAddress());
            ogContactInfoStruct.setPhoneNumber(jsBean.getPhoneNumber());
            ogContactInfoStruct.setFaxNumber(jsBean.getFaxNumber());
            ogContactInfoStruct.setWebsite(jsBean.getWebsite());
        }
        return ogContactInfoStruct;
    }

}
