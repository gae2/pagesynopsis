package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgTvShowListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.resource.OgTvShowResource;
import com.pagesynopsis.af.resource.util.OgAudioStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgImageStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgActorStructResourceUtil;
import com.pagesynopsis.af.resource.util.OgVideoStructResourceUtil;


// MockOgTvShowResource is a decorator.
// It can be used as a base class to mock OgTvShowResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/ogTvShows/")
public abstract class MockOgTvShowResource implements OgTvShowResource
{
    private static final Logger log = Logger.getLogger(MockOgTvShowResource.class.getName());

    // MockOgTvShowResource uses the decorator design pattern.
    private OgTvShowResource decoratedResource;

    public MockOgTvShowResource(OgTvShowResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected OgTvShowResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(OgTvShowResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgTvShows(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgTvShowKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgTvShowsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findOgTvShowsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getOgTvShowAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getOgTvShowAsHtml(guid);
//    }

    @Override
    public Response getOgTvShow(String guid) throws BaseResourceException
    {
        return decoratedResource.getOgTvShow(guid);
    }

    @Override
    public Response getOgTvShowAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getOgTvShowAsJsonp(guid, callback);
    }

    @Override
    public Response getOgTvShow(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getOgTvShow(guid, field);
    }

    // TBD
    @Override
    public Response constructOgTvShow(OgTvShowStub ogTvShow) throws BaseResourceException
    {
        return decoratedResource.constructOgTvShow(ogTvShow);
    }

    @Override
    public Response createOgTvShow(OgTvShowStub ogTvShow) throws BaseResourceException
    {
        return decoratedResource.createOgTvShow(ogTvShow);
    }

//    @Override
//    public Response createOgTvShow(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createOgTvShow(formParams);
//    }

    // TBD
    @Override
    public Response refreshOgTvShow(String guid, OgTvShowStub ogTvShow) throws BaseResourceException
    {
        return decoratedResource.refreshOgTvShow(guid, ogTvShow);
    }

    @Override
    public Response updateOgTvShow(String guid, OgTvShowStub ogTvShow) throws BaseResourceException
    {
        return decoratedResource.updateOgTvShow(guid, ogTvShow);
    }

    @Override
    public Response updateOgTvShow(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<String> image, List<String> audio, List<String> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<String> actor, Integer duration, List<String> tag, String releaseDate)
    {
        return decoratedResource.updateOgTvShow(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

//    @Override
//    public Response updateOgTvShow(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateOgTvShow(guid, formParams);
//    }

    @Override
    public Response deleteOgTvShow(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteOgTvShow(guid);
    }

    @Override
    public Response deleteOgTvShows(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteOgTvShows(filter, params, values);
    }


// TBD ....
    @Override
    public Response createOgTvShows(OgTvShowListStub ogTvShows) throws BaseResourceException
    {
        return decoratedResource.createOgTvShows(ogTvShows);
    }


}
