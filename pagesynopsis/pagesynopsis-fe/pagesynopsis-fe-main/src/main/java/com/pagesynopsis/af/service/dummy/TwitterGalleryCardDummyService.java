package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterGalleryCardService;


// The primary purpose of TwitterGalleryCardDummyService is to fake the service api, TwitterGalleryCardService.
// It has no real implementation.
public class TwitterGalleryCardDummyService implements TwitterGalleryCardService
{
    private static final Logger log = Logger.getLogger(TwitterGalleryCardDummyService.class.getName());

    public TwitterGalleryCardDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterGalleryCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterGalleryCard getTwitterGalleryCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterGalleryCard(): guid = " + guid);
        return null;
    }

    @Override
    public Object getTwitterGalleryCard(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterGalleryCard(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<TwitterGalleryCard> getTwitterGalleryCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterGalleryCards()");
        return null;
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards() throws BaseException
    {
        return getAllTwitterGalleryCards(null, null, null);
    }


    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterGalleryCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterGalleryCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterGalleryCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterGalleryCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterGalleryCardDummyService.findTwitterGalleryCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterGalleryCardDummyService.findTwitterGalleryCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterGalleryCardDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createTwitterGalleryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        log.finer("createTwitterGalleryCard()");
        return null;
    }

    @Override
    public String createTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("createTwitterGalleryCard()");
        return null;
    }

    @Override
    public TwitterGalleryCard constructTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("constructTwitterGalleryCard()");
        return null;
    }

    @Override
    public Boolean updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        log.finer("updateTwitterGalleryCard()");
        return null;
    }
        
    @Override
    public Boolean updateTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("updateTwitterGalleryCard()");
        return null;
    }

    @Override
    public TwitterGalleryCard refreshTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("refreshTwitterGalleryCard()");
        return null;
    }

    @Override
    public Boolean deleteTwitterGalleryCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTwitterGalleryCard(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        log.finer("deleteTwitterGalleryCard()");
        return null;
    }

    // TBD
    @Override
    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTwitterGalleryCard(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException
    {
        log.finer("createTwitterGalleryCards()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException
    //{
    //}

}
