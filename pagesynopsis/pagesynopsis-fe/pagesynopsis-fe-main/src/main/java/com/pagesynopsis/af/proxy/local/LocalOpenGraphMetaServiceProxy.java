package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OpenGraphMeta;
// import com.pagesynopsis.ws.bean.OpenGraphMetaBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.OpenGraphMetaService;
import com.pagesynopsis.af.bean.OpenGraphMetaBean;
import com.pagesynopsis.af.proxy.OpenGraphMetaServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalOgProfileServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalOgWebsiteServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalOgBlogServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalOgArticleServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalOgBookServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalOgVideoServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalOgMovieServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalOgTvShowServiceProxy;
import com.pagesynopsis.af.proxy.local.LocalOgTvEpisodeServiceProxy;


public class LocalOpenGraphMetaServiceProxy extends BaseLocalServiceProxy implements OpenGraphMetaServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOpenGraphMetaServiceProxy.class.getName());

    public LocalOpenGraphMetaServiceProxy()
    {
    }

    @Override
    public OpenGraphMeta getOpenGraphMeta(String guid) throws BaseException
    {
        OpenGraphMeta serverBean = getOpenGraphMetaService().getOpenGraphMeta(guid);
        OpenGraphMeta appBean = convertServerOpenGraphMetaBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getOpenGraphMeta(String guid, String field) throws BaseException
    {
        return getOpenGraphMetaService().getOpenGraphMeta(guid, field);       
    }

    @Override
    public List<OpenGraphMeta> getOpenGraphMetas(List<String> guids) throws BaseException
    {
        List<OpenGraphMeta> serverBeanList = getOpenGraphMetaService().getOpenGraphMetas(guids);
        List<OpenGraphMeta> appBeanList = convertServerOpenGraphMetaBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas() throws BaseException
    {
        return getAllOpenGraphMetas(null, null, null);
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOpenGraphMetaService().getAllOpenGraphMetas(ordering, offset, count);
        return getAllOpenGraphMetas(ordering, offset, count, null);
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OpenGraphMeta> serverBeanList = getOpenGraphMetaService().getAllOpenGraphMetas(ordering, offset, count, forwardCursor);
        List<OpenGraphMeta> appBeanList = convertServerOpenGraphMetaBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOpenGraphMetaService().getAllOpenGraphMetaKeys(ordering, offset, count);
        return getAllOpenGraphMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOpenGraphMetaService().getAllOpenGraphMetaKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOpenGraphMetas(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOpenGraphMetaService().findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count);
        return findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<OpenGraphMeta> serverBeanList = getOpenGraphMetaService().findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<OpenGraphMeta> appBeanList = convertServerOpenGraphMetaBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOpenGraphMetaService().findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOpenGraphMetaService().findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOpenGraphMetaService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOpenGraphMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        return getOpenGraphMetaService().createOpenGraphMeta(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfile, ogWebsite, ogBlog, ogArticle, ogBook, ogVideo, ogMovie, ogTvShow, ogTvEpisode);
    }

    @Override
    public String createOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        com.pagesynopsis.ws.bean.OpenGraphMetaBean serverBean =  convertAppOpenGraphMetaBeanToServerBean(openGraphMeta);
        return getOpenGraphMetaService().createOpenGraphMeta(serverBean);
    }

    @Override
    public Boolean updateOpenGraphMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        return getOpenGraphMetaService().updateOpenGraphMeta(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfile, ogWebsite, ogBlog, ogArticle, ogBook, ogVideo, ogMovie, ogTvShow, ogTvEpisode);
    }

    @Override
    public Boolean updateOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        com.pagesynopsis.ws.bean.OpenGraphMetaBean serverBean =  convertAppOpenGraphMetaBeanToServerBean(openGraphMeta);
        return getOpenGraphMetaService().updateOpenGraphMeta(serverBean);
    }

    @Override
    public Boolean deleteOpenGraphMeta(String guid) throws BaseException
    {
        return getOpenGraphMetaService().deleteOpenGraphMeta(guid);
    }

    @Override
    public Boolean deleteOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        com.pagesynopsis.ws.bean.OpenGraphMetaBean serverBean =  convertAppOpenGraphMetaBeanToServerBean(openGraphMeta);
        return getOpenGraphMetaService().deleteOpenGraphMeta(serverBean);
    }

    @Override
    public Long deleteOpenGraphMetas(String filter, String params, List<String> values) throws BaseException
    {
        return getOpenGraphMetaService().deleteOpenGraphMetas(filter, params, values);
    }




    public static OpenGraphMetaBean convertServerOpenGraphMetaBeanToAppBean(OpenGraphMeta serverBean)
    {
        OpenGraphMetaBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new OpenGraphMetaBean();
            bean.setGuid(serverBean.getGuid());
            bean.setUser(serverBean.getUser());
            bean.setFetchRequest(serverBean.getFetchRequest());
            bean.setTargetUrl(serverBean.getTargetUrl());
            bean.setPageUrl(serverBean.getPageUrl());
            bean.setQueryString(serverBean.getQueryString());
            bean.setQueryParams(serverBean.getQueryParams());
            bean.setLastFetchResult(serverBean.getLastFetchResult());
            bean.setResponseCode(serverBean.getResponseCode());
            bean.setContentType(serverBean.getContentType());
            bean.setContentLength(serverBean.getContentLength());
            bean.setLanguage(serverBean.getLanguage());
            bean.setRedirect(serverBean.getRedirect());
            bean.setLocation(serverBean.getLocation());
            bean.setPageTitle(serverBean.getPageTitle());
            bean.setNote(serverBean.getNote());
            bean.setDeferred(serverBean.isDeferred());
            bean.setStatus(serverBean.getStatus());
            bean.setRefreshStatus(serverBean.getRefreshStatus());
            bean.setRefreshInterval(serverBean.getRefreshInterval());
            bean.setNextRefreshTime(serverBean.getNextRefreshTime());
            bean.setLastCheckedTime(serverBean.getLastCheckedTime());
            bean.setLastUpdatedTime(serverBean.getLastUpdatedTime());
            bean.setOgType(serverBean.getOgType());
            bean.setOgProfile(LocalOgProfileServiceProxy.convertServerOgProfileBeanToAppBean(serverBean.getOgProfile()));
            bean.setOgWebsite(LocalOgWebsiteServiceProxy.convertServerOgWebsiteBeanToAppBean(serverBean.getOgWebsite()));
            bean.setOgBlog(LocalOgBlogServiceProxy.convertServerOgBlogBeanToAppBean(serverBean.getOgBlog()));
            bean.setOgArticle(LocalOgArticleServiceProxy.convertServerOgArticleBeanToAppBean(serverBean.getOgArticle()));
            bean.setOgBook(LocalOgBookServiceProxy.convertServerOgBookBeanToAppBean(serverBean.getOgBook()));
            bean.setOgVideo(LocalOgVideoServiceProxy.convertServerOgVideoBeanToAppBean(serverBean.getOgVideo()));
            bean.setOgMovie(LocalOgMovieServiceProxy.convertServerOgMovieBeanToAppBean(serverBean.getOgMovie()));
            bean.setOgTvShow(LocalOgTvShowServiceProxy.convertServerOgTvShowBeanToAppBean(serverBean.getOgTvShow()));
            bean.setOgTvEpisode(LocalOgTvEpisodeServiceProxy.convertServerOgTvEpisodeBeanToAppBean(serverBean.getOgTvEpisode()));
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OpenGraphMeta> convertServerOpenGraphMetaBeanListToAppBeanList(List<OpenGraphMeta> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<OpenGraphMeta> beanList = new ArrayList<OpenGraphMeta>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(OpenGraphMeta sb : serverBeanList) {
                OpenGraphMetaBean bean = convertServerOpenGraphMetaBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.OpenGraphMetaBean convertAppOpenGraphMetaBeanToServerBean(OpenGraphMeta appBean)
    {
        com.pagesynopsis.ws.bean.OpenGraphMetaBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OpenGraphMetaBean();
            bean.setGuid(appBean.getGuid());
            bean.setUser(appBean.getUser());
            bean.setFetchRequest(appBean.getFetchRequest());
            bean.setTargetUrl(appBean.getTargetUrl());
            bean.setPageUrl(appBean.getPageUrl());
            bean.setQueryString(appBean.getQueryString());
            bean.setQueryParams(appBean.getQueryParams());
            bean.setLastFetchResult(appBean.getLastFetchResult());
            bean.setResponseCode(appBean.getResponseCode());
            bean.setContentType(appBean.getContentType());
            bean.setContentLength(appBean.getContentLength());
            bean.setLanguage(appBean.getLanguage());
            bean.setRedirect(appBean.getRedirect());
            bean.setLocation(appBean.getLocation());
            bean.setPageTitle(appBean.getPageTitle());
            bean.setNote(appBean.getNote());
            bean.setDeferred(appBean.isDeferred());
            bean.setStatus(appBean.getStatus());
            bean.setRefreshStatus(appBean.getRefreshStatus());
            bean.setRefreshInterval(appBean.getRefreshInterval());
            bean.setNextRefreshTime(appBean.getNextRefreshTime());
            bean.setLastCheckedTime(appBean.getLastCheckedTime());
            bean.setLastUpdatedTime(appBean.getLastUpdatedTime());
            bean.setOgType(appBean.getOgType());
            bean.setOgProfile(LocalOgProfileServiceProxy.convertAppOgProfileBeanToServerBean(appBean.getOgProfile()));
            bean.setOgWebsite(LocalOgWebsiteServiceProxy.convertAppOgWebsiteBeanToServerBean(appBean.getOgWebsite()));
            bean.setOgBlog(LocalOgBlogServiceProxy.convertAppOgBlogBeanToServerBean(appBean.getOgBlog()));
            bean.setOgArticle(LocalOgArticleServiceProxy.convertAppOgArticleBeanToServerBean(appBean.getOgArticle()));
            bean.setOgBook(LocalOgBookServiceProxy.convertAppOgBookBeanToServerBean(appBean.getOgBook()));
            bean.setOgVideo(LocalOgVideoServiceProxy.convertAppOgVideoBeanToServerBean(appBean.getOgVideo()));
            bean.setOgMovie(LocalOgMovieServiceProxy.convertAppOgMovieBeanToServerBean(appBean.getOgMovie()));
            bean.setOgTvShow(LocalOgTvShowServiceProxy.convertAppOgTvShowBeanToServerBean(appBean.getOgTvShow()));
            bean.setOgTvEpisode(LocalOgTvEpisodeServiceProxy.convertAppOgTvEpisodeBeanToServerBean(appBean.getOgTvEpisode()));
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<OpenGraphMeta> convertAppOpenGraphMetaBeanListToServerBeanList(List<OpenGraphMeta> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<OpenGraphMeta> beanList = new ArrayList<OpenGraphMeta>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(OpenGraphMeta sb : appBeanList) {
                com.pagesynopsis.ws.bean.OpenGraphMetaBean bean = convertAppOpenGraphMetaBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
