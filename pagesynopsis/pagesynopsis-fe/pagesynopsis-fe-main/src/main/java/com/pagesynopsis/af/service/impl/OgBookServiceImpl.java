package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgBookService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgBookServiceImpl implements OgBookService
{
    private static final Logger log = Logger.getLogger(OgBookServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "OgBook-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("OgBook:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public OgBookServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgBook related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgBook getOgBook(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgBook(): guid = " + guid);

        OgBookBean bean = null;
        if(getCache() != null) {
            bean = (OgBookBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (OgBookBean) getProxyFactory().getOgBookServiceProxy().getOgBook(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "OgBookBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgBookBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgBook(String guid, String field) throws BaseException
    {
        OgBookBean bean = null;
        if(getCache() != null) {
            bean = (OgBookBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (OgBookBean) getProxyFactory().getOgBookServiceProxy().getOgBook(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "OgBookBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgBookBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return bean.getUrl();
        } else if(field.equals("type")) {
            return bean.getType();
        } else if(field.equals("siteName")) {
            return bean.getSiteName();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("fbAdmins")) {
            return bean.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return bean.getFbAppId();
        } else if(field.equals("image")) {
            return bean.getImage();
        } else if(field.equals("audio")) {
            return bean.getAudio();
        } else if(field.equals("video")) {
            return bean.getVideo();
        } else if(field.equals("locale")) {
            return bean.getLocale();
        } else if(field.equals("localeAlternate")) {
            return bean.getLocaleAlternate();
        } else if(field.equals("author")) {
            return bean.getAuthor();
        } else if(field.equals("isbn")) {
            return bean.getIsbn();
        } else if(field.equals("tag")) {
            return bean.getTag();
        } else if(field.equals("releaseDate")) {
            return bean.getReleaseDate();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgBook> getOgBooks(List<String> guids) throws BaseException
    {
        log.fine("getOgBooks()");

        // TBD: Is there a better way????
        List<OgBook> ogBooks = getProxyFactory().getOgBookServiceProxy().getOgBooks(guids);
        if(ogBooks == null) {
            log.log(Level.WARNING, "Failed to retrieve OgBookBean list.");
        }

        log.finer("END");
        return ogBooks;
    }

    @Override
    public List<OgBook> getAllOgBooks() throws BaseException
    {
        return getAllOgBooks(null, null, null);
    }


    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBooks(ordering, offset, count, null);
    }

    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgBooks(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgBook> ogBooks = getProxyFactory().getOgBookServiceProxy().getAllOgBooks(ordering, offset, count, forwardCursor);
        if(ogBooks == null) {
            log.log(Level.WARNING, "Failed to retrieve OgBookBean list.");
        }

        log.finer("END");
        return ogBooks;
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBookKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgBookKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getOgBookServiceProxy().getAllOgBookKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgBookBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty OgBookBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgBooks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgBookServiceImpl.findOgBooks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgBook> ogBooks = getProxyFactory().getOgBookServiceProxy().findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(ogBooks == null) {
            log.log(Level.WARNING, "Failed to find ogBooks for the given criterion.");
        }

        log.finer("END");
        return ogBooks;
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgBookServiceImpl.findOgBookKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getOgBookServiceProxy().findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgBook keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty OgBook key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgBookServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getOgBookServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgBook(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        OgBookBean bean = new OgBookBean(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
        return createOgBook(bean);
    }

    @Override
    public String createOgBook(OgBook ogBook) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //OgBook bean = constructOgBook(ogBook);
        //return bean.getGuid();

        // Param ogBook cannot be null.....
        if(ogBook == null) {
            log.log(Level.INFO, "Param ogBook is null!");
            throw new BadRequestException("Param ogBook object is null!");
        }
        OgBookBean bean = null;
        if(ogBook instanceof OgBookBean) {
            bean = (OgBookBean) ogBook;
        } else if(ogBook instanceof OgBook) {
            // bean = new OgBookBean(null, ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new OgBookBean(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate());
        } else {
            log.log(Level.WARNING, "createOgBook(): Arg ogBook is of an unknown type.");
            //bean = new OgBookBean();
            bean = new OgBookBean(ogBook.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getOgBookServiceProxy().createOgBook(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public OgBook constructOgBook(OgBook ogBook) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogBook cannot be null.....
        if(ogBook == null) {
            log.log(Level.INFO, "Param ogBook is null!");
            throw new BadRequestException("Param ogBook object is null!");
        }
        OgBookBean bean = null;
        if(ogBook instanceof OgBookBean) {
            bean = (OgBookBean) ogBook;
        } else if(ogBook instanceof OgBook) {
            // bean = new OgBookBean(null, ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new OgBookBean(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate());
        } else {
            log.log(Level.WARNING, "createOgBook(): Arg ogBook is of an unknown type.");
            //bean = new OgBookBean();
            bean = new OgBookBean(ogBook.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getOgBookServiceProxy().createOgBook(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateOgBook(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgBookBean bean = new OgBookBean(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
        return updateOgBook(bean);
    }
        
    @Override
    public Boolean updateOgBook(OgBook ogBook) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //OgBook bean = refreshOgBook(ogBook);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param ogBook cannot be null.....
        if(ogBook == null || ogBook.getGuid() == null) {
            log.log(Level.INFO, "Param ogBook or its guid is null!");
            throw new BadRequestException("Param ogBook object or its guid is null!");
        }
        OgBookBean bean = null;
        if(ogBook instanceof OgBookBean) {
            bean = (OgBookBean) ogBook;
        } else {  // if(ogBook instanceof OgBook)
            bean = new OgBookBean(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate());
        }
        Boolean suc = getProxyFactory().getOgBookServiceProxy().updateOgBook(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public OgBook refreshOgBook(OgBook ogBook) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogBook cannot be null.....
        if(ogBook == null || ogBook.getGuid() == null) {
            log.log(Level.INFO, "Param ogBook or its guid is null!");
            throw new BadRequestException("Param ogBook object or its guid is null!");
        }
        OgBookBean bean = null;
        if(ogBook instanceof OgBookBean) {
            bean = (OgBookBean) ogBook;
        } else {  // if(ogBook instanceof OgBook)
            bean = new OgBookBean(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate());
        }
        Boolean suc = getProxyFactory().getOgBookServiceProxy().updateOgBook(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteOgBook(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getOgBookServiceProxy().deleteOgBook(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            OgBook ogBook = null;
            try {
                ogBook = getProxyFactory().getOgBookServiceProxy().getOgBook(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch ogBook with a key, " + guid);
                return false;
            }
            if(ogBook != null) {
                String beanGuid = ogBook.getGuid();
                Boolean suc1 = getProxyFactory().getOgBookServiceProxy().deleteOgBook(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("ogBook with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteOgBook(OgBook ogBook) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogBook cannot be null.....
        if(ogBook == null || ogBook.getGuid() == null) {
            log.log(Level.INFO, "Param ogBook or its guid is null!");
            throw new BadRequestException("Param ogBook object or its guid is null!");
        }
        OgBookBean bean = null;
        if(ogBook instanceof OgBookBean) {
            bean = (OgBookBean) ogBook;
        } else {  // if(ogBook instanceof OgBook)
            // ????
            log.warning("ogBook is not an instance of OgBookBean.");
            bean = new OgBookBean(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate());
        }
        Boolean suc = getProxyFactory().getOgBookServiceProxy().deleteOgBook(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOgBooks(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getOgBookServiceProxy().deleteOgBooks(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgBooks(List<OgBook> ogBooks) throws BaseException
    {
        log.finer("BEGIN");

        if(ogBooks == null) {
            log.log(Level.WARNING, "createOgBooks() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = ogBooks.size();
        if(size == 0) {
            log.log(Level.WARNING, "createOgBooks() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(OgBook ogBook : ogBooks) {
            String guid = createOgBook(ogBook);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createOgBooks() failed for at least one ogBook. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateOgBooks(List<OgBook> ogBooks) throws BaseException
    //{
    //}

}
