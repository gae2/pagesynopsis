package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.af.proxy.OgMovieServiceProxy;


// MockOgMovieServiceProxy is a decorator.
// It can be used as a base class to mock OgMovieServiceProxy objects.
public abstract class MockOgMovieServiceProxy implements OgMovieServiceProxy
{
    private static final Logger log = Logger.getLogger(MockOgMovieServiceProxy.class.getName());

    // MockOgMovieServiceProxy uses the decorator design pattern.
    private OgMovieServiceProxy decoratedProxy;

    public MockOgMovieServiceProxy(OgMovieServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected OgMovieServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(OgMovieServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public OgMovie getOgMovie(String guid) throws BaseException
    {
        return decoratedProxy.getOgMovie(guid);
    }

    @Override
    public Object getOgMovie(String guid, String field) throws BaseException
    {
        return decoratedProxy.getOgMovie(guid, field);       
    }

    @Override
    public List<OgMovie> getOgMovies(List<String> guids) throws BaseException
    {
        return decoratedProxy.getOgMovies(guids);
    }

    @Override
    public List<OgMovie> getAllOgMovies() throws BaseException
    {
        return getAllOgMovies(null, null, null);
    }

    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgMovies(ordering, offset, count);
        return getAllOgMovies(ordering, offset, count, null);
    }

    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgMovies(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgMovieKeys(ordering, offset, count);
        return getAllOgMovieKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgMovieKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgMovies(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgMovie(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedProxy.createOgMovie(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public String createOgMovie(OgMovie ogMovie) throws BaseException
    {
        return decoratedProxy.createOgMovie(ogMovie);
    }

    @Override
    public Boolean updateOgMovie(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedProxy.updateOgMovie(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public Boolean updateOgMovie(OgMovie ogMovie) throws BaseException
    {
        return decoratedProxy.updateOgMovie(ogMovie);
    }

    @Override
    public Boolean deleteOgMovie(String guid) throws BaseException
    {
        return decoratedProxy.deleteOgMovie(guid);
    }

    @Override
    public Boolean deleteOgMovie(OgMovie ogMovie) throws BaseException
    {
        String guid = ogMovie.getGuid();
        return deleteOgMovie(guid);
    }

    @Override
    public Long deleteOgMovies(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteOgMovies(filter, params, values);
    }

}
