package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgWebsiteService;


// The primary purpose of OgWebsiteDummyService is to fake the service api, OgWebsiteService.
// It has no real implementation.
public class OgWebsiteDummyService implements OgWebsiteService
{
    private static final Logger log = Logger.getLogger(OgWebsiteDummyService.class.getName());

    public OgWebsiteDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // OgWebsite related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgWebsite getOgWebsite(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgWebsite(): guid = " + guid);
        return null;
    }

    @Override
    public Object getOgWebsite(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgWebsite(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<OgWebsite> getOgWebsites(List<String> guids) throws BaseException
    {
        log.fine("getOgWebsites()");
        return null;
    }

    @Override
    public List<OgWebsite> getAllOgWebsites() throws BaseException
    {
        return getAllOgWebsites(null, null, null);
    }


    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgWebsites(ordering, offset, count, null);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgWebsites(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgWebsiteKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgWebsiteKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgWebsites(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgWebsiteDummyService.findOgWebsites(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgWebsiteDummyService.findOgWebsiteKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgWebsiteDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createOgWebsite(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        log.finer("createOgWebsite()");
        return null;
    }

    @Override
    public String createOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        log.finer("createOgWebsite()");
        return null;
    }

    @Override
    public OgWebsite constructOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        log.finer("constructOgWebsite()");
        return null;
    }

    @Override
    public Boolean updateOgWebsite(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        log.finer("updateOgWebsite()");
        return null;
    }
        
    @Override
    public Boolean updateOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        log.finer("updateOgWebsite()");
        return null;
    }

    @Override
    public OgWebsite refreshOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        log.finer("refreshOgWebsite()");
        return null;
    }

    @Override
    public Boolean deleteOgWebsite(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgWebsite(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        log.finer("deleteOgWebsite()");
        return null;
    }

    // TBD
    @Override
    public Long deleteOgWebsites(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgWebsite(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgWebsites(List<OgWebsite> ogWebsites) throws BaseException
    {
        log.finer("createOgWebsites()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateOgWebsites(List<OgWebsite> ogWebsites) throws BaseException
    //{
    //}

}
