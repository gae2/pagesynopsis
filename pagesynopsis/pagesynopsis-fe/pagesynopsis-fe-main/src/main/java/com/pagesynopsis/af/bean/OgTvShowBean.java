package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.stub.OgObjectBaseStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;


// Wrapper class + bean combo.
public class OgTvShowBean extends OgObjectBaseBean implements OgTvShow, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgTvShowBean.class.getName());


    // [2] Or, without an embedded object.
    private List<String> director;
    private List<String> writer;
    private List<OgActorStruct> actor;
    private Integer duration;
    private List<String> tag;
    private String releaseDate;

    // Ctors.
    public OgTvShowBean()
    {
        //this((String) null);
    }
    public OgTvShowBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OgTvShowBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate)
    {
        this(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate, null, null);
    }
    public OgTvShowBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate, Long createdTime, Long modifiedTime)
    {
        super(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, createdTime, modifiedTime);

        this.director = director;
        this.writer = writer;
        this.actor = actor;
        this.duration = duration;
        this.tag = tag;
        this.releaseDate = releaseDate;
    }
    public OgTvShowBean(OgTvShow stub)
    {
        if(stub instanceof OgTvShowStub) {
            super.setStub((OgObjectBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUrl(stub.getUrl());   
            setType(stub.getType());   
            setSiteName(stub.getSiteName());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setFbAdmins(stub.getFbAdmins());   
            setFbAppId(stub.getFbAppId());   
            setImage(stub.getImage());   
            setAudio(stub.getAudio());   
            setVideo(stub.getVideo());   
            setLocale(stub.getLocale());   
            setLocaleAlternate(stub.getLocaleAlternate());   
            setDirector(stub.getDirector());   
            setWriter(stub.getWriter());   
            setActor(stub.getActor());   
            setDuration(stub.getDuration());   
            setTag(stub.getTag());   
            setReleaseDate(stub.getReleaseDate());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    public String getType()
    {
        return super.getType();
    }
    public void setType(String type)
    {
        super.setType(type);
    }

    public String getSiteName()
    {
        return super.getSiteName();
    }
    public void setSiteName(String siteName)
    {
        super.setSiteName(siteName);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public List<String> getFbAdmins()
    {
        return super.getFbAdmins();
    }
    public void setFbAdmins(List<String> fbAdmins)
    {
        super.setFbAdmins(fbAdmins);
    }

    public List<String> getFbAppId()
    {
        return super.getFbAppId();
    }
    public void setFbAppId(List<String> fbAppId)
    {
        super.setFbAppId(fbAppId);
    }

    public List<OgImageStruct> getImage()
    {
        return super.getImage();
    }
    public void setImage(List<OgImageStruct> image)
    {
        super.setImage(image);
    }

    public List<OgAudioStruct> getAudio()
    {
        return super.getAudio();
    }
    public void setAudio(List<OgAudioStruct> audio)
    {
        super.setAudio(audio);
    }

    public List<OgVideoStruct> getVideo()
    {
        return super.getVideo();
    }
    public void setVideo(List<OgVideoStruct> video)
    {
        super.setVideo(video);
    }

    public String getLocale()
    {
        return super.getLocale();
    }
    public void setLocale(String locale)
    {
        super.setLocale(locale);
    }

    public List<String> getLocaleAlternate()
    {
        return super.getLocaleAlternate();
    }
    public void setLocaleAlternate(List<String> localeAlternate)
    {
        super.setLocaleAlternate(localeAlternate);
    }

    public List<String> getDirector()
    {
        if(getStub() != null) {
            return getStub().getDirector();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.director;
        }
    }
    public void setDirector(List<String> director)
    {
        if(getStub() != null) {
            getStub().setDirector(director);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.director = director;
        }
    }

    public List<String> getWriter()
    {
        if(getStub() != null) {
            return getStub().getWriter();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.writer;
        }
    }
    public void setWriter(List<String> writer)
    {
        if(getStub() != null) {
            getStub().setWriter(writer);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.writer = writer;
        }
    }

    public List<OgActorStruct> getActor()
    {
        if(getStub() != null) {
            List<OgActorStruct> list = getStub().getActor();
            if(list != null) {
                List<OgActorStruct> bean = new ArrayList<OgActorStruct>();
                for(OgActorStruct ogActorStruct : list) {
                    OgActorStructBean elem = null;
                    if(ogActorStruct instanceof OgActorStructBean) {
                        elem = (OgActorStructBean) ogActorStruct;
                    } else if(ogActorStruct instanceof OgActorStructStub) {
                        elem = new OgActorStructBean(ogActorStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.actor;
        }
    }
    public void setActor(List<OgActorStruct> actor)
    {
        if(actor != null) {
            if(getStub() != null) {
                List<OgActorStruct> stub = new ArrayList<OgActorStruct>();
                for(OgActorStruct ogActorStruct : actor) {
                    OgActorStructStub elem = null;
                    if(ogActorStruct instanceof OgActorStructStub) {
                        elem = (OgActorStructStub) ogActorStruct;
                    } else if(ogActorStruct instanceof OgActorStructBean) {
                        elem = ((OgActorStructBean) ogActorStruct).getStub();
                    } else if(ogActorStruct instanceof OgActorStruct) {
                        elem = new OgActorStructStub(ogActorStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setActor(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.actor = actor;  // ???

                List<OgActorStruct> beans = new ArrayList<OgActorStruct>();
                for(OgActorStruct ogActorStruct : actor) {
                    OgActorStructBean elem = null;
                    if(ogActorStruct != null) {
                        if(ogActorStruct instanceof OgActorStructBean) {
                            elem = (OgActorStructBean) ogActorStruct;
                        } else {
                            elem = new OgActorStructBean(ogActorStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.actor = beans;
            }
        } else {
            this.actor = null;
        }
    }

    public Integer getDuration()
    {
        if(getStub() != null) {
            return getStub().getDuration();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.duration;
        }
    }
    public void setDuration(Integer duration)
    {
        if(getStub() != null) {
            getStub().setDuration(duration);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.duration = duration;
        }
    }

    public List<String> getTag()
    {
        if(getStub() != null) {
            return getStub().getTag();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tag;
        }
    }
    public void setTag(List<String> tag)
    {
        if(getStub() != null) {
            getStub().setTag(tag);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tag = tag;
        }
    }

    public String getReleaseDate()
    {
        if(getStub() != null) {
            return getStub().getReleaseDate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.releaseDate;
        }
    }
    public void setReleaseDate(String releaseDate)
    {
        if(getStub() != null) {
            getStub().setReleaseDate(releaseDate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.releaseDate = releaseDate;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public OgTvShowStub getStub()
    {
        return (OgTvShowStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("director = " + this.director).append(";");
            sb.append("writer = " + this.writer).append(";");
            sb.append("actor = " + this.actor).append(";");
            sb.append("duration = " + this.duration).append(";");
            sb.append("tag = " + this.tag).append(";");
            sb.append("releaseDate = " + this.releaseDate).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = director == null ? 0 : director.hashCode();
            _hash = 31 * _hash + delta;
            delta = writer == null ? 0 : writer.hashCode();
            _hash = 31 * _hash + delta;
            delta = actor == null ? 0 : actor.hashCode();
            _hash = 31 * _hash + delta;
            delta = duration == null ? 0 : duration.hashCode();
            _hash = 31 * _hash + delta;
            delta = tag == null ? 0 : tag.hashCode();
            _hash = 31 * _hash + delta;
            delta = releaseDate == null ? 0 : releaseDate.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
