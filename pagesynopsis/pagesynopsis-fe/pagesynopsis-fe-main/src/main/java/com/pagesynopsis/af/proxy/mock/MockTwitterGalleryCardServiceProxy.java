package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.af.proxy.TwitterGalleryCardServiceProxy;


// MockTwitterGalleryCardServiceProxy is a decorator.
// It can be used as a base class to mock TwitterGalleryCardServiceProxy objects.
public abstract class MockTwitterGalleryCardServiceProxy implements TwitterGalleryCardServiceProxy
{
    private static final Logger log = Logger.getLogger(MockTwitterGalleryCardServiceProxy.class.getName());

    // MockTwitterGalleryCardServiceProxy uses the decorator design pattern.
    private TwitterGalleryCardServiceProxy decoratedProxy;

    public MockTwitterGalleryCardServiceProxy(TwitterGalleryCardServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected TwitterGalleryCardServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(TwitterGalleryCardServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public TwitterGalleryCard getTwitterGalleryCard(String guid) throws BaseException
    {
        return decoratedProxy.getTwitterGalleryCard(guid);
    }

    @Override
    public Object getTwitterGalleryCard(String guid, String field) throws BaseException
    {
        return decoratedProxy.getTwitterGalleryCard(guid, field);       
    }

    @Override
    public List<TwitterGalleryCard> getTwitterGalleryCards(List<String> guids) throws BaseException
    {
        return decoratedProxy.getTwitterGalleryCards(guids);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards() throws BaseException
    {
        return getAllTwitterGalleryCards(null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllTwitterGalleryCards(ordering, offset, count);
        return getAllTwitterGalleryCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterGalleryCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllTwitterGalleryCardKeys(ordering, offset, count);
        return getAllTwitterGalleryCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterGalleryCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterGalleryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        return decoratedProxy.createTwitterGalleryCard(card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
    }

    @Override
    public String createTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        return decoratedProxy.createTwitterGalleryCard(twitterGalleryCard);
    }

    @Override
    public Boolean updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        return decoratedProxy.updateTwitterGalleryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
    }

    @Override
    public Boolean updateTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        return decoratedProxy.updateTwitterGalleryCard(twitterGalleryCard);
    }

    @Override
    public Boolean deleteTwitterGalleryCard(String guid) throws BaseException
    {
        return decoratedProxy.deleteTwitterGalleryCard(guid);
    }

    @Override
    public Boolean deleteTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard) throws BaseException
    {
        String guid = twitterGalleryCard.getGuid();
        return deleteTwitterGalleryCard(guid);
    }

    @Override
    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteTwitterGalleryCards(filter, params, values);
    }

}
