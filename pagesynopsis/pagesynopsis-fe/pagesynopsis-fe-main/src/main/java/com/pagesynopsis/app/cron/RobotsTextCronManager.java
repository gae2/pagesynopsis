package com.pagesynopsis.app.cron;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.fetch.RobotsTextProcessor;
import com.pagesynopsis.app.helper.AudioSetHelper;
import com.pagesynopsis.app.helper.RobotsTextRefreshHelper;
import com.pagesynopsis.app.helper.ImageSetHelper;
import com.pagesynopsis.app.helper.LinkListHelper;
import com.pagesynopsis.app.helper.RobotsTextRefreshHelper;
import com.pagesynopsis.app.helper.RobotsTextFileHelper;
import com.pagesynopsis.app.helper.VideoSetHelper;
import com.pagesynopsis.app.service.AudioSetAppService;
import com.pagesynopsis.app.service.RobotsTextRefreshAppService;
import com.pagesynopsis.app.service.ImageSetAppService;
import com.pagesynopsis.app.service.LinkListAppService;
import com.pagesynopsis.app.service.RobotsTextRefreshAppService;
import com.pagesynopsis.app.service.RobotsTextFileAppService;
import com.pagesynopsis.app.service.RobotsTextRefreshAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.app.service.VideoSetAppService;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.VideoSet;


public class RobotsTextCronManager
{
    private static final Logger log = Logger.getLogger(RobotsTextCronManager.class.getName());   

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private RobotsTextFileAppService robotsTextFileAppService = null;
    private RobotsTextRefreshAppService robotsTextRefreshAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private RobotsTextFileAppService getRobotsTextFileService()
    {
        if(robotsTextFileAppService == null) {
            robotsTextFileAppService = new RobotsTextFileAppService();
        }
        return robotsTextFileAppService;
    }
    private RobotsTextRefreshAppService getRobotsTextRefreshService()
    {
        if(robotsTextRefreshAppService == null) {
            robotsTextRefreshAppService = new RobotsTextRefreshAppService();
        }
        return robotsTextRefreshAppService;
    }
  
        // etc. ...

    
    private RobotsTextCronManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class RobotsTextCronManagerHolder
    {
        private static final RobotsTextCronManager INSTANCE = new RobotsTextCronManager();
    }

    // Singleton method
    public static RobotsTextCronManager getInstance()
    {
        return RobotsTextCronManagerHolder.INSTANCE;
    }

    private void init()
    {
        // TBD: ...
    }
    
    
    
    public int processRobotsTextRefresh()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = RobotsTextRefreshHelper.getInstance().getRobotsTextRefreshKeysForRefreshProcessing(maxCount);
        return processRobotsRefreshes(keys);
    }


    private int processRobotsRefreshes(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    RobotsTextRefresh robotsTextRefresh = RobotsTextRefreshHelper.getInstance().getRobotsTextRefresh(key);
                    log.fine("RobotsTextRefresh retrieved for processing: robotsTextRefresh = " + robotsTextRefresh);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    robotsTextRefresh = RobotsTextProcessor.getInstance().processRobotsTextRefresh(robotsTextRefresh);
                    log.fine("RobotsTextRefresh processed: robotsTextRefresh = " + robotsTextRefresh);
                   
                    // ????
                    // ...
                    Boolean suc = getRobotsTextRefreshService().updateRobotsTextRefresh(robotsTextRefresh);
                    log.info("Refresh request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated fetch record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
    


}
