package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AnchorStructJsBean implements Serializable, Cloneable  //, AnchorStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AnchorStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String id;
    private String name;
    private String href;
    private String hrefUrl;
    private String urlScheme;
    private String rel;
    private String target;
    private String innerHtml;
    private String anchorText;
    private String anchorTitle;
    private String anchorImage;
    private String anchorLegend;
    private String note;

    // Ctors.
    public AnchorStructJsBean()
    {
        //this((String) null);
    }
    public AnchorStructJsBean(String uuid, String id, String name, String href, String hrefUrl, String urlScheme, String rel, String target, String innerHtml, String anchorText, String anchorTitle, String anchorImage, String anchorLegend, String note)
    {
        this.uuid = uuid;
        this.id = id;
        this.name = name;
        this.href = href;
        this.hrefUrl = hrefUrl;
        this.urlScheme = urlScheme;
        this.rel = rel;
        this.target = target;
        this.innerHtml = innerHtml;
        this.anchorText = anchorText;
        this.anchorTitle = anchorTitle;
        this.anchorImage = anchorImage;
        this.anchorLegend = anchorLegend;
        this.note = note;
    }
    public AnchorStructJsBean(AnchorStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setId(bean.getId());
            setName(bean.getName());
            setHref(bean.getHref());
            setHrefUrl(bean.getHrefUrl());
            setUrlScheme(bean.getUrlScheme());
            setRel(bean.getRel());
            setTarget(bean.getTarget());
            setInnerHtml(bean.getInnerHtml());
            setAnchorText(bean.getAnchorText());
            setAnchorTitle(bean.getAnchorTitle());
            setAnchorImage(bean.getAnchorImage());
            setAnchorLegend(bean.getAnchorLegend());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static AnchorStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        AnchorStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(AnchorStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, AnchorStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getHref()
    {
        return this.href;
    }
    public void setHref(String href)
    {
        this.href = href;
    }

    public String getHrefUrl()
    {
        return this.hrefUrl;
    }
    public void setHrefUrl(String hrefUrl)
    {
        this.hrefUrl = hrefUrl;
    }

    public String getUrlScheme()
    {
        return this.urlScheme;
    }
    public void setUrlScheme(String urlScheme)
    {
        this.urlScheme = urlScheme;
    }

    public String getRel()
    {
        return this.rel;
    }
    public void setRel(String rel)
    {
        this.rel = rel;
    }

    public String getTarget()
    {
        return this.target;
    }
    public void setTarget(String target)
    {
        this.target = target;
    }

    public String getInnerHtml()
    {
        return this.innerHtml;
    }
    public void setInnerHtml(String innerHtml)
    {
        this.innerHtml = innerHtml;
    }

    public String getAnchorText()
    {
        return this.anchorText;
    }
    public void setAnchorText(String anchorText)
    {
        this.anchorText = anchorText;
    }

    public String getAnchorTitle()
    {
        return this.anchorTitle;
    }
    public void setAnchorTitle(String anchorTitle)
    {
        this.anchorTitle = anchorTitle;
    }

    public String getAnchorImage()
    {
        return this.anchorImage;
    }
    public void setAnchorImage(String anchorImage)
    {
        this.anchorImage = anchorImage;
    }

    public String getAnchorLegend()
    {
        return this.anchorLegend;
    }
    public void setAnchorLegend(String anchorLegend)
    {
        this.anchorLegend = anchorLegend;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHref() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHrefUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUrlScheme() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTarget() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getInnerHtml() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorImage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorLegend() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("id:null, ");
        sb.append("name:null, ");
        sb.append("href:null, ");
        sb.append("hrefUrl:null, ");
        sb.append("urlScheme:null, ");
        sb.append("rel:null, ");
        sb.append("target:null, ");
        sb.append("innerHtml:null, ");
        sb.append("anchorText:null, ");
        sb.append("anchorTitle:null, ");
        sb.append("anchorImage:null, ");
        sb.append("anchorLegend:null, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("id:");
        if(this.getId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getId()).append("\", ");
        }
        sb.append("name:");
        if(this.getName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getName()).append("\", ");
        }
        sb.append("href:");
        if(this.getHref() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getHref()).append("\", ");
        }
        sb.append("hrefUrl:");
        if(this.getHrefUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getHrefUrl()).append("\", ");
        }
        sb.append("urlScheme:");
        if(this.getUrlScheme() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUrlScheme()).append("\", ");
        }
        sb.append("rel:");
        if(this.getRel() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRel()).append("\", ");
        }
        sb.append("target:");
        if(this.getTarget() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTarget()).append("\", ");
        }
        sb.append("innerHtml:");
        if(this.getInnerHtml() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getInnerHtml()).append("\", ");
        }
        sb.append("anchorText:");
        if(this.getAnchorText() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAnchorText()).append("\", ");
        }
        sb.append("anchorTitle:");
        if(this.getAnchorTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAnchorTitle()).append("\", ");
        }
        sb.append("anchorImage:");
        if(this.getAnchorImage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAnchorImage()).append("\", ");
        }
        sb.append("anchorLegend:");
        if(this.getAnchorLegend() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAnchorLegend()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getId() != null) {
            sb.append("\"id\":").append("\"").append(this.getId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"id\":").append("null, ");
        }
        if(this.getName() != null) {
            sb.append("\"name\":").append("\"").append(this.getName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"name\":").append("null, ");
        }
        if(this.getHref() != null) {
            sb.append("\"href\":").append("\"").append(this.getHref()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"href\":").append("null, ");
        }
        if(this.getHrefUrl() != null) {
            sb.append("\"hrefUrl\":").append("\"").append(this.getHrefUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"hrefUrl\":").append("null, ");
        }
        if(this.getUrlScheme() != null) {
            sb.append("\"urlScheme\":").append("\"").append(this.getUrlScheme()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"urlScheme\":").append("null, ");
        }
        if(this.getRel() != null) {
            sb.append("\"rel\":").append("\"").append(this.getRel()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"rel\":").append("null, ");
        }
        if(this.getTarget() != null) {
            sb.append("\"target\":").append("\"").append(this.getTarget()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"target\":").append("null, ");
        }
        if(this.getInnerHtml() != null) {
            sb.append("\"innerHtml\":").append("\"").append(this.getInnerHtml()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"innerHtml\":").append("null, ");
        }
        if(this.getAnchorText() != null) {
            sb.append("\"anchorText\":").append("\"").append(this.getAnchorText()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"anchorText\":").append("null, ");
        }
        if(this.getAnchorTitle() != null) {
            sb.append("\"anchorTitle\":").append("\"").append(this.getAnchorTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"anchorTitle\":").append("null, ");
        }
        if(this.getAnchorImage() != null) {
            sb.append("\"anchorImage\":").append("\"").append(this.getAnchorImage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"anchorImage\":").append("null, ");
        }
        if(this.getAnchorLegend() != null) {
            sb.append("\"anchorLegend\":").append("\"").append(this.getAnchorLegend()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"anchorLegend\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("id = " + this.id).append(";");
        sb.append("name = " + this.name).append(";");
        sb.append("href = " + this.href).append(";");
        sb.append("hrefUrl = " + this.hrefUrl).append(";");
        sb.append("urlScheme = " + this.urlScheme).append(";");
        sb.append("rel = " + this.rel).append(";");
        sb.append("target = " + this.target).append(";");
        sb.append("innerHtml = " + this.innerHtml).append(";");
        sb.append("anchorText = " + this.anchorText).append(";");
        sb.append("anchorTitle = " + this.anchorTitle).append(";");
        sb.append("anchorImage = " + this.anchorImage).append(";");
        sb.append("anchorLegend = " + this.anchorLegend).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        AnchorStructJsBean cloned = new AnchorStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setId(this.getId());   
        cloned.setName(this.getName());   
        cloned.setHref(this.getHref());   
        cloned.setHrefUrl(this.getHrefUrl());   
        cloned.setUrlScheme(this.getUrlScheme());   
        cloned.setRel(this.getRel());   
        cloned.setTarget(this.getTarget());   
        cloned.setInnerHtml(this.getInnerHtml());   
        cloned.setAnchorText(this.getAnchorText());   
        cloned.setAnchorTitle(this.getAnchorTitle());   
        cloned.setAnchorImage(this.getAnchorImage());   
        cloned.setAnchorLegend(this.getAnchorLegend());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
