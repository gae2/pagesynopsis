package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.DecodedQueryParamStruct;
// import com.pagesynopsis.ws.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;


public class DecodedQueryParamStructProxyUtil
{
    private static final Logger log = Logger.getLogger(DecodedQueryParamStructProxyUtil.class.getName());

    // Static methods only.
    private DecodedQueryParamStructProxyUtil() {}

    public static DecodedQueryParamStructBean convertServerDecodedQueryParamStructBeanToAppBean(DecodedQueryParamStruct serverBean)
    {
        DecodedQueryParamStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new DecodedQueryParamStructBean();
            bean.setParamType(serverBean.getParamType());
            bean.setOriginalString(serverBean.getOriginalString());
            bean.setDecodedString(serverBean.getDecodedString());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.DecodedQueryParamStructBean convertAppDecodedQueryParamStructBeanToServerBean(DecodedQueryParamStruct appBean)
    {
        com.pagesynopsis.ws.bean.DecodedQueryParamStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.DecodedQueryParamStructBean();
            bean.setParamType(appBean.getParamType());
            bean.setOriginalString(appBean.getOriginalString());
            bean.setDecodedString(appBean.getDecodedString());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
