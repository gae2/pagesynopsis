package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterCardMetaService;


// The primary purpose of TwitterCardMetaDummyService is to fake the service api, TwitterCardMetaService.
// It has no real implementation.
public class TwitterCardMetaDummyService implements TwitterCardMetaService
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaDummyService.class.getName());

    public TwitterCardMetaDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterCardMeta related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterCardMeta getTwitterCardMeta(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterCardMeta(): guid = " + guid);
        return null;
    }

    @Override
    public Object getTwitterCardMeta(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterCardMeta(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<TwitterCardMeta> getTwitterCardMetas(List<String> guids) throws BaseException
    {
        log.fine("getTwitterCardMetas()");
        return null;
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas() throws BaseException
    {
        return getAllTwitterCardMetas(null, null, null);
    }


    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterCardMetas(ordering, offset, count, null);
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterCardMetas(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterCardMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterCardMetaKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterCardMetas(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterCardMetaDummyService.findTwitterCardMetas(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterCardMetaDummyService.findTwitterCardMetaKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterCardMetaDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createTwitterCardMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException
    {
        log.finer("createTwitterCardMeta()");
        return null;
    }

    @Override
    public String createTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("createTwitterCardMeta()");
        return null;
    }

    @Override
    public TwitterCardMeta constructTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("constructTwitterCardMeta()");
        return null;
    }

    @Override
    public Boolean updateTwitterCardMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException
    {
        log.finer("updateTwitterCardMeta()");
        return null;
    }
        
    @Override
    public Boolean updateTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("updateTwitterCardMeta()");
        return null;
    }

    @Override
    public TwitterCardMeta refreshTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("refreshTwitterCardMeta()");
        return null;
    }

    @Override
    public Boolean deleteTwitterCardMeta(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTwitterCardMeta(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("deleteTwitterCardMeta()");
        return null;
    }

    // TBD
    @Override
    public Long deleteTwitterCardMetas(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTwitterCardMeta(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterCardMetas(List<TwitterCardMeta> twitterCardMetas) throws BaseException
    {
        log.finer("createTwitterCardMetas()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterCardMetas(List<TwitterCardMeta> twitterCardMetas) throws BaseException
    //{
    //}

}
