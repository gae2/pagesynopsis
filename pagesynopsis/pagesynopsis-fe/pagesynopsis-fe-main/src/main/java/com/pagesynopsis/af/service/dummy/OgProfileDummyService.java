package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgProfileService;


// The primary purpose of OgProfileDummyService is to fake the service api, OgProfileService.
// It has no real implementation.
public class OgProfileDummyService implements OgProfileService
{
    private static final Logger log = Logger.getLogger(OgProfileDummyService.class.getName());

    public OgProfileDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // OgProfile related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgProfile getOgProfile(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgProfile(): guid = " + guid);
        return null;
    }

    @Override
    public Object getOgProfile(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgProfile(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<OgProfile> getOgProfiles(List<String> guids) throws BaseException
    {
        log.fine("getOgProfiles()");
        return null;
    }

    @Override
    public List<OgProfile> getAllOgProfiles() throws BaseException
    {
        return getAllOgProfiles(null, null, null);
    }


    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgProfiles(ordering, offset, count, null);
    }

    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgProfiles(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgProfileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgProfileKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgProfiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgProfileDummyService.findOgProfiles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgProfileDummyService.findOgProfileKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgProfileDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createOgProfile(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        log.finer("createOgProfile()");
        return null;
    }

    @Override
    public String createOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("createOgProfile()");
        return null;
    }

    @Override
    public OgProfile constructOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("constructOgProfile()");
        return null;
    }

    @Override
    public Boolean updateOgProfile(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        log.finer("updateOgProfile()");
        return null;
    }
        
    @Override
    public Boolean updateOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("updateOgProfile()");
        return null;
    }

    @Override
    public OgProfile refreshOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("refreshOgProfile()");
        return null;
    }

    @Override
    public Boolean deleteOgProfile(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgProfile(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("deleteOgProfile()");
        return null;
    }

    // TBD
    @Override
    public Long deleteOgProfiles(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteOgProfile(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgProfiles(List<OgProfile> ogProfiles) throws BaseException
    {
        log.finer("createOgProfiles()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateOgProfiles(List<OgProfile> ogProfiles) throws BaseException
    //{
    //}

}
