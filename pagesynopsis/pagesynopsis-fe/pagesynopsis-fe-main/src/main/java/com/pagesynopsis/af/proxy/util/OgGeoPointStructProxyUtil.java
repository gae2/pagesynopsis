package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgGeoPointStruct;
// import com.pagesynopsis.ws.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.OgGeoPointStructBean;


public class OgGeoPointStructProxyUtil
{
    private static final Logger log = Logger.getLogger(OgGeoPointStructProxyUtil.class.getName());

    // Static methods only.
    private OgGeoPointStructProxyUtil() {}

    public static OgGeoPointStructBean convertServerOgGeoPointStructBeanToAppBean(OgGeoPointStruct serverBean)
    {
        OgGeoPointStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new OgGeoPointStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setLatitude(serverBean.getLatitude());
            bean.setLongitude(serverBean.getLongitude());
            bean.setAltitude(serverBean.getAltitude());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.OgGeoPointStructBean convertAppOgGeoPointStructBeanToServerBean(OgGeoPointStruct appBean)
    {
        com.pagesynopsis.ws.bean.OgGeoPointStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgGeoPointStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setLatitude(appBean.getLatitude());
            bean.setLongitude(appBean.getLongitude());
            bean.setAltitude(appBean.getAltitude());
        }
        return bean;
    }

}
