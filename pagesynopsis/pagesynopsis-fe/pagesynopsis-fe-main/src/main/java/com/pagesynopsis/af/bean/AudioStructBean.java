package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.stub.MediaSourceStructStub;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.stub.AudioStructStub;


// Wrapper class + bean combo.
public class AudioStructBean implements AudioStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AudioStructBean.class.getName());

    // [1] With an embedded object.
    private AudioStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String id;
    private String controls;
    private Boolean autoplayEnabled;
    private Boolean loopEnabled;
    private Boolean preloadEnabled;
    private String remark;
    private MediaSourceStructBean source;
    private MediaSourceStructBean source1;
    private MediaSourceStructBean source2;
    private MediaSourceStructBean source3;
    private MediaSourceStructBean source4;
    private MediaSourceStructBean source5;
    private String note;

    // Ctors.
    public AudioStructBean()
    {
        //this((String) null);
    }
    public AudioStructBean(String uuid, String id, String controls, Boolean autoplayEnabled, Boolean loopEnabled, Boolean preloadEnabled, String remark, MediaSourceStructBean source, MediaSourceStructBean source1, MediaSourceStructBean source2, MediaSourceStructBean source3, MediaSourceStructBean source4, MediaSourceStructBean source5, String note)
    {
        this.uuid = uuid;
        this.id = id;
        this.controls = controls;
        this.autoplayEnabled = autoplayEnabled;
        this.loopEnabled = loopEnabled;
        this.preloadEnabled = preloadEnabled;
        this.remark = remark;
        this.source = source;
        this.source1 = source1;
        this.source2 = source2;
        this.source3 = source3;
        this.source4 = source4;
        this.source5 = source5;
        this.note = note;
    }
    public AudioStructBean(AudioStruct stub)
    {
        if(stub instanceof AudioStructStub) {
            this.stub = (AudioStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setId(stub.getId());   
            setControls(stub.getControls());   
            setAutoplayEnabled(stub.isAutoplayEnabled());   
            setLoopEnabled(stub.isLoopEnabled());   
            setPreloadEnabled(stub.isPreloadEnabled());   
            setRemark(stub.getRemark());   
            MediaSourceStruct source = stub.getSource();
            if(source instanceof MediaSourceStructBean) {
                setSource((MediaSourceStructBean) source);   
            } else {
                setSource(new MediaSourceStructBean(source));   
            }
            MediaSourceStruct source1 = stub.getSource1();
            if(source1 instanceof MediaSourceStructBean) {
                setSource1((MediaSourceStructBean) source1);   
            } else {
                setSource1(new MediaSourceStructBean(source1));   
            }
            MediaSourceStruct source2 = stub.getSource2();
            if(source2 instanceof MediaSourceStructBean) {
                setSource2((MediaSourceStructBean) source2);   
            } else {
                setSource2(new MediaSourceStructBean(source2));   
            }
            MediaSourceStruct source3 = stub.getSource3();
            if(source3 instanceof MediaSourceStructBean) {
                setSource3((MediaSourceStructBean) source3);   
            } else {
                setSource3(new MediaSourceStructBean(source3));   
            }
            MediaSourceStruct source4 = stub.getSource4();
            if(source4 instanceof MediaSourceStructBean) {
                setSource4((MediaSourceStructBean) source4);   
            } else {
                setSource4(new MediaSourceStructBean(source4));   
            }
            MediaSourceStruct source5 = stub.getSource5();
            if(source5 instanceof MediaSourceStructBean) {
                setSource5((MediaSourceStructBean) source5);   
            } else {
                setSource5(new MediaSourceStructBean(source5));   
            }
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getId()
    {
        if(getStub() != null) {
            return getStub().getId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.id;
        }
    }
    public void setId(String id)
    {
        if(getStub() != null) {
            getStub().setId(id);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.id = id;
        }
    }

    public String getControls()
    {
        if(getStub() != null) {
            return getStub().getControls();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.controls;
        }
    }
    public void setControls(String controls)
    {
        if(getStub() != null) {
            getStub().setControls(controls);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.controls = controls;
        }
    }

    public Boolean isAutoplayEnabled()
    {
        if(getStub() != null) {
            return getStub().isAutoplayEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.autoplayEnabled;
        }
    }
    public void setAutoplayEnabled(Boolean autoplayEnabled)
    {
        if(getStub() != null) {
            getStub().setAutoplayEnabled(autoplayEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.autoplayEnabled = autoplayEnabled;
        }
    }

    public Boolean isLoopEnabled()
    {
        if(getStub() != null) {
            return getStub().isLoopEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.loopEnabled;
        }
    }
    public void setLoopEnabled(Boolean loopEnabled)
    {
        if(getStub() != null) {
            getStub().setLoopEnabled(loopEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.loopEnabled = loopEnabled;
        }
    }

    public Boolean isPreloadEnabled()
    {
        if(getStub() != null) {
            return getStub().isPreloadEnabled();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.preloadEnabled;
        }
    }
    public void setPreloadEnabled(Boolean preloadEnabled)
    {
        if(getStub() != null) {
            getStub().setPreloadEnabled(preloadEnabled);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.preloadEnabled = preloadEnabled;
        }
    }

    public String getRemark()
    {
        if(getStub() != null) {
            return getStub().getRemark();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.remark;
        }
    }
    public void setRemark(String remark)
    {
        if(getStub() != null) {
            getStub().setRemark(remark);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.remark = remark;
        }
    }

    public MediaSourceStruct getSource()
    {  
        if(getStub() != null) {
            // Note the object type.
            MediaSourceStruct _stub_field = getStub().getSource();
            if(_stub_field == null) {
                return null;
            } else {
                return new MediaSourceStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.source;
        }
    }
    public void setSource(MediaSourceStruct source)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setSource(source);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(source == null) {
                this.source = null;
            } else {
                if(source instanceof MediaSourceStructBean) {
                    this.source = (MediaSourceStructBean) source;
                } else {
                    this.source = new MediaSourceStructBean(source);
                }
            }
        }
    }

    public MediaSourceStruct getSource1()
    {  
        if(getStub() != null) {
            // Note the object type.
            MediaSourceStruct _stub_field = getStub().getSource1();
            if(_stub_field == null) {
                return null;
            } else {
                return new MediaSourceStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.source1;
        }
    }
    public void setSource1(MediaSourceStruct source1)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setSource1(source1);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(source1 == null) {
                this.source1 = null;
            } else {
                if(source1 instanceof MediaSourceStructBean) {
                    this.source1 = (MediaSourceStructBean) source1;
                } else {
                    this.source1 = new MediaSourceStructBean(source1);
                }
            }
        }
    }

    public MediaSourceStruct getSource2()
    {  
        if(getStub() != null) {
            // Note the object type.
            MediaSourceStruct _stub_field = getStub().getSource2();
            if(_stub_field == null) {
                return null;
            } else {
                return new MediaSourceStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.source2;
        }
    }
    public void setSource2(MediaSourceStruct source2)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setSource2(source2);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(source2 == null) {
                this.source2 = null;
            } else {
                if(source2 instanceof MediaSourceStructBean) {
                    this.source2 = (MediaSourceStructBean) source2;
                } else {
                    this.source2 = new MediaSourceStructBean(source2);
                }
            }
        }
    }

    public MediaSourceStruct getSource3()
    {  
        if(getStub() != null) {
            // Note the object type.
            MediaSourceStruct _stub_field = getStub().getSource3();
            if(_stub_field == null) {
                return null;
            } else {
                return new MediaSourceStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.source3;
        }
    }
    public void setSource3(MediaSourceStruct source3)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setSource3(source3);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(source3 == null) {
                this.source3 = null;
            } else {
                if(source3 instanceof MediaSourceStructBean) {
                    this.source3 = (MediaSourceStructBean) source3;
                } else {
                    this.source3 = new MediaSourceStructBean(source3);
                }
            }
        }
    }

    public MediaSourceStruct getSource4()
    {  
        if(getStub() != null) {
            // Note the object type.
            MediaSourceStruct _stub_field = getStub().getSource4();
            if(_stub_field == null) {
                return null;
            } else {
                return new MediaSourceStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.source4;
        }
    }
    public void setSource4(MediaSourceStruct source4)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setSource4(source4);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(source4 == null) {
                this.source4 = null;
            } else {
                if(source4 instanceof MediaSourceStructBean) {
                    this.source4 = (MediaSourceStructBean) source4;
                } else {
                    this.source4 = new MediaSourceStructBean(source4);
                }
            }
        }
    }

    public MediaSourceStruct getSource5()
    {  
        if(getStub() != null) {
            // Note the object type.
            MediaSourceStruct _stub_field = getStub().getSource5();
            if(_stub_field == null) {
                return null;
            } else {
                return new MediaSourceStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.source5;
        }
    }
    public void setSource5(MediaSourceStruct source5)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setSource5(source5);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(source5 == null) {
                this.source5 = null;
            } else {
                if(source5 instanceof MediaSourceStructBean) {
                    this.source5 = (MediaSourceStructBean) source5;
                } else {
                    this.source5 = new MediaSourceStructBean(source5);
                }
            }
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getControls() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isAutoplayEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isLoopEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isPreloadEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRemark() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource3() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource4() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource5() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public AudioStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(AudioStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("id = " + this.id).append(";");
            sb.append("controls = " + this.controls).append(";");
            sb.append("autoplayEnabled = " + this.autoplayEnabled).append(";");
            sb.append("loopEnabled = " + this.loopEnabled).append(";");
            sb.append("preloadEnabled = " + this.preloadEnabled).append(";");
            sb.append("remark = " + this.remark).append(";");
            sb.append("source = " + this.source).append(";");
            sb.append("source1 = " + this.source1).append(";");
            sb.append("source2 = " + this.source2).append(";");
            sb.append("source3 = " + this.source3).append(";");
            sb.append("source4 = " + this.source4).append(";");
            sb.append("source5 = " + this.source5).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = id == null ? 0 : id.hashCode();
            _hash = 31 * _hash + delta;
            delta = controls == null ? 0 : controls.hashCode();
            _hash = 31 * _hash + delta;
            delta = autoplayEnabled == null ? 0 : autoplayEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = loopEnabled == null ? 0 : loopEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = preloadEnabled == null ? 0 : preloadEnabled.hashCode();
            _hash = 31 * _hash + delta;
            delta = remark == null ? 0 : remark.hashCode();
            _hash = 31 * _hash + delta;
            delta = source == null ? 0 : source.hashCode();
            _hash = 31 * _hash + delta;
            delta = source1 == null ? 0 : source1.hashCode();
            _hash = 31 * _hash + delta;
            delta = source2 == null ? 0 : source2.hashCode();
            _hash = 31 * _hash + delta;
            delta = source3 == null ? 0 : source3.hashCode();
            _hash = 31 * _hash + delta;
            delta = source4 == null ? 0 : source4.hashCode();
            _hash = 31 * _hash + delta;
            delta = source5 == null ? 0 : source5.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
