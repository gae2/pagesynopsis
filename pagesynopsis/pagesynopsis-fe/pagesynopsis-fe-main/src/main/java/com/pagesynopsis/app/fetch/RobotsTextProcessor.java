package com.pagesynopsis.app.fetch;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.app.helper.RobotsTextFileHelper;
import com.pagesynopsis.app.service.RobotsTextFileAppService;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.util.RobotsTextConstants;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.util.CommonUtil;


// Note: HTMLUnit API.
// http://htmlunit.sourceforge.net/apidocs/index.html
public class RobotsTextProcessor
{
    private static final Logger log = Logger.getLogger(RobotsTextProcessor.class.getName());   
    
    
    private RobotsTextProcessor()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class RobotsTextProcessorHolder
    {
        private static final RobotsTextProcessor INSTANCE = new RobotsTextProcessor();
    }

    // Singleton method
    public static RobotsTextProcessor getInstance()
    {
        return RobotsTextProcessorHolder.INSTANCE;
    }

    private void init()
    {
        // TBD:
        // ...
    }

    // TBD: Is this safe for concurrent calls??
    private RobotsTextFileAppService fetchRequestAppService = null;
    private RobotsTextFileAppService getRobotsTextFileService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new RobotsTextFileAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...

    
    public RobotsTextFile processRobotsTextFetch(String siteUrl) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processRobotsTextFetch() called with siteUrl = " + siteUrl);

        if(siteUrl == null || siteUrl.isEmpty()) {
            log.warning("siteUrl is not set.");
            throw new BadRequestException("siteUrl is not set.");
        }
        // TBD: siteUrl validation???
        if( !(siteUrl.startsWith("http://") || siteUrl.startsWith("https://")) ) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid siteUrl = " + siteUrl);
            throw new BadRequestException("Invalid siteUrl = " + siteUrl);
        }
        // TBD: Pick domain part only?
        //      Or, throw error if includes path???
        // ????
        // We store siteUrl without robots.txt part....
        if(siteUrl.endsWith("robots.txt")) {
            siteUrl = siteUrl.substring(0, siteUrl.lastIndexOf("robots.txt"));
            if(log.isLoggable(Level.INFO)) log.info("siteUrl modified: " + siteUrl);
        }
        // ...
        RobotsTextFileBean robotsTextFile = new RobotsTextFileBean();
        robotsTextFile.setGuid(GUID.generate());
        robotsTextFile.setSiteUrl(siteUrl);
        return processRobotsTextFile(robotsTextFile);
    }
    // Process or "re"-process...
    public RobotsTextFile processRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("BEGIN: processRobotsTextFile() called with robotsTextFile = " + robotsTextFile);

        if(robotsTextFile == null) {
            log.warning("robotsTextFile is null.");
            throw new BadRequestException("robotsTextFile is null.");
        }

        // TargetUrl/input cannot be null
        String siteUrl = robotsTextFile.getSiteUrl();
        if(siteUrl == null || siteUrl.isEmpty()) {
            log.warning("siteUrl is not set.");
            throw new BadRequestException("siteUrl is not set.");
        }
        
        String targetUrl = null;
        if(siteUrl.endsWith("/")) {
            targetUrl = siteUrl + "robots.txt";
        } else {
            targetUrl = siteUrl + "/robots.txt";
        }
        // ...

        URL robotsURL = null;
        try {
            robotsURL = new URL(targetUrl);
        } catch (MalformedURLException e) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid targetUrl = " + targetUrl);
            throw new BadRequestException("Invalid targetUrl = " + targetUrl, e);
        }

        
        // TBD:
        // What about existing robotsTextFile.getContent() ???
        // ...
        
        
        String content = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(robotsURL.openStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                // ...
                sb.append(line).append("\n");
            }
            content = sb.toString();
            reader.close();
        // } catch (IOException e) {
        } catch (Exception e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to get robots.txt from targetUrl = " + targetUrl, e);
            throw new InternalServerErrorException("Failed to get robots.txt from targetUrl = " + targetUrl, e);
        }

        if(content == null) {
            // ???
            // what to do???
            if(log.isLoggable(Level.WARNING)) log.warning("Failed to get content from targetUrl = " + targetUrl);
            throw new InternalServerErrorException("Failed to get content from targetUrl = " + targetUrl);
        }
        
        String contentHash = robotsTextFile.getContentHash();
        if(contentHash == null || contentHash.isEmpty()) {
            // ignore
        }
        
        String newContentHash = null;
        try {
            newContentHash = CommonUtil.SHA1(content);
        } catch(Exception e) {
            // ignore
            if(log.isLoggable(Level.INFO)) log.info("Failed to compute content hash.");
        }

        long now = System.currentTimeMillis();
        if(newContentHash != null && newContentHash.equals(contentHash)) {
            // No need to update.
            ((RobotsTextFileBean) robotsTextFile).setLastCheckedTime(now);            
        } else {
            RobotsTextFileBean newRobotsTextFile = RobotsTextUtil.parseRobotsText(content, siteUrl);
            if(newRobotsTextFile != null) {
                newRobotsTextFile.setGuid(robotsTextFile.getGuid());
                newRobotsTextFile.setCreatedTime(robotsTextFile.getCreatedTime());
                newRobotsTextFile.setModifiedTime(robotsTextFile.getModifiedTime());
                // newRobotsTextFile.setSiteUrl(siteUrl);
                // newRobotsTextFile.setContent(content);
                if(newContentHash != null) {
                    newRobotsTextFile.setContentHash(newContentHash);
                }
                newRobotsTextFile.setLastCheckedTime(now);
                if(newContentHash == null || ! newContentHash.equals(contentHash)) {
                    newRobotsTextFile.setLastUpdatedTime(now);
                }
                // etc...

                // Then swap!
                robotsTextFile = newRobotsTextFile;
            } else {
                // ???
                // robotsTextFile = null; // ????
            }
        }
        
        log.finer("END: processRobotsTextFile()");
        return robotsTextFile;
    }

    
    public RobotsTextRefresh processRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processRobotsTextRefresh() called with robotsTextRefresh = " + robotsTextRefresh);
        if(robotsTextRefresh == null) {
            log.warning("robotsTextRefresh is null.");
            throw new BadRequestException("robotsTextRefresh is null.");
        }
        
        long now = System.currentTimeMillis();
        Long expirationTime = robotsTextRefresh.getExpirationTime();
        if(expirationTime != null && expirationTime < now) {
            // No more refreshing for this....
            ((RobotsTextRefreshBean) robotsTextRefresh).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
            return robotsTextRefresh;
        }

        String robotsTextFileGuid = robotsTextRefresh.getRobotsTextFile();
        if(robotsTextFileGuid == null) {
            log.warning("robotsTextFileGuid is null.");
            throw new BadRequestException("robotsTextFileGuid is null.");
        }
        
        RobotsTextFile robotsTextFile = RobotsTextFileHelper.getInstance().getRobotsTextFile(robotsTextFileGuid);
        if(robotsTextFile == null) {
            // How did this happen???
            log.warning("Failed to find robotsTextFile for robotsTextFileGuid = " + robotsTextFileGuid);
            throw new InternalServerErrorException("Failed to find robotsTextFile for robotsTextFileGuid = " + robotsTextFileGuid);            
        }
        
        // OK, reprocesss....
        robotsTextFile = processRobotsTextFile(robotsTextFile);
        if(robotsTextFile != null) {
            // Then, save it...
            robotsTextFile = getRobotsTextFileService().refreshRobotsTextFile(robotsTextFile);
        }

        if(robotsTextFile != null) {
            Integer refreshInterval = robotsTextRefresh.getRefreshInterval();
            if(refreshInterval == null || refreshInterval <= 0) {
                refreshInterval = RobotsTextConstants.REFRESH_INTERVAL;
                ((RobotsTextRefreshBean) robotsTextRefresh).setRefreshInterval(refreshInterval);
            }
            long nextCheckedTime = now + refreshInterval * 1000L;
            ((RobotsTextRefreshBean) robotsTextRefresh).setLastCheckedTime(now);
            ((RobotsTextRefreshBean) robotsTextRefresh).setNextCheckedTime(nextCheckedTime);
            ((RobotsTextRefreshBean) robotsTextRefresh).setResult("Succeeded");
        } else {
            // ???
            ((RobotsTextRefreshBean) robotsTextRefresh).setResult("Failed");  // ???            
        }

        return robotsTextRefresh;
    }
    
    

}
