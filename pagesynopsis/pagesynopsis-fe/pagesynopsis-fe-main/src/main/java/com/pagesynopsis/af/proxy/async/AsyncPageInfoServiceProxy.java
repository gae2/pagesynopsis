package com.pagesynopsis.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.OgVideoStub;
import com.pagesynopsis.ws.stub.OgVideoListStub;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.TwitterProductCardListStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardListStub;
import com.pagesynopsis.ws.stub.OgBlogStub;
import com.pagesynopsis.ws.stub.OgBlogListStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardListStub;
import com.pagesynopsis.ws.stub.UrlStructStub;
import com.pagesynopsis.ws.stub.UrlStructListStub;
import com.pagesynopsis.ws.stub.ImageStructStub;
import com.pagesynopsis.ws.stub.ImageStructListStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardListStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardListStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgTvShowListStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgBookListStub;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgWebsiteListStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.stub.OgMovieListStub;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.TwitterAppCardListStub;
import com.pagesynopsis.ws.stub.AnchorStructStub;
import com.pagesynopsis.ws.stub.AnchorStructListStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructListStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgArticleListStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeListStub;
import com.pagesynopsis.ws.stub.AudioStructStub;
import com.pagesynopsis.ws.stub.AudioStructListStub;
import com.pagesynopsis.ws.stub.VideoStructStub;
import com.pagesynopsis.ws.stub.VideoStructListStub;
import com.pagesynopsis.ws.stub.OgProfileStub;
import com.pagesynopsis.ws.stub.OgProfileListStub;
import com.pagesynopsis.ws.stub.PageInfoStub;
import com.pagesynopsis.ws.stub.PageInfoListStub;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.PageInfoBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.PageInfoService;
import com.pagesynopsis.af.proxy.PageInfoServiceProxy;
import com.pagesynopsis.af.proxy.remote.RemotePageInfoServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncPageInfoServiceProxy extends BaseAsyncServiceProxy implements PageInfoServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncPageInfoServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemotePageInfoServiceProxy remoteProxy;

    public AsyncPageInfoServiceProxy()
    {
        remoteProxy = new RemotePageInfoServiceProxy();
    }

    @Override
    public PageInfo getPageInfo(String guid) throws BaseException
    {
        return remoteProxy.getPageInfo(guid);
    }

    @Override
    public Object getPageInfo(String guid, String field) throws BaseException
    {
        return remoteProxy.getPageInfo(guid, field);       
    }

    @Override
    public List<PageInfo> getPageInfos(List<String> guids) throws BaseException
    {
        return remoteProxy.getPageInfos(guids);
    }

    @Override
    public List<PageInfo> getAllPageInfos() throws BaseException
    {
        return getAllPageInfos(null, null, null);
    }

    @Override
    public List<PageInfo> getAllPageInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllPageInfos(ordering, offset, count);
        return getAllPageInfos(ordering, offset, count, null);
    }

    @Override
    public List<PageInfo> getAllPageInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllPageInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllPageInfoKeys(ordering, offset, count);
        return getAllPageInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllPageInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<PageInfo> findPageInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findPageInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<PageInfo> findPageInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findPageInfos(filter, ordering, params, values, grouping, unique, offset, count);
        return findPageInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<PageInfo> findPageInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findPageInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createPageInfo(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String pageAuthor, String pageDescription, String favicon, String faviconUrl) throws BaseException
    {
        PageInfoBean bean = new PageInfoBean(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, pageAuthor, pageDescription, favicon, faviconUrl);
        return createPageInfo(bean);        
    }

    @Override
    public String createPageInfo(PageInfo pageInfo) throws BaseException
    {
        log.finer("BEGIN");

        String guid = pageInfo.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((PageInfoBean) pageInfo).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreatePageInfo-" + guid;
        String taskName = "RsCreatePageInfo-" + guid + "-" + (new Date()).getTime();
        PageInfoStub stub = MarshalHelper.convertPageInfoToStub(pageInfo);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(PageInfoStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = pageInfo.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    PageInfoStub dummyStub = new PageInfoStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createPageInfo(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "pageInfos/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createPageInfo(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "pageInfos/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updatePageInfo(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String pageAuthor, String pageDescription, String favicon, String faviconUrl) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "PageInfo guid is invalid.");
        	throw new BaseException("PageInfo guid is invalid.");
        }
        PageInfoBean bean = new PageInfoBean(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, pageAuthor, pageDescription, favicon, faviconUrl);
        return updatePageInfo(bean);        
    }

    @Override
    public Boolean updatePageInfo(PageInfo pageInfo) throws BaseException
    {
        log.finer("BEGIN");

        String guid = pageInfo.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "PageInfo object is invalid.");
        	throw new BaseException("PageInfo object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdatePageInfo-" + guid;
        String taskName = "RsUpdatePageInfo-" + guid + "-" + (new Date()).getTime();
        PageInfoStub stub = MarshalHelper.convertPageInfoToStub(pageInfo);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(PageInfoStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = pageInfo.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    PageInfoStub dummyStub = new PageInfoStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updatePageInfo(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "pageInfos/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updatePageInfo(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "pageInfos/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deletePageInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeletePageInfo-" + guid;
        String taskName = "RsDeletePageInfo-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "pageInfos/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deletePageInfo(PageInfo pageInfo) throws BaseException
    {
        String guid = pageInfo.getGuid();
        return deletePageInfo(guid);
    }

    @Override
    public Long deletePageInfos(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deletePageInfos(filter, params, values);
    }

}
