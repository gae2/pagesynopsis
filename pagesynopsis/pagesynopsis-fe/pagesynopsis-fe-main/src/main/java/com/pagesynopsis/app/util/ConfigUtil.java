package com.pagesynopsis.app.util;

import java.util.logging.Logger;

import com.pagesynopsis.af.config.Config;
import com.pagesynopsis.common.AppBrand;


// For fetching system default values.
// User-specific default values/settings should be read from somewhere else...
public class ConfigUtil
{
    private static final Logger log = Logger.getLogger(ConfigUtil.class.getName());
    private static Config sConfig = null;

    // temporary
    private static final int DEFAULT_PAGER_PAGE_SIZE = 10;

    // Config keys... 
    private static final String KEY_DEVELFEATURE_ENABLED = "pagesynopsisapp.develfeature.enabled";
    private static final String KEY_BETAFEATURE_ENABLED = "pagesynopsisapp.betafeature.enabled";
    private static final String KEY_PAGER_PAGESIZE = "pagesynopsisapp.pager.pagesize";

    private static final String KEY_APPLICATION_BRAND = "pagesynopsisapp.application.brand";
    // ...
    // etc...

    // "Cache" values.  (TBD: Convert types to enums????)
    private static Boolean sDevelFeatureEnabled = null;
    private static Boolean sBetaFeatureEnabled = null;
    private static String sApplicationBrand = null;
    private static Integer sPagerPageSize = null;
    // ...
 
    
    // Static methods only.
    private ConfigUtil() {}

    
    public static int getDefaultPagerPageSize()
    {
        return DEFAULT_PAGER_PAGE_SIZE;
    }

    
    public static Boolean isDevelFeatureEnabled()
    {
        if(sDevelFeatureEnabled == null) {
            sDevelFeatureEnabled = Config.getInstance().getBoolean(KEY_DEVELFEATURE_ENABLED, false);
        }
        return sDevelFeatureEnabled;
    }
    public static Boolean isBetaFeatureEnabled()
    {
        if(sBetaFeatureEnabled == null) {
            // Do this in a higher level in the stack (e.g, DebugUtil)...
            //if(isDevelFeatureEnabled()) {
            //    sBetaFeatureEnabled = true;
            //} else {
               // sBetaFeatureEnabled = Config.getInstance().getBoolean(KEY_BETAFEATURE_ENABLED, false);
               sBetaFeatureEnabled = Config.getInstance().getBoolean(KEY_BETAFEATURE_ENABLED, isDevelFeatureEnabled());  // ???
            //}
        }
        return sBetaFeatureEnabled;
    }


    // App "brand" 
    public static String getApplicationBrand()
    {
        if(sApplicationBrand == null) {
            sApplicationBrand = Config.getInstance().getString(KEY_APPLICATION_BRAND, AppBrand.getDefaultBrand());
        }
        return sApplicationBrand;
    }

    // List page size.
    public static Integer getPagerPageSize()
    {
        if(sPagerPageSize == null) {
            sPagerPageSize = Config.getInstance().getInteger(KEY_PAGER_PAGESIZE, getDefaultPagerPageSize());
        }
        return sPagerPageSize;
    }

    
}
