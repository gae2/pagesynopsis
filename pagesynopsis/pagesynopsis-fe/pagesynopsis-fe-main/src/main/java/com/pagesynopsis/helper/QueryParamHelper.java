package com.pagesynopsis.helper;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.ws.core.GUID;


public class QueryParamHelper
{
    private static final Logger log = Logger.getLogger(QueryParamHelper.class.getName());

    private QueryParamHelper() {}


    // Initialization-on-demand holder.
    private static final class QueryParamHelperHolder
    {
        private static final QueryParamHelper INSTANCE = new QueryParamHelper();
    }

    // Singleton method
    public static QueryParamHelper getInstance()
    {
        return QueryParamHelperHolder.INSTANCE;
    }

    public List<KeyValuePairStructJsBean> parseQueryString(String query)
    {
        if(query == null) {
            return null;
        }
        List<KeyValuePairStructJsBean> parameters = new ArrayList<KeyValuePairStructJsBean>();
        if(query.isEmpty()) {
            return parameters;
        }
        
        String[] parametersEncoded = query.split("&");
        for(String pe : parametersEncoded) {
            String[] pair = pe.split("=", 2);
            try {
                String key = URLDecoder.decode(pair[0], "UTF-8");
                String value = "";
                if(pair.length > 1) {
                    value = URLDecoder.decode(pair[1], "UTF-8");
                }
                KeyValuePairStructJsBean pairBean = new KeyValuePairStructJsBean();
                pairBean.setKey(key);
                pairBean.setValue(value);
                pairBean.setUuid(GUID.generate());
                parameters.add(pairBean);
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Failed to url decode a query param, " + pe, e);
                // ignore, and continue.
            }
        }

        return parameters;
    }
    

}
