package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgActorStruct;
// import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;


public class OgActorStructProxyUtil
{
    private static final Logger log = Logger.getLogger(OgActorStructProxyUtil.class.getName());

    // Static methods only.
    private OgActorStructProxyUtil() {}

    public static OgActorStructBean convertServerOgActorStructBeanToAppBean(OgActorStruct serverBean)
    {
        OgActorStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new OgActorStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setProfile(serverBean.getProfile());
            bean.setRole(serverBean.getRole());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.OgActorStructBean convertAppOgActorStructBeanToServerBean(OgActorStruct appBean)
    {
        com.pagesynopsis.ws.bean.OgActorStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgActorStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setProfile(appBean.getProfile());
            bean.setRole(appBean.getRole());
        }
        return bean;
    }

}
