package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.stub.OpenGraphMetaStub;
import com.pagesynopsis.ws.stub.OpenGraphMetaListStub;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OpenGraphMetaBean;
import com.pagesynopsis.af.resource.OpenGraphMetaResource;
import com.pagesynopsis.af.resource.util.UrlStructResourceUtil;
import com.pagesynopsis.af.resource.util.ImageStructResourceUtil;
import com.pagesynopsis.af.resource.util.AnchorStructResourceUtil;
import com.pagesynopsis.af.resource.util.KeyValuePairStructResourceUtil;
import com.pagesynopsis.af.resource.util.AudioStructResourceUtil;
import com.pagesynopsis.af.resource.util.VideoStructResourceUtil;


// MockOpenGraphMetaResource is a decorator.
// It can be used as a base class to mock OpenGraphMetaResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/openGraphMetas/")
public abstract class MockOpenGraphMetaResource implements OpenGraphMetaResource
{
    private static final Logger log = Logger.getLogger(MockOpenGraphMetaResource.class.getName());

    // MockOpenGraphMetaResource uses the decorator design pattern.
    private OpenGraphMetaResource decoratedResource;

    public MockOpenGraphMetaResource(OpenGraphMetaResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected OpenGraphMetaResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(OpenGraphMetaResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllOpenGraphMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOpenGraphMetas(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOpenGraphMetaKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOpenGraphMetasAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findOpenGraphMetasAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getOpenGraphMetaAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getOpenGraphMetaAsHtml(guid);
//    }

    @Override
    public Response getOpenGraphMeta(String guid) throws BaseResourceException
    {
        return decoratedResource.getOpenGraphMeta(guid);
    }

    @Override
    public Response getOpenGraphMetaAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getOpenGraphMetaAsJsonp(guid, callback);
    }

    @Override
    public Response getOpenGraphMeta(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getOpenGraphMeta(guid, field);
    }

    // TBD
    @Override
    public Response constructOpenGraphMeta(OpenGraphMetaStub openGraphMeta) throws BaseResourceException
    {
        return decoratedResource.constructOpenGraphMeta(openGraphMeta);
    }

    @Override
    public Response createOpenGraphMeta(OpenGraphMetaStub openGraphMeta) throws BaseResourceException
    {
        return decoratedResource.createOpenGraphMeta(openGraphMeta);
    }

//    @Override
//    public Response createOpenGraphMeta(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createOpenGraphMeta(formParams);
//    }

    // TBD
    @Override
    public Response refreshOpenGraphMeta(String guid, OpenGraphMetaStub openGraphMeta) throws BaseResourceException
    {
        return decoratedResource.refreshOpenGraphMeta(guid, openGraphMeta);
    }

    @Override
    public Response updateOpenGraphMeta(String guid, OpenGraphMetaStub openGraphMeta) throws BaseResourceException
    {
        return decoratedResource.updateOpenGraphMeta(guid, openGraphMeta);
    }

    @Override
    public Response updateOpenGraphMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<String> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, String ogProfile, String ogWebsite, String ogBlog, String ogArticle, String ogBook, String ogVideo, String ogMovie, String ogTvShow, String ogTvEpisode)
    {
        return decoratedResource.updateOpenGraphMeta(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfile, ogWebsite, ogBlog, ogArticle, ogBook, ogVideo, ogMovie, ogTvShow, ogTvEpisode);
    }

//    @Override
//    public Response updateOpenGraphMeta(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateOpenGraphMeta(guid, formParams);
//    }

    @Override
    public Response deleteOpenGraphMeta(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteOpenGraphMeta(guid);
    }

    @Override
    public Response deleteOpenGraphMetas(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteOpenGraphMetas(filter, params, values);
    }


// TBD ....
    @Override
    public Response createOpenGraphMetas(OpenGraphMetaListStub openGraphMetas) throws BaseResourceException
    {
        return decoratedResource.createOpenGraphMetas(openGraphMetas);
    }


}
