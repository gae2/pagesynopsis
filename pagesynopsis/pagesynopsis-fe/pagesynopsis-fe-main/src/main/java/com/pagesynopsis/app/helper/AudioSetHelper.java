package com.pagesynopsis.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.service.AudioSetAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.AudioSet;


public class AudioSetHelper
{
    private static final Logger log = Logger.getLogger(AudioSetHelper.class.getName());

    private AudioSetHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private AudioSetAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private AudioSetAppService getAudioSetService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new AudioSetAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class AudioSetHelperHolder
    {
        private static final AudioSetHelper INSTANCE = new AudioSetHelper();
    }

    // Singleton method
    public static AudioSetHelper getInstance()
    {
        return AudioSetHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public AudioSet getAudioSet(String guid) 
    {
        AudioSet message = null;
        try {
            message = getAudioSetService().getAudioSet(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
    
    // TBD:
    // Check the pageTitle field as well....  ????
    // ...
    

    public List<String> getAudioSetKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getAudioSetService().findAudioSetKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    public List<AudioSet> getAudioSetsForRefreshProcessing(Integer maxCount)
    {
        List<AudioSet> audioSets = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            audioSets = getAudioSetService().findAudioSets(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return audioSets;
    }


}
