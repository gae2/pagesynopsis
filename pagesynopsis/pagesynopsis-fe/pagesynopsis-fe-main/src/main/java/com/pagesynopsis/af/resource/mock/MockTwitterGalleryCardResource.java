package com.pagesynopsis.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardListStub;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.resource.TwitterGalleryCardResource;
import com.pagesynopsis.af.resource.util.TwitterCardAppInfoResourceUtil;
import com.pagesynopsis.af.resource.util.TwitterCardProductDataResourceUtil;


// MockTwitterGalleryCardResource is a decorator.
// It can be used as a base class to mock TwitterGalleryCardResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/twitterGalleryCards/")
public abstract class MockTwitterGalleryCardResource implements TwitterGalleryCardResource
{
    private static final Logger log = Logger.getLogger(MockTwitterGalleryCardResource.class.getName());

    // MockTwitterGalleryCardResource uses the decorator design pattern.
    private TwitterGalleryCardResource decoratedResource;

    public MockTwitterGalleryCardResource(TwitterGalleryCardResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected TwitterGalleryCardResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(TwitterGalleryCardResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllTwitterGalleryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterGalleryCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterGalleryCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterGalleryCardsAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findTwitterGalleryCardsAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getTwitterGalleryCardAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getTwitterGalleryCardAsHtml(guid);
//    }

    @Override
    public Response getTwitterGalleryCard(String guid) throws BaseResourceException
    {
        return decoratedResource.getTwitterGalleryCard(guid);
    }

    @Override
    public Response getTwitterGalleryCardAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getTwitterGalleryCardAsJsonp(guid, callback);
    }

    @Override
    public Response getTwitterGalleryCard(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getTwitterGalleryCard(guid, field);
    }

    // TBD
    @Override
    public Response constructTwitterGalleryCard(TwitterGalleryCardStub twitterGalleryCard) throws BaseResourceException
    {
        return decoratedResource.constructTwitterGalleryCard(twitterGalleryCard);
    }

    @Override
    public Response createTwitterGalleryCard(TwitterGalleryCardStub twitterGalleryCard) throws BaseResourceException
    {
        return decoratedResource.createTwitterGalleryCard(twitterGalleryCard);
    }

//    @Override
//    public Response createTwitterGalleryCard(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createTwitterGalleryCard(formParams);
//    }

    // TBD
    @Override
    public Response refreshTwitterGalleryCard(String guid, TwitterGalleryCardStub twitterGalleryCard) throws BaseResourceException
    {
        return decoratedResource.refreshTwitterGalleryCard(guid, twitterGalleryCard);
    }

    @Override
    public Response updateTwitterGalleryCard(String guid, TwitterGalleryCardStub twitterGalleryCard) throws BaseResourceException
    {
        return decoratedResource.updateTwitterGalleryCard(guid, twitterGalleryCard);
    }

    @Override
    public Response updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3)
    {
        return decoratedResource.updateTwitterGalleryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
    }

//    @Override
//    public Response updateTwitterGalleryCard(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateTwitterGalleryCard(guid, formParams);
//    }

    @Override
    public Response deleteTwitterGalleryCard(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterGalleryCard(guid);
    }

    @Override
    public Response deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterGalleryCards(filter, params, values);
    }


// TBD ....
    @Override
    public Response createTwitterGalleryCards(TwitterGalleryCardListStub twitterGalleryCards) throws BaseResourceException
    {
        return decoratedResource.createTwitterGalleryCards(twitterGalleryCards);
    }


}
