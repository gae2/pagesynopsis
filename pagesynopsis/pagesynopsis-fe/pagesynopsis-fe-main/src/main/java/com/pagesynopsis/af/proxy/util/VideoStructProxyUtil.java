package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.VideoStruct;
// import com.pagesynopsis.ws.bean.VideoStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;


public class VideoStructProxyUtil
{
    private static final Logger log = Logger.getLogger(VideoStructProxyUtil.class.getName());

    // Static methods only.
    private VideoStructProxyUtil() {}

    public static VideoStructBean convertServerVideoStructBeanToAppBean(VideoStruct serverBean)
    {
        VideoStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new VideoStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setId(serverBean.getId());
            bean.setWidth(serverBean.getWidth());
            bean.setHeight(serverBean.getHeight());
            bean.setControls(serverBean.getControls());
            bean.setAutoplayEnabled(serverBean.isAutoplayEnabled());
            bean.setLoopEnabled(serverBean.isLoopEnabled());
            bean.setPreloadEnabled(serverBean.isPreloadEnabled());
            bean.setMuted(serverBean.isMuted());
            bean.setRemark(serverBean.getRemark());
            bean.setSource(MediaSourceStructProxyUtil.convertServerMediaSourceStructBeanToAppBean(serverBean.getSource()));
            bean.setSource1(MediaSourceStructProxyUtil.convertServerMediaSourceStructBeanToAppBean(serverBean.getSource1()));
            bean.setSource2(MediaSourceStructProxyUtil.convertServerMediaSourceStructBeanToAppBean(serverBean.getSource2()));
            bean.setSource3(MediaSourceStructProxyUtil.convertServerMediaSourceStructBeanToAppBean(serverBean.getSource3()));
            bean.setSource4(MediaSourceStructProxyUtil.convertServerMediaSourceStructBeanToAppBean(serverBean.getSource4()));
            bean.setSource5(MediaSourceStructProxyUtil.convertServerMediaSourceStructBeanToAppBean(serverBean.getSource5()));
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.VideoStructBean convertAppVideoStructBeanToServerBean(VideoStruct appBean)
    {
        com.pagesynopsis.ws.bean.VideoStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.VideoStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setId(appBean.getId());
            bean.setWidth(appBean.getWidth());
            bean.setHeight(appBean.getHeight());
            bean.setControls(appBean.getControls());
            bean.setAutoplayEnabled(appBean.isAutoplayEnabled());
            bean.setLoopEnabled(appBean.isLoopEnabled());
            bean.setPreloadEnabled(appBean.isPreloadEnabled());
            bean.setMuted(appBean.isMuted());
            bean.setRemark(appBean.getRemark());
            bean.setSource(MediaSourceStructProxyUtil.convertAppMediaSourceStructBeanToServerBean(appBean.getSource()));
            bean.setSource1(MediaSourceStructProxyUtil.convertAppMediaSourceStructBeanToServerBean(appBean.getSource1()));
            bean.setSource2(MediaSourceStructProxyUtil.convertAppMediaSourceStructBeanToServerBean(appBean.getSource2()));
            bean.setSource3(MediaSourceStructProxyUtil.convertAppMediaSourceStructBeanToServerBean(appBean.getSource3()));
            bean.setSource4(MediaSourceStructProxyUtil.convertAppMediaSourceStructBeanToServerBean(appBean.getSource4()));
            bean.setSource5(MediaSourceStructProxyUtil.convertAppMediaSourceStructBeanToServerBean(appBean.getSource5()));
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
