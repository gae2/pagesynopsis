package com.pagesynopsis.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.af.proxy.OgVideoServiceProxy;


// MockOgVideoServiceProxy is a decorator.
// It can be used as a base class to mock OgVideoServiceProxy objects.
public abstract class MockOgVideoServiceProxy implements OgVideoServiceProxy
{
    private static final Logger log = Logger.getLogger(MockOgVideoServiceProxy.class.getName());

    // MockOgVideoServiceProxy uses the decorator design pattern.
    private OgVideoServiceProxy decoratedProxy;

    public MockOgVideoServiceProxy(OgVideoServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected OgVideoServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(OgVideoServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public OgVideo getOgVideo(String guid) throws BaseException
    {
        return decoratedProxy.getOgVideo(guid);
    }

    @Override
    public Object getOgVideo(String guid, String field) throws BaseException
    {
        return decoratedProxy.getOgVideo(guid, field);       
    }

    @Override
    public List<OgVideo> getOgVideos(List<String> guids) throws BaseException
    {
        return decoratedProxy.getOgVideos(guids);
    }

    @Override
    public List<OgVideo> getAllOgVideos() throws BaseException
    {
        return getAllOgVideos(null, null, null);
    }

    @Override
    public List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgVideos(ordering, offset, count);
        return getAllOgVideos(ordering, offset, count, null);
    }

    @Override
    public List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgVideos(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllOgVideoKeys(ordering, offset, count);
        return getAllOgVideoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgVideoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgVideos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgVideos(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgVideo(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedProxy.createOgVideo(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public String createOgVideo(OgVideo ogVideo) throws BaseException
    {
        return decoratedProxy.createOgVideo(ogVideo);
    }

    @Override
    public Boolean updateOgVideo(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedProxy.updateOgVideo(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public Boolean updateOgVideo(OgVideo ogVideo) throws BaseException
    {
        return decoratedProxy.updateOgVideo(ogVideo);
    }

    @Override
    public Boolean deleteOgVideo(String guid) throws BaseException
    {
        return decoratedProxy.deleteOgVideo(guid);
    }

    @Override
    public Boolean deleteOgVideo(OgVideo ogVideo) throws BaseException
    {
        String guid = ogVideo.getGuid();
        return deleteOgVideo(guid);
    }

    @Override
    public Long deleteOgVideos(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteOgVideos(filter, params, values);
    }

}
