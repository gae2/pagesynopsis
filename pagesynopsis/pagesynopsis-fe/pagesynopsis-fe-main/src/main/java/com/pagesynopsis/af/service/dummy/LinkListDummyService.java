package com.pagesynopsis.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.LinkListService;


// The primary purpose of LinkListDummyService is to fake the service api, LinkListService.
// It has no real implementation.
public class LinkListDummyService implements LinkListService
{
    private static final Logger log = Logger.getLogger(LinkListDummyService.class.getName());

    public LinkListDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // LinkList related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public LinkList getLinkList(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getLinkList(): guid = " + guid);
        return null;
    }

    @Override
    public Object getLinkList(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getLinkList(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<LinkList> getLinkLists(List<String> guids) throws BaseException
    {
        log.fine("getLinkLists()");
        return null;
    }

    @Override
    public List<LinkList> getAllLinkLists() throws BaseException
    {
        return getAllLinkLists(null, null, null);
    }


    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllLinkLists(ordering, offset, count, null);
    }

    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkLists(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllLinkListKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkListKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkListDummyService.findLinkLists(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkListDummyService.findLinkListKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkListDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createLinkList(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        log.finer("createLinkList()");
        return null;
    }

    @Override
    public String createLinkList(LinkList linkList) throws BaseException
    {
        log.finer("createLinkList()");
        return null;
    }

    @Override
    public LinkList constructLinkList(LinkList linkList) throws BaseException
    {
        log.finer("constructLinkList()");
        return null;
    }

    @Override
    public Boolean updateLinkList(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        log.finer("updateLinkList()");
        return null;
    }
        
    @Override
    public Boolean updateLinkList(LinkList linkList) throws BaseException
    {
        log.finer("updateLinkList()");
        return null;
    }

    @Override
    public LinkList refreshLinkList(LinkList linkList) throws BaseException
    {
        log.finer("refreshLinkList()");
        return null;
    }

    @Override
    public Boolean deleteLinkList(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteLinkList(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteLinkList(LinkList linkList) throws BaseException
    {
        log.finer("deleteLinkList()");
        return null;
    }

    // TBD
    @Override
    public Long deleteLinkLists(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteLinkList(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createLinkLists(List<LinkList> linkLists) throws BaseException
    {
        log.finer("createLinkLists()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateLinkLists(List<LinkList> linkLists) throws BaseException
    //{
    //}

}
