package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPlayerCard;
// import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.proxy.TwitterPlayerCardServiceProxy;


public class LocalTwitterPlayerCardServiceProxy extends BaseLocalServiceProxy implements TwitterPlayerCardServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterPlayerCardServiceProxy.class.getName());

    public LocalTwitterPlayerCardServiceProxy()
    {
    }

    @Override
    public TwitterPlayerCard getTwitterPlayerCard(String guid) throws BaseException
    {
        TwitterPlayerCard serverBean = getTwitterPlayerCardService().getTwitterPlayerCard(guid);
        TwitterPlayerCard appBean = convertServerTwitterPlayerCardBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getTwitterPlayerCard(String guid, String field) throws BaseException
    {
        return getTwitterPlayerCardService().getTwitterPlayerCard(guid, field);       
    }

    @Override
    public List<TwitterPlayerCard> getTwitterPlayerCards(List<String> guids) throws BaseException
    {
        List<TwitterPlayerCard> serverBeanList = getTwitterPlayerCardService().getTwitterPlayerCards(guids);
        List<TwitterPlayerCard> appBeanList = convertServerTwitterPlayerCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards() throws BaseException
    {
        return getAllTwitterPlayerCards(null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterPlayerCardService().getAllTwitterPlayerCards(ordering, offset, count);
        return getAllTwitterPlayerCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterPlayerCard> serverBeanList = getTwitterPlayerCardService().getAllTwitterPlayerCards(ordering, offset, count, forwardCursor);
        List<TwitterPlayerCard> appBeanList = convertServerTwitterPlayerCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterPlayerCardService().getAllTwitterPlayerCardKeys(ordering, offset, count);
        return getAllTwitterPlayerCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterPlayerCardService().getAllTwitterPlayerCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterPlayerCardService().findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterPlayerCard> serverBeanList = getTwitterPlayerCardService().findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<TwitterPlayerCard> appBeanList = convertServerTwitterPlayerCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterPlayerCardService().findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterPlayerCardService().findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterPlayerCardService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterPlayerCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        return getTwitterPlayerCardService().createTwitterPlayerCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
    }

    @Override
    public String createTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterPlayerCardBean serverBean =  convertAppTwitterPlayerCardBeanToServerBean(twitterPlayerCard);
        return getTwitterPlayerCardService().createTwitterPlayerCard(serverBean);
    }

    @Override
    public Boolean updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        return getTwitterPlayerCardService().updateTwitterPlayerCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
    }

    @Override
    public Boolean updateTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterPlayerCardBean serverBean =  convertAppTwitterPlayerCardBeanToServerBean(twitterPlayerCard);
        return getTwitterPlayerCardService().updateTwitterPlayerCard(serverBean);
    }

    @Override
    public Boolean deleteTwitterPlayerCard(String guid) throws BaseException
    {
        return getTwitterPlayerCardService().deleteTwitterPlayerCard(guid);
    }

    @Override
    public Boolean deleteTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterPlayerCardBean serverBean =  convertAppTwitterPlayerCardBeanToServerBean(twitterPlayerCard);
        return getTwitterPlayerCardService().deleteTwitterPlayerCard(serverBean);
    }

    @Override
    public Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterPlayerCardService().deleteTwitterPlayerCards(filter, params, values);
    }




    public static TwitterPlayerCardBean convertServerTwitterPlayerCardBeanToAppBean(TwitterPlayerCard serverBean)
    {
        TwitterPlayerCardBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new TwitterPlayerCardBean();
            bean.setGuid(serverBean.getGuid());
            bean.setCard(serverBean.getCard());
            bean.setUrl(serverBean.getUrl());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setSite(serverBean.getSite());
            bean.setSiteId(serverBean.getSiteId());
            bean.setCreator(serverBean.getCreator());
            bean.setCreatorId(serverBean.getCreatorId());
            bean.setImage(serverBean.getImage());
            bean.setImageWidth(serverBean.getImageWidth());
            bean.setImageHeight(serverBean.getImageHeight());
            bean.setPlayer(serverBean.getPlayer());
            bean.setPlayerWidth(serverBean.getPlayerWidth());
            bean.setPlayerHeight(serverBean.getPlayerHeight());
            bean.setPlayerStream(serverBean.getPlayerStream());
            bean.setPlayerStreamContentType(serverBean.getPlayerStreamContentType());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterPlayerCard> convertServerTwitterPlayerCardBeanListToAppBeanList(List<TwitterPlayerCard> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterPlayerCard> beanList = new ArrayList<TwitterPlayerCard>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(TwitterPlayerCard sb : serverBeanList) {
                TwitterPlayerCardBean bean = convertServerTwitterPlayerCardBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.TwitterPlayerCardBean convertAppTwitterPlayerCardBeanToServerBean(TwitterPlayerCard appBean)
    {
        com.pagesynopsis.ws.bean.TwitterPlayerCardBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.TwitterPlayerCardBean();
            bean.setGuid(appBean.getGuid());
            bean.setCard(appBean.getCard());
            bean.setUrl(appBean.getUrl());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setSite(appBean.getSite());
            bean.setSiteId(appBean.getSiteId());
            bean.setCreator(appBean.getCreator());
            bean.setCreatorId(appBean.getCreatorId());
            bean.setImage(appBean.getImage());
            bean.setImageWidth(appBean.getImageWidth());
            bean.setImageHeight(appBean.getImageHeight());
            bean.setPlayer(appBean.getPlayer());
            bean.setPlayerWidth(appBean.getPlayerWidth());
            bean.setPlayerHeight(appBean.getPlayerHeight());
            bean.setPlayerStream(appBean.getPlayerStream());
            bean.setPlayerStreamContentType(appBean.getPlayerStreamContentType());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterPlayerCard> convertAppTwitterPlayerCardBeanListToServerBeanList(List<TwitterPlayerCard> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterPlayerCard> beanList = new ArrayList<TwitterPlayerCard>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(TwitterPlayerCard sb : appBeanList) {
                com.pagesynopsis.ws.bean.TwitterPlayerCardBean bean = convertAppTwitterPlayerCardBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
