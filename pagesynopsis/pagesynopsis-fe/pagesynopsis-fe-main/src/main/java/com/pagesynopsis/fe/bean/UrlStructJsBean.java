package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlStructJsBean implements Serializable, Cloneable  //, UrlStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UrlStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private Integer statusCode;
    private String redirectUrl;
    private String absoluteUrl;
    private String hashFragment;
    private String note;

    // Ctors.
    public UrlStructJsBean()
    {
        //this((String) null);
    }
    public UrlStructJsBean(String uuid, Integer statusCode, String redirectUrl, String absoluteUrl, String hashFragment, String note)
    {
        this.uuid = uuid;
        this.statusCode = statusCode;
        this.redirectUrl = redirectUrl;
        this.absoluteUrl = absoluteUrl;
        this.hashFragment = hashFragment;
        this.note = note;
    }
    public UrlStructJsBean(UrlStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setStatusCode(bean.getStatusCode());
            setRedirectUrl(bean.getRedirectUrl());
            setAbsoluteUrl(bean.getAbsoluteUrl());
            setHashFragment(bean.getHashFragment());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static UrlStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        UrlStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(UrlStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, UrlStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public Integer getStatusCode()
    {
        return this.statusCode;
    }
    public void setStatusCode(Integer statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getRedirectUrl()
    {
        return this.redirectUrl;
    }
    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
    }

    public String getAbsoluteUrl()
    {
        return this.absoluteUrl;
    }
    public void setAbsoluteUrl(String absoluteUrl)
    {
        this.absoluteUrl = absoluteUrl;
    }

    public String getHashFragment()
    {
        return this.hashFragment;
    }
    public void setHashFragment(String hashFragment)
    {
        this.hashFragment = hashFragment;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStatusCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRedirectUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAbsoluteUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHashFragment() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("statusCode:0, ");
        sb.append("redirectUrl:null, ");
        sb.append("absoluteUrl:null, ");
        sb.append("hashFragment:null, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("statusCode:" + this.getStatusCode()).append(", ");
        sb.append("redirectUrl:");
        if(this.getRedirectUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRedirectUrl()).append("\", ");
        }
        sb.append("absoluteUrl:");
        if(this.getAbsoluteUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAbsoluteUrl()).append("\", ");
        }
        sb.append("hashFragment:");
        if(this.getHashFragment() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getHashFragment()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getStatusCode() != null) {
            sb.append("\"statusCode\":").append("").append(this.getStatusCode()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"statusCode\":").append("null, ");
        }
        if(this.getRedirectUrl() != null) {
            sb.append("\"redirectUrl\":").append("\"").append(this.getRedirectUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"redirectUrl\":").append("null, ");
        }
        if(this.getAbsoluteUrl() != null) {
            sb.append("\"absoluteUrl\":").append("\"").append(this.getAbsoluteUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"absoluteUrl\":").append("null, ");
        }
        if(this.getHashFragment() != null) {
            sb.append("\"hashFragment\":").append("\"").append(this.getHashFragment()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"hashFragment\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("statusCode = " + this.statusCode).append(";");
        sb.append("redirectUrl = " + this.redirectUrl).append(";");
        sb.append("absoluteUrl = " + this.absoluteUrl).append(";");
        sb.append("hashFragment = " + this.hashFragment).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UrlStructJsBean cloned = new UrlStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setStatusCode(this.getStatusCode());   
        cloned.setRedirectUrl(this.getRedirectUrl());   
        cloned.setAbsoluteUrl(this.getAbsoluteUrl());   
        cloned.setHashFragment(this.getHashFragment());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
