package com.pagesynopsis.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgAudioStruct;
// import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;


public class OgAudioStructProxyUtil
{
    private static final Logger log = Logger.getLogger(OgAudioStructProxyUtil.class.getName());

    // Static methods only.
    private OgAudioStructProxyUtil() {}

    public static OgAudioStructBean convertServerOgAudioStructBeanToAppBean(OgAudioStruct serverBean)
    {
        OgAudioStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new OgAudioStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setUrl(serverBean.getUrl());
            bean.setSecureUrl(serverBean.getSecureUrl());
            bean.setType(serverBean.getType());
        }
        return bean;
    }

    public static com.pagesynopsis.ws.bean.OgAudioStructBean convertAppOgAudioStructBeanToServerBean(OgAudioStruct appBean)
    {
        com.pagesynopsis.ws.bean.OgAudioStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.OgAudioStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setUrl(appBean.getUrl());
            bean.setSecureUrl(appBean.getSecureUrl());
            bean.setType(appBean.getType());
        }
        return bean;
    }

}
