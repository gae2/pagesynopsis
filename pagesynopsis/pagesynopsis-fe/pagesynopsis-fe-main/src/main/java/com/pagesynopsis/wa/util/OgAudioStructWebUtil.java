package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;


public class OgAudioStructWebUtil
{
    private static final Logger log = Logger.getLogger(OgAudioStructWebUtil.class.getName());

    // Static methods only.
    private OgAudioStructWebUtil() {}
    

    public static OgAudioStructJsBean convertOgAudioStructToJsBean(OgAudioStruct ogAudioStruct)
    {
        OgAudioStructJsBean jsBean = null;
        if(ogAudioStruct != null) {
            jsBean = new OgAudioStructJsBean();
            jsBean.setUuid(ogAudioStruct.getUuid());
            jsBean.setUrl(ogAudioStruct.getUrl());
            jsBean.setSecureUrl(ogAudioStruct.getSecureUrl());
            jsBean.setType(ogAudioStruct.getType());
        }
        return jsBean;
    }

    public static OgAudioStruct convertOgAudioStructJsBeanToBean(OgAudioStructJsBean jsBean)
    {
        OgAudioStructBean ogAudioStruct = null;
        if(jsBean != null) {
            ogAudioStruct = new OgAudioStructBean();
            ogAudioStruct.setUuid(jsBean.getUuid());
            ogAudioStruct.setUrl(jsBean.getUrl());
            ogAudioStruct.setSecureUrl(jsBean.getSecureUrl());
            ogAudioStruct.setType(jsBean.getType());
        }
        return ogAudioStruct;
    }

}
