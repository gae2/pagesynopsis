package com.pagesynopsis.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.TwitterCardMetaBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterCardMetaService;


// TwitterCardMetaMockService is a decorator.
// It can be used as a base class to mock TwitterCardMetaService objects.
public abstract class TwitterCardMetaMockService implements TwitterCardMetaService
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaMockService.class.getName());

    // TwitterCardMetaMockService uses the decorator design pattern.
    private TwitterCardMetaService decoratedService;

    public TwitterCardMetaMockService(TwitterCardMetaService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected TwitterCardMetaService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(TwitterCardMetaService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // TwitterCardMeta related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterCardMeta getTwitterCardMeta(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterCardMeta(): guid = " + guid);
        TwitterCardMeta bean = decoratedService.getTwitterCardMeta(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterCardMeta(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getTwitterCardMeta(guid, field);
        return obj;
    }

    @Override
    public List<TwitterCardMeta> getTwitterCardMetas(List<String> guids) throws BaseException
    {
        log.fine("getTwitterCardMetas()");
        List<TwitterCardMeta> twitterCardMetas = decoratedService.getTwitterCardMetas(guids);
        log.finer("END");
        return twitterCardMetas;
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas() throws BaseException
    {
        return getAllTwitterCardMetas(null, null, null);
    }


    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterCardMetas(ordering, offset, count, null);
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterCardMetas(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<TwitterCardMeta> twitterCardMetas = decoratedService.getAllTwitterCardMetas(ordering, offset, count, forwardCursor);
        log.finer("END");
        return twitterCardMetas;
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterCardMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterCardMetaKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllTwitterCardMetaKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterCardMetas(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterCardMetaMockService.findTwitterCardMetas(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<TwitterCardMeta> twitterCardMetas = decoratedService.findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return twitterCardMetas;
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterCardMetaMockService.findTwitterCardMetaKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterCardMetaMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterCardMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException
    {
        TwitterSummaryCardBean summaryCardBean = null;
        if(summaryCard instanceof TwitterSummaryCardBean) {
            summaryCardBean = (TwitterSummaryCardBean) summaryCard;
        } else if(summaryCard instanceof TwitterSummaryCard) {
            summaryCardBean = new TwitterSummaryCardBean(summaryCard.getGuid(), summaryCard.getCard(), summaryCard.getUrl(), summaryCard.getTitle(), summaryCard.getDescription(), summaryCard.getSite(), summaryCard.getSiteId(), summaryCard.getCreator(), summaryCard.getCreatorId(), summaryCard.getImage(), summaryCard.getImageWidth(), summaryCard.getImageHeight(), summaryCard.getCreatedTime(), summaryCard.getModifiedTime());
        } else {
            summaryCardBean = null;   // ????
        }
        TwitterPhotoCardBean photoCardBean = null;
        if(photoCard instanceof TwitterPhotoCardBean) {
            photoCardBean = (TwitterPhotoCardBean) photoCard;
        } else if(photoCard instanceof TwitterPhotoCard) {
            photoCardBean = new TwitterPhotoCardBean(photoCard.getGuid(), photoCard.getCard(), photoCard.getUrl(), photoCard.getTitle(), photoCard.getDescription(), photoCard.getSite(), photoCard.getSiteId(), photoCard.getCreator(), photoCard.getCreatorId(), photoCard.getImage(), photoCard.getImageWidth(), photoCard.getImageHeight(), photoCard.getCreatedTime(), photoCard.getModifiedTime());
        } else {
            photoCardBean = null;   // ????
        }
        TwitterGalleryCardBean galleryCardBean = null;
        if(galleryCard instanceof TwitterGalleryCardBean) {
            galleryCardBean = (TwitterGalleryCardBean) galleryCard;
        } else if(galleryCard instanceof TwitterGalleryCard) {
            galleryCardBean = new TwitterGalleryCardBean(galleryCard.getGuid(), galleryCard.getCard(), galleryCard.getUrl(), galleryCard.getTitle(), galleryCard.getDescription(), galleryCard.getSite(), galleryCard.getSiteId(), galleryCard.getCreator(), galleryCard.getCreatorId(), galleryCard.getImage0(), galleryCard.getImage1(), galleryCard.getImage2(), galleryCard.getImage3(), galleryCard.getCreatedTime(), galleryCard.getModifiedTime());
        } else {
            galleryCardBean = null;   // ????
        }
        TwitterAppCardBean appCardBean = null;
        if(appCard instanceof TwitterAppCardBean) {
            appCardBean = (TwitterAppCardBean) appCard;
        } else if(appCard instanceof TwitterAppCard) {
            appCardBean = new TwitterAppCardBean(appCard.getGuid(), appCard.getCard(), appCard.getUrl(), appCard.getTitle(), appCard.getDescription(), appCard.getSite(), appCard.getSiteId(), appCard.getCreator(), appCard.getCreatorId(), appCard.getImage(), appCard.getImageWidth(), appCard.getImageHeight(), (TwitterCardAppInfoBean) appCard.getIphoneApp(), (TwitterCardAppInfoBean) appCard.getIpadApp(), (TwitterCardAppInfoBean) appCard.getGooglePlayApp(), appCard.getCreatedTime(), appCard.getModifiedTime());
        } else {
            appCardBean = null;   // ????
        }
        TwitterPlayerCardBean playerCardBean = null;
        if(playerCard instanceof TwitterPlayerCardBean) {
            playerCardBean = (TwitterPlayerCardBean) playerCard;
        } else if(playerCard instanceof TwitterPlayerCard) {
            playerCardBean = new TwitterPlayerCardBean(playerCard.getGuid(), playerCard.getCard(), playerCard.getUrl(), playerCard.getTitle(), playerCard.getDescription(), playerCard.getSite(), playerCard.getSiteId(), playerCard.getCreator(), playerCard.getCreatorId(), playerCard.getImage(), playerCard.getImageWidth(), playerCard.getImageHeight(), playerCard.getPlayer(), playerCard.getPlayerWidth(), playerCard.getPlayerHeight(), playerCard.getPlayerStream(), playerCard.getPlayerStreamContentType(), playerCard.getCreatedTime(), playerCard.getModifiedTime());
        } else {
            playerCardBean = null;   // ????
        }
        TwitterProductCardBean productCardBean = null;
        if(productCard instanceof TwitterProductCardBean) {
            productCardBean = (TwitterProductCardBean) productCard;
        } else if(productCard instanceof TwitterProductCard) {
            productCardBean = new TwitterProductCardBean(productCard.getGuid(), productCard.getCard(), productCard.getUrl(), productCard.getTitle(), productCard.getDescription(), productCard.getSite(), productCard.getSiteId(), productCard.getCreator(), productCard.getCreatorId(), productCard.getImage(), productCard.getImageWidth(), productCard.getImageHeight(), (TwitterCardProductDataBean) productCard.getData1(), (TwitterCardProductDataBean) productCard.getData2(), productCard.getCreatedTime(), productCard.getModifiedTime());
        } else {
            productCardBean = null;   // ????
        }
        TwitterCardMetaBean bean = new TwitterCardMetaBean(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, summaryCardBean, photoCardBean, galleryCardBean, appCardBean, playerCardBean, productCardBean);
        return createTwitterCardMeta(bean);
    }

    @Override
    public String createTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createTwitterCardMeta(twitterCardMeta);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TwitterCardMeta constructTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");
        TwitterCardMeta bean = decoratedService.constructTwitterCardMeta(twitterCardMeta);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTwitterCardMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterSummaryCardBean summaryCardBean = null;
        if(summaryCard instanceof TwitterSummaryCardBean) {
            summaryCardBean = (TwitterSummaryCardBean) summaryCard;
        } else if(summaryCard instanceof TwitterSummaryCard) {
            summaryCardBean = new TwitterSummaryCardBean(summaryCard.getGuid(), summaryCard.getCard(), summaryCard.getUrl(), summaryCard.getTitle(), summaryCard.getDescription(), summaryCard.getSite(), summaryCard.getSiteId(), summaryCard.getCreator(), summaryCard.getCreatorId(), summaryCard.getImage(), summaryCard.getImageWidth(), summaryCard.getImageHeight(), summaryCard.getCreatedTime(), summaryCard.getModifiedTime());
        } else {
            summaryCardBean = null;   // ????
        }
        TwitterPhotoCardBean photoCardBean = null;
        if(photoCard instanceof TwitterPhotoCardBean) {
            photoCardBean = (TwitterPhotoCardBean) photoCard;
        } else if(photoCard instanceof TwitterPhotoCard) {
            photoCardBean = new TwitterPhotoCardBean(photoCard.getGuid(), photoCard.getCard(), photoCard.getUrl(), photoCard.getTitle(), photoCard.getDescription(), photoCard.getSite(), photoCard.getSiteId(), photoCard.getCreator(), photoCard.getCreatorId(), photoCard.getImage(), photoCard.getImageWidth(), photoCard.getImageHeight(), photoCard.getCreatedTime(), photoCard.getModifiedTime());
        } else {
            photoCardBean = null;   // ????
        }
        TwitterGalleryCardBean galleryCardBean = null;
        if(galleryCard instanceof TwitterGalleryCardBean) {
            galleryCardBean = (TwitterGalleryCardBean) galleryCard;
        } else if(galleryCard instanceof TwitterGalleryCard) {
            galleryCardBean = new TwitterGalleryCardBean(galleryCard.getGuid(), galleryCard.getCard(), galleryCard.getUrl(), galleryCard.getTitle(), galleryCard.getDescription(), galleryCard.getSite(), galleryCard.getSiteId(), galleryCard.getCreator(), galleryCard.getCreatorId(), galleryCard.getImage0(), galleryCard.getImage1(), galleryCard.getImage2(), galleryCard.getImage3(), galleryCard.getCreatedTime(), galleryCard.getModifiedTime());
        } else {
            galleryCardBean = null;   // ????
        }
        TwitterAppCardBean appCardBean = null;
        if(appCard instanceof TwitterAppCardBean) {
            appCardBean = (TwitterAppCardBean) appCard;
        } else if(appCard instanceof TwitterAppCard) {
            appCardBean = new TwitterAppCardBean(appCard.getGuid(), appCard.getCard(), appCard.getUrl(), appCard.getTitle(), appCard.getDescription(), appCard.getSite(), appCard.getSiteId(), appCard.getCreator(), appCard.getCreatorId(), appCard.getImage(), appCard.getImageWidth(), appCard.getImageHeight(), (TwitterCardAppInfoBean) appCard.getIphoneApp(), (TwitterCardAppInfoBean) appCard.getIpadApp(), (TwitterCardAppInfoBean) appCard.getGooglePlayApp(), appCard.getCreatedTime(), appCard.getModifiedTime());
        } else {
            appCardBean = null;   // ????
        }
        TwitterPlayerCardBean playerCardBean = null;
        if(playerCard instanceof TwitterPlayerCardBean) {
            playerCardBean = (TwitterPlayerCardBean) playerCard;
        } else if(playerCard instanceof TwitterPlayerCard) {
            playerCardBean = new TwitterPlayerCardBean(playerCard.getGuid(), playerCard.getCard(), playerCard.getUrl(), playerCard.getTitle(), playerCard.getDescription(), playerCard.getSite(), playerCard.getSiteId(), playerCard.getCreator(), playerCard.getCreatorId(), playerCard.getImage(), playerCard.getImageWidth(), playerCard.getImageHeight(), playerCard.getPlayer(), playerCard.getPlayerWidth(), playerCard.getPlayerHeight(), playerCard.getPlayerStream(), playerCard.getPlayerStreamContentType(), playerCard.getCreatedTime(), playerCard.getModifiedTime());
        } else {
            playerCardBean = null;   // ????
        }
        TwitterProductCardBean productCardBean = null;
        if(productCard instanceof TwitterProductCardBean) {
            productCardBean = (TwitterProductCardBean) productCard;
        } else if(productCard instanceof TwitterProductCard) {
            productCardBean = new TwitterProductCardBean(productCard.getGuid(), productCard.getCard(), productCard.getUrl(), productCard.getTitle(), productCard.getDescription(), productCard.getSite(), productCard.getSiteId(), productCard.getCreator(), productCard.getCreatorId(), productCard.getImage(), productCard.getImageWidth(), productCard.getImageHeight(), (TwitterCardProductDataBean) productCard.getData1(), (TwitterCardProductDataBean) productCard.getData2(), productCard.getCreatedTime(), productCard.getModifiedTime());
        } else {
            productCardBean = null;   // ????
        }
        TwitterCardMetaBean bean = new TwitterCardMetaBean(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, summaryCardBean, photoCardBean, galleryCardBean, appCardBean, playerCardBean, productCardBean);
        return updateTwitterCardMeta(bean);
    }
        
    @Override
    public Boolean updateTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateTwitterCardMeta(twitterCardMeta);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TwitterCardMeta refreshTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");
        TwitterCardMeta bean = decoratedService.refreshTwitterCardMeta(twitterCardMeta);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTwitterCardMeta(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterCardMeta(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterCardMeta(twitterCardMeta);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterCardMetas(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteTwitterCardMetas(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterCardMetas(List<TwitterCardMeta> twitterCardMetas) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createTwitterCardMetas(twitterCardMetas);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterCardMetas(List<TwitterCardMeta> twitterCardMetas) throws BaseException
    //{
    //}

}
