package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterSummaryCard;
// import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.proxy.TwitterSummaryCardServiceProxy;


public class LocalTwitterSummaryCardServiceProxy extends BaseLocalServiceProxy implements TwitterSummaryCardServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterSummaryCardServiceProxy.class.getName());

    public LocalTwitterSummaryCardServiceProxy()
    {
    }

    @Override
    public TwitterSummaryCard getTwitterSummaryCard(String guid) throws BaseException
    {
        TwitterSummaryCard serverBean = getTwitterSummaryCardService().getTwitterSummaryCard(guid);
        TwitterSummaryCard appBean = convertServerTwitterSummaryCardBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getTwitterSummaryCard(String guid, String field) throws BaseException
    {
        return getTwitterSummaryCardService().getTwitterSummaryCard(guid, field);       
    }

    @Override
    public List<TwitterSummaryCard> getTwitterSummaryCards(List<String> guids) throws BaseException
    {
        List<TwitterSummaryCard> serverBeanList = getTwitterSummaryCardService().getTwitterSummaryCards(guids);
        List<TwitterSummaryCard> appBeanList = convertServerTwitterSummaryCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards() throws BaseException
    {
        return getAllTwitterSummaryCards(null, null, null);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterSummaryCardService().getAllTwitterSummaryCards(ordering, offset, count);
        return getAllTwitterSummaryCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterSummaryCard> serverBeanList = getTwitterSummaryCardService().getAllTwitterSummaryCards(ordering, offset, count, forwardCursor);
        List<TwitterSummaryCard> appBeanList = convertServerTwitterSummaryCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterSummaryCardService().getAllTwitterSummaryCardKeys(ordering, offset, count);
        return getAllTwitterSummaryCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterSummaryCardService().getAllTwitterSummaryCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterSummaryCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterSummaryCardService().findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterSummaryCard> serverBeanList = getTwitterSummaryCardService().findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<TwitterSummaryCard> appBeanList = convertServerTwitterSummaryCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterSummaryCardService().findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterSummaryCardService().findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterSummaryCardService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterSummaryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return getTwitterSummaryCardService().createTwitterSummaryCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public String createTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterSummaryCardBean serverBean =  convertAppTwitterSummaryCardBeanToServerBean(twitterSummaryCard);
        return getTwitterSummaryCardService().createTwitterSummaryCard(serverBean);
    }

    @Override
    public Boolean updateTwitterSummaryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return getTwitterSummaryCardService().updateTwitterSummaryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public Boolean updateTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterSummaryCardBean serverBean =  convertAppTwitterSummaryCardBeanToServerBean(twitterSummaryCard);
        return getTwitterSummaryCardService().updateTwitterSummaryCard(serverBean);
    }

    @Override
    public Boolean deleteTwitterSummaryCard(String guid) throws BaseException
    {
        return getTwitterSummaryCardService().deleteTwitterSummaryCard(guid);
    }

    @Override
    public Boolean deleteTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterSummaryCardBean serverBean =  convertAppTwitterSummaryCardBeanToServerBean(twitterSummaryCard);
        return getTwitterSummaryCardService().deleteTwitterSummaryCard(serverBean);
    }

    @Override
    public Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterSummaryCardService().deleteTwitterSummaryCards(filter, params, values);
    }




    public static TwitterSummaryCardBean convertServerTwitterSummaryCardBeanToAppBean(TwitterSummaryCard serverBean)
    {
        TwitterSummaryCardBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new TwitterSummaryCardBean();
            bean.setGuid(serverBean.getGuid());
            bean.setCard(serverBean.getCard());
            bean.setUrl(serverBean.getUrl());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setSite(serverBean.getSite());
            bean.setSiteId(serverBean.getSiteId());
            bean.setCreator(serverBean.getCreator());
            bean.setCreatorId(serverBean.getCreatorId());
            bean.setImage(serverBean.getImage());
            bean.setImageWidth(serverBean.getImageWidth());
            bean.setImageHeight(serverBean.getImageHeight());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterSummaryCard> convertServerTwitterSummaryCardBeanListToAppBeanList(List<TwitterSummaryCard> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterSummaryCard> beanList = new ArrayList<TwitterSummaryCard>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(TwitterSummaryCard sb : serverBeanList) {
                TwitterSummaryCardBean bean = convertServerTwitterSummaryCardBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.TwitterSummaryCardBean convertAppTwitterSummaryCardBeanToServerBean(TwitterSummaryCard appBean)
    {
        com.pagesynopsis.ws.bean.TwitterSummaryCardBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.TwitterSummaryCardBean();
            bean.setGuid(appBean.getGuid());
            bean.setCard(appBean.getCard());
            bean.setUrl(appBean.getUrl());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setSite(appBean.getSite());
            bean.setSiteId(appBean.getSiteId());
            bean.setCreator(appBean.getCreator());
            bean.setCreatorId(appBean.getCreatorId());
            bean.setImage(appBean.getImage());
            bean.setImageWidth(appBean.getImageWidth());
            bean.setImageHeight(appBean.getImageHeight());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterSummaryCard> convertAppTwitterSummaryCardBeanListToServerBeanList(List<TwitterSummaryCard> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterSummaryCard> beanList = new ArrayList<TwitterSummaryCard>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(TwitterSummaryCard sb : appBeanList) {
                com.pagesynopsis.ws.bean.TwitterSummaryCardBean bean = convertAppTwitterSummaryCardBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
