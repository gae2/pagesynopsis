package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.stub.OgImageStructStub;


// Wrapper class + bean combo.
public class OgImageStructBean implements OgImageStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgImageStructBean.class.getName());

    // [1] With an embedded object.
    private OgImageStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String url;
    private String secureUrl;
    private String type;
    private Integer width;
    private Integer height;

    // Ctors.
    public OgImageStructBean()
    {
        //this((String) null);
    }
    public OgImageStructBean(String uuid, String url, String secureUrl, String type, Integer width, Integer height)
    {
        this.uuid = uuid;
        this.url = url;
        this.secureUrl = secureUrl;
        this.type = type;
        this.width = width;
        this.height = height;
    }
    public OgImageStructBean(OgImageStruct stub)
    {
        if(stub instanceof OgImageStructStub) {
            this.stub = (OgImageStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setUrl(stub.getUrl());   
            setSecureUrl(stub.getSecureUrl());   
            setType(stub.getType());   
            setWidth(stub.getWidth());   
            setHeight(stub.getHeight());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getUrl()
    {
        if(getStub() != null) {
            return getStub().getUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.url;
        }
    }
    public void setUrl(String url)
    {
        if(getStub() != null) {
            getStub().setUrl(url);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.url = url;
        }
    }

    public String getSecureUrl()
    {
        if(getStub() != null) {
            return getStub().getSecureUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.secureUrl;
        }
    }
    public void setSecureUrl(String secureUrl)
    {
        if(getStub() != null) {
            getStub().setSecureUrl(secureUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.secureUrl = secureUrl;
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }

    public Integer getWidth()
    {
        if(getStub() != null) {
            return getStub().getWidth();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.width;
        }
    }
    public void setWidth(Integer width)
    {
        if(getStub() != null) {
            getStub().setWidth(width);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.width = width;
        }
    }

    public Integer getHeight()
    {
        if(getStub() != null) {
            return getStub().getHeight();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.height;
        }
    }
    public void setHeight(Integer height)
    {
        if(getStub() != null) {
            getStub().setHeight(height);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.height = height;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecureUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public OgImageStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(OgImageStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("url = " + this.url).append(";");
            sb.append("secureUrl = " + this.secureUrl).append(";");
            sb.append("type = " + this.type).append(";");
            sb.append("width = " + this.width).append(";");
            sb.append("height = " + this.height).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = url == null ? 0 : url.hashCode();
            _hash = 31 * _hash + delta;
            delta = secureUrl == null ? 0 : secureUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            delta = width == null ? 0 : width.hashCode();
            _hash = 31 * _hash + delta;
            delta = height == null ? 0 : height.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
