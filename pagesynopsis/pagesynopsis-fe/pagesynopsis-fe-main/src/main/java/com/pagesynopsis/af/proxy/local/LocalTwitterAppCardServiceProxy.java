package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterAppCard;
// import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.proxy.TwitterAppCardServiceProxy;
import com.pagesynopsis.af.proxy.util.TwitterCardAppInfoProxyUtil;
import com.pagesynopsis.af.proxy.util.TwitterCardAppInfoProxyUtil;
import com.pagesynopsis.af.proxy.util.TwitterCardAppInfoProxyUtil;


public class LocalTwitterAppCardServiceProxy extends BaseLocalServiceProxy implements TwitterAppCardServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTwitterAppCardServiceProxy.class.getName());

    public LocalTwitterAppCardServiceProxy()
    {
    }

    @Override
    public TwitterAppCard getTwitterAppCard(String guid) throws BaseException
    {
        TwitterAppCard serverBean = getTwitterAppCardService().getTwitterAppCard(guid);
        TwitterAppCard appBean = convertServerTwitterAppCardBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getTwitterAppCard(String guid, String field) throws BaseException
    {
        return getTwitterAppCardService().getTwitterAppCard(guid, field);       
    }

    @Override
    public List<TwitterAppCard> getTwitterAppCards(List<String> guids) throws BaseException
    {
        List<TwitterAppCard> serverBeanList = getTwitterAppCardService().getTwitterAppCards(guids);
        List<TwitterAppCard> appBeanList = convertServerTwitterAppCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards() throws BaseException
    {
        return getAllTwitterAppCards(null, null, null);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterAppCardService().getAllTwitterAppCards(ordering, offset, count);
        return getAllTwitterAppCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterAppCard> serverBeanList = getTwitterAppCardService().getAllTwitterAppCards(ordering, offset, count, forwardCursor);
        List<TwitterAppCard> appBeanList = convertServerTwitterAppCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTwitterAppCardService().getAllTwitterAppCardKeys(ordering, offset, count);
        return getAllTwitterAppCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterAppCardService().getAllTwitterAppCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterAppCardService().findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TwitterAppCard> serverBeanList = getTwitterAppCardService().findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<TwitterAppCard> appBeanList = convertServerTwitterAppCardBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTwitterAppCardService().findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTwitterAppCardService().findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTwitterAppCardService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterAppCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        return getTwitterAppCardService().createTwitterAppCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp);
    }

    @Override
    public String createTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterAppCardBean serverBean =  convertAppTwitterAppCardBeanToServerBean(twitterAppCard);
        return getTwitterAppCardService().createTwitterAppCard(serverBean);
    }

    @Override
    public Boolean updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        return getTwitterAppCardService().updateTwitterAppCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp);
    }

    @Override
    public Boolean updateTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterAppCardBean serverBean =  convertAppTwitterAppCardBeanToServerBean(twitterAppCard);
        return getTwitterAppCardService().updateTwitterAppCard(serverBean);
    }

    @Override
    public Boolean deleteTwitterAppCard(String guid) throws BaseException
    {
        return getTwitterAppCardService().deleteTwitterAppCard(guid);
    }

    @Override
    public Boolean deleteTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        com.pagesynopsis.ws.bean.TwitterAppCardBean serverBean =  convertAppTwitterAppCardBeanToServerBean(twitterAppCard);
        return getTwitterAppCardService().deleteTwitterAppCard(serverBean);
    }

    @Override
    public Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException
    {
        return getTwitterAppCardService().deleteTwitterAppCards(filter, params, values);
    }




    public static TwitterAppCardBean convertServerTwitterAppCardBeanToAppBean(TwitterAppCard serverBean)
    {
        TwitterAppCardBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new TwitterAppCardBean();
            bean.setGuid(serverBean.getGuid());
            bean.setCard(serverBean.getCard());
            bean.setUrl(serverBean.getUrl());
            bean.setTitle(serverBean.getTitle());
            bean.setDescription(serverBean.getDescription());
            bean.setSite(serverBean.getSite());
            bean.setSiteId(serverBean.getSiteId());
            bean.setCreator(serverBean.getCreator());
            bean.setCreatorId(serverBean.getCreatorId());
            bean.setImage(serverBean.getImage());
            bean.setImageWidth(serverBean.getImageWidth());
            bean.setImageHeight(serverBean.getImageHeight());
            bean.setIphoneApp(TwitterCardAppInfoProxyUtil.convertServerTwitterCardAppInfoBeanToAppBean(serverBean.getIphoneApp()));
            bean.setIpadApp(TwitterCardAppInfoProxyUtil.convertServerTwitterCardAppInfoBeanToAppBean(serverBean.getIpadApp()));
            bean.setGooglePlayApp(TwitterCardAppInfoProxyUtil.convertServerTwitterCardAppInfoBeanToAppBean(serverBean.getGooglePlayApp()));
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterAppCard> convertServerTwitterAppCardBeanListToAppBeanList(List<TwitterAppCard> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterAppCard> beanList = new ArrayList<TwitterAppCard>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(TwitterAppCard sb : serverBeanList) {
                TwitterAppCardBean bean = convertServerTwitterAppCardBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.TwitterAppCardBean convertAppTwitterAppCardBeanToServerBean(TwitterAppCard appBean)
    {
        com.pagesynopsis.ws.bean.TwitterAppCardBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.TwitterAppCardBean();
            bean.setGuid(appBean.getGuid());
            bean.setCard(appBean.getCard());
            bean.setUrl(appBean.getUrl());
            bean.setTitle(appBean.getTitle());
            bean.setDescription(appBean.getDescription());
            bean.setSite(appBean.getSite());
            bean.setSiteId(appBean.getSiteId());
            bean.setCreator(appBean.getCreator());
            bean.setCreatorId(appBean.getCreatorId());
            bean.setImage(appBean.getImage());
            bean.setImageWidth(appBean.getImageWidth());
            bean.setImageHeight(appBean.getImageHeight());
            bean.setIphoneApp(TwitterCardAppInfoProxyUtil.convertAppTwitterCardAppInfoBeanToServerBean(appBean.getIphoneApp()));
            bean.setIpadApp(TwitterCardAppInfoProxyUtil.convertAppTwitterCardAppInfoBeanToServerBean(appBean.getIpadApp()));
            bean.setGooglePlayApp(TwitterCardAppInfoProxyUtil.convertAppTwitterCardAppInfoBeanToServerBean(appBean.getGooglePlayApp()));
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TwitterAppCard> convertAppTwitterAppCardBeanListToServerBeanList(List<TwitterAppCard> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<TwitterAppCard> beanList = new ArrayList<TwitterAppCard>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(TwitterAppCard sb : appBeanList) {
                com.pagesynopsis.ws.bean.TwitterAppCardBean bean = convertAppTwitterAppCardBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
