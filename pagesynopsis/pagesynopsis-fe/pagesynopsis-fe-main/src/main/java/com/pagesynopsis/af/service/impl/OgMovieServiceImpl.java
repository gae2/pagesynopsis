package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgMovieService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgMovieServiceImpl implements OgMovieService
{
    private static final Logger log = Logger.getLogger(OgMovieServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "OgMovie-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("OgMovie:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public OgMovieServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgMovie related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgMovie getOgMovie(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgMovie(): guid = " + guid);

        OgMovieBean bean = null;
        if(getCache() != null) {
            bean = (OgMovieBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (OgMovieBean) getProxyFactory().getOgMovieServiceProxy().getOgMovie(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "OgMovieBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgMovieBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgMovie(String guid, String field) throws BaseException
    {
        OgMovieBean bean = null;
        if(getCache() != null) {
            bean = (OgMovieBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (OgMovieBean) getProxyFactory().getOgMovieServiceProxy().getOgMovie(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "OgMovieBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgMovieBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return bean.getUrl();
        } else if(field.equals("type")) {
            return bean.getType();
        } else if(field.equals("siteName")) {
            return bean.getSiteName();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("fbAdmins")) {
            return bean.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return bean.getFbAppId();
        } else if(field.equals("image")) {
            return bean.getImage();
        } else if(field.equals("audio")) {
            return bean.getAudio();
        } else if(field.equals("video")) {
            return bean.getVideo();
        } else if(field.equals("locale")) {
            return bean.getLocale();
        } else if(field.equals("localeAlternate")) {
            return bean.getLocaleAlternate();
        } else if(field.equals("director")) {
            return bean.getDirector();
        } else if(field.equals("writer")) {
            return bean.getWriter();
        } else if(field.equals("actor")) {
            return bean.getActor();
        } else if(field.equals("duration")) {
            return bean.getDuration();
        } else if(field.equals("tag")) {
            return bean.getTag();
        } else if(field.equals("releaseDate")) {
            return bean.getReleaseDate();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgMovie> getOgMovies(List<String> guids) throws BaseException
    {
        log.fine("getOgMovies()");

        // TBD: Is there a better way????
        List<OgMovie> ogMovies = getProxyFactory().getOgMovieServiceProxy().getOgMovies(guids);
        if(ogMovies == null) {
            log.log(Level.WARNING, "Failed to retrieve OgMovieBean list.");
        }

        log.finer("END");
        return ogMovies;
    }

    @Override
    public List<OgMovie> getAllOgMovies() throws BaseException
    {
        return getAllOgMovies(null, null, null);
    }


    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgMovies(ordering, offset, count, null);
    }

    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgMovies(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgMovie> ogMovies = getProxyFactory().getOgMovieServiceProxy().getAllOgMovies(ordering, offset, count, forwardCursor);
        if(ogMovies == null) {
            log.log(Level.WARNING, "Failed to retrieve OgMovieBean list.");
        }

        log.finer("END");
        return ogMovies;
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgMovieKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgMovieKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getOgMovieServiceProxy().getAllOgMovieKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgMovieBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty OgMovieBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgMovieServiceImpl.findOgMovies(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgMovie> ogMovies = getProxyFactory().getOgMovieServiceProxy().findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(ogMovies == null) {
            log.log(Level.WARNING, "Failed to find ogMovies for the given criterion.");
        }

        log.finer("END");
        return ogMovies;
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgMovieServiceImpl.findOgMovieKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getOgMovieServiceProxy().findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgMovie keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty OgMovie key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgMovieServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getOgMovieServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgMovie(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        OgMovieBean bean = new OgMovieBean(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return createOgMovie(bean);
    }

    @Override
    public String createOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //OgMovie bean = constructOgMovie(ogMovie);
        //return bean.getGuid();

        // Param ogMovie cannot be null.....
        if(ogMovie == null) {
            log.log(Level.INFO, "Param ogMovie is null!");
            throw new BadRequestException("Param ogMovie object is null!");
        }
        OgMovieBean bean = null;
        if(ogMovie instanceof OgMovieBean) {
            bean = (OgMovieBean) ogMovie;
        } else if(ogMovie instanceof OgMovie) {
            // bean = new OgMovieBean(null, ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new OgMovieBean(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate());
        } else {
            log.log(Level.WARNING, "createOgMovie(): Arg ogMovie is of an unknown type.");
            //bean = new OgMovieBean();
            bean = new OgMovieBean(ogMovie.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getOgMovieServiceProxy().createOgMovie(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public OgMovie constructOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogMovie cannot be null.....
        if(ogMovie == null) {
            log.log(Level.INFO, "Param ogMovie is null!");
            throw new BadRequestException("Param ogMovie object is null!");
        }
        OgMovieBean bean = null;
        if(ogMovie instanceof OgMovieBean) {
            bean = (OgMovieBean) ogMovie;
        } else if(ogMovie instanceof OgMovie) {
            // bean = new OgMovieBean(null, ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new OgMovieBean(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate());
        } else {
            log.log(Level.WARNING, "createOgMovie(): Arg ogMovie is of an unknown type.");
            //bean = new OgMovieBean();
            bean = new OgMovieBean(ogMovie.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getOgMovieServiceProxy().createOgMovie(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateOgMovie(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgMovieBean bean = new OgMovieBean(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return updateOgMovie(bean);
    }
        
    @Override
    public Boolean updateOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //OgMovie bean = refreshOgMovie(ogMovie);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param ogMovie cannot be null.....
        if(ogMovie == null || ogMovie.getGuid() == null) {
            log.log(Level.INFO, "Param ogMovie or its guid is null!");
            throw new BadRequestException("Param ogMovie object or its guid is null!");
        }
        OgMovieBean bean = null;
        if(ogMovie instanceof OgMovieBean) {
            bean = (OgMovieBean) ogMovie;
        } else {  // if(ogMovie instanceof OgMovie)
            bean = new OgMovieBean(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate());
        }
        Boolean suc = getProxyFactory().getOgMovieServiceProxy().updateOgMovie(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public OgMovie refreshOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogMovie cannot be null.....
        if(ogMovie == null || ogMovie.getGuid() == null) {
            log.log(Level.INFO, "Param ogMovie or its guid is null!");
            throw new BadRequestException("Param ogMovie object or its guid is null!");
        }
        OgMovieBean bean = null;
        if(ogMovie instanceof OgMovieBean) {
            bean = (OgMovieBean) ogMovie;
        } else {  // if(ogMovie instanceof OgMovie)
            bean = new OgMovieBean(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate());
        }
        Boolean suc = getProxyFactory().getOgMovieServiceProxy().updateOgMovie(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteOgMovie(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getOgMovieServiceProxy().deleteOgMovie(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            OgMovie ogMovie = null;
            try {
                ogMovie = getProxyFactory().getOgMovieServiceProxy().getOgMovie(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch ogMovie with a key, " + guid);
                return false;
            }
            if(ogMovie != null) {
                String beanGuid = ogMovie.getGuid();
                Boolean suc1 = getProxyFactory().getOgMovieServiceProxy().deleteOgMovie(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("ogMovie with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogMovie cannot be null.....
        if(ogMovie == null || ogMovie.getGuid() == null) {
            log.log(Level.INFO, "Param ogMovie or its guid is null!");
            throw new BadRequestException("Param ogMovie object or its guid is null!");
        }
        OgMovieBean bean = null;
        if(ogMovie instanceof OgMovieBean) {
            bean = (OgMovieBean) ogMovie;
        } else {  // if(ogMovie instanceof OgMovie)
            // ????
            log.warning("ogMovie is not an instance of OgMovieBean.");
            bean = new OgMovieBean(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate());
        }
        Boolean suc = getProxyFactory().getOgMovieServiceProxy().deleteOgMovie(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOgMovies(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getOgMovieServiceProxy().deleteOgMovies(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgMovies(List<OgMovie> ogMovies) throws BaseException
    {
        log.finer("BEGIN");

        if(ogMovies == null) {
            log.log(Level.WARNING, "createOgMovies() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = ogMovies.size();
        if(size == 0) {
            log.log(Level.WARNING, "createOgMovies() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(OgMovie ogMovie : ogMovies) {
            String guid = createOgMovie(ogMovie);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createOgMovies() failed for at least one ogMovie. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateOgMovies(List<OgMovie> ogMovies) throws BaseException
    //{
    //}

}
