package com.pagesynopsis.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterProductCardService;


// TwitterProductCardMockService is a decorator.
// It can be used as a base class to mock TwitterProductCardService objects.
public abstract class TwitterProductCardMockService implements TwitterProductCardService
{
    private static final Logger log = Logger.getLogger(TwitterProductCardMockService.class.getName());

    // TwitterProductCardMockService uses the decorator design pattern.
    private TwitterProductCardService decoratedService;

    public TwitterProductCardMockService(TwitterProductCardService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected TwitterProductCardService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(TwitterProductCardService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // TwitterProductCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterProductCard getTwitterProductCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterProductCard(): guid = " + guid);
        TwitterProductCard bean = decoratedService.getTwitterProductCard(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterProductCard(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getTwitterProductCard(guid, field);
        return obj;
    }

    @Override
    public List<TwitterProductCard> getTwitterProductCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterProductCards()");
        List<TwitterProductCard> twitterProductCards = decoratedService.getTwitterProductCards(guids);
        log.finer("END");
        return twitterProductCards;
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards() throws BaseException
    {
        return getAllTwitterProductCards(null, null, null);
    }


    @Override
    public List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterProductCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterProductCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<TwitterProductCard> twitterProductCards = decoratedService.getAllTwitterProductCards(ordering, offset, count, forwardCursor);
        log.finer("END");
        return twitterProductCards;
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterProductCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterProductCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllTwitterProductCardKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterProductCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterProductCardMockService.findTwitterProductCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<TwitterProductCard> twitterProductCards = decoratedService.findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return twitterProductCards;
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterProductCardMockService.findTwitterProductCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterProductCardMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterProductCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException
    {
        TwitterCardProductDataBean data1Bean = null;
        if(data1 instanceof TwitterCardProductDataBean) {
            data1Bean = (TwitterCardProductDataBean) data1;
        } else if(data1 instanceof TwitterCardProductData) {
            data1Bean = new TwitterCardProductDataBean(data1.getData(), data1.getLabel());
        } else {
            data1Bean = null;   // ????
        }
        TwitterCardProductDataBean data2Bean = null;
        if(data2 instanceof TwitterCardProductDataBean) {
            data2Bean = (TwitterCardProductDataBean) data2;
        } else if(data2 instanceof TwitterCardProductData) {
            data2Bean = new TwitterCardProductDataBean(data2.getData(), data2.getLabel());
        } else {
            data2Bean = null;   // ????
        }
        TwitterProductCardBean bean = new TwitterProductCardBean(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1Bean, data2Bean);
        return createTwitterProductCard(bean);
    }

    @Override
    public String createTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createTwitterProductCard(twitterProductCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TwitterProductCard constructTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        log.finer("BEGIN");
        TwitterProductCard bean = decoratedService.constructTwitterProductCard(twitterProductCard);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTwitterProductCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterCardProductDataBean data1Bean = null;
        if(data1 instanceof TwitterCardProductDataBean) {
            data1Bean = (TwitterCardProductDataBean) data1;
        } else if(data1 instanceof TwitterCardProductData) {
            data1Bean = new TwitterCardProductDataBean(data1.getData(), data1.getLabel());
        } else {
            data1Bean = null;   // ????
        }
        TwitterCardProductDataBean data2Bean = null;
        if(data2 instanceof TwitterCardProductDataBean) {
            data2Bean = (TwitterCardProductDataBean) data2;
        } else if(data2 instanceof TwitterCardProductData) {
            data2Bean = new TwitterCardProductDataBean(data2.getData(), data2.getLabel());
        } else {
            data2Bean = null;   // ????
        }
        TwitterProductCardBean bean = new TwitterProductCardBean(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1Bean, data2Bean);
        return updateTwitterProductCard(bean);
    }
        
    @Override
    public Boolean updateTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateTwitterProductCard(twitterProductCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TwitterProductCard refreshTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        log.finer("BEGIN");
        TwitterProductCard bean = decoratedService.refreshTwitterProductCard(twitterProductCard);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTwitterProductCard(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterProductCard(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterProductCard(twitterProductCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterProductCards(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteTwitterProductCards(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterProductCards(List<TwitterProductCard> twitterProductCards) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createTwitterProductCards(twitterProductCards);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterProductCards(List<TwitterProductCard> twitterProductCards) throws BaseException
    //{
    //}

}
