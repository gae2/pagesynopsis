package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class DecodedQueryParamStructJsBean implements Serializable, Cloneable  //, DecodedQueryParamStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DecodedQueryParamStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String paramType;
    private String originalString;
    private String decodedString;
    private String note;

    // Ctors.
    public DecodedQueryParamStructJsBean()
    {
        //this((String) null);
    }
    public DecodedQueryParamStructJsBean(String paramType, String originalString, String decodedString, String note)
    {
        this.paramType = paramType;
        this.originalString = originalString;
        this.decodedString = decodedString;
        this.note = note;
    }
    public DecodedQueryParamStructJsBean(DecodedQueryParamStructJsBean bean)
    {
        if(bean != null) {
            setParamType(bean.getParamType());
            setOriginalString(bean.getOriginalString());
            setDecodedString(bean.getDecodedString());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static DecodedQueryParamStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        DecodedQueryParamStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(DecodedQueryParamStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, DecodedQueryParamStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getParamType()
    {
        return this.paramType;
    }
    public void setParamType(String paramType)
    {
        this.paramType = paramType;
    }

    public String getOriginalString()
    {
        return this.originalString;
    }
    public void setOriginalString(String originalString)
    {
        this.originalString = originalString;
    }

    public String getDecodedString()
    {
        return this.decodedString;
    }
    public void setDecodedString(String decodedString)
    {
        this.decodedString = decodedString;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getParamType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getOriginalString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDecodedString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("paramType:null, ");
        sb.append("originalString:null, ");
        sb.append("decodedString:null, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("paramType:");
        if(this.getParamType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getParamType()).append("\", ");
        }
        sb.append("originalString:");
        if(this.getOriginalString() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getOriginalString()).append("\", ");
        }
        sb.append("decodedString:");
        if(this.getDecodedString() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDecodedString()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getParamType() != null) {
            sb.append("\"paramType\":").append("\"").append(this.getParamType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"paramType\":").append("null, ");
        }
        if(this.getOriginalString() != null) {
            sb.append("\"originalString\":").append("\"").append(this.getOriginalString()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"originalString\":").append("null, ");
        }
        if(this.getDecodedString() != null) {
            sb.append("\"decodedString\":").append("\"").append(this.getDecodedString()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"decodedString\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("paramType = " + this.paramType).append(";");
        sb.append("originalString = " + this.originalString).append(";");
        sb.append("decodedString = " + this.decodedString).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        DecodedQueryParamStructJsBean cloned = new DecodedQueryParamStructJsBean();
        cloned.setParamType(this.getParamType());   
        cloned.setOriginalString(this.getOriginalString());   
        cloned.setDecodedString(this.getDecodedString());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
