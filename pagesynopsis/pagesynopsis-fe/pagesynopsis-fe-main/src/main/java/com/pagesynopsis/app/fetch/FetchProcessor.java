package com.pagesynopsis.app.fetch;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipException;

import org.w3c.dom.Node;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.RefreshHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlMeta;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.AudioSetBean;
import com.pagesynopsis.af.bean.FetchRequestBean;
import com.pagesynopsis.af.bean.ImageSetBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.LinkListBean;
import com.pagesynopsis.af.bean.PageFetchBean;
import com.pagesynopsis.af.bean.PageInfoBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.VideoSetBean;
import com.pagesynopsis.app.common.FetchResult;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;


// Note: HTMLUnit API.
// http://htmlunit.sourceforge.net/apidocs/index.html
public class FetchProcessor
{
    private static final Logger log = Logger.getLogger(FetchProcessor.class.getName());   
    
    // temporary
    private static final long REFRESH_INTERVAL = 3600 * 1000L * 24 * 7;   // a week
    private static final long MIN_REFRESH_INTERVAL = 1800 * 1000L;        // half an hour...
    // temporary
    // If createdTime is older than < now - MAX_REFRESHING_AGE_MILLIS
    // Do not refresh it any more...
    // This should really depend on the current refresh counter (if we keep track of it)
    //                           and/or refresh internal, etc....
    private static final long MAX_REFRESHING_AGE_MILLIS = 3600 * 1000L * 24 * 30;   // ~1 month

    
    // TBD:
    // Max refresh/fetch count and/or expiration time, etc...
    // Without this, 
    // it's kind of dangerous to run every request indefinitely...
    // ...
    

    // Max entity size on GAE data store/memcache is 1M.  (1 char == 2 bytes)
    private static final int MAX_OUTPUT_LENGTH = 450000;  // ???

    
    private FetchProcessor()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class FetchProcessorHolder
    {
        private static final FetchProcessor INSTANCE = new FetchProcessor();
    }

    // Singleton method
    public static FetchProcessor getInstance()
    {
        return FetchProcessorHolder.INSTANCE;
    }

    private void init()
    {
        // TBD:
        // ...
    }


    
    // TBD
    // refresh meta fails for htmlunit on gae...
    // e.g., <meta http-equiv="refresh" content="1800;url=http://www.cnn.com/?refresh=1"/>
    // ...
    // Error message: Refresh to http://www.cnn.com/?refresh=1 (1800s) aborted by HtmlUnit: Attempted to refresh a page using an ImmediateRefreshHandler which could have caused an OutOfMemoryError Please use WaitingRefreshHandler or ThreadedRefreshHandler instead.
    // ...
    
    
    // TBD: HTMLUnit Bug...
    // This URL: http://www.pagesynopsis.com/pageinfo?targetUrl=http%3A%2F%2Fwww.theverge.com%2F
    // throws ZIPException....
    // ...
    
    // TBD:
    // Need to check status code and for some status codes, periodic "refresh" should not happen...
    // e.g., 403 forbidden...
    // http://www.pagesynopsis.com/pageinfo?targetUrl=http%3A%2F%2Fwww.sintelevisor.com%2Ftv%2Fcanal13.html
    
    // TBD:
    // Private/authenticated fetch (private to requester only) ????
    // ....
    
    

    public PageInfo processPageInfoFetch(PageInfo pageInfo) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processPageFetch() called with pageInfo = " + pageInfo);
        if(pageInfo == null) {
            // ????
            log.warning("pageInfo is null.");
            throw new BadRequestException("pageInfo is null.");
        }

        // TargetUrl/input cannot be null
        String targetUrl =  pageInfo.getTargetUrl();
        // ...
                
        //final WebClient webClient = new WebClient();
        final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_3_6);  // ???
        // http://htmlunit.sourceforge.net/cgi-bin/browserVersion
        // new BrowserVersion( "Netscape", "5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.52 Safari/536.5", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.52 Safari/536.5", "1.2" );
        // ...
        webClient.setCssEnabled(false);               // So many CSS/JS related errors on GAE.
        webClient.setJavaScriptEnabled(false);        // So many CSS/JS related errors on GAE.
        webClient.setPopupBlockerEnabled(true);       // ...
        webClient.setTimeout(30000);                  // Does this work on GAE????
        webClient.setRedirectEnabled(true);           // temporary.... Use redirect request field... ???
        
        // ????
        //webClient.setRefreshHandler(new WaitingRefreshHandler(1));  // ???
        // Same page refresh (as opposed to the refresh used for redirect) normally does not terminate.
        // There is really no good solution other than disabling it....
        webClient.setRefreshHandler(new RefreshHandler() {
            @Override
            public void handleRefresh(Page page, URL url, int seconds) throws IOException
            {
                // Do nothing.
            }
        });
        
        int statusCode = -1;  // ???
        try {
            // Starting processing...
            long now = System.currentTimeMillis();
            ((PageInfoBean) pageInfo).setLastCheckedTime(now);
            ((PageInfoBean) pageInfo).setLastFetchResult(FetchResult.RES_UNKNOWN);  // ???
            //((PageInfoBean) pageInfo).setRefreshStatus(RefreshStatus.STATUS_PROCESSING);  // ???
            
            log.finer("Before calling getPage()");


            // TBD: text pages??? xml pages???
            //final HtmlPage targetPage = webClient.getPage(targetUrl);
            final HtmlPage targetPage = webClient.getPage(new URL(targetUrl));  // ???
            
            log.finer("After calling getPage()");
            
            if(targetPage != null) {

                // TBD: ...
                WebResponse webResponse = targetPage.getWebResponse();
                statusCode = webResponse.getStatusCode();
                ((PageInfoBean) pageInfo).setResponseCode(statusCode);
                if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for targetUrl, " + targetUrl);
                
                String contentType = webResponse.getContentType();
                ((PageInfoBean) pageInfo).setContentType(contentType);
                //int contentLength = webResponse.getContentLength();
                //((PageInfoBean) pageInfo).setContentLength(contentLength);
                String contentLanguage = webResponse.getResponseHeaderValue("Content-Language");
                // String contentLanguage = webResponse.getContentCharset();  // ???
                ((PageInfoBean) pageInfo).setLanguage(contentLanguage);
                // ...
                
                String pageTitle = targetPage.getTitleText();
                log.info("pageTitle = " + pageTitle);
                if(pageTitle != null && pageTitle.length() > 500) {
                    String truncatedTitle = pageTitle.substring(0, 500);
                    log.info("Title has been truncated: Original pageTitle = " + pageTitle);
                    log.info("Truncated pageTitle = " + truncatedTitle);
                    pageTitle = truncatedTitle;
                }
                ((PageInfoBean) pageInfo).setPageTitle(pageTitle);
    
                @SuppressWarnings("unchecked")
                List<HtmlMeta> metaDescs = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='description']");
                if(metaDescs != null && !metaDescs.isEmpty()) {
                    String pageDescription = metaDescs.get(0).getAttribute("content");
                    if(pageDescription != null && !pageDescription.isEmpty()) {
                        ((PageInfoBean) pageInfo).setPageDescription(pageDescription.trim());  // trim ???
                    }
                }
                
                @SuppressWarnings("unchecked")
                List<HtmlMeta> metaAuthors = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='author']");
                if(metaAuthors != null && !metaAuthors.isEmpty()) {
                    String pageAuthor = metaAuthors.get(0).getAttribute("content");
                    if(pageAuthor != null && !pageAuthor.isEmpty()) {
                        ((PageInfoBean) pageInfo).setPageAuthor(pageAuthor.trim());  // trim ???
                    }
                }
                
                
                // TBD: favicon, etc...
                //List<HtmlMeta> metaImages = (List<HtmlMeta>) targetPage.getByXPath("//meta[@itemprop='image']");     // ???
                //List<HtmlLink> linkIcons = (List<HtmlLink>) targetPage.getByXPath("//link[@rel='shortcut icon']");   // ???
                // ...
                
                
   
                ((PageInfoBean) pageInfo).setLastFetchResult(FetchResult.RES_SUCCESS);
                ((PageInfoBean) pageInfo).setLastUpdatedTime(now);  // ???
                // etc...
                
            } else {
                // TBD ....
                ((PageInfoBean) pageInfo).setLastFetchResult(FetchResult.RES_FAILURE);
                // etc...
                // What to do???
                // ????
            }

            // TBD:
            // Update these only if RefreshStatus != STATUS_NOREFRESH???
            Integer refreshStatus = pageInfo.getRefreshStatus();
            if(refreshStatus ==  null || refreshStatus != RefreshStatus.STATUS_NOREFRESH) {
                Long createdTime = pageInfo.getCreatedTime();
                if(createdTime != null && createdTime < now - MAX_REFRESHING_AGE_MILLIS) {
                    // The record is too old, no more refreshing...
                    ((PageInfoBean) pageInfo).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                    // ((PageInfoBean) pageInfo).setNextRefreshTime(Long.MAX_VALUE);  // ????
                    ((PageInfoBean) pageInfo).setNextRefreshTime(0L);   // ???
                    // ...
                } else {
                    ((PageInfoBean) pageInfo).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
                    Long refreshInterval = pageInfo.getRefreshInterval();
                    if(refreshInterval == null || refreshInterval < MIN_REFRESH_INTERVAL) {
                        if(refreshInterval == null) {
                            if(log.isLoggable(Level.INFO)) log.info("refreshInterval is set to the default value: " + REFRESH_INTERVAL);
                            refreshInterval = REFRESH_INTERVAL;  // Default value
                        } else if(refreshInterval < MIN_REFRESH_INTERVAL) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Input refreshInterval is too smmal, " + refreshInterval + ". Resetting it to the minimum value: " + MIN_REFRESH_INTERVAL);
                            refreshInterval = MIN_REFRESH_INTERVAL;
                        }
                        ((PageInfoBean) pageInfo).setRefreshInterval(refreshInterval);
                    }
                    ((PageInfoBean) pageInfo).setNextRefreshTime(now + refreshInterval);
                }
            } else {
                // This should not happen.... (because we filter out records based on refreshStatus....)
                log.warning("Invalid refreshStatus for pageInfo = " + pageInfo.getGuid());
                throw new InternalServerErrorException("Invalid refreshStatus for pageInfo = " + pageInfo.getGuid());
            }
            // ...

        } catch (MalformedURLException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (ZipException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (IOException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (FailingHttpStatusCodeException e) {
            // TBD:
            // Handle 404, etc... ????
            // (what if it was just temporary 404 ????)
            if(statusCode == 406) {
                // temporary
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Authentication required for this page: statusCode= " + statusCode, e);
                ((PageInfoBean) pageInfo).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                ((PageInfoBean) pageInfo).setNextRefreshTime(0L);   // ???
            } else {
                throw new BaseException("Error processing targetUrl = " + targetUrl, e);
            }
        } catch (Exception e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } finally{
            // ???
            try {
                webClient.closeAllWindows();
            } catch (Exception e) {
                // ????
                //throw new BaseException("Error while closing HTMLUnit WebClient.", e);
                log.log(Level.WARNING, "Error while closing HTMLUnit WebClient.", e);  // ignore...
            }
        }
        
        log.finer("END: processPageFetch()");
        return pageInfo;
    }
    
    

    public PageFetch processPageFetchFetch(PageFetch pageFetch) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processPageFetch() called with pageFetch = " + pageFetch);
        if(pageFetch == null) {
            log.warning("pageFetch is null.");
            throw new BadRequestException("pageFetch is null.");
        }

        // TargetUrl/input cannot be null
        String targetUrl =  pageFetch.getTargetUrl();
        if(targetUrl == null || targetUrl.isEmpty()) {   // TBD: Validate targetUrl??
            log.warning("targetUrl is invalid.");
            throw new BadRequestException("targetUrl is invalid.");
        }
        if(log.isLoggable(Level.INFO)) log.info("targetUrl = " + targetUrl);
        
        Integer inputMaxRedirects = pageFetch.getInputMaxRedirects();
        if(inputMaxRedirects == null || inputMaxRedirects < 0) {
            inputMaxRedirects = 0;
        }
        if(log.isLoggable(Level.FINE)) log.fine("inputMaxRedirects = " + inputMaxRedirects);
        // ...

        //final WebClient webClient = new WebClient();
        final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_3_6);  // ???
        // http://htmlunit.sourceforge.net/cgi-bin/browserVersion
        // new BrowserVersion( "Netscape", "5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.52 Safari/536.5", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.52 Safari/536.5", "1.2" );
        // ...
        webClient.setCssEnabled(false);               // So many CSS/JS related errors on GAE.
        webClient.setJavaScriptEnabled(false);        // So many CSS/JS related errors on GAE.
        webClient.setPopupBlockerEnabled(true);       // ...
        webClient.setTimeout(30000);                  // Does this work on GAE????
        webClient.setThrowExceptionOnFailingStatusCode(false);
        // TBD:
        // Note that this does not work on GAE
        // because the underlying UrlFetch by default follows the redirect up to 5 times...
        // Look at the workaround below...
        webClient.setRedirectEnabled(false);          // Explicit redirect handling for PageFetch, unlike for the rest of the object types....
        
        // ????
        //webClient.setRefreshHandler(new WaitingRefreshHandler(1));  // ???
        // Same page refresh (as opposed to the refresh used for redirect) normally does not terminate.
        // There is really no good solution other than disabling it....
        webClient.setRefreshHandler(new RefreshHandler() {
            @Override
            public void handleRefresh(Page page, URL url, int seconds) throws IOException
            {
                // Do nothing.
                if(log.isLoggable(Level.WARNING)) log.warning("Refresh handler called: url = " + url + "; seconds = " + seconds + "; page = " + page);
            }
        });
        
        
        int statusCode = -1;    // ???
        String currentTargetUrl = targetUrl;  // For while loop....
        try {
            // Starting processing...
            long now = System.currentTimeMillis();
            ((PageFetchBean) pageFetch).setLastCheckedTime(now);
            ((PageFetchBean) pageFetch).setLastFetchResult(FetchResult.RES_UNKNOWN);  // ???
            //((PageFetchBean) pageFetch).setRefreshStatus(RefreshStatus.STATUS_PROCESSING);  // ???

            
            // Begin of while loop...
            int redirectCounter = -1;
            while(++redirectCounter <= inputMaxRedirects) {   // redirectCounter starts from 0.

                if(log.isLoggable(Level.FINE)) log.fine("Before getting response code: redirectCounter = " + redirectCounter);
                ((PageFetchBean) pageFetch).setResultRedirectCount(redirectCounter);

                URL currentTargetUrlURL = new URL(currentTargetUrl);                
                HttpURLConnection conn = (HttpURLConnection) currentTargetUrlURL.openConnection();
                conn.setInstanceFollowRedirects(false);       // <--- We need to do this to stop automatic redirect follow....
                conn.setConnectTimeout(30 * 1000);   // ????
                conn.setRequestMethod("GET");
                //conn.connect();
                statusCode = conn.getResponseCode();
                ((PageFetchBean) pageFetch).setResponseCode(statusCode);
                if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for currentTargetUrl, " + currentTargetUrl);
                
                String contentType = conn.getContentType();
                ((PageFetchBean) pageFetch).setContentType(contentType);
                int contentLength = conn.getContentLength();
                ((PageFetchBean) pageFetch).setContentLength(contentLength);
                String contentLanguage = conn.getHeaderField("Content-Language");
                ((PageFetchBean) pageFetch).setLanguage(contentLanguage);

                String redirectLocation = conn.getHeaderField("Location");
                
                conn.disconnect();
                if(log.isLoggable(Level.FINE)) log.fine("After getting response code: redirectCounter = " + redirectCounter);

                if(StatusCode.isRedirection(statusCode)) {

                    UrlStructBean uriStruct = new UrlStructBean();
                    String uuid = GUID.generate();
                    uriStruct.setUuid(uuid);
                    uriStruct.setStatusCode(statusCode);

                    String redirectAbsoluteUrl = null;
                    if(redirectLocation != null && !redirectLocation.isEmpty()) {  // TBD: Validate?
                        if(log.isLoggable(Level.FINE)) log.fine("redirectLocation = " + redirectLocation);
                        uriStruct.setRedirectUrl(redirectLocation);
                        
                        // temporary
                        redirectAbsoluteUrl = FetchUtil.buildAbsoluteUrl(redirectLocation, currentTargetUrl);
                        if(redirectAbsoluteUrl != null && !redirectAbsoluteUrl.isEmpty()) {   // TBD: Validate???
                            if(log.isLoggable(Level.INFO)) log.info("redirectAbsoluteUrl = " + redirectAbsoluteUrl);
                            uriStruct.setAbsoluteUrl(redirectAbsoluteUrl);
                        } else {
                            // Can this happen????
                            if(log.isLoggable(Level.WARNING)) log.warning("Invalid redirect URL = " + redirectAbsoluteUrl);
                            ((PageFetchBean) pageFetch).setLastFetchResult(FetchResult.RES_FAILURE);
                            break;
                        }
                        // ...
                    } else {
                        // Error. What to do???
                        if(log.isLoggable(Level.WARNING)) log.warning("Invalid location header for statusCode = " + statusCode);
                        ((PageFetchBean) pageFetch).setLastFetchResult(FetchResult.RES_FAILURE);
                        break;
                    }

                    List<UrlStruct> redirectPages = pageFetch.getRedirectPages();
                    if(redirectPages == null) {
                        redirectPages = new ArrayList<UrlStruct>();
                    }
                    redirectPages.add(uriStruct);
                    ((PageFetchBean) pageFetch).setRedirectPages(redirectPages);

                    // ???
                    if(redirectCounter == 0) {   // Only for the first redirect...
                        ((PageFetchBean) pageFetch).setRedirect(Integer.toString(statusCode));
                        ((PageFetchBean) pageFetch).setLocation(redirectLocation);
                    }
                    // ...

                    if(redirectCounter < inputMaxRedirects) {
                        // ??? 
                        if(log.isLoggable(Level.INFO)) log.info("Following redirectAbsoluteUrl... " + redirectAbsoluteUrl);
                        currentTargetUrl = redirectAbsoluteUrl;
                        continue; // ???
                    } else {
                        // fail..  what to do???
                        if(log.isLoggable(Level.WARNING)) log.warning("Failed after redirectCounter " + redirectCounter + "; inputMaxRedirects = " + inputMaxRedirects);
                        ((PageFetchBean) pageFetch).setLastFetchResult(FetchResult.RES_FAILURE);
                        break;
                    }
                } else if(StatusCode.isSuccessful(statusCode)) { 

                    // Try one more time...
                    if(log.isLoggable(Level.FINE)) log.fine("Before calling getPage(): redirectCounter = " + redirectCounter);                    
                    // TBD: text pages??? xml pages???
                    final HtmlPage targetPage = webClient.getPage(currentTargetUrl);
                    if(log.isLoggable(Level.FINE)) log.fine("After calling getPage(): redirectCounter = " + redirectCounter);

                    if(targetPage != null) {
                        
                        // TBD...
                        ((PageFetchBean) pageFetch).setDestinationUrl(currentTargetUrl);
                        // ...

                        String pageTitle = targetPage.getTitleText();
                        if(log.isLoggable(Level.INFO)) log.info("pageTitle = " + pageTitle);
                        if(pageTitle != null && pageTitle.length() > 500) {
                            String truncatedTitle = pageTitle.substring(0, 500);
                            if(log.isLoggable(Level.INFO)) log.info("Title has been truncated: Original pageTitle = " + pageTitle);
                            if(log.isLoggable(Level.INFO)) log.info("Truncated pageTitle = " + truncatedTitle);
                            pageTitle = truncatedTitle;
                        }
                        ((PageFetchBean) pageFetch).setPageTitle(pageTitle);
            
                        @SuppressWarnings("unchecked")
                        List<HtmlMeta> metaDescs = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='description']");
                        if(metaDescs != null && !metaDescs.isEmpty()) {
                            String pageSummary = metaDescs.get(0).getAttribute("content");
                            if(log.isLoggable(Level.FINER)) log.finer("pageSummary = " + pageSummary);
                            if(pageSummary != null && !pageSummary.isEmpty()) {
                                ((PageFetchBean) pageFetch).setPageSummary(pageSummary.trim());  // trim ???
                            } else {
                                // ???
                            }
                        } else {
                            log.fine("No description found.");
                        }
                        
                        @SuppressWarnings("unchecked")
                        List<HtmlMeta> metaAuthors = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='author']");
                        if(metaAuthors != null && !metaAuthors.isEmpty()) {
                            String pageAuthor = metaAuthors.get(0).getAttribute("content");
                            if(log.isLoggable(Level.FINER)) log.finer("pageAuthor = " + pageAuthor);
                            if(pageAuthor != null && !pageAuthor.isEmpty()) {
                                ((PageFetchBean) pageFetch).setPageAuthor(pageAuthor.trim());  // trim ???
                            } else {
                                // ???
                            }
                        } else {
                            log.fine("No author found.");
                        }
                        
                        
                        // TBD: favicon, etc...
                        //List<HtmlMeta> metaImages = (List<HtmlMeta>) targetPage.getByXPath("//meta[@itemprop='image']");     // ???
                        //List<HtmlLink> linkIcons = (List<HtmlLink>) targetPage.getByXPath("//link[@rel='shortcut icon']");   // ???
                        // ...
                        
                       
//                        UrlStructBean finalPageUrl = new UrlStructBean();
//                        String uuid = GUID.generate();
//                        finalPageUrl.setUuid(uuid);
//                        finalPageUrl.setStatusCode(statusCode);
//                        // finalPageUrl.setRedirectUrl(redirectUrl);
//                        // finalPageUrl.setAbsoluteUrl(absoluteUrl);
//                        // ....
//                        ((PageFetchBean) pageFetch).setFinalPage(finalPageUrl);

                        ((PageFetchBean) pageFetch).setLastFetchResult(FetchResult.RES_SUCCESS);
                        ((PageFetchBean) pageFetch).setLastUpdatedTime(now);  // ???

                        if(log.isLoggable(Level.INFO)) log.info("PageFetch succeeded for currentTargetUrl " + currentTargetUrl);
                        break;
                    } else {
                        // ???? What to do??
                        if(log.isLoggable(Level.INFO)) log.info("PageFetch failed for currentTargetUrl " + currentTargetUrl);
                        ((PageFetchBean) pageFetch).setLastFetchResult(FetchResult.RES_FAILURE);
                        break;
                    }
                } else {
                    // ???? What to do??
                    if(log.isLoggable(Level.INFO)) log.info("PageFetch failed for currentTargetUrl " + currentTargetUrl);
                    ((PageFetchBean) pageFetch).setLastFetchResult(FetchResult.RES_FAILURE);
                    break;
                }

            }  // End of while
            
            // TBD:
            // Update these only if RefreshStatus != STATUS_NOREFRESH???
            Integer refreshStatus = pageFetch.getRefreshStatus();
            if(refreshStatus ==  null || refreshStatus != RefreshStatus.STATUS_NOREFRESH) {
                Long createdTime = pageFetch.getCreatedTime();
                if(createdTime != null && createdTime < now - MAX_REFRESHING_AGE_MILLIS) {
                    // The record is too old, no more refreshing...
                    ((PageFetchBean) pageFetch).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                    // ((PageFetchBean) pageFetch).setNextRefreshTime(Long.MAX_VALUE);  // ????
                    ((PageFetchBean) pageFetch).setNextRefreshTime(0L);   // ???
                    // ...
                } else {
                    ((PageFetchBean) pageFetch).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
                    Long refreshInterval = pageFetch.getRefreshInterval();
                    if(refreshInterval == null || refreshInterval < MIN_REFRESH_INTERVAL) {
                        if(refreshInterval == null) {
                            if(log.isLoggable(Level.INFO)) log.info("refreshInterval is set to the default value: " + REFRESH_INTERVAL);
                            refreshInterval = REFRESH_INTERVAL;  // Default value
                        } else if(refreshInterval < MIN_REFRESH_INTERVAL) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Input refreshInterval is too smmal, " + refreshInterval + ". Resetting it to the minimum value: " + MIN_REFRESH_INTERVAL);
                            refreshInterval = MIN_REFRESH_INTERVAL;
                        }
                        ((PageFetchBean) pageFetch).setRefreshInterval(refreshInterval);
                    }
                    ((PageFetchBean) pageFetch).setNextRefreshTime(now + refreshInterval);
                }
            } else {
                // This should not happen.... (because we filter out records based on refreshStatus....)
                log.warning("Invalid refreshStatus for pageFetch = " + pageFetch.getGuid());
                throw new InternalServerErrorException("Invalid refreshStatus for pageFetch = " + pageFetch.getGuid());
            }
            // ...

        } catch (MalformedURLException e) {
            throw new BaseException("Error processing currentTargetUrl = " + currentTargetUrl, e);
        } catch (ZipException e) {
            throw new BaseException("Error processing currentTargetUrl = " + currentTargetUrl, e);
        } catch (IOException e) {
            throw new BaseException("Error processing currentTargetUrl = " + currentTargetUrl, e);
        } catch (FailingHttpStatusCodeException e) {
            // TBD:
            // Handle 404, etc... ????
            // (what if it was just temporary 404 ????)
            if(statusCode == 406) {
                // temporary
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Authentication required for this page: statusCode= " + statusCode, e);
                ((PageFetchBean) pageFetch).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                ((PageFetchBean) pageFetch).setNextRefreshTime(0L);   // ???
            } else {
                throw new BaseException("Error processing currentTargetUrl = " + currentTargetUrl, e);
            }
        } catch (Exception e) {
            throw new BaseException("Error processing currentTargetUrl = " + currentTargetUrl, e);
        } finally{
            // ???
            try {
                webClient.closeAllWindows();
            } catch (Exception e) {
                // ????
                //throw new BaseException("Error while closing HTMLUnit WebClient.", e);
                log.log(Level.WARNING, "Error while closing HTMLUnit WebClient.", e);  // ignore...
            }
        }
        
        log.finer("END: processPageFetch()");
        return pageFetch;
    }
    
   

    public LinkList processLinkFetch(LinkList linkList) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processLinkFetch() called with linkList = " + linkList);
        if(linkList == null) {
            // ????
            log.warning("linkList is null.");
            throw new BadRequestException("linkList is null.");
        }

        // TargetUrl/input cannot be null
        String targetUrl =  linkList.getTargetUrl();
        // ...
                
        //final WebClient webClient = new WebClient();
        final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_3_6);  // ???
        webClient.setCssEnabled(false);               // So many CSS/JS related errors on GAE.
        webClient.setJavaScriptEnabled(false);        // So many CSS/JS related errors on GAE.
        webClient.setPopupBlockerEnabled(true);       // ...
        webClient.setTimeout(30000);                  // Does this work on GAE????
        webClient.setRedirectEnabled(true);           // temporary.... Use redirect request field... ???
        
        // ????
        //webClient.setRefreshHandler(new WaitingRefreshHandler(1));  // ???
        // Same page refresh (as opposed to the refresh used for redirect) normally does not terminate.
        // There is really no good solution other than disabling it....
        webClient.setRefreshHandler(new RefreshHandler() {
            @Override
            public void handleRefresh(Page page, URL url, int seconds) throws IOException
            {
                // Do nothing.
            }
        });
        
        int statusCode = -1;  // ???
        try {
            // Starting processing...
            long now = System.currentTimeMillis();
            ((LinkListBean) linkList).setLastCheckedTime(now);
            ((LinkListBean) linkList).setLastFetchResult(FetchResult.RES_UNKNOWN);  // ???
            //((LinkListBean) linkList).setRefreshStatus(RefreshStatus.STATUS_PROCESSING);  // ???
            log.finer("Before calling getPage()");
            // TBD: text pages??? xml pages???
            final HtmlPage targetPage = webClient.getPage(targetUrl);
            //final HtmlPage targetPage = webClient.getPage(new URL(targetUrl));  // ???
            log.finer("After calling getPage()");
            
            if(targetPage != null) {
                // TBD: ...
                WebResponse webResponse = targetPage.getWebResponse();
                statusCode = webResponse.getStatusCode();
                ((LinkListBean) linkList).setResponseCode(statusCode);
                if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for targetUrl, " + targetUrl);

                String pageTitle = targetPage.getTitleText();
                log.info("pageTitle = " + pageTitle);
                if(pageTitle != null && pageTitle.length() > 500) {
                    String truncatedTitle = pageTitle.substring(0, 500);
                    log.info("Title has been truncated: Original pageTitle = " + pageTitle);
                    log.info("Truncated pageTitle = " + truncatedTitle);
                    pageTitle = truncatedTitle;
                }
                ((LinkListBean) linkList).setPageTitle(pageTitle);

//                @SuppressWarnings("unchecked")
//                List<HtmlMeta> metaDescs = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='description']");
//                if(metaDescs != null && !metaDescs.isEmpty()) {
//                    String pageDescription = metaDescs.get(0).getAttribute("content");
//                    if(pageDescription != null && !pageDescription.isEmpty()) {
//                        ((LinkListBean) linkList).setPageDescription(pageDescription.trim());  // trim ???
//                    }
//                }
                

                List<AnchorStruct> links = new ArrayList<AnchorStruct>();
                List<HtmlAnchor> anchors = targetPage.getAnchors();
                if(anchors != null && !anchors.isEmpty()) {
                    for(HtmlAnchor a : anchors) {
                        String href = a.getHrefAttribute();
                        if(href != null && !href.isEmpty()) {
                            AnchorStructBean link = new AnchorStructBean();

                            if(href.length() > 500) {
                                String truncatedHref = href.substring(0, 500);
                                log.info("Href has been truncated: Original href = " + href);
                                log.info("Truncated href = " + truncatedHref);
                                href = truncatedHref;
                            }
                            link.setHref(href);

                            //String baseURI = a.getBaseURI();  // <- getBaseURI() not implemented... ????
                            // TBD: Convert relative url to absolute url ???
                            // e.g., "#" -> baseurl + "#",
                            //       "/contact" -> baseurl + "contact", etc...
                            // ...
                            
                            // temporary
                            String hrefUrl = FetchUtil.buildAbsoluteUrl(href, targetUrl);
                            if(hrefUrl != null && !hrefUrl.isEmpty()) {
                                link.setHrefUrl(hrefUrl);
                            }

                            // Uuid for DataStore...
                            String uuid = GUID.generate();
                            link.setUuid(uuid);
                            // Unique Id
                            String id = a.getId();
                            if(id == null || id.isEmpty()) {
                                //id = GUID.generate().substring(0, 8);
                                id = uuid.substring(0, 8);
                            }
                            link.setId(id);

                            String name = a.getNameAttribute();
                            if(name != null) {
                                link.setName(name);
                            }
                            
                            // Etc.
                            // ...
                            

                            DomNode child = a.getFirstChild();
                            if(child != null) {
                                // TBD: Should get the child of child, etc. until you get the "leaf"
                                // ...
                                String text = child.getNodeValue();
                                if(text != null) {
                                    if(text.length() > 500) {
                                        text = text.substring(0, 500);
                                    }
                                    // TBD:
                                    link.setInnerHtml(text);
                                    if(child.getNodeType() == Node.TEXT_NODE) {  // ???
                                        link.setAnchorText(text);
                                    } else {
                                        DomNode grandChild = child.getFirstChild();
                                        if(grandChild != null) {
                                            if(grandChild.getNodeType() == Node.TEXT_NODE) {
                                                text = grandChild.getNodeValue();
                                                if(text != null) {
                                                    if(text.length() > 500) {
                                                        text = text.substring(0, 500);
                                                    }
                                                    link.setAnchorText(text);
                                                }
                                            } else {
                                                // Continue ... 
                                                // ???
                                                // ...
                                            }
                                        }
                                    }
                                    // ...
                                }
                            }
                            
                            // ...
                            
                            links.add(link);
                            
                        } else {
                            // skip...
                            log.info("Href is null or empty.");  // ???
                        }
                    }                    
                }
                ((LinkListBean) linkList).setPageAnchors(links);
                // ....
                
                
                ((LinkListBean) linkList).setLastFetchResult(FetchResult.RES_SUCCESS);
                ((LinkListBean) linkList).setLastUpdatedTime(now);  // ???
                // etc...
                
            } else {
                // TBD ....
                ((LinkListBean) linkList).setLastFetchResult(FetchResult.RES_FAILURE);
                // etc...
                // What to do???
                // ????
            }

            // TBD:
            // Update these only if RefreshStatus != STATUS_NOREFRESH???
            Integer refreshStatus = linkList.getRefreshStatus();
            if(refreshStatus ==  null || refreshStatus != RefreshStatus.STATUS_NOREFRESH) {
                Long createdTime = linkList.getCreatedTime();
                if(createdTime != null && createdTime < now - MAX_REFRESHING_AGE_MILLIS) {
                    // The record is too old, no more refreshing...
                    ((LinkListBean) linkList).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                    // ((LinkListBean) linkList).setNextRefreshTime(Long.MAX_VALUE);  // ????
                    ((LinkListBean) linkList).setNextRefreshTime(0L);   // ???
                    // ...
                } else {
                    ((LinkListBean) linkList).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
                    Long refreshInterval = linkList.getRefreshInterval();
                    if(refreshInterval == null || refreshInterval < MIN_REFRESH_INTERVAL) {
                        if(refreshInterval == null) {
                            if(log.isLoggable(Level.INFO)) log.info("refreshInterval is set to the default value: " + REFRESH_INTERVAL);
                            refreshInterval = REFRESH_INTERVAL;  // Default value
                        } else if(refreshInterval < MIN_REFRESH_INTERVAL) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Input refreshInterval is too smmal, " + refreshInterval + ". Resetting it to the minimum value: " + MIN_REFRESH_INTERVAL);
                            refreshInterval = MIN_REFRESH_INTERVAL;
                        }
                        ((LinkListBean) linkList).setRefreshInterval(refreshInterval);
                    }
                    ((LinkListBean) linkList).setNextRefreshTime(now + refreshInterval);
                }
            } else {
                // This should not happen.... (because we filter out records based on refreshStatus....)
                log.warning("Invalid refreshStatus for linkList = " + linkList.getGuid());
                throw new InternalServerErrorException("Invalid refreshStatus for linkList = " + linkList.getGuid());
            }
            // ...

        } catch (MalformedURLException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (ZipException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (IOException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (FailingHttpStatusCodeException e) {
            // TBD:
            // Handle 404, etc... ????
            // (what if it was just temporary 404 ????)
            if(statusCode == 406) {
                // temporary
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Authentication required for this page: statusCode= " + statusCode, e);
                ((LinkListBean) linkList).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                ((LinkListBean) linkList).setNextRefreshTime(0L);   // ???
            } else {
                throw new BaseException("Error processing targetUrl = " + targetUrl, e);
            }
        } catch (Exception e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } finally{
            // ???
            try {
                webClient.closeAllWindows();
            } catch (Exception e) {
                // ????
                //throw new BaseException("Error while closing HTMLUnit WebClient.", e);
                log.log(Level.WARNING, "Error while closing HTMLUnit WebClient.", e);  // ignore...
            }
        }
        
        log.finer("END: processLinkFetch()");
        return linkList;
    }


    
    
    // TBD:
    // Get images "embedded" in a style (e.g., background-image.url...) as well????
    // ....
    
    public ImageSet processImageFetch(ImageSet imageSet) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processImageFetch() called with imageSet = " + imageSet);
        if(imageSet == null) {
            // ????
            log.warning("imageSet is null.");
            throw new BadRequestException("imageSet is null.");
        }

        // TargetUrl/input cannot be null
        String targetUrl =  imageSet.getTargetUrl();
        // ...
                
        //final WebClient webClient = new WebClient();
        final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_3_6);  // ???
        webClient.setCssEnabled(false);               // So many CSS/JS related errors on GAE.
        webClient.setJavaScriptEnabled(false);        // So many CSS/JS related errors on GAE.
        webClient.setPopupBlockerEnabled(true);       // ...
        webClient.setTimeout(30000);                  // Does this work on GAE????
        webClient.setRedirectEnabled(true);           // temporary.... Use redirect request field... ???
        
        // ????
        //webClient.setRefreshHandler(new WaitingRefreshHandler(1));  // ???
        // Same page refresh (as opposed to the refresh used for redirect) normally does not terminate.
        // There is really no good solution other than disabling it....
        webClient.setRefreshHandler(new RefreshHandler() {
            @Override
            public void handleRefresh(Page page, URL url, int seconds) throws IOException
            {
                // Do nothing.
            }
        });
        
        int statusCode = -1;  // ???
        try {
            // Starting processing...
            long now = System.currentTimeMillis();
            ((ImageSetBean) imageSet).setLastCheckedTime(now);
            ((ImageSetBean) imageSet).setLastFetchResult(FetchResult.RES_UNKNOWN);  // ???
            //((ImageSetBean) imageSet).setRefreshStatus(RefreshStatus.STATUS_PROCESSING);  // ???
            log.finer("Before calling getPage()");
            // TBD: text pages??? xml pages???
            final HtmlPage targetPage = webClient.getPage(targetUrl);
            //final HtmlPage targetPage = webClient.getPage(new URL(targetUrl));  // ???
            log.finer("After calling getPage()");
            
            if(targetPage != null) {
                // TBD: ...
                WebResponse webResponse = targetPage.getWebResponse();
                statusCode = webResponse.getStatusCode();
                ((ImageSetBean) imageSet).setResponseCode(statusCode);
                if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for targetUrl, " + targetUrl);

                String pageTitle = targetPage.getTitleText();
                log.info("pageTitle = " + pageTitle);
                if(pageTitle != null && pageTitle.length() > 500) {
                    String truncatedTitle = pageTitle.substring(0, 500);
                    log.info("Title has been truncated: Original pageTitle = " + pageTitle);
                    log.info("Truncated pageTitle = " + truncatedTitle);
                    pageTitle = truncatedTitle;
                }
                ((ImageSetBean) imageSet).setPageTitle(pageTitle);

//                @SuppressWarnings("unchecked")
//                List<HtmlMeta> metaDescs = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='description']");
//                if(metaDescs != null && !metaDescs.isEmpty()) {
//                    String pageDescription = metaDescs.get(0).getAttribute("content");
//                    if(pageDescription != null && !pageDescription.isEmpty()) {
//                        ((ImageSetBean) imageSet).setPageDescription(pageDescription.trim());  // trim ???
//                    }
//                }
                
                
                
                // TBD:
//                Set<ImageStruct> currentImgSet = imageSet.getPageImages();
//                if(currentImgSet == null) {
//                    currentImgSet = new HashSet<ImageStruct>(); 
//                }

                
                // TBD:
                // ...
                Map<String, ImageStruct> imgMap = new HashMap<String, ImageStruct>();
                //Set<ImageStruct> imgStructs = new HashSet<ImageStruct>();

                @SuppressWarnings("unchecked")
                final List<HtmlImage> images = (List<HtmlImage>) targetPage.getByXPath("//img");
                if(images != null && !images.isEmpty()) {
                    for (HtmlImage img : images) {
                        String src = img.getSrcAttribute();
                        if(src != null && !src.isEmpty()) {   // && URLUtil.isValidUrl(src) -> this returns false for relative URL.... Cannot use this...
                            ImageStructBean bean = new ImageStructBean();
                            bean.setSrc(src);
                            //String baseURI = a.getBaseURI();  // <- getBaseURI() not implemented... ????
                            // TBD: Convert relative url to absolute url ???
                            // ...
                  
                            // TBD:
                            // (Other attributes, alt, w/h, might be different. But, still we include one one....)
                            if(imgMap.containsKey(src)) {
                                // Skip
                                log.info("This image has already been included: src = " + src);
                                continue;
                            }                            

                            // temporary
                            String srcUrl = FetchUtil.buildAbsoluteUrl(src, targetUrl);
                            if(srcUrl != null && !srcUrl.isEmpty()) {
                                bean.setSrcUrl(srcUrl);
                            }

                            // Uuid for DataStore...
                            String uuid = GUID.generate();
                            bean.setUuid(uuid);
                            // Unique Id
                            String id = img.getId();
                            if(id == null || id.isEmpty()) {
                                //id = GUID.generate().substring(0, 8);
                                id = uuid.substring(0, 8);
                            }
                            bean.setId(id);

                            // Other attributes
                            String alt = img.getAltAttribute();
                            if(alt != null) {
                                if(alt.length() > 500) {
                                    String truncatedAlt = alt.substring(0, 500);
                                    log.info("Img.alt has been truncated: Original alt = " + alt);
                                    log.info("Truncated alt = " + truncatedAlt);
                                    alt = truncatedAlt;
                                }
                                bean.setAlt(alt);
                            }

                            String width = img.getWidthAttribute();
                            if(width != null && !width.isEmpty()) {
                                bean.setWidthAttr(width);
                                try {
                                    int w = Integer.parseInt(width);
                                    bean.setWidth(w);
                                } catch(NumberFormatException e) {
                                    // ignore...
                                }
                            }
                            String height = img.getHeightAttribute();
                            if(height != null && !height.isEmpty()) {
                                bean.setHeightAttr(height);
                                try {
                                    int h = Integer.parseInt(height);
                                    bean.setHeight(h);
                                } catch(NumberFormatException e) {
                                    // ignore...
                                }
                            }
                            // ...

                            imgMap.put(src, bean);

                        } else {
                            // Skip... ???
                        }
                    }
                }

                Set<ImageStruct> imgSet = new HashSet<ImageStruct>(imgMap.values());

                
//                // TBD:
//                if(currentImgSet == null) {
//                    currentImgSet = imgSet; 
//                } else {
//                    // Copy imgSet to currentImgSet
//                    // ????
//                }


                
                ((ImageSetBean) imageSet).setPageImages(imgSet);

                ((ImageSetBean) imageSet).setLastFetchResult(FetchResult.RES_SUCCESS);
                ((ImageSetBean) imageSet).setLastUpdatedTime(now);  // ???
                // etc...
                
            } else {
                // TBD ....
                ((ImageSetBean) imageSet).setLastFetchResult(FetchResult.RES_FAILURE);
                // etc...
                // What to do???
                // ????
            }

            // TBD:
            // Update these only if RefreshStatus != STATUS_NOREFRESH???
            Integer refreshStatus = imageSet.getRefreshStatus();
            if(refreshStatus ==  null || refreshStatus != RefreshStatus.STATUS_NOREFRESH) {
                Long createdTime = imageSet.getCreatedTime();
                if(createdTime != null && createdTime < now - MAX_REFRESHING_AGE_MILLIS) {
                    // The record is too old, no more refreshing...
                    ((ImageSetBean) imageSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                    // ((ImageSetBean) imageSet).setNextRefreshTime(Long.MAX_VALUE);  // ????
                    ((ImageSetBean) imageSet).setNextRefreshTime(0L);   // ???
                    // ...
                } else {
                    ((ImageSetBean) imageSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
                    Long refreshInterval = imageSet.getRefreshInterval();
                    if(refreshInterval == null || refreshInterval < MIN_REFRESH_INTERVAL) {
                        if(refreshInterval == null) {
                            if(log.isLoggable(Level.INFO)) log.info("refreshInterval is set to the default value: " + REFRESH_INTERVAL);
                            refreshInterval = REFRESH_INTERVAL;  // Default value
                        } else if(refreshInterval < MIN_REFRESH_INTERVAL) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Input refreshInterval is too smmal, " + refreshInterval + ". Resetting it to the minimum value: " + MIN_REFRESH_INTERVAL);
                            refreshInterval = MIN_REFRESH_INTERVAL;
                        }
                        ((ImageSetBean) imageSet).setRefreshInterval(refreshInterval);
                    }
                    ((ImageSetBean) imageSet).setNextRefreshTime(now + refreshInterval);
                }
            } else {
                // This should not happen.... (because we filter out records based on refreshStatus....)
                log.warning("Invalid refreshStatus for imageSet = " + imageSet.getGuid());
                throw new InternalServerErrorException("Invalid refreshStatus for imageSet = " + imageSet.getGuid());
            }
            // ...

        } catch (MalformedURLException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (ZipException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (IOException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (FailingHttpStatusCodeException e) {
            // TBD:
            // Handle 404, etc... ????
            // (what if it was just temporary 404 ????)
            if(statusCode == 406) {
                // temporary
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Authentication required for this page: statusCode= " + statusCode, e);
                ((ImageSetBean) imageSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                ((ImageSetBean) imageSet).setNextRefreshTime(0L);   // ???
            } else {
                throw new BaseException("Error processing targetUrl = " + targetUrl, e);
            }
        } catch (Exception e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } finally{
            // ???
            try {
                webClient.closeAllWindows();
            } catch (Exception e) {
                // ????
                //throw new BaseException("Error while closing HTMLUnit WebClient.", e);
                log.log(Level.WARNING, "Error while closing HTMLUnit WebClient.", e);  // ignore...
            }
        }
        
        log.finer("END: processImageFetch()");
        return imageSet;
    }


    
    // TBD:
    
    
    public AudioSet processAudioFetch(AudioSet audioSet) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processAudioFetch() called with audioSet = " + audioSet);
        if(audioSet == null) {
            // ????
            log.warning("audioSet is null.");
            throw new BadRequestException("audioSet is null.");
        }

        // TargetUrl/input cannot be null
        String targetUrl =  audioSet.getTargetUrl();
        // ...
                
        //final WebClient webClient = new WebClient();
        final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_3_6);  // ???
        webClient.setCssEnabled(false);               // So many CSS/JS related errors on GAE.
        webClient.setJavaScriptEnabled(false);        // So many CSS/JS related errors on GAE.
        webClient.setPopupBlockerEnabled(true);       // ...
        webClient.setTimeout(30000);                  // Does this work on GAE????
        webClient.setRedirectEnabled(true);           // temporary.... Use redirect request field... ???
        
        // ????
        //webClient.setRefreshHandler(new WaitingRefreshHandler(1));  // ???
        // Same page refresh (as opposed to the refresh used for redirect) normally does not terminate.
        // There is really no good solution other than disabling it....
        webClient.setRefreshHandler(new RefreshHandler() {
            @Override
            public void handleRefresh(Page page, URL url, int seconds) throws IOException
            {
                // Do nothing.
            }
        });
        
        int statusCode = -1;  // ???
        try {
            // Starting processing...
            long now = System.currentTimeMillis();
            ((AudioSetBean) audioSet).setLastCheckedTime(now);
            ((AudioSetBean) audioSet).setLastFetchResult(FetchResult.RES_UNKNOWN);  // ???
            //((AudioSetBean) audioSet).setRefreshStatus(RefreshStatus.STATUS_PROCESSING);  // ???
            log.finer("Before calling getPage()");
            // TBD: text pages??? xml pages???
            final HtmlPage targetPage = webClient.getPage(targetUrl);
            //final HtmlPage targetPage = webClient.getPage(new URL(targetUrl));  // ???
            log.finer("After calling getPage()");
            
            if(targetPage != null) {
                // TBD: ...
                WebResponse webResponse = targetPage.getWebResponse();
                statusCode = webResponse.getStatusCode();
                ((AudioSetBean) audioSet).setResponseCode(statusCode);
                if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for targetUrl, " + targetUrl);

                String pageTitle = targetPage.getTitleText();
                log.info("pageTitle = " + pageTitle);
                if(pageTitle != null && pageTitle.length() > 500) {
                    String truncatedTitle = pageTitle.substring(0, 500);
                    log.info("Title has been truncated: Original pageTitle = " + pageTitle);
                    log.info("Truncated pageTitle = " + truncatedTitle);
                    pageTitle = truncatedTitle;
                }
                ((AudioSetBean) audioSet).setPageTitle(pageTitle);

//                @SuppressWarnings("unchecked")
//                List<HtmlMeta> metaDescs = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='description']");
//                if(metaDescs != null && !metaDescs.isEmpty()) {
//                    String pageDescription = metaDescs.get(0).getAttribute("content");
//                    if(pageDescription != null && !pageDescription.isEmpty()) {
//                        ((AudioSetBean) audioSet).setPageDescription(pageDescription.trim());  // trim ???
//                    }
//                }
                
                
                // TBD:
                // ...
                Map<String, AudioStruct> imgMap = new HashMap<String, AudioStruct>();
                //Set<AudioStruct> imgStructs = new HashSet<AudioStruct>();

//                @SuppressWarnings("unchecked")
//                final List<HtmlAudio> audios = (List<HtmlAudio>) targetPage.getByXPath("//img");
//                if(audios != null && !audios.isEmpty()) {
//                    for (HtmlAudio img : audios) {
//                        String src = img.getSrcAttribute();
//                        if(src != null && !src.isEmpty()) {   // && URLUtil.isValidUrl(src) -> this returns false for relative URL.... Cannot use this...
//                            AudioStructBean bean = new AudioStructBean();
//                            bean.setSrc(src);
//                            //String baseURI = a.getBaseURI();  // <- getBaseURI() not implemented... ????
//                            // TBD: Convert relative url to absolute url ???
//                            // ...
//                  
//                            // TBD:
//                            // (Other attributes, alt, w/h, might be different. But, still we include one one....)
//                            if(imgMap.containsKey(src)) {
//                                // Skip
//                                log.info("This audio has already been included: src = " + src);
//                                continue;
//                            }                            
//
//                            // temporary
//                            String srcUrl = FetchUtil.buildAbsoluteUrl(src, targetUrl);
//                            if(srcUrl != null && !srcUrl.isEmpty()) {
//                                bean.setSrcUrl(srcUrl);
//                            }
//                            
//                            // Uuid for DataStore...
//                            String uuid = GUID.generate();
//                            bean.setUuid(uuid);
//                            // Unique Id
//                            String id = img.getId();
//                            if(id == null || id.isEmpty()) {
//                                //id = GUID.generate().substring(0, 8);
//                                id = uuid.substring(0, 8);
//                            }
//                            bean.setId(id);
//
//                            // Other attributes
//                            String alt = img.getAltAttribute();
//                            if(alt != null) {
//                                if(alt.length() > 500) {
//                                    String truncatedAlt = alt.substring(0, 500);
//                                    log.info("Img.alt has been truncated: Original alt = " + alt);
//                                    log.info("Truncated alt = " + truncatedAlt);
//                                    alt = truncatedAlt;
//                                }
//                                bean.setAlt(alt);
//                            }
//
//                            String width = img.getWidthAttribute();
//                            if(width != null && !width.isEmpty()) {
//                                bean.setWidthAttr(width);
//                                try {
//                                    int w = Integer.parseInt(width);
//                                    bean.setWidth(w);
//                                } catch(NumberFormatException e) {
//                                    // ignore...
//                                }
//                            }
//                            String height = img.getHeightAttribute();
//                            if(height != null && !height.isEmpty()) {
//                                bean.setHeightAttr(height);
//                                try {
//                                    int h = Integer.parseInt(height);
//                                    bean.setHeight(h);
//                                } catch(NumberFormatException e) {
//                                    // ignore...
//                                }
//                            }
//                            // ...
//
//                            imgMap.put(src, bean);
//
//                        } else {
//                            // Skip... ???
//                        }
//                    }
//                }

                Set<AudioStruct> imgSet = new HashSet<AudioStruct>(imgMap.values());
                ((AudioSetBean) audioSet).setPageAudios(imgSet);

                ((AudioSetBean) audioSet).setLastFetchResult(FetchResult.RES_SUCCESS);
                ((AudioSetBean) audioSet).setLastUpdatedTime(now);  // ???
                // etc...
                
            } else {
                // TBD ....
                ((AudioSetBean) audioSet).setLastFetchResult(FetchResult.RES_FAILURE);
                // etc...
                // What to do???
                // ????
            }

            // TBD:
            // Update these only if RefreshStatus != STATUS_NOREFRESH???
            Integer refreshStatus = audioSet.getRefreshStatus();
            if(refreshStatus ==  null || refreshStatus != RefreshStatus.STATUS_NOREFRESH) {
                Long createdTime = audioSet.getCreatedTime();
                if(createdTime != null && createdTime < now - MAX_REFRESHING_AGE_MILLIS) {
                    // The record is too old, no more refreshing...
                    ((AudioSetBean) audioSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                    // ((AudioSetBean) audioSet).setNextRefreshTime(Long.MAX_VALUE);  // ????
                    ((AudioSetBean) audioSet).setNextRefreshTime(0L);   // ???
                    // ...
                } else {
                    ((AudioSetBean) audioSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
                    Long refreshInterval = audioSet.getRefreshInterval();
                    if(refreshInterval == null || refreshInterval < MIN_REFRESH_INTERVAL) {
                        if(refreshInterval == null) {
                            if(log.isLoggable(Level.INFO)) log.info("refreshInterval is set to the default value: " + REFRESH_INTERVAL);
                            refreshInterval = REFRESH_INTERVAL;  // Default value
                        } else if(refreshInterval < MIN_REFRESH_INTERVAL) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Input refreshInterval is too smmal, " + refreshInterval + ". Resetting it to the minimum value: " + MIN_REFRESH_INTERVAL);
                            refreshInterval = MIN_REFRESH_INTERVAL;
                        }
                        ((AudioSetBean) audioSet).setRefreshInterval(refreshInterval);
                    }
                    ((AudioSetBean) audioSet).setNextRefreshTime(now + refreshInterval);
                }
            } else {
                // This should not happen.... (because we filter out records based on refreshStatus....)
                log.warning("Invalid refreshStatus for audioSet = " + audioSet.getGuid());
                throw new InternalServerErrorException("Invalid refreshStatus for audioSet = " + audioSet.getGuid());
            }
            // ...

        } catch (MalformedURLException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (ZipException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (IOException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (FailingHttpStatusCodeException e) {
            // TBD:
            // Handle 404, etc... ????
            // (what if it was just temporary 404 ????)
            if(statusCode == 406) {
                // temporary
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Authentication required for this page: statusCode= " + statusCode, e);
                ((AudioSetBean) audioSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                ((AudioSetBean) audioSet).setNextRefreshTime(0L);   // ???
            } else {
                throw new BaseException("Error processing targetUrl = " + targetUrl, e);
            }
        } catch (Exception e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } finally{
            // ???
            try {
                webClient.closeAllWindows();
            } catch (Exception e) {
                // ????
                //throw new BaseException("Error while closing HTMLUnit WebClient.", e);
                log.log(Level.WARNING, "Error while closing HTMLUnit WebClient.", e);  // ignore...
            }
        }
        
        log.finer("END: processAudioFetch()");
        return audioSet;
    }


    public VideoSet processVideoFetch(VideoSet videoSet) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processVideoFetch() called with videoSet = " + videoSet);
        if(videoSet == null) {
            // ????
            log.warning("videoSet is null.");
            throw new BadRequestException("videoSet is null.");
        }

        // TargetUrl/input cannot be null
        String targetUrl =  videoSet.getTargetUrl();
        // ...
                
        //final WebClient webClient = new WebClient();
        final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_3_6);  // ???
        webClient.setCssEnabled(false);               // So many CSS/JS related errors on GAE.
        webClient.setJavaScriptEnabled(false);        // So many CSS/JS related errors on GAE.
        webClient.setPopupBlockerEnabled(true);       // ...
        webClient.setTimeout(30000);                  // Does this work on GAE????
        webClient.setRedirectEnabled(true);           // temporary.... Use redirect request field... ???
        
        // ????
        //webClient.setRefreshHandler(new WaitingRefreshHandler(1));  // ???
        // Same page refresh (as opposed to the refresh used for redirect) normally does not terminate.
        // There is really no good solution other than disabling it....
        webClient.setRefreshHandler(new RefreshHandler() {
            @Override
            public void handleRefresh(Page page, URL url, int seconds) throws IOException
            {
                // Do nothing.
            }
        });
        
        int statusCode = -1;  // ???
        try {
            // Starting processing...
            long now = System.currentTimeMillis();
            ((VideoSetBean) videoSet).setLastCheckedTime(now);
            ((VideoSetBean) videoSet).setLastFetchResult(FetchResult.RES_UNKNOWN);  // ???
            //((VideoSetBean) videoSet).setRefreshStatus(RefreshStatus.STATUS_PROCESSING);  // ???
            log.finer("Before calling getPage()");
            // TBD: text pages??? xml pages???
            final HtmlPage targetPage = webClient.getPage(targetUrl);
            //final HtmlPage targetPage = webClient.getPage(new URL(targetUrl));  // ???
            log.finer("After calling getPage()");
            
            if(targetPage != null) {
                // TBD: ...
                WebResponse webResponse = targetPage.getWebResponse();
                statusCode = webResponse.getStatusCode();
                ((VideoSetBean) videoSet).setResponseCode(statusCode);
                if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for targetUrl, " + targetUrl);

                String pageTitle = targetPage.getTitleText();
                log.info("pageTitle = " + pageTitle);
                if(pageTitle != null && pageTitle.length() > 500) {
                    String truncatedTitle = pageTitle.substring(0, 500);
                    log.info("Title has been truncated: Original pageTitle = " + pageTitle);
                    log.info("Truncated pageTitle = " + truncatedTitle);
                    pageTitle = truncatedTitle;
                }
                ((VideoSetBean) videoSet).setPageTitle(pageTitle);

//                @SuppressWarnings("unchecked")
//                List<HtmlMeta> metaDescs = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='description']");
//                if(metaDescs != null && !metaDescs.isEmpty()) {
//                    String pageDescription = metaDescs.get(0).getAttribute("content");
//                    if(pageDescription != null && !pageDescription.isEmpty()) {
//                        ((VideoSetBean) videoSet).setPageDescription(pageDescription.trim());  // trim ???
//                    }
//                }
                
                
                // TBD:
                // ...
                Map<String, VideoStruct> imgMap = new HashMap<String, VideoStruct>();
                //Set<VideoStruct> imgStructs = new HashSet<VideoStruct>();

//                @SuppressWarnings("unchecked")
//                final List<HtmlVideo> videos = (List<HtmlVideo>) targetPage.getByXPath("//img");
//                if(videos != null && !videos.isEmpty()) {
//                    for (HtmlVideo img : videos) {
//                        String src = img.getSrcAttribute();
//                        if(src != null && !src.isEmpty()) {   // && URLUtil.isValidUrl(src) -> this returns false for relative URL.... Cannot use this...
//                            VideoStructBean bean = new VideoStructBean();
//                            bean.setSrc(src);
//                            //String baseURI = a.getBaseURI();  // <- getBaseURI() not implemented... ????
//                            // TBD: Convert relative url to absolute url ???
//                            // ...
//                  
//                            // TBD:
//                            // (Other attributes, alt, w/h, might be different. But, still we include one one....)
//                            if(imgMap.containsKey(src)) {
//                                // Skip
//                                log.info("This video has already been included: src = " + src);
//                                continue;
//                            }                            
//
//                            // temporary
//                            String srcUrl = FetchUtil.buildAbsoluteUrl(src, targetUrl);
//                            if(srcUrl != null && !srcUrl.isEmpty()) {
//                                bean.setSrcUrl(srcUrl);
//                            }
//                            
//              // Uuid for DataStore...
//              String uuid = GUID.generate();
//              bean.setUuid(uuid);
//              // Unique Id
//              String id = img.getId();
//              if(id == null || id.isEmpty()) {
//                  //id = GUID.generate().substring(0, 8);
//                  id = uuid.substring(0, 8);
//              }
//              bean.setId(id);
//
//                            // Other attributes
//                            String alt = img.getAltAttribute();
//                            if(alt != null) {
//                                if(alt.length() > 500) {
//                                    String truncatedAlt = alt.substring(0, 500);
//                                    log.info("Img.alt has been truncated: Original alt = " + alt);
//                                    log.info("Truncated alt = " + truncatedAlt);
//                                    alt = truncatedAlt;
//                                }
//                                bean.setAlt(alt);
//                            }
//
//                            String width = img.getWidthAttribute();
//                            if(width != null && !width.isEmpty()) {
//                                bean.setWidthAttr(width);
//                                try {
//                                    int w = Integer.parseInt(width);
//                                    bean.setWidth(w);
//                                } catch(NumberFormatException e) {
//                                    // ignore...
//                                }
//                            }
//                            String height = img.getHeightAttribute();
//                            if(height != null && !height.isEmpty()) {
//                                bean.setHeightAttr(height);
//                                try {
//                                    int h = Integer.parseInt(height);
//                                    bean.setHeight(h);
//                                } catch(NumberFormatException e) {
//                                    // ignore...
//                                }
//                            }
//                            // ...
//
//                            imgMap.put(src, bean);
//
//                        } else {
//                            // Skip... ???
//                        }
//                    }
//                }

                Set<VideoStruct> imgSet = new HashSet<VideoStruct>(imgMap.values());
                ((VideoSetBean) videoSet).setPageVideos(imgSet);

                ((VideoSetBean) videoSet).setLastFetchResult(FetchResult.RES_SUCCESS);
                ((VideoSetBean) videoSet).setLastUpdatedTime(now);  // ???
                // etc...
                
            } else {
                // TBD ....
                ((VideoSetBean) videoSet).setLastFetchResult(FetchResult.RES_FAILURE);
                // etc...
                // What to do???
                // ????
            }

            // TBD:
            // Update these only if RefreshStatus != STATUS_NOREFRESH???
            Integer refreshStatus = videoSet.getRefreshStatus();
            if(refreshStatus ==  null || refreshStatus != RefreshStatus.STATUS_NOREFRESH) {
                Long createdTime = videoSet.getCreatedTime();
                if(createdTime != null && createdTime < now - MAX_REFRESHING_AGE_MILLIS) {
                    // The record is too old, no more refreshing...
                    ((VideoSetBean) videoSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                    // ((VideoSetBean) videoSet).setNextRefreshTime(Long.MAX_VALUE);  // ????
                    ((VideoSetBean) videoSet).setNextRefreshTime(0L);   // ???
                    // ...
                } else {
                    ((VideoSetBean) videoSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
                    Long refreshInterval = videoSet.getRefreshInterval();
                    if(refreshInterval == null || refreshInterval < MIN_REFRESH_INTERVAL) {
                        if(refreshInterval == null) {
                            if(log.isLoggable(Level.INFO)) log.info("refreshInterval is set to the default value: " + REFRESH_INTERVAL);
                            refreshInterval = REFRESH_INTERVAL;  // Default value
                        } else if(refreshInterval < MIN_REFRESH_INTERVAL) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Input refreshInterval is too smmal, " + refreshInterval + ". Resetting it to the minimum value: " + MIN_REFRESH_INTERVAL);
                            refreshInterval = MIN_REFRESH_INTERVAL;
                        }
                        ((VideoSetBean) videoSet).setRefreshInterval(refreshInterval);
                    }
                    ((VideoSetBean) videoSet).setNextRefreshTime(now + refreshInterval);
                }
            } else {
                // This should not happen.... (because we filter out records based on refreshStatus....)
                log.warning("Invalid refreshStatus for videoSet = " + videoSet.getGuid());
                throw new InternalServerErrorException("Invalid refreshStatus for videoSet = " + videoSet.getGuid());
            }
            // ...

        } catch (MalformedURLException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (ZipException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (IOException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (FailingHttpStatusCodeException e) {
            // TBD:
            // Handle 404, etc... ????
            // (what if it was just temporary 404 ????)
            if(statusCode == 406) {
                // temporary
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Authentication required for this page: statusCode= " + statusCode, e);
                ((VideoSetBean) videoSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                ((VideoSetBean) videoSet).setNextRefreshTime(0L);   // ???
            } else {
                throw new BaseException("Error processing targetUrl = " + targetUrl, e);
            }
        } catch (Exception e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } finally{
            // ???
            try {
                webClient.closeAllWindows();
            } catch (Exception e) {
                // ????
                //throw new BaseException("Error while closing HTMLUnit WebClient.", e);
                log.log(Level.WARNING, "Error while closing HTMLUnit WebClient.", e);  // ignore...
            }
        }
        
        log.finer("END: processVideoFetch()");
        return videoSet;
    }


    
    
    

    
    // TBD
    // Not being used... ????
    public FetchRequest processPageFetch(FetchRequest fetchRequest) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processLinkFetch() called with fetchRequest = " + fetchRequest);
        if(fetchRequest == null) {
            // ????
            log.warning("fetchRequest is null.");
            throw new BadRequestException("fetchRequest is null.");
        }

        
        // TBD
        // ...
        
        
        // TargetUrl/input cannot be null
        String targetUrl =  fetchRequest.getTargetUrl();
        // ...
        
        
        //final WebClient webClient = new WebClient();
        final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_3_6);  // ???
        webClient.setCssEnabled(false);               // So many CSS/JS related errors on GAE.
        webClient.setJavaScriptEnabled(false);        // So many CSS/JS related errors on GAE.
        webClient.setPopupBlockerEnabled(true);       // ...
        webClient.setTimeout(30000);                  // Does this work on GAE????
        try {
            // Starting processing...
            ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_PROCESSING);
            // etc....

            log.finer("Before calling getPage()");
            final HtmlPage targetPage = webClient.getPage(targetUrl);
            log.finer("After calling getPage()");
            
            final String pageTitle = targetPage.getTitleText();
//            log.info("pageTitle = " + pageTitle);
//            ((FetchRequestBean) fetchRequest).setPageTitle(pageTitle);

            @SuppressWarnings("unchecked")
            List<HtmlMeta> metaDescs = (List<HtmlMeta>) targetPage.getByXPath("//meta[@name='description']");
            if(metaDescs != null && !metaDescs.isEmpty()) {
                String pageDescription = metaDescs.get(0).getAttribute("content");
                if(pageDescription != null && !pageDescription.isEmpty()) {
//                    ((FetchRequestBean) fetchRequest).setPageDescription(pageDescription.trim());  // trim ???
                }
            }
            
            // TBD
            // Update
            // FetchRequest
            // LinkList
            // ImageSet
            // ....
            // ...
            
            // TBD ....
            // ...
            
            
        } catch (MalformedURLException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (ZipException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (IOException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (FailingHttpStatusCodeException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (Exception e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } finally{
            // ???
            try {
                webClient.closeAllWindows();
            } catch (Exception e) {
                // ????
                //throw new BaseException("Error while closing HTMLUnit WebClient.", e);
                log.log(Level.WARNING, "Error while closing HTMLUnit WebClient.", e);  // ignore...
            }
        }
        
        log.finer("END: processLinkFetch()");
        return fetchRequest;
    }
    
    

}
