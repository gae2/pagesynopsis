package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.DomainInfo;
// import com.pagesynopsis.ws.bean.DomainInfoBean;
import com.pagesynopsis.ws.service.DomainInfoService;
import com.pagesynopsis.af.bean.DomainInfoBean;
import com.pagesynopsis.af.proxy.DomainInfoServiceProxy;


public class LocalDomainInfoServiceProxy extends BaseLocalServiceProxy implements DomainInfoServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalDomainInfoServiceProxy.class.getName());

    public LocalDomainInfoServiceProxy()
    {
    }

    @Override
    public DomainInfo getDomainInfo(String guid) throws BaseException
    {
        DomainInfo serverBean = getDomainInfoService().getDomainInfo(guid);
        DomainInfo appBean = convertServerDomainInfoBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getDomainInfo(String guid, String field) throws BaseException
    {
        return getDomainInfoService().getDomainInfo(guid, field);       
    }

    @Override
    public List<DomainInfo> getDomainInfos(List<String> guids) throws BaseException
    {
        List<DomainInfo> serverBeanList = getDomainInfoService().getDomainInfos(guids);
        List<DomainInfo> appBeanList = convertServerDomainInfoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<DomainInfo> getAllDomainInfos() throws BaseException
    {
        return getAllDomainInfos(null, null, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getDomainInfoService().getAllDomainInfos(ordering, offset, count);
        return getAllDomainInfos(ordering, offset, count, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<DomainInfo> serverBeanList = getDomainInfoService().getAllDomainInfos(ordering, offset, count, forwardCursor);
        List<DomainInfo> appBeanList = convertServerDomainInfoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getDomainInfoService().getAllDomainInfoKeys(ordering, offset, count);
        return getAllDomainInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDomainInfoService().getAllDomainInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDomainInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getDomainInfoService().findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count);
        return findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<DomainInfo> serverBeanList = getDomainInfoService().findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<DomainInfo> appBeanList = convertServerDomainInfoBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getDomainInfoService().findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDomainInfoService().findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getDomainInfoService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDomainInfo(String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return getDomainInfoService().createDomainInfo(domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public String createDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        com.pagesynopsis.ws.bean.DomainInfoBean serverBean =  convertAppDomainInfoBeanToServerBean(domainInfo);
        return getDomainInfoService().createDomainInfo(serverBean);
    }

    @Override
    public Boolean updateDomainInfo(String guid, String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return getDomainInfoService().updateDomainInfo(guid, domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public Boolean updateDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        com.pagesynopsis.ws.bean.DomainInfoBean serverBean =  convertAppDomainInfoBeanToServerBean(domainInfo);
        return getDomainInfoService().updateDomainInfo(serverBean);
    }

    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        return getDomainInfoService().deleteDomainInfo(guid);
    }

    @Override
    public Boolean deleteDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        com.pagesynopsis.ws.bean.DomainInfoBean serverBean =  convertAppDomainInfoBeanToServerBean(domainInfo);
        return getDomainInfoService().deleteDomainInfo(serverBean);
    }

    @Override
    public Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException
    {
        return getDomainInfoService().deleteDomainInfos(filter, params, values);
    }




    public static DomainInfoBean convertServerDomainInfoBeanToAppBean(DomainInfo serverBean)
    {
        DomainInfoBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new DomainInfoBean();
            bean.setGuid(serverBean.getGuid());
            bean.setDomain(serverBean.getDomain());
            bean.setExcluded(serverBean.isExcluded());
            bean.setCategory(serverBean.getCategory());
            bean.setReputation(serverBean.getReputation());
            bean.setAuthority(serverBean.getAuthority());
            bean.setNote(serverBean.getNote());
            bean.setStatus(serverBean.getStatus());
            bean.setLastCheckedTime(serverBean.getLastCheckedTime());
            bean.setLastUpdatedTime(serverBean.getLastUpdatedTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<DomainInfo> convertServerDomainInfoBeanListToAppBeanList(List<DomainInfo> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<DomainInfo> beanList = new ArrayList<DomainInfo>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(DomainInfo sb : serverBeanList) {
                DomainInfoBean bean = convertServerDomainInfoBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.pagesynopsis.ws.bean.DomainInfoBean convertAppDomainInfoBeanToServerBean(DomainInfo appBean)
    {
        com.pagesynopsis.ws.bean.DomainInfoBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.pagesynopsis.ws.bean.DomainInfoBean();
            bean.setGuid(appBean.getGuid());
            bean.setDomain(appBean.getDomain());
            bean.setExcluded(appBean.isExcluded());
            bean.setCategory(appBean.getCategory());
            bean.setReputation(appBean.getReputation());
            bean.setAuthority(appBean.getAuthority());
            bean.setNote(appBean.getNote());
            bean.setStatus(appBean.getStatus());
            bean.setLastCheckedTime(appBean.getLastCheckedTime());
            bean.setLastUpdatedTime(appBean.getLastUpdatedTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<DomainInfo> convertAppDomainInfoBeanListToServerBeanList(List<DomainInfo> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<DomainInfo> beanList = new ArrayList<DomainInfo>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(DomainInfo sb : appBeanList) {
                com.pagesynopsis.ws.bean.DomainInfoBean bean = convertAppDomainInfoBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
