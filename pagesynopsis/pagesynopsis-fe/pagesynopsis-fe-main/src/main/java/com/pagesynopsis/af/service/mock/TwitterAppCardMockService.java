package com.pagesynopsis.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterAppCardService;


// TwitterAppCardMockService is a decorator.
// It can be used as a base class to mock TwitterAppCardService objects.
public abstract class TwitterAppCardMockService implements TwitterAppCardService
{
    private static final Logger log = Logger.getLogger(TwitterAppCardMockService.class.getName());

    // TwitterAppCardMockService uses the decorator design pattern.
    private TwitterAppCardService decoratedService;

    public TwitterAppCardMockService(TwitterAppCardService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected TwitterAppCardService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(TwitterAppCardService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // TwitterAppCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterAppCard getTwitterAppCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterAppCard(): guid = " + guid);
        TwitterAppCard bean = decoratedService.getTwitterAppCard(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterAppCard(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getTwitterAppCard(guid, field);
        return obj;
    }

    @Override
    public List<TwitterAppCard> getTwitterAppCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterAppCards()");
        List<TwitterAppCard> twitterAppCards = decoratedService.getTwitterAppCards(guids);
        log.finer("END");
        return twitterAppCards;
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards() throws BaseException
    {
        return getAllTwitterAppCards(null, null, null);
    }


    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterAppCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterAppCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<TwitterAppCard> twitterAppCards = decoratedService.getAllTwitterAppCards(ordering, offset, count, forwardCursor);
        log.finer("END");
        return twitterAppCards;
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterAppCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterAppCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllTwitterAppCardKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardMockService.findTwitterAppCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<TwitterAppCard> twitterAppCards = decoratedService.findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return twitterAppCards;
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardMockService.findTwitterAppCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterAppCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        TwitterCardAppInfoBean iphoneAppBean = null;
        if(iphoneApp instanceof TwitterCardAppInfoBean) {
            iphoneAppBean = (TwitterCardAppInfoBean) iphoneApp;
        } else if(iphoneApp instanceof TwitterCardAppInfo) {
            iphoneAppBean = new TwitterCardAppInfoBean(iphoneApp.getName(), iphoneApp.getId(), iphoneApp.getUrl());
        } else {
            iphoneAppBean = null;   // ????
        }
        TwitterCardAppInfoBean ipadAppBean = null;
        if(ipadApp instanceof TwitterCardAppInfoBean) {
            ipadAppBean = (TwitterCardAppInfoBean) ipadApp;
        } else if(ipadApp instanceof TwitterCardAppInfo) {
            ipadAppBean = new TwitterCardAppInfoBean(ipadApp.getName(), ipadApp.getId(), ipadApp.getUrl());
        } else {
            ipadAppBean = null;   // ????
        }
        TwitterCardAppInfoBean googlePlayAppBean = null;
        if(googlePlayApp instanceof TwitterCardAppInfoBean) {
            googlePlayAppBean = (TwitterCardAppInfoBean) googlePlayApp;
        } else if(googlePlayApp instanceof TwitterCardAppInfo) {
            googlePlayAppBean = new TwitterCardAppInfoBean(googlePlayApp.getName(), googlePlayApp.getId(), googlePlayApp.getUrl());
        } else {
            googlePlayAppBean = null;   // ????
        }
        TwitterAppCardBean bean = new TwitterAppCardBean(null, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneAppBean, ipadAppBean, googlePlayAppBean);
        return createTwitterAppCard(bean);
    }

    @Override
    public String createTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createTwitterAppCard(twitterAppCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TwitterAppCard constructTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");
        TwitterAppCard bean = decoratedService.constructTwitterAppCard(twitterAppCard);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterCardAppInfoBean iphoneAppBean = null;
        if(iphoneApp instanceof TwitterCardAppInfoBean) {
            iphoneAppBean = (TwitterCardAppInfoBean) iphoneApp;
        } else if(iphoneApp instanceof TwitterCardAppInfo) {
            iphoneAppBean = new TwitterCardAppInfoBean(iphoneApp.getName(), iphoneApp.getId(), iphoneApp.getUrl());
        } else {
            iphoneAppBean = null;   // ????
        }
        TwitterCardAppInfoBean ipadAppBean = null;
        if(ipadApp instanceof TwitterCardAppInfoBean) {
            ipadAppBean = (TwitterCardAppInfoBean) ipadApp;
        } else if(ipadApp instanceof TwitterCardAppInfo) {
            ipadAppBean = new TwitterCardAppInfoBean(ipadApp.getName(), ipadApp.getId(), ipadApp.getUrl());
        } else {
            ipadAppBean = null;   // ????
        }
        TwitterCardAppInfoBean googlePlayAppBean = null;
        if(googlePlayApp instanceof TwitterCardAppInfoBean) {
            googlePlayAppBean = (TwitterCardAppInfoBean) googlePlayApp;
        } else if(googlePlayApp instanceof TwitterCardAppInfo) {
            googlePlayAppBean = new TwitterCardAppInfoBean(googlePlayApp.getName(), googlePlayApp.getId(), googlePlayApp.getUrl());
        } else {
            googlePlayAppBean = null;   // ????
        }
        TwitterAppCardBean bean = new TwitterAppCardBean(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneAppBean, ipadAppBean, googlePlayAppBean);
        return updateTwitterAppCard(bean);
    }
        
    @Override
    public Boolean updateTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateTwitterAppCard(twitterAppCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TwitterAppCard refreshTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");
        TwitterAppCard bean = decoratedService.refreshTwitterAppCard(twitterAppCard);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTwitterAppCard(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterAppCard(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterAppCard(twitterAppCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteTwitterAppCards(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createTwitterAppCards(twitterAppCards);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException
    //{
    //}

}
