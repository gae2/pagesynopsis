package com.pagesynopsis.af.permission;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.permission.ApiConsumerBasePermission;
import com.pagesynopsis.ws.permission.UserBasePermission;
import com.pagesynopsis.ws.permission.RobotsTextFileBasePermission;
import com.pagesynopsis.ws.permission.RobotsTextRefreshBasePermission;
import com.pagesynopsis.ws.permission.OgProfileBasePermission;
import com.pagesynopsis.ws.permission.OgWebsiteBasePermission;
import com.pagesynopsis.ws.permission.OgBlogBasePermission;
import com.pagesynopsis.ws.permission.OgArticleBasePermission;
import com.pagesynopsis.ws.permission.OgBookBasePermission;
import com.pagesynopsis.ws.permission.OgVideoBasePermission;
import com.pagesynopsis.ws.permission.OgMovieBasePermission;
import com.pagesynopsis.ws.permission.OgTvShowBasePermission;
import com.pagesynopsis.ws.permission.OgTvEpisodeBasePermission;
import com.pagesynopsis.ws.permission.TwitterSummaryCardBasePermission;
import com.pagesynopsis.ws.permission.TwitterPhotoCardBasePermission;
import com.pagesynopsis.ws.permission.TwitterGalleryCardBasePermission;
import com.pagesynopsis.ws.permission.TwitterAppCardBasePermission;
import com.pagesynopsis.ws.permission.TwitterPlayerCardBasePermission;
import com.pagesynopsis.ws.permission.TwitterProductCardBasePermission;
import com.pagesynopsis.ws.permission.FetchRequestBasePermission;
import com.pagesynopsis.ws.permission.PageInfoBasePermission;
import com.pagesynopsis.ws.permission.PageFetchBasePermission;
import com.pagesynopsis.ws.permission.LinkListBasePermission;
import com.pagesynopsis.ws.permission.ImageSetBasePermission;
import com.pagesynopsis.ws.permission.AudioSetBasePermission;
import com.pagesynopsis.ws.permission.VideoSetBasePermission;
import com.pagesynopsis.ws.permission.OpenGraphMetaBasePermission;
import com.pagesynopsis.ws.permission.TwitterCardMetaBasePermission;
import com.pagesynopsis.ws.permission.DomainInfoBasePermission;
import com.pagesynopsis.ws.permission.UrlRatingBasePermission;
import com.pagesynopsis.ws.permission.ServiceInfoBasePermission;
import com.pagesynopsis.ws.permission.FiveTenBasePermission;


// TBD:
public class BasePermissionManager
{
    private static final Logger log = Logger.getLogger(BasePermissionManager.class.getName());

    private ApiConsumerBasePermission apiConsumerPermission = null;
    private UserBasePermission userPermission = null;
    private RobotsTextFileBasePermission robotsTextFilePermission = null;
    private RobotsTextRefreshBasePermission robotsTextRefreshPermission = null;
    private OgProfileBasePermission ogProfilePermission = null;
    private OgWebsiteBasePermission ogWebsitePermission = null;
    private OgBlogBasePermission ogBlogPermission = null;
    private OgArticleBasePermission ogArticlePermission = null;
    private OgBookBasePermission ogBookPermission = null;
    private OgVideoBasePermission ogVideoPermission = null;
    private OgMovieBasePermission ogMoviePermission = null;
    private OgTvShowBasePermission ogTvShowPermission = null;
    private OgTvEpisodeBasePermission ogTvEpisodePermission = null;
    private TwitterSummaryCardBasePermission twitterSummaryCardPermission = null;
    private TwitterPhotoCardBasePermission twitterPhotoCardPermission = null;
    private TwitterGalleryCardBasePermission twitterGalleryCardPermission = null;
    private TwitterAppCardBasePermission twitterAppCardPermission = null;
    private TwitterPlayerCardBasePermission twitterPlayerCardPermission = null;
    private TwitterProductCardBasePermission twitterProductCardPermission = null;
    private FetchRequestBasePermission fetchRequestPermission = null;
    private PageInfoBasePermission pageInfoPermission = null;
    private PageFetchBasePermission pageFetchPermission = null;
    private LinkListBasePermission linkListPermission = null;
    private ImageSetBasePermission imageSetPermission = null;
    private AudioSetBasePermission audioSetPermission = null;
    private VideoSetBasePermission videoSetPermission = null;
    private OpenGraphMetaBasePermission openGraphMetaPermission = null;
    private TwitterCardMetaBasePermission twitterCardMetaPermission = null;
    private DomainInfoBasePermission domainInfoPermission = null;
    private UrlRatingBasePermission urlRatingPermission = null;
    private ServiceInfoBasePermission serviceInfoPermission = null;
    private FiveTenBasePermission fiveTenPermission = null;

    // Ctor.
    public BasePermissionManager()
    {
        // TBD:
    }

	public ApiConsumerBasePermission getApiConsumerPermission() 
    {
        if(apiConsumerPermission == null) {
            apiConsumerPermission = new ApiConsumerBasePermission();
        }
        return apiConsumerPermission;
    }
	public void setApiConsumerPermission(ApiConsumerBasePermission apiConsumerPermission) 
    {
        this.apiConsumerPermission = apiConsumerPermission;
    }

	public UserBasePermission getUserPermission() 
    {
        if(userPermission == null) {
            userPermission = new UserBasePermission();
        }
        return userPermission;
    }
	public void setUserPermission(UserBasePermission userPermission) 
    {
        this.userPermission = userPermission;
    }

	public RobotsTextFileBasePermission getRobotsTextFilePermission() 
    {
        if(robotsTextFilePermission == null) {
            robotsTextFilePermission = new RobotsTextFileBasePermission();
        }
        return robotsTextFilePermission;
    }
	public void setRobotsTextFilePermission(RobotsTextFileBasePermission robotsTextFilePermission) 
    {
        this.robotsTextFilePermission = robotsTextFilePermission;
    }

	public RobotsTextRefreshBasePermission getRobotsTextRefreshPermission() 
    {
        if(robotsTextRefreshPermission == null) {
            robotsTextRefreshPermission = new RobotsTextRefreshBasePermission();
        }
        return robotsTextRefreshPermission;
    }
	public void setRobotsTextRefreshPermission(RobotsTextRefreshBasePermission robotsTextRefreshPermission) 
    {
        this.robotsTextRefreshPermission = robotsTextRefreshPermission;
    }

	public OgProfileBasePermission getOgProfilePermission() 
    {
        if(ogProfilePermission == null) {
            ogProfilePermission = new OgProfileBasePermission();
        }
        return ogProfilePermission;
    }
	public void setOgProfilePermission(OgProfileBasePermission ogProfilePermission) 
    {
        this.ogProfilePermission = ogProfilePermission;
    }

	public OgWebsiteBasePermission getOgWebsitePermission() 
    {
        if(ogWebsitePermission == null) {
            ogWebsitePermission = new OgWebsiteBasePermission();
        }
        return ogWebsitePermission;
    }
	public void setOgWebsitePermission(OgWebsiteBasePermission ogWebsitePermission) 
    {
        this.ogWebsitePermission = ogWebsitePermission;
    }

	public OgBlogBasePermission getOgBlogPermission() 
    {
        if(ogBlogPermission == null) {
            ogBlogPermission = new OgBlogBasePermission();
        }
        return ogBlogPermission;
    }
	public void setOgBlogPermission(OgBlogBasePermission ogBlogPermission) 
    {
        this.ogBlogPermission = ogBlogPermission;
    }

	public OgArticleBasePermission getOgArticlePermission() 
    {
        if(ogArticlePermission == null) {
            ogArticlePermission = new OgArticleBasePermission();
        }
        return ogArticlePermission;
    }
	public void setOgArticlePermission(OgArticleBasePermission ogArticlePermission) 
    {
        this.ogArticlePermission = ogArticlePermission;
    }

	public OgBookBasePermission getOgBookPermission() 
    {
        if(ogBookPermission == null) {
            ogBookPermission = new OgBookBasePermission();
        }
        return ogBookPermission;
    }
	public void setOgBookPermission(OgBookBasePermission ogBookPermission) 
    {
        this.ogBookPermission = ogBookPermission;
    }

	public OgVideoBasePermission getOgVideoPermission() 
    {
        if(ogVideoPermission == null) {
            ogVideoPermission = new OgVideoBasePermission();
        }
        return ogVideoPermission;
    }
	public void setOgVideoPermission(OgVideoBasePermission ogVideoPermission) 
    {
        this.ogVideoPermission = ogVideoPermission;
    }

	public OgMovieBasePermission getOgMoviePermission() 
    {
        if(ogMoviePermission == null) {
            ogMoviePermission = new OgMovieBasePermission();
        }
        return ogMoviePermission;
    }
	public void setOgMoviePermission(OgMovieBasePermission ogMoviePermission) 
    {
        this.ogMoviePermission = ogMoviePermission;
    }

	public OgTvShowBasePermission getOgTvShowPermission() 
    {
        if(ogTvShowPermission == null) {
            ogTvShowPermission = new OgTvShowBasePermission();
        }
        return ogTvShowPermission;
    }
	public void setOgTvShowPermission(OgTvShowBasePermission ogTvShowPermission) 
    {
        this.ogTvShowPermission = ogTvShowPermission;
    }

	public OgTvEpisodeBasePermission getOgTvEpisodePermission() 
    {
        if(ogTvEpisodePermission == null) {
            ogTvEpisodePermission = new OgTvEpisodeBasePermission();
        }
        return ogTvEpisodePermission;
    }
	public void setOgTvEpisodePermission(OgTvEpisodeBasePermission ogTvEpisodePermission) 
    {
        this.ogTvEpisodePermission = ogTvEpisodePermission;
    }

	public TwitterSummaryCardBasePermission getTwitterSummaryCardPermission() 
    {
        if(twitterSummaryCardPermission == null) {
            twitterSummaryCardPermission = new TwitterSummaryCardBasePermission();
        }
        return twitterSummaryCardPermission;
    }
	public void setTwitterSummaryCardPermission(TwitterSummaryCardBasePermission twitterSummaryCardPermission) 
    {
        this.twitterSummaryCardPermission = twitterSummaryCardPermission;
    }

	public TwitterPhotoCardBasePermission getTwitterPhotoCardPermission() 
    {
        if(twitterPhotoCardPermission == null) {
            twitterPhotoCardPermission = new TwitterPhotoCardBasePermission();
        }
        return twitterPhotoCardPermission;
    }
	public void setTwitterPhotoCardPermission(TwitterPhotoCardBasePermission twitterPhotoCardPermission) 
    {
        this.twitterPhotoCardPermission = twitterPhotoCardPermission;
    }

	public TwitterGalleryCardBasePermission getTwitterGalleryCardPermission() 
    {
        if(twitterGalleryCardPermission == null) {
            twitterGalleryCardPermission = new TwitterGalleryCardBasePermission();
        }
        return twitterGalleryCardPermission;
    }
	public void setTwitterGalleryCardPermission(TwitterGalleryCardBasePermission twitterGalleryCardPermission) 
    {
        this.twitterGalleryCardPermission = twitterGalleryCardPermission;
    }

	public TwitterAppCardBasePermission getTwitterAppCardPermission() 
    {
        if(twitterAppCardPermission == null) {
            twitterAppCardPermission = new TwitterAppCardBasePermission();
        }
        return twitterAppCardPermission;
    }
	public void setTwitterAppCardPermission(TwitterAppCardBasePermission twitterAppCardPermission) 
    {
        this.twitterAppCardPermission = twitterAppCardPermission;
    }

	public TwitterPlayerCardBasePermission getTwitterPlayerCardPermission() 
    {
        if(twitterPlayerCardPermission == null) {
            twitterPlayerCardPermission = new TwitterPlayerCardBasePermission();
        }
        return twitterPlayerCardPermission;
    }
	public void setTwitterPlayerCardPermission(TwitterPlayerCardBasePermission twitterPlayerCardPermission) 
    {
        this.twitterPlayerCardPermission = twitterPlayerCardPermission;
    }

	public TwitterProductCardBasePermission getTwitterProductCardPermission() 
    {
        if(twitterProductCardPermission == null) {
            twitterProductCardPermission = new TwitterProductCardBasePermission();
        }
        return twitterProductCardPermission;
    }
	public void setTwitterProductCardPermission(TwitterProductCardBasePermission twitterProductCardPermission) 
    {
        this.twitterProductCardPermission = twitterProductCardPermission;
    }

	public FetchRequestBasePermission getFetchRequestPermission() 
    {
        if(fetchRequestPermission == null) {
            fetchRequestPermission = new FetchRequestBasePermission();
        }
        return fetchRequestPermission;
    }
	public void setFetchRequestPermission(FetchRequestBasePermission fetchRequestPermission) 
    {
        this.fetchRequestPermission = fetchRequestPermission;
    }

	public PageInfoBasePermission getPageInfoPermission() 
    {
        if(pageInfoPermission == null) {
            pageInfoPermission = new PageInfoBasePermission();
        }
        return pageInfoPermission;
    }
	public void setPageInfoPermission(PageInfoBasePermission pageInfoPermission) 
    {
        this.pageInfoPermission = pageInfoPermission;
    }

	public PageFetchBasePermission getPageFetchPermission() 
    {
        if(pageFetchPermission == null) {
            pageFetchPermission = new PageFetchBasePermission();
        }
        return pageFetchPermission;
    }
	public void setPageFetchPermission(PageFetchBasePermission pageFetchPermission) 
    {
        this.pageFetchPermission = pageFetchPermission;
    }

	public LinkListBasePermission getLinkListPermission() 
    {
        if(linkListPermission == null) {
            linkListPermission = new LinkListBasePermission();
        }
        return linkListPermission;
    }
	public void setLinkListPermission(LinkListBasePermission linkListPermission) 
    {
        this.linkListPermission = linkListPermission;
    }

	public ImageSetBasePermission getImageSetPermission() 
    {
        if(imageSetPermission == null) {
            imageSetPermission = new ImageSetBasePermission();
        }
        return imageSetPermission;
    }
	public void setImageSetPermission(ImageSetBasePermission imageSetPermission) 
    {
        this.imageSetPermission = imageSetPermission;
    }

	public AudioSetBasePermission getAudioSetPermission() 
    {
        if(audioSetPermission == null) {
            audioSetPermission = new AudioSetBasePermission();
        }
        return audioSetPermission;
    }
	public void setAudioSetPermission(AudioSetBasePermission audioSetPermission) 
    {
        this.audioSetPermission = audioSetPermission;
    }

	public VideoSetBasePermission getVideoSetPermission() 
    {
        if(videoSetPermission == null) {
            videoSetPermission = new VideoSetBasePermission();
        }
        return videoSetPermission;
    }
	public void setVideoSetPermission(VideoSetBasePermission videoSetPermission) 
    {
        this.videoSetPermission = videoSetPermission;
    }

	public OpenGraphMetaBasePermission getOpenGraphMetaPermission() 
    {
        if(openGraphMetaPermission == null) {
            openGraphMetaPermission = new OpenGraphMetaBasePermission();
        }
        return openGraphMetaPermission;
    }
	public void setOpenGraphMetaPermission(OpenGraphMetaBasePermission openGraphMetaPermission) 
    {
        this.openGraphMetaPermission = openGraphMetaPermission;
    }

	public TwitterCardMetaBasePermission getTwitterCardMetaPermission() 
    {
        if(twitterCardMetaPermission == null) {
            twitterCardMetaPermission = new TwitterCardMetaBasePermission();
        }
        return twitterCardMetaPermission;
    }
	public void setTwitterCardMetaPermission(TwitterCardMetaBasePermission twitterCardMetaPermission) 
    {
        this.twitterCardMetaPermission = twitterCardMetaPermission;
    }

	public DomainInfoBasePermission getDomainInfoPermission() 
    {
        if(domainInfoPermission == null) {
            domainInfoPermission = new DomainInfoBasePermission();
        }
        return domainInfoPermission;
    }
	public void setDomainInfoPermission(DomainInfoBasePermission domainInfoPermission) 
    {
        this.domainInfoPermission = domainInfoPermission;
    }

	public UrlRatingBasePermission getUrlRatingPermission() 
    {
        if(urlRatingPermission == null) {
            urlRatingPermission = new UrlRatingBasePermission();
        }
        return urlRatingPermission;
    }
	public void setUrlRatingPermission(UrlRatingBasePermission urlRatingPermission) 
    {
        this.urlRatingPermission = urlRatingPermission;
    }

	public ServiceInfoBasePermission getServiceInfoPermission() 
    {
        if(serviceInfoPermission == null) {
            serviceInfoPermission = new ServiceInfoBasePermission();
        }
        return serviceInfoPermission;
    }
	public void setServiceInfoPermission(ServiceInfoBasePermission serviceInfoPermission) 
    {
        this.serviceInfoPermission = serviceInfoPermission;
    }

	public FiveTenBasePermission getFiveTenPermission() 
    {
        if(fiveTenPermission == null) {
            fiveTenPermission = new FiveTenBasePermission();
        }
        return fiveTenPermission;
    }
	public void setFiveTenPermission(FiveTenBasePermission fiveTenPermission) 
    {
        this.fiveTenPermission = fiveTenPermission;
    }


	public boolean isPermissionRequired(String resource, String action) 
    {
        if(resource == null || resource.isEmpty()) {
            return false;   // ???
        } else if(resource.equals("ApiConsumer")) {
            return getApiConsumerPermission().isPermissionRequired(action);
        } else if(resource.equals("User")) {
            return getUserPermission().isPermissionRequired(action);
        } else if(resource.equals("RobotsTextFile")) {
            return getRobotsTextFilePermission().isPermissionRequired(action);
        } else if(resource.equals("RobotsTextRefresh")) {
            return getRobotsTextRefreshPermission().isPermissionRequired(action);
        } else if(resource.equals("OgProfile")) {
            return getOgProfilePermission().isPermissionRequired(action);
        } else if(resource.equals("OgWebsite")) {
            return getOgWebsitePermission().isPermissionRequired(action);
        } else if(resource.equals("OgBlog")) {
            return getOgBlogPermission().isPermissionRequired(action);
        } else if(resource.equals("OgArticle")) {
            return getOgArticlePermission().isPermissionRequired(action);
        } else if(resource.equals("OgBook")) {
            return getOgBookPermission().isPermissionRequired(action);
        } else if(resource.equals("OgVideo")) {
            return getOgVideoPermission().isPermissionRequired(action);
        } else if(resource.equals("OgMovie")) {
            return getOgMoviePermission().isPermissionRequired(action);
        } else if(resource.equals("OgTvShow")) {
            return getOgTvShowPermission().isPermissionRequired(action);
        } else if(resource.equals("OgTvEpisode")) {
            return getOgTvEpisodePermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterSummaryCard")) {
            return getTwitterSummaryCardPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterPhotoCard")) {
            return getTwitterPhotoCardPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterGalleryCard")) {
            return getTwitterGalleryCardPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterAppCard")) {
            return getTwitterAppCardPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterPlayerCard")) {
            return getTwitterPlayerCardPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterProductCard")) {
            return getTwitterProductCardPermission().isPermissionRequired(action);
        } else if(resource.equals("FetchRequest")) {
            return getFetchRequestPermission().isPermissionRequired(action);
        } else if(resource.equals("PageInfo")) {
            return getPageInfoPermission().isPermissionRequired(action);
        } else if(resource.equals("PageFetch")) {
            return getPageFetchPermission().isPermissionRequired(action);
        } else if(resource.equals("LinkList")) {
            return getLinkListPermission().isPermissionRequired(action);
        } else if(resource.equals("ImageSet")) {
            return getImageSetPermission().isPermissionRequired(action);
        } else if(resource.equals("AudioSet")) {
            return getAudioSetPermission().isPermissionRequired(action);
        } else if(resource.equals("VideoSet")) {
            return getVideoSetPermission().isPermissionRequired(action);
        } else if(resource.equals("OpenGraphMeta")) {
            return getOpenGraphMetaPermission().isPermissionRequired(action);
        } else if(resource.equals("TwitterCardMeta")) {
            return getTwitterCardMetaPermission().isPermissionRequired(action);
        } else if(resource.equals("DomainInfo")) {
            return getDomainInfoPermission().isPermissionRequired(action);
        } else if(resource.equals("UrlRating")) {
            return getUrlRatingPermission().isPermissionRequired(action);
        } else if(resource.equals("ServiceInfo")) {
            return getServiceInfoPermission().isPermissionRequired(action);
        } else if(resource.equals("FiveTen")) {
            return getFiveTenPermission().isPermissionRequired(action);
        } else {
            log.warning("Unrecognized resource = " + resource);
            return true;   // ???
        }
    }

    // TBD: "instance" field is currently ignored...
	public boolean isPermissionRequired(String permissionName) 
    {
        if(permissionName == null || permissionName.isEmpty()) {
            return false;   // ???
        } else if(permissionName.startsWith("ApiConsumer::")) {
            String resource = "ApiConsumer";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("User::")) {
            String resource = "User";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("RobotsTextFile::")) {
            String resource = "RobotsTextFile";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("RobotsTextRefresh::")) {
            String resource = "RobotsTextRefresh";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("OgProfile::")) {
            String resource = "OgProfile";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("OgWebsite::")) {
            String resource = "OgWebsite";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("OgBlog::")) {
            String resource = "OgBlog";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("OgArticle::")) {
            String resource = "OgArticle";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("OgBook::")) {
            String resource = "OgBook";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("OgVideo::")) {
            String resource = "OgVideo";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("OgMovie::")) {
            String resource = "OgMovie";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("OgTvShow::")) {
            String resource = "OgTvShow";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("OgTvEpisode::")) {
            String resource = "OgTvEpisode";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterSummaryCard::")) {
            String resource = "TwitterSummaryCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterPhotoCard::")) {
            String resource = "TwitterPhotoCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterGalleryCard::")) {
            String resource = "TwitterGalleryCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterAppCard::")) {
            String resource = "TwitterAppCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterPlayerCard::")) {
            String resource = "TwitterPlayerCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterProductCard::")) {
            String resource = "TwitterProductCard";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("FetchRequest::")) {
            String resource = "FetchRequest";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("PageInfo::")) {
            String resource = "PageInfo";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("PageFetch::")) {
            String resource = "PageFetch";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("LinkList::")) {
            String resource = "LinkList";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ImageSet::")) {
            String resource = "ImageSet";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("AudioSet::")) {
            String resource = "AudioSet";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("VideoSet::")) {
            String resource = "VideoSet";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("OpenGraphMeta::")) {
            String resource = "OpenGraphMeta";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TwitterCardMeta::")) {
            String resource = "TwitterCardMeta";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("DomainInfo::")) {
            String resource = "DomainInfo";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("UrlRating::")) {
            String resource = "UrlRating";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ServiceInfo::")) {
            String resource = "ServiceInfo";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("FiveTen::")) {
            String resource = "FiveTen";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else {
            log.warning("Unrecognized permissionName = " + permissionName);
            return true;   // ???
        }
    }

}
