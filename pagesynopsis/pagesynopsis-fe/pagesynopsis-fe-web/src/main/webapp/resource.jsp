<%@ page import="com.pagesynopsis.ws.core.*, com.pagesynopsis.af.auth.*, com.pagesynopsis.fe.*, com.pagesynopsis.fe.core.*, com.pagesynopsis.fe.bean.*, com.pagesynopsis.fe.util.*, com.pagesynopsis.wa.service.*, com.pagesynopsis.util.*, com.pagesynopsis.app.util.*, com.pagesynopsis.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%
//{1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = URLHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
// ...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
%><%
//[3] Local "global" variables.
//EncodedQueryParamStructJsBean encodedQueryParamBean = null;
//DecodedQueryParamStructJsBean decodedQueryParamBean = null;
// ...
%><!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | Resources - Fetch Web page title and description through Web Services API</title>
    <meta name="author" content="Aery software">
    <meta name="description" content="<%=brandDisplayName%> Service is a web page summary service. Service is exposed through REST Web services API. Currently, it uses HTML page meta tags.">

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/bootstrap/blueridge/bootstrap-2.0.css">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/jquery-ui-1.9.2/pepper-grinder/jquery-ui-1.9.2.custom.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis.css"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis-<%=appBrand%>.css"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis-screen.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis-mobile.css" media="only screen and (max-device-width: 480px)"/>
<!-- 
    <link href="/css/bootstrap.responsive-2.0.css" rel="stylesheet">
 -->

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-pagesynopsis.ico" />
	<link rel="icon" href="/img/favicon-pagesynopsis.ico" type="image/x-icon" />

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://www.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

    <!--  Twitter @anywhere API  -->
    <script src="http://platform.twitter.com/anywhere.js?id=aQv8kescAfPXIVijKI63Iw&v=1" type="text/javascript"></script>
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="/"><%=brandDisplayName%><sup>&beta;</sup></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li><a href="/page/pageinfo">Page Info</a></li>
              <li><a href="/page/linklist">Link List</a></li>
              <li><a href="/page/imageset">Image Set</a></li>
<%
if(DebugUtil.isDevelFeatureEnabled()) {
%>
<%
}
%>
              <li><a href="/urlcoder">URL Encoder</a></li>
            </ul>
            <p class="navbar-text pull-right">
              <a id="anchor_topmenu_tweet" href="#" title="Share it on Twitter"><i class="icon-retweet icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_share" href="#" title="Email it to your friend"><i class="icon-share icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_email" href="contact:info+pagesynopsis+com?subject=Re: <%=brandDisplayName%>" title="Email us"><i class="icon-envelope icon-white"></i></a>
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <h1><%=brandDisplayName%></h1>
        <p>
        <%=brandDisplayName%> is a Web page scraping service. 
        Please sign up now to be notified of our upcoming releases. 
        </p>
        <p><a id="anchor_mainmenu_signup" class="btn btn-primary btn-large" title="Please provide your email address, and we will keep you posted on upcoming pre-release trials at <%=brandDisplayName%>.">Sign up &raquo;</a></p>
      </div>

      <!-- Example row of columns -->
      <div class="row-fluid">
        <div class="span12">
          <h2>Web Page Scraping</h2>
          (TBD)
<!--
          <p><a class="btn" href="/reminder">View details &raquo;</a></p>
-->
       </div>
      </div>

      <hr>

      <footer>
        <p>
        &copy; <%=brandDisplayName%> 2012&nbsp; 
        <a id="anchor_feedback_footer" href="#" title="Feedback"><i class="icon-comment"></i></a>
        <span class="pull-right">
<!-- 
              <a href="/resource">Resource</a> | 
              <a href="/about">About</a> | 
-->
        <a href="/contact">Contact</a> | <a href="http://blog.pagesynopsis.com/">Blog</a>
        </span>
        </p>
      </footer>

    </div> <!-- /container -->



    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://www.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://www.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://www.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/emailhelper-1.0.js"></script>


    <script>
        // Global vars
    	var statusHelper;
    	var tweetHelper;
    	var signupHelper;
    	var feedbackHelper;
    	var emailHelper;
    </script>
    <script>
    $(function() {
    	// Init...
    	statusHelper = new webstoa.StatusHelper(400, 65);
    	// test.
    	//statusHelper.update('test status message');

    	var servicename = "<%=brandDisplayName%>";
    	var launchname = "Initial pre-launch trial";
    	var pageid = "home";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);

    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "home";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);

    	var senderEmail = webstoa.EmailUtil.decodeEmailAddress('gaeemail+gmail' + '+com');
    	var defaultSenderName = "<%=brandDisplayName%>";
    	var subject = "Interesting site: <%=brandDisplayName%>";
    	var defaultMessage = "Hi,\n\n<%=brandDisplayName%>, http://www.pagesynopsis.com/, is a Web service for fetching HTML page title and description. Please check it out.\n\n-me\n";
    	emailHelper = new webstoa.EmailHelper(senderEmail, subject, defaultMessage, defaultSenderName);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_mainmenu_signup").click(function() {
            if(DEBUG_ENABLED) console.log("Signup button clicked."); 

       	    signupHelper.signup();
       	    //statusHelper.update('Thanks for signing up.');
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_share").click(function() {
            if(DEBUG_ENABLED) console.log("Share button clicked."); 
       	    emailHelper.email();
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 

            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_tweet").click(function() {
            if(DEBUG_ENABLED) console.log("Tweet button clicked."); 

            if(!tweetHelper) {
                tweetHelper = new webstoa.TweetHelper();
            }
        	if(tweetHelper) {
	            var tweetTitle = '<%=brandDisplayName%> Message';
	            var tweetMessage = '<%=brandDisplayName%>, http://www.pagesynopsis.com/, is a Web service for fetching HTML page title and description. Please check it out.';
	            tweetHelper.tweet(tweetTitle, tweetMessage);
        	} else {
        		// ????
        	}
		    return false;
        });
    });
    </script>



<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

  </body>
</html>
