/*
Help Notice Manager.
*/

//$(function() {
    var $helpDialog = $('<div id="helpnotice_help_dialog" title="Help Dialog" style="display: none;"><div id="helpnotice_help_main" style="width:100%;">' 
            + '<div id="helpnotice_help_head" style="width:100%;padding:5px 3px;"><div id="helpnotice_help_noticetitle" style="font-size:1.2em;font-weight:bold"></div></div>' 
            + '<div id="helpnotice_help_body2" style="width:100%;padding:5px 3px;"><div id="helpnotice_help_noticecontent"></div></div>' 
            + '</div></div>');
//    var $helpDialog = $('<div id="helpnotice_help_dialog" title="Help Dialog" style="display: none;">' 
//            + '<div id="helpnotice_help_noticetitle" style="font-size:1.05em;"></div>' 
//            + '<div id="helpnotice_help_noticecontent"></div>' 
//            + '</div>');
    $('body').append($helpDialog);
//});


// HelpNoticeManager object.
var pagesynopsis = pagesynopsis || {};
pagesynopsis.HelpNoticeManager = ( function() {

  var cls = function(helpNoticeUuid) {
      this.noticeUuid = helpNoticeUuid ? helpNoticeUuid : '';

      this.showHelp = function(helpNoticeUuid) {
        if(DEBUG_ENABLED) console.log("showHelp() called with helpNoticeUuid = " + helpNoticeUuid);

        if(! helpNoticeUuid) {
            helpNoticeUuid = this.noticeUuid;
        }
        // ...        
        
        // Reset the fields?
        $('#helpnotice_help_noticetitle').text('');
        $('#helpnotice_help_noticecontent').text('');
        
        var endpointUrl = '/ajax/helpnotice/' + helpNoticeUuid;
        $.ajax({
            type: 'GET',
            url: endpointUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data, textStatus) {
                // TBD
                if(DEBUG_ENABLED) console.log("Successfully retrieved. textStatus = " + textStatus);
                if(DEBUG_ENABLED) console.log("data = " + data);
                if(DEBUG_ENABLED) console.log(data);

                if(data) {
                    var title = data.title;
                    //window.alert("Title = " + title);
                    var content = data.content;
                    var format = data.format;
                    if(format == 'text') {
                    	// TBD:
                    	// escape for html to use text() ???
                    	// or, just use html() ??? <- Does that work?
                        $('#helpnotice_help_noticetitle').text(title);
                        $('#helpnotice_help_noticecontent').text(content);
                    } else if(format == 'html') {
                        // TBD: ...	
                        $('#helpnotice_help_noticetitle').html(title);
                        $('#helpnotice_help_noticecontent').html(content);
                    } else {
                    	// ????
                    	if(DEBUG_ENABLED) console.log("Unrecognized format = " + format);
                    }

                    $('#helpnotice_help_noticetitle').html(title);
                    $('#helpnotice_help_noticecontent').html(content);
                }
            },
            error: function(req, textStatus) {
                if(DEBUG_ENABLED) console.log("Failed to retrieve. textStatus = " + textStatus);

                // TBD:
                // Ignore for now...
                var errMsg = "<h3>Failed to retrieve the gloss.</h3>";
                $('#helpnotice_help_noticetitle').html(errMsg);
                $('#helpnotice_help_noticecontent').html('');
            }
        });

        $( "#helpnotice_help_dialog" ).dialog({
            // height: 500,
            //width: 450,
        	height: 'auto',
        	//width: 'auto',
        	resize: true,
            modal: true,
            buttons: {
                OK: function() {
                    $(this).dialog('close'); 
                }
            }
        });        
    };
  };

  return cls;
})();
