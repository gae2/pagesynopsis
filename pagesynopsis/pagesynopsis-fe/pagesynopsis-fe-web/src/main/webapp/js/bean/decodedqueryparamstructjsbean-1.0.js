//////////////////////////////////////////////////////////
// <script src="/js/bean/decodedqueryparamstructjsbean-1.0.js"></script>
// Last modified time: 1383422544446.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.DecodedQueryParamStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var paramType;
    var originalString;
    var decodedString;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getParamType = function() { return paramType; };
    this.setParamType = function(value) { paramType = value; };
    this.getOriginalString = function() { return originalString; };
    this.setOriginalString = function(value) { originalString = value; };
    this.getDecodedString = function() { return decodedString; };
    this.setDecodedString = function(value) { decodedString = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.DecodedQueryParamStructJsBean();

      if(paramType !== undefined && paramType != null) {
        o.setParamType(paramType);
      }
      if(originalString !== undefined && originalString != null) {
        o.setOriginalString(originalString);
      }
      if(decodedString !== undefined && decodedString != null) {
        o.setDecodedString(decodedString);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(paramType !== undefined && paramType != null) {
        jsonObj.paramType = paramType;
      } // Otherwise ignore...
      if(originalString !== undefined && originalString != null) {
        jsonObj.originalString = originalString;
      } // Otherwise ignore...
      if(decodedString !== undefined && decodedString != null) {
        jsonObj.decodedString = decodedString;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(paramType) {
        str += "\"paramType\":\"" + paramType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"paramType\":null, ";
      }
      if(originalString) {
        str += "\"originalString\":\"" + originalString + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"originalString\":null, ";
      }
      if(decodedString) {
        str += "\"decodedString\":\"" + decodedString + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"decodedString\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "paramType:" + paramType + ", ";
      str += "originalString:" + originalString + ", ";
      str += "decodedString:" + decodedString + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.DecodedQueryParamStructJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.DecodedQueryParamStructJsBean();

  if(obj.paramType !== undefined && obj.paramType != null) {
    o.setParamType(obj.paramType);
  }
  if(obj.originalString !== undefined && obj.originalString != null) {
    o.setOriginalString(obj.originalString);
  }
  if(obj.decodedString !== undefined && obj.decodedString != null) {
    o.setDecodedString(obj.decodedString);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

pagesynopsis.wa.bean.DecodedQueryParamStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.DecodedQueryParamStructJsBean.create(jsonObj);
  return obj;
};
