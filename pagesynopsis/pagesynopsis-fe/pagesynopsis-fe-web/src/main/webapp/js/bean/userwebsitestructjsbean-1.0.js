//////////////////////////////////////////////////////////
// <script src="/js/bean/userwebsitestructjsbean-1.0.js"></script>
// Last modified time: 1383422544347.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.UserWebsiteStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var primarySite;
    var homePage;
    var blogSite;
    var portfolioSite;
    var twitterProfile;
    var facebookProfile;
    var googlePlusProfile;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getPrimarySite = function() { return primarySite; };
    this.setPrimarySite = function(value) { primarySite = value; };
    this.getHomePage = function() { return homePage; };
    this.setHomePage = function(value) { homePage = value; };
    this.getBlogSite = function() { return blogSite; };
    this.setBlogSite = function(value) { blogSite = value; };
    this.getPortfolioSite = function() { return portfolioSite; };
    this.setPortfolioSite = function(value) { portfolioSite = value; };
    this.getTwitterProfile = function() { return twitterProfile; };
    this.setTwitterProfile = function(value) { twitterProfile = value; };
    this.getFacebookProfile = function() { return facebookProfile; };
    this.setFacebookProfile = function(value) { facebookProfile = value; };
    this.getGooglePlusProfile = function() { return googlePlusProfile; };
    this.setGooglePlusProfile = function(value) { googlePlusProfile = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.UserWebsiteStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(primarySite !== undefined && primarySite != null) {
        o.setPrimarySite(primarySite);
      }
      if(homePage !== undefined && homePage != null) {
        o.setHomePage(homePage);
      }
      if(blogSite !== undefined && blogSite != null) {
        o.setBlogSite(blogSite);
      }
      if(portfolioSite !== undefined && portfolioSite != null) {
        o.setPortfolioSite(portfolioSite);
      }
      if(twitterProfile !== undefined && twitterProfile != null) {
        o.setTwitterProfile(twitterProfile);
      }
      if(facebookProfile !== undefined && facebookProfile != null) {
        o.setFacebookProfile(facebookProfile);
      }
      if(googlePlusProfile !== undefined && googlePlusProfile != null) {
        o.setGooglePlusProfile(googlePlusProfile);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(primarySite !== undefined && primarySite != null) {
        jsonObj.primarySite = primarySite;
      } // Otherwise ignore...
      if(homePage !== undefined && homePage != null) {
        jsonObj.homePage = homePage;
      } // Otherwise ignore...
      if(blogSite !== undefined && blogSite != null) {
        jsonObj.blogSite = blogSite;
      } // Otherwise ignore...
      if(portfolioSite !== undefined && portfolioSite != null) {
        jsonObj.portfolioSite = portfolioSite;
      } // Otherwise ignore...
      if(twitterProfile !== undefined && twitterProfile != null) {
        jsonObj.twitterProfile = twitterProfile;
      } // Otherwise ignore...
      if(facebookProfile !== undefined && facebookProfile != null) {
        jsonObj.facebookProfile = facebookProfile;
      } // Otherwise ignore...
      if(googlePlusProfile !== undefined && googlePlusProfile != null) {
        jsonObj.googlePlusProfile = googlePlusProfile;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(primarySite) {
        str += "\"primarySite\":\"" + primarySite + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"primarySite\":null, ";
      }
      if(homePage) {
        str += "\"homePage\":\"" + homePage + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"homePage\":null, ";
      }
      if(blogSite) {
        str += "\"blogSite\":\"" + blogSite + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"blogSite\":null, ";
      }
      if(portfolioSite) {
        str += "\"portfolioSite\":\"" + portfolioSite + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"portfolioSite\":null, ";
      }
      if(twitterProfile) {
        str += "\"twitterProfile\":\"" + twitterProfile + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"twitterProfile\":null, ";
      }
      if(facebookProfile) {
        str += "\"facebookProfile\":\"" + facebookProfile + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"facebookProfile\":null, ";
      }
      if(googlePlusProfile) {
        str += "\"googlePlusProfile\":\"" + googlePlusProfile + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"googlePlusProfile\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "primarySite:" + primarySite + ", ";
      str += "homePage:" + homePage + ", ";
      str += "blogSite:" + blogSite + ", ";
      str += "portfolioSite:" + portfolioSite + ", ";
      str += "twitterProfile:" + twitterProfile + ", ";
      str += "facebookProfile:" + facebookProfile + ", ";
      str += "googlePlusProfile:" + googlePlusProfile + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.UserWebsiteStructJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.UserWebsiteStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.primarySite !== undefined && obj.primarySite != null) {
    o.setPrimarySite(obj.primarySite);
  }
  if(obj.homePage !== undefined && obj.homePage != null) {
    o.setHomePage(obj.homePage);
  }
  if(obj.blogSite !== undefined && obj.blogSite != null) {
    o.setBlogSite(obj.blogSite);
  }
  if(obj.portfolioSite !== undefined && obj.portfolioSite != null) {
    o.setPortfolioSite(obj.portfolioSite);
  }
  if(obj.twitterProfile !== undefined && obj.twitterProfile != null) {
    o.setTwitterProfile(obj.twitterProfile);
  }
  if(obj.facebookProfile !== undefined && obj.facebookProfile != null) {
    o.setFacebookProfile(obj.facebookProfile);
  }
  if(obj.googlePlusProfile !== undefined && obj.googlePlusProfile != null) {
    o.setGooglePlusProfile(obj.googlePlusProfile);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

pagesynopsis.wa.bean.UserWebsiteStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.UserWebsiteStructJsBean.create(jsonObj);
  return obj;
};
