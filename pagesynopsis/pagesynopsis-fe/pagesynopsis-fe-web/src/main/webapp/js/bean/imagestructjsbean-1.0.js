//////////////////////////////////////////////////////////
// <script src="/js/bean/imagestructjsbean-1.0.js"></script>
// Last modified time: 1383422544470.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.ImageStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var id;
    var alt;
    var src;
    var srcUrl;
    var mediaType;
    var widthAttr;
    var width;
    var heightAttr;
    var height;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getId = function() { return id; };
    this.setId = function(value) { id = value; };
    this.getAlt = function() { return alt; };
    this.setAlt = function(value) { alt = value; };
    this.getSrc = function() { return src; };
    this.setSrc = function(value) { src = value; };
    this.getSrcUrl = function() { return srcUrl; };
    this.setSrcUrl = function(value) { srcUrl = value; };
    this.getMediaType = function() { return mediaType; };
    this.setMediaType = function(value) { mediaType = value; };
    this.getWidthAttr = function() { return widthAttr; };
    this.setWidthAttr = function(value) { widthAttr = value; };
    this.getWidth = function() { return width; };
    this.setWidth = function(value) { width = value; };
    this.getHeightAttr = function() { return heightAttr; };
    this.setHeightAttr = function(value) { heightAttr = value; };
    this.getHeight = function() { return height; };
    this.setHeight = function(value) { height = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.ImageStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(id !== undefined && id != null) {
        o.setId(id);
      }
      if(alt !== undefined && alt != null) {
        o.setAlt(alt);
      }
      if(src !== undefined && src != null) {
        o.setSrc(src);
      }
      if(srcUrl !== undefined && srcUrl != null) {
        o.setSrcUrl(srcUrl);
      }
      if(mediaType !== undefined && mediaType != null) {
        o.setMediaType(mediaType);
      }
      if(widthAttr !== undefined && widthAttr != null) {
        o.setWidthAttr(widthAttr);
      }
      if(width !== undefined && width != null) {
        o.setWidth(width);
      }
      if(heightAttr !== undefined && heightAttr != null) {
        o.setHeightAttr(heightAttr);
      }
      if(height !== undefined && height != null) {
        o.setHeight(height);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(id !== undefined && id != null) {
        jsonObj.id = id;
      } // Otherwise ignore...
      if(alt !== undefined && alt != null) {
        jsonObj.alt = alt;
      } // Otherwise ignore...
      if(src !== undefined && src != null) {
        jsonObj.src = src;
      } // Otherwise ignore...
      if(srcUrl !== undefined && srcUrl != null) {
        jsonObj.srcUrl = srcUrl;
      } // Otherwise ignore...
      if(mediaType !== undefined && mediaType != null) {
        jsonObj.mediaType = mediaType;
      } // Otherwise ignore...
      if(widthAttr !== undefined && widthAttr != null) {
        jsonObj.widthAttr = widthAttr;
      } // Otherwise ignore...
      if(width !== undefined && width != null) {
        jsonObj.width = width;
      } // Otherwise ignore...
      if(heightAttr !== undefined && heightAttr != null) {
        jsonObj.heightAttr = heightAttr;
      } // Otherwise ignore...
      if(height !== undefined && height != null) {
        jsonObj.height = height;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(id) {
        str += "\"id\":\"" + id + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"id\":null, ";
      }
      if(alt) {
        str += "\"alt\":\"" + alt + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"alt\":null, ";
      }
      if(src) {
        str += "\"src\":\"" + src + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"src\":null, ";
      }
      if(srcUrl) {
        str += "\"srcUrl\":\"" + srcUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"srcUrl\":null, ";
      }
      if(mediaType) {
        str += "\"mediaType\":\"" + mediaType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"mediaType\":null, ";
      }
      if(widthAttr) {
        str += "\"widthAttr\":\"" + widthAttr + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"widthAttr\":null, ";
      }
      if(width) {
        str += "\"width\":" + width + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"width\":null, ";
      }
      if(heightAttr) {
        str += "\"heightAttr\":\"" + heightAttr + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"heightAttr\":null, ";
      }
      if(height) {
        str += "\"height\":" + height + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"height\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "id:" + id + ", ";
      str += "alt:" + alt + ", ";
      str += "src:" + src + ", ";
      str += "srcUrl:" + srcUrl + ", ";
      str += "mediaType:" + mediaType + ", ";
      str += "widthAttr:" + widthAttr + ", ";
      str += "width:" + width + ", ";
      str += "heightAttr:" + heightAttr + ", ";
      str += "height:" + height + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.ImageStructJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.ImageStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.id !== undefined && obj.id != null) {
    o.setId(obj.id);
  }
  if(obj.alt !== undefined && obj.alt != null) {
    o.setAlt(obj.alt);
  }
  if(obj.src !== undefined && obj.src != null) {
    o.setSrc(obj.src);
  }
  if(obj.srcUrl !== undefined && obj.srcUrl != null) {
    o.setSrcUrl(obj.srcUrl);
  }
  if(obj.mediaType !== undefined && obj.mediaType != null) {
    o.setMediaType(obj.mediaType);
  }
  if(obj.widthAttr !== undefined && obj.widthAttr != null) {
    o.setWidthAttr(obj.widthAttr);
  }
  if(obj.width !== undefined && obj.width != null) {
    o.setWidth(obj.width);
  }
  if(obj.heightAttr !== undefined && obj.heightAttr != null) {
    o.setHeightAttr(obj.heightAttr);
  }
  if(obj.height !== undefined && obj.height != null) {
    o.setHeight(obj.height);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

pagesynopsis.wa.bean.ImageStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.ImageStructJsBean.create(jsonObj);
  return obj;
};
