//////////////////////////////////////////////////////////
// <script src="/js/bean/ogactorstructjsbean-1.0.js"></script>
// Last modified time: 1383422544579.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.OgActorStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var profile;
    var role;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getProfile = function() { return profile; };
    this.setProfile = function(value) { profile = value; };
    this.getRole = function() { return role; };
    this.setRole = function(value) { role = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.OgActorStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(profile !== undefined && profile != null) {
        o.setProfile(profile);
      }
      if(role !== undefined && role != null) {
        o.setRole(role);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(profile !== undefined && profile != null) {
        jsonObj.profile = profile;
      } // Otherwise ignore...
      if(role !== undefined && role != null) {
        jsonObj.role = role;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(profile) {
        str += "\"profile\":\"" + profile + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"profile\":null, ";
      }
      if(role) {
        str += "\"role\":\"" + role + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"role\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "profile:" + profile + ", ";
      str += "role:" + role + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.OgActorStructJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.OgActorStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.profile !== undefined && obj.profile != null) {
    o.setProfile(obj.profile);
  }
  if(obj.role !== undefined && obj.role != null) {
    o.setRole(obj.role);
  }
    
  return o;
};

pagesynopsis.wa.bean.OgActorStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.OgActorStructJsBean.create(jsonObj);
  return obj;
};
