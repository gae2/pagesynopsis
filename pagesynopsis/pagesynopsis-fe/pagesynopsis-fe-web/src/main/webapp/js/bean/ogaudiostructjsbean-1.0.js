//////////////////////////////////////////////////////////
// <script src="/js/bean/ogaudiostructjsbean-1.0.js"></script>
// Last modified time: 1383422544547.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.OgAudioStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var url;
    var secureUrl;
    var type;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getUrl = function() { return url; };
    this.setUrl = function(value) { url = value; };
    this.getSecureUrl = function() { return secureUrl; };
    this.setSecureUrl = function(value) { secureUrl = value; };
    this.getType = function() { return type; };
    this.setType = function(value) { type = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.OgAudioStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(url !== undefined && url != null) {
        o.setUrl(url);
      }
      if(secureUrl !== undefined && secureUrl != null) {
        o.setSecureUrl(secureUrl);
      }
      if(type !== undefined && type != null) {
        o.setType(type);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(url !== undefined && url != null) {
        jsonObj.url = url;
      } // Otherwise ignore...
      if(secureUrl !== undefined && secureUrl != null) {
        jsonObj.secureUrl = secureUrl;
      } // Otherwise ignore...
      if(type !== undefined && type != null) {
        jsonObj.type = type;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(url) {
        str += "\"url\":\"" + url + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"url\":null, ";
      }
      if(secureUrl) {
        str += "\"secureUrl\":\"" + secureUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"secureUrl\":null, ";
      }
      if(type) {
        str += "\"type\":\"" + type + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"type\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "url:" + url + ", ";
      str += "secureUrl:" + secureUrl + ", ";
      str += "type:" + type + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.OgAudioStructJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.OgAudioStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.url !== undefined && obj.url != null) {
    o.setUrl(obj.url);
  }
  if(obj.secureUrl !== undefined && obj.secureUrl != null) {
    o.setSecureUrl(obj.secureUrl);
  }
  if(obj.type !== undefined && obj.type != null) {
    o.setType(obj.type);
  }
    
  return o;
};

pagesynopsis.wa.bean.OgAudioStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.OgAudioStructJsBean.create(jsonObj);
  return obj;
};
