package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "videoStructs")
@XmlType(propOrder = {"videoStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class VideoStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(VideoStructListStub.class.getName());

    private List<VideoStructStub> videoStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public VideoStructListStub()
    {
        this(new ArrayList<VideoStructStub>());
    }
    public VideoStructListStub(List<VideoStructStub> videoStructs)
    {
        this(videoStructs, null);
    }
    public VideoStructListStub(List<VideoStructStub> videoStructs, String forwardCursor)
    {
        this.videoStructs = videoStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(videoStructs == null) {
            return true;
        } else {
            return videoStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(videoStructs == null) {
            return 0;
        } else {
            return videoStructs.size();
        }
    }


    @XmlElement(name = "videoStruct")
    public List<VideoStructStub> getVideoStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<VideoStructStub> getList()
    {
        return videoStructs;
    }
    public void setList(List<VideoStructStub> videoStructs)
    {
        this.videoStructs = videoStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<VideoStructStub> it = this.videoStructs.iterator();
        while(it.hasNext()) {
            VideoStructStub videoStruct = it.next();
            sb.append(videoStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static VideoStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of VideoStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write VideoStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write VideoStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write VideoStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static VideoStructListStub fromJsonString(String jsonStr)
    {
        try {
            VideoStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, VideoStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into VideoStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into VideoStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into VideoStructListStub object.", e);
        }
        
        return null;
    }

}
