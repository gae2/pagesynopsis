package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.KeyValueRelationStruct;
import com.pagesynopsis.ws.bean.KeyValueRelationStructBean;
import com.pagesynopsis.ws.stub.KeyValueRelationStructStub;


public class KeyValueRelationStructResourceUtil
{
    private static final Logger log = Logger.getLogger(KeyValueRelationStructResourceUtil.class.getName());

    // Static methods only.
    private KeyValueRelationStructResourceUtil() {}

    public static KeyValueRelationStructBean convertKeyValueRelationStructStubToBean(KeyValueRelationStruct stub)
    {
        KeyValueRelationStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null KeyValueRelationStructBean is returned.");
        } else {
            bean = new KeyValueRelationStructBean();
            bean.setUuid(stub.getUuid());
            bean.setKey(stub.getKey());
            bean.setValue(stub.getValue());
            bean.setNote(stub.getNote());
            bean.setRelation(stub.getRelation());
        }
        return bean;
    }

}
