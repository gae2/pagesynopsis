package com.pagesynopsis.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.stub.ErrorStub;

@Provider
public class FoundElsewhereExceptionMapper implements ExceptionMapper<FoundElsewhereRsException>
{
    public Response toResponse(FoundElsewhereRsException ex) {
        return Response.status(StatusCode.FOUND)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
