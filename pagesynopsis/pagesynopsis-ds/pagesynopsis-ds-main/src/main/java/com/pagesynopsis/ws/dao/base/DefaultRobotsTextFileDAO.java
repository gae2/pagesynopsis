package com.pagesynopsis.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.RobotsTextFileDAO;
import com.pagesynopsis.ws.data.RobotsTextFileDataObject;


public class DefaultRobotsTextFileDAO extends DefaultDAOBase implements RobotsTextFileDAO
{
    private static final Logger log = Logger.getLogger(DefaultRobotsTextFileDAO.class.getName()); 

    // Returns the robotsTextFile for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public RobotsTextFileDataObject getRobotsTextFile(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        RobotsTextFileDataObject robotsTextFile = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = RobotsTextFileDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = RobotsTextFileDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                robotsTextFile = pm.getObjectById(RobotsTextFileDataObject.class, key);
                robotsTextFile.getGroups();  // "Touch". Otherwise this field will not be fetched by default.
                robotsTextFile.getSitemaps();  // "Touch". Otherwise this field will not be fetched by default.
                robotsTextFile.getContent();  // "Touch". Otherwise this field will not be fetched by default.
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve robotsTextFile for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return robotsTextFile;
	}

    @Override
    public List<RobotsTextFileDataObject> getRobotsTextFiles(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<RobotsTextFileDataObject> robotsTextFiles = null;
        if(guids != null && !guids.isEmpty()) {
            robotsTextFiles = new ArrayList<RobotsTextFileDataObject>();
            for(String guid : guids) {
                RobotsTextFileDataObject obj = getRobotsTextFile(guid); 
                robotsTextFiles.add(obj);
            }
	    }

        log.finer("END");
        return robotsTextFiles;
    }

    @Override
    public List<RobotsTextFileDataObject> getAllRobotsTextFiles() throws BaseException
	{
	    return getAllRobotsTextFiles(null, null, null);
    }


    @Override
    public List<RobotsTextFileDataObject> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllRobotsTextFiles(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<RobotsTextFileDataObject> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<RobotsTextFileDataObject> robotsTextFiles = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(RobotsTextFileDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            robotsTextFiles = (List<RobotsTextFileDataObject>) q.execute();
            if(robotsTextFiles != null) {
                robotsTextFiles.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(robotsTextFiles);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            /*
            // ???
            Collection<RobotsTextFileDataObject> rs_robotsTextFiles = (Collection<RobotsTextFileDataObject>) q.execute();
            if(rs_robotsTextFiles == null) {
                log.log(Level.WARNING, "Failed to retrieve all robotsTextFiles.");
                robotsTextFiles = new ArrayList<RobotsTextFileDataObject>();  // ???           
            } else {
                robotsTextFiles = new ArrayList<RobotsTextFileDataObject>(pm.detachCopyAll(rs_robotsTextFiles));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all robotsTextFiles.", ex);
            //robotsTextFiles = new ArrayList<RobotsTextFileDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all robotsTextFiles.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return robotsTextFiles;
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextFileKeys(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + RobotsTextFileDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all RobotsTextFile keys.", ex);
            throw new DataStoreException("Failed to retrieve all RobotsTextFile keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findRobotsTextFiles(filter, ordering, params, values, null, null);
    }

    @Override
	public List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findRobotsTextFiles(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultRobotsTextFileDAO.findRobotsTextFiles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findRobotsTextFiles() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<RobotsTextFileDataObject> robotsTextFiles = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(RobotsTextFileDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                robotsTextFiles = (List<RobotsTextFileDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                robotsTextFiles = (List<RobotsTextFileDataObject>) q.execute();
            }
            if(robotsTextFiles != null) {
                robotsTextFiles.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(robotsTextFiles);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(RobotsTextFileDataObject dobj : robotsTextFiles) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find robotsTextFiles because index is missing.", ex);
            //robotsTextFiles = new ArrayList<RobotsTextFileDataObject>();  // ???
            throw new DataStoreException("Failed to find robotsTextFiles because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find robotsTextFiles meeting the criterion.", ex);
            //robotsTextFiles = new ArrayList<RobotsTextFileDataObject>();  // ???
            throw new DataStoreException("Failed to find robotsTextFiles meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return robotsTextFiles;
	}

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultRobotsTextFileDAO.findRobotsTextFileKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + RobotsTextFileDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find RobotsTextFile keys because index is missing.", ex);
            throw new DataStoreException("Failed to find RobotsTextFile keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find RobotsTextFile keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find RobotsTextFile keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultRobotsTextFileDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(RobotsTextFileDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get robotsTextFile count because index is missing.", ex);
            throw new DataStoreException("Failed to get robotsTextFile count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get robotsTextFile count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get robotsTextFile count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the robotsTextFile in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeRobotsTextFile(RobotsTextFileDataObject robotsTextFile) throws BaseException
    {
        log.fine("storeRobotsTextFile() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        try {
            // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
            // "Object is marked as dirty yet no fields are marked dirty".
            // tx.setOptimistic(true);
            // ....
            tx.begin();
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = robotsTextFile.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                robotsTextFile.setCreatedTime(createdTime);
            }
            Long modifiedTime = robotsTextFile.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                robotsTextFile.setModifiedTime(createdTime);
            }
            // ????
            // javax.jdo.JDOHelper.makeDirty(robotsTextFile, "guid");
            pm.makePersistent(robotsTextFile); 
            tx.commit();
            // TBD: How do you know the makePersistent() call was successful???
            guid = robotsTextFile.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store robotsTextFile because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store robotsTextFile because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store robotsTextFile.", ex);
            throw new DataStoreException("Failed to store robotsTextFile.", ex);
        } finally {
            try {
                if(tx.isActive()) {
                    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    tx.rollback();
                }
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeRobotsTextFile(): guid = " + guid);
        return guid;
    }

    @Override
    public String createRobotsTextFile(RobotsTextFileDataObject robotsTextFile) throws BaseException
    {
        // The createdTime field will be automatically set in storeRobotsTextFile().
        //Long createdTime = robotsTextFile.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    robotsTextFile.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = robotsTextFile.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    robotsTextFile.setModifiedTime(createdTime);
        //}
        return storeRobotsTextFile(robotsTextFile);
    }

    @Override
	public Boolean updateRobotsTextFile(RobotsTextFileDataObject robotsTextFile) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeRobotsTextFile()
	    // (in which case modifiedTime might be updated again).
	    robotsTextFile.setModifiedTime(System.currentTimeMillis());
	    String guid = storeRobotsTextFile(robotsTextFile);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteRobotsTextFile(RobotsTextFileDataObject robotsTextFile) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        try {
            // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
            // "Object is marked as dirty yet no fields are marked dirty".
            // tx.setOptimistic(true);
            // ....
            tx.begin();
            pm.deletePersistent(robotsTextFile);
            tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete robotsTextFile because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete robotsTextFile because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete robotsTextFile.", ex);
            throw new DataStoreException("Failed to delete robotsTextFile.", ex);
        } finally {
            try {
                if(tx.isActive()) {
                    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    tx.rollback();
                }
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteRobotsTextFile(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            Transaction tx = pm.currentTransaction();
            try {
                // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
                // "Object is marked as dirty yet no fields are marked dirty".
                // tx.setOptimistic(true);
                // ....
                tx.begin();
                //Key key = RobotsTextFileDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = RobotsTextFileDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                RobotsTextFileDataObject robotsTextFile = pm.getObjectById(RobotsTextFileDataObject.class, key);
                pm.deletePersistent(robotsTextFile);
                tx.commit();
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete robotsTextFile because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete robotsTextFile because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete robotsTextFile for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete robotsTextFile for guid = " + guid, ex);
            } finally {
                try {
                    if(tx.isActive()) {
                        log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                        tx.rollback();
                    }
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultRobotsTextFileDAO.deleteRobotsTextFiles(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        try {
            // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
            // "Object is marked as dirty yet no fields are marked dirty".
            // tx.setOptimistic(true);
            // ....
            tx.begin();
            Query q = pm.newQuery(RobotsTextFileDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleterobotsTextFiles because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete robotsTextFiles because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete robotsTextFiles because index is missing", ex);
            throw new DataStoreException("Failed to delete robotsTextFiles because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete robotsTextFiles", ex);
            throw new DataStoreException("Failed to delete robotsTextFiles", ex);
        } finally {
            try {
                if(tx.isActive()) {
                    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    tx.rollback();
                }
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
