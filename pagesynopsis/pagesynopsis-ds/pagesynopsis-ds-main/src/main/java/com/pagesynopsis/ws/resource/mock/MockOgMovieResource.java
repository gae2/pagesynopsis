package com.pagesynopsis.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ResourceAlreadyPresentException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.DataStoreRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.stub.OgMovieListStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.OgMovieResource;
import com.pagesynopsis.ws.resource.util.OgAudioStructResourceUtil;
import com.pagesynopsis.ws.resource.util.OgImageStructResourceUtil;
import com.pagesynopsis.ws.resource.util.OgActorStructResourceUtil;
import com.pagesynopsis.ws.resource.util.OgVideoStructResourceUtil;

// MockOgMovieResource is a decorator.
// It can be used as a base class to mock OgMovieResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/ogMovies/")
public abstract class MockOgMovieResource implements OgMovieResource
{
    private static final Logger log = Logger.getLogger(MockOgMovieResource.class.getName());

    // MockOgMovieResource uses the decorator design pattern.
    private OgMovieResource decoratedResource;

    public MockOgMovieResource(OgMovieResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected OgMovieResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(OgMovieResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgMovies(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgMovieKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getOgMovieKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getOgMovieKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getOgMovie(String guid) throws BaseResourceException
    {
        return decoratedResource.getOgMovie(guid);
    }

    @Override
    public Response getOgMovie(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getOgMovie(guid, field);
    }

    @Override
    public Response createOgMovie(OgMovieStub ogMovie) throws BaseResourceException
    {
        return decoratedResource.createOgMovie(ogMovie);
    }

    @Override
    public Response updateOgMovie(String guid, OgMovieStub ogMovie) throws BaseResourceException
    {
        return decoratedResource.updateOgMovie(guid, ogMovie);
    }

    @Override
    public Response updateOgMovie(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<String> image, List<String> audio, List<String> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<String> actor, Integer duration, List<String> tag, String releaseDate) throws BaseResourceException
    {
        return decoratedResource.updateOgMovie(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public Response deleteOgMovie(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteOgMovie(guid);
    }

    @Override
    public Response deleteOgMovies(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteOgMovies(filter, params, values);
    }


}
