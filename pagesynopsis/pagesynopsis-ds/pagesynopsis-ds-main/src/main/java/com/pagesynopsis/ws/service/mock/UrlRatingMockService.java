package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.ws.bean.UrlRatingBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.UrlRatingDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.UrlRatingService;


// UrlRatingMockService is a decorator.
// It can be used as a base class to mock UrlRatingService objects.
public abstract class UrlRatingMockService implements UrlRatingService
{
    private static final Logger log = Logger.getLogger(UrlRatingMockService.class.getName());

    // UrlRatingMockService uses the decorator design pattern.
    private UrlRatingService decoratedService;

    public UrlRatingMockService(UrlRatingService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected UrlRatingService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(UrlRatingService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // UrlRating related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UrlRating getUrlRating(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUrlRating(): guid = " + guid);
        UrlRating bean = decoratedService.getUrlRating(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getUrlRating(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getUrlRating(guid, field);
        return obj;
    }

    @Override
    public List<UrlRating> getUrlRatings(List<String> guids) throws BaseException
    {
        log.fine("getUrlRatings()");
        List<UrlRating> urlRatings = decoratedService.getUrlRatings(guids);
        log.finer("END");
        return urlRatings;
    }

    @Override
    public List<UrlRating> getAllUrlRatings() throws BaseException
    {
        return getAllUrlRatings(null, null, null);
    }


    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUrlRatings(ordering, offset, count, null);
    }

    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUrlRatings(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<UrlRating> urlRatings = decoratedService.getAllUrlRatings(ordering, offset, count, forwardCursor);
        log.finer("END");
        return urlRatings;
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUrlRatingKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUrlRatingKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllUrlRatingKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUrlRatings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UrlRatingMockService.findUrlRatings(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<UrlRating> urlRatings = decoratedService.findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return urlRatings;
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UrlRatingMockService.findUrlRatingKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UrlRatingMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createUrlRating(String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException
    {
        return decoratedService.createUrlRating(domain, pageUrl, preview, flag, rating, note, status, ratedTime);
    }

    @Override
    public String createUrlRating(UrlRating urlRating) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createUrlRating(urlRating);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUrlRating(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException
    {
        return decoratedService.updateUrlRating(guid, domain, pageUrl, preview, flag, rating, note, status, ratedTime);
    }
        
    @Override
    public Boolean updateUrlRating(UrlRating urlRating) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateUrlRating(urlRating);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteUrlRating(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteUrlRating(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteUrlRating(UrlRating urlRating) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteUrlRating(urlRating);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteUrlRatings(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteUrlRatings(filter, params, values);
        return count;
    }

}
