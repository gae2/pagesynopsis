package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "imageSets")
@XmlType(propOrder = {"imageSet", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageSetListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ImageSetListStub.class.getName());

    private List<ImageSetStub> imageSets = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public ImageSetListStub()
    {
        this(new ArrayList<ImageSetStub>());
    }
    public ImageSetListStub(List<ImageSetStub> imageSets)
    {
        this(imageSets, null);
    }
    public ImageSetListStub(List<ImageSetStub> imageSets, String forwardCursor)
    {
        this.imageSets = imageSets;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(imageSets == null) {
            return true;
        } else {
            return imageSets.isEmpty();
        }
    }
    public int getSize()
    {
        if(imageSets == null) {
            return 0;
        } else {
            return imageSets.size();
        }
    }


    @XmlElement(name = "imageSet")
    public List<ImageSetStub> getImageSet()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<ImageSetStub> getList()
    {
        return imageSets;
    }
    public void setList(List<ImageSetStub> imageSets)
    {
        this.imageSets = imageSets;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<ImageSetStub> it = this.imageSets.iterator();
        while(it.hasNext()) {
            ImageSetStub imageSet = it.next();
            sb.append(imageSet.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ImageSetListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ImageSetListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ImageSetListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ImageSetListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ImageSetListStub object as a string.", e);
        }
        
        return null;
    }
    public static ImageSetListStub fromJsonString(String jsonStr)
    {
        try {
            ImageSetListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ImageSetListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ImageSetListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ImageSetListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ImageSetListStub object.", e);
        }
        
        return null;
    }

}
