package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgContactInfoStruct;
import com.pagesynopsis.ws.bean.OgContactInfoStructBean;
import com.pagesynopsis.ws.stub.OgContactInfoStructStub;


public class OgContactInfoStructResourceUtil
{
    private static final Logger log = Logger.getLogger(OgContactInfoStructResourceUtil.class.getName());

    // Static methods only.
    private OgContactInfoStructResourceUtil() {}

    public static OgContactInfoStructBean convertOgContactInfoStructStubToBean(OgContactInfoStruct stub)
    {
        OgContactInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null OgContactInfoStructBean is returned.");
        } else {
            bean = new OgContactInfoStructBean();
            bean.setUuid(stub.getUuid());
            bean.setStreetAddress(stub.getStreetAddress());
            bean.setLocality(stub.getLocality());
            bean.setRegion(stub.getRegion());
            bean.setPostalCode(stub.getPostalCode());
            bean.setCountryName(stub.getCountryName());
            bean.setEmailAddress(stub.getEmailAddress());
            bean.setPhoneNumber(stub.getPhoneNumber());
            bean.setFaxNumber(stub.getFaxNumber());
            bean.setWebsite(stub.getWebsite());
        }
        return bean;
    }

}
