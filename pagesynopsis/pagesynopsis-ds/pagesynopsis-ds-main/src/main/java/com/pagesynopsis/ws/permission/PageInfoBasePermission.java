package com.pagesynopsis.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class PageInfoBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PageInfoBasePermission.class.getName());

    public PageInfoBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "PageInfo::" + action;
    }

    @Override
    public boolean isCreatePermissionRequired()
    {
        return true;
    }

    @Override
    public boolean isReadPermissionRequired()
    {
        return true;
    }

    @Override
    public boolean isUpdatePermissionRequired()
    {
        return true;
    }

    @Override
    public boolean isDeletePermissionRequired()
    {
        return true;
    }

}
