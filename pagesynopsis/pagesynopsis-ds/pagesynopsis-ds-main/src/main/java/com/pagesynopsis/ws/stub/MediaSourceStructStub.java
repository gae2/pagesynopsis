package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "mediaSourceStruct")
@XmlType(propOrder = {"uuid", "src", "srcUrl", "type", "remark"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MediaSourceStructStub implements MediaSourceStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(MediaSourceStructStub.class.getName());

    private String uuid;
    private String src;
    private String srcUrl;
    private String type;
    private String remark;

    public MediaSourceStructStub()
    {
        this(null);
    }
    public MediaSourceStructStub(MediaSourceStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.src = bean.getSrc();
            this.srcUrl = bean.getSrcUrl();
            this.type = bean.getType();
            this.remark = bean.getRemark();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getSrc()
    {
        return this.src;
    }
    public void setSrc(String src)
    {
        this.src = src;
    }

    @XmlElement
    public String getSrcUrl()
    {
        return this.srcUrl;
    }
    public void setSrcUrl(String srcUrl)
    {
        this.srcUrl = srcUrl;
    }

    @XmlElement
    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    @XmlElement
    public String getRemark()
    {
        return this.remark;
    }
    public void setRemark(String remark)
    {
        this.remark = remark;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getSrc() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrcUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRemark() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("src", this.src);
        dataMap.put("srcUrl", this.srcUrl);
        dataMap.put("type", this.type);
        dataMap.put("remark", this.remark);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = src == null ? 0 : src.hashCode();
        _hash = 31 * _hash + delta;
        delta = srcUrl == null ? 0 : srcUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = remark == null ? 0 : remark.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static MediaSourceStructStub convertBeanToStub(MediaSourceStruct bean)
    {
        MediaSourceStructStub stub = null;
        if(bean instanceof MediaSourceStructStub) {
            stub = (MediaSourceStructStub) bean;
        } else {
            if(bean != null) {
                stub = new MediaSourceStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static MediaSourceStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of MediaSourceStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write MediaSourceStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write MediaSourceStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write MediaSourceStructStub object as a string.", e);
        }
        
        return null;
    }
    public static MediaSourceStructStub fromJsonString(String jsonStr)
    {
        try {
            MediaSourceStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, MediaSourceStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into MediaSourceStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into MediaSourceStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into MediaSourceStructStub object.", e);
        }
        
        return null;
    }

}
