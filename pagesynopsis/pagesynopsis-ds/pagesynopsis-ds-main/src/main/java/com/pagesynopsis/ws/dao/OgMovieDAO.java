package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.OgMovieDataObject;


// TBD: Add offset/count to getAllOgMovies() and findOgMovies(), etc.
public interface OgMovieDAO
{
    OgMovieDataObject getOgMovie(String guid) throws BaseException;
    List<OgMovieDataObject> getOgMovies(List<String> guids) throws BaseException;
    List<OgMovieDataObject> getAllOgMovies() throws BaseException;
    /* @Deprecated */ List<OgMovieDataObject> getAllOgMovies(String ordering, Long offset, Integer count) throws BaseException;
    List<OgMovieDataObject> getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgMovieDataObject> findOgMovies(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgMovieDataObject> findOgMovies(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<OgMovieDataObject> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgMovieDataObject> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createOgMovie(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgMovieDataObject?)
    String createOgMovie(OgMovieDataObject ogMovie) throws BaseException;          // Returns Guid.  (Return OgMovieDataObject?)
    //Boolean updateOgMovie(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgMovie(OgMovieDataObject ogMovie) throws BaseException;
    Boolean deleteOgMovie(String guid) throws BaseException;
    Boolean deleteOgMovie(OgMovieDataObject ogMovie) throws BaseException;
    Long deleteOgMovies(String filter, String params, List<String> values) throws BaseException;
}
