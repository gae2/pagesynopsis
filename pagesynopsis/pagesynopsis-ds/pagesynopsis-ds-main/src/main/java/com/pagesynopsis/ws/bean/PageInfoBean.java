package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.PageInfoDataObject;

public class PageInfoBean extends PageBaseBean implements PageInfo
{
    private static final Logger log = Logger.getLogger(PageInfoBean.class.getName());

    // Embedded data object.
    private PageInfoDataObject dobj = null;

    public PageInfoBean()
    {
        this(new PageInfoDataObject());
    }
    public PageInfoBean(String guid)
    {
        this(new PageInfoDataObject(guid));
    }
    public PageInfoBean(PageInfoDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public PageInfoDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getPageAuthor()
    {
        if(getDataObject() != null) {
            return getDataObject().getPageAuthor();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setPageAuthor(String pageAuthor)
    {
        if(getDataObject() != null) {
            getDataObject().setPageAuthor(pageAuthor);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageInfoDataObject is null!");
        }
    }

    public String getPageDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getPageDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setPageDescription(String pageDescription)
    {
        if(getDataObject() != null) {
            getDataObject().setPageDescription(pageDescription);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageInfoDataObject is null!");
        }
    }

    public String getFavicon()
    {
        if(getDataObject() != null) {
            return getDataObject().getFavicon();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setFavicon(String favicon)
    {
        if(getDataObject() != null) {
            getDataObject().setFavicon(favicon);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageInfoDataObject is null!");
        }
    }

    public String getFaviconUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getFaviconUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageInfoDataObject is null!");
            return null;   // ???
        }
    }
    public void setFaviconUrl(String faviconUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setFaviconUrl(faviconUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageInfoDataObject is null!");
        }
    }


    // TBD
    public PageInfoDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
