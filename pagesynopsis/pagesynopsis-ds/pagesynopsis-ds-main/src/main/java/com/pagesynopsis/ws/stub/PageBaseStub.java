package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageBase;
import com.pagesynopsis.ws.util.JsonUtil;


// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class PageBaseStub implements PageBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PageBaseStub.class.getName());

    private String guid;
    private String user;
    private String fetchRequest;
    private String targetUrl;
    private String pageUrl;
    private String queryString;
    private List<KeyValuePairStructStub> queryParams;
    private String lastFetchResult;
    private Integer responseCode;
    private String contentType;
    private Integer contentLength;
    private String language;
    private String redirect;
    private String location;
    private String pageTitle;
    private String note;
    private Boolean deferred;
    private String status;
    private Integer refreshStatus;
    private Long refreshInterval;
    private Long nextRefreshTime;
    private Long lastCheckedTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    public PageBaseStub()
    {
        this(null);
    }
    public PageBaseStub(PageBase bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.user = bean.getUser();
            this.fetchRequest = bean.getFetchRequest();
            this.targetUrl = bean.getTargetUrl();
            this.pageUrl = bean.getPageUrl();
            this.queryString = bean.getQueryString();
            List<KeyValuePairStructStub> _queryParamsStubs = null;
            if(bean.getQueryParams() != null) {
                _queryParamsStubs = new ArrayList<KeyValuePairStructStub>();
                for(KeyValuePairStruct b : bean.getQueryParams()) {
                    _queryParamsStubs.add(KeyValuePairStructStub.convertBeanToStub(b));
                }
            }
            this.queryParams = _queryParamsStubs;
            this.lastFetchResult = bean.getLastFetchResult();
            this.responseCode = bean.getResponseCode();
            this.contentType = bean.getContentType();
            this.contentLength = bean.getContentLength();
            this.language = bean.getLanguage();
            this.redirect = bean.getRedirect();
            this.location = bean.getLocation();
            this.pageTitle = bean.getPageTitle();
            this.note = bean.getNote();
            this.deferred = bean.isDeferred();
            this.status = bean.getStatus();
            this.refreshStatus = bean.getRefreshStatus();
            this.refreshInterval = bean.getRefreshInterval();
            this.nextRefreshTime = bean.getNextRefreshTime();
            this.lastCheckedTime = bean.getLastCheckedTime();
            this.lastUpdatedTime = bean.getLastUpdatedTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlTransient
    //@JsonIgnore
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlTransient
    //@JsonIgnore
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlTransient
    //@JsonIgnore
    public String getFetchRequest()
    {
        return this.fetchRequest;
    }
    public void setFetchRequest(String fetchRequest)
    {
        this.fetchRequest = fetchRequest;
    }

    @XmlTransient
    //@JsonIgnore
    public String getTargetUrl()
    {
        return this.targetUrl;
    }
    public void setTargetUrl(String targetUrl)
    {
        this.targetUrl = targetUrl;
    }

    @XmlTransient
    //@JsonIgnore
    public String getPageUrl()
    {
        return this.pageUrl;
    }
    public void setPageUrl(String pageUrl)
    {
        this.pageUrl = pageUrl;
    }

    @XmlTransient
    //@JsonIgnore
    public String getQueryString()
    {
        return this.queryString;
    }
    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    @XmlTransient
    //@JsonIgnore
    public List<KeyValuePairStructStub> getQueryParamsStub()
    {
        return this.queryParams;
    }
    public void setQueryParamsStub(List<KeyValuePairStructStub> queryParams)
    {
        this.queryParams = queryParams;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=KeyValuePairStructStub.class)
    public List<KeyValuePairStruct> getQueryParams()
    {  
        return (List<KeyValuePairStruct>) ((List<?>) getQueryParamsStub());
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        // TBD
        //if(queryParams instanceof List<KeyValuePairStructStub>) {
            setQueryParamsStub((List<KeyValuePairStructStub>) ((List<?>) queryParams));
        //} else {
        //    // TBD
        //    List<KeyValuePairStructStub> _KeyValuePairStructList = new ArrayList<KeyValuePairStructStub>(); // ???
        //    for(KeyValuePairStruct b : queryParams) {
        //        _KeyValuePairStructList.add(KeyValuePairStructStub.convertBeanToStub(queryParams));
        //    }
        //    setQueryParamsStub(_KeyValuePairStructList);
        //}
    }

    @XmlTransient
    //@JsonIgnore
    public String getLastFetchResult()
    {
        return this.lastFetchResult;
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        this.lastFetchResult = lastFetchResult;
    }

    @XmlTransient
    //@JsonIgnore
    public Integer getResponseCode()
    {
        return this.responseCode;
    }
    public void setResponseCode(Integer responseCode)
    {
        this.responseCode = responseCode;
    }

    @XmlTransient
    //@JsonIgnore
    public String getContentType()
    {
        return this.contentType;
    }
    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    @XmlTransient
    //@JsonIgnore
    public Integer getContentLength()
    {
        return this.contentLength;
    }
    public void setContentLength(Integer contentLength)
    {
        this.contentLength = contentLength;
    }

    @XmlTransient
    //@JsonIgnore
    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    @XmlTransient
    //@JsonIgnore
    public String getRedirect()
    {
        return this.redirect;
    }
    public void setRedirect(String redirect)
    {
        this.redirect = redirect;
    }

    @XmlTransient
    //@JsonIgnore
    public String getLocation()
    {
        return this.location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    @XmlTransient
    //@JsonIgnore
    public String getPageTitle()
    {
        return this.pageTitle;
    }
    public void setPageTitle(String pageTitle)
    {
        this.pageTitle = pageTitle;
    }

    @XmlTransient
    //@JsonIgnore
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlTransient
    //@JsonIgnore
    public Boolean isDeferred()
    {
        return this.deferred;
    }
    public void setDeferred(Boolean deferred)
    {
        this.deferred = deferred;
    }

    @XmlTransient
    //@JsonIgnore
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlTransient
    //@JsonIgnore
    public Integer getRefreshStatus()
    {
        return this.refreshStatus;
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        this.refreshStatus = refreshStatus;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getRefreshInterval()
    {
        return this.refreshInterval;
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        this.refreshInterval = refreshInterval;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getNextRefreshTime()
    {
        return this.nextRefreshTime;
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        this.nextRefreshTime = nextRefreshTime;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getLastCheckedTime()
    {
        return this.lastCheckedTime;
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        this.lastCheckedTime = lastCheckedTime;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("fetchRequest", this.fetchRequest);
        dataMap.put("targetUrl", this.targetUrl);
        dataMap.put("pageUrl", this.pageUrl);
        dataMap.put("queryString", this.queryString);
        dataMap.put("queryParams", this.queryParams);
        dataMap.put("lastFetchResult", this.lastFetchResult);
        dataMap.put("responseCode", this.responseCode);
        dataMap.put("contentType", this.contentType);
        dataMap.put("contentLength", this.contentLength);
        dataMap.put("language", this.language);
        dataMap.put("redirect", this.redirect);
        dataMap.put("location", this.location);
        dataMap.put("pageTitle", this.pageTitle);
        dataMap.put("note", this.note);
        dataMap.put("deferred", this.deferred);
        dataMap.put("status", this.status);
        dataMap.put("refreshStatus", this.refreshStatus);
        dataMap.put("refreshInterval", this.refreshInterval);
        dataMap.put("nextRefreshTime", this.nextRefreshTime);
        dataMap.put("lastCheckedTime", this.lastCheckedTime);
        dataMap.put("lastUpdatedTime", this.lastUpdatedTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchRequest == null ? 0 : fetchRequest.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetUrl == null ? 0 : targetUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageUrl == null ? 0 : pageUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryString == null ? 0 : queryString.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryParams == null ? 0 : queryParams.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastFetchResult == null ? 0 : lastFetchResult.hashCode();
        _hash = 31 * _hash + delta;
        delta = responseCode == null ? 0 : responseCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = contentType == null ? 0 : contentType.hashCode();
        _hash = 31 * _hash + delta;
        delta = contentLength == null ? 0 : contentLength.hashCode();
        _hash = 31 * _hash + delta;
        delta = language == null ? 0 : language.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirect == null ? 0 : redirect.hashCode();
        _hash = 31 * _hash + delta;
        delta = location == null ? 0 : location.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageTitle == null ? 0 : pageTitle.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = deferred == null ? 0 : deferred.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshStatus == null ? 0 : refreshStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshInterval == null ? 0 : refreshInterval.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextRefreshTime == null ? 0 : nextRefreshTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastCheckedTime == null ? 0 : lastCheckedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static PageBaseStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of PageBaseStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write PageBaseStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write PageBaseStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write PageBaseStub object as a string.", e);
        }
        
        return null;
    }
    public static PageBaseStub fromJsonString(String jsonStr)
    {
        try {
            PageBaseStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, PageBaseStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into PageBaseStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into PageBaseStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into PageBaseStub object.", e);
        }
        
        return null;
    }

}
