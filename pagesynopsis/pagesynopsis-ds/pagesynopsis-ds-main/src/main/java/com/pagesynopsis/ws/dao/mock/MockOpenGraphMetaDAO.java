package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OpenGraphMetaDAO;
import com.pagesynopsis.ws.data.OpenGraphMetaDataObject;


// MockOpenGraphMetaDAO is a decorator.
// It can be used as a base class to mock OpenGraphMetaDAO objects.
public abstract class MockOpenGraphMetaDAO implements OpenGraphMetaDAO
{
    private static final Logger log = Logger.getLogger(MockOpenGraphMetaDAO.class.getName()); 

    // MockOpenGraphMetaDAO uses the decorator design pattern.
    private OpenGraphMetaDAO decoratedDAO;

    public MockOpenGraphMetaDAO(OpenGraphMetaDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected OpenGraphMetaDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(OpenGraphMetaDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public OpenGraphMetaDataObject getOpenGraphMeta(String guid) throws BaseException
    {
        return decoratedDAO.getOpenGraphMeta(guid);
	}

    @Override
    public List<OpenGraphMetaDataObject> getOpenGraphMetas(List<String> guids) throws BaseException
    {
        return decoratedDAO.getOpenGraphMetas(guids);
    }

    @Override
    public List<OpenGraphMetaDataObject> getAllOpenGraphMetas() throws BaseException
	{
	    return getAllOpenGraphMetas(null, null, null);
    }


    @Override
    public List<OpenGraphMetaDataObject> getAllOpenGraphMetas(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOpenGraphMetas(ordering, offset, count, null);
    }

    @Override
    public List<OpenGraphMetaDataObject> getAllOpenGraphMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllOpenGraphMetas(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOpenGraphMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllOpenGraphMetaKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<OpenGraphMetaDataObject> findOpenGraphMetas(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOpenGraphMetas(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OpenGraphMetaDataObject> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOpenGraphMetas(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OpenGraphMetaDataObject> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<OpenGraphMetaDataObject> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOpenGraphMeta(OpenGraphMetaDataObject openGraphMeta) throws BaseException
    {
        return decoratedDAO.createOpenGraphMeta( openGraphMeta);
    }

    @Override
	public Boolean updateOpenGraphMeta(OpenGraphMetaDataObject openGraphMeta) throws BaseException
	{
        return decoratedDAO.updateOpenGraphMeta(openGraphMeta);
	}
	
    @Override
    public Boolean deleteOpenGraphMeta(OpenGraphMetaDataObject openGraphMeta) throws BaseException
    {
        return decoratedDAO.deleteOpenGraphMeta(openGraphMeta);
    }

    @Override
    public Boolean deleteOpenGraphMeta(String guid) throws BaseException
    {
        return decoratedDAO.deleteOpenGraphMeta(guid);
	}

    @Override
    public Long deleteOpenGraphMetas(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteOpenGraphMetas(filter, params, values);
    }

}
