package com.pagesynopsis.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ResourceAlreadyPresentException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.DataStoreRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.stub.TwitterSummaryCardListStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.TwitterSummaryCardResource;
import com.pagesynopsis.ws.resource.util.TwitterCardAppInfoResourceUtil;
import com.pagesynopsis.ws.resource.util.TwitterCardProductDataResourceUtil;

// MockTwitterSummaryCardResource is a decorator.
// It can be used as a base class to mock TwitterSummaryCardResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/twitterSummaryCards/")
public abstract class MockTwitterSummaryCardResource implements TwitterSummaryCardResource
{
    private static final Logger log = Logger.getLogger(MockTwitterSummaryCardResource.class.getName());

    // MockTwitterSummaryCardResource uses the decorator design pattern.
    private TwitterSummaryCardResource decoratedResource;

    public MockTwitterSummaryCardResource(TwitterSummaryCardResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected TwitterSummaryCardResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(TwitterSummaryCardResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllTwitterSummaryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterSummaryCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterSummaryCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getTwitterSummaryCardKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getTwitterSummaryCardKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getTwitterSummaryCard(String guid) throws BaseResourceException
    {
        return decoratedResource.getTwitterSummaryCard(guid);
    }

    @Override
    public Response getTwitterSummaryCard(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getTwitterSummaryCard(guid, field);
    }

    @Override
    public Response createTwitterSummaryCard(TwitterSummaryCardStub twitterSummaryCard) throws BaseResourceException
    {
        return decoratedResource.createTwitterSummaryCard(twitterSummaryCard);
    }

    @Override
    public Response updateTwitterSummaryCard(String guid, TwitterSummaryCardStub twitterSummaryCard) throws BaseResourceException
    {
        return decoratedResource.updateTwitterSummaryCard(guid, twitterSummaryCard);
    }

    @Override
    public Response updateTwitterSummaryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseResourceException
    {
        return decoratedResource.updateTwitterSummaryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public Response deleteTwitterSummaryCard(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterSummaryCard(guid);
    }

    @Override
    public Response deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterSummaryCards(filter, params, values);
    }


}
