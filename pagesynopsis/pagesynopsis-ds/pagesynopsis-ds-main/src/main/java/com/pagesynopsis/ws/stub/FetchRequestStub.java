package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "fetchRequest")
@XmlType(propOrder = {"guid", "user", "targetUrl", "pageUrl", "queryString", "queryParamsStub", "version", "fetchObject", "fetchStatus", "result", "note", "referrerInfoStub", "status", "nextPageUrl", "followPages", "followDepth", "createVersion", "deferred", "alert", "notificationPrefStub", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FetchRequestStub implements FetchRequest, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FetchRequestStub.class.getName());

    private String guid;
    private String user;
    private String targetUrl;
    private String pageUrl;
    private String queryString;
    private List<KeyValuePairStructStub> queryParams;
    private String version;
    private String fetchObject;
    private Integer fetchStatus;
    private String result;
    private String note;
    private ReferrerInfoStructStub referrerInfo;
    private String status;
    private String nextPageUrl;
    private Integer followPages;
    private Integer followDepth;
    private Boolean createVersion;
    private Boolean deferred;
    private Boolean alert;
    private NotificationStructStub notificationPref;
    private Long createdTime;
    private Long modifiedTime;

    public FetchRequestStub()
    {
        this(null);
    }
    public FetchRequestStub(FetchRequest bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.user = bean.getUser();
            this.targetUrl = bean.getTargetUrl();
            this.pageUrl = bean.getPageUrl();
            this.queryString = bean.getQueryString();
            List<KeyValuePairStructStub> _queryParamsStubs = null;
            if(bean.getQueryParams() != null) {
                _queryParamsStubs = new ArrayList<KeyValuePairStructStub>();
                for(KeyValuePairStruct b : bean.getQueryParams()) {
                    _queryParamsStubs.add(KeyValuePairStructStub.convertBeanToStub(b));
                }
            }
            this.queryParams = _queryParamsStubs;
            this.version = bean.getVersion();
            this.fetchObject = bean.getFetchObject();
            this.fetchStatus = bean.getFetchStatus();
            this.result = bean.getResult();
            this.note = bean.getNote();
            this.referrerInfo = ReferrerInfoStructStub.convertBeanToStub(bean.getReferrerInfo());
            this.status = bean.getStatus();
            this.nextPageUrl = bean.getNextPageUrl();
            this.followPages = bean.getFollowPages();
            this.followDepth = bean.getFollowDepth();
            this.createVersion = bean.isCreateVersion();
            this.deferred = bean.isDeferred();
            this.alert = bean.isAlert();
            this.notificationPref = NotificationStructStub.convertBeanToStub(bean.getNotificationPref());
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getTargetUrl()
    {
        return this.targetUrl;
    }
    public void setTargetUrl(String targetUrl)
    {
        this.targetUrl = targetUrl;
    }

    @XmlElement
    public String getPageUrl()
    {
        return this.pageUrl;
    }
    public void setPageUrl(String pageUrl)
    {
        this.pageUrl = pageUrl;
    }

    @XmlElement
    public String getQueryString()
    {
        return this.queryString;
    }
    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    @XmlElement(name = "queryParams")
    @JsonIgnore
    public List<KeyValuePairStructStub> getQueryParamsStub()
    {
        return this.queryParams;
    }
    public void setQueryParamsStub(List<KeyValuePairStructStub> queryParams)
    {
        this.queryParams = queryParams;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=KeyValuePairStructStub.class)
    public List<KeyValuePairStruct> getQueryParams()
    {  
        return (List<KeyValuePairStruct>) ((List<?>) getQueryParamsStub());
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        // TBD
        //if(queryParams instanceof List<KeyValuePairStructStub>) {
            setQueryParamsStub((List<KeyValuePairStructStub>) ((List<?>) queryParams));
        //} else {
        //    // TBD
        //    List<KeyValuePairStructStub> _KeyValuePairStructList = new ArrayList<KeyValuePairStructStub>(); // ???
        //    for(KeyValuePairStruct b : queryParams) {
        //        _KeyValuePairStructList.add(KeyValuePairStructStub.convertBeanToStub(queryParams));
        //    }
        //    setQueryParamsStub(_KeyValuePairStructList);
        //}
    }

    @XmlElement
    public String getVersion()
    {
        return this.version;
    }
    public void setVersion(String version)
    {
        this.version = version;
    }

    @XmlElement
    public String getFetchObject()
    {
        return this.fetchObject;
    }
    public void setFetchObject(String fetchObject)
    {
        this.fetchObject = fetchObject;
    }

    @XmlElement
    public Integer getFetchStatus()
    {
        return this.fetchStatus;
    }
    public void setFetchStatus(Integer fetchStatus)
    {
        this.fetchStatus = fetchStatus;
    }

    @XmlElement
    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement(name = "referrerInfo")
    @JsonIgnore
    public ReferrerInfoStructStub getReferrerInfoStub()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfoStub(ReferrerInfoStructStub referrerInfo)
    {
        this.referrerInfo = referrerInfo;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=ReferrerInfoStructStub.class)
    public ReferrerInfoStruct getReferrerInfo()
    {  
        return getReferrerInfoStub();
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if((referrerInfo == null) || (referrerInfo instanceof ReferrerInfoStructStub)) {
            setReferrerInfoStub((ReferrerInfoStructStub) referrerInfo);
        } else {
            // TBD
            setReferrerInfoStub(ReferrerInfoStructStub.convertBeanToStub(referrerInfo));
        }
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public String getNextPageUrl()
    {
        return this.nextPageUrl;
    }
    public void setNextPageUrl(String nextPageUrl)
    {
        this.nextPageUrl = nextPageUrl;
    }

    @XmlElement
    public Integer getFollowPages()
    {
        return this.followPages;
    }
    public void setFollowPages(Integer followPages)
    {
        this.followPages = followPages;
    }

    @XmlElement
    public Integer getFollowDepth()
    {
        return this.followDepth;
    }
    public void setFollowDepth(Integer followDepth)
    {
        this.followDepth = followDepth;
    }

    @XmlElement
    public Boolean isCreateVersion()
    {
        return this.createVersion;
    }
    public void setCreateVersion(Boolean createVersion)
    {
        this.createVersion = createVersion;
    }

    @XmlElement
    public Boolean isDeferred()
    {
        return this.deferred;
    }
    public void setDeferred(Boolean deferred)
    {
        this.deferred = deferred;
    }

    @XmlElement
    public Boolean isAlert()
    {
        return this.alert;
    }
    public void setAlert(Boolean alert)
    {
        this.alert = alert;
    }

    @XmlElement(name = "notificationPref")
    @JsonIgnore
    public NotificationStructStub getNotificationPrefStub()
    {
        return this.notificationPref;
    }
    public void setNotificationPrefStub(NotificationStructStub notificationPref)
    {
        this.notificationPref = notificationPref;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=NotificationStructStub.class)
    public NotificationStruct getNotificationPref()
    {  
        return getNotificationPrefStub();
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if((notificationPref == null) || (notificationPref instanceof NotificationStructStub)) {
            setNotificationPrefStub((NotificationStructStub) notificationPref);
        } else {
            // TBD
            setNotificationPrefStub(NotificationStructStub.convertBeanToStub(notificationPref));
        }
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("targetUrl", this.targetUrl);
        dataMap.put("pageUrl", this.pageUrl);
        dataMap.put("queryString", this.queryString);
        dataMap.put("queryParams", this.queryParams);
        dataMap.put("version", this.version);
        dataMap.put("fetchObject", this.fetchObject);
        dataMap.put("fetchStatus", this.fetchStatus);
        dataMap.put("result", this.result);
        dataMap.put("note", this.note);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("status", this.status);
        dataMap.put("nextPageUrl", this.nextPageUrl);
        dataMap.put("followPages", this.followPages);
        dataMap.put("followDepth", this.followDepth);
        dataMap.put("createVersion", this.createVersion);
        dataMap.put("deferred", this.deferred);
        dataMap.put("alert", this.alert);
        dataMap.put("notificationPref", this.notificationPref);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetUrl == null ? 0 : targetUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageUrl == null ? 0 : pageUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryString == null ? 0 : queryString.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryParams == null ? 0 : queryParams.hashCode();
        _hash = 31 * _hash + delta;
        delta = version == null ? 0 : version.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchObject == null ? 0 : fetchObject.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchStatus == null ? 0 : fetchStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = result == null ? 0 : result.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextPageUrl == null ? 0 : nextPageUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = followPages == null ? 0 : followPages.hashCode();
        _hash = 31 * _hash + delta;
        delta = followDepth == null ? 0 : followDepth.hashCode();
        _hash = 31 * _hash + delta;
        delta = createVersion == null ? 0 : createVersion.hashCode();
        _hash = 31 * _hash + delta;
        delta = deferred == null ? 0 : deferred.hashCode();
        _hash = 31 * _hash + delta;
        delta = alert == null ? 0 : alert.hashCode();
        _hash = 31 * _hash + delta;
        delta = notificationPref == null ? 0 : notificationPref.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static FetchRequestStub convertBeanToStub(FetchRequest bean)
    {
        FetchRequestStub stub = null;
        if(bean instanceof FetchRequestStub) {
            stub = (FetchRequestStub) bean;
        } else {
            if(bean != null) {
                stub = new FetchRequestStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FetchRequestStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of FetchRequestStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FetchRequestStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FetchRequestStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FetchRequestStub object as a string.", e);
        }
        
        return null;
    }
    public static FetchRequestStub fromJsonString(String jsonStr)
    {
        try {
            FetchRequestStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FetchRequestStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FetchRequestStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FetchRequestStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FetchRequestStub object.", e);
        }
        
        return null;
    }

}
