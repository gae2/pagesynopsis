package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.ws.bean.TwitterCardProductDataBean;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.TwitterCardAppInfoDataObject;
import com.pagesynopsis.ws.data.TwitterCardProductDataDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;


// TwitterSummaryCardMockService is a decorator.
// It can be used as a base class to mock TwitterSummaryCardService objects.
public abstract class TwitterSummaryCardMockService implements TwitterSummaryCardService
{
    private static final Logger log = Logger.getLogger(TwitterSummaryCardMockService.class.getName());

    // TwitterSummaryCardMockService uses the decorator design pattern.
    private TwitterSummaryCardService decoratedService;

    public TwitterSummaryCardMockService(TwitterSummaryCardService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected TwitterSummaryCardService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(TwitterSummaryCardService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // TwitterSummaryCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterSummaryCard getTwitterSummaryCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterSummaryCard(): guid = " + guid);
        TwitterSummaryCard bean = decoratedService.getTwitterSummaryCard(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterSummaryCard(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getTwitterSummaryCard(guid, field);
        return obj;
    }

    @Override
    public List<TwitterSummaryCard> getTwitterSummaryCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterSummaryCards()");
        List<TwitterSummaryCard> twitterSummaryCards = decoratedService.getTwitterSummaryCards(guids);
        log.finer("END");
        return twitterSummaryCards;
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards() throws BaseException
    {
        return getAllTwitterSummaryCards(null, null, null);
    }


    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterSummaryCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterSummaryCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<TwitterSummaryCard> twitterSummaryCards = decoratedService.getAllTwitterSummaryCards(ordering, offset, count, forwardCursor);
        log.finer("END");
        return twitterSummaryCards;
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterSummaryCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterSummaryCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllTwitterSummaryCardKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterSummaryCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterSummaryCardMockService.findTwitterSummaryCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<TwitterSummaryCard> twitterSummaryCards = decoratedService.findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return twitterSummaryCards;
    }

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterSummaryCardMockService.findTwitterSummaryCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterSummaryCardMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterSummaryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return decoratedService.createTwitterSummaryCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public String createTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createTwitterSummaryCard(twitterSummaryCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTwitterSummaryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return decoratedService.updateTwitterSummaryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }
        
    @Override
    public Boolean updateTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateTwitterSummaryCard(twitterSummaryCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteTwitterSummaryCard(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterSummaryCard(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterSummaryCard(twitterSummaryCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteTwitterSummaryCards(filter, params, values);
        return count;
    }

}
