package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.TwitterPhotoCardDAO;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;


// MockTwitterPhotoCardDAO is a decorator.
// It can be used as a base class to mock TwitterPhotoCardDAO objects.
public abstract class MockTwitterPhotoCardDAO implements TwitterPhotoCardDAO
{
    private static final Logger log = Logger.getLogger(MockTwitterPhotoCardDAO.class.getName()); 

    // MockTwitterPhotoCardDAO uses the decorator design pattern.
    private TwitterPhotoCardDAO decoratedDAO;

    public MockTwitterPhotoCardDAO(TwitterPhotoCardDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected TwitterPhotoCardDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(TwitterPhotoCardDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public TwitterPhotoCardDataObject getTwitterPhotoCard(String guid) throws BaseException
    {
        return decoratedDAO.getTwitterPhotoCard(guid);
	}

    @Override
    public List<TwitterPhotoCardDataObject> getTwitterPhotoCards(List<String> guids) throws BaseException
    {
        return decoratedDAO.getTwitterPhotoCards(guids);
    }

    @Override
    public List<TwitterPhotoCardDataObject> getAllTwitterPhotoCards() throws BaseException
	{
	    return getAllTwitterPhotoCards(null, null, null);
    }


    @Override
    public List<TwitterPhotoCardDataObject> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllTwitterPhotoCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPhotoCardDataObject> getAllTwitterPhotoCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllTwitterPhotoCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPhotoCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllTwitterPhotoCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<TwitterPhotoCardDataObject> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTwitterPhotoCards(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TwitterPhotoCardDataObject> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTwitterPhotoCards(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<TwitterPhotoCardDataObject> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<TwitterPhotoCardDataObject> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterPhotoCard(TwitterPhotoCardDataObject twitterPhotoCard) throws BaseException
    {
        return decoratedDAO.createTwitterPhotoCard( twitterPhotoCard);
    }

    @Override
	public Boolean updateTwitterPhotoCard(TwitterPhotoCardDataObject twitterPhotoCard) throws BaseException
	{
        return decoratedDAO.updateTwitterPhotoCard(twitterPhotoCard);
	}
	
    @Override
    public Boolean deleteTwitterPhotoCard(TwitterPhotoCardDataObject twitterPhotoCard) throws BaseException
    {
        return decoratedDAO.deleteTwitterPhotoCard(twitterPhotoCard);
    }

    @Override
    public Boolean deleteTwitterPhotoCard(String guid) throws BaseException
    {
        return decoratedDAO.deleteTwitterPhotoCard(guid);
	}

    @Override
    public Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteTwitterPhotoCards(filter, params, values);
    }

}
