package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgTvEpisodeService;


// OgTvEpisodeMockService is a decorator.
// It can be used as a base class to mock OgTvEpisodeService objects.
public abstract class OgTvEpisodeMockService implements OgTvEpisodeService
{
    private static final Logger log = Logger.getLogger(OgTvEpisodeMockService.class.getName());

    // OgTvEpisodeMockService uses the decorator design pattern.
    private OgTvEpisodeService decoratedService;

    public OgTvEpisodeMockService(OgTvEpisodeService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected OgTvEpisodeService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(OgTvEpisodeService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // OgTvEpisode related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgTvEpisode getOgTvEpisode(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgTvEpisode(): guid = " + guid);
        OgTvEpisode bean = decoratedService.getOgTvEpisode(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgTvEpisode(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getOgTvEpisode(guid, field);
        return obj;
    }

    @Override
    public List<OgTvEpisode> getOgTvEpisodes(List<String> guids) throws BaseException
    {
        log.fine("getOgTvEpisodes()");
        List<OgTvEpisode> ogTvEpisodes = decoratedService.getOgTvEpisodes(guids);
        log.finer("END");
        return ogTvEpisodes;
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes() throws BaseException
    {
        return getAllOgTvEpisodes(null, null, null);
    }


    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvEpisodes(ordering, offset, count, null);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgTvEpisodes(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<OgTvEpisode> ogTvEpisodes = decoratedService.getAllOgTvEpisodes(ordering, offset, count, forwardCursor);
        log.finer("END");
        return ogTvEpisodes;
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvEpisodeKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgTvEpisodeKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllOgTvEpisodeKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgTvEpisodes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvEpisodeMockService.findOgTvEpisodes(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<OgTvEpisode> ogTvEpisodes = decoratedService.findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return ogTvEpisodes;
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvEpisodeMockService.findOgTvEpisodeKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvEpisodeMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgTvEpisode(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedService.createOgTvEpisode(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
    }

    @Override
    public String createOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createOgTvEpisode(ogTvEpisode);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgTvEpisode(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedService.updateOgTvEpisode(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
    }
        
    @Override
    public Boolean updateOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateOgTvEpisode(ogTvEpisode);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteOgTvEpisode(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgTvEpisode(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgTvEpisode(ogTvEpisode);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOgTvEpisodes(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteOgTvEpisodes(filter, params, values);
        return count;
    }

}
