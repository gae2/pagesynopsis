package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.bean.ImageStructBean;
import com.pagesynopsis.ws.stub.ImageStructStub;


public class ImageStructResourceUtil
{
    private static final Logger log = Logger.getLogger(ImageStructResourceUtil.class.getName());

    // Static methods only.
    private ImageStructResourceUtil() {}

    public static ImageStructBean convertImageStructStubToBean(ImageStruct stub)
    {
        ImageStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null ImageStructBean is returned.");
        } else {
            bean = new ImageStructBean();
            bean.setUuid(stub.getUuid());
            bean.setId(stub.getId());
            bean.setAlt(stub.getAlt());
            bean.setSrc(stub.getSrc());
            bean.setSrcUrl(stub.getSrcUrl());
            bean.setMediaType(stub.getMediaType());
            bean.setWidthAttr(stub.getWidthAttr());
            bean.setWidth(stub.getWidth());
            bean.setHeightAttr(stub.getHeightAttr());
            bean.setHeight(stub.getHeight());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
