package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogMovies")
@XmlType(propOrder = {"ogMovie", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgMovieListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgMovieListStub.class.getName());

    private List<OgMovieStub> ogMovies = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgMovieListStub()
    {
        this(new ArrayList<OgMovieStub>());
    }
    public OgMovieListStub(List<OgMovieStub> ogMovies)
    {
        this(ogMovies, null);
    }
    public OgMovieListStub(List<OgMovieStub> ogMovies, String forwardCursor)
    {
        this.ogMovies = ogMovies;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogMovies == null) {
            return true;
        } else {
            return ogMovies.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogMovies == null) {
            return 0;
        } else {
            return ogMovies.size();
        }
    }


    @XmlElement(name = "ogMovie")
    public List<OgMovieStub> getOgMovie()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgMovieStub> getList()
    {
        return ogMovies;
    }
    public void setList(List<OgMovieStub> ogMovies)
    {
        this.ogMovies = ogMovies;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgMovieStub> it = this.ogMovies.iterator();
        while(it.hasNext()) {
            OgMovieStub ogMovie = it.next();
            sb.append(ogMovie.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgMovieListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgMovieListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgMovieListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgMovieListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgMovieListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgMovieListStub fromJsonString(String jsonStr)
    {
        try {
            OgMovieListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgMovieListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgMovieListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgMovieListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgMovieListStub object.", e);
        }
        
        return null;
    }

}
