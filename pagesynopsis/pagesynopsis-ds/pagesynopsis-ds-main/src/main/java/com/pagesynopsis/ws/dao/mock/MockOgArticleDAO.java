package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OgArticleDAO;
import com.pagesynopsis.ws.data.OgArticleDataObject;


// MockOgArticleDAO is a decorator.
// It can be used as a base class to mock OgArticleDAO objects.
public abstract class MockOgArticleDAO implements OgArticleDAO
{
    private static final Logger log = Logger.getLogger(MockOgArticleDAO.class.getName()); 

    // MockOgArticleDAO uses the decorator design pattern.
    private OgArticleDAO decoratedDAO;

    public MockOgArticleDAO(OgArticleDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected OgArticleDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(OgArticleDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public OgArticleDataObject getOgArticle(String guid) throws BaseException
    {
        return decoratedDAO.getOgArticle(guid);
	}

    @Override
    public List<OgArticleDataObject> getOgArticles(List<String> guids) throws BaseException
    {
        return decoratedDAO.getOgArticles(guids);
    }

    @Override
    public List<OgArticleDataObject> getAllOgArticles() throws BaseException
	{
	    return getAllOgArticles(null, null, null);
    }


    @Override
    public List<OgArticleDataObject> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOgArticles(ordering, offset, count, null);
    }

    @Override
    public List<OgArticleDataObject> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllOgArticles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticleKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllOgArticleKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOgArticles(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOgArticles(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgArticle(OgArticleDataObject ogArticle) throws BaseException
    {
        return decoratedDAO.createOgArticle( ogArticle);
    }

    @Override
	public Boolean updateOgArticle(OgArticleDataObject ogArticle) throws BaseException
	{
        return decoratedDAO.updateOgArticle(ogArticle);
	}
	
    @Override
    public Boolean deleteOgArticle(OgArticleDataObject ogArticle) throws BaseException
    {
        return decoratedDAO.deleteOgArticle(ogArticle);
    }

    @Override
    public Boolean deleteOgArticle(String guid) throws BaseException
    {
        return decoratedDAO.deleteOgArticle(guid);
	}

    @Override
    public Long deleteOgArticles(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteOgArticles(filter, params, values);
    }

}
