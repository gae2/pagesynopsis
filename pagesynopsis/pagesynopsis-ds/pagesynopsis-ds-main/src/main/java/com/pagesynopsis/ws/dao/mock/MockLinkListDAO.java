package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.LinkListDAO;
import com.pagesynopsis.ws.data.LinkListDataObject;


// MockLinkListDAO is a decorator.
// It can be used as a base class to mock LinkListDAO objects.
public abstract class MockLinkListDAO implements LinkListDAO
{
    private static final Logger log = Logger.getLogger(MockLinkListDAO.class.getName()); 

    // MockLinkListDAO uses the decorator design pattern.
    private LinkListDAO decoratedDAO;

    public MockLinkListDAO(LinkListDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected LinkListDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(LinkListDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public LinkListDataObject getLinkList(String guid) throws BaseException
    {
        return decoratedDAO.getLinkList(guid);
	}

    @Override
    public List<LinkListDataObject> getLinkLists(List<String> guids) throws BaseException
    {
        return decoratedDAO.getLinkLists(guids);
    }

    @Override
    public List<LinkListDataObject> getAllLinkLists() throws BaseException
	{
	    return getAllLinkLists(null, null, null);
    }


    @Override
    public List<LinkListDataObject> getAllLinkLists(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllLinkLists(ordering, offset, count, null);
    }

    @Override
    public List<LinkListDataObject> getAllLinkLists(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllLinkLists(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllLinkListKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllLinkListKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<LinkListDataObject> findLinkLists(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findLinkLists(filter, ordering, params, values, null, null);
    }

    @Override
	public List<LinkListDataObject> findLinkLists(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findLinkLists(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<LinkListDataObject> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<LinkListDataObject> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createLinkList(LinkListDataObject linkList) throws BaseException
    {
        return decoratedDAO.createLinkList( linkList);
    }

    @Override
	public Boolean updateLinkList(LinkListDataObject linkList) throws BaseException
	{
        return decoratedDAO.updateLinkList(linkList);
	}
	
    @Override
    public Boolean deleteLinkList(LinkListDataObject linkList) throws BaseException
    {
        return decoratedDAO.deleteLinkList(linkList);
    }

    @Override
    public Boolean deleteLinkList(String guid) throws BaseException
    {
        return decoratedDAO.deleteLinkList(guid);
	}

    @Override
    public Long deleteLinkLists(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteLinkLists(filter, params, values);
    }

}
