package com.pagesynopsis.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ResourceAlreadyPresentException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.DataStoreRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.bean.RobotsTextRefreshBean;
import com.pagesynopsis.ws.stub.RobotsTextRefreshListStub;
import com.pagesynopsis.ws.stub.RobotsTextRefreshStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.RobotsTextRefreshResource;

// MockRobotsTextRefreshResource is a decorator.
// It can be used as a base class to mock RobotsTextRefreshResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/robotsTextRefreshes/")
public abstract class MockRobotsTextRefreshResource implements RobotsTextRefreshResource
{
    private static final Logger log = Logger.getLogger(MockRobotsTextRefreshResource.class.getName());

    // MockRobotsTextRefreshResource uses the decorator design pattern.
    private RobotsTextRefreshResource decoratedResource;

    public MockRobotsTextRefreshResource(RobotsTextRefreshResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected RobotsTextRefreshResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(RobotsTextRefreshResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getRobotsTextRefreshKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getRobotsTextRefreshKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getRobotsTextRefresh(String guid) throws BaseResourceException
    {
        return decoratedResource.getRobotsTextRefresh(guid);
    }

    @Override
    public Response getRobotsTextRefresh(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getRobotsTextRefresh(guid, field);
    }

    @Override
    public Response createRobotsTextRefresh(RobotsTextRefreshStub robotsTextRefresh) throws BaseResourceException
    {
        return decoratedResource.createRobotsTextRefresh(robotsTextRefresh);
    }

    @Override
    public Response updateRobotsTextRefresh(String guid, RobotsTextRefreshStub robotsTextRefresh) throws BaseResourceException
    {
        return decoratedResource.updateRobotsTextRefresh(guid, robotsTextRefresh);
    }

    @Override
    public Response updateRobotsTextRefresh(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseResourceException
    {
        return decoratedResource.updateRobotsTextRefresh(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
    }

    @Override
    public Response deleteRobotsTextRefresh(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteRobotsTextRefresh(guid);
    }

    @Override
    public Response deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteRobotsTextRefreshes(filter, params, values);
    }


}
