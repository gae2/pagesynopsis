package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.bean.UrlStructBean;
import com.pagesynopsis.ws.bean.ImageStructBean;
import com.pagesynopsis.ws.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.bean.AnchorStructBean;
import com.pagesynopsis.ws.bean.KeyValuePairStructBean;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.bean.AudioStructBean;
import com.pagesynopsis.ws.bean.VideoStructBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.bean.PageInfoBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.PageInfoDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.PageInfoService;


// PageInfoMockService is a decorator.
// It can be used as a base class to mock PageInfoService objects.
public abstract class PageInfoMockService implements PageInfoService
{
    private static final Logger log = Logger.getLogger(PageInfoMockService.class.getName());

    // PageInfoMockService uses the decorator design pattern.
    private PageInfoService decoratedService;

    public PageInfoMockService(PageInfoService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected PageInfoService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(PageInfoService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // PageInfo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public PageInfo getPageInfo(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getPageInfo(): guid = " + guid);
        PageInfo bean = decoratedService.getPageInfo(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getPageInfo(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getPageInfo(guid, field);
        return obj;
    }

    @Override
    public List<PageInfo> getPageInfos(List<String> guids) throws BaseException
    {
        log.fine("getPageInfos()");
        List<PageInfo> pageInfos = decoratedService.getPageInfos(guids);
        log.finer("END");
        return pageInfos;
    }

    @Override
    public List<PageInfo> getAllPageInfos() throws BaseException
    {
        return getAllPageInfos(null, null, null);
    }


    @Override
    public List<PageInfo> getAllPageInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageInfos(ordering, offset, count, null);
    }

    @Override
    public List<PageInfo> getAllPageInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllPageInfos(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<PageInfo> pageInfos = decoratedService.getAllPageInfos(ordering, offset, count, forwardCursor);
        log.finer("END");
        return pageInfos;
    }

    @Override
    public List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllPageInfoKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllPageInfoKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<PageInfo> findPageInfos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findPageInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<PageInfo> findPageInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<PageInfo> findPageInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("PageInfoMockService.findPageInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<PageInfo> pageInfos = decoratedService.findPageInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return pageInfos;
    }

    @Override
    public List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("PageInfoMockService.findPageInfoKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("PageInfoMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createPageInfo(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String pageAuthor, String pageDescription, String favicon, String faviconUrl) throws BaseException
    {
        return decoratedService.createPageInfo(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, pageAuthor, pageDescription, favicon, faviconUrl);
    }

    @Override
    public String createPageInfo(PageInfo pageInfo) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createPageInfo(pageInfo);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updatePageInfo(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String pageAuthor, String pageDescription, String favicon, String faviconUrl) throws BaseException
    {
        return decoratedService.updatePageInfo(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, pageAuthor, pageDescription, favicon, faviconUrl);
    }
        
    @Override
    public Boolean updatePageInfo(PageInfo pageInfo) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updatePageInfo(pageInfo);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deletePageInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deletePageInfo(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deletePageInfo(PageInfo pageInfo) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deletePageInfo(pageInfo);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deletePageInfos(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deletePageInfos(filter, params, values);
        return count;
    }

}
