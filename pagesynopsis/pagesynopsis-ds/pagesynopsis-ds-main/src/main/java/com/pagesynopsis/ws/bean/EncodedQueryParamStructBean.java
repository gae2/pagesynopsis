package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.EncodedQueryParamStruct;
import com.pagesynopsis.ws.data.EncodedQueryParamStructDataObject;

public class EncodedQueryParamStructBean implements EncodedQueryParamStruct
{
    private static final Logger log = Logger.getLogger(EncodedQueryParamStructBean.class.getName());

    // Embedded data object.
    private EncodedQueryParamStructDataObject dobj = null;

    public EncodedQueryParamStructBean()
    {
        this(new EncodedQueryParamStructDataObject());
    }
    public EncodedQueryParamStructBean(EncodedQueryParamStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public EncodedQueryParamStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getParamType()
    {
        if(getDataObject() != null) {
            return getDataObject().getParamType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EncodedQueryParamStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setParamType(String paramType)
    {
        if(getDataObject() != null) {
            getDataObject().setParamType(paramType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EncodedQueryParamStructDataObject is null!");
        }
    }

    public String getOriginalString()
    {
        if(getDataObject() != null) {
            return getDataObject().getOriginalString();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EncodedQueryParamStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setOriginalString(String originalString)
    {
        if(getDataObject() != null) {
            getDataObject().setOriginalString(originalString);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EncodedQueryParamStructDataObject is null!");
        }
    }

    public String getEncodedString()
    {
        if(getDataObject() != null) {
            return getDataObject().getEncodedString();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EncodedQueryParamStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setEncodedString(String encodedString)
    {
        if(getDataObject() != null) {
            getDataObject().setEncodedString(encodedString);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EncodedQueryParamStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EncodedQueryParamStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EncodedQueryParamStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded EncodedQueryParamStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getParamType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getOriginalString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEncodedString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public EncodedQueryParamStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
