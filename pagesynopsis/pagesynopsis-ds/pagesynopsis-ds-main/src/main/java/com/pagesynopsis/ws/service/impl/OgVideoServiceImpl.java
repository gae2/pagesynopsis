package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgVideoService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgVideoServiceImpl implements OgVideoService
{
    private static final Logger log = Logger.getLogger(OgVideoServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // OgVideo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgVideo getOgVideo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        OgVideoDataObject dataObj = getDAOFactory().getOgVideoDAO().getOgVideo(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgVideoDataObject for guid = " + guid);
            return null;  // ????
        }
        OgVideoBean bean = new OgVideoBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgVideo(String guid, String field) throws BaseException
    {
        OgVideoDataObject dataObj = getDAOFactory().getOgVideoDAO().getOgVideo(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgVideoDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("siteName")) {
            return dataObj.getSiteName();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("fbAdmins")) {
            return dataObj.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return dataObj.getFbAppId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("audio")) {
            return dataObj.getAudio();
        } else if(field.equals("video")) {
            return dataObj.getVideo();
        } else if(field.equals("locale")) {
            return dataObj.getLocale();
        } else if(field.equals("localeAlternate")) {
            return dataObj.getLocaleAlternate();
        } else if(field.equals("director")) {
            return dataObj.getDirector();
        } else if(field.equals("writer")) {
            return dataObj.getWriter();
        } else if(field.equals("actor")) {
            return dataObj.getActor();
        } else if(field.equals("duration")) {
            return dataObj.getDuration();
        } else if(field.equals("tag")) {
            return dataObj.getTag();
        } else if(field.equals("releaseDate")) {
            return dataObj.getReleaseDate();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgVideo> getOgVideos(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<OgVideo> list = new ArrayList<OgVideo>();
        List<OgVideoDataObject> dataObjs = getDAOFactory().getOgVideoDAO().getOgVideos(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgVideoDataObject list.");
        } else {
            Iterator<OgVideoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgVideoDataObject dataObj = (OgVideoDataObject) it.next();
                list.add(new OgVideoBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<OgVideo> getAllOgVideos() throws BaseException
    {
        return getAllOgVideos(null, null, null);
    }

    @Override
    public List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgVideos(ordering, offset, count, null);
    }

    @Override
    public List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgVideos(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgVideo> list = new ArrayList<OgVideo>();
        List<OgVideoDataObject> dataObjs = getDAOFactory().getOgVideoDAO().getAllOgVideos(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgVideoDataObject list.");
        } else {
            Iterator<OgVideoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgVideoDataObject dataObj = (OgVideoDataObject) it.next();
                list.add(new OgVideoBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgVideoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllOgVideoKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgVideoDAO().getAllOgVideoKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgVideo key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgVideos(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgVideoServiceImpl.findOgVideos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgVideo> list = new ArrayList<OgVideo>();
        List<OgVideoDataObject> dataObjs = getDAOFactory().getOgVideoDAO().findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find ogVideos for the given criterion.");
        } else {
            Iterator<OgVideoDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgVideoDataObject dataObj = (OgVideoDataObject) it.next();
                list.add(new OgVideoBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgVideoServiceImpl.findOgVideoKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgVideoDAO().findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgVideo keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgVideoServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getOgVideoDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createOgVideo(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        OgVideoDataObject dataObj = new OgVideoDataObject(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return createOgVideo(dataObj);
    }

    @Override
    public String createOgVideo(OgVideo ogVideo) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogVideo cannot be null.....
        if(ogVideo == null) {
            log.log(Level.INFO, "Param ogVideo is null!");
            throw new BadRequestException("Param ogVideo object is null!");
        }
        OgVideoDataObject dataObj = null;
        if(ogVideo instanceof OgVideoDataObject) {
            dataObj = (OgVideoDataObject) ogVideo;
        } else if(ogVideo instanceof OgVideoBean) {
            dataObj = ((OgVideoBean) ogVideo).toDataObject();
        } else {  // if(ogVideo instanceof OgVideo)
            //dataObj = new OgVideoDataObject(null, ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new OgVideoDataObject(ogVideo.getGuid(), ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate());
        }
        String guid = getDAOFactory().getOgVideoDAO().createOgVideo(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgVideo(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgVideoDataObject dataObj = new OgVideoDataObject(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return updateOgVideo(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateOgVideo(OgVideo ogVideo) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogVideo cannot be null.....
        if(ogVideo == null || ogVideo.getGuid() == null) {
            log.log(Level.INFO, "Param ogVideo or its guid is null!");
            throw new BadRequestException("Param ogVideo object or its guid is null!");
        }
        OgVideoDataObject dataObj = null;
        if(ogVideo instanceof OgVideoDataObject) {
            dataObj = (OgVideoDataObject) ogVideo;
        } else if(ogVideo instanceof OgVideoBean) {
            dataObj = ((OgVideoBean) ogVideo).toDataObject();
        } else {  // if(ogVideo instanceof OgVideo)
            dataObj = new OgVideoDataObject(ogVideo.getGuid(), ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate());
        }
        Boolean suc = getDAOFactory().getOgVideoDAO().updateOgVideo(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteOgVideo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getOgVideoDAO().deleteOgVideo(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteOgVideo(OgVideo ogVideo) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogVideo cannot be null.....
        if(ogVideo == null || ogVideo.getGuid() == null) {
            log.log(Level.INFO, "Param ogVideo or its guid is null!");
            throw new BadRequestException("Param ogVideo object or its guid is null!");
        }
        OgVideoDataObject dataObj = null;
        if(ogVideo instanceof OgVideoDataObject) {
            dataObj = (OgVideoDataObject) ogVideo;
        } else if(ogVideo instanceof OgVideoBean) {
            dataObj = ((OgVideoBean) ogVideo).toDataObject();
        } else {  // if(ogVideo instanceof OgVideo)
            dataObj = new OgVideoDataObject(ogVideo.getGuid(), ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate());
        }
        Boolean suc = getDAOFactory().getOgVideoDAO().deleteOgVideo(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteOgVideos(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getOgVideoDAO().deleteOgVideos(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
