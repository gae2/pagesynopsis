package com.pagesynopsis.ws.search.gae;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.GeoPointStruct;
import com.pagesynopsis.ws.StreetAddressStruct;
import com.pagesynopsis.ws.GaeAppStruct;
import com.pagesynopsis.ws.FullNameStruct;
import com.pagesynopsis.ws.GaeUserStruct;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.User;


public class UserIndexBuilder extends BaseIndexBuilder
{
    private static final Logger log = Logger.getLogger(UserIndexBuilder.class.getName());
    
    public UserIndexBuilder()
    {
        super("UserIndex");	
    }
    
    public boolean addDocument(User user)
    {
        return addDocument(user.getGuid(), user.getNickname(), user.getLocation(), user.getGeoPoint());
    }

    public boolean addDocument(String guid, String nickname, String location, GeoPointStruct geoPoint)
    {
    	if(log.isLoggable(Level.FINER)) log.finer("BEGIN: addDocument() called with guid = " + guid);

    	Builder builder = Document.newBuilder().setId(guid);
        if(nickname != null) {
            if(!nickname.isEmpty()) {
                builder.addField(Field.newBuilder().setName("nickname").setText(nickname));
            }
        }
        if(location != null) {
            if(!location.isEmpty()) {
                builder.addField(Field.newBuilder().setName("location").setText(location));
            }
        }
        if(geoPoint != null) {
        	Double lat = geoPoint.getLatitude();
        	Double lng = geoPoint.getLongitude();
        	if(lat != null && lng != null) {
                builder.addField(Field.newBuilder().setName("geoPoint").setGeoPoint(new GeoPoint(lat, lng)));
        	}
        }
    	Document doc = builder.build();
    	boolean suc = addDocument(doc);

    	if(log.isLoggable(Level.FINER)) log.finer("END: addDocument(). suc = " + suc);
    	return suc;
    }


}

