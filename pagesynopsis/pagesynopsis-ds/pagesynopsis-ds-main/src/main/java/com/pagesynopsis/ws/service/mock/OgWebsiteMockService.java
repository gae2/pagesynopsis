package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgWebsiteService;


// OgWebsiteMockService is a decorator.
// It can be used as a base class to mock OgWebsiteService objects.
public abstract class OgWebsiteMockService implements OgWebsiteService
{
    private static final Logger log = Logger.getLogger(OgWebsiteMockService.class.getName());

    // OgWebsiteMockService uses the decorator design pattern.
    private OgWebsiteService decoratedService;

    public OgWebsiteMockService(OgWebsiteService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected OgWebsiteService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(OgWebsiteService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // OgWebsite related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgWebsite getOgWebsite(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgWebsite(): guid = " + guid);
        OgWebsite bean = decoratedService.getOgWebsite(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgWebsite(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getOgWebsite(guid, field);
        return obj;
    }

    @Override
    public List<OgWebsite> getOgWebsites(List<String> guids) throws BaseException
    {
        log.fine("getOgWebsites()");
        List<OgWebsite> ogWebsites = decoratedService.getOgWebsites(guids);
        log.finer("END");
        return ogWebsites;
    }

    @Override
    public List<OgWebsite> getAllOgWebsites() throws BaseException
    {
        return getAllOgWebsites(null, null, null);
    }


    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgWebsites(ordering, offset, count, null);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgWebsites(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<OgWebsite> ogWebsites = decoratedService.getAllOgWebsites(ordering, offset, count, forwardCursor);
        log.finer("END");
        return ogWebsites;
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgWebsiteKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgWebsiteKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllOgWebsiteKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgWebsites(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgWebsiteMockService.findOgWebsites(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<OgWebsite> ogWebsites = decoratedService.findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return ogWebsites;
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgWebsiteMockService.findOgWebsiteKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgWebsiteMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgWebsite(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return decoratedService.createOgWebsite(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public String createOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createOgWebsite(ogWebsite);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgWebsite(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return decoratedService.updateOgWebsite(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }
        
    @Override
    public Boolean updateOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateOgWebsite(ogWebsite);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteOgWebsite(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgWebsite(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgWebsite(ogWebsite);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOgWebsites(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteOgWebsites(filter, params, values);
        return count;
    }

}
