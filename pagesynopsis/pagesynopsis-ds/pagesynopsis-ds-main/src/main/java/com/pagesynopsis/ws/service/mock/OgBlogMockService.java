package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgBlogService;


// OgBlogMockService is a decorator.
// It can be used as a base class to mock OgBlogService objects.
public abstract class OgBlogMockService implements OgBlogService
{
    private static final Logger log = Logger.getLogger(OgBlogMockService.class.getName());

    // OgBlogMockService uses the decorator design pattern.
    private OgBlogService decoratedService;

    public OgBlogMockService(OgBlogService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected OgBlogService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(OgBlogService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // OgBlog related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgBlog getOgBlog(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgBlog(): guid = " + guid);
        OgBlog bean = decoratedService.getOgBlog(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgBlog(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getOgBlog(guid, field);
        return obj;
    }

    @Override
    public List<OgBlog> getOgBlogs(List<String> guids) throws BaseException
    {
        log.fine("getOgBlogs()");
        List<OgBlog> ogBlogs = decoratedService.getOgBlogs(guids);
        log.finer("END");
        return ogBlogs;
    }

    @Override
    public List<OgBlog> getAllOgBlogs() throws BaseException
    {
        return getAllOgBlogs(null, null, null);
    }


    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBlogs(ordering, offset, count, null);
    }

    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgBlogs(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<OgBlog> ogBlogs = decoratedService.getAllOgBlogs(ordering, offset, count, forwardCursor);
        log.finer("END");
        return ogBlogs;
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBlogKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgBlogKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllOgBlogKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgBlogs(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgBlogMockService.findOgBlogs(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<OgBlog> ogBlogs = decoratedService.findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return ogBlogs;
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgBlogMockService.findOgBlogKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgBlogMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgBlog(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return decoratedService.createOgBlog(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public String createOgBlog(OgBlog ogBlog) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createOgBlog(ogBlog);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgBlog(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return decoratedService.updateOgBlog(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }
        
    @Override
    public Boolean updateOgBlog(OgBlog ogBlog) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateOgBlog(ogBlog);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteOgBlog(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgBlog(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteOgBlog(OgBlog ogBlog) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgBlog(ogBlog);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOgBlogs(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteOgBlogs(filter, params, values);
        return count;
    }

}
