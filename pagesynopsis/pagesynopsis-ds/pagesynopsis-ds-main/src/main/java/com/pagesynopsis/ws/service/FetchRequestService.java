package com.pagesynopsis.ws.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface FetchRequestService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    FetchRequest getFetchRequest(String guid) throws BaseException;
    Object getFetchRequest(String guid, String field) throws BaseException;
    List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException;
    List<FetchRequest> getAllFetchRequests() throws BaseException;
    /* @Deprecated */ List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException;
    List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createFetchRequest(String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException;
    //String createFetchRequest(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return FetchRequest?)
    String createFetchRequest(FetchRequest fetchRequest) throws BaseException;          // Returns Guid.  (Return FetchRequest?)
    Boolean updateFetchRequest(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException;
    //Boolean updateFetchRequest(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException;
    Boolean deleteFetchRequest(String guid) throws BaseException;
    Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException;
    Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException;

//    Integer createFetchRequests(List<FetchRequest> fetchRequests) throws BaseException;
//    Boolean updateeFetchRequests(List<FetchRequest> fetchRequests) throws BaseException;

}
