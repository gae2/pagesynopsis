package com.pagesynopsis.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ResourceAlreadyPresentException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.DataStoreRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.stub.OgArticleListStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.OgArticleResource;
import com.pagesynopsis.ws.resource.util.OgAudioStructResourceUtil;
import com.pagesynopsis.ws.resource.util.OgImageStructResourceUtil;
import com.pagesynopsis.ws.resource.util.OgActorStructResourceUtil;
import com.pagesynopsis.ws.resource.util.OgVideoStructResourceUtil;

// MockOgArticleResource is a decorator.
// It can be used as a base class to mock OgArticleResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/ogArticles/")
public abstract class MockOgArticleResource implements OgArticleResource
{
    private static final Logger log = Logger.getLogger(MockOgArticleResource.class.getName());

    // MockOgArticleResource uses the decorator design pattern.
    private OgArticleResource decoratedResource;

    public MockOgArticleResource(OgArticleResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected OgArticleResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(OgArticleResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgArticles(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllOgArticleKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getOgArticleKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getOgArticleKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getOgArticle(String guid) throws BaseResourceException
    {
        return decoratedResource.getOgArticle(guid);
    }

    @Override
    public Response getOgArticle(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getOgArticle(guid, field);
    }

    @Override
    public Response createOgArticle(OgArticleStub ogArticle) throws BaseResourceException
    {
        return decoratedResource.createOgArticle(ogArticle);
    }

    @Override
    public Response updateOgArticle(String guid, OgArticleStub ogArticle) throws BaseResourceException
    {
        return decoratedResource.updateOgArticle(guid, ogArticle);
    }

    @Override
    public Response updateOgArticle(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<String> image, List<String> audio, List<String> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseResourceException
    {
        return decoratedResource.updateOgArticle(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
    }

    @Override
    public Response deleteOgArticle(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteOgArticle(guid);
    }

    @Override
    public Response deleteOgArticles(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteOgArticles(filter, params, values);
    }


}
