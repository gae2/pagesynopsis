package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgArticleService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgArticleServiceImpl implements OgArticleService
{
    private static final Logger log = Logger.getLogger(OgArticleServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // OgArticle related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgArticle getOgArticle(String guid) throws BaseException
    {
        log.finer("BEGIN");

        OgArticleDataObject dataObj = getDAOFactory().getOgArticleDAO().getOgArticle(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgArticleDataObject for guid = " + guid);
            return null;  // ????
        }
        OgArticleBean bean = new OgArticleBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgArticle(String guid, String field) throws BaseException
    {
        OgArticleDataObject dataObj = getDAOFactory().getOgArticleDAO().getOgArticle(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgArticleDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("siteName")) {
            return dataObj.getSiteName();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("fbAdmins")) {
            return dataObj.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return dataObj.getFbAppId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("audio")) {
            return dataObj.getAudio();
        } else if(field.equals("video")) {
            return dataObj.getVideo();
        } else if(field.equals("locale")) {
            return dataObj.getLocale();
        } else if(field.equals("localeAlternate")) {
            return dataObj.getLocaleAlternate();
        } else if(field.equals("author")) {
            return dataObj.getAuthor();
        } else if(field.equals("section")) {
            return dataObj.getSection();
        } else if(field.equals("tag")) {
            return dataObj.getTag();
        } else if(field.equals("publishedDate")) {
            return dataObj.getPublishedDate();
        } else if(field.equals("modifiedDate")) {
            return dataObj.getModifiedDate();
        } else if(field.equals("expirationDate")) {
            return dataObj.getExpirationDate();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgArticle> getOgArticles(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<OgArticle> list = new ArrayList<OgArticle>();
        List<OgArticleDataObject> dataObjs = getDAOFactory().getOgArticleDAO().getOgArticles(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgArticleDataObject list.");
        } else {
            Iterator<OgArticleDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgArticleDataObject dataObj = (OgArticleDataObject) it.next();
                list.add(new OgArticleBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<OgArticle> getAllOgArticles() throws BaseException
    {
        return getAllOgArticles(null, null, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticles(ordering, offset, count, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgArticles(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgArticle> list = new ArrayList<OgArticle>();
        List<OgArticleDataObject> dataObjs = getDAOFactory().getOgArticleDAO().getAllOgArticles(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgArticleDataObject list.");
        } else {
            Iterator<OgArticleDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgArticleDataObject dataObj = (OgArticleDataObject) it.next();
                list.add(new OgArticleBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticleKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllOgArticleKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgArticleDAO().getAllOgArticleKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgArticle key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgArticleServiceImpl.findOgArticles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgArticle> list = new ArrayList<OgArticle>();
        List<OgArticleDataObject> dataObjs = getDAOFactory().getOgArticleDAO().findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find ogArticles for the given criterion.");
        } else {
            Iterator<OgArticleDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgArticleDataObject dataObj = (OgArticleDataObject) it.next();
                list.add(new OgArticleBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgArticleServiceImpl.findOgArticleKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgArticleDAO().findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgArticle keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgArticleServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getOgArticleDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createOgArticle(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        OgArticleDataObject dataObj = new OgArticleDataObject(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
        return createOgArticle(dataObj);
    }

    @Override
    public String createOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogArticle cannot be null.....
        if(ogArticle == null) {
            log.log(Level.INFO, "Param ogArticle is null!");
            throw new BadRequestException("Param ogArticle object is null!");
        }
        OgArticleDataObject dataObj = null;
        if(ogArticle instanceof OgArticleDataObject) {
            dataObj = (OgArticleDataObject) ogArticle;
        } else if(ogArticle instanceof OgArticleBean) {
            dataObj = ((OgArticleBean) ogArticle).toDataObject();
        } else {  // if(ogArticle instanceof OgArticle)
            //dataObj = new OgArticleDataObject(null, ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new OgArticleDataObject(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate());
        }
        String guid = getDAOFactory().getOgArticleDAO().createOgArticle(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgArticle(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgArticleDataObject dataObj = new OgArticleDataObject(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
        return updateOgArticle(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogArticle cannot be null.....
        if(ogArticle == null || ogArticle.getGuid() == null) {
            log.log(Level.INFO, "Param ogArticle or its guid is null!");
            throw new BadRequestException("Param ogArticle object or its guid is null!");
        }
        OgArticleDataObject dataObj = null;
        if(ogArticle instanceof OgArticleDataObject) {
            dataObj = (OgArticleDataObject) ogArticle;
        } else if(ogArticle instanceof OgArticleBean) {
            dataObj = ((OgArticleBean) ogArticle).toDataObject();
        } else {  // if(ogArticle instanceof OgArticle)
            dataObj = new OgArticleDataObject(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate());
        }
        Boolean suc = getDAOFactory().getOgArticleDAO().updateOgArticle(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteOgArticle(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getOgArticleDAO().deleteOgArticle(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogArticle cannot be null.....
        if(ogArticle == null || ogArticle.getGuid() == null) {
            log.log(Level.INFO, "Param ogArticle or its guid is null!");
            throw new BadRequestException("Param ogArticle object or its guid is null!");
        }
        OgArticleDataObject dataObj = null;
        if(ogArticle instanceof OgArticleDataObject) {
            dataObj = (OgArticleDataObject) ogArticle;
        } else if(ogArticle instanceof OgArticleBean) {
            dataObj = ((OgArticleBean) ogArticle).toDataObject();
        } else {  // if(ogArticle instanceof OgArticle)
            dataObj = new OgArticleDataObject(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate());
        }
        Boolean suc = getDAOFactory().getOgArticleDAO().deleteOgArticle(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteOgArticles(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getOgArticleDAO().deleteOgArticles(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
