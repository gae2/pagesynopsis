package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.TwitterProductCardDAO;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;


// MockTwitterProductCardDAO is a decorator.
// It can be used as a base class to mock TwitterProductCardDAO objects.
public abstract class MockTwitterProductCardDAO implements TwitterProductCardDAO
{
    private static final Logger log = Logger.getLogger(MockTwitterProductCardDAO.class.getName()); 

    // MockTwitterProductCardDAO uses the decorator design pattern.
    private TwitterProductCardDAO decoratedDAO;

    public MockTwitterProductCardDAO(TwitterProductCardDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected TwitterProductCardDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(TwitterProductCardDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public TwitterProductCardDataObject getTwitterProductCard(String guid) throws BaseException
    {
        return decoratedDAO.getTwitterProductCard(guid);
	}

    @Override
    public List<TwitterProductCardDataObject> getTwitterProductCards(List<String> guids) throws BaseException
    {
        return decoratedDAO.getTwitterProductCards(guids);
    }

    @Override
    public List<TwitterProductCardDataObject> getAllTwitterProductCards() throws BaseException
	{
	    return getAllTwitterProductCards(null, null, null);
    }


    @Override
    public List<TwitterProductCardDataObject> getAllTwitterProductCards(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllTwitterProductCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterProductCardDataObject> getAllTwitterProductCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllTwitterProductCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterProductCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllTwitterProductCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<TwitterProductCardDataObject> findTwitterProductCards(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTwitterProductCards(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TwitterProductCardDataObject> findTwitterProductCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTwitterProductCards(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<TwitterProductCardDataObject> findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<TwitterProductCardDataObject> findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterProductCard(TwitterProductCardDataObject twitterProductCard) throws BaseException
    {
        return decoratedDAO.createTwitterProductCard( twitterProductCard);
    }

    @Override
	public Boolean updateTwitterProductCard(TwitterProductCardDataObject twitterProductCard) throws BaseException
	{
        return decoratedDAO.updateTwitterProductCard(twitterProductCard);
	}
	
    @Override
    public Boolean deleteTwitterProductCard(TwitterProductCardDataObject twitterProductCard) throws BaseException
    {
        return decoratedDAO.deleteTwitterProductCard(twitterProductCard);
    }

    @Override
    public Boolean deleteTwitterProductCard(String guid) throws BaseException
    {
        return decoratedDAO.deleteTwitterProductCard(guid);
	}

    @Override
    public Long deleteTwitterProductCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteTwitterProductCards(filter, params, values);
    }

}
