package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.ImageSetDataObject;

public class ImageSetBean extends PageBaseBean implements ImageSet
{
    private static final Logger log = Logger.getLogger(ImageSetBean.class.getName());

    // Embedded data object.
    private ImageSetDataObject dobj = null;

    public ImageSetBean()
    {
        this(new ImageSetDataObject());
    }
    public ImageSetBean(String guid)
    {
        this(new ImageSetDataObject(guid));
    }
    public ImageSetBean(ImageSetDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public ImageSetDataObject getDataObject()
    {
        return this.dobj;
    }

    public List<String> getMediaTypeFilter()
    {
        if(getDataObject() != null) {
            return getDataObject().getMediaTypeFilter();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageSetDataObject is null!");
            return null;   // ???
        }
    }
    public void setMediaTypeFilter(List<String> mediaTypeFilter)
    {
        if(getDataObject() != null) {
            getDataObject().setMediaTypeFilter(mediaTypeFilter);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageSetDataObject is null!");
        }
    }

    public Set<ImageStruct> getPageImages()
    {
        if(getDataObject() != null) {
            Set<ImageStruct> list = getDataObject().getPageImages();
            if(list != null) {
                Set<ImageStruct> bean = new HashSet<ImageStruct>();
                for(ImageStruct imageStruct : list) {
                    ImageStructBean elem = null;
                    if(imageStruct instanceof ImageStructBean) {
                        elem = (ImageStructBean) imageStruct;
                    } else if(imageStruct instanceof ImageStructDataObject) {
                        elem = new ImageStructBean((ImageStructDataObject) imageStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageSetDataObject is null!");
            return null;
        }
    }
    public void setPageImages(Set<ImageStruct> pageImages)
    {
        if(pageImages != null) {
            if(getDataObject() != null) {
                Set<ImageStruct> dataObj = new HashSet<ImageStruct>();
                for(ImageStruct imageStruct : pageImages) {
                    ImageStructDataObject elem = null;
                    if(imageStruct instanceof ImageStructBean) {
                        elem = ((ImageStructBean) imageStruct).toDataObject();
                    } else if(imageStruct instanceof ImageStructDataObject) {
                        elem = (ImageStructDataObject) imageStruct;
                    } else if(imageStruct instanceof ImageStruct) {
                        elem = new ImageStructDataObject(imageStruct.getUuid(), imageStruct.getId(), imageStruct.getAlt(), imageStruct.getSrc(), imageStruct.getSrcUrl(), imageStruct.getMediaType(), imageStruct.getWidthAttr(), imageStruct.getWidth(), imageStruct.getHeightAttr(), imageStruct.getHeight(), imageStruct.getNote());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setPageImages(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded ImageSetDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setPageImages(pageImages);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded ImageSetDataObject is null!");
            }
        }
    }


    // TBD
    public ImageSetDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
