package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgWebsiteService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgWebsiteServiceImpl implements OgWebsiteService
{
    private static final Logger log = Logger.getLogger(OgWebsiteServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // OgWebsite related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgWebsite getOgWebsite(String guid) throws BaseException
    {
        log.finer("BEGIN");

        OgWebsiteDataObject dataObj = getDAOFactory().getOgWebsiteDAO().getOgWebsite(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgWebsiteDataObject for guid = " + guid);
            return null;  // ????
        }
        OgWebsiteBean bean = new OgWebsiteBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgWebsite(String guid, String field) throws BaseException
    {
        OgWebsiteDataObject dataObj = getDAOFactory().getOgWebsiteDAO().getOgWebsite(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgWebsiteDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("siteName")) {
            return dataObj.getSiteName();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("fbAdmins")) {
            return dataObj.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return dataObj.getFbAppId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("audio")) {
            return dataObj.getAudio();
        } else if(field.equals("video")) {
            return dataObj.getVideo();
        } else if(field.equals("locale")) {
            return dataObj.getLocale();
        } else if(field.equals("localeAlternate")) {
            return dataObj.getLocaleAlternate();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgWebsite> getOgWebsites(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<OgWebsite> list = new ArrayList<OgWebsite>();
        List<OgWebsiteDataObject> dataObjs = getDAOFactory().getOgWebsiteDAO().getOgWebsites(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgWebsiteDataObject list.");
        } else {
            Iterator<OgWebsiteDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgWebsiteDataObject dataObj = (OgWebsiteDataObject) it.next();
                list.add(new OgWebsiteBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<OgWebsite> getAllOgWebsites() throws BaseException
    {
        return getAllOgWebsites(null, null, null);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgWebsites(ordering, offset, count, null);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgWebsites(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgWebsite> list = new ArrayList<OgWebsite>();
        List<OgWebsiteDataObject> dataObjs = getDAOFactory().getOgWebsiteDAO().getAllOgWebsites(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgWebsiteDataObject list.");
        } else {
            Iterator<OgWebsiteDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgWebsiteDataObject dataObj = (OgWebsiteDataObject) it.next();
                list.add(new OgWebsiteBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgWebsiteKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllOgWebsiteKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgWebsiteDAO().getAllOgWebsiteKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgWebsite key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgWebsites(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgWebsiteServiceImpl.findOgWebsites(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgWebsite> list = new ArrayList<OgWebsite>();
        List<OgWebsiteDataObject> dataObjs = getDAOFactory().getOgWebsiteDAO().findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find ogWebsites for the given criterion.");
        } else {
            Iterator<OgWebsiteDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgWebsiteDataObject dataObj = (OgWebsiteDataObject) it.next();
                list.add(new OgWebsiteBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgWebsiteServiceImpl.findOgWebsiteKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgWebsiteDAO().findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgWebsite keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgWebsiteServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getOgWebsiteDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createOgWebsite(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        OgWebsiteDataObject dataObj = new OgWebsiteDataObject(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
        return createOgWebsite(dataObj);
    }

    @Override
    public String createOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogWebsite cannot be null.....
        if(ogWebsite == null) {
            log.log(Level.INFO, "Param ogWebsite is null!");
            throw new BadRequestException("Param ogWebsite object is null!");
        }
        OgWebsiteDataObject dataObj = null;
        if(ogWebsite instanceof OgWebsiteDataObject) {
            dataObj = (OgWebsiteDataObject) ogWebsite;
        } else if(ogWebsite instanceof OgWebsiteBean) {
            dataObj = ((OgWebsiteBean) ogWebsite).toDataObject();
        } else {  // if(ogWebsite instanceof OgWebsite)
            //dataObj = new OgWebsiteDataObject(null, ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new OgWebsiteDataObject(ogWebsite.getGuid(), ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote());
        }
        String guid = getDAOFactory().getOgWebsiteDAO().createOgWebsite(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgWebsite(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgWebsiteDataObject dataObj = new OgWebsiteDataObject(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
        return updateOgWebsite(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogWebsite cannot be null.....
        if(ogWebsite == null || ogWebsite.getGuid() == null) {
            log.log(Level.INFO, "Param ogWebsite or its guid is null!");
            throw new BadRequestException("Param ogWebsite object or its guid is null!");
        }
        OgWebsiteDataObject dataObj = null;
        if(ogWebsite instanceof OgWebsiteDataObject) {
            dataObj = (OgWebsiteDataObject) ogWebsite;
        } else if(ogWebsite instanceof OgWebsiteBean) {
            dataObj = ((OgWebsiteBean) ogWebsite).toDataObject();
        } else {  // if(ogWebsite instanceof OgWebsite)
            dataObj = new OgWebsiteDataObject(ogWebsite.getGuid(), ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote());
        }
        Boolean suc = getDAOFactory().getOgWebsiteDAO().updateOgWebsite(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteOgWebsite(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getOgWebsiteDAO().deleteOgWebsite(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteOgWebsite(OgWebsite ogWebsite) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogWebsite cannot be null.....
        if(ogWebsite == null || ogWebsite.getGuid() == null) {
            log.log(Level.INFO, "Param ogWebsite or its guid is null!");
            throw new BadRequestException("Param ogWebsite object or its guid is null!");
        }
        OgWebsiteDataObject dataObj = null;
        if(ogWebsite instanceof OgWebsiteDataObject) {
            dataObj = (OgWebsiteDataObject) ogWebsite;
        } else if(ogWebsite instanceof OgWebsiteBean) {
            dataObj = ((OgWebsiteBean) ogWebsite).toDataObject();
        } else {  // if(ogWebsite instanceof OgWebsite)
            dataObj = new OgWebsiteDataObject(ogWebsite.getGuid(), ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote());
        }
        Boolean suc = getDAOFactory().getOgWebsiteDAO().deleteOgWebsite(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteOgWebsites(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getOgWebsiteDAO().deleteOgWebsites(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
