package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class OpenGraphMetaDataObject extends PageBaseDataObject implements OpenGraphMeta
{
    private static final Logger log = Logger.getLogger(OpenGraphMetaDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(OpenGraphMetaDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(OpenGraphMetaDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String ogType;

    @Persistent(defaultFetchGroup = "false")
    private OgProfileDataObject ogProfile;

    @Persistent(defaultFetchGroup = "false")
    private OgWebsiteDataObject ogWebsite;

    @Persistent(defaultFetchGroup = "false")
    private OgBlogDataObject ogBlog;

    @Persistent(defaultFetchGroup = "false")
    private OgArticleDataObject ogArticle;

    @Persistent(defaultFetchGroup = "false")
    private OgBookDataObject ogBook;

    @Persistent(defaultFetchGroup = "false")
    private OgVideoDataObject ogVideo;

    @Persistent(defaultFetchGroup = "false")
    private OgMovieDataObject ogMovie;

    @Persistent(defaultFetchGroup = "false")
    private OgTvShowDataObject ogTvShow;

    @Persistent(defaultFetchGroup = "false")
    private OgTvEpisodeDataObject ogTvEpisode;

    public OpenGraphMetaDataObject()
    {
        this(null);
    }
    public OpenGraphMetaDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OpenGraphMetaDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfile, ogWebsite, ogBlog, ogArticle, ogBook, ogVideo, ogMovie, ogTvShow, ogTvEpisode, null, null);
    }
    public OpenGraphMetaDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);
        this.ogType = ogType;
        if(ogProfile != null) {
            this.ogProfile = new OgProfileDataObject(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender(), ogProfile.getCreatedTime(), ogProfile.getModifiedTime());
        } else {
            this.ogProfile = null;
        }
        if(ogWebsite != null) {
            this.ogWebsite = new OgWebsiteDataObject(ogWebsite.getGuid(), ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote(), ogWebsite.getCreatedTime(), ogWebsite.getModifiedTime());
        } else {
            this.ogWebsite = null;
        }
        if(ogBlog != null) {
            this.ogBlog = new OgBlogDataObject(ogBlog.getGuid(), ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote(), ogBlog.getCreatedTime(), ogBlog.getModifiedTime());
        } else {
            this.ogBlog = null;
        }
        if(ogArticle != null) {
            this.ogArticle = new OgArticleDataObject(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate(), ogArticle.getCreatedTime(), ogArticle.getModifiedTime());
        } else {
            this.ogArticle = null;
        }
        if(ogBook != null) {
            this.ogBook = new OgBookDataObject(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate(), ogBook.getCreatedTime(), ogBook.getModifiedTime());
        } else {
            this.ogBook = null;
        }
        if(ogVideo != null) {
            this.ogVideo = new OgVideoDataObject(ogVideo.getGuid(), ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate(), ogVideo.getCreatedTime(), ogVideo.getModifiedTime());
        } else {
            this.ogVideo = null;
        }
        if(ogMovie != null) {
            this.ogMovie = new OgMovieDataObject(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate(), ogMovie.getCreatedTime(), ogMovie.getModifiedTime());
        } else {
            this.ogMovie = null;
        }
        if(ogTvShow != null) {
            this.ogTvShow = new OgTvShowDataObject(ogTvShow.getGuid(), ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate(), ogTvShow.getCreatedTime(), ogTvShow.getModifiedTime());
        } else {
            this.ogTvShow = null;
        }
        if(ogTvEpisode != null) {
            this.ogTvEpisode = new OgTvEpisodeDataObject(ogTvEpisode.getGuid(), ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate(), ogTvEpisode.getCreatedTime(), ogTvEpisode.getModifiedTime());
        } else {
            this.ogTvEpisode = null;
        }
    }

//    @Override
//    protected Key createKey()
//    {
//        return OpenGraphMetaDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return OpenGraphMetaDataObject.composeKey(getGuid());
    }

    public String getOgType()
    {
        return this.ogType;
    }
    public void setOgType(String ogType)
    {
        this.ogType = ogType;
    }

    public OgProfile getOgProfile()
    {
        return this.ogProfile;
    }
    public void setOgProfile(OgProfile ogProfile)
    {
        if(ogProfile == null) {
            this.ogProfile = null;
            log.log(Level.INFO, "OpenGraphMetaDataObject.setOgProfile(OgProfile ogProfile): Arg ogProfile is null.");            
        } else if(ogProfile instanceof OgProfileDataObject) {
            this.ogProfile = (OgProfileDataObject) ogProfile;
        } else if(ogProfile instanceof OgProfile) {
            this.ogProfile = new OgProfileDataObject(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender(), ogProfile.getCreatedTime(), ogProfile.getModifiedTime());
        } else {
            this.ogProfile = new OgProfileDataObject();   // ????
            log.log(Level.WARNING, "OpenGraphMetaDataObject.setOgProfile(OgProfile ogProfile): Arg ogProfile is of an invalid type.");
        }
    }

    public OgWebsite getOgWebsite()
    {
        return this.ogWebsite;
    }
    public void setOgWebsite(OgWebsite ogWebsite)
    {
        if(ogWebsite == null) {
            this.ogWebsite = null;
            log.log(Level.INFO, "OpenGraphMetaDataObject.setOgWebsite(OgWebsite ogWebsite): Arg ogWebsite is null.");            
        } else if(ogWebsite instanceof OgWebsiteDataObject) {
            this.ogWebsite = (OgWebsiteDataObject) ogWebsite;
        } else if(ogWebsite instanceof OgWebsite) {
            this.ogWebsite = new OgWebsiteDataObject(ogWebsite.getGuid(), ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote(), ogWebsite.getCreatedTime(), ogWebsite.getModifiedTime());
        } else {
            this.ogWebsite = new OgWebsiteDataObject();   // ????
            log.log(Level.WARNING, "OpenGraphMetaDataObject.setOgWebsite(OgWebsite ogWebsite): Arg ogWebsite is of an invalid type.");
        }
    }

    public OgBlog getOgBlog()
    {
        return this.ogBlog;
    }
    public void setOgBlog(OgBlog ogBlog)
    {
        if(ogBlog == null) {
            this.ogBlog = null;
            log.log(Level.INFO, "OpenGraphMetaDataObject.setOgBlog(OgBlog ogBlog): Arg ogBlog is null.");            
        } else if(ogBlog instanceof OgBlogDataObject) {
            this.ogBlog = (OgBlogDataObject) ogBlog;
        } else if(ogBlog instanceof OgBlog) {
            this.ogBlog = new OgBlogDataObject(ogBlog.getGuid(), ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote(), ogBlog.getCreatedTime(), ogBlog.getModifiedTime());
        } else {
            this.ogBlog = new OgBlogDataObject();   // ????
            log.log(Level.WARNING, "OpenGraphMetaDataObject.setOgBlog(OgBlog ogBlog): Arg ogBlog is of an invalid type.");
        }
    }

    public OgArticle getOgArticle()
    {
        return this.ogArticle;
    }
    public void setOgArticle(OgArticle ogArticle)
    {
        if(ogArticle == null) {
            this.ogArticle = null;
            log.log(Level.INFO, "OpenGraphMetaDataObject.setOgArticle(OgArticle ogArticle): Arg ogArticle is null.");            
        } else if(ogArticle instanceof OgArticleDataObject) {
            this.ogArticle = (OgArticleDataObject) ogArticle;
        } else if(ogArticle instanceof OgArticle) {
            this.ogArticle = new OgArticleDataObject(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate(), ogArticle.getCreatedTime(), ogArticle.getModifiedTime());
        } else {
            this.ogArticle = new OgArticleDataObject();   // ????
            log.log(Level.WARNING, "OpenGraphMetaDataObject.setOgArticle(OgArticle ogArticle): Arg ogArticle is of an invalid type.");
        }
    }

    public OgBook getOgBook()
    {
        return this.ogBook;
    }
    public void setOgBook(OgBook ogBook)
    {
        if(ogBook == null) {
            this.ogBook = null;
            log.log(Level.INFO, "OpenGraphMetaDataObject.setOgBook(OgBook ogBook): Arg ogBook is null.");            
        } else if(ogBook instanceof OgBookDataObject) {
            this.ogBook = (OgBookDataObject) ogBook;
        } else if(ogBook instanceof OgBook) {
            this.ogBook = new OgBookDataObject(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate(), ogBook.getCreatedTime(), ogBook.getModifiedTime());
        } else {
            this.ogBook = new OgBookDataObject();   // ????
            log.log(Level.WARNING, "OpenGraphMetaDataObject.setOgBook(OgBook ogBook): Arg ogBook is of an invalid type.");
        }
    }

    public OgVideo getOgVideo()
    {
        return this.ogVideo;
    }
    public void setOgVideo(OgVideo ogVideo)
    {
        if(ogVideo == null) {
            this.ogVideo = null;
            log.log(Level.INFO, "OpenGraphMetaDataObject.setOgVideo(OgVideo ogVideo): Arg ogVideo is null.");            
        } else if(ogVideo instanceof OgVideoDataObject) {
            this.ogVideo = (OgVideoDataObject) ogVideo;
        } else if(ogVideo instanceof OgVideo) {
            this.ogVideo = new OgVideoDataObject(ogVideo.getGuid(), ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate(), ogVideo.getCreatedTime(), ogVideo.getModifiedTime());
        } else {
            this.ogVideo = new OgVideoDataObject();   // ????
            log.log(Level.WARNING, "OpenGraphMetaDataObject.setOgVideo(OgVideo ogVideo): Arg ogVideo is of an invalid type.");
        }
    }

    public OgMovie getOgMovie()
    {
        return this.ogMovie;
    }
    public void setOgMovie(OgMovie ogMovie)
    {
        if(ogMovie == null) {
            this.ogMovie = null;
            log.log(Level.INFO, "OpenGraphMetaDataObject.setOgMovie(OgMovie ogMovie): Arg ogMovie is null.");            
        } else if(ogMovie instanceof OgMovieDataObject) {
            this.ogMovie = (OgMovieDataObject) ogMovie;
        } else if(ogMovie instanceof OgMovie) {
            this.ogMovie = new OgMovieDataObject(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate(), ogMovie.getCreatedTime(), ogMovie.getModifiedTime());
        } else {
            this.ogMovie = new OgMovieDataObject();   // ????
            log.log(Level.WARNING, "OpenGraphMetaDataObject.setOgMovie(OgMovie ogMovie): Arg ogMovie is of an invalid type.");
        }
    }

    public OgTvShow getOgTvShow()
    {
        return this.ogTvShow;
    }
    public void setOgTvShow(OgTvShow ogTvShow)
    {
        if(ogTvShow == null) {
            this.ogTvShow = null;
            log.log(Level.INFO, "OpenGraphMetaDataObject.setOgTvShow(OgTvShow ogTvShow): Arg ogTvShow is null.");            
        } else if(ogTvShow instanceof OgTvShowDataObject) {
            this.ogTvShow = (OgTvShowDataObject) ogTvShow;
        } else if(ogTvShow instanceof OgTvShow) {
            this.ogTvShow = new OgTvShowDataObject(ogTvShow.getGuid(), ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate(), ogTvShow.getCreatedTime(), ogTvShow.getModifiedTime());
        } else {
            this.ogTvShow = new OgTvShowDataObject();   // ????
            log.log(Level.WARNING, "OpenGraphMetaDataObject.setOgTvShow(OgTvShow ogTvShow): Arg ogTvShow is of an invalid type.");
        }
    }

    public OgTvEpisode getOgTvEpisode()
    {
        return this.ogTvEpisode;
    }
    public void setOgTvEpisode(OgTvEpisode ogTvEpisode)
    {
        if(ogTvEpisode == null) {
            this.ogTvEpisode = null;
            log.log(Level.INFO, "OpenGraphMetaDataObject.setOgTvEpisode(OgTvEpisode ogTvEpisode): Arg ogTvEpisode is null.");            
        } else if(ogTvEpisode instanceof OgTvEpisodeDataObject) {
            this.ogTvEpisode = (OgTvEpisodeDataObject) ogTvEpisode;
        } else if(ogTvEpisode instanceof OgTvEpisode) {
            this.ogTvEpisode = new OgTvEpisodeDataObject(ogTvEpisode.getGuid(), ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate(), ogTvEpisode.getCreatedTime(), ogTvEpisode.getModifiedTime());
        } else {
            this.ogTvEpisode = new OgTvEpisodeDataObject();   // ????
            log.log(Level.WARNING, "OpenGraphMetaDataObject.setOgTvEpisode(OgTvEpisode ogTvEpisode): Arg ogTvEpisode is of an invalid type.");
        }
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("ogType", this.ogType);
        dataMap.put("ogProfile", this.ogProfile);
        dataMap.put("ogWebsite", this.ogWebsite);
        dataMap.put("ogBlog", this.ogBlog);
        dataMap.put("ogArticle", this.ogArticle);
        dataMap.put("ogBook", this.ogBook);
        dataMap.put("ogVideo", this.ogVideo);
        dataMap.put("ogMovie", this.ogMovie);
        dataMap.put("ogTvShow", this.ogTvShow);
        dataMap.put("ogTvEpisode", this.ogTvEpisode);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        OpenGraphMeta thatObj = (OpenGraphMeta) obj;
        if( (this.ogType == null && thatObj.getOgType() != null)
            || (this.ogType != null && thatObj.getOgType() == null)
            || !this.ogType.equals(thatObj.getOgType()) ) {
            return false;
        }
        if( (this.ogProfile == null && thatObj.getOgProfile() != null)
            || (this.ogProfile != null && thatObj.getOgProfile() == null)
            || !this.ogProfile.equals(thatObj.getOgProfile()) ) {
            return false;
        }
        if( (this.ogWebsite == null && thatObj.getOgWebsite() != null)
            || (this.ogWebsite != null && thatObj.getOgWebsite() == null)
            || !this.ogWebsite.equals(thatObj.getOgWebsite()) ) {
            return false;
        }
        if( (this.ogBlog == null && thatObj.getOgBlog() != null)
            || (this.ogBlog != null && thatObj.getOgBlog() == null)
            || !this.ogBlog.equals(thatObj.getOgBlog()) ) {
            return false;
        }
        if( (this.ogArticle == null && thatObj.getOgArticle() != null)
            || (this.ogArticle != null && thatObj.getOgArticle() == null)
            || !this.ogArticle.equals(thatObj.getOgArticle()) ) {
            return false;
        }
        if( (this.ogBook == null && thatObj.getOgBook() != null)
            || (this.ogBook != null && thatObj.getOgBook() == null)
            || !this.ogBook.equals(thatObj.getOgBook()) ) {
            return false;
        }
        if( (this.ogVideo == null && thatObj.getOgVideo() != null)
            || (this.ogVideo != null && thatObj.getOgVideo() == null)
            || !this.ogVideo.equals(thatObj.getOgVideo()) ) {
            return false;
        }
        if( (this.ogMovie == null && thatObj.getOgMovie() != null)
            || (this.ogMovie != null && thatObj.getOgMovie() == null)
            || !this.ogMovie.equals(thatObj.getOgMovie()) ) {
            return false;
        }
        if( (this.ogTvShow == null && thatObj.getOgTvShow() != null)
            || (this.ogTvShow != null && thatObj.getOgTvShow() == null)
            || !this.ogTvShow.equals(thatObj.getOgTvShow()) ) {
            return false;
        }
        if( (this.ogTvEpisode == null && thatObj.getOgTvEpisode() != null)
            || (this.ogTvEpisode != null && thatObj.getOgTvEpisode() == null)
            || !this.ogTvEpisode.equals(thatObj.getOgTvEpisode()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = ogType == null ? 0 : ogType.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogProfile == null ? 0 : ogProfile.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogWebsite == null ? 0 : ogWebsite.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogBlog == null ? 0 : ogBlog.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogArticle == null ? 0 : ogArticle.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogBook == null ? 0 : ogBook.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogVideo == null ? 0 : ogVideo.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogMovie == null ? 0 : ogMovie.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogTvShow == null ? 0 : ogTvShow.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogTvEpisode == null ? 0 : ogTvEpisode.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
