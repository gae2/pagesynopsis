package com.pagesynopsis.ws;



public interface RobotsTextRefresh 
{
    String  getGuid();
    String  getRobotsTextFile();
    Integer  getRefreshInterval();
    String  getNote();
    String  getStatus();
    Integer  getRefreshStatus();
    String  getResult();
    Long  getLastCheckedTime();
    Long  getNextCheckedTime();
    Long  getExpirationTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
