package com.pagesynopsis.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ResourceAlreadyPresentException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.DataStoreRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.ws.bean.DomainInfoBean;
import com.pagesynopsis.ws.stub.DomainInfoListStub;
import com.pagesynopsis.ws.stub.DomainInfoStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.DomainInfoResource;

// MockDomainInfoResource is a decorator.
// It can be used as a base class to mock DomainInfoResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/domainInfos/")
public abstract class MockDomainInfoResource implements DomainInfoResource
{
    private static final Logger log = Logger.getLogger(MockDomainInfoResource.class.getName());

    // MockDomainInfoResource uses the decorator design pattern.
    private DomainInfoResource decoratedResource;

    public MockDomainInfoResource(DomainInfoResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected DomainInfoResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(DomainInfoResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllDomainInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDomainInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllDomainInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDomainInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getDomainInfoKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getDomainInfoKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getDomainInfo(String guid) throws BaseResourceException
    {
        return decoratedResource.getDomainInfo(guid);
    }

    @Override
    public Response getDomainInfo(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getDomainInfo(guid, field);
    }

    @Override
    public Response createDomainInfo(DomainInfoStub domainInfo) throws BaseResourceException
    {
        return decoratedResource.createDomainInfo(domainInfo);
    }

    @Override
    public Response updateDomainInfo(String guid, DomainInfoStub domainInfo) throws BaseResourceException
    {
        return decoratedResource.updateDomainInfo(guid, domainInfo);
    }

    @Override
    public Response updateDomainInfo(String guid, String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws BaseResourceException
    {
        return decoratedResource.updateDomainInfo(guid, domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public Response deleteDomainInfo(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteDomainInfo(guid);
    }

    @Override
    public Response deleteDomainInfos(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteDomainInfos(filter, params, values);
    }


}
