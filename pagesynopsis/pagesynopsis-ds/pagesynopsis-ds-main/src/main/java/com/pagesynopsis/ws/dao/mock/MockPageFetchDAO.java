package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.PageFetchDAO;
import com.pagesynopsis.ws.data.PageFetchDataObject;


// MockPageFetchDAO is a decorator.
// It can be used as a base class to mock PageFetchDAO objects.
public abstract class MockPageFetchDAO implements PageFetchDAO
{
    private static final Logger log = Logger.getLogger(MockPageFetchDAO.class.getName()); 

    // MockPageFetchDAO uses the decorator design pattern.
    private PageFetchDAO decoratedDAO;

    public MockPageFetchDAO(PageFetchDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected PageFetchDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(PageFetchDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public PageFetchDataObject getPageFetch(String guid) throws BaseException
    {
        return decoratedDAO.getPageFetch(guid);
	}

    @Override
    public List<PageFetchDataObject> getPageFetches(List<String> guids) throws BaseException
    {
        return decoratedDAO.getPageFetches(guids);
    }

    @Override
    public List<PageFetchDataObject> getAllPageFetches() throws BaseException
	{
	    return getAllPageFetches(null, null, null);
    }


    @Override
    public List<PageFetchDataObject> getAllPageFetches(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllPageFetches(ordering, offset, count, null);
    }

    @Override
    public List<PageFetchDataObject> getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllPageFetches(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllPageFetchKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<PageFetchDataObject> findPageFetches(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findPageFetches(filter, ordering, params, values, null, null);
    }

    @Override
	public List<PageFetchDataObject> findPageFetches(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findPageFetches(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<PageFetchDataObject> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<PageFetchDataObject> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createPageFetch(PageFetchDataObject pageFetch) throws BaseException
    {
        return decoratedDAO.createPageFetch( pageFetch);
    }

    @Override
	public Boolean updatePageFetch(PageFetchDataObject pageFetch) throws BaseException
	{
        return decoratedDAO.updatePageFetch(pageFetch);
	}
	
    @Override
    public Boolean deletePageFetch(PageFetchDataObject pageFetch) throws BaseException
    {
        return decoratedDAO.deletePageFetch(pageFetch);
    }

    @Override
    public Boolean deletePageFetch(String guid) throws BaseException
    {
        return decoratedDAO.deletePageFetch(guid);
	}

    @Override
    public Long deletePageFetches(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deletePageFetches(filter, params, values);
    }

}
