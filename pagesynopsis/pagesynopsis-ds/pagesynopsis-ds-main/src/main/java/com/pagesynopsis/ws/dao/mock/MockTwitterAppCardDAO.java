package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.TwitterAppCardDAO;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;


// MockTwitterAppCardDAO is a decorator.
// It can be used as a base class to mock TwitterAppCardDAO objects.
public abstract class MockTwitterAppCardDAO implements TwitterAppCardDAO
{
    private static final Logger log = Logger.getLogger(MockTwitterAppCardDAO.class.getName()); 

    // MockTwitterAppCardDAO uses the decorator design pattern.
    private TwitterAppCardDAO decoratedDAO;

    public MockTwitterAppCardDAO(TwitterAppCardDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected TwitterAppCardDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(TwitterAppCardDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public TwitterAppCardDataObject getTwitterAppCard(String guid) throws BaseException
    {
        return decoratedDAO.getTwitterAppCard(guid);
	}

    @Override
    public List<TwitterAppCardDataObject> getTwitterAppCards(List<String> guids) throws BaseException
    {
        return decoratedDAO.getTwitterAppCards(guids);
    }

    @Override
    public List<TwitterAppCardDataObject> getAllTwitterAppCards() throws BaseException
	{
	    return getAllTwitterAppCards(null, null, null);
    }


    @Override
    public List<TwitterAppCardDataObject> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllTwitterAppCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterAppCardDataObject> getAllTwitterAppCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllTwitterAppCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterAppCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllTwitterAppCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<TwitterAppCardDataObject> findTwitterAppCards(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTwitterAppCards(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TwitterAppCardDataObject> findTwitterAppCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTwitterAppCards(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<TwitterAppCardDataObject> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<TwitterAppCardDataObject> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterAppCard(TwitterAppCardDataObject twitterAppCard) throws BaseException
    {
        return decoratedDAO.createTwitterAppCard( twitterAppCard);
    }

    @Override
	public Boolean updateTwitterAppCard(TwitterAppCardDataObject twitterAppCard) throws BaseException
	{
        return decoratedDAO.updateTwitterAppCard(twitterAppCard);
	}
	
    @Override
    public Boolean deleteTwitterAppCard(TwitterAppCardDataObject twitterAppCard) throws BaseException
    {
        return decoratedDAO.deleteTwitterAppCard(twitterAppCard);
    }

    @Override
    public Boolean deleteTwitterAppCard(String guid) throws BaseException
    {
        return decoratedDAO.deleteTwitterAppCard(guid);
	}

    @Override
    public Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteTwitterAppCards(filter, params, values);
    }

}
