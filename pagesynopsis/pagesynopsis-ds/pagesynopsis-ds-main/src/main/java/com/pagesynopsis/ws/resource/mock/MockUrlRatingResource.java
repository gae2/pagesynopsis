package com.pagesynopsis.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ResourceAlreadyPresentException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.DataStoreRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.ws.bean.UrlRatingBean;
import com.pagesynopsis.ws.stub.UrlRatingListStub;
import com.pagesynopsis.ws.stub.UrlRatingStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.UrlRatingResource;

// MockUrlRatingResource is a decorator.
// It can be used as a base class to mock UrlRatingResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/urlRatings/")
public abstract class MockUrlRatingResource implements UrlRatingResource
{
    private static final Logger log = Logger.getLogger(MockUrlRatingResource.class.getName());

    // MockUrlRatingResource uses the decorator design pattern.
    private UrlRatingResource decoratedResource;

    public MockUrlRatingResource(UrlRatingResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected UrlRatingResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(UrlRatingResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllUrlRatings(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUrlRatings(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllUrlRatingKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUrlRatingKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getUrlRatingKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getUrlRatingKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getUrlRating(String guid) throws BaseResourceException
    {
        return decoratedResource.getUrlRating(guid);
    }

    @Override
    public Response getUrlRating(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getUrlRating(guid, field);
    }

    @Override
    public Response createUrlRating(UrlRatingStub urlRating) throws BaseResourceException
    {
        return decoratedResource.createUrlRating(urlRating);
    }

    @Override
    public Response updateUrlRating(String guid, UrlRatingStub urlRating) throws BaseResourceException
    {
        return decoratedResource.updateUrlRating(guid, urlRating);
    }

    @Override
    public Response updateUrlRating(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseResourceException
    {
        return decoratedResource.updateUrlRating(guid, domain, pageUrl, preview, flag, rating, note, status, ratedTime);
    }

    @Override
    public Response deleteUrlRating(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteUrlRating(guid);
    }

    @Override
    public Response deleteUrlRatings(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteUrlRatings(filter, params, values);
    }


}
