package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.ws.bean.TwitterCardProductDataBean;
import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.TwitterCardAppInfoDataObject;
import com.pagesynopsis.ws.data.TwitterCardProductDataDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.TwitterAppCardService;


// TwitterAppCardMockService is a decorator.
// It can be used as a base class to mock TwitterAppCardService objects.
public abstract class TwitterAppCardMockService implements TwitterAppCardService
{
    private static final Logger log = Logger.getLogger(TwitterAppCardMockService.class.getName());

    // TwitterAppCardMockService uses the decorator design pattern.
    private TwitterAppCardService decoratedService;

    public TwitterAppCardMockService(TwitterAppCardService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected TwitterAppCardService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(TwitterAppCardService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // TwitterAppCard related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterAppCard getTwitterAppCard(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterAppCard(): guid = " + guid);
        TwitterAppCard bean = decoratedService.getTwitterAppCard(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterAppCard(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getTwitterAppCard(guid, field);
        return obj;
    }

    @Override
    public List<TwitterAppCard> getTwitterAppCards(List<String> guids) throws BaseException
    {
        log.fine("getTwitterAppCards()");
        List<TwitterAppCard> twitterAppCards = decoratedService.getTwitterAppCards(guids);
        log.finer("END");
        return twitterAppCards;
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards() throws BaseException
    {
        return getAllTwitterAppCards(null, null, null);
    }


    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterAppCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterAppCards(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<TwitterAppCard> twitterAppCards = decoratedService.getAllTwitterAppCards(ordering, offset, count, forwardCursor);
        log.finer("END");
        return twitterAppCards;
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterAppCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterAppCardKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllTwitterAppCardKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardMockService.findTwitterAppCards(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<TwitterAppCard> twitterAppCards = decoratedService.findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return twitterAppCards;
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardMockService.findTwitterAppCardKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterAppCardMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterAppCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        return decoratedService.createTwitterAppCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp);
    }

    @Override
    public String createTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createTwitterAppCard(twitterAppCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        return decoratedService.updateTwitterAppCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp);
    }
        
    @Override
    public Boolean updateTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateTwitterAppCard(twitterAppCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteTwitterAppCard(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterAppCard(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteTwitterAppCard(TwitterAppCard twitterAppCard) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTwitterAppCard(twitterAppCard);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteTwitterAppCards(filter, params, values);
        return count;
    }

}
