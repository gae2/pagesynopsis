package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.AudioSetDAO;
import com.pagesynopsis.ws.data.AudioSetDataObject;


// MockAudioSetDAO is a decorator.
// It can be used as a base class to mock AudioSetDAO objects.
public abstract class MockAudioSetDAO implements AudioSetDAO
{
    private static final Logger log = Logger.getLogger(MockAudioSetDAO.class.getName()); 

    // MockAudioSetDAO uses the decorator design pattern.
    private AudioSetDAO decoratedDAO;

    public MockAudioSetDAO(AudioSetDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected AudioSetDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(AudioSetDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public AudioSetDataObject getAudioSet(String guid) throws BaseException
    {
        return decoratedDAO.getAudioSet(guid);
	}

    @Override
    public List<AudioSetDataObject> getAudioSets(List<String> guids) throws BaseException
    {
        return decoratedDAO.getAudioSets(guids);
    }

    @Override
    public List<AudioSetDataObject> getAllAudioSets() throws BaseException
	{
	    return getAllAudioSets(null, null, null);
    }


    @Override
    public List<AudioSetDataObject> getAllAudioSets(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllAudioSets(ordering, offset, count, null);
    }

    @Override
    public List<AudioSetDataObject> getAllAudioSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllAudioSets(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAudioSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllAudioSetKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<AudioSetDataObject> findAudioSets(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findAudioSets(filter, ordering, params, values, null, null);
    }

    @Override
	public List<AudioSetDataObject> findAudioSets(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findAudioSets(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<AudioSetDataObject> findAudioSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findAudioSets(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<AudioSetDataObject> findAudioSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findAudioSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createAudioSet(AudioSetDataObject audioSet) throws BaseException
    {
        return decoratedDAO.createAudioSet( audioSet);
    }

    @Override
	public Boolean updateAudioSet(AudioSetDataObject audioSet) throws BaseException
	{
        return decoratedDAO.updateAudioSet(audioSet);
	}
	
    @Override
    public Boolean deleteAudioSet(AudioSetDataObject audioSet) throws BaseException
    {
        return decoratedDAO.deleteAudioSet(audioSet);
    }

    @Override
    public Boolean deleteAudioSet(String guid) throws BaseException
    {
        return decoratedDAO.deleteAudioSet(guid);
	}

    @Override
    public Long deleteAudioSets(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteAudioSets(filter, params, values);
    }

}
