package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.OpenGraphMetaDataObject;


// TBD: Add offset/count to getAllOpenGraphMetas() and findOpenGraphMetas(), etc.
public interface OpenGraphMetaDAO
{
    OpenGraphMetaDataObject getOpenGraphMeta(String guid) throws BaseException;
    List<OpenGraphMetaDataObject> getOpenGraphMetas(List<String> guids) throws BaseException;
    List<OpenGraphMetaDataObject> getAllOpenGraphMetas() throws BaseException;
    /* @Deprecated */ List<OpenGraphMetaDataObject> getAllOpenGraphMetas(String ordering, Long offset, Integer count) throws BaseException;
    List<OpenGraphMetaDataObject> getAllOpenGraphMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OpenGraphMetaDataObject> findOpenGraphMetas(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OpenGraphMetaDataObject> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<OpenGraphMetaDataObject> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OpenGraphMetaDataObject> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createOpenGraphMeta(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OpenGraphMetaDataObject?)
    String createOpenGraphMeta(OpenGraphMetaDataObject openGraphMeta) throws BaseException;          // Returns Guid.  (Return OpenGraphMetaDataObject?)
    //Boolean updateOpenGraphMeta(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOpenGraphMeta(OpenGraphMetaDataObject openGraphMeta) throws BaseException;
    Boolean deleteOpenGraphMeta(String guid) throws BaseException;
    Boolean deleteOpenGraphMeta(OpenGraphMetaDataObject openGraphMeta) throws BaseException;
    Long deleteOpenGraphMetas(String filter, String params, List<String> values) throws BaseException;
}
