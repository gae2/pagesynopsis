package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.UrlRatingDAO;
import com.pagesynopsis.ws.data.UrlRatingDataObject;


// MockUrlRatingDAO is a decorator.
// It can be used as a base class to mock UrlRatingDAO objects.
public abstract class MockUrlRatingDAO implements UrlRatingDAO
{
    private static final Logger log = Logger.getLogger(MockUrlRatingDAO.class.getName()); 

    // MockUrlRatingDAO uses the decorator design pattern.
    private UrlRatingDAO decoratedDAO;

    public MockUrlRatingDAO(UrlRatingDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected UrlRatingDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(UrlRatingDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public UrlRatingDataObject getUrlRating(String guid) throws BaseException
    {
        return decoratedDAO.getUrlRating(guid);
	}

    @Override
    public List<UrlRatingDataObject> getUrlRatings(List<String> guids) throws BaseException
    {
        return decoratedDAO.getUrlRatings(guids);
    }

    @Override
    public List<UrlRatingDataObject> getAllUrlRatings() throws BaseException
	{
	    return getAllUrlRatings(null, null, null);
    }


    @Override
    public List<UrlRatingDataObject> getAllUrlRatings(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllUrlRatings(ordering, offset, count, null);
    }

    @Override
    public List<UrlRatingDataObject> getAllUrlRatings(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllUrlRatings(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUrlRatingKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllUrlRatingKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<UrlRatingDataObject> findUrlRatings(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findUrlRatings(filter, ordering, params, values, null, null);
    }

    @Override
	public List<UrlRatingDataObject> findUrlRatings(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findUrlRatings(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<UrlRatingDataObject> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<UrlRatingDataObject> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createUrlRating(UrlRatingDataObject urlRating) throws BaseException
    {
        return decoratedDAO.createUrlRating( urlRating);
    }

    @Override
	public Boolean updateUrlRating(UrlRatingDataObject urlRating) throws BaseException
	{
        return decoratedDAO.updateUrlRating(urlRating);
	}
	
    @Override
    public Boolean deleteUrlRating(UrlRatingDataObject urlRating) throws BaseException
    {
        return decoratedDAO.deleteUrlRating(urlRating);
    }

    @Override
    public Boolean deleteUrlRating(String guid) throws BaseException
    {
        return decoratedDAO.deleteUrlRating(guid);
	}

    @Override
    public Long deleteUrlRatings(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteUrlRatings(filter, params, values);
    }

}
