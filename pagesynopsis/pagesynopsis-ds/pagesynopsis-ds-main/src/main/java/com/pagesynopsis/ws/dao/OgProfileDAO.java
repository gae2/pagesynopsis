package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.OgProfileDataObject;


// TBD: Add offset/count to getAllOgProfiles() and findOgProfiles(), etc.
public interface OgProfileDAO
{
    OgProfileDataObject getOgProfile(String guid) throws BaseException;
    List<OgProfileDataObject> getOgProfiles(List<String> guids) throws BaseException;
    List<OgProfileDataObject> getAllOgProfiles() throws BaseException;
    /* @Deprecated */ List<OgProfileDataObject> getAllOgProfiles(String ordering, Long offset, Integer count) throws BaseException;
    List<OgProfileDataObject> getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgProfileDataObject> findOgProfiles(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgProfileDataObject> findOgProfiles(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<OgProfileDataObject> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgProfileDataObject> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createOgProfile(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgProfileDataObject?)
    String createOgProfile(OgProfileDataObject ogProfile) throws BaseException;          // Returns Guid.  (Return OgProfileDataObject?)
    //Boolean updateOgProfile(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgProfile(OgProfileDataObject ogProfile) throws BaseException;
    Boolean deleteOgProfile(String guid) throws BaseException;
    Boolean deleteOgProfile(OgProfileDataObject ogProfile) throws BaseException;
    Long deleteOgProfiles(String filter, String params, List<String> values) throws BaseException;
}
