package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;


public interface OgTvShow extends OgObjectBase
{
    List<String>  getDirector();
    List<String>  getWriter();
    List<OgActorStruct>  getActor();
    Integer  getDuration();
    List<String>  getTag();
    String  getReleaseDate();
}
