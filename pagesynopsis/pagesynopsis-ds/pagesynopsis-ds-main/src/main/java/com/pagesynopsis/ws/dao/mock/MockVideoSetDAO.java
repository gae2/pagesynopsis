package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.VideoSetDAO;
import com.pagesynopsis.ws.data.VideoSetDataObject;


// MockVideoSetDAO is a decorator.
// It can be used as a base class to mock VideoSetDAO objects.
public abstract class MockVideoSetDAO implements VideoSetDAO
{
    private static final Logger log = Logger.getLogger(MockVideoSetDAO.class.getName()); 

    // MockVideoSetDAO uses the decorator design pattern.
    private VideoSetDAO decoratedDAO;

    public MockVideoSetDAO(VideoSetDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected VideoSetDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(VideoSetDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public VideoSetDataObject getVideoSet(String guid) throws BaseException
    {
        return decoratedDAO.getVideoSet(guid);
	}

    @Override
    public List<VideoSetDataObject> getVideoSets(List<String> guids) throws BaseException
    {
        return decoratedDAO.getVideoSets(guids);
    }

    @Override
    public List<VideoSetDataObject> getAllVideoSets() throws BaseException
	{
	    return getAllVideoSets(null, null, null);
    }


    @Override
    public List<VideoSetDataObject> getAllVideoSets(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllVideoSets(ordering, offset, count, null);
    }

    @Override
    public List<VideoSetDataObject> getAllVideoSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllVideoSets(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllVideoSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllVideoSetKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<VideoSetDataObject> findVideoSets(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findVideoSets(filter, ordering, params, values, null, null);
    }

    @Override
	public List<VideoSetDataObject> findVideoSets(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findVideoSets(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<VideoSetDataObject> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findVideoSets(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<VideoSetDataObject> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findVideoSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createVideoSet(VideoSetDataObject videoSet) throws BaseException
    {
        return decoratedDAO.createVideoSet( videoSet);
    }

    @Override
	public Boolean updateVideoSet(VideoSetDataObject videoSet) throws BaseException
	{
        return decoratedDAO.updateVideoSet(videoSet);
	}
	
    @Override
    public Boolean deleteVideoSet(VideoSetDataObject videoSet) throws BaseException
    {
        return decoratedDAO.deleteVideoSet(videoSet);
    }

    @Override
    public Boolean deleteVideoSet(String guid) throws BaseException
    {
        return decoratedDAO.deleteVideoSet(guid);
	}

    @Override
    public Long deleteVideoSets(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteVideoSets(filter, params, values);
    }

}
