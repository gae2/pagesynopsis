package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.bean.RobotsTextRefreshBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.RobotsTextRefreshDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.RobotsTextRefreshService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RobotsTextRefreshServiceImpl implements RobotsTextRefreshService
{
    private static final Logger log = Logger.getLogger(RobotsTextRefreshServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // RobotsTextRefresh related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public RobotsTextRefresh getRobotsTextRefresh(String guid) throws BaseException
    {
        log.finer("BEGIN");

        RobotsTextRefreshDataObject dataObj = getDAOFactory().getRobotsTextRefreshDAO().getRobotsTextRefresh(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RobotsTextRefreshDataObject for guid = " + guid);
            return null;  // ????
        }
        RobotsTextRefreshBean bean = new RobotsTextRefreshBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getRobotsTextRefresh(String guid, String field) throws BaseException
    {
        RobotsTextRefreshDataObject dataObj = getDAOFactory().getRobotsTextRefreshDAO().getRobotsTextRefresh(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RobotsTextRefreshDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("robotsTextFile")) {
            return dataObj.getRobotsTextFile();
        } else if(field.equals("refreshInterval")) {
            return dataObj.getRefreshInterval();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("refreshStatus")) {
            return dataObj.getRefreshStatus();
        } else if(field.equals("result")) {
            return dataObj.getResult();
        } else if(field.equals("lastCheckedTime")) {
            return dataObj.getLastCheckedTime();
        } else if(field.equals("nextCheckedTime")) {
            return dataObj.getNextCheckedTime();
        } else if(field.equals("expirationTime")) {
            return dataObj.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<RobotsTextRefresh> getRobotsTextRefreshes(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<RobotsTextRefresh> list = new ArrayList<RobotsTextRefresh>();
        List<RobotsTextRefreshDataObject> dataObjs = getDAOFactory().getRobotsTextRefreshDAO().getRobotsTextRefreshes(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextRefreshDataObject list.");
        } else {
            Iterator<RobotsTextRefreshDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                RobotsTextRefreshDataObject dataObj = (RobotsTextRefreshDataObject) it.next();
                list.add(new RobotsTextRefreshBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes() throws BaseException
    {
        return getAllRobotsTextRefreshes(null, null, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextRefreshes(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRobotsTextRefreshes(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<RobotsTextRefresh> list = new ArrayList<RobotsTextRefresh>();
        List<RobotsTextRefreshDataObject> dataObjs = getDAOFactory().getRobotsTextRefreshDAO().getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextRefreshDataObject list.");
        } else {
            Iterator<RobotsTextRefreshDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                RobotsTextRefreshDataObject dataObj = (RobotsTextRefreshDataObject) it.next();
                list.add(new RobotsTextRefreshBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextRefreshKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllRobotsTextRefreshKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getRobotsTextRefreshDAO().getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextRefresh key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("RobotsTextRefreshServiceImpl.findRobotsTextRefreshes(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<RobotsTextRefresh> list = new ArrayList<RobotsTextRefresh>();
        List<RobotsTextRefreshDataObject> dataObjs = getDAOFactory().getRobotsTextRefreshDAO().findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find robotsTextRefreshes for the given criterion.");
        } else {
            Iterator<RobotsTextRefreshDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                RobotsTextRefreshDataObject dataObj = (RobotsTextRefreshDataObject) it.next();
                list.add(new RobotsTextRefreshBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("RobotsTextRefreshServiceImpl.findRobotsTextRefreshKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getRobotsTextRefreshDAO().findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find RobotsTextRefresh keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("RobotsTextRefreshServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getRobotsTextRefreshDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createRobotsTextRefresh(String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        RobotsTextRefreshDataObject dataObj = new RobotsTextRefreshDataObject(null, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
        return createRobotsTextRefresh(dataObj);
    }

    @Override
    public String createRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextRefresh cannot be null.....
        if(robotsTextRefresh == null) {
            log.log(Level.INFO, "Param robotsTextRefresh is null!");
            throw new BadRequestException("Param robotsTextRefresh object is null!");
        }
        RobotsTextRefreshDataObject dataObj = null;
        if(robotsTextRefresh instanceof RobotsTextRefreshDataObject) {
            dataObj = (RobotsTextRefreshDataObject) robotsTextRefresh;
        } else if(robotsTextRefresh instanceof RobotsTextRefreshBean) {
            dataObj = ((RobotsTextRefreshBean) robotsTextRefresh).toDataObject();
        } else {  // if(robotsTextRefresh instanceof RobotsTextRefresh)
            //dataObj = new RobotsTextRefreshDataObject(null, robotsTextRefresh.getRobotsTextFile(), robotsTextRefresh.getRefreshInterval(), robotsTextRefresh.getNote(), robotsTextRefresh.getStatus(), robotsTextRefresh.getRefreshStatus(), robotsTextRefresh.getResult(), robotsTextRefresh.getLastCheckedTime(), robotsTextRefresh.getNextCheckedTime(), robotsTextRefresh.getExpirationTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new RobotsTextRefreshDataObject(robotsTextRefresh.getGuid(), robotsTextRefresh.getRobotsTextFile(), robotsTextRefresh.getRefreshInterval(), robotsTextRefresh.getNote(), robotsTextRefresh.getStatus(), robotsTextRefresh.getRefreshStatus(), robotsTextRefresh.getResult(), robotsTextRefresh.getLastCheckedTime(), robotsTextRefresh.getNextCheckedTime(), robotsTextRefresh.getExpirationTime());
        }
        String guid = getDAOFactory().getRobotsTextRefreshDAO().createRobotsTextRefresh(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateRobotsTextRefresh(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        RobotsTextRefreshDataObject dataObj = new RobotsTextRefreshDataObject(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
        return updateRobotsTextRefresh(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextRefresh cannot be null.....
        if(robotsTextRefresh == null || robotsTextRefresh.getGuid() == null) {
            log.log(Level.INFO, "Param robotsTextRefresh or its guid is null!");
            throw new BadRequestException("Param robotsTextRefresh object or its guid is null!");
        }
        RobotsTextRefreshDataObject dataObj = null;
        if(robotsTextRefresh instanceof RobotsTextRefreshDataObject) {
            dataObj = (RobotsTextRefreshDataObject) robotsTextRefresh;
        } else if(robotsTextRefresh instanceof RobotsTextRefreshBean) {
            dataObj = ((RobotsTextRefreshBean) robotsTextRefresh).toDataObject();
        } else {  // if(robotsTextRefresh instanceof RobotsTextRefresh)
            dataObj = new RobotsTextRefreshDataObject(robotsTextRefresh.getGuid(), robotsTextRefresh.getRobotsTextFile(), robotsTextRefresh.getRefreshInterval(), robotsTextRefresh.getNote(), robotsTextRefresh.getStatus(), robotsTextRefresh.getRefreshStatus(), robotsTextRefresh.getResult(), robotsTextRefresh.getLastCheckedTime(), robotsTextRefresh.getNextCheckedTime(), robotsTextRefresh.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getRobotsTextRefreshDAO().updateRobotsTextRefresh(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteRobotsTextRefresh(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getRobotsTextRefreshDAO().deleteRobotsTextRefresh(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextRefresh cannot be null.....
        if(robotsTextRefresh == null || robotsTextRefresh.getGuid() == null) {
            log.log(Level.INFO, "Param robotsTextRefresh or its guid is null!");
            throw new BadRequestException("Param robotsTextRefresh object or its guid is null!");
        }
        RobotsTextRefreshDataObject dataObj = null;
        if(robotsTextRefresh instanceof RobotsTextRefreshDataObject) {
            dataObj = (RobotsTextRefreshDataObject) robotsTextRefresh;
        } else if(robotsTextRefresh instanceof RobotsTextRefreshBean) {
            dataObj = ((RobotsTextRefreshBean) robotsTextRefresh).toDataObject();
        } else {  // if(robotsTextRefresh instanceof RobotsTextRefresh)
            dataObj = new RobotsTextRefreshDataObject(robotsTextRefresh.getGuid(), robotsTextRefresh.getRobotsTextFile(), robotsTextRefresh.getRefreshInterval(), robotsTextRefresh.getNote(), robotsTextRefresh.getStatus(), robotsTextRefresh.getRefreshStatus(), robotsTextRefresh.getResult(), robotsTextRefresh.getLastCheckedTime(), robotsTextRefresh.getNextCheckedTime(), robotsTextRefresh.getExpirationTime());
        }
        Boolean suc = getDAOFactory().getRobotsTextRefreshDAO().deleteRobotsTextRefresh(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getRobotsTextRefreshDAO().deleteRobotsTextRefreshes(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
