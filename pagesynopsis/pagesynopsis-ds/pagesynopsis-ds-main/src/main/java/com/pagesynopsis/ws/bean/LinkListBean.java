package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.LinkListDataObject;

public class LinkListBean extends PageBaseBean implements LinkList
{
    private static final Logger log = Logger.getLogger(LinkListBean.class.getName());

    // Embedded data object.
    private LinkListDataObject dobj = null;

    public LinkListBean()
    {
        this(new LinkListDataObject());
    }
    public LinkListBean(String guid)
    {
        this(new LinkListDataObject(guid));
    }
    public LinkListBean(LinkListDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public LinkListDataObject getDataObject()
    {
        return this.dobj;
    }

    public List<String> getUrlSchemeFilter()
    {
        if(getDataObject() != null) {
            return getDataObject().getUrlSchemeFilter();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkListDataObject is null!");
            return null;   // ???
        }
    }
    public void setUrlSchemeFilter(List<String> urlSchemeFilter)
    {
        if(getDataObject() != null) {
            getDataObject().setUrlSchemeFilter(urlSchemeFilter);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkListDataObject is null!");
        }
    }

    public List<AnchorStruct> getPageAnchors()
    {
        if(getDataObject() != null) {
            List<AnchorStruct> list = getDataObject().getPageAnchors();
            if(list != null) {
                List<AnchorStruct> bean = new ArrayList<AnchorStruct>();
                for(AnchorStruct anchorStruct : list) {
                    AnchorStructBean elem = null;
                    if(anchorStruct instanceof AnchorStructBean) {
                        elem = (AnchorStructBean) anchorStruct;
                    } else if(anchorStruct instanceof AnchorStructDataObject) {
                        elem = new AnchorStructBean((AnchorStructDataObject) anchorStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkListDataObject is null!");
            return null;
        }
    }
    public void setPageAnchors(List<AnchorStruct> pageAnchors)
    {
        if(pageAnchors != null) {
            if(getDataObject() != null) {
                List<AnchorStruct> dataObj = new ArrayList<AnchorStruct>();
                for(AnchorStruct anchorStruct : pageAnchors) {
                    AnchorStructDataObject elem = null;
                    if(anchorStruct instanceof AnchorStructBean) {
                        elem = ((AnchorStructBean) anchorStruct).toDataObject();
                    } else if(anchorStruct instanceof AnchorStructDataObject) {
                        elem = (AnchorStructDataObject) anchorStruct;
                    } else if(anchorStruct instanceof AnchorStruct) {
                        elem = new AnchorStructDataObject(anchorStruct.getUuid(), anchorStruct.getId(), anchorStruct.getName(), anchorStruct.getHref(), anchorStruct.getHrefUrl(), anchorStruct.getUrlScheme(), anchorStruct.getRel(), anchorStruct.getTarget(), anchorStruct.getInnerHtml(), anchorStruct.getAnchorText(), anchorStruct.getAnchorTitle(), anchorStruct.getAnchorImage(), anchorStruct.getAnchorLegend(), anchorStruct.getNote());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setPageAnchors(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded LinkListDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setPageAnchors(pageAnchors);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded LinkListDataObject is null!");
            }
        }
    }

    public Boolean isExcludeRelativeUrls()
    {
        if(getDataObject() != null) {
            return getDataObject().isExcludeRelativeUrls();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkListDataObject is null!");
            return null;   // ???
        }
    }
    public void setExcludeRelativeUrls(Boolean excludeRelativeUrls)
    {
        if(getDataObject() != null) {
            getDataObject().setExcludeRelativeUrls(excludeRelativeUrls);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkListDataObject is null!");
        }
    }

    public List<String> getExcludedBaseUrls()
    {
        if(getDataObject() != null) {
            return getDataObject().getExcludedBaseUrls();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkListDataObject is null!");
            return null;   // ???
        }
    }
    public void setExcludedBaseUrls(List<String> excludedBaseUrls)
    {
        if(getDataObject() != null) {
            getDataObject().setExcludedBaseUrls(excludedBaseUrls);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded LinkListDataObject is null!");
        }
    }


    // TBD
    public LinkListDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
