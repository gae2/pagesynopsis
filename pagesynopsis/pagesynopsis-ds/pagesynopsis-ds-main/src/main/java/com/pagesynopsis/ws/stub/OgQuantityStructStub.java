package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.OgQuantityStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogQuantityStruct")
@XmlType(propOrder = {"uuid", "value", "units"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgQuantityStructStub implements OgQuantityStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgQuantityStructStub.class.getName());

    private String uuid;
    private Float value;
    private String units;

    public OgQuantityStructStub()
    {
        this(null);
    }
    public OgQuantityStructStub(OgQuantityStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.value = bean.getValue();
            this.units = bean.getUnits();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public Float getValue()
    {
        return this.value;
    }
    public void setValue(Float value)
    {
        this.value = value;
    }

    @XmlElement
    public String getUnits()
    {
        return this.units;
    }
    public void setUnits(String units)
    {
        this.units = units;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getValue() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUnits() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("value", this.value);
        dataMap.put("units", this.units);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = value == null ? 0 : value.hashCode();
        _hash = 31 * _hash + delta;
        delta = units == null ? 0 : units.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static OgQuantityStructStub convertBeanToStub(OgQuantityStruct bean)
    {
        OgQuantityStructStub stub = null;
        if(bean instanceof OgQuantityStructStub) {
            stub = (OgQuantityStructStub) bean;
        } else {
            if(bean != null) {
                stub = new OgQuantityStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgQuantityStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of OgQuantityStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgQuantityStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgQuantityStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgQuantityStructStub object as a string.", e);
        }
        
        return null;
    }
    public static OgQuantityStructStub fromJsonString(String jsonStr)
    {
        try {
            OgQuantityStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgQuantityStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgQuantityStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgQuantityStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgQuantityStructStub object.", e);
        }
        
        return null;
    }

}
