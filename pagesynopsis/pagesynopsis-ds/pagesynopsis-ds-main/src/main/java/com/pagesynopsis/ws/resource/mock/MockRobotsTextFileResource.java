package com.pagesynopsis.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ResourceAlreadyPresentException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.DataStoreRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.bean.RobotsTextFileBean;
import com.pagesynopsis.ws.stub.RobotsTextFileListStub;
import com.pagesynopsis.ws.stub.RobotsTextFileStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.RobotsTextFileResource;
import com.pagesynopsis.ws.resource.util.RobotsTextGroupResourceUtil;

// MockRobotsTextFileResource is a decorator.
// It can be used as a base class to mock RobotsTextFileResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/robotsTextFiles/")
public abstract class MockRobotsTextFileResource implements RobotsTextFileResource
{
    private static final Logger log = Logger.getLogger(MockRobotsTextFileResource.class.getName());

    // MockRobotsTextFileResource uses the decorator design pattern.
    private RobotsTextFileResource decoratedResource;

    public MockRobotsTextFileResource(RobotsTextFileResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected RobotsTextFileResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(RobotsTextFileResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllRobotsTextFiles(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllRobotsTextFileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getRobotsTextFileKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getRobotsTextFileKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getRobotsTextFile(String guid) throws BaseResourceException
    {
        return decoratedResource.getRobotsTextFile(guid);
    }

    @Override
    public Response getRobotsTextFile(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getRobotsTextFile(guid, field);
    }

    @Override
    public Response createRobotsTextFile(RobotsTextFileStub robotsTextFile) throws BaseResourceException
    {
        return decoratedResource.createRobotsTextFile(robotsTextFile);
    }

    @Override
    public Response updateRobotsTextFile(String guid, RobotsTextFileStub robotsTextFile) throws BaseResourceException
    {
        return decoratedResource.updateRobotsTextFile(guid, robotsTextFile);
    }

    @Override
    public Response updateRobotsTextFile(String guid, String siteUrl, List<String> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseResourceException
    {
        return decoratedResource.updateRobotsTextFile(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public Response deleteRobotsTextFile(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteRobotsTextFile(guid);
    }

    @Override
    public Response deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteRobotsTextFiles(filter, params, values);
    }


}
