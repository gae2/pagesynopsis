package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.AppBrandStruct;
import com.pagesynopsis.ws.bean.AppBrandStructBean;
import com.pagesynopsis.ws.stub.AppBrandStructStub;


public class AppBrandStructResourceUtil
{
    private static final Logger log = Logger.getLogger(AppBrandStructResourceUtil.class.getName());

    // Static methods only.
    private AppBrandStructResourceUtil() {}

    public static AppBrandStructBean convertAppBrandStructStubToBean(AppBrandStruct stub)
    {
        AppBrandStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null AppBrandStructBean is returned.");
        } else {
            bean = new AppBrandStructBean();
            bean.setBrand(stub.getBrand());
            bean.setName(stub.getName());
            bean.setDescription(stub.getDescription());
        }
        return bean;
    }

}
