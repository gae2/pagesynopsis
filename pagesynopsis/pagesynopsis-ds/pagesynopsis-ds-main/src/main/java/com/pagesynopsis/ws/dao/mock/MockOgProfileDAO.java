package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OgProfileDAO;
import com.pagesynopsis.ws.data.OgProfileDataObject;


// MockOgProfileDAO is a decorator.
// It can be used as a base class to mock OgProfileDAO objects.
public abstract class MockOgProfileDAO implements OgProfileDAO
{
    private static final Logger log = Logger.getLogger(MockOgProfileDAO.class.getName()); 

    // MockOgProfileDAO uses the decorator design pattern.
    private OgProfileDAO decoratedDAO;

    public MockOgProfileDAO(OgProfileDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected OgProfileDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(OgProfileDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public OgProfileDataObject getOgProfile(String guid) throws BaseException
    {
        return decoratedDAO.getOgProfile(guid);
	}

    @Override
    public List<OgProfileDataObject> getOgProfiles(List<String> guids) throws BaseException
    {
        return decoratedDAO.getOgProfiles(guids);
    }

    @Override
    public List<OgProfileDataObject> getAllOgProfiles() throws BaseException
	{
	    return getAllOgProfiles(null, null, null);
    }


    @Override
    public List<OgProfileDataObject> getAllOgProfiles(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOgProfiles(ordering, offset, count, null);
    }

    @Override
    public List<OgProfileDataObject> getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllOgProfiles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgProfileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllOgProfileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<OgProfileDataObject> findOgProfiles(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOgProfiles(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OgProfileDataObject> findOgProfiles(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOgProfiles(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OgProfileDataObject> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<OgProfileDataObject> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgProfile(OgProfileDataObject ogProfile) throws BaseException
    {
        return decoratedDAO.createOgProfile( ogProfile);
    }

    @Override
	public Boolean updateOgProfile(OgProfileDataObject ogProfile) throws BaseException
	{
        return decoratedDAO.updateOgProfile(ogProfile);
	}
	
    @Override
    public Boolean deleteOgProfile(OgProfileDataObject ogProfile) throws BaseException
    {
        return decoratedDAO.deleteOgProfile(ogProfile);
    }

    @Override
    public Boolean deleteOgProfile(String guid) throws BaseException
    {
        return decoratedDAO.deleteOgProfile(guid);
	}

    @Override
    public Long deleteOgProfiles(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteOgProfiles(filter, params, values);
    }

}
