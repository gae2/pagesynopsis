package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;

public class OgBookBean extends OgObjectBaseBean implements OgBook
{
    private static final Logger log = Logger.getLogger(OgBookBean.class.getName());

    // Embedded data object.
    private OgBookDataObject dobj = null;

    public OgBookBean()
    {
        this(new OgBookDataObject());
    }
    public OgBookBean(String guid)
    {
        this(new OgBookDataObject(guid));
    }
    public OgBookBean(OgBookDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public OgBookDataObject getDataObject()
    {
        return this.dobj;
    }

    public List<String> getAuthor()
    {
        if(getDataObject() != null) {
            return getDataObject().getAuthor();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgBookDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthor(List<String> author)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthor(author);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgBookDataObject is null!");
        }
    }

    public String getIsbn()
    {
        if(getDataObject() != null) {
            return getDataObject().getIsbn();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgBookDataObject is null!");
            return null;   // ???
        }
    }
    public void setIsbn(String isbn)
    {
        if(getDataObject() != null) {
            getDataObject().setIsbn(isbn);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgBookDataObject is null!");
        }
    }

    public List<String> getTag()
    {
        if(getDataObject() != null) {
            return getDataObject().getTag();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgBookDataObject is null!");
            return null;   // ???
        }
    }
    public void setTag(List<String> tag)
    {
        if(getDataObject() != null) {
            getDataObject().setTag(tag);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgBookDataObject is null!");
        }
    }

    public String getReleaseDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getReleaseDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgBookDataObject is null!");
            return null;   // ???
        }
    }
    public void setReleaseDate(String releaseDate)
    {
        if(getDataObject() != null) {
            getDataObject().setReleaseDate(releaseDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgBookDataObject is null!");
        }
    }


    // TBD
    public OgBookDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
