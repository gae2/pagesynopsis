package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.TwitterPlayerCardDAO;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;


// MockTwitterPlayerCardDAO is a decorator.
// It can be used as a base class to mock TwitterPlayerCardDAO objects.
public abstract class MockTwitterPlayerCardDAO implements TwitterPlayerCardDAO
{
    private static final Logger log = Logger.getLogger(MockTwitterPlayerCardDAO.class.getName()); 

    // MockTwitterPlayerCardDAO uses the decorator design pattern.
    private TwitterPlayerCardDAO decoratedDAO;

    public MockTwitterPlayerCardDAO(TwitterPlayerCardDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected TwitterPlayerCardDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(TwitterPlayerCardDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public TwitterPlayerCardDataObject getTwitterPlayerCard(String guid) throws BaseException
    {
        return decoratedDAO.getTwitterPlayerCard(guid);
	}

    @Override
    public List<TwitterPlayerCardDataObject> getTwitterPlayerCards(List<String> guids) throws BaseException
    {
        return decoratedDAO.getTwitterPlayerCards(guids);
    }

    @Override
    public List<TwitterPlayerCardDataObject> getAllTwitterPlayerCards() throws BaseException
	{
	    return getAllTwitterPlayerCards(null, null, null);
    }


    @Override
    public List<TwitterPlayerCardDataObject> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllTwitterPlayerCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPlayerCardDataObject> getAllTwitterPlayerCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllTwitterPlayerCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPlayerCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllTwitterPlayerCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<TwitterPlayerCardDataObject> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTwitterPlayerCards(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TwitterPlayerCardDataObject> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTwitterPlayerCards(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<TwitterPlayerCardDataObject> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<TwitterPlayerCardDataObject> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterPlayerCard(TwitterPlayerCardDataObject twitterPlayerCard) throws BaseException
    {
        return decoratedDAO.createTwitterPlayerCard( twitterPlayerCard);
    }

    @Override
	public Boolean updateTwitterPlayerCard(TwitterPlayerCardDataObject twitterPlayerCard) throws BaseException
	{
        return decoratedDAO.updateTwitterPlayerCard(twitterPlayerCard);
	}
	
    @Override
    public Boolean deleteTwitterPlayerCard(TwitterPlayerCardDataObject twitterPlayerCard) throws BaseException
    {
        return decoratedDAO.deleteTwitterPlayerCard(twitterPlayerCard);
    }

    @Override
    public Boolean deleteTwitterPlayerCard(String guid) throws BaseException
    {
        return decoratedDAO.deleteTwitterPlayerCard(guid);
	}

    @Override
    public Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteTwitterPlayerCards(filter, params, values);
    }

}
