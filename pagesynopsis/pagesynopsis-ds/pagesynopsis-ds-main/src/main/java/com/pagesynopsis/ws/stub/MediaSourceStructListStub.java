package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "mediaSourceStructs")
@XmlType(propOrder = {"mediaSourceStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class MediaSourceStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(MediaSourceStructListStub.class.getName());

    private List<MediaSourceStructStub> mediaSourceStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public MediaSourceStructListStub()
    {
        this(new ArrayList<MediaSourceStructStub>());
    }
    public MediaSourceStructListStub(List<MediaSourceStructStub> mediaSourceStructs)
    {
        this(mediaSourceStructs, null);
    }
    public MediaSourceStructListStub(List<MediaSourceStructStub> mediaSourceStructs, String forwardCursor)
    {
        this.mediaSourceStructs = mediaSourceStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(mediaSourceStructs == null) {
            return true;
        } else {
            return mediaSourceStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(mediaSourceStructs == null) {
            return 0;
        } else {
            return mediaSourceStructs.size();
        }
    }


    @XmlElement(name = "mediaSourceStruct")
    public List<MediaSourceStructStub> getMediaSourceStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<MediaSourceStructStub> getList()
    {
        return mediaSourceStructs;
    }
    public void setList(List<MediaSourceStructStub> mediaSourceStructs)
    {
        this.mediaSourceStructs = mediaSourceStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<MediaSourceStructStub> it = this.mediaSourceStructs.iterator();
        while(it.hasNext()) {
            MediaSourceStructStub mediaSourceStruct = it.next();
            sb.append(mediaSourceStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static MediaSourceStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of MediaSourceStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write MediaSourceStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write MediaSourceStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write MediaSourceStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static MediaSourceStructListStub fromJsonString(String jsonStr)
    {
        try {
            MediaSourceStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, MediaSourceStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into MediaSourceStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into MediaSourceStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into MediaSourceStructListStub object.", e);
        }
        
        return null;
    }

}
