package com.pagesynopsis.ws.fixture;

import java.io.File;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.ApiConsumer;
import com.pagesynopsis.ws.User;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.ws.ServiceInfo;
import com.pagesynopsis.ws.FiveTen;
import com.pagesynopsis.ws.BaseException;


// Note:
// Fixture file name conventions.
// File path may in the form of "/a/b/c/[Type]Yyy.[suf]"
// The [Type] part determins the object type (class),
// whereas the file suffix [suf] determines the media type, xml or json.
// ...
public class FixtureUtil
{
    private static final Logger log = Logger.getLogger(FixtureUtil.class.getName());

    // temporary
    public static final String MEDIA_TYPE_XML = "XML";
    public static final String MEDIA_TYPE_JSON = "JSON";
    // ...
    private static final String DEFAULT_MEDIA_TYPE = MEDIA_TYPE_JSON;
    // ...
    // Fixture dir is, in the current implementation, under "/war" dir. 
    // Using "/war/WEB-INF/classes" (e.g., copied from "src" dir during build) is more secure,
    // but it requires using a class loader (whereas we can just use File API for files under "/war").
    private static final String DEFAULT_FIXTURE_DIR = "fixture";
    private static final String DEFAULT_FIXTURE_FILE = DEFAULT_FIXTURE_DIR + File.separator + "fixture.json";   // ????
    // ...
    private static final boolean DEFAULT_FIXTURE_LOAD = false;
    
    
    private FixtureUtil() {}

    
    public static boolean getDefaultFixtureLoad()
    {
        return DEFAULT_FIXTURE_LOAD;
    }

//    public static String getDefaultObjectType()
//    {
//        // ????
//        return null;
//    }
    
    public static String getDefaultMediaType()
    {
        // ????
        return DEFAULT_MEDIA_TYPE;
    }

    public static String getMediaType(String filePath)
    {
        if(filePath == null || filePath.isEmpty()) {
            return getDefaultMediaType();  // ???
        }
        int idx = filePath.lastIndexOf(".");
        if(idx >= 0) {
            String suffix = filePath.substring(idx + 1);
            if(suffix.equalsIgnoreCase(MEDIA_TYPE_XML)) {
                return MEDIA_TYPE_XML;
            } else if(suffix.equalsIgnoreCase(MEDIA_TYPE_JSON)) {
                return MEDIA_TYPE_JSON;
            } else {
                return getDefaultMediaType();  // ???
            }
        } else {
            return getDefaultMediaType();  // ???            
        }
    }

    // Returns the class name..
    // TBD: Or, return Class ????
    public static String getObjectType(String filePath)
    {
        if(filePath == null || filePath.isEmpty()) {
            return null;   // ???
        }
        int idx1 = filePath.lastIndexOf(File.separator);
        if(idx1 >= 0) {
            filePath = filePath.substring(idx1 + 1);
        }
        int idx2 = filePath.lastIndexOf(".");
        if(idx2 >= 0) {
            filePath = filePath.substring(0, idx2);
        }
        // At this point,
        // "filePath" refers to the root part of the file name. Just reusing the name filePath...

        // temporary
        // TBD: Type names should be sorted and longer type names should be put in front... 
        if(filePath.toLowerCase().startsWith("ApiConsumer".toLowerCase())) {
            String className = ApiConsumer.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ApiConsumer".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("User".toLowerCase())) {
            String className = User.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("User".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("RobotsTextFile".toLowerCase())) {
            String className = RobotsTextFile.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("RobotsTextFile".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("RobotsTextRefresh".toLowerCase())) {
            String className = RobotsTextRefresh.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("RobotsTextRefresh".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("OgProfile".toLowerCase())) {
            String className = OgProfile.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("OgProfile".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("OgWebsite".toLowerCase())) {
            String className = OgWebsite.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("OgWebsite".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("OgBlog".toLowerCase())) {
            String className = OgBlog.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("OgBlog".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("OgArticle".toLowerCase())) {
            String className = OgArticle.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("OgArticle".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("OgBook".toLowerCase())) {
            String className = OgBook.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("OgBook".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("OgVideo".toLowerCase())) {
            String className = OgVideo.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("OgVideo".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("OgMovie".toLowerCase())) {
            String className = OgMovie.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("OgMovie".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("OgTvShow".toLowerCase())) {
            String className = OgTvShow.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("OgTvShow".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("OgTvEpisode".toLowerCase())) {
            String className = OgTvEpisode.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("OgTvEpisode".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterSummaryCard".toLowerCase())) {
            String className = TwitterSummaryCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterSummaryCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterPhotoCard".toLowerCase())) {
            String className = TwitterPhotoCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterPhotoCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterGalleryCard".toLowerCase())) {
            String className = TwitterGalleryCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterGalleryCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterAppCard".toLowerCase())) {
            String className = TwitterAppCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterAppCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterPlayerCard".toLowerCase())) {
            String className = TwitterPlayerCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterPlayerCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterProductCard".toLowerCase())) {
            String className = TwitterProductCard.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterProductCard".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("FetchRequest".toLowerCase())) {
            String className = FetchRequest.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("FetchRequest".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("PageInfo".toLowerCase())) {
            String className = PageInfo.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("PageInfo".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("PageFetch".toLowerCase())) {
            String className = PageFetch.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("PageFetch".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("LinkList".toLowerCase())) {
            String className = LinkList.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("LinkList".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("ImageSet".toLowerCase())) {
            String className = ImageSet.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ImageSet".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("AudioSet".toLowerCase())) {
            String className = AudioSet.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("AudioSet".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("VideoSet".toLowerCase())) {
            String className = VideoSet.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("VideoSet".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("OpenGraphMeta".toLowerCase())) {
            String className = OpenGraphMeta.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("OpenGraphMeta".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("TwitterCardMeta".toLowerCase())) {
            String className = TwitterCardMeta.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("TwitterCardMeta".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("DomainInfo".toLowerCase())) {
            String className = DomainInfo.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("DomainInfo".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("UrlRating".toLowerCase())) {
            String className = UrlRating.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("UrlRating".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("ServiceInfo".toLowerCase())) {
            String className = ServiceInfo.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("ServiceInfo".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        if(filePath.toLowerCase().startsWith("FiveTen".toLowerCase())) {
            String className = FiveTen.class.getSimpleName();
            if(filePath.toLowerCase().startsWith("FiveTen".toLowerCase() + "list")) {
               return className + "List";
            } else {
                return className;
            }
        }
        
        //...
        return null;        
    }

    // TBD
    public static String getDefaultFixtureDir()
    {
        // temporary
        return DEFAULT_FIXTURE_DIR;
    }
    public static String getDefaultFixtureFile()
    {
        // temporary
        return DEFAULT_FIXTURE_FILE;
    }

}
