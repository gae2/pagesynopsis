package com.pagesynopsis.ws;



public interface OgActorStruct 
{
    String  getUuid();
    String  getProfile();
    String  getRole();
    boolean isEmpty();
}
