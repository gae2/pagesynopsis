package com.pagesynopsis.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.stub.ErrorStub;

@Provider
public class TemporaryRedirectExceptionMapper implements ExceptionMapper<TemporaryRedirectRsException>
{
    public Response toResponse(TemporaryRedirectRsException ex) {
        return Response.status(StatusCode.TEMPORARY_REDIRECT)
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
