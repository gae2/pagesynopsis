package com.pagesynopsis.ws;



public interface DomainInfo 
{
    String  getGuid();
    String  getDomain();
    Boolean  isExcluded();
    String  getCategory();
    String  getReputation();
    String  getAuthority();
    String  getNote();
    String  getStatus();
    Long  getLastCheckedTime();
    Long  getLastUpdatedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
