package com.pagesynopsis.ws;



public interface UrlRating 
{
    String  getGuid();
    String  getDomain();
    String  getPageUrl();
    String  getPreview();
    String  getFlag();
    Double  getRating();
    String  getNote();
    String  getStatus();
    Long  getRatedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
