package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.AudioSetDataObject;

public class AudioSetBean extends PageBaseBean implements AudioSet
{
    private static final Logger log = Logger.getLogger(AudioSetBean.class.getName());

    // Embedded data object.
    private AudioSetDataObject dobj = null;

    public AudioSetBean()
    {
        this(new AudioSetDataObject());
    }
    public AudioSetBean(String guid)
    {
        this(new AudioSetDataObject(guid));
    }
    public AudioSetBean(AudioSetDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public AudioSetDataObject getDataObject()
    {
        return this.dobj;
    }

    public List<String> getMediaTypeFilter()
    {
        if(getDataObject() != null) {
            return getDataObject().getMediaTypeFilter();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioSetDataObject is null!");
            return null;   // ???
        }
    }
    public void setMediaTypeFilter(List<String> mediaTypeFilter)
    {
        if(getDataObject() != null) {
            getDataObject().setMediaTypeFilter(mediaTypeFilter);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioSetDataObject is null!");
        }
    }

    public Set<AudioStruct> getPageAudios()
    {
        if(getDataObject() != null) {
            Set<AudioStruct> list = getDataObject().getPageAudios();
            if(list != null) {
                Set<AudioStruct> bean = new HashSet<AudioStruct>();
                for(AudioStruct audioStruct : list) {
                    AudioStructBean elem = null;
                    if(audioStruct instanceof AudioStructBean) {
                        elem = (AudioStructBean) audioStruct;
                    } else if(audioStruct instanceof AudioStructDataObject) {
                        elem = new AudioStructBean((AudioStructDataObject) audioStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioSetDataObject is null!");
            return null;
        }
    }
    public void setPageAudios(Set<AudioStruct> pageAudios)
    {
        if(pageAudios != null) {
            if(getDataObject() != null) {
                Set<AudioStruct> dataObj = new HashSet<AudioStruct>();
                for(AudioStruct audioStruct : pageAudios) {
                    AudioStructDataObject elem = null;
                    if(audioStruct instanceof AudioStructBean) {
                        elem = ((AudioStructBean) audioStruct).toDataObject();
                    } else if(audioStruct instanceof AudioStructDataObject) {
                        elem = (AudioStructDataObject) audioStruct;
                    } else if(audioStruct instanceof AudioStruct) {
                        elem = new AudioStructDataObject(audioStruct.getUuid(), audioStruct.getId(), audioStruct.getControls(), audioStruct.isAutoplayEnabled(), audioStruct.isLoopEnabled(), audioStruct.isPreloadEnabled(), audioStruct.getRemark(), audioStruct.getSource(), audioStruct.getSource1(), audioStruct.getSource2(), audioStruct.getSource3(), audioStruct.getSource4(), audioStruct.getSource5(), audioStruct.getNote());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setPageAudios(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded AudioSetDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setPageAudios(pageAudios);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded AudioSetDataObject is null!");
            }
        }
    }


    // TBD
    public AudioSetDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
