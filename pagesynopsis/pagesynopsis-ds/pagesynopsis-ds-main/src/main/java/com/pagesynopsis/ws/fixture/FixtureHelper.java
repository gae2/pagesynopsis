package com.pagesynopsis.ws.fixture;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.bean.ApiConsumerBean;
import com.pagesynopsis.ws.bean.UserBean;
import com.pagesynopsis.ws.bean.RobotsTextFileBean;
import com.pagesynopsis.ws.bean.RobotsTextRefreshBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.bean.FetchRequestBean;
import com.pagesynopsis.ws.bean.PageInfoBean;
import com.pagesynopsis.ws.bean.PageFetchBean;
import com.pagesynopsis.ws.bean.LinkListBean;
import com.pagesynopsis.ws.bean.ImageSetBean;
import com.pagesynopsis.ws.bean.AudioSetBean;
import com.pagesynopsis.ws.bean.VideoSetBean;
import com.pagesynopsis.ws.bean.OpenGraphMetaBean;
import com.pagesynopsis.ws.bean.TwitterCardMetaBean;
import com.pagesynopsis.ws.bean.DomainInfoBean;
import com.pagesynopsis.ws.bean.UrlRatingBean;
import com.pagesynopsis.ws.bean.ServiceInfoBean;
import com.pagesynopsis.ws.bean.FiveTenBean;
import com.pagesynopsis.ws.stub.ApiConsumerStub;
import com.pagesynopsis.ws.stub.UserStub;
import com.pagesynopsis.ws.stub.RobotsTextFileStub;
import com.pagesynopsis.ws.stub.RobotsTextRefreshStub;
import com.pagesynopsis.ws.stub.OgProfileStub;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgBlogStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgVideoStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.FetchRequestStub;
import com.pagesynopsis.ws.stub.PageInfoStub;
import com.pagesynopsis.ws.stub.PageFetchStub;
import com.pagesynopsis.ws.stub.LinkListStub;
import com.pagesynopsis.ws.stub.ImageSetStub;
import com.pagesynopsis.ws.stub.AudioSetStub;
import com.pagesynopsis.ws.stub.VideoSetStub;
import com.pagesynopsis.ws.stub.OpenGraphMetaStub;
import com.pagesynopsis.ws.stub.TwitterCardMetaStub;
import com.pagesynopsis.ws.stub.DomainInfoStub;
import com.pagesynopsis.ws.stub.UrlRatingStub;
import com.pagesynopsis.ws.stub.ServiceInfoStub;
import com.pagesynopsis.ws.stub.FiveTenStub;
import com.pagesynopsis.ws.stub.ApiConsumerListStub;
import com.pagesynopsis.ws.stub.UserListStub;
import com.pagesynopsis.ws.stub.RobotsTextFileListStub;
import com.pagesynopsis.ws.stub.RobotsTextRefreshListStub;
import com.pagesynopsis.ws.stub.OgProfileListStub;
import com.pagesynopsis.ws.stub.OgWebsiteListStub;
import com.pagesynopsis.ws.stub.OgBlogListStub;
import com.pagesynopsis.ws.stub.OgArticleListStub;
import com.pagesynopsis.ws.stub.OgBookListStub;
import com.pagesynopsis.ws.stub.OgVideoListStub;
import com.pagesynopsis.ws.stub.OgMovieListStub;
import com.pagesynopsis.ws.stub.OgTvShowListStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeListStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardListStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardListStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardListStub;
import com.pagesynopsis.ws.stub.TwitterAppCardListStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardListStub;
import com.pagesynopsis.ws.stub.TwitterProductCardListStub;
import com.pagesynopsis.ws.stub.FetchRequestListStub;
import com.pagesynopsis.ws.stub.PageInfoListStub;
import com.pagesynopsis.ws.stub.PageFetchListStub;
import com.pagesynopsis.ws.stub.LinkListListStub;
import com.pagesynopsis.ws.stub.ImageSetListStub;
import com.pagesynopsis.ws.stub.AudioSetListStub;
import com.pagesynopsis.ws.stub.VideoSetListStub;
import com.pagesynopsis.ws.stub.OpenGraphMetaListStub;
import com.pagesynopsis.ws.stub.TwitterCardMetaListStub;
import com.pagesynopsis.ws.stub.DomainInfoListStub;
import com.pagesynopsis.ws.stub.UrlRatingListStub;
import com.pagesynopsis.ws.stub.ServiceInfoListStub;
import com.pagesynopsis.ws.stub.FiveTenListStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.impl.ApiConsumerResourceImpl;
import com.pagesynopsis.ws.resource.impl.UserResourceImpl;
import com.pagesynopsis.ws.resource.impl.RobotsTextFileResourceImpl;
import com.pagesynopsis.ws.resource.impl.RobotsTextRefreshResourceImpl;
import com.pagesynopsis.ws.resource.impl.OgProfileResourceImpl;
import com.pagesynopsis.ws.resource.impl.OgWebsiteResourceImpl;
import com.pagesynopsis.ws.resource.impl.OgBlogResourceImpl;
import com.pagesynopsis.ws.resource.impl.OgArticleResourceImpl;
import com.pagesynopsis.ws.resource.impl.OgBookResourceImpl;
import com.pagesynopsis.ws.resource.impl.OgVideoResourceImpl;
import com.pagesynopsis.ws.resource.impl.OgMovieResourceImpl;
import com.pagesynopsis.ws.resource.impl.OgTvShowResourceImpl;
import com.pagesynopsis.ws.resource.impl.OgTvEpisodeResourceImpl;
import com.pagesynopsis.ws.resource.impl.TwitterSummaryCardResourceImpl;
import com.pagesynopsis.ws.resource.impl.TwitterPhotoCardResourceImpl;
import com.pagesynopsis.ws.resource.impl.TwitterGalleryCardResourceImpl;
import com.pagesynopsis.ws.resource.impl.TwitterAppCardResourceImpl;
import com.pagesynopsis.ws.resource.impl.TwitterPlayerCardResourceImpl;
import com.pagesynopsis.ws.resource.impl.TwitterProductCardResourceImpl;
import com.pagesynopsis.ws.resource.impl.FetchRequestResourceImpl;
import com.pagesynopsis.ws.resource.impl.PageInfoResourceImpl;
import com.pagesynopsis.ws.resource.impl.PageFetchResourceImpl;
import com.pagesynopsis.ws.resource.impl.LinkListResourceImpl;
import com.pagesynopsis.ws.resource.impl.ImageSetResourceImpl;
import com.pagesynopsis.ws.resource.impl.AudioSetResourceImpl;
import com.pagesynopsis.ws.resource.impl.VideoSetResourceImpl;
import com.pagesynopsis.ws.resource.impl.OpenGraphMetaResourceImpl;
import com.pagesynopsis.ws.resource.impl.TwitterCardMetaResourceImpl;
import com.pagesynopsis.ws.resource.impl.DomainInfoResourceImpl;
import com.pagesynopsis.ws.resource.impl.UrlRatingResourceImpl;
import com.pagesynopsis.ws.resource.impl.ServiceInfoResourceImpl;
import com.pagesynopsis.ws.resource.impl.FiveTenResourceImpl;


// "Helper" functions...
// Get the list of fixture files,
// Read them, and
// Load the data into the datastore, etc....
// See the note in FixtureUtil...
public class FixtureHelper
{
    private static final Logger log = Logger.getLogger(FixtureHelper.class.getName());

    // ???
    private static final String CONFIG_KEY_FIXTURE_LOAD = "pagesynopsis.fixture.load";
    private static final String CONFIG_KEY_FIXTURE_DIR = "pagesynopsis.fixture.directory";
    // ...

    // Dummy var.
    protected boolean dummyFalse1 = false;
    // ...

    
    private FixtureHelper()
    {
        // ...
    }
    
    // Initialization-on-demand holder.
    private static class FixtureHelperHolder
    {
        private static final FixtureHelper INSTANCE = new FixtureHelper();
    }

    // Singleton method
    public static FixtureHelper getInstance()
    {
        return FixtureHelperHolder.INSTANCE;
    }


    public boolean isFixtureLoad()
    {
        // temporary
        return Config.getInstance().getBoolean(CONFIG_KEY_FIXTURE_LOAD, FixtureUtil.getDefaultFixtureLoad());
    }

    public String getFixtureDirName()
    {
        // temporary
        return Config.getInstance().getString(CONFIG_KEY_FIXTURE_DIR, FixtureUtil.getDefaultFixtureDir());
    }

    
    // TBD
    private List<FixtureFile> buildFixtureFileList()
    {
        List<FixtureFile> list = new ArrayList<FixtureFile>();

        String fixtureDirName = getFixtureDirName();
        File fixtureDir = new File(fixtureDirName);
        if(!fixtureDir.exists() || !fixtureDir.isDirectory()) {
            // error. what to do????
            if(log.isLoggable(Level.WARNING)) log.warning("Fixture file directory does not exist. fixtureDirName = " + fixtureDirName);
        } else {
            File[] files = fixtureDir.listFiles();
            for(File f : files) {
                if(f.isFile()) {
                    String filePath = f.getPath();  // ???
                    FixtureFile ff = new FixtureFile(filePath);
                    list.add(ff);
                } else {
                    // This should not happen. Ignore...                    
                }
            }
        }
        
        return list;
    }
    
    public int processFixtureFiles()
    {
        List<FixtureFile> list = buildFixtureFileList();
        if(log.isLoggable(Level.FINE)) {
            for(FixtureFile f : list) {
                log.fine("FixtureFile f = " + f);
            }
        }
        
        int count = 0;
        for(FixtureFile f : list) {  // list cannot be null.
            // [1] Read the file
            // [2] Convert the file content to objects
            // [3] Then, save it. (We use update/overwrite rather than create to ensure "idempotency".)
            String filePath = f.getFilePath();
            String objectType = f.getObjectType();
            String mediaType = f.getMediaType();
            
            // JSON only, for now....
            if(! FixtureUtil.MEDIA_TYPE_JSON.equals(mediaType)) {
                if(log.isLoggable(Level.WARNING)) log.warning("Currently supports only Json fixture files. Skipping filePath = " + filePath);
                continue;
            }

            BufferedReader reader = null;
            File inputFile = new File(filePath);
            if(inputFile.canRead()) {
                try {
                    reader = new BufferedReader(new FileReader(inputFile));
                    StringBuilder sb = new StringBuilder(0x1000);
                    final char[] buf = new char[0x1000];
                    int read = -1;
                    do {
                        read = reader.read(buf, 0, buf.length);
                        if (read>0) {
                            sb.append(buf, 0, read);
                        }
                    } while (read>=0);

                    if(dummyFalse1) {
                    } else if("ApiConsumer".equals(objectType)) {
                        ApiConsumerStub stub = ApiConsumerStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ApiConsumerBean bean = ApiConsumerResourceImpl.convertApiConsumerStubToBean(stub);
                        boolean suc = ServiceManager.getApiConsumerService().updateApiConsumer(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ApiConsumerList".equals(objectType)) {
                        ApiConsumerListStub listStub = ApiConsumerListStub.fromJsonString(sb.toString());
                        List<ApiConsumerBean> beanList = ApiConsumerResourceImpl.convertApiConsumerListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ApiConsumerBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getApiConsumerService().updateApiConsumer(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("User".equals(objectType)) {
                        UserStub stub = UserStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserBean bean = UserResourceImpl.convertUserStubToBean(stub);
                        boolean suc = ServiceManager.getUserService().updateUser(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserList".equals(objectType)) {
                        UserListStub listStub = UserListStub.fromJsonString(sb.toString());
                        List<UserBean> beanList = UserResourceImpl.convertUserListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserService().updateUser(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("RobotsTextFile".equals(objectType)) {
                        RobotsTextFileStub stub = RobotsTextFileStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        RobotsTextFileBean bean = RobotsTextFileResourceImpl.convertRobotsTextFileStubToBean(stub);
                        boolean suc = ServiceManager.getRobotsTextFileService().updateRobotsTextFile(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("RobotsTextFileList".equals(objectType)) {
                        RobotsTextFileListStub listStub = RobotsTextFileListStub.fromJsonString(sb.toString());
                        List<RobotsTextFileBean> beanList = RobotsTextFileResourceImpl.convertRobotsTextFileListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(RobotsTextFileBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getRobotsTextFileService().updateRobotsTextFile(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("RobotsTextRefresh".equals(objectType)) {
                        RobotsTextRefreshStub stub = RobotsTextRefreshStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        RobotsTextRefreshBean bean = RobotsTextRefreshResourceImpl.convertRobotsTextRefreshStubToBean(stub);
                        boolean suc = ServiceManager.getRobotsTextRefreshService().updateRobotsTextRefresh(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("RobotsTextRefreshList".equals(objectType)) {
                        RobotsTextRefreshListStub listStub = RobotsTextRefreshListStub.fromJsonString(sb.toString());
                        List<RobotsTextRefreshBean> beanList = RobotsTextRefreshResourceImpl.convertRobotsTextRefreshListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(RobotsTextRefreshBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getRobotsTextRefreshService().updateRobotsTextRefresh(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgProfile".equals(objectType)) {
                        OgProfileStub stub = OgProfileStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        OgProfileBean bean = OgProfileResourceImpl.convertOgProfileStubToBean(stub);
                        boolean suc = ServiceManager.getOgProfileService().updateOgProfile(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgProfileList".equals(objectType)) {
                        OgProfileListStub listStub = OgProfileListStub.fromJsonString(sb.toString());
                        List<OgProfileBean> beanList = OgProfileResourceImpl.convertOgProfileListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(OgProfileBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getOgProfileService().updateOgProfile(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgWebsite".equals(objectType)) {
                        OgWebsiteStub stub = OgWebsiteStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        OgWebsiteBean bean = OgWebsiteResourceImpl.convertOgWebsiteStubToBean(stub);
                        boolean suc = ServiceManager.getOgWebsiteService().updateOgWebsite(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgWebsiteList".equals(objectType)) {
                        OgWebsiteListStub listStub = OgWebsiteListStub.fromJsonString(sb.toString());
                        List<OgWebsiteBean> beanList = OgWebsiteResourceImpl.convertOgWebsiteListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(OgWebsiteBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getOgWebsiteService().updateOgWebsite(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgBlog".equals(objectType)) {
                        OgBlogStub stub = OgBlogStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        OgBlogBean bean = OgBlogResourceImpl.convertOgBlogStubToBean(stub);
                        boolean suc = ServiceManager.getOgBlogService().updateOgBlog(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgBlogList".equals(objectType)) {
                        OgBlogListStub listStub = OgBlogListStub.fromJsonString(sb.toString());
                        List<OgBlogBean> beanList = OgBlogResourceImpl.convertOgBlogListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(OgBlogBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getOgBlogService().updateOgBlog(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgArticle".equals(objectType)) {
                        OgArticleStub stub = OgArticleStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        OgArticleBean bean = OgArticleResourceImpl.convertOgArticleStubToBean(stub);
                        boolean suc = ServiceManager.getOgArticleService().updateOgArticle(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgArticleList".equals(objectType)) {
                        OgArticleListStub listStub = OgArticleListStub.fromJsonString(sb.toString());
                        List<OgArticleBean> beanList = OgArticleResourceImpl.convertOgArticleListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(OgArticleBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getOgArticleService().updateOgArticle(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgBook".equals(objectType)) {
                        OgBookStub stub = OgBookStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        OgBookBean bean = OgBookResourceImpl.convertOgBookStubToBean(stub);
                        boolean suc = ServiceManager.getOgBookService().updateOgBook(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgBookList".equals(objectType)) {
                        OgBookListStub listStub = OgBookListStub.fromJsonString(sb.toString());
                        List<OgBookBean> beanList = OgBookResourceImpl.convertOgBookListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(OgBookBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getOgBookService().updateOgBook(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgVideo".equals(objectType)) {
                        OgVideoStub stub = OgVideoStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        OgVideoBean bean = OgVideoResourceImpl.convertOgVideoStubToBean(stub);
                        boolean suc = ServiceManager.getOgVideoService().updateOgVideo(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgVideoList".equals(objectType)) {
                        OgVideoListStub listStub = OgVideoListStub.fromJsonString(sb.toString());
                        List<OgVideoBean> beanList = OgVideoResourceImpl.convertOgVideoListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(OgVideoBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getOgVideoService().updateOgVideo(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgMovie".equals(objectType)) {
                        OgMovieStub stub = OgMovieStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        OgMovieBean bean = OgMovieResourceImpl.convertOgMovieStubToBean(stub);
                        boolean suc = ServiceManager.getOgMovieService().updateOgMovie(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgMovieList".equals(objectType)) {
                        OgMovieListStub listStub = OgMovieListStub.fromJsonString(sb.toString());
                        List<OgMovieBean> beanList = OgMovieResourceImpl.convertOgMovieListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(OgMovieBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getOgMovieService().updateOgMovie(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgTvShow".equals(objectType)) {
                        OgTvShowStub stub = OgTvShowStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        OgTvShowBean bean = OgTvShowResourceImpl.convertOgTvShowStubToBean(stub);
                        boolean suc = ServiceManager.getOgTvShowService().updateOgTvShow(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgTvShowList".equals(objectType)) {
                        OgTvShowListStub listStub = OgTvShowListStub.fromJsonString(sb.toString());
                        List<OgTvShowBean> beanList = OgTvShowResourceImpl.convertOgTvShowListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(OgTvShowBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getOgTvShowService().updateOgTvShow(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgTvEpisode".equals(objectType)) {
                        OgTvEpisodeStub stub = OgTvEpisodeStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        OgTvEpisodeBean bean = OgTvEpisodeResourceImpl.convertOgTvEpisodeStubToBean(stub);
                        boolean suc = ServiceManager.getOgTvEpisodeService().updateOgTvEpisode(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OgTvEpisodeList".equals(objectType)) {
                        OgTvEpisodeListStub listStub = OgTvEpisodeListStub.fromJsonString(sb.toString());
                        List<OgTvEpisodeBean> beanList = OgTvEpisodeResourceImpl.convertOgTvEpisodeListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(OgTvEpisodeBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getOgTvEpisodeService().updateOgTvEpisode(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterSummaryCard".equals(objectType)) {
                        TwitterSummaryCardStub stub = TwitterSummaryCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterSummaryCardBean bean = TwitterSummaryCardResourceImpl.convertTwitterSummaryCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterSummaryCardService().updateTwitterSummaryCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterSummaryCardList".equals(objectType)) {
                        TwitterSummaryCardListStub listStub = TwitterSummaryCardListStub.fromJsonString(sb.toString());
                        List<TwitterSummaryCardBean> beanList = TwitterSummaryCardResourceImpl.convertTwitterSummaryCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterSummaryCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterSummaryCardService().updateTwitterSummaryCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterPhotoCard".equals(objectType)) {
                        TwitterPhotoCardStub stub = TwitterPhotoCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterPhotoCardBean bean = TwitterPhotoCardResourceImpl.convertTwitterPhotoCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterPhotoCardService().updateTwitterPhotoCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterPhotoCardList".equals(objectType)) {
                        TwitterPhotoCardListStub listStub = TwitterPhotoCardListStub.fromJsonString(sb.toString());
                        List<TwitterPhotoCardBean> beanList = TwitterPhotoCardResourceImpl.convertTwitterPhotoCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterPhotoCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterPhotoCardService().updateTwitterPhotoCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterGalleryCard".equals(objectType)) {
                        TwitterGalleryCardStub stub = TwitterGalleryCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterGalleryCardBean bean = TwitterGalleryCardResourceImpl.convertTwitterGalleryCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterGalleryCardService().updateTwitterGalleryCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterGalleryCardList".equals(objectType)) {
                        TwitterGalleryCardListStub listStub = TwitterGalleryCardListStub.fromJsonString(sb.toString());
                        List<TwitterGalleryCardBean> beanList = TwitterGalleryCardResourceImpl.convertTwitterGalleryCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterGalleryCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterGalleryCardService().updateTwitterGalleryCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterAppCard".equals(objectType)) {
                        TwitterAppCardStub stub = TwitterAppCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterAppCardBean bean = TwitterAppCardResourceImpl.convertTwitterAppCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterAppCardService().updateTwitterAppCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterAppCardList".equals(objectType)) {
                        TwitterAppCardListStub listStub = TwitterAppCardListStub.fromJsonString(sb.toString());
                        List<TwitterAppCardBean> beanList = TwitterAppCardResourceImpl.convertTwitterAppCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterAppCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterAppCardService().updateTwitterAppCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterPlayerCard".equals(objectType)) {
                        TwitterPlayerCardStub stub = TwitterPlayerCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterPlayerCardBean bean = TwitterPlayerCardResourceImpl.convertTwitterPlayerCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterPlayerCardService().updateTwitterPlayerCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterPlayerCardList".equals(objectType)) {
                        TwitterPlayerCardListStub listStub = TwitterPlayerCardListStub.fromJsonString(sb.toString());
                        List<TwitterPlayerCardBean> beanList = TwitterPlayerCardResourceImpl.convertTwitterPlayerCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterPlayerCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterPlayerCardService().updateTwitterPlayerCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterProductCard".equals(objectType)) {
                        TwitterProductCardStub stub = TwitterProductCardStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterProductCardBean bean = TwitterProductCardResourceImpl.convertTwitterProductCardStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterProductCardService().updateTwitterProductCard(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterProductCardList".equals(objectType)) {
                        TwitterProductCardListStub listStub = TwitterProductCardListStub.fromJsonString(sb.toString());
                        List<TwitterProductCardBean> beanList = TwitterProductCardResourceImpl.convertTwitterProductCardListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterProductCardBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterProductCardService().updateTwitterProductCard(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FetchRequest".equals(objectType)) {
                        FetchRequestStub stub = FetchRequestStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        FetchRequestBean bean = FetchRequestResourceImpl.convertFetchRequestStubToBean(stub);
                        boolean suc = ServiceManager.getFetchRequestService().updateFetchRequest(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FetchRequestList".equals(objectType)) {
                        FetchRequestListStub listStub = FetchRequestListStub.fromJsonString(sb.toString());
                        List<FetchRequestBean> beanList = FetchRequestResourceImpl.convertFetchRequestListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(FetchRequestBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getFetchRequestService().updateFetchRequest(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("PageInfo".equals(objectType)) {
                        PageInfoStub stub = PageInfoStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        PageInfoBean bean = PageInfoResourceImpl.convertPageInfoStubToBean(stub);
                        boolean suc = ServiceManager.getPageInfoService().updatePageInfo(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("PageInfoList".equals(objectType)) {
                        PageInfoListStub listStub = PageInfoListStub.fromJsonString(sb.toString());
                        List<PageInfoBean> beanList = PageInfoResourceImpl.convertPageInfoListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(PageInfoBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getPageInfoService().updatePageInfo(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("PageFetch".equals(objectType)) {
                        PageFetchStub stub = PageFetchStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        PageFetchBean bean = PageFetchResourceImpl.convertPageFetchStubToBean(stub);
                        boolean suc = ServiceManager.getPageFetchService().updatePageFetch(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("PageFetchList".equals(objectType)) {
                        PageFetchListStub listStub = PageFetchListStub.fromJsonString(sb.toString());
                        List<PageFetchBean> beanList = PageFetchResourceImpl.convertPageFetchListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(PageFetchBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getPageFetchService().updatePageFetch(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("LinkList".equals(objectType)) {
                        LinkListStub stub = LinkListStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        LinkListBean bean = LinkListResourceImpl.convertLinkListStubToBean(stub);
                        boolean suc = ServiceManager.getLinkListService().updateLinkList(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("LinkListList".equals(objectType)) {
                        LinkListListStub listStub = LinkListListStub.fromJsonString(sb.toString());
                        List<LinkListBean> beanList = LinkListResourceImpl.convertLinkListListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(LinkListBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getLinkListService().updateLinkList(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ImageSet".equals(objectType)) {
                        ImageSetStub stub = ImageSetStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ImageSetBean bean = ImageSetResourceImpl.convertImageSetStubToBean(stub);
                        boolean suc = ServiceManager.getImageSetService().updateImageSet(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ImageSetList".equals(objectType)) {
                        ImageSetListStub listStub = ImageSetListStub.fromJsonString(sb.toString());
                        List<ImageSetBean> beanList = ImageSetResourceImpl.convertImageSetListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ImageSetBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getImageSetService().updateImageSet(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("AudioSet".equals(objectType)) {
                        AudioSetStub stub = AudioSetStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        AudioSetBean bean = AudioSetResourceImpl.convertAudioSetStubToBean(stub);
                        boolean suc = ServiceManager.getAudioSetService().updateAudioSet(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("AudioSetList".equals(objectType)) {
                        AudioSetListStub listStub = AudioSetListStub.fromJsonString(sb.toString());
                        List<AudioSetBean> beanList = AudioSetResourceImpl.convertAudioSetListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(AudioSetBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getAudioSetService().updateAudioSet(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("VideoSet".equals(objectType)) {
                        VideoSetStub stub = VideoSetStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        VideoSetBean bean = VideoSetResourceImpl.convertVideoSetStubToBean(stub);
                        boolean suc = ServiceManager.getVideoSetService().updateVideoSet(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("VideoSetList".equals(objectType)) {
                        VideoSetListStub listStub = VideoSetListStub.fromJsonString(sb.toString());
                        List<VideoSetBean> beanList = VideoSetResourceImpl.convertVideoSetListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(VideoSetBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getVideoSetService().updateVideoSet(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OpenGraphMeta".equals(objectType)) {
                        OpenGraphMetaStub stub = OpenGraphMetaStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        OpenGraphMetaBean bean = OpenGraphMetaResourceImpl.convertOpenGraphMetaStubToBean(stub);
                        boolean suc = ServiceManager.getOpenGraphMetaService().updateOpenGraphMeta(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("OpenGraphMetaList".equals(objectType)) {
                        OpenGraphMetaListStub listStub = OpenGraphMetaListStub.fromJsonString(sb.toString());
                        List<OpenGraphMetaBean> beanList = OpenGraphMetaResourceImpl.convertOpenGraphMetaListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(OpenGraphMetaBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getOpenGraphMetaService().updateOpenGraphMeta(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterCardMeta".equals(objectType)) {
                        TwitterCardMetaStub stub = TwitterCardMetaStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TwitterCardMetaBean bean = TwitterCardMetaResourceImpl.convertTwitterCardMetaStubToBean(stub);
                        boolean suc = ServiceManager.getTwitterCardMetaService().updateTwitterCardMeta(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TwitterCardMetaList".equals(objectType)) {
                        TwitterCardMetaListStub listStub = TwitterCardMetaListStub.fromJsonString(sb.toString());
                        List<TwitterCardMetaBean> beanList = TwitterCardMetaResourceImpl.convertTwitterCardMetaListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TwitterCardMetaBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTwitterCardMetaService().updateTwitterCardMeta(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("DomainInfo".equals(objectType)) {
                        DomainInfoStub stub = DomainInfoStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        DomainInfoBean bean = DomainInfoResourceImpl.convertDomainInfoStubToBean(stub);
                        boolean suc = ServiceManager.getDomainInfoService().updateDomainInfo(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("DomainInfoList".equals(objectType)) {
                        DomainInfoListStub listStub = DomainInfoListStub.fromJsonString(sb.toString());
                        List<DomainInfoBean> beanList = DomainInfoResourceImpl.convertDomainInfoListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(DomainInfoBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getDomainInfoService().updateDomainInfo(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UrlRating".equals(objectType)) {
                        UrlRatingStub stub = UrlRatingStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UrlRatingBean bean = UrlRatingResourceImpl.convertUrlRatingStubToBean(stub);
                        boolean suc = ServiceManager.getUrlRatingService().updateUrlRating(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UrlRatingList".equals(objectType)) {
                        UrlRatingListStub listStub = UrlRatingListStub.fromJsonString(sb.toString());
                        List<UrlRatingBean> beanList = UrlRatingResourceImpl.convertUrlRatingListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UrlRatingBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUrlRatingService().updateUrlRating(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceInfo".equals(objectType)) {
                        ServiceInfoStub stub = ServiceInfoStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ServiceInfoBean bean = ServiceInfoResourceImpl.convertServiceInfoStubToBean(stub);
                        boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceInfoList".equals(objectType)) {
                        ServiceInfoListStub listStub = ServiceInfoListStub.fromJsonString(sb.toString());
                        List<ServiceInfoBean> beanList = ServiceInfoResourceImpl.convertServiceInfoListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ServiceInfoBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FiveTen".equals(objectType)) {
                        FiveTenStub stub = FiveTenStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        FiveTenBean bean = FiveTenResourceImpl.convertFiveTenStubToBean(stub);
                        boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FiveTenList".equals(objectType)) {
                        FiveTenListStub listStub = FiveTenListStub.fromJsonString(sb.toString());
                        List<FiveTenBean> beanList = FiveTenResourceImpl.convertFiveTenListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(FiveTenBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else {
                        // This cannot happen
                    }
                } catch (FileNotFoundException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (IOException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (BaseException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process a fixture file: filePath = " + filePath, e);
                } catch (Exception e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected error while processing a fixture file: filePath = " + filePath, e);
                } finally {
                    if(reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Error while closing the fixture file: filePath = " + filePath, e);
                        }
                    }
                }                
            } else {
                if(log.isLoggable(Level.WARNING)) log.warning("Skipping a fixture file because it is not readable: filePath = " + filePath);
            }
        }
                
        return count;
    }
    
}
