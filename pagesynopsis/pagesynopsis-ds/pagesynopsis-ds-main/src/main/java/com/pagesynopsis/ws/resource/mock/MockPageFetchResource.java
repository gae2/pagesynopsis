package com.pagesynopsis.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ResourceAlreadyPresentException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.DataStoreRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.bean.PageFetchBean;
import com.pagesynopsis.ws.stub.PageFetchListStub;
import com.pagesynopsis.ws.stub.PageFetchStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.PageFetchResource;
import com.pagesynopsis.ws.resource.util.UrlStructResourceUtil;
import com.pagesynopsis.ws.resource.util.ImageStructResourceUtil;
import com.pagesynopsis.ws.resource.util.AnchorStructResourceUtil;
import com.pagesynopsis.ws.resource.util.KeyValuePairStructResourceUtil;
import com.pagesynopsis.ws.resource.util.AudioStructResourceUtil;
import com.pagesynopsis.ws.resource.util.VideoStructResourceUtil;

// MockPageFetchResource is a decorator.
// It can be used as a base class to mock PageFetchResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/pageFetches/")
public abstract class MockPageFetchResource implements PageFetchResource
{
    private static final Logger log = Logger.getLogger(MockPageFetchResource.class.getName());

    // MockPageFetchResource uses the decorator design pattern.
    private PageFetchResource decoratedResource;

    public MockPageFetchResource(PageFetchResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected PageFetchResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(PageFetchResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllPageFetches(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllPageFetchKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getPageFetchKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getPageFetchKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getPageFetch(String guid) throws BaseResourceException
    {
        return decoratedResource.getPageFetch(guid);
    }

    @Override
    public Response getPageFetch(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getPageFetch(guid, field);
    }

    @Override
    public Response createPageFetch(PageFetchStub pageFetch) throws BaseResourceException
    {
        return decoratedResource.createPageFetch(pageFetch);
    }

    @Override
    public Response updatePageFetch(String guid, PageFetchStub pageFetch) throws BaseResourceException
    {
        return decoratedResource.updatePageFetch(guid, pageFetch);
    }

    @Override
    public Response updatePageFetch(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<String> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<String> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseResourceException
    {
        return decoratedResource.updatePageFetch(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
    }

    @Override
    public Response deletePageFetch(String guid) throws BaseResourceException
    {
        return decoratedResource.deletePageFetch(guid);
    }

    @Override
    public Response deletePageFetches(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deletePageFetches(filter, params, values);
    }


}
