package com.pagesynopsis.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ResourceAlreadyPresentException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.DataStoreRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.stub.TwitterPlayerCardListStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.TwitterPlayerCardResource;
import com.pagesynopsis.ws.resource.util.TwitterCardAppInfoResourceUtil;
import com.pagesynopsis.ws.resource.util.TwitterCardProductDataResourceUtil;

// MockTwitterPlayerCardResource is a decorator.
// It can be used as a base class to mock TwitterPlayerCardResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/twitterPlayerCards/")
public abstract class MockTwitterPlayerCardResource implements TwitterPlayerCardResource
{
    private static final Logger log = Logger.getLogger(MockTwitterPlayerCardResource.class.getName());

    // MockTwitterPlayerCardResource uses the decorator design pattern.
    private TwitterPlayerCardResource decoratedResource;

    public MockTwitterPlayerCardResource(TwitterPlayerCardResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected TwitterPlayerCardResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(TwitterPlayerCardResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllTwitterPlayerCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterPlayerCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTwitterPlayerCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getTwitterPlayerCardKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getTwitterPlayerCardKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getTwitterPlayerCard(String guid) throws BaseResourceException
    {
        return decoratedResource.getTwitterPlayerCard(guid);
    }

    @Override
    public Response getTwitterPlayerCard(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getTwitterPlayerCard(guid, field);
    }

    @Override
    public Response createTwitterPlayerCard(TwitterPlayerCardStub twitterPlayerCard) throws BaseResourceException
    {
        return decoratedResource.createTwitterPlayerCard(twitterPlayerCard);
    }

    @Override
    public Response updateTwitterPlayerCard(String guid, TwitterPlayerCardStub twitterPlayerCard) throws BaseResourceException
    {
        return decoratedResource.updateTwitterPlayerCard(guid, twitterPlayerCard);
    }

    @Override
    public Response updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseResourceException
    {
        return decoratedResource.updateTwitterPlayerCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
    }

    @Override
    public Response deleteTwitterPlayerCard(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterPlayerCard(guid);
    }

    @Override
    public Response deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteTwitterPlayerCards(filter, params, values);
    }


}
