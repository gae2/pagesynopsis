package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class RobotsTextRefreshDataObject extends KeyedDataObject implements RobotsTextRefresh
{
    private static final Logger log = Logger.getLogger(RobotsTextRefreshDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(RobotsTextRefreshDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(RobotsTextRefreshDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String robotsTextFile;

    @Persistent(defaultFetchGroup = "true")
    private Integer refreshInterval;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Integer refreshStatus;

    @Persistent(defaultFetchGroup = "true")
    private String result;

    @Persistent(defaultFetchGroup = "true")
    private Long lastCheckedTime;

    @Persistent(defaultFetchGroup = "true")
    private Long nextCheckedTime;

    @Persistent(defaultFetchGroup = "true")
    private Long expirationTime;

    public RobotsTextRefreshDataObject()
    {
        this(null);
    }
    public RobotsTextRefreshDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null);
    }
    public RobotsTextRefreshDataObject(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime)
    {
        this(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime, null, null);
    }
    public RobotsTextRefreshDataObject(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.robotsTextFile = robotsTextFile;
        this.refreshInterval = refreshInterval;
        this.note = note;
        this.status = status;
        this.refreshStatus = refreshStatus;
        this.result = result;
        this.lastCheckedTime = lastCheckedTime;
        this.nextCheckedTime = nextCheckedTime;
        this.expirationTime = expirationTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return RobotsTextRefreshDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return RobotsTextRefreshDataObject.composeKey(getGuid());
    }

    public String getRobotsTextFile()
    {
        return this.robotsTextFile;
    }
    public void setRobotsTextFile(String robotsTextFile)
    {
        this.robotsTextFile = robotsTextFile;
    }

    public Integer getRefreshInterval()
    {
        return this.refreshInterval;
    }
    public void setRefreshInterval(Integer refreshInterval)
    {
        this.refreshInterval = refreshInterval;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Integer getRefreshStatus()
    {
        return this.refreshStatus;
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        this.refreshStatus = refreshStatus;
    }

    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public Long getLastCheckedTime()
    {
        return this.lastCheckedTime;
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        this.lastCheckedTime = lastCheckedTime;
    }

    public Long getNextCheckedTime()
    {
        return this.nextCheckedTime;
    }
    public void setNextCheckedTime(Long nextCheckedTime)
    {
        this.nextCheckedTime = nextCheckedTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("robotsTextFile", this.robotsTextFile);
        dataMap.put("refreshInterval", this.refreshInterval);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);
        dataMap.put("refreshStatus", this.refreshStatus);
        dataMap.put("result", this.result);
        dataMap.put("lastCheckedTime", this.lastCheckedTime);
        dataMap.put("nextCheckedTime", this.nextCheckedTime);
        dataMap.put("expirationTime", this.expirationTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        RobotsTextRefresh thatObj = (RobotsTextRefresh) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.robotsTextFile == null && thatObj.getRobotsTextFile() != null)
            || (this.robotsTextFile != null && thatObj.getRobotsTextFile() == null)
            || !this.robotsTextFile.equals(thatObj.getRobotsTextFile()) ) {
            return false;
        }
        if( (this.refreshInterval == null && thatObj.getRefreshInterval() != null)
            || (this.refreshInterval != null && thatObj.getRefreshInterval() == null)
            || !this.refreshInterval.equals(thatObj.getRefreshInterval()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.refreshStatus == null && thatObj.getRefreshStatus() != null)
            || (this.refreshStatus != null && thatObj.getRefreshStatus() == null)
            || !this.refreshStatus.equals(thatObj.getRefreshStatus()) ) {
            return false;
        }
        if( (this.result == null && thatObj.getResult() != null)
            || (this.result != null && thatObj.getResult() == null)
            || !this.result.equals(thatObj.getResult()) ) {
            return false;
        }
        if( (this.lastCheckedTime == null && thatObj.getLastCheckedTime() != null)
            || (this.lastCheckedTime != null && thatObj.getLastCheckedTime() == null)
            || !this.lastCheckedTime.equals(thatObj.getLastCheckedTime()) ) {
            return false;
        }
        if( (this.nextCheckedTime == null && thatObj.getNextCheckedTime() != null)
            || (this.nextCheckedTime != null && thatObj.getNextCheckedTime() == null)
            || !this.nextCheckedTime.equals(thatObj.getNextCheckedTime()) ) {
            return false;
        }
        if( (this.expirationTime == null && thatObj.getExpirationTime() != null)
            || (this.expirationTime != null && thatObj.getExpirationTime() == null)
            || !this.expirationTime.equals(thatObj.getExpirationTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = robotsTextFile == null ? 0 : robotsTextFile.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshInterval == null ? 0 : refreshInterval.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshStatus == null ? 0 : refreshStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = result == null ? 0 : result.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastCheckedTime == null ? 0 : lastCheckedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextCheckedTime == null ? 0 : nextCheckedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
