package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.bean.UrlStructBean;
import com.pagesynopsis.ws.bean.ImageStructBean;
import com.pagesynopsis.ws.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.bean.AnchorStructBean;
import com.pagesynopsis.ws.bean.KeyValuePairStructBean;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.bean.AudioStructBean;
import com.pagesynopsis.ws.bean.VideoStructBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.bean.VideoSetBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.VideoSetDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.VideoSetService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class VideoSetServiceImpl implements VideoSetService
{
    private static final Logger log = Logger.getLogger(VideoSetServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // VideoSet related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public VideoSet getVideoSet(String guid) throws BaseException
    {
        log.finer("BEGIN");

        VideoSetDataObject dataObj = getDAOFactory().getVideoSetDAO().getVideoSet(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve VideoSetDataObject for guid = " + guid);
            return null;  // ????
        }
        VideoSetBean bean = new VideoSetBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getVideoSet(String guid, String field) throws BaseException
    {
        VideoSetDataObject dataObj = getDAOFactory().getVideoSetDAO().getVideoSet(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve VideoSetDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("fetchRequest")) {
            return dataObj.getFetchRequest();
        } else if(field.equals("targetUrl")) {
            return dataObj.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return dataObj.getPageUrl();
        } else if(field.equals("queryString")) {
            return dataObj.getQueryString();
        } else if(field.equals("queryParams")) {
            return dataObj.getQueryParams();
        } else if(field.equals("lastFetchResult")) {
            return dataObj.getLastFetchResult();
        } else if(field.equals("responseCode")) {
            return dataObj.getResponseCode();
        } else if(field.equals("contentType")) {
            return dataObj.getContentType();
        } else if(field.equals("contentLength")) {
            return dataObj.getContentLength();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("redirect")) {
            return dataObj.getRedirect();
        } else if(field.equals("location")) {
            return dataObj.getLocation();
        } else if(field.equals("pageTitle")) {
            return dataObj.getPageTitle();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("deferred")) {
            return dataObj.isDeferred();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("refreshStatus")) {
            return dataObj.getRefreshStatus();
        } else if(field.equals("refreshInterval")) {
            return dataObj.getRefreshInterval();
        } else if(field.equals("nextRefreshTime")) {
            return dataObj.getNextRefreshTime();
        } else if(field.equals("lastCheckedTime")) {
            return dataObj.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return dataObj.getLastUpdatedTime();
        } else if(field.equals("mediaTypeFilter")) {
            return dataObj.getMediaTypeFilter();
        } else if(field.equals("pageVideos")) {
            return dataObj.getPageVideos();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<VideoSet> getVideoSets(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<VideoSet> list = new ArrayList<VideoSet>();
        List<VideoSetDataObject> dataObjs = getDAOFactory().getVideoSetDAO().getVideoSets(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve VideoSetDataObject list.");
        } else {
            Iterator<VideoSetDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                VideoSetDataObject dataObj = (VideoSetDataObject) it.next();
                list.add(new VideoSetBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<VideoSet> getAllVideoSets() throws BaseException
    {
        return getAllVideoSets(null, null, null);
    }

    @Override
    public List<VideoSet> getAllVideoSets(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllVideoSets(ordering, offset, count, null);
    }

    @Override
    public List<VideoSet> getAllVideoSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllVideoSets(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<VideoSet> list = new ArrayList<VideoSet>();
        List<VideoSetDataObject> dataObjs = getDAOFactory().getVideoSetDAO().getAllVideoSets(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve VideoSetDataObject list.");
        } else {
            Iterator<VideoSetDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                VideoSetDataObject dataObj = (VideoSetDataObject) it.next();
                list.add(new VideoSetBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllVideoSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllVideoSetKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getVideoSetDAO().getAllVideoSetKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve VideoSet key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findVideoSets(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findVideoSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("VideoSetServiceImpl.findVideoSets(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<VideoSet> list = new ArrayList<VideoSet>();
        List<VideoSetDataObject> dataObjs = getDAOFactory().getVideoSetDAO().findVideoSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find videoSets for the given criterion.");
        } else {
            Iterator<VideoSetDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                VideoSetDataObject dataObj = (VideoSetDataObject) it.next();
                list.add(new VideoSetBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("VideoSetServiceImpl.findVideoSetKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getVideoSetDAO().findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find VideoSet keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("VideoSetServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getVideoSetDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createVideoSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<VideoStruct> pageVideos) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        VideoSetDataObject dataObj = new VideoSetDataObject(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageVideos);
        return createVideoSet(dataObj);
    }

    @Override
    public String createVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("BEGIN");

        // Param videoSet cannot be null.....
        if(videoSet == null) {
            log.log(Level.INFO, "Param videoSet is null!");
            throw new BadRequestException("Param videoSet object is null!");
        }
        VideoSetDataObject dataObj = null;
        if(videoSet instanceof VideoSetDataObject) {
            dataObj = (VideoSetDataObject) videoSet;
        } else if(videoSet instanceof VideoSetBean) {
            dataObj = ((VideoSetBean) videoSet).toDataObject();
        } else {  // if(videoSet instanceof VideoSet)
            //dataObj = new VideoSetDataObject(null, videoSet.getUser(), videoSet.getFetchRequest(), videoSet.getTargetUrl(), videoSet.getPageUrl(), videoSet.getQueryString(), videoSet.getQueryParams(), videoSet.getLastFetchResult(), videoSet.getResponseCode(), videoSet.getContentType(), videoSet.getContentLength(), videoSet.getLanguage(), videoSet.getRedirect(), videoSet.getLocation(), videoSet.getPageTitle(), videoSet.getNote(), videoSet.isDeferred(), videoSet.getStatus(), videoSet.getRefreshStatus(), videoSet.getRefreshInterval(), videoSet.getNextRefreshTime(), videoSet.getLastCheckedTime(), videoSet.getLastUpdatedTime(), videoSet.getMediaTypeFilter(), videoSet.getPageVideos());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new VideoSetDataObject(videoSet.getGuid(), videoSet.getUser(), videoSet.getFetchRequest(), videoSet.getTargetUrl(), videoSet.getPageUrl(), videoSet.getQueryString(), videoSet.getQueryParams(), videoSet.getLastFetchResult(), videoSet.getResponseCode(), videoSet.getContentType(), videoSet.getContentLength(), videoSet.getLanguage(), videoSet.getRedirect(), videoSet.getLocation(), videoSet.getPageTitle(), videoSet.getNote(), videoSet.isDeferred(), videoSet.getStatus(), videoSet.getRefreshStatus(), videoSet.getRefreshInterval(), videoSet.getNextRefreshTime(), videoSet.getLastCheckedTime(), videoSet.getLastUpdatedTime(), videoSet.getMediaTypeFilter(), videoSet.getPageVideos());
        }
        String guid = getDAOFactory().getVideoSetDAO().createVideoSet(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateVideoSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<VideoStruct> pageVideos) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        VideoSetDataObject dataObj = new VideoSetDataObject(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageVideos);
        return updateVideoSet(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("BEGIN");

        // Param videoSet cannot be null.....
        if(videoSet == null || videoSet.getGuid() == null) {
            log.log(Level.INFO, "Param videoSet or its guid is null!");
            throw new BadRequestException("Param videoSet object or its guid is null!");
        }
        VideoSetDataObject dataObj = null;
        if(videoSet instanceof VideoSetDataObject) {
            dataObj = (VideoSetDataObject) videoSet;
        } else if(videoSet instanceof VideoSetBean) {
            dataObj = ((VideoSetBean) videoSet).toDataObject();
        } else {  // if(videoSet instanceof VideoSet)
            dataObj = new VideoSetDataObject(videoSet.getGuid(), videoSet.getUser(), videoSet.getFetchRequest(), videoSet.getTargetUrl(), videoSet.getPageUrl(), videoSet.getQueryString(), videoSet.getQueryParams(), videoSet.getLastFetchResult(), videoSet.getResponseCode(), videoSet.getContentType(), videoSet.getContentLength(), videoSet.getLanguage(), videoSet.getRedirect(), videoSet.getLocation(), videoSet.getPageTitle(), videoSet.getNote(), videoSet.isDeferred(), videoSet.getStatus(), videoSet.getRefreshStatus(), videoSet.getRefreshInterval(), videoSet.getNextRefreshTime(), videoSet.getLastCheckedTime(), videoSet.getLastUpdatedTime(), videoSet.getMediaTypeFilter(), videoSet.getPageVideos());
        }
        Boolean suc = getDAOFactory().getVideoSetDAO().updateVideoSet(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteVideoSet(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getVideoSetDAO().deleteVideoSet(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("BEGIN");

        // Param videoSet cannot be null.....
        if(videoSet == null || videoSet.getGuid() == null) {
            log.log(Level.INFO, "Param videoSet or its guid is null!");
            throw new BadRequestException("Param videoSet object or its guid is null!");
        }
        VideoSetDataObject dataObj = null;
        if(videoSet instanceof VideoSetDataObject) {
            dataObj = (VideoSetDataObject) videoSet;
        } else if(videoSet instanceof VideoSetBean) {
            dataObj = ((VideoSetBean) videoSet).toDataObject();
        } else {  // if(videoSet instanceof VideoSet)
            dataObj = new VideoSetDataObject(videoSet.getGuid(), videoSet.getUser(), videoSet.getFetchRequest(), videoSet.getTargetUrl(), videoSet.getPageUrl(), videoSet.getQueryString(), videoSet.getQueryParams(), videoSet.getLastFetchResult(), videoSet.getResponseCode(), videoSet.getContentType(), videoSet.getContentLength(), videoSet.getLanguage(), videoSet.getRedirect(), videoSet.getLocation(), videoSet.getPageTitle(), videoSet.getNote(), videoSet.isDeferred(), videoSet.getStatus(), videoSet.getRefreshStatus(), videoSet.getRefreshInterval(), videoSet.getNextRefreshTime(), videoSet.getLastCheckedTime(), videoSet.getLastUpdatedTime(), videoSet.getMediaTypeFilter(), videoSet.getPageVideos());
        }
        Boolean suc = getDAOFactory().getVideoSetDAO().deleteVideoSet(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteVideoSets(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getVideoSetDAO().deleteVideoSets(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
