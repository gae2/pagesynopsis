package com.pagesynopsis.ws.dao.mock;

import java.util.logging.Logger;

import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.dao.ApiConsumerDAO;
import com.pagesynopsis.ws.dao.UserDAO;
import com.pagesynopsis.ws.dao.RobotsTextFileDAO;
import com.pagesynopsis.ws.dao.RobotsTextRefreshDAO;
import com.pagesynopsis.ws.dao.OgProfileDAO;
import com.pagesynopsis.ws.dao.OgWebsiteDAO;
import com.pagesynopsis.ws.dao.OgBlogDAO;
import com.pagesynopsis.ws.dao.OgArticleDAO;
import com.pagesynopsis.ws.dao.OgBookDAO;
import com.pagesynopsis.ws.dao.OgVideoDAO;
import com.pagesynopsis.ws.dao.OgMovieDAO;
import com.pagesynopsis.ws.dao.OgTvShowDAO;
import com.pagesynopsis.ws.dao.OgTvEpisodeDAO;
import com.pagesynopsis.ws.dao.TwitterSummaryCardDAO;
import com.pagesynopsis.ws.dao.TwitterPhotoCardDAO;
import com.pagesynopsis.ws.dao.TwitterGalleryCardDAO;
import com.pagesynopsis.ws.dao.TwitterAppCardDAO;
import com.pagesynopsis.ws.dao.TwitterPlayerCardDAO;
import com.pagesynopsis.ws.dao.TwitterProductCardDAO;
import com.pagesynopsis.ws.dao.FetchRequestDAO;
import com.pagesynopsis.ws.dao.PageInfoDAO;
import com.pagesynopsis.ws.dao.PageFetchDAO;
import com.pagesynopsis.ws.dao.LinkListDAO;
import com.pagesynopsis.ws.dao.ImageSetDAO;
import com.pagesynopsis.ws.dao.AudioSetDAO;
import com.pagesynopsis.ws.dao.VideoSetDAO;
import com.pagesynopsis.ws.dao.OpenGraphMetaDAO;
import com.pagesynopsis.ws.dao.TwitterCardMetaDAO;
import com.pagesynopsis.ws.dao.DomainInfoDAO;
import com.pagesynopsis.ws.dao.UrlRatingDAO;
import com.pagesynopsis.ws.dao.ServiceInfoDAO;
import com.pagesynopsis.ws.dao.FiveTenDAO;


// Create your own mock object factory using MockDAOFactory as a template.
public class MockDAOFactory extends DAOFactory
{
    private static final Logger log = Logger.getLogger(MockDAOFactory.class.getName());

    // Using the Decorator pattern.
    private MockDAOFactory decoratedDAOFactory;
    private MockDAOFactory()
    {
        this(null);   // ????
    }
    private MockDAOFactory(MockDAOFactory decoratedDAOFactory)
    {
        this.decoratedDAOFactory = decoratedDAOFactory;
    }

    // Initialization-on-demand holder.
    private static class MockDAOFactoryHolder
    {
        private static final MockDAOFactory INSTANCE = new MockDAOFactory();
    }

    // Singleton method
    public static MockDAOFactory getInstance()
    {
        return MockDAOFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public MockDAOFactory getDecoratedDAOFactory()
    {
        return decoratedDAOFactory;
    }
    public void setDecoratedDAOFactory(MockDAOFactory decoratedDAOFactory)
    {
        this.decoratedDAOFactory = decoratedDAOFactory;
    }


    @Override
    public ApiConsumerDAO getApiConsumerDAO()
    {
        return new MockApiConsumerDAO(decoratedDAOFactory.getApiConsumerDAO()) {};
    }

    @Override
    public UserDAO getUserDAO()
    {
        return new MockUserDAO(decoratedDAOFactory.getUserDAO()) {};
    }

    @Override
    public RobotsTextFileDAO getRobotsTextFileDAO()
    {
        return new MockRobotsTextFileDAO(decoratedDAOFactory.getRobotsTextFileDAO()) {};
    }

    @Override
    public RobotsTextRefreshDAO getRobotsTextRefreshDAO()
    {
        return new MockRobotsTextRefreshDAO(decoratedDAOFactory.getRobotsTextRefreshDAO()) {};
    }

    @Override
    public OgProfileDAO getOgProfileDAO()
    {
        return new MockOgProfileDAO(decoratedDAOFactory.getOgProfileDAO()) {};
    }

    @Override
    public OgWebsiteDAO getOgWebsiteDAO()
    {
        return new MockOgWebsiteDAO(decoratedDAOFactory.getOgWebsiteDAO()) {};
    }

    @Override
    public OgBlogDAO getOgBlogDAO()
    {
        return new MockOgBlogDAO(decoratedDAOFactory.getOgBlogDAO()) {};
    }

    @Override
    public OgArticleDAO getOgArticleDAO()
    {
        return new MockOgArticleDAO(decoratedDAOFactory.getOgArticleDAO()) {};
    }

    @Override
    public OgBookDAO getOgBookDAO()
    {
        return new MockOgBookDAO(decoratedDAOFactory.getOgBookDAO()) {};
    }

    @Override
    public OgVideoDAO getOgVideoDAO()
    {
        return new MockOgVideoDAO(decoratedDAOFactory.getOgVideoDAO()) {};
    }

    @Override
    public OgMovieDAO getOgMovieDAO()
    {
        return new MockOgMovieDAO(decoratedDAOFactory.getOgMovieDAO()) {};
    }

    @Override
    public OgTvShowDAO getOgTvShowDAO()
    {
        return new MockOgTvShowDAO(decoratedDAOFactory.getOgTvShowDAO()) {};
    }

    @Override
    public OgTvEpisodeDAO getOgTvEpisodeDAO()
    {
        return new MockOgTvEpisodeDAO(decoratedDAOFactory.getOgTvEpisodeDAO()) {};
    }

    @Override
    public TwitterSummaryCardDAO getTwitterSummaryCardDAO()
    {
        return new MockTwitterSummaryCardDAO(decoratedDAOFactory.getTwitterSummaryCardDAO()) {};
    }

    @Override
    public TwitterPhotoCardDAO getTwitterPhotoCardDAO()
    {
        return new MockTwitterPhotoCardDAO(decoratedDAOFactory.getTwitterPhotoCardDAO()) {};
    }

    @Override
    public TwitterGalleryCardDAO getTwitterGalleryCardDAO()
    {
        return new MockTwitterGalleryCardDAO(decoratedDAOFactory.getTwitterGalleryCardDAO()) {};
    }

    @Override
    public TwitterAppCardDAO getTwitterAppCardDAO()
    {
        return new MockTwitterAppCardDAO(decoratedDAOFactory.getTwitterAppCardDAO()) {};
    }

    @Override
    public TwitterPlayerCardDAO getTwitterPlayerCardDAO()
    {
        return new MockTwitterPlayerCardDAO(decoratedDAOFactory.getTwitterPlayerCardDAO()) {};
    }

    @Override
    public TwitterProductCardDAO getTwitterProductCardDAO()
    {
        return new MockTwitterProductCardDAO(decoratedDAOFactory.getTwitterProductCardDAO()) {};
    }

    @Override
    public FetchRequestDAO getFetchRequestDAO()
    {
        return new MockFetchRequestDAO(decoratedDAOFactory.getFetchRequestDAO()) {};
    }

    @Override
    public PageInfoDAO getPageInfoDAO()
    {
        return new MockPageInfoDAO(decoratedDAOFactory.getPageInfoDAO()) {};
    }

    @Override
    public PageFetchDAO getPageFetchDAO()
    {
        return new MockPageFetchDAO(decoratedDAOFactory.getPageFetchDAO()) {};
    }

    @Override
    public LinkListDAO getLinkListDAO()
    {
        return new MockLinkListDAO(decoratedDAOFactory.getLinkListDAO()) {};
    }

    @Override
    public ImageSetDAO getImageSetDAO()
    {
        return new MockImageSetDAO(decoratedDAOFactory.getImageSetDAO()) {};
    }

    @Override
    public AudioSetDAO getAudioSetDAO()
    {
        return new MockAudioSetDAO(decoratedDAOFactory.getAudioSetDAO()) {};
    }

    @Override
    public VideoSetDAO getVideoSetDAO()
    {
        return new MockVideoSetDAO(decoratedDAOFactory.getVideoSetDAO()) {};
    }

    @Override
    public OpenGraphMetaDAO getOpenGraphMetaDAO()
    {
        return new MockOpenGraphMetaDAO(decoratedDAOFactory.getOpenGraphMetaDAO()) {};
    }

    @Override
    public TwitterCardMetaDAO getTwitterCardMetaDAO()
    {
        return new MockTwitterCardMetaDAO(decoratedDAOFactory.getTwitterCardMetaDAO()) {};
    }

    @Override
    public DomainInfoDAO getDomainInfoDAO()
    {
        return new MockDomainInfoDAO(decoratedDAOFactory.getDomainInfoDAO()) {};
    }

    @Override
    public UrlRatingDAO getUrlRatingDAO()
    {
        return new MockUrlRatingDAO(decoratedDAOFactory.getUrlRatingDAO()) {};
    }

    @Override
    public ServiceInfoDAO getServiceInfoDAO()
    {
        return new MockServiceInfoDAO(decoratedDAOFactory.getServiceInfoDAO()) {};
    }

    @Override
    public FiveTenDAO getFiveTenDAO()
    {
        return new MockFiveTenDAO(decoratedDAOFactory.getFiveTenDAO()) {};
    }

}
