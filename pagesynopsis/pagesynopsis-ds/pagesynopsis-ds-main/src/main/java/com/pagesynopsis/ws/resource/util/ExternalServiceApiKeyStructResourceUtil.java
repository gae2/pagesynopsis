package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.ExternalServiceApiKeyStruct;
import com.pagesynopsis.ws.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.ws.stub.ExternalServiceApiKeyStructStub;


public class ExternalServiceApiKeyStructResourceUtil
{
    private static final Logger log = Logger.getLogger(ExternalServiceApiKeyStructResourceUtil.class.getName());

    // Static methods only.
    private ExternalServiceApiKeyStructResourceUtil() {}

    public static ExternalServiceApiKeyStructBean convertExternalServiceApiKeyStructStubToBean(ExternalServiceApiKeyStruct stub)
    {
        ExternalServiceApiKeyStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null ExternalServiceApiKeyStructBean is returned.");
        } else {
            bean = new ExternalServiceApiKeyStructBean();
            bean.setUuid(stub.getUuid());
            bean.setService(stub.getService());
            bean.setKey(stub.getKey());
            bean.setSecret(stub.getSecret());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
