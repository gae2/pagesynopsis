package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogActorStructs")
@XmlType(propOrder = {"ogActorStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgActorStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgActorStructListStub.class.getName());

    private List<OgActorStructStub> ogActorStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgActorStructListStub()
    {
        this(new ArrayList<OgActorStructStub>());
    }
    public OgActorStructListStub(List<OgActorStructStub> ogActorStructs)
    {
        this(ogActorStructs, null);
    }
    public OgActorStructListStub(List<OgActorStructStub> ogActorStructs, String forwardCursor)
    {
        this.ogActorStructs = ogActorStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogActorStructs == null) {
            return true;
        } else {
            return ogActorStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogActorStructs == null) {
            return 0;
        } else {
            return ogActorStructs.size();
        }
    }


    @XmlElement(name = "ogActorStruct")
    public List<OgActorStructStub> getOgActorStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgActorStructStub> getList()
    {
        return ogActorStructs;
    }
    public void setList(List<OgActorStructStub> ogActorStructs)
    {
        this.ogActorStructs = ogActorStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgActorStructStub> it = this.ogActorStructs.iterator();
        while(it.hasNext()) {
            OgActorStructStub ogActorStruct = it.next();
            sb.append(ogActorStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgActorStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgActorStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgActorStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgActorStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgActorStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgActorStructListStub fromJsonString(String jsonStr)
    {
        try {
            OgActorStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgActorStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgActorStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgActorStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgActorStructListStub object.", e);
        }
        
        return null;
    }

}
