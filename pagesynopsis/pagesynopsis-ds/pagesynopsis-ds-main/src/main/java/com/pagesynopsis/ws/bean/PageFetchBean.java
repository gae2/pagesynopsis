package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.PageFetchDataObject;

public class PageFetchBean extends PageBaseBean implements PageFetch
{
    private static final Logger log = Logger.getLogger(PageFetchBean.class.getName());

    // Embedded data object.
    private PageFetchDataObject dobj = null;

    public PageFetchBean()
    {
        this(new PageFetchDataObject());
    }
    public PageFetchBean(String guid)
    {
        this(new PageFetchDataObject(guid));
    }
    public PageFetchBean(PageFetchDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public PageFetchDataObject getDataObject()
    {
        return this.dobj;
    }

    public Integer getInputMaxRedirects()
    {
        if(getDataObject() != null) {
            return getDataObject().getInputMaxRedirects();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
            return null;   // ???
        }
    }
    public void setInputMaxRedirects(Integer inputMaxRedirects)
    {
        if(getDataObject() != null) {
            getDataObject().setInputMaxRedirects(inputMaxRedirects);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
        }
    }

    public Integer getResultRedirectCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getResultRedirectCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
            return null;   // ???
        }
    }
    public void setResultRedirectCount(Integer resultRedirectCount)
    {
        if(getDataObject() != null) {
            getDataObject().setResultRedirectCount(resultRedirectCount);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
        }
    }

    public List<UrlStruct> getRedirectPages()
    {
        if(getDataObject() != null) {
            List<UrlStruct> list = getDataObject().getRedirectPages();
            if(list != null) {
                List<UrlStruct> bean = new ArrayList<UrlStruct>();
                for(UrlStruct urlStruct : list) {
                    UrlStructBean elem = null;
                    if(urlStruct instanceof UrlStructBean) {
                        elem = (UrlStructBean) urlStruct;
                    } else if(urlStruct instanceof UrlStructDataObject) {
                        elem = new UrlStructBean((UrlStructDataObject) urlStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
            return null;
        }
    }
    public void setRedirectPages(List<UrlStruct> redirectPages)
    {
        if(redirectPages != null) {
            if(getDataObject() != null) {
                List<UrlStruct> dataObj = new ArrayList<UrlStruct>();
                for(UrlStruct urlStruct : redirectPages) {
                    UrlStructDataObject elem = null;
                    if(urlStruct instanceof UrlStructBean) {
                        elem = ((UrlStructBean) urlStruct).toDataObject();
                    } else if(urlStruct instanceof UrlStructDataObject) {
                        elem = (UrlStructDataObject) urlStruct;
                    } else if(urlStruct instanceof UrlStruct) {
                        elem = new UrlStructDataObject(urlStruct.getUuid(), urlStruct.getStatusCode(), urlStruct.getRedirectUrl(), urlStruct.getAbsoluteUrl(), urlStruct.getHashFragment(), urlStruct.getNote());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setRedirectPages(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setRedirectPages(redirectPages);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
            }
        }
    }

    public String getDestinationUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getDestinationUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
            return null;   // ???
        }
    }
    public void setDestinationUrl(String destinationUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setDestinationUrl(destinationUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
        }
    }

    public String getPageAuthor()
    {
        if(getDataObject() != null) {
            return getDataObject().getPageAuthor();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
            return null;   // ???
        }
    }
    public void setPageAuthor(String pageAuthor)
    {
        if(getDataObject() != null) {
            getDataObject().setPageAuthor(pageAuthor);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
        }
    }

    public String getPageSummary()
    {
        if(getDataObject() != null) {
            return getDataObject().getPageSummary();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
            return null;   // ???
        }
    }
    public void setPageSummary(String pageSummary)
    {
        if(getDataObject() != null) {
            getDataObject().setPageSummary(pageSummary);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
        }
    }

    public String getFavicon()
    {
        if(getDataObject() != null) {
            return getDataObject().getFavicon();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
            return null;   // ???
        }
    }
    public void setFavicon(String favicon)
    {
        if(getDataObject() != null) {
            getDataObject().setFavicon(favicon);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
        }
    }

    public String getFaviconUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getFaviconUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
            return null;   // ???
        }
    }
    public void setFaviconUrl(String faviconUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setFaviconUrl(faviconUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageFetchDataObject is null!");
        }
    }


    // TBD
    public PageFetchDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
