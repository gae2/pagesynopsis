package com.pagesynopsis.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OgTvEpisodeDAO;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;


public class DefaultOgTvEpisodeDAO extends DefaultDAOBase implements OgTvEpisodeDAO
{
    private static final Logger log = Logger.getLogger(DefaultOgTvEpisodeDAO.class.getName()); 

    // Returns the ogTvEpisode for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public OgTvEpisodeDataObject getOgTvEpisode(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        OgTvEpisodeDataObject ogTvEpisode = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = OgTvEpisodeDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = OgTvEpisodeDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                ogTvEpisode = pm.getObjectById(OgTvEpisodeDataObject.class, key);
                ogTvEpisode.getImage();  // "Touch". Otherwise this field will not be fetched by default.
                ogTvEpisode.getAudio();  // "Touch". Otherwise this field will not be fetched by default.
                ogTvEpisode.getVideo();  // "Touch". Otherwise this field will not be fetched by default.
                ogTvEpisode.getLocaleAlternate();  // "Touch". Otherwise this field will not be fetched by default.
                ogTvEpisode.getDirector();  // "Touch". Otherwise this field will not be fetched by default.
                ogTvEpisode.getWriter();  // "Touch". Otherwise this field will not be fetched by default.
                ogTvEpisode.getActor();  // "Touch". Otherwise this field will not be fetched by default.
                ogTvEpisode.getTag();  // "Touch". Otherwise this field will not be fetched by default.
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ogTvEpisode for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return ogTvEpisode;
	}

    @Override
    public List<OgTvEpisodeDataObject> getOgTvEpisodes(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<OgTvEpisodeDataObject> ogTvEpisodes = null;
        if(guids != null && !guids.isEmpty()) {
            ogTvEpisodes = new ArrayList<OgTvEpisodeDataObject>();
            for(String guid : guids) {
                OgTvEpisodeDataObject obj = getOgTvEpisode(guid); 
                ogTvEpisodes.add(obj);
            }
	    }

        log.finer("END");
        return ogTvEpisodes;
    }

    @Override
    public List<OgTvEpisodeDataObject> getAllOgTvEpisodes() throws BaseException
	{
	    return getAllOgTvEpisodes(null, null, null);
    }


    @Override
    public List<OgTvEpisodeDataObject> getAllOgTvEpisodes(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOgTvEpisodes(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<OgTvEpisodeDataObject> getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<OgTvEpisodeDataObject> ogTvEpisodes = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(OgTvEpisodeDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            ogTvEpisodes = (List<OgTvEpisodeDataObject>) q.execute();
            if(ogTvEpisodes != null) {
                ogTvEpisodes.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(ogTvEpisodes);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            /*
            // ???
            Collection<OgTvEpisodeDataObject> rs_ogTvEpisodes = (Collection<OgTvEpisodeDataObject>) q.execute();
            if(rs_ogTvEpisodes == null) {
                log.log(Level.WARNING, "Failed to retrieve all ogTvEpisodes.");
                ogTvEpisodes = new ArrayList<OgTvEpisodeDataObject>();  // ???           
            } else {
                ogTvEpisodes = new ArrayList<OgTvEpisodeDataObject>(pm.detachCopyAll(rs_ogTvEpisodes));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all ogTvEpisodes.", ex);
            //ogTvEpisodes = new ArrayList<OgTvEpisodeDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all ogTvEpisodes.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return ogTvEpisodes;
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvEpisodeKeys(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + OgTvEpisodeDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all OgTvEpisode keys.", ex);
            throw new DataStoreException("Failed to retrieve all OgTvEpisode keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOgTvEpisodes(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOgTvEpisodes(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultOgTvEpisodeDAO.findOgTvEpisodes(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findOgTvEpisodes() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<OgTvEpisodeDataObject> ogTvEpisodes = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(OgTvEpisodeDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                ogTvEpisodes = (List<OgTvEpisodeDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                ogTvEpisodes = (List<OgTvEpisodeDataObject>) q.execute();
            }
            if(ogTvEpisodes != null) {
                ogTvEpisodes.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(ogTvEpisodes);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(OgTvEpisodeDataObject dobj : ogTvEpisodes) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find ogTvEpisodes because index is missing.", ex);
            //ogTvEpisodes = new ArrayList<OgTvEpisodeDataObject>();  // ???
            throw new DataStoreException("Failed to find ogTvEpisodes because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find ogTvEpisodes meeting the criterion.", ex);
            //ogTvEpisodes = new ArrayList<OgTvEpisodeDataObject>();  // ???
            throw new DataStoreException("Failed to find ogTvEpisodes meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return ogTvEpisodes;
	}

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultOgTvEpisodeDAO.findOgTvEpisodeKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + OgTvEpisodeDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find OgTvEpisode keys because index is missing.", ex);
            throw new DataStoreException("Failed to find OgTvEpisode keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find OgTvEpisode keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find OgTvEpisode keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultOgTvEpisodeDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(OgTvEpisodeDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get ogTvEpisode count because index is missing.", ex);
            throw new DataStoreException("Failed to get ogTvEpisode count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get ogTvEpisode count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get ogTvEpisode count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the ogTvEpisode in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeOgTvEpisode(OgTvEpisodeDataObject ogTvEpisode) throws BaseException
    {
        log.fine("storeOgTvEpisode() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        try {
            // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
            // "Object is marked as dirty yet no fields are marked dirty".
            // tx.setOptimistic(true);
            // ....
            tx.begin();
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = ogTvEpisode.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                ogTvEpisode.setCreatedTime(createdTime);
            }
            Long modifiedTime = ogTvEpisode.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                ogTvEpisode.setModifiedTime(createdTime);
            }
            // ????
            // javax.jdo.JDOHelper.makeDirty(ogTvEpisode, "guid");
            pm.makePersistent(ogTvEpisode); 
            tx.commit();
            // TBD: How do you know the makePersistent() call was successful???
            guid = ogTvEpisode.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store ogTvEpisode because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store ogTvEpisode because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store ogTvEpisode.", ex);
            throw new DataStoreException("Failed to store ogTvEpisode.", ex);
        } finally {
            try {
                if(tx.isActive()) {
                    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    tx.rollback();
                }
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeOgTvEpisode(): guid = " + guid);
        return guid;
    }

    @Override
    public String createOgTvEpisode(OgTvEpisodeDataObject ogTvEpisode) throws BaseException
    {
        // The createdTime field will be automatically set in storeOgTvEpisode().
        //Long createdTime = ogTvEpisode.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    ogTvEpisode.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = ogTvEpisode.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    ogTvEpisode.setModifiedTime(createdTime);
        //}
        return storeOgTvEpisode(ogTvEpisode);
    }

    @Override
	public Boolean updateOgTvEpisode(OgTvEpisodeDataObject ogTvEpisode) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeOgTvEpisode()
	    // (in which case modifiedTime might be updated again).
	    ogTvEpisode.setModifiedTime(System.currentTimeMillis());
	    String guid = storeOgTvEpisode(ogTvEpisode);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteOgTvEpisode(OgTvEpisodeDataObject ogTvEpisode) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        try {
            // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
            // "Object is marked as dirty yet no fields are marked dirty".
            // tx.setOptimistic(true);
            // ....
            tx.begin();
            pm.deletePersistent(ogTvEpisode);
            tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete ogTvEpisode because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete ogTvEpisode because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete ogTvEpisode.", ex);
            throw new DataStoreException("Failed to delete ogTvEpisode.", ex);
        } finally {
            try {
                if(tx.isActive()) {
                    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    tx.rollback();
                }
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteOgTvEpisode(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            Transaction tx = pm.currentTransaction();
            try {
                // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
                // "Object is marked as dirty yet no fields are marked dirty".
                // tx.setOptimistic(true);
                // ....
                tx.begin();
                //Key key = OgTvEpisodeDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = OgTvEpisodeDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                OgTvEpisodeDataObject ogTvEpisode = pm.getObjectById(OgTvEpisodeDataObject.class, key);
                pm.deletePersistent(ogTvEpisode);
                tx.commit();
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete ogTvEpisode because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete ogTvEpisode because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete ogTvEpisode for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete ogTvEpisode for guid = " + guid, ex);
            } finally {
                try {
                    if(tx.isActive()) {
                        log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                        tx.rollback();
                    }
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteOgTvEpisodes(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultOgTvEpisodeDAO.deleteOgTvEpisodes(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        try {
            // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
            // "Object is marked as dirty yet no fields are marked dirty".
            // tx.setOptimistic(true);
            // ....
            tx.begin();
            Query q = pm.newQuery(OgTvEpisodeDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteogTvEpisodes because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete ogTvEpisodes because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete ogTvEpisodes because index is missing", ex);
            throw new DataStoreException("Failed to delete ogTvEpisodes because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete ogTvEpisodes", ex);
            throw new DataStoreException("Failed to delete ogTvEpisodes", ex);
        } finally {
            try {
                if(tx.isActive()) {
                    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    tx.rollback();
                }
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
