package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.PageInfoDataObject;


// TBD: Add offset/count to getAllPageInfos() and findPageInfos(), etc.
public interface PageInfoDAO
{
    PageInfoDataObject getPageInfo(String guid) throws BaseException;
    List<PageInfoDataObject> getPageInfos(List<String> guids) throws BaseException;
    List<PageInfoDataObject> getAllPageInfos() throws BaseException;
    /* @Deprecated */ List<PageInfoDataObject> getAllPageInfos(String ordering, Long offset, Integer count) throws BaseException;
    List<PageInfoDataObject> getAllPageInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<PageInfoDataObject> findPageInfos(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<PageInfoDataObject> findPageInfos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<PageInfoDataObject> findPageInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<PageInfoDataObject> findPageInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createPageInfo(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return PageInfoDataObject?)
    String createPageInfo(PageInfoDataObject pageInfo) throws BaseException;          // Returns Guid.  (Return PageInfoDataObject?)
    //Boolean updatePageInfo(String guid, Map<String, Object> args) throws BaseException;
    Boolean updatePageInfo(PageInfoDataObject pageInfo) throws BaseException;
    Boolean deletePageInfo(String guid) throws BaseException;
    Boolean deletePageInfo(PageInfoDataObject pageInfo) throws BaseException;
    Long deletePageInfos(String filter, String params, List<String> values) throws BaseException;
}
