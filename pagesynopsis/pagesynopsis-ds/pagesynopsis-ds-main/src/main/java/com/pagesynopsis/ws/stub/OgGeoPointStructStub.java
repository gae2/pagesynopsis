package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.OgGeoPointStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogGeoPointStruct")
@XmlType(propOrder = {"uuid", "latitude", "longitude", "altitude"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgGeoPointStructStub implements OgGeoPointStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgGeoPointStructStub.class.getName());

    private String uuid;
    private Float latitude;
    private Float longitude;
    private Float altitude;

    public OgGeoPointStructStub()
    {
        this(null);
    }
    public OgGeoPointStructStub(OgGeoPointStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.latitude = bean.getLatitude();
            this.longitude = bean.getLongitude();
            this.altitude = bean.getAltitude();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public Float getLatitude()
    {
        return this.latitude;
    }
    public void setLatitude(Float latitude)
    {
        this.latitude = latitude;
    }

    @XmlElement
    public Float getLongitude()
    {
        return this.longitude;
    }
    public void setLongitude(Float longitude)
    {
        this.longitude = longitude;
    }

    @XmlElement
    public Float getAltitude()
    {
        return this.altitude;
    }
    public void setAltitude(Float altitude)
    {
        this.altitude = altitude;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("latitude", this.latitude);
        dataMap.put("longitude", this.longitude);
        dataMap.put("altitude", this.altitude);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = latitude == null ? 0 : latitude.hashCode();
        _hash = 31 * _hash + delta;
        delta = longitude == null ? 0 : longitude.hashCode();
        _hash = 31 * _hash + delta;
        delta = altitude == null ? 0 : altitude.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static OgGeoPointStructStub convertBeanToStub(OgGeoPointStruct bean)
    {
        OgGeoPointStructStub stub = null;
        if(bean instanceof OgGeoPointStructStub) {
            stub = (OgGeoPointStructStub) bean;
        } else {
            if(bean != null) {
                stub = new OgGeoPointStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgGeoPointStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of OgGeoPointStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgGeoPointStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgGeoPointStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgGeoPointStructStub object as a string.", e);
        }
        
        return null;
    }
    public static OgGeoPointStructStub fromJsonString(String jsonStr)
    {
        try {
            OgGeoPointStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgGeoPointStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgGeoPointStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgGeoPointStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgGeoPointStructStub object.", e);
        }
        
        return null;
    }

}
