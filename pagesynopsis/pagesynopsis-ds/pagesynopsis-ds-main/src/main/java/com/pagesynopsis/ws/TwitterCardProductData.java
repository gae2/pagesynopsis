package com.pagesynopsis.ws;



public interface TwitterCardProductData 
{
    String  getData();
    String  getLabel();
    boolean isEmpty();
}
