package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.AudioSetDataObject;


// TBD: Add offset/count to getAllAudioSets() and findAudioSets(), etc.
public interface AudioSetDAO
{
    AudioSetDataObject getAudioSet(String guid) throws BaseException;
    List<AudioSetDataObject> getAudioSets(List<String> guids) throws BaseException;
    List<AudioSetDataObject> getAllAudioSets() throws BaseException;
    /* @Deprecated */ List<AudioSetDataObject> getAllAudioSets(String ordering, Long offset, Integer count) throws BaseException;
    List<AudioSetDataObject> getAllAudioSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<AudioSetDataObject> findAudioSets(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<AudioSetDataObject> findAudioSets(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<AudioSetDataObject> findAudioSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<AudioSetDataObject> findAudioSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createAudioSet(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AudioSetDataObject?)
    String createAudioSet(AudioSetDataObject audioSet) throws BaseException;          // Returns Guid.  (Return AudioSetDataObject?)
    //Boolean updateAudioSet(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAudioSet(AudioSetDataObject audioSet) throws BaseException;
    Boolean deleteAudioSet(String guid) throws BaseException;
    Boolean deleteAudioSet(AudioSetDataObject audioSet) throws BaseException;
    Long deleteAudioSets(String filter, String params, List<String> values) throws BaseException;
}
