package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogImageStructs")
@XmlType(propOrder = {"ogImageStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgImageStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgImageStructListStub.class.getName());

    private List<OgImageStructStub> ogImageStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgImageStructListStub()
    {
        this(new ArrayList<OgImageStructStub>());
    }
    public OgImageStructListStub(List<OgImageStructStub> ogImageStructs)
    {
        this(ogImageStructs, null);
    }
    public OgImageStructListStub(List<OgImageStructStub> ogImageStructs, String forwardCursor)
    {
        this.ogImageStructs = ogImageStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogImageStructs == null) {
            return true;
        } else {
            return ogImageStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogImageStructs == null) {
            return 0;
        } else {
            return ogImageStructs.size();
        }
    }


    @XmlElement(name = "ogImageStruct")
    public List<OgImageStructStub> getOgImageStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgImageStructStub> getList()
    {
        return ogImageStructs;
    }
    public void setList(List<OgImageStructStub> ogImageStructs)
    {
        this.ogImageStructs = ogImageStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgImageStructStub> it = this.ogImageStructs.iterator();
        while(it.hasNext()) {
            OgImageStructStub ogImageStruct = it.next();
            sb.append(ogImageStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgImageStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgImageStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgImageStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgImageStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgImageStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgImageStructListStub fromJsonString(String jsonStr)
    {
        try {
            OgImageStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgImageStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgImageStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgImageStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgImageStructListStub object.", e);
        }
        
        return null;
    }

}
