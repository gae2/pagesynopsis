package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.PageFetchDataObject;


// TBD: Add offset/count to getAllPageFetches() and findPageFetches(), etc.
public interface PageFetchDAO
{
    PageFetchDataObject getPageFetch(String guid) throws BaseException;
    List<PageFetchDataObject> getPageFetches(List<String> guids) throws BaseException;
    List<PageFetchDataObject> getAllPageFetches() throws BaseException;
    /* @Deprecated */ List<PageFetchDataObject> getAllPageFetches(String ordering, Long offset, Integer count) throws BaseException;
    List<PageFetchDataObject> getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<PageFetchDataObject> findPageFetches(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<PageFetchDataObject> findPageFetches(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<PageFetchDataObject> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<PageFetchDataObject> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createPageFetch(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return PageFetchDataObject?)
    String createPageFetch(PageFetchDataObject pageFetch) throws BaseException;          // Returns Guid.  (Return PageFetchDataObject?)
    //Boolean updatePageFetch(String guid, Map<String, Object> args) throws BaseException;
    Boolean updatePageFetch(PageFetchDataObject pageFetch) throws BaseException;
    Boolean deletePageFetch(String guid) throws BaseException;
    Boolean deletePageFetch(PageFetchDataObject pageFetch) throws BaseException;
    Long deletePageFetches(String filter, String params, List<String> values) throws BaseException;
}
