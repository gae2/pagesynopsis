package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OgWebsiteDAO;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;


// MockOgWebsiteDAO is a decorator.
// It can be used as a base class to mock OgWebsiteDAO objects.
public abstract class MockOgWebsiteDAO implements OgWebsiteDAO
{
    private static final Logger log = Logger.getLogger(MockOgWebsiteDAO.class.getName()); 

    // MockOgWebsiteDAO uses the decorator design pattern.
    private OgWebsiteDAO decoratedDAO;

    public MockOgWebsiteDAO(OgWebsiteDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected OgWebsiteDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(OgWebsiteDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public OgWebsiteDataObject getOgWebsite(String guid) throws BaseException
    {
        return decoratedDAO.getOgWebsite(guid);
	}

    @Override
    public List<OgWebsiteDataObject> getOgWebsites(List<String> guids) throws BaseException
    {
        return decoratedDAO.getOgWebsites(guids);
    }

    @Override
    public List<OgWebsiteDataObject> getAllOgWebsites() throws BaseException
	{
	    return getAllOgWebsites(null, null, null);
    }


    @Override
    public List<OgWebsiteDataObject> getAllOgWebsites(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOgWebsites(ordering, offset, count, null);
    }

    @Override
    public List<OgWebsiteDataObject> getAllOgWebsites(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllOgWebsites(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgWebsiteKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllOgWebsiteKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<OgWebsiteDataObject> findOgWebsites(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOgWebsites(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OgWebsiteDataObject> findOgWebsites(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOgWebsites(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OgWebsiteDataObject> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<OgWebsiteDataObject> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgWebsite(OgWebsiteDataObject ogWebsite) throws BaseException
    {
        return decoratedDAO.createOgWebsite( ogWebsite);
    }

    @Override
	public Boolean updateOgWebsite(OgWebsiteDataObject ogWebsite) throws BaseException
	{
        return decoratedDAO.updateOgWebsite(ogWebsite);
	}
	
    @Override
    public Boolean deleteOgWebsite(OgWebsiteDataObject ogWebsite) throws BaseException
    {
        return decoratedDAO.deleteOgWebsite(ogWebsite);
    }

    @Override
    public Boolean deleteOgWebsite(String guid) throws BaseException
    {
        return decoratedDAO.deleteOgWebsite(guid);
	}

    @Override
    public Long deleteOgWebsites(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteOgWebsites(filter, params, values);
    }

}
