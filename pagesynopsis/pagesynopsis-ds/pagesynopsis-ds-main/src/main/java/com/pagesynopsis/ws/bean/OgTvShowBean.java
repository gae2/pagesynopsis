package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;

public class OgTvShowBean extends OgObjectBaseBean implements OgTvShow
{
    private static final Logger log = Logger.getLogger(OgTvShowBean.class.getName());

    // Embedded data object.
    private OgTvShowDataObject dobj = null;

    public OgTvShowBean()
    {
        this(new OgTvShowDataObject());
    }
    public OgTvShowBean(String guid)
    {
        this(new OgTvShowDataObject(guid));
    }
    public OgTvShowBean(OgTvShowDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public OgTvShowDataObject getDataObject()
    {
        return this.dobj;
    }

    public List<String> getDirector()
    {
        if(getDataObject() != null) {
            return getDataObject().getDirector();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
            return null;   // ???
        }
    }
    public void setDirector(List<String> director)
    {
        if(getDataObject() != null) {
            getDataObject().setDirector(director);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
        }
    }

    public List<String> getWriter()
    {
        if(getDataObject() != null) {
            return getDataObject().getWriter();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
            return null;   // ???
        }
    }
    public void setWriter(List<String> writer)
    {
        if(getDataObject() != null) {
            getDataObject().setWriter(writer);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
        }
    }

    public List<OgActorStruct> getActor()
    {
        if(getDataObject() != null) {
            List<OgActorStruct> list = getDataObject().getActor();
            if(list != null) {
                List<OgActorStruct> bean = new ArrayList<OgActorStruct>();
                for(OgActorStruct ogActorStruct : list) {
                    OgActorStructBean elem = null;
                    if(ogActorStruct instanceof OgActorStructBean) {
                        elem = (OgActorStructBean) ogActorStruct;
                    } else if(ogActorStruct instanceof OgActorStructDataObject) {
                        elem = new OgActorStructBean((OgActorStructDataObject) ogActorStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
            return null;
        }
    }
    public void setActor(List<OgActorStruct> actor)
    {
        if(actor != null) {
            if(getDataObject() != null) {
                List<OgActorStruct> dataObj = new ArrayList<OgActorStruct>();
                for(OgActorStruct ogActorStruct : actor) {
                    OgActorStructDataObject elem = null;
                    if(ogActorStruct instanceof OgActorStructBean) {
                        elem = ((OgActorStructBean) ogActorStruct).toDataObject();
                    } else if(ogActorStruct instanceof OgActorStructDataObject) {
                        elem = (OgActorStructDataObject) ogActorStruct;
                    } else if(ogActorStruct instanceof OgActorStruct) {
                        elem = new OgActorStructDataObject(ogActorStruct.getUuid(), ogActorStruct.getProfile(), ogActorStruct.getRole());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setActor(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setActor(actor);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
            }
        }
    }

    public Integer getDuration()
    {
        if(getDataObject() != null) {
            return getDataObject().getDuration();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
            return null;   // ???
        }
    }
    public void setDuration(Integer duration)
    {
        if(getDataObject() != null) {
            getDataObject().setDuration(duration);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
        }
    }

    public List<String> getTag()
    {
        if(getDataObject() != null) {
            return getDataObject().getTag();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
            return null;   // ???
        }
    }
    public void setTag(List<String> tag)
    {
        if(getDataObject() != null) {
            getDataObject().setTag(tag);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
        }
    }

    public String getReleaseDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getReleaseDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
            return null;   // ???
        }
    }
    public void setReleaseDate(String releaseDate)
    {
        if(getDataObject() != null) {
            getDataObject().setReleaseDate(releaseDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgTvShowDataObject is null!");
        }
    }


    // TBD
    public OgTvShowDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
