package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgTvShowService;


// OgTvShowMockService is a decorator.
// It can be used as a base class to mock OgTvShowService objects.
public abstract class OgTvShowMockService implements OgTvShowService
{
    private static final Logger log = Logger.getLogger(OgTvShowMockService.class.getName());

    // OgTvShowMockService uses the decorator design pattern.
    private OgTvShowService decoratedService;

    public OgTvShowMockService(OgTvShowService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected OgTvShowService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(OgTvShowService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // OgTvShow related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgTvShow getOgTvShow(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgTvShow(): guid = " + guid);
        OgTvShow bean = decoratedService.getOgTvShow(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgTvShow(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getOgTvShow(guid, field);
        return obj;
    }

    @Override
    public List<OgTvShow> getOgTvShows(List<String> guids) throws BaseException
    {
        log.fine("getOgTvShows()");
        List<OgTvShow> ogTvShows = decoratedService.getOgTvShows(guids);
        log.finer("END");
        return ogTvShows;
    }

    @Override
    public List<OgTvShow> getAllOgTvShows() throws BaseException
    {
        return getAllOgTvShows(null, null, null);
    }


    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShows(ordering, offset, count, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgTvShows(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<OgTvShow> ogTvShows = decoratedService.getAllOgTvShows(ordering, offset, count, forwardCursor);
        log.finer("END");
        return ogTvShows;
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShowKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgTvShowKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllOgTvShowKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvShowMockService.findOgTvShows(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<OgTvShow> ogTvShows = decoratedService.findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return ogTvShows;
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvShowMockService.findOgTvShowKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgTvShowMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgTvShow(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedService.createOgTvShow(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public String createOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createOgTvShow(ogTvShow);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgTvShow(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedService.updateOgTvShow(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }
        
    @Override
    public Boolean updateOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateOgTvShow(ogTvShow);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteOgTvShow(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgTvShow(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgTvShow(ogTvShow);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOgTvShows(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteOgTvShows(filter, params, values);
        return count;
    }

}
