package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.data.OgImageStructDataObject;

public class OgImageStructBean implements OgImageStruct
{
    private static final Logger log = Logger.getLogger(OgImageStructBean.class.getName());

    // Embedded data object.
    private OgImageStructDataObject dobj = null;

    public OgImageStructBean()
    {
        this(new OgImageStructDataObject());
    }
    public OgImageStructBean(OgImageStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public OgImageStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
        }
    }

    public String getUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUrl(String url)
    {
        if(getDataObject() != null) {
            getDataObject().setUrl(url);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
        }
    }

    public String getSecureUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getSecureUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSecureUrl(String secureUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setSecureUrl(secureUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
        }
    }

    public String getType()
    {
        if(getDataObject() != null) {
            return getDataObject().getType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setType(String type)
    {
        if(getDataObject() != null) {
            getDataObject().setType(type);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
        }
    }

    public Integer getWidth()
    {
        if(getDataObject() != null) {
            return getDataObject().getWidth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setWidth(Integer width)
    {
        if(getDataObject() != null) {
            getDataObject().setWidth(width);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
        }
    }

    public Integer getHeight()
    {
        if(getDataObject() != null) {
            return getDataObject().getHeight();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHeight(Integer height)
    {
        if(getDataObject() != null) {
            getDataObject().setHeight(height);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgImageStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecureUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public OgImageStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
