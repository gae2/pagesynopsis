package com.pagesynopsis.ws.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface ImageSetService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    ImageSet getImageSet(String guid) throws BaseException;
    Object getImageSet(String guid, String field) throws BaseException;
    List<ImageSet> getImageSets(List<String> guids) throws BaseException;
    List<ImageSet> getAllImageSets() throws BaseException;
    /* @Deprecated */ List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count) throws BaseException;
    List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllImageSetKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllImageSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createImageSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException;
    //String createImageSet(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ImageSet?)
    String createImageSet(ImageSet imageSet) throws BaseException;          // Returns Guid.  (Return ImageSet?)
    Boolean updateImageSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException;
    //Boolean updateImageSet(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateImageSet(ImageSet imageSet) throws BaseException;
    Boolean deleteImageSet(String guid) throws BaseException;
    Boolean deleteImageSet(ImageSet imageSet) throws BaseException;
    Long deleteImageSets(String filter, String params, List<String> values) throws BaseException;

//    Integer createImageSets(List<ImageSet> imageSets) throws BaseException;
//    Boolean updateeImageSets(List<ImageSet> imageSets) throws BaseException;

}
