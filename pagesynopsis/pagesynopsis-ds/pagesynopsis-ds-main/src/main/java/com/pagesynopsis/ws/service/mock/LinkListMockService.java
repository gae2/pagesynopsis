package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.bean.UrlStructBean;
import com.pagesynopsis.ws.bean.ImageStructBean;
import com.pagesynopsis.ws.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.bean.AnchorStructBean;
import com.pagesynopsis.ws.bean.KeyValuePairStructBean;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.bean.AudioStructBean;
import com.pagesynopsis.ws.bean.VideoStructBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.bean.LinkListBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.LinkListDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.LinkListService;


// LinkListMockService is a decorator.
// It can be used as a base class to mock LinkListService objects.
public abstract class LinkListMockService implements LinkListService
{
    private static final Logger log = Logger.getLogger(LinkListMockService.class.getName());

    // LinkListMockService uses the decorator design pattern.
    private LinkListService decoratedService;

    public LinkListMockService(LinkListService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected LinkListService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(LinkListService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // LinkList related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public LinkList getLinkList(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getLinkList(): guid = " + guid);
        LinkList bean = decoratedService.getLinkList(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getLinkList(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getLinkList(guid, field);
        return obj;
    }

    @Override
    public List<LinkList> getLinkLists(List<String> guids) throws BaseException
    {
        log.fine("getLinkLists()");
        List<LinkList> linkLists = decoratedService.getLinkLists(guids);
        log.finer("END");
        return linkLists;
    }

    @Override
    public List<LinkList> getAllLinkLists() throws BaseException
    {
        return getAllLinkLists(null, null, null);
    }


    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllLinkLists(ordering, offset, count, null);
    }

    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkLists(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<LinkList> linkLists = decoratedService.getAllLinkLists(ordering, offset, count, forwardCursor);
        log.finer("END");
        return linkLists;
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllLinkListKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkListKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllLinkListKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkListMockService.findLinkLists(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<LinkList> linkLists = decoratedService.findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return linkLists;
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkListMockService.findLinkListKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkListMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createLinkList(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        return decoratedService.createLinkList(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
    }

    @Override
    public String createLinkList(LinkList linkList) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createLinkList(linkList);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateLinkList(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        return decoratedService.updateLinkList(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
    }
        
    @Override
    public Boolean updateLinkList(LinkList linkList) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateLinkList(linkList);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteLinkList(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteLinkList(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteLinkList(LinkList linkList) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteLinkList(linkList);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteLinkLists(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteLinkLists(filter, params, values);
        return count;
    }

}
