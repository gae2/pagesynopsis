package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgQuantityStruct;
import com.pagesynopsis.ws.bean.OgQuantityStructBean;
import com.pagesynopsis.ws.stub.OgQuantityStructStub;


public class OgQuantityStructResourceUtil
{
    private static final Logger log = Logger.getLogger(OgQuantityStructResourceUtil.class.getName());

    // Static methods only.
    private OgQuantityStructResourceUtil() {}

    public static OgQuantityStructBean convertOgQuantityStructStubToBean(OgQuantityStruct stub)
    {
        OgQuantityStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null OgQuantityStructBean is returned.");
        } else {
            bean = new OgQuantityStructBean();
            bean.setUuid(stub.getUuid());
            bean.setValue(stub.getValue());
            bean.setUnits(stub.getUnits());
        }
        return bean;
    }

}
