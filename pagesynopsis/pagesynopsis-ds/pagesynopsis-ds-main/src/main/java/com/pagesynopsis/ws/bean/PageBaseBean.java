package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageBase;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.PageBaseDataObject;

public abstract class PageBaseBean extends BeanBase implements PageBase
{
    private static final Logger log = Logger.getLogger(PageBaseBean.class.getName());

    public PageBaseBean()
    {
        super();
    }

    @Override
    public abstract PageBaseDataObject getDataObject();

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getFetchRequest()
    {
        if(getDataObject() != null) {
            return getDataObject().getFetchRequest();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setFetchRequest(String fetchRequest)
    {
        if(getDataObject() != null) {
            getDataObject().setFetchRequest(fetchRequest);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getTargetUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getTargetUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setTargetUrl(String targetUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setTargetUrl(targetUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getPageUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getPageUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setPageUrl(String pageUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setPageUrl(pageUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getQueryString()
    {
        if(getDataObject() != null) {
            return getDataObject().getQueryString();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setQueryString(String queryString)
    {
        if(getDataObject() != null) {
            getDataObject().setQueryString(queryString);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public List<KeyValuePairStruct> getQueryParams()
    {
        if(getDataObject() != null) {
            List<KeyValuePairStruct> list = getDataObject().getQueryParams();
            if(list != null) {
                List<KeyValuePairStruct> bean = new ArrayList<KeyValuePairStruct>();
                for(KeyValuePairStruct keyValuePairStruct : list) {
                    KeyValuePairStructBean elem = null;
                    if(keyValuePairStruct instanceof KeyValuePairStructBean) {
                        elem = (KeyValuePairStructBean) keyValuePairStruct;
                    } else if(keyValuePairStruct instanceof KeyValuePairStructDataObject) {
                        elem = new KeyValuePairStructBean((KeyValuePairStructDataObject) keyValuePairStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;
        }
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        if(queryParams != null) {
            if(getDataObject() != null) {
                List<KeyValuePairStruct> dataObj = new ArrayList<KeyValuePairStruct>();
                for(KeyValuePairStruct keyValuePairStruct : queryParams) {
                    KeyValuePairStructDataObject elem = null;
                    if(keyValuePairStruct instanceof KeyValuePairStructBean) {
                        elem = ((KeyValuePairStructBean) keyValuePairStruct).toDataObject();
                    } else if(keyValuePairStruct instanceof KeyValuePairStructDataObject) {
                        elem = (KeyValuePairStructDataObject) keyValuePairStruct;
                    } else if(keyValuePairStruct instanceof KeyValuePairStruct) {
                        elem = new KeyValuePairStructDataObject(keyValuePairStruct.getUuid(), keyValuePairStruct.getKey(), keyValuePairStruct.getValue(), keyValuePairStruct.getNote());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setQueryParams(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setQueryParams(queryParams);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            }
        }
    }

    public String getLastFetchResult()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastFetchResult();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        if(getDataObject() != null) {
            getDataObject().setLastFetchResult(lastFetchResult);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public Integer getResponseCode()
    {
        if(getDataObject() != null) {
            return getDataObject().getResponseCode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setResponseCode(Integer responseCode)
    {
        if(getDataObject() != null) {
            getDataObject().setResponseCode(responseCode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getContentType()
    {
        if(getDataObject() != null) {
            return getDataObject().getContentType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setContentType(String contentType)
    {
        if(getDataObject() != null) {
            getDataObject().setContentType(contentType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public Integer getContentLength()
    {
        if(getDataObject() != null) {
            return getDataObject().getContentLength();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setContentLength(Integer contentLength)
    {
        if(getDataObject() != null) {
            getDataObject().setContentLength(contentLength);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getLanguage()
    {
        if(getDataObject() != null) {
            return getDataObject().getLanguage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setLanguage(String language)
    {
        if(getDataObject() != null) {
            getDataObject().setLanguage(language);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getRedirect()
    {
        if(getDataObject() != null) {
            return getDataObject().getRedirect();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setRedirect(String redirect)
    {
        if(getDataObject() != null) {
            getDataObject().setRedirect(redirect);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getLocation()
    {
        if(getDataObject() != null) {
            return getDataObject().getLocation();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setLocation(String location)
    {
        if(getDataObject() != null) {
            getDataObject().setLocation(location);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getPageTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getPageTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setPageTitle(String pageTitle)
    {
        if(getDataObject() != null) {
            getDataObject().setPageTitle(pageTitle);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public Boolean isDeferred()
    {
        if(getDataObject() != null) {
            return getDataObject().isDeferred();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setDeferred(Boolean deferred)
    {
        if(getDataObject() != null) {
            getDataObject().setDeferred(deferred);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public Integer getRefreshStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getRefreshStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        if(getDataObject() != null) {
            getDataObject().setRefreshStatus(refreshStatus);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public Long getRefreshInterval()
    {
        if(getDataObject() != null) {
            return getDataObject().getRefreshInterval();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        if(getDataObject() != null) {
            getDataObject().setRefreshInterval(refreshInterval);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public Long getNextRefreshTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getNextRefreshTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        if(getDataObject() != null) {
            getDataObject().setNextRefreshTime(nextRefreshTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public Long getLastCheckedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastCheckedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastCheckedTime(lastCheckedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }

    public Long getLastUpdatedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastUpdatedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastUpdatedTime(lastUpdatedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded PageBaseDataObject is null!");
        }
    }



    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
