package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.ImageSetDAO;
import com.pagesynopsis.ws.data.ImageSetDataObject;


// MockImageSetDAO is a decorator.
// It can be used as a base class to mock ImageSetDAO objects.
public abstract class MockImageSetDAO implements ImageSetDAO
{
    private static final Logger log = Logger.getLogger(MockImageSetDAO.class.getName()); 

    // MockImageSetDAO uses the decorator design pattern.
    private ImageSetDAO decoratedDAO;

    public MockImageSetDAO(ImageSetDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected ImageSetDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(ImageSetDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public ImageSetDataObject getImageSet(String guid) throws BaseException
    {
        return decoratedDAO.getImageSet(guid);
	}

    @Override
    public List<ImageSetDataObject> getImageSets(List<String> guids) throws BaseException
    {
        return decoratedDAO.getImageSets(guids);
    }

    @Override
    public List<ImageSetDataObject> getAllImageSets() throws BaseException
	{
	    return getAllImageSets(null, null, null);
    }


    @Override
    public List<ImageSetDataObject> getAllImageSets(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllImageSets(ordering, offset, count, null);
    }

    @Override
    public List<ImageSetDataObject> getAllImageSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllImageSets(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllImageSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllImageSetKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<ImageSetDataObject> findImageSets(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findImageSets(filter, ordering, params, values, null, null);
    }

    @Override
	public List<ImageSetDataObject> findImageSets(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findImageSets(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<ImageSetDataObject> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findImageSets(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<ImageSetDataObject> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findImageSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createImageSet(ImageSetDataObject imageSet) throws BaseException
    {
        return decoratedDAO.createImageSet( imageSet);
    }

    @Override
	public Boolean updateImageSet(ImageSetDataObject imageSet) throws BaseException
	{
        return decoratedDAO.updateImageSet(imageSet);
	}
	
    @Override
    public Boolean deleteImageSet(ImageSetDataObject imageSet) throws BaseException
    {
        return decoratedDAO.deleteImageSet(imageSet);
    }

    @Override
    public Boolean deleteImageSet(String guid) throws BaseException
    {
        return decoratedDAO.deleteImageSet(guid);
	}

    @Override
    public Long deleteImageSets(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteImageSets(filter, params, values);
    }

}
