package com.pagesynopsis.ws.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface RobotsTextFileService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    RobotsTextFile getRobotsTextFile(String guid) throws BaseException;
    Object getRobotsTextFile(String guid, String field) throws BaseException;
    List<RobotsTextFile> getRobotsTextFiles(List<String> guids) throws BaseException;
    List<RobotsTextFile> getAllRobotsTextFiles() throws BaseException;
    /* @Deprecated */ List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws BaseException;
    List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createRobotsTextFile(String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException;
    //String createRobotsTextFile(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return RobotsTextFile?)
    String createRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException;          // Returns Guid.  (Return RobotsTextFile?)
    Boolean updateRobotsTextFile(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException;
    //Boolean updateRobotsTextFile(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException;
    Boolean deleteRobotsTextFile(String guid) throws BaseException;
    Boolean deleteRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException;
    Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseException;

//    Integer createRobotsTextFiles(List<RobotsTextFile> robotsTextFiles) throws BaseException;
//    Boolean updateeRobotsTextFiles(List<RobotsTextFile> robotsTextFiles) throws BaseException;

}
