package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.bean.VideoStructBean;
import com.pagesynopsis.ws.stub.VideoStructStub;


public class VideoStructResourceUtil
{
    private static final Logger log = Logger.getLogger(VideoStructResourceUtil.class.getName());

    // Static methods only.
    private VideoStructResourceUtil() {}

    public static VideoStructBean convertVideoStructStubToBean(VideoStruct stub)
    {
        VideoStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null VideoStructBean is returned.");
        } else {
            bean = new VideoStructBean();
            bean.setUuid(stub.getUuid());
            bean.setId(stub.getId());
            bean.setWidth(stub.getWidth());
            bean.setHeight(stub.getHeight());
            bean.setControls(stub.getControls());
            bean.setAutoplayEnabled(stub.isAutoplayEnabled());
            bean.setLoopEnabled(stub.isLoopEnabled());
            bean.setPreloadEnabled(stub.isPreloadEnabled());
            bean.setMuted(stub.isMuted());
            bean.setRemark(stub.getRemark());
            bean.setSource(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource()));
            bean.setSource1(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource1()));
            bean.setSource2(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource2()));
            bean.setSource3(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource3()));
            bean.setSource4(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource4()));
            bean.setSource5(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource5()));
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
