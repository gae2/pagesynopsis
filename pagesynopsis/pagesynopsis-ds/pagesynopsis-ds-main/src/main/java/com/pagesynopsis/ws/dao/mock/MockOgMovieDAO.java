package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OgMovieDAO;
import com.pagesynopsis.ws.data.OgMovieDataObject;


// MockOgMovieDAO is a decorator.
// It can be used as a base class to mock OgMovieDAO objects.
public abstract class MockOgMovieDAO implements OgMovieDAO
{
    private static final Logger log = Logger.getLogger(MockOgMovieDAO.class.getName()); 

    // MockOgMovieDAO uses the decorator design pattern.
    private OgMovieDAO decoratedDAO;

    public MockOgMovieDAO(OgMovieDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected OgMovieDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(OgMovieDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public OgMovieDataObject getOgMovie(String guid) throws BaseException
    {
        return decoratedDAO.getOgMovie(guid);
	}

    @Override
    public List<OgMovieDataObject> getOgMovies(List<String> guids) throws BaseException
    {
        return decoratedDAO.getOgMovies(guids);
    }

    @Override
    public List<OgMovieDataObject> getAllOgMovies() throws BaseException
	{
	    return getAllOgMovies(null, null, null);
    }


    @Override
    public List<OgMovieDataObject> getAllOgMovies(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOgMovies(ordering, offset, count, null);
    }

    @Override
    public List<OgMovieDataObject> getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllOgMovies(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgMovieKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllOgMovieKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<OgMovieDataObject> findOgMovies(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOgMovies(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OgMovieDataObject> findOgMovies(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOgMovies(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OgMovieDataObject> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<OgMovieDataObject> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgMovie(OgMovieDataObject ogMovie) throws BaseException
    {
        return decoratedDAO.createOgMovie( ogMovie);
    }

    @Override
	public Boolean updateOgMovie(OgMovieDataObject ogMovie) throws BaseException
	{
        return decoratedDAO.updateOgMovie(ogMovie);
	}
	
    @Override
    public Boolean deleteOgMovie(OgMovieDataObject ogMovie) throws BaseException
    {
        return decoratedDAO.deleteOgMovie(ogMovie);
    }

    @Override
    public Boolean deleteOgMovie(String guid) throws BaseException
    {
        return decoratedDAO.deleteOgMovie(guid);
	}

    @Override
    public Long deleteOgMovies(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteOgMovies(filter, params, values);
    }

}
