package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OgTvEpisodeDAO;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;


// MockOgTvEpisodeDAO is a decorator.
// It can be used as a base class to mock OgTvEpisodeDAO objects.
public abstract class MockOgTvEpisodeDAO implements OgTvEpisodeDAO
{
    private static final Logger log = Logger.getLogger(MockOgTvEpisodeDAO.class.getName()); 

    // MockOgTvEpisodeDAO uses the decorator design pattern.
    private OgTvEpisodeDAO decoratedDAO;

    public MockOgTvEpisodeDAO(OgTvEpisodeDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected OgTvEpisodeDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(OgTvEpisodeDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public OgTvEpisodeDataObject getOgTvEpisode(String guid) throws BaseException
    {
        return decoratedDAO.getOgTvEpisode(guid);
	}

    @Override
    public List<OgTvEpisodeDataObject> getOgTvEpisodes(List<String> guids) throws BaseException
    {
        return decoratedDAO.getOgTvEpisodes(guids);
    }

    @Override
    public List<OgTvEpisodeDataObject> getAllOgTvEpisodes() throws BaseException
	{
	    return getAllOgTvEpisodes(null, null, null);
    }


    @Override
    public List<OgTvEpisodeDataObject> getAllOgTvEpisodes(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOgTvEpisodes(ordering, offset, count, null);
    }

    @Override
    public List<OgTvEpisodeDataObject> getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllOgTvEpisodes(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvEpisodeKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllOgTvEpisodeKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOgTvEpisodes(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOgTvEpisodes(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgTvEpisode(OgTvEpisodeDataObject ogTvEpisode) throws BaseException
    {
        return decoratedDAO.createOgTvEpisode( ogTvEpisode);
    }

    @Override
	public Boolean updateOgTvEpisode(OgTvEpisodeDataObject ogTvEpisode) throws BaseException
	{
        return decoratedDAO.updateOgTvEpisode(ogTvEpisode);
	}
	
    @Override
    public Boolean deleteOgTvEpisode(OgTvEpisodeDataObject ogTvEpisode) throws BaseException
    {
        return decoratedDAO.deleteOgTvEpisode(ogTvEpisode);
    }

    @Override
    public Boolean deleteOgTvEpisode(String guid) throws BaseException
    {
        return decoratedDAO.deleteOgTvEpisode(guid);
	}

    @Override
    public Long deleteOgTvEpisodes(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteOgTvEpisodes(filter, params, values);
    }

}
