package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.DecodedQueryParamStruct;
import com.pagesynopsis.ws.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.ws.stub.DecodedQueryParamStructStub;


public class DecodedQueryParamStructResourceUtil
{
    private static final Logger log = Logger.getLogger(DecodedQueryParamStructResourceUtil.class.getName());

    // Static methods only.
    private DecodedQueryParamStructResourceUtil() {}

    public static DecodedQueryParamStructBean convertDecodedQueryParamStructStubToBean(DecodedQueryParamStruct stub)
    {
        DecodedQueryParamStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null DecodedQueryParamStructBean is returned.");
        } else {
            bean = new DecodedQueryParamStructBean();
            bean.setParamType(stub.getParamType());
            bean.setOriginalString(stub.getOriginalString());
            bean.setDecodedString(stub.getDecodedString());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
