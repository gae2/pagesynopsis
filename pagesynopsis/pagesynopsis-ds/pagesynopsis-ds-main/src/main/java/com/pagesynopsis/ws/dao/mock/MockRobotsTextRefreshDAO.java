package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.RobotsTextRefreshDAO;
import com.pagesynopsis.ws.data.RobotsTextRefreshDataObject;


// MockRobotsTextRefreshDAO is a decorator.
// It can be used as a base class to mock RobotsTextRefreshDAO objects.
public abstract class MockRobotsTextRefreshDAO implements RobotsTextRefreshDAO
{
    private static final Logger log = Logger.getLogger(MockRobotsTextRefreshDAO.class.getName()); 

    // MockRobotsTextRefreshDAO uses the decorator design pattern.
    private RobotsTextRefreshDAO decoratedDAO;

    public MockRobotsTextRefreshDAO(RobotsTextRefreshDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected RobotsTextRefreshDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(RobotsTextRefreshDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public RobotsTextRefreshDataObject getRobotsTextRefresh(String guid) throws BaseException
    {
        return decoratedDAO.getRobotsTextRefresh(guid);
	}

    @Override
    public List<RobotsTextRefreshDataObject> getRobotsTextRefreshes(List<String> guids) throws BaseException
    {
        return decoratedDAO.getRobotsTextRefreshes(guids);
    }

    @Override
    public List<RobotsTextRefreshDataObject> getAllRobotsTextRefreshes() throws BaseException
	{
	    return getAllRobotsTextRefreshes(null, null, null);
    }


    @Override
    public List<RobotsTextRefreshDataObject> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllRobotsTextRefreshes(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextRefreshDataObject> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextRefreshKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<RobotsTextRefreshDataObject> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findRobotsTextRefreshes(filter, ordering, params, values, null, null);
    }

    @Override
	public List<RobotsTextRefreshDataObject> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findRobotsTextRefreshes(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<RobotsTextRefreshDataObject> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<RobotsTextRefreshDataObject> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createRobotsTextRefresh(RobotsTextRefreshDataObject robotsTextRefresh) throws BaseException
    {
        return decoratedDAO.createRobotsTextRefresh( robotsTextRefresh);
    }

    @Override
	public Boolean updateRobotsTextRefresh(RobotsTextRefreshDataObject robotsTextRefresh) throws BaseException
	{
        return decoratedDAO.updateRobotsTextRefresh(robotsTextRefresh);
	}
	
    @Override
    public Boolean deleteRobotsTextRefresh(RobotsTextRefreshDataObject robotsTextRefresh) throws BaseException
    {
        return decoratedDAO.deleteRobotsTextRefresh(robotsTextRefresh);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(String guid) throws BaseException
    {
        return decoratedDAO.deleteRobotsTextRefresh(guid);
	}

    @Override
    public Long deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteRobotsTextRefreshes(filter, params, values);
    }

}
