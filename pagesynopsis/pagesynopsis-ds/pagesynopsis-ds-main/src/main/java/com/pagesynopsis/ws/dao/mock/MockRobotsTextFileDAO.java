package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.RobotsTextFileDAO;
import com.pagesynopsis.ws.data.RobotsTextFileDataObject;


// MockRobotsTextFileDAO is a decorator.
// It can be used as a base class to mock RobotsTextFileDAO objects.
public abstract class MockRobotsTextFileDAO implements RobotsTextFileDAO
{
    private static final Logger log = Logger.getLogger(MockRobotsTextFileDAO.class.getName()); 

    // MockRobotsTextFileDAO uses the decorator design pattern.
    private RobotsTextFileDAO decoratedDAO;

    public MockRobotsTextFileDAO(RobotsTextFileDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected RobotsTextFileDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(RobotsTextFileDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public RobotsTextFileDataObject getRobotsTextFile(String guid) throws BaseException
    {
        return decoratedDAO.getRobotsTextFile(guid);
	}

    @Override
    public List<RobotsTextFileDataObject> getRobotsTextFiles(List<String> guids) throws BaseException
    {
        return decoratedDAO.getRobotsTextFiles(guids);
    }

    @Override
    public List<RobotsTextFileDataObject> getAllRobotsTextFiles() throws BaseException
	{
	    return getAllRobotsTextFiles(null, null, null);
    }


    @Override
    public List<RobotsTextFileDataObject> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllRobotsTextFiles(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextFileDataObject> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllRobotsTextFiles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextFileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllRobotsTextFileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findRobotsTextFiles(filter, ordering, params, values, null, null);
    }

    @Override
	public List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findRobotsTextFiles(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createRobotsTextFile(RobotsTextFileDataObject robotsTextFile) throws BaseException
    {
        return decoratedDAO.createRobotsTextFile( robotsTextFile);
    }

    @Override
	public Boolean updateRobotsTextFile(RobotsTextFileDataObject robotsTextFile) throws BaseException
	{
        return decoratedDAO.updateRobotsTextFile(robotsTextFile);
	}
	
    @Override
    public Boolean deleteRobotsTextFile(RobotsTextFileDataObject robotsTextFile) throws BaseException
    {
        return decoratedDAO.deleteRobotsTextFile(robotsTextFile);
    }

    @Override
    public Boolean deleteRobotsTextFile(String guid) throws BaseException
    {
        return decoratedDAO.deleteRobotsTextFile(guid);
	}

    @Override
    public Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteRobotsTextFiles(filter, params, values);
    }

}
