package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;


public interface VideoSet extends PageBase
{
    List<String>  getMediaTypeFilter();
    Set<VideoStruct>  getPageVideos();
}
