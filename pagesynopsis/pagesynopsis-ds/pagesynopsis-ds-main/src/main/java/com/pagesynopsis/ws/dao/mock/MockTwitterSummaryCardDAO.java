package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.TwitterSummaryCardDAO;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;


// MockTwitterSummaryCardDAO is a decorator.
// It can be used as a base class to mock TwitterSummaryCardDAO objects.
public abstract class MockTwitterSummaryCardDAO implements TwitterSummaryCardDAO
{
    private static final Logger log = Logger.getLogger(MockTwitterSummaryCardDAO.class.getName()); 

    // MockTwitterSummaryCardDAO uses the decorator design pattern.
    private TwitterSummaryCardDAO decoratedDAO;

    public MockTwitterSummaryCardDAO(TwitterSummaryCardDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected TwitterSummaryCardDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(TwitterSummaryCardDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public TwitterSummaryCardDataObject getTwitterSummaryCard(String guid) throws BaseException
    {
        return decoratedDAO.getTwitterSummaryCard(guid);
	}

    @Override
    public List<TwitterSummaryCardDataObject> getTwitterSummaryCards(List<String> guids) throws BaseException
    {
        return decoratedDAO.getTwitterSummaryCards(guids);
    }

    @Override
    public List<TwitterSummaryCardDataObject> getAllTwitterSummaryCards() throws BaseException
	{
	    return getAllTwitterSummaryCards(null, null, null);
    }


    @Override
    public List<TwitterSummaryCardDataObject> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllTwitterSummaryCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterSummaryCardDataObject> getAllTwitterSummaryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllTwitterSummaryCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterSummaryCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllTwitterSummaryCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<TwitterSummaryCardDataObject> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTwitterSummaryCards(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TwitterSummaryCardDataObject> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTwitterSummaryCards(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<TwitterSummaryCardDataObject> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<TwitterSummaryCardDataObject> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterSummaryCard(TwitterSummaryCardDataObject twitterSummaryCard) throws BaseException
    {
        return decoratedDAO.createTwitterSummaryCard( twitterSummaryCard);
    }

    @Override
	public Boolean updateTwitterSummaryCard(TwitterSummaryCardDataObject twitterSummaryCard) throws BaseException
	{
        return decoratedDAO.updateTwitterSummaryCard(twitterSummaryCard);
	}
	
    @Override
    public Boolean deleteTwitterSummaryCard(TwitterSummaryCardDataObject twitterSummaryCard) throws BaseException
    {
        return decoratedDAO.deleteTwitterSummaryCard(twitterSummaryCard);
    }

    @Override
    public Boolean deleteTwitterSummaryCard(String guid) throws BaseException
    {
        return decoratedDAO.deleteTwitterSummaryCard(guid);
	}

    @Override
    public Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteTwitterSummaryCards(filter, params, values);
    }

}
