package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.OgVideoDataObject;


// TBD: Add offset/count to getAllOgVideos() and findOgVideos(), etc.
public interface OgVideoDAO
{
    OgVideoDataObject getOgVideo(String guid) throws BaseException;
    List<OgVideoDataObject> getOgVideos(List<String> guids) throws BaseException;
    List<OgVideoDataObject> getAllOgVideos() throws BaseException;
    /* @Deprecated */ List<OgVideoDataObject> getAllOgVideos(String ordering, Long offset, Integer count) throws BaseException;
    List<OgVideoDataObject> getAllOgVideos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgVideoDataObject> findOgVideos(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgVideoDataObject> findOgVideos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<OgVideoDataObject> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgVideoDataObject> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createOgVideo(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgVideoDataObject?)
    String createOgVideo(OgVideoDataObject ogVideo) throws BaseException;          // Returns Guid.  (Return OgVideoDataObject?)
    //Boolean updateOgVideo(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgVideo(OgVideoDataObject ogVideo) throws BaseException;
    Boolean deleteOgVideo(String guid) throws BaseException;
    Boolean deleteOgVideo(OgVideoDataObject ogVideo) throws BaseException;
    Long deleteOgVideos(String filter, String params, List<String> values) throws BaseException;
}
