package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class AudioStructDataObject implements AudioStruct, Serializable
{
    private static final Logger log = Logger.getLogger(AudioStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _audiostruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String id;

    @Persistent(defaultFetchGroup = "true")
    private String controls;

    @Persistent(defaultFetchGroup = "true")
    private Boolean autoplayEnabled;

    @Persistent(defaultFetchGroup = "true")
    private Boolean loopEnabled;

    @Persistent(defaultFetchGroup = "true")
    private Boolean preloadEnabled;

    @Persistent(defaultFetchGroup = "true")
    private String remark;

    @Persistent(defaultFetchGroup = "true")
    private MediaSourceStructDataObject source;

    @Persistent(defaultFetchGroup = "true")
    private MediaSourceStructDataObject source1;

    @Persistent(defaultFetchGroup = "true")
    private MediaSourceStructDataObject source2;

    @Persistent(defaultFetchGroup = "true")
    private MediaSourceStructDataObject source3;

    @Persistent(defaultFetchGroup = "true")
    private MediaSourceStructDataObject source4;

    @Persistent(defaultFetchGroup = "true")
    private MediaSourceStructDataObject source5;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public AudioStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public AudioStructDataObject(String uuid, String id, String controls, Boolean autoplayEnabled, Boolean loopEnabled, Boolean preloadEnabled, String remark, MediaSourceStruct source, MediaSourceStruct source1, MediaSourceStruct source2, MediaSourceStruct source3, MediaSourceStruct source4, MediaSourceStruct source5, String note)
    {
        setUuid(uuid);
        setId(id);
        setControls(controls);
        setAutoplayEnabled(autoplayEnabled);
        setLoopEnabled(loopEnabled);
        setPreloadEnabled(preloadEnabled);
        setRemark(remark);
        if(source != null) {
            this.source = new MediaSourceStructDataObject(source.getUuid(), source.getSrc(), source.getSrcUrl(), source.getType(), source.getRemark());
        } else {
            this.source = null;
        }
        if(source1 != null) {
            this.source1 = new MediaSourceStructDataObject(source1.getUuid(), source1.getSrc(), source1.getSrcUrl(), source1.getType(), source1.getRemark());
        } else {
            this.source1 = null;
        }
        if(source2 != null) {
            this.source2 = new MediaSourceStructDataObject(source2.getUuid(), source2.getSrc(), source2.getSrcUrl(), source2.getType(), source2.getRemark());
        } else {
            this.source2 = null;
        }
        if(source3 != null) {
            this.source3 = new MediaSourceStructDataObject(source3.getUuid(), source3.getSrc(), source3.getSrcUrl(), source3.getType(), source3.getRemark());
        } else {
            this.source3 = null;
        }
        if(source4 != null) {
            this.source4 = new MediaSourceStructDataObject(source4.getUuid(), source4.getSrc(), source4.getSrcUrl(), source4.getType(), source4.getRemark());
        } else {
            this.source4 = null;
        }
        if(source5 != null) {
            this.source5 = new MediaSourceStructDataObject(source5.getUuid(), source5.getSrc(), source5.getSrcUrl(), source5.getType(), source5.getRemark());
        } else {
            this.source5 = null;
        }
        setNote(note);
    }

    private void resetEncodedKey()
    {
        if(_audiostruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _audiostruct_encoded_key = KeyFactory.createKeyString(AudioStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _audiostruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _audiostruct_encoded_key = KeyFactory.createKeyString(AudioStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
        if(this.id != null) {
            resetEncodedKey();
        }
    }

    public String getControls()
    {
        return this.controls;
    }
    public void setControls(String controls)
    {
        this.controls = controls;
        if(this.controls != null) {
            resetEncodedKey();
        }
    }

    public Boolean isAutoplayEnabled()
    {
        return this.autoplayEnabled;
    }
    public void setAutoplayEnabled(Boolean autoplayEnabled)
    {
        this.autoplayEnabled = autoplayEnabled;
        if(this.autoplayEnabled != null) {
            resetEncodedKey();
        }
    }

    public Boolean isLoopEnabled()
    {
        return this.loopEnabled;
    }
    public void setLoopEnabled(Boolean loopEnabled)
    {
        this.loopEnabled = loopEnabled;
        if(this.loopEnabled != null) {
            resetEncodedKey();
        }
    }

    public Boolean isPreloadEnabled()
    {
        return this.preloadEnabled;
    }
    public void setPreloadEnabled(Boolean preloadEnabled)
    {
        this.preloadEnabled = preloadEnabled;
        if(this.preloadEnabled != null) {
            resetEncodedKey();
        }
    }

    public String getRemark()
    {
        return this.remark;
    }
    public void setRemark(String remark)
    {
        this.remark = remark;
        if(this.remark != null) {
            resetEncodedKey();
        }
    }

    public MediaSourceStruct getSource()
    {
        return this.source;
    }
    public void setSource(MediaSourceStruct source)
    {
        if(source == null) {
            this.source = null;
            log.log(Level.INFO, "AudioStructDataObject.setSource(MediaSourceStruct source): Arg source is null.");            
        } else if(source instanceof MediaSourceStructDataObject) {
            this.source = (MediaSourceStructDataObject) source;
        } else if(source instanceof MediaSourceStruct) {
            this.source = new MediaSourceStructDataObject(source.getUuid(), source.getSrc(), source.getSrcUrl(), source.getType(), source.getRemark());
        } else {
            this.source = new MediaSourceStructDataObject();   // ????
            log.log(Level.WARNING, "AudioStructDataObject.setSource(MediaSourceStruct source): Arg source is of an invalid type.");
        }
    }

    public MediaSourceStruct getSource1()
    {
        return this.source1;
    }
    public void setSource1(MediaSourceStruct source1)
    {
        if(source1 == null) {
            this.source1 = null;
            log.log(Level.INFO, "AudioStructDataObject.setSource1(MediaSourceStruct source1): Arg source1 is null.");            
        } else if(source1 instanceof MediaSourceStructDataObject) {
            this.source1 = (MediaSourceStructDataObject) source1;
        } else if(source1 instanceof MediaSourceStruct) {
            this.source1 = new MediaSourceStructDataObject(source1.getUuid(), source1.getSrc(), source1.getSrcUrl(), source1.getType(), source1.getRemark());
        } else {
            this.source1 = new MediaSourceStructDataObject();   // ????
            log.log(Level.WARNING, "AudioStructDataObject.setSource1(MediaSourceStruct source1): Arg source1 is of an invalid type.");
        }
    }

    public MediaSourceStruct getSource2()
    {
        return this.source2;
    }
    public void setSource2(MediaSourceStruct source2)
    {
        if(source2 == null) {
            this.source2 = null;
            log.log(Level.INFO, "AudioStructDataObject.setSource2(MediaSourceStruct source2): Arg source2 is null.");            
        } else if(source2 instanceof MediaSourceStructDataObject) {
            this.source2 = (MediaSourceStructDataObject) source2;
        } else if(source2 instanceof MediaSourceStruct) {
            this.source2 = new MediaSourceStructDataObject(source2.getUuid(), source2.getSrc(), source2.getSrcUrl(), source2.getType(), source2.getRemark());
        } else {
            this.source2 = new MediaSourceStructDataObject();   // ????
            log.log(Level.WARNING, "AudioStructDataObject.setSource2(MediaSourceStruct source2): Arg source2 is of an invalid type.");
        }
    }

    public MediaSourceStruct getSource3()
    {
        return this.source3;
    }
    public void setSource3(MediaSourceStruct source3)
    {
        if(source3 == null) {
            this.source3 = null;
            log.log(Level.INFO, "AudioStructDataObject.setSource3(MediaSourceStruct source3): Arg source3 is null.");            
        } else if(source3 instanceof MediaSourceStructDataObject) {
            this.source3 = (MediaSourceStructDataObject) source3;
        } else if(source3 instanceof MediaSourceStruct) {
            this.source3 = new MediaSourceStructDataObject(source3.getUuid(), source3.getSrc(), source3.getSrcUrl(), source3.getType(), source3.getRemark());
        } else {
            this.source3 = new MediaSourceStructDataObject();   // ????
            log.log(Level.WARNING, "AudioStructDataObject.setSource3(MediaSourceStruct source3): Arg source3 is of an invalid type.");
        }
    }

    public MediaSourceStruct getSource4()
    {
        return this.source4;
    }
    public void setSource4(MediaSourceStruct source4)
    {
        if(source4 == null) {
            this.source4 = null;
            log.log(Level.INFO, "AudioStructDataObject.setSource4(MediaSourceStruct source4): Arg source4 is null.");            
        } else if(source4 instanceof MediaSourceStructDataObject) {
            this.source4 = (MediaSourceStructDataObject) source4;
        } else if(source4 instanceof MediaSourceStruct) {
            this.source4 = new MediaSourceStructDataObject(source4.getUuid(), source4.getSrc(), source4.getSrcUrl(), source4.getType(), source4.getRemark());
        } else {
            this.source4 = new MediaSourceStructDataObject();   // ????
            log.log(Level.WARNING, "AudioStructDataObject.setSource4(MediaSourceStruct source4): Arg source4 is of an invalid type.");
        }
    }

    public MediaSourceStruct getSource5()
    {
        return this.source5;
    }
    public void setSource5(MediaSourceStruct source5)
    {
        if(source5 == null) {
            this.source5 = null;
            log.log(Level.INFO, "AudioStructDataObject.setSource5(MediaSourceStruct source5): Arg source5 is null.");            
        } else if(source5 instanceof MediaSourceStructDataObject) {
            this.source5 = (MediaSourceStructDataObject) source5;
        } else if(source5 instanceof MediaSourceStruct) {
            this.source5 = new MediaSourceStructDataObject(source5.getUuid(), source5.getSrc(), source5.getSrcUrl(), source5.getType(), source5.getRemark());
        } else {
            this.source5 = new MediaSourceStructDataObject();   // ????
            log.log(Level.WARNING, "AudioStructDataObject.setSource5(MediaSourceStruct source5): Arg source5 is of an invalid type.");
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getControls() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isAutoplayEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isLoopEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isPreloadEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRemark() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource3() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource4() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource5() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("id", this.id);
        dataMap.put("controls", this.controls);
        dataMap.put("autoplayEnabled", this.autoplayEnabled);
        dataMap.put("loopEnabled", this.loopEnabled);
        dataMap.put("preloadEnabled", this.preloadEnabled);
        dataMap.put("remark", this.remark);
        dataMap.put("source", this.source);
        dataMap.put("source1", this.source1);
        dataMap.put("source2", this.source2);
        dataMap.put("source3", this.source3);
        dataMap.put("source4", this.source4);
        dataMap.put("source5", this.source5);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        AudioStruct thatObj = (AudioStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.id == null && thatObj.getId() != null)
            || (this.id != null && thatObj.getId() == null)
            || !this.id.equals(thatObj.getId()) ) {
            return false;
        }
        if( (this.controls == null && thatObj.getControls() != null)
            || (this.controls != null && thatObj.getControls() == null)
            || !this.controls.equals(thatObj.getControls()) ) {
            return false;
        }
        if( (this.autoplayEnabled == null && thatObj.isAutoplayEnabled() != null)
            || (this.autoplayEnabled != null && thatObj.isAutoplayEnabled() == null)
            || !this.autoplayEnabled.equals(thatObj.isAutoplayEnabled()) ) {
            return false;
        }
        if( (this.loopEnabled == null && thatObj.isLoopEnabled() != null)
            || (this.loopEnabled != null && thatObj.isLoopEnabled() == null)
            || !this.loopEnabled.equals(thatObj.isLoopEnabled()) ) {
            return false;
        }
        if( (this.preloadEnabled == null && thatObj.isPreloadEnabled() != null)
            || (this.preloadEnabled != null && thatObj.isPreloadEnabled() == null)
            || !this.preloadEnabled.equals(thatObj.isPreloadEnabled()) ) {
            return false;
        }
        if( (this.remark == null && thatObj.getRemark() != null)
            || (this.remark != null && thatObj.getRemark() == null)
            || !this.remark.equals(thatObj.getRemark()) ) {
            return false;
        }
        if( (this.source == null && thatObj.getSource() != null)
            || (this.source != null && thatObj.getSource() == null)
            || !this.source.equals(thatObj.getSource()) ) {
            return false;
        }
        if( (this.source1 == null && thatObj.getSource1() != null)
            || (this.source1 != null && thatObj.getSource1() == null)
            || !this.source1.equals(thatObj.getSource1()) ) {
            return false;
        }
        if( (this.source2 == null && thatObj.getSource2() != null)
            || (this.source2 != null && thatObj.getSource2() == null)
            || !this.source2.equals(thatObj.getSource2()) ) {
            return false;
        }
        if( (this.source3 == null && thatObj.getSource3() != null)
            || (this.source3 != null && thatObj.getSource3() == null)
            || !this.source3.equals(thatObj.getSource3()) ) {
            return false;
        }
        if( (this.source4 == null && thatObj.getSource4() != null)
            || (this.source4 != null && thatObj.getSource4() == null)
            || !this.source4.equals(thatObj.getSource4()) ) {
            return false;
        }
        if( (this.source5 == null && thatObj.getSource5() != null)
            || (this.source5 != null && thatObj.getSource5() == null)
            || !this.source5.equals(thatObj.getSource5()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = id == null ? 0 : id.hashCode();
        _hash = 31 * _hash + delta;
        delta = controls == null ? 0 : controls.hashCode();
        _hash = 31 * _hash + delta;
        delta = autoplayEnabled == null ? 0 : autoplayEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = loopEnabled == null ? 0 : loopEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = preloadEnabled == null ? 0 : preloadEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = remark == null ? 0 : remark.hashCode();
        _hash = 31 * _hash + delta;
        delta = source == null ? 0 : source.hashCode();
        _hash = 31 * _hash + delta;
        delta = source1 == null ? 0 : source1.hashCode();
        _hash = 31 * _hash + delta;
        delta = source2 == null ? 0 : source2.hashCode();
        _hash = 31 * _hash + delta;
        delta = source3 == null ? 0 : source3.hashCode();
        _hash = 31 * _hash + delta;
        delta = source4 == null ? 0 : source4.hashCode();
        _hash = 31 * _hash + delta;
        delta = source5 == null ? 0 : source5.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
