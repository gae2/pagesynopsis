package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgObjectBase;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgObjectBaseDataObject;

public abstract class OgObjectBaseBean extends BeanBase implements OgObjectBase
{
    private static final Logger log = Logger.getLogger(OgObjectBaseBean.class.getName());

    public OgObjectBaseBean()
    {
        super();
    }

    @Override
    public abstract OgObjectBaseDataObject getDataObject();

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
        }
    }

    public String getUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setUrl(String url)
    {
        if(getDataObject() != null) {
            getDataObject().setUrl(url);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
        }
    }

    public String getType()
    {
        if(getDataObject() != null) {
            return getDataObject().getType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setType(String type)
    {
        if(getDataObject() != null) {
            getDataObject().setType(type);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
        }
    }

    public String getSiteName()
    {
        if(getDataObject() != null) {
            return getDataObject().getSiteName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setSiteName(String siteName)
    {
        if(getDataObject() != null) {
            getDataObject().setSiteName(siteName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
        }
    }

    public String getTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setTitle(String title)
    {
        if(getDataObject() != null) {
            getDataObject().setTitle(title);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
        }
    }

    public String getDescription()
    {
        if(getDataObject() != null) {
            return getDataObject().getDescription();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setDescription(String description)
    {
        if(getDataObject() != null) {
            getDataObject().setDescription(description);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
        }
    }

    public List<String> getFbAdmins()
    {
        if(getDataObject() != null) {
            return getDataObject().getFbAdmins();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setFbAdmins(List<String> fbAdmins)
    {
        if(getDataObject() != null) {
            getDataObject().setFbAdmins(fbAdmins);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
        }
    }

    public List<String> getFbAppId()
    {
        if(getDataObject() != null) {
            return getDataObject().getFbAppId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setFbAppId(List<String> fbAppId)
    {
        if(getDataObject() != null) {
            getDataObject().setFbAppId(fbAppId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
        }
    }

    public List<OgImageStruct> getImage()
    {
        if(getDataObject() != null) {
            List<OgImageStruct> list = getDataObject().getImage();
            if(list != null) {
                List<OgImageStruct> bean = new ArrayList<OgImageStruct>();
                for(OgImageStruct ogImageStruct : list) {
                    OgImageStructBean elem = null;
                    if(ogImageStruct instanceof OgImageStructBean) {
                        elem = (OgImageStructBean) ogImageStruct;
                    } else if(ogImageStruct instanceof OgImageStructDataObject) {
                        elem = new OgImageStructBean((OgImageStructDataObject) ogImageStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;
        }
    }
    public void setImage(List<OgImageStruct> image)
    {
        if(image != null) {
            if(getDataObject() != null) {
                List<OgImageStruct> dataObj = new ArrayList<OgImageStruct>();
                for(OgImageStruct ogImageStruct : image) {
                    OgImageStructDataObject elem = null;
                    if(ogImageStruct instanceof OgImageStructBean) {
                        elem = ((OgImageStructBean) ogImageStruct).toDataObject();
                    } else if(ogImageStruct instanceof OgImageStructDataObject) {
                        elem = (OgImageStructDataObject) ogImageStruct;
                    } else if(ogImageStruct instanceof OgImageStruct) {
                        elem = new OgImageStructDataObject(ogImageStruct.getUuid(), ogImageStruct.getUrl(), ogImageStruct.getSecureUrl(), ogImageStruct.getType(), ogImageStruct.getWidth(), ogImageStruct.getHeight());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setImage(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setImage(image);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            }
        }
    }

    public List<OgAudioStruct> getAudio()
    {
        if(getDataObject() != null) {
            List<OgAudioStruct> list = getDataObject().getAudio();
            if(list != null) {
                List<OgAudioStruct> bean = new ArrayList<OgAudioStruct>();
                for(OgAudioStruct ogAudioStruct : list) {
                    OgAudioStructBean elem = null;
                    if(ogAudioStruct instanceof OgAudioStructBean) {
                        elem = (OgAudioStructBean) ogAudioStruct;
                    } else if(ogAudioStruct instanceof OgAudioStructDataObject) {
                        elem = new OgAudioStructBean((OgAudioStructDataObject) ogAudioStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;
        }
    }
    public void setAudio(List<OgAudioStruct> audio)
    {
        if(audio != null) {
            if(getDataObject() != null) {
                List<OgAudioStruct> dataObj = new ArrayList<OgAudioStruct>();
                for(OgAudioStruct ogAudioStruct : audio) {
                    OgAudioStructDataObject elem = null;
                    if(ogAudioStruct instanceof OgAudioStructBean) {
                        elem = ((OgAudioStructBean) ogAudioStruct).toDataObject();
                    } else if(ogAudioStruct instanceof OgAudioStructDataObject) {
                        elem = (OgAudioStructDataObject) ogAudioStruct;
                    } else if(ogAudioStruct instanceof OgAudioStruct) {
                        elem = new OgAudioStructDataObject(ogAudioStruct.getUuid(), ogAudioStruct.getUrl(), ogAudioStruct.getSecureUrl(), ogAudioStruct.getType());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setAudio(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setAudio(audio);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            }
        }
    }

    public List<OgVideoStruct> getVideo()
    {
        if(getDataObject() != null) {
            List<OgVideoStruct> list = getDataObject().getVideo();
            if(list != null) {
                List<OgVideoStruct> bean = new ArrayList<OgVideoStruct>();
                for(OgVideoStruct ogVideoStruct : list) {
                    OgVideoStructBean elem = null;
                    if(ogVideoStruct instanceof OgVideoStructBean) {
                        elem = (OgVideoStructBean) ogVideoStruct;
                    } else if(ogVideoStruct instanceof OgVideoStructDataObject) {
                        elem = new OgVideoStructBean((OgVideoStructDataObject) ogVideoStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;
        }
    }
    public void setVideo(List<OgVideoStruct> video)
    {
        if(video != null) {
            if(getDataObject() != null) {
                List<OgVideoStruct> dataObj = new ArrayList<OgVideoStruct>();
                for(OgVideoStruct ogVideoStruct : video) {
                    OgVideoStructDataObject elem = null;
                    if(ogVideoStruct instanceof OgVideoStructBean) {
                        elem = ((OgVideoStructBean) ogVideoStruct).toDataObject();
                    } else if(ogVideoStruct instanceof OgVideoStructDataObject) {
                        elem = (OgVideoStructDataObject) ogVideoStruct;
                    } else if(ogVideoStruct instanceof OgVideoStruct) {
                        elem = new OgVideoStructDataObject(ogVideoStruct.getUuid(), ogVideoStruct.getUrl(), ogVideoStruct.getSecureUrl(), ogVideoStruct.getType(), ogVideoStruct.getWidth(), ogVideoStruct.getHeight());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setVideo(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setVideo(video);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            }
        }
    }

    public String getLocale()
    {
        if(getDataObject() != null) {
            return getDataObject().getLocale();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setLocale(String locale)
    {
        if(getDataObject() != null) {
            getDataObject().setLocale(locale);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
        }
    }

    public List<String> getLocaleAlternate()
    {
        if(getDataObject() != null) {
            return getDataObject().getLocaleAlternate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
            return null;   // ???
        }
    }
    public void setLocaleAlternate(List<String> localeAlternate)
    {
        if(getDataObject() != null) {
            getDataObject().setLocaleAlternate(localeAlternate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgObjectBaseDataObject is null!");
        }
    }



    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
