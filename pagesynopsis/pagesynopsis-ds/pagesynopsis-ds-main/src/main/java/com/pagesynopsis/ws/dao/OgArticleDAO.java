package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.OgArticleDataObject;


// TBD: Add offset/count to getAllOgArticles() and findOgArticles(), etc.
public interface OgArticleDAO
{
    OgArticleDataObject getOgArticle(String guid) throws BaseException;
    List<OgArticleDataObject> getOgArticles(List<String> guids) throws BaseException;
    List<OgArticleDataObject> getAllOgArticles() throws BaseException;
    /* @Deprecated */ List<OgArticleDataObject> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException;
    List<OgArticleDataObject> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createOgArticle(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgArticleDataObject?)
    String createOgArticle(OgArticleDataObject ogArticle) throws BaseException;          // Returns Guid.  (Return OgArticleDataObject?)
    //Boolean updateOgArticle(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgArticle(OgArticleDataObject ogArticle) throws BaseException;
    Boolean deleteOgArticle(String guid) throws BaseException;
    Boolean deleteOgArticle(OgArticleDataObject ogArticle) throws BaseException;
    Long deleteOgArticles(String filter, String params, List<String> values) throws BaseException;
}
