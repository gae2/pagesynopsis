package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgProfileService;


// OgProfileMockService is a decorator.
// It can be used as a base class to mock OgProfileService objects.
public abstract class OgProfileMockService implements OgProfileService
{
    private static final Logger log = Logger.getLogger(OgProfileMockService.class.getName());

    // OgProfileMockService uses the decorator design pattern.
    private OgProfileService decoratedService;

    public OgProfileMockService(OgProfileService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected OgProfileService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(OgProfileService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // OgProfile related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgProfile getOgProfile(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgProfile(): guid = " + guid);
        OgProfile bean = decoratedService.getOgProfile(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgProfile(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getOgProfile(guid, field);
        return obj;
    }

    @Override
    public List<OgProfile> getOgProfiles(List<String> guids) throws BaseException
    {
        log.fine("getOgProfiles()");
        List<OgProfile> ogProfiles = decoratedService.getOgProfiles(guids);
        log.finer("END");
        return ogProfiles;
    }

    @Override
    public List<OgProfile> getAllOgProfiles() throws BaseException
    {
        return getAllOgProfiles(null, null, null);
    }


    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgProfiles(ordering, offset, count, null);
    }

    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgProfiles(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<OgProfile> ogProfiles = decoratedService.getAllOgProfiles(ordering, offset, count, forwardCursor);
        log.finer("END");
        return ogProfiles;
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgProfileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgProfileKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllOgProfileKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgProfiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgProfileMockService.findOgProfiles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<OgProfile> ogProfiles = decoratedService.findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return ogProfiles;
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgProfileMockService.findOgProfileKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgProfileMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgProfile(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        return decoratedService.createOgProfile(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
    }

    @Override
    public String createOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createOgProfile(ogProfile);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgProfile(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        return decoratedService.updateOgProfile(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
    }
        
    @Override
    public Boolean updateOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateOgProfile(ogProfile);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteOgProfile(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgProfile(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteOgProfile(ogProfile);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOgProfiles(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteOgProfiles(filter, params, values);
        return count;
    }

}
