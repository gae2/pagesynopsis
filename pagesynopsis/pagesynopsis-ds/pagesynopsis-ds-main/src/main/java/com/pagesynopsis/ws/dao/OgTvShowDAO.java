package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.OgTvShowDataObject;


// TBD: Add offset/count to getAllOgTvShows() and findOgTvShows(), etc.
public interface OgTvShowDAO
{
    OgTvShowDataObject getOgTvShow(String guid) throws BaseException;
    List<OgTvShowDataObject> getOgTvShows(List<String> guids) throws BaseException;
    List<OgTvShowDataObject> getAllOgTvShows() throws BaseException;
    /* @Deprecated */ List<OgTvShowDataObject> getAllOgTvShows(String ordering, Long offset, Integer count) throws BaseException;
    List<OgTvShowDataObject> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgTvShowDataObject> findOgTvShows(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgTvShowDataObject> findOgTvShows(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<OgTvShowDataObject> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgTvShowDataObject> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createOgTvShow(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgTvShowDataObject?)
    String createOgTvShow(OgTvShowDataObject ogTvShow) throws BaseException;          // Returns Guid.  (Return OgTvShowDataObject?)
    //Boolean updateOgTvShow(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgTvShow(OgTvShowDataObject ogTvShow) throws BaseException;
    Boolean deleteOgTvShow(String guid) throws BaseException;
    Boolean deleteOgTvShow(OgTvShowDataObject ogTvShow) throws BaseException;
    Long deleteOgTvShows(String filter, String params, List<String> values) throws BaseException;
}
