package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.OgContactInfoStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogContactInfoStructs")
@XmlType(propOrder = {"ogContactInfoStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgContactInfoStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgContactInfoStructListStub.class.getName());

    private List<OgContactInfoStructStub> ogContactInfoStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgContactInfoStructListStub()
    {
        this(new ArrayList<OgContactInfoStructStub>());
    }
    public OgContactInfoStructListStub(List<OgContactInfoStructStub> ogContactInfoStructs)
    {
        this(ogContactInfoStructs, null);
    }
    public OgContactInfoStructListStub(List<OgContactInfoStructStub> ogContactInfoStructs, String forwardCursor)
    {
        this.ogContactInfoStructs = ogContactInfoStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogContactInfoStructs == null) {
            return true;
        } else {
            return ogContactInfoStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogContactInfoStructs == null) {
            return 0;
        } else {
            return ogContactInfoStructs.size();
        }
    }


    @XmlElement(name = "ogContactInfoStruct")
    public List<OgContactInfoStructStub> getOgContactInfoStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgContactInfoStructStub> getList()
    {
        return ogContactInfoStructs;
    }
    public void setList(List<OgContactInfoStructStub> ogContactInfoStructs)
    {
        this.ogContactInfoStructs = ogContactInfoStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgContactInfoStructStub> it = this.ogContactInfoStructs.iterator();
        while(it.hasNext()) {
            OgContactInfoStructStub ogContactInfoStruct = it.next();
            sb.append(ogContactInfoStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgContactInfoStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgContactInfoStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgContactInfoStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgContactInfoStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgContactInfoStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgContactInfoStructListStub fromJsonString(String jsonStr)
    {
        try {
            OgContactInfoStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgContactInfoStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgContactInfoStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgContactInfoStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgContactInfoStructListStub object.", e);
        }
        
        return null;
    }

}
