package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "pageInfos")
@XmlType(propOrder = {"pageInfo", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class PageInfoListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PageInfoListStub.class.getName());

    private List<PageInfoStub> pageInfos = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public PageInfoListStub()
    {
        this(new ArrayList<PageInfoStub>());
    }
    public PageInfoListStub(List<PageInfoStub> pageInfos)
    {
        this(pageInfos, null);
    }
    public PageInfoListStub(List<PageInfoStub> pageInfos, String forwardCursor)
    {
        this.pageInfos = pageInfos;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(pageInfos == null) {
            return true;
        } else {
            return pageInfos.isEmpty();
        }
    }
    public int getSize()
    {
        if(pageInfos == null) {
            return 0;
        } else {
            return pageInfos.size();
        }
    }


    @XmlElement(name = "pageInfo")
    public List<PageInfoStub> getPageInfo()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<PageInfoStub> getList()
    {
        return pageInfos;
    }
    public void setList(List<PageInfoStub> pageInfos)
    {
        this.pageInfos = pageInfos;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<PageInfoStub> it = this.pageInfos.iterator();
        while(it.hasNext()) {
            PageInfoStub pageInfo = it.next();
            sb.append(pageInfo.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static PageInfoListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of PageInfoListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write PageInfoListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write PageInfoListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write PageInfoListStub object as a string.", e);
        }
        
        return null;
    }
    public static PageInfoListStub fromJsonString(String jsonStr)
    {
        try {
            PageInfoListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, PageInfoListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into PageInfoListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into PageInfoListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into PageInfoListStub object.", e);
        }
        
        return null;
    }

}
