package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogArticles")
@XmlType(propOrder = {"ogArticle", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgArticleListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgArticleListStub.class.getName());

    private List<OgArticleStub> ogArticles = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgArticleListStub()
    {
        this(new ArrayList<OgArticleStub>());
    }
    public OgArticleListStub(List<OgArticleStub> ogArticles)
    {
        this(ogArticles, null);
    }
    public OgArticleListStub(List<OgArticleStub> ogArticles, String forwardCursor)
    {
        this.ogArticles = ogArticles;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogArticles == null) {
            return true;
        } else {
            return ogArticles.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogArticles == null) {
            return 0;
        } else {
            return ogArticles.size();
        }
    }


    @XmlElement(name = "ogArticle")
    public List<OgArticleStub> getOgArticle()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgArticleStub> getList()
    {
        return ogArticles;
    }
    public void setList(List<OgArticleStub> ogArticles)
    {
        this.ogArticles = ogArticles;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgArticleStub> it = this.ogArticles.iterator();
        while(it.hasNext()) {
            OgArticleStub ogArticle = it.next();
            sb.append(ogArticle.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgArticleListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgArticleListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgArticleListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgArticleListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgArticleListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgArticleListStub fromJsonString(String jsonStr)
    {
        try {
            OgArticleListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgArticleListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgArticleListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgArticleListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgArticleListStub object.", e);
        }
        
        return null;
    }

}
