package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.DecodedQueryParamStruct;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class DecodedQueryParamStructDataObject implements DecodedQueryParamStruct, Serializable
{
    private static final Logger log = Logger.getLogger(DecodedQueryParamStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long _decodedqueryparamstruct_auto_id;         // Note: Object with long PK cannot be a parent...

    @Persistent(defaultFetchGroup = "true")
    private String paramType;

    @Persistent(defaultFetchGroup = "true")
    private String originalString;

    @Persistent(defaultFetchGroup = "true")
    private String decodedString;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public DecodedQueryParamStructDataObject()
    {
        // ???
        // this(null, null, null, null);
    }
    public DecodedQueryParamStructDataObject(String paramType, String originalString, String decodedString, String note)
    {
        setParamType(paramType);
        setOriginalString(originalString);
        setDecodedString(decodedString);
        setNote(note);
    }

    private void resetEncodedKey()
    {
    }

    public String getParamType()
    {
        return this.paramType;
    }
    public void setParamType(String paramType)
    {
        this.paramType = paramType;
        if(this.paramType != null) {
            resetEncodedKey();
        }
    }

    public String getOriginalString()
    {
        return this.originalString;
    }
    public void setOriginalString(String originalString)
    {
        this.originalString = originalString;
        if(this.originalString != null) {
            resetEncodedKey();
        }
    }

    public String getDecodedString()
    {
        return this.decodedString;
    }
    public void setDecodedString(String decodedString)
    {
        this.decodedString = decodedString;
        if(this.decodedString != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getParamType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getOriginalString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDecodedString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("paramType", this.paramType);
        dataMap.put("originalString", this.originalString);
        dataMap.put("decodedString", this.decodedString);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        DecodedQueryParamStruct thatObj = (DecodedQueryParamStruct) obj;
        if( (this.paramType == null && thatObj.getParamType() != null)
            || (this.paramType != null && thatObj.getParamType() == null)
            || !this.paramType.equals(thatObj.getParamType()) ) {
            return false;
        }
        if( (this.originalString == null && thatObj.getOriginalString() != null)
            || (this.originalString != null && thatObj.getOriginalString() == null)
            || !this.originalString.equals(thatObj.getOriginalString()) ) {
            return false;
        }
        if( (this.decodedString == null && thatObj.getDecodedString() != null)
            || (this.decodedString != null && thatObj.getDecodedString() == null)
            || !this.decodedString.equals(thatObj.getDecodedString()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = paramType == null ? 0 : paramType.hashCode();
        _hash = 31 * _hash + delta;
        delta = originalString == null ? 0 : originalString.hashCode();
        _hash = 31 * _hash + delta;
        delta = decodedString == null ? 0 : decodedString.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
