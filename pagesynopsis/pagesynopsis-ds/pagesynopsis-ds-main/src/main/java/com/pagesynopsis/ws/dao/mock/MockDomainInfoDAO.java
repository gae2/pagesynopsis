package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.DomainInfoDAO;
import com.pagesynopsis.ws.data.DomainInfoDataObject;


// MockDomainInfoDAO is a decorator.
// It can be used as a base class to mock DomainInfoDAO objects.
public abstract class MockDomainInfoDAO implements DomainInfoDAO
{
    private static final Logger log = Logger.getLogger(MockDomainInfoDAO.class.getName()); 

    // MockDomainInfoDAO uses the decorator design pattern.
    private DomainInfoDAO decoratedDAO;

    public MockDomainInfoDAO(DomainInfoDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected DomainInfoDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(DomainInfoDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public DomainInfoDataObject getDomainInfo(String guid) throws BaseException
    {
        return decoratedDAO.getDomainInfo(guid);
	}

    @Override
    public List<DomainInfoDataObject> getDomainInfos(List<String> guids) throws BaseException
    {
        return decoratedDAO.getDomainInfos(guids);
    }

    @Override
    public List<DomainInfoDataObject> getAllDomainInfos() throws BaseException
	{
	    return getAllDomainInfos(null, null, null);
    }


    @Override
    public List<DomainInfoDataObject> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllDomainInfos(ordering, offset, count, null);
    }

    @Override
    public List<DomainInfoDataObject> getAllDomainInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllDomainInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDomainInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllDomainInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<DomainInfoDataObject> findDomainInfos(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findDomainInfos(filter, ordering, params, values, null, null);
    }

    @Override
	public List<DomainInfoDataObject> findDomainInfos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findDomainInfos(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<DomainInfoDataObject> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<DomainInfoDataObject> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createDomainInfo(DomainInfoDataObject domainInfo) throws BaseException
    {
        return decoratedDAO.createDomainInfo( domainInfo);
    }

    @Override
	public Boolean updateDomainInfo(DomainInfoDataObject domainInfo) throws BaseException
	{
        return decoratedDAO.updateDomainInfo(domainInfo);
	}
	
    @Override
    public Boolean deleteDomainInfo(DomainInfoDataObject domainInfo) throws BaseException
    {
        return decoratedDAO.deleteDomainInfo(domainInfo);
    }

    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        return decoratedDAO.deleteDomainInfo(guid);
	}

    @Override
    public Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteDomainInfos(filter, params, values);
    }

}
