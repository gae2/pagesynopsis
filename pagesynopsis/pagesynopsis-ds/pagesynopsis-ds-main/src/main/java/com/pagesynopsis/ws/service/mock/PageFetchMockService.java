package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.bean.UrlStructBean;
import com.pagesynopsis.ws.bean.ImageStructBean;
import com.pagesynopsis.ws.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.bean.AnchorStructBean;
import com.pagesynopsis.ws.bean.KeyValuePairStructBean;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.bean.AudioStructBean;
import com.pagesynopsis.ws.bean.VideoStructBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.bean.PageFetchBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.PageFetchDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.PageFetchService;


// PageFetchMockService is a decorator.
// It can be used as a base class to mock PageFetchService objects.
public abstract class PageFetchMockService implements PageFetchService
{
    private static final Logger log = Logger.getLogger(PageFetchMockService.class.getName());

    // PageFetchMockService uses the decorator design pattern.
    private PageFetchService decoratedService;

    public PageFetchMockService(PageFetchService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected PageFetchService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(PageFetchService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // PageFetch related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public PageFetch getPageFetch(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getPageFetch(): guid = " + guid);
        PageFetch bean = decoratedService.getPageFetch(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getPageFetch(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getPageFetch(guid, field);
        return obj;
    }

    @Override
    public List<PageFetch> getPageFetches(List<String> guids) throws BaseException
    {
        log.fine("getPageFetches()");
        List<PageFetch> pageFetches = decoratedService.getPageFetches(guids);
        log.finer("END");
        return pageFetches;
    }

    @Override
    public List<PageFetch> getAllPageFetches() throws BaseException
    {
        return getAllPageFetches(null, null, null);
    }


    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageFetches(ordering, offset, count, null);
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllPageFetches(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<PageFetch> pageFetches = decoratedService.getAllPageFetches(ordering, offset, count, forwardCursor);
        log.finer("END");
        return pageFetches;
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllPageFetchKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllPageFetchKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findPageFetches(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("PageFetchMockService.findPageFetches(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<PageFetch> pageFetches = decoratedService.findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return pageFetches;
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("PageFetchMockService.findPageFetchKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("PageFetchMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createPageFetch(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {
        return decoratedService.createPageFetch(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
    }

    @Override
    public String createPageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createPageFetch(pageFetch);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updatePageFetch(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {
        return decoratedService.updatePageFetch(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
    }
        
    @Override
    public Boolean updatePageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updatePageFetch(pageFetch);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deletePageFetch(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deletePageFetch(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deletePageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deletePageFetch(pageFetch);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deletePageFetches(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deletePageFetches(filter, params, values);
        return count;
    }

}
