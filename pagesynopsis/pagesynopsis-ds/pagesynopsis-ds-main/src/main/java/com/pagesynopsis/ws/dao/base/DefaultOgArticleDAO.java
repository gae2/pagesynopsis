package com.pagesynopsis.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OgArticleDAO;
import com.pagesynopsis.ws.data.OgArticleDataObject;


public class DefaultOgArticleDAO extends DefaultDAOBase implements OgArticleDAO
{
    private static final Logger log = Logger.getLogger(DefaultOgArticleDAO.class.getName()); 

    // Returns the ogArticle for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public OgArticleDataObject getOgArticle(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        OgArticleDataObject ogArticle = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = OgArticleDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = OgArticleDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                ogArticle = pm.getObjectById(OgArticleDataObject.class, key);
                ogArticle.getImage();  // "Touch". Otherwise this field will not be fetched by default.
                ogArticle.getAudio();  // "Touch". Otherwise this field will not be fetched by default.
                ogArticle.getVideo();  // "Touch". Otherwise this field will not be fetched by default.
                ogArticle.getLocaleAlternate();  // "Touch". Otherwise this field will not be fetched by default.
                ogArticle.getAuthor();  // "Touch". Otherwise this field will not be fetched by default.
                ogArticle.getTag();  // "Touch". Otherwise this field will not be fetched by default.
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ogArticle for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return ogArticle;
	}

    @Override
    public List<OgArticleDataObject> getOgArticles(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<OgArticleDataObject> ogArticles = null;
        if(guids != null && !guids.isEmpty()) {
            ogArticles = new ArrayList<OgArticleDataObject>();
            for(String guid : guids) {
                OgArticleDataObject obj = getOgArticle(guid); 
                ogArticles.add(obj);
            }
	    }

        log.finer("END");
        return ogArticles;
    }

    @Override
    public List<OgArticleDataObject> getAllOgArticles() throws BaseException
	{
	    return getAllOgArticles(null, null, null);
    }


    @Override
    public List<OgArticleDataObject> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOgArticles(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<OgArticleDataObject> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<OgArticleDataObject> ogArticles = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(OgArticleDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            ogArticles = (List<OgArticleDataObject>) q.execute();
            if(ogArticles != null) {
                ogArticles.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(ogArticles);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            /*
            // ???
            Collection<OgArticleDataObject> rs_ogArticles = (Collection<OgArticleDataObject>) q.execute();
            if(rs_ogArticles == null) {
                log.log(Level.WARNING, "Failed to retrieve all ogArticles.");
                ogArticles = new ArrayList<OgArticleDataObject>();  // ???           
            } else {
                ogArticles = new ArrayList<OgArticleDataObject>(pm.detachCopyAll(rs_ogArticles));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all ogArticles.", ex);
            //ogArticles = new ArrayList<OgArticleDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all ogArticles.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return ogArticles;
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticleKeys(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + OgArticleDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all OgArticle keys.", ex);
            throw new DataStoreException("Failed to retrieve all OgArticle keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOgArticles(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOgArticles(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<OgArticleDataObject> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultOgArticleDAO.findOgArticles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findOgArticles() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<OgArticleDataObject> ogArticles = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(OgArticleDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                ogArticles = (List<OgArticleDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                ogArticles = (List<OgArticleDataObject>) q.execute();
            }
            if(ogArticles != null) {
                ogArticles.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(ogArticles);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(OgArticleDataObject dobj : ogArticles) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find ogArticles because index is missing.", ex);
            //ogArticles = new ArrayList<OgArticleDataObject>();  // ???
            throw new DataStoreException("Failed to find ogArticles because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find ogArticles meeting the criterion.", ex);
            //ogArticles = new ArrayList<OgArticleDataObject>();  // ???
            throw new DataStoreException("Failed to find ogArticles meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return ogArticles;
	}

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultOgArticleDAO.findOgArticleKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + OgArticleDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find OgArticle keys because index is missing.", ex);
            throw new DataStoreException("Failed to find OgArticle keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find OgArticle keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find OgArticle keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultOgArticleDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(OgArticleDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get ogArticle count because index is missing.", ex);
            throw new DataStoreException("Failed to get ogArticle count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get ogArticle count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get ogArticle count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the ogArticle in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeOgArticle(OgArticleDataObject ogArticle) throws BaseException
    {
        log.fine("storeOgArticle() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        try {
            // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
            // "Object is marked as dirty yet no fields are marked dirty".
            // tx.setOptimistic(true);
            // ....
            tx.begin();
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = ogArticle.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                ogArticle.setCreatedTime(createdTime);
            }
            Long modifiedTime = ogArticle.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                ogArticle.setModifiedTime(createdTime);
            }
            // ????
            // javax.jdo.JDOHelper.makeDirty(ogArticle, "guid");
            pm.makePersistent(ogArticle); 
            tx.commit();
            // TBD: How do you know the makePersistent() call was successful???
            guid = ogArticle.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store ogArticle because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store ogArticle because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store ogArticle.", ex);
            throw new DataStoreException("Failed to store ogArticle.", ex);
        } finally {
            try {
                if(tx.isActive()) {
                    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    tx.rollback();
                }
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeOgArticle(): guid = " + guid);
        return guid;
    }

    @Override
    public String createOgArticle(OgArticleDataObject ogArticle) throws BaseException
    {
        // The createdTime field will be automatically set in storeOgArticle().
        //Long createdTime = ogArticle.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    ogArticle.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = ogArticle.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    ogArticle.setModifiedTime(createdTime);
        //}
        return storeOgArticle(ogArticle);
    }

    @Override
	public Boolean updateOgArticle(OgArticleDataObject ogArticle) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeOgArticle()
	    // (in which case modifiedTime might be updated again).
	    ogArticle.setModifiedTime(System.currentTimeMillis());
	    String guid = storeOgArticle(ogArticle);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteOgArticle(OgArticleDataObject ogArticle) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        try {
            // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
            // "Object is marked as dirty yet no fields are marked dirty".
            // tx.setOptimistic(true);
            // ....
            tx.begin();
            pm.deletePersistent(ogArticle);
            tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete ogArticle because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete ogArticle because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete ogArticle.", ex);
            throw new DataStoreException("Failed to delete ogArticle.", ex);
        } finally {
            try {
                if(tx.isActive()) {
                    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    tx.rollback();
                }
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteOgArticle(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            Transaction tx = pm.currentTransaction();
            try {
                // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
                // "Object is marked as dirty yet no fields are marked dirty".
                // tx.setOptimistic(true);
                // ....
                tx.begin();
                //Key key = OgArticleDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = OgArticleDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                OgArticleDataObject ogArticle = pm.getObjectById(OgArticleDataObject.class, key);
                pm.deletePersistent(ogArticle);
                tx.commit();
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete ogArticle because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete ogArticle because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete ogArticle for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete ogArticle for guid = " + guid, ex);
            } finally {
                try {
                    if(tx.isActive()) {
                        log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                        tx.rollback();
                    }
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteOgArticles(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultOgArticleDAO.deleteOgArticles(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        try {
            // Note: As of GAE SDK 1.7.5, this generates a JDO exception...
            // "Object is marked as dirty yet no fields are marked dirty".
            // tx.setOptimistic(true);
            // ....
            tx.begin();
            Query q = pm.newQuery(OgArticleDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteogArticles because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete ogArticles because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete ogArticles because index is missing", ex);
            throw new DataStoreException("Failed to delete ogArticles because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete ogArticles", ex);
            throw new DataStoreException("Failed to delete ogArticles", ex);
        } finally {
            try {
                if(tx.isActive()) {
                    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    tx.rollback();
                }
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
