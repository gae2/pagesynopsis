package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class TwitterCardMetaDataObject extends PageBaseDataObject implements TwitterCardMeta
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(TwitterCardMetaDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(TwitterCardMetaDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String cardType;

    @Persistent(defaultFetchGroup = "false")
    private TwitterSummaryCardDataObject summaryCard;

    @Persistent(defaultFetchGroup = "false")
    private TwitterPhotoCardDataObject photoCard;

    @Persistent(defaultFetchGroup = "false")
    private TwitterGalleryCardDataObject galleryCard;

    @Persistent(defaultFetchGroup = "false")
    private TwitterAppCardDataObject appCard;

    @Persistent(defaultFetchGroup = "false")
    private TwitterPlayerCardDataObject playerCard;

    @Persistent(defaultFetchGroup = "false")
    private TwitterProductCardDataObject productCard;

    public TwitterCardMetaDataObject()
    {
        this(null);
    }
    public TwitterCardMetaDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterCardMetaDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, summaryCard, photoCard, galleryCard, appCard, playerCard, productCard, null, null);
    }
    public TwitterCardMetaDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);
        this.cardType = cardType;
        if(summaryCard != null) {
            this.summaryCard = new TwitterSummaryCardDataObject(summaryCard.getGuid(), summaryCard.getCard(), summaryCard.getUrl(), summaryCard.getTitle(), summaryCard.getDescription(), summaryCard.getSite(), summaryCard.getSiteId(), summaryCard.getCreator(), summaryCard.getCreatorId(), summaryCard.getImage(), summaryCard.getImageWidth(), summaryCard.getImageHeight(), summaryCard.getCreatedTime(), summaryCard.getModifiedTime());
        } else {
            this.summaryCard = null;
        }
        if(photoCard != null) {
            this.photoCard = new TwitterPhotoCardDataObject(photoCard.getGuid(), photoCard.getCard(), photoCard.getUrl(), photoCard.getTitle(), photoCard.getDescription(), photoCard.getSite(), photoCard.getSiteId(), photoCard.getCreator(), photoCard.getCreatorId(), photoCard.getImage(), photoCard.getImageWidth(), photoCard.getImageHeight(), photoCard.getCreatedTime(), photoCard.getModifiedTime());
        } else {
            this.photoCard = null;
        }
        if(galleryCard != null) {
            this.galleryCard = new TwitterGalleryCardDataObject(galleryCard.getGuid(), galleryCard.getCard(), galleryCard.getUrl(), galleryCard.getTitle(), galleryCard.getDescription(), galleryCard.getSite(), galleryCard.getSiteId(), galleryCard.getCreator(), galleryCard.getCreatorId(), galleryCard.getImage0(), galleryCard.getImage1(), galleryCard.getImage2(), galleryCard.getImage3(), galleryCard.getCreatedTime(), galleryCard.getModifiedTime());
        } else {
            this.galleryCard = null;
        }
        if(appCard != null) {
            this.appCard = new TwitterAppCardDataObject(appCard.getGuid(), appCard.getCard(), appCard.getUrl(), appCard.getTitle(), appCard.getDescription(), appCard.getSite(), appCard.getSiteId(), appCard.getCreator(), appCard.getCreatorId(), appCard.getImage(), appCard.getImageWidth(), appCard.getImageHeight(), appCard.getIphoneApp(), appCard.getIpadApp(), appCard.getGooglePlayApp(), appCard.getCreatedTime(), appCard.getModifiedTime());
        } else {
            this.appCard = null;
        }
        if(playerCard != null) {
            this.playerCard = new TwitterPlayerCardDataObject(playerCard.getGuid(), playerCard.getCard(), playerCard.getUrl(), playerCard.getTitle(), playerCard.getDescription(), playerCard.getSite(), playerCard.getSiteId(), playerCard.getCreator(), playerCard.getCreatorId(), playerCard.getImage(), playerCard.getImageWidth(), playerCard.getImageHeight(), playerCard.getPlayer(), playerCard.getPlayerWidth(), playerCard.getPlayerHeight(), playerCard.getPlayerStream(), playerCard.getPlayerStreamContentType(), playerCard.getCreatedTime(), playerCard.getModifiedTime());
        } else {
            this.playerCard = null;
        }
        if(productCard != null) {
            this.productCard = new TwitterProductCardDataObject(productCard.getGuid(), productCard.getCard(), productCard.getUrl(), productCard.getTitle(), productCard.getDescription(), productCard.getSite(), productCard.getSiteId(), productCard.getCreator(), productCard.getCreatorId(), productCard.getImage(), productCard.getImageWidth(), productCard.getImageHeight(), productCard.getData1(), productCard.getData2(), productCard.getCreatedTime(), productCard.getModifiedTime());
        } else {
            this.productCard = null;
        }
    }

//    @Override
//    protected Key createKey()
//    {
//        return TwitterCardMetaDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return TwitterCardMetaDataObject.composeKey(getGuid());
    }

    public String getCardType()
    {
        return this.cardType;
    }
    public void setCardType(String cardType)
    {
        this.cardType = cardType;
    }

    public TwitterSummaryCard getSummaryCard()
    {
        return this.summaryCard;
    }
    public void setSummaryCard(TwitterSummaryCard summaryCard)
    {
        if(summaryCard == null) {
            this.summaryCard = null;
            log.log(Level.INFO, "TwitterCardMetaDataObject.setSummaryCard(TwitterSummaryCard summaryCard): Arg summaryCard is null.");            
        } else if(summaryCard instanceof TwitterSummaryCardDataObject) {
            this.summaryCard = (TwitterSummaryCardDataObject) summaryCard;
        } else if(summaryCard instanceof TwitterSummaryCard) {
            this.summaryCard = new TwitterSummaryCardDataObject(summaryCard.getGuid(), summaryCard.getCard(), summaryCard.getUrl(), summaryCard.getTitle(), summaryCard.getDescription(), summaryCard.getSite(), summaryCard.getSiteId(), summaryCard.getCreator(), summaryCard.getCreatorId(), summaryCard.getImage(), summaryCard.getImageWidth(), summaryCard.getImageHeight(), summaryCard.getCreatedTime(), summaryCard.getModifiedTime());
        } else {
            this.summaryCard = new TwitterSummaryCardDataObject();   // ????
            log.log(Level.WARNING, "TwitterCardMetaDataObject.setSummaryCard(TwitterSummaryCard summaryCard): Arg summaryCard is of an invalid type.");
        }
    }

    public TwitterPhotoCard getPhotoCard()
    {
        return this.photoCard;
    }
    public void setPhotoCard(TwitterPhotoCard photoCard)
    {
        if(photoCard == null) {
            this.photoCard = null;
            log.log(Level.INFO, "TwitterCardMetaDataObject.setPhotoCard(TwitterPhotoCard photoCard): Arg photoCard is null.");            
        } else if(photoCard instanceof TwitterPhotoCardDataObject) {
            this.photoCard = (TwitterPhotoCardDataObject) photoCard;
        } else if(photoCard instanceof TwitterPhotoCard) {
            this.photoCard = new TwitterPhotoCardDataObject(photoCard.getGuid(), photoCard.getCard(), photoCard.getUrl(), photoCard.getTitle(), photoCard.getDescription(), photoCard.getSite(), photoCard.getSiteId(), photoCard.getCreator(), photoCard.getCreatorId(), photoCard.getImage(), photoCard.getImageWidth(), photoCard.getImageHeight(), photoCard.getCreatedTime(), photoCard.getModifiedTime());
        } else {
            this.photoCard = new TwitterPhotoCardDataObject();   // ????
            log.log(Level.WARNING, "TwitterCardMetaDataObject.setPhotoCard(TwitterPhotoCard photoCard): Arg photoCard is of an invalid type.");
        }
    }

    public TwitterGalleryCard getGalleryCard()
    {
        return this.galleryCard;
    }
    public void setGalleryCard(TwitterGalleryCard galleryCard)
    {
        if(galleryCard == null) {
            this.galleryCard = null;
            log.log(Level.INFO, "TwitterCardMetaDataObject.setGalleryCard(TwitterGalleryCard galleryCard): Arg galleryCard is null.");            
        } else if(galleryCard instanceof TwitterGalleryCardDataObject) {
            this.galleryCard = (TwitterGalleryCardDataObject) galleryCard;
        } else if(galleryCard instanceof TwitterGalleryCard) {
            this.galleryCard = new TwitterGalleryCardDataObject(galleryCard.getGuid(), galleryCard.getCard(), galleryCard.getUrl(), galleryCard.getTitle(), galleryCard.getDescription(), galleryCard.getSite(), galleryCard.getSiteId(), galleryCard.getCreator(), galleryCard.getCreatorId(), galleryCard.getImage0(), galleryCard.getImage1(), galleryCard.getImage2(), galleryCard.getImage3(), galleryCard.getCreatedTime(), galleryCard.getModifiedTime());
        } else {
            this.galleryCard = new TwitterGalleryCardDataObject();   // ????
            log.log(Level.WARNING, "TwitterCardMetaDataObject.setGalleryCard(TwitterGalleryCard galleryCard): Arg galleryCard is of an invalid type.");
        }
    }

    public TwitterAppCard getAppCard()
    {
        return this.appCard;
    }
    public void setAppCard(TwitterAppCard appCard)
    {
        if(appCard == null) {
            this.appCard = null;
            log.log(Level.INFO, "TwitterCardMetaDataObject.setAppCard(TwitterAppCard appCard): Arg appCard is null.");            
        } else if(appCard instanceof TwitterAppCardDataObject) {
            this.appCard = (TwitterAppCardDataObject) appCard;
        } else if(appCard instanceof TwitterAppCard) {
            this.appCard = new TwitterAppCardDataObject(appCard.getGuid(), appCard.getCard(), appCard.getUrl(), appCard.getTitle(), appCard.getDescription(), appCard.getSite(), appCard.getSiteId(), appCard.getCreator(), appCard.getCreatorId(), appCard.getImage(), appCard.getImageWidth(), appCard.getImageHeight(), appCard.getIphoneApp(), appCard.getIpadApp(), appCard.getGooglePlayApp(), appCard.getCreatedTime(), appCard.getModifiedTime());
        } else {
            this.appCard = new TwitterAppCardDataObject();   // ????
            log.log(Level.WARNING, "TwitterCardMetaDataObject.setAppCard(TwitterAppCard appCard): Arg appCard is of an invalid type.");
        }
    }

    public TwitterPlayerCard getPlayerCard()
    {
        return this.playerCard;
    }
    public void setPlayerCard(TwitterPlayerCard playerCard)
    {
        if(playerCard == null) {
            this.playerCard = null;
            log.log(Level.INFO, "TwitterCardMetaDataObject.setPlayerCard(TwitterPlayerCard playerCard): Arg playerCard is null.");            
        } else if(playerCard instanceof TwitterPlayerCardDataObject) {
            this.playerCard = (TwitterPlayerCardDataObject) playerCard;
        } else if(playerCard instanceof TwitterPlayerCard) {
            this.playerCard = new TwitterPlayerCardDataObject(playerCard.getGuid(), playerCard.getCard(), playerCard.getUrl(), playerCard.getTitle(), playerCard.getDescription(), playerCard.getSite(), playerCard.getSiteId(), playerCard.getCreator(), playerCard.getCreatorId(), playerCard.getImage(), playerCard.getImageWidth(), playerCard.getImageHeight(), playerCard.getPlayer(), playerCard.getPlayerWidth(), playerCard.getPlayerHeight(), playerCard.getPlayerStream(), playerCard.getPlayerStreamContentType(), playerCard.getCreatedTime(), playerCard.getModifiedTime());
        } else {
            this.playerCard = new TwitterPlayerCardDataObject();   // ????
            log.log(Level.WARNING, "TwitterCardMetaDataObject.setPlayerCard(TwitterPlayerCard playerCard): Arg playerCard is of an invalid type.");
        }
    }

    public TwitterProductCard getProductCard()
    {
        return this.productCard;
    }
    public void setProductCard(TwitterProductCard productCard)
    {
        if(productCard == null) {
            this.productCard = null;
            log.log(Level.INFO, "TwitterCardMetaDataObject.setProductCard(TwitterProductCard productCard): Arg productCard is null.");            
        } else if(productCard instanceof TwitterProductCardDataObject) {
            this.productCard = (TwitterProductCardDataObject) productCard;
        } else if(productCard instanceof TwitterProductCard) {
            this.productCard = new TwitterProductCardDataObject(productCard.getGuid(), productCard.getCard(), productCard.getUrl(), productCard.getTitle(), productCard.getDescription(), productCard.getSite(), productCard.getSiteId(), productCard.getCreator(), productCard.getCreatorId(), productCard.getImage(), productCard.getImageWidth(), productCard.getImageHeight(), productCard.getData1(), productCard.getData2(), productCard.getCreatedTime(), productCard.getModifiedTime());
        } else {
            this.productCard = new TwitterProductCardDataObject();   // ????
            log.log(Level.WARNING, "TwitterCardMetaDataObject.setProductCard(TwitterProductCard productCard): Arg productCard is of an invalid type.");
        }
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("cardType", this.cardType);
        dataMap.put("summaryCard", this.summaryCard);
        dataMap.put("photoCard", this.photoCard);
        dataMap.put("galleryCard", this.galleryCard);
        dataMap.put("appCard", this.appCard);
        dataMap.put("playerCard", this.playerCard);
        dataMap.put("productCard", this.productCard);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        TwitterCardMeta thatObj = (TwitterCardMeta) obj;
        if( (this.cardType == null && thatObj.getCardType() != null)
            || (this.cardType != null && thatObj.getCardType() == null)
            || !this.cardType.equals(thatObj.getCardType()) ) {
            return false;
        }
        if( (this.summaryCard == null && thatObj.getSummaryCard() != null)
            || (this.summaryCard != null && thatObj.getSummaryCard() == null)
            || !this.summaryCard.equals(thatObj.getSummaryCard()) ) {
            return false;
        }
        if( (this.photoCard == null && thatObj.getPhotoCard() != null)
            || (this.photoCard != null && thatObj.getPhotoCard() == null)
            || !this.photoCard.equals(thatObj.getPhotoCard()) ) {
            return false;
        }
        if( (this.galleryCard == null && thatObj.getGalleryCard() != null)
            || (this.galleryCard != null && thatObj.getGalleryCard() == null)
            || !this.galleryCard.equals(thatObj.getGalleryCard()) ) {
            return false;
        }
        if( (this.appCard == null && thatObj.getAppCard() != null)
            || (this.appCard != null && thatObj.getAppCard() == null)
            || !this.appCard.equals(thatObj.getAppCard()) ) {
            return false;
        }
        if( (this.playerCard == null && thatObj.getPlayerCard() != null)
            || (this.playerCard != null && thatObj.getPlayerCard() == null)
            || !this.playerCard.equals(thatObj.getPlayerCard()) ) {
            return false;
        }
        if( (this.productCard == null && thatObj.getProductCard() != null)
            || (this.productCard != null && thatObj.getProductCard() == null)
            || !this.productCard.equals(thatObj.getProductCard()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = cardType == null ? 0 : cardType.hashCode();
        _hash = 31 * _hash + delta;
        delta = summaryCard == null ? 0 : summaryCard.hashCode();
        _hash = 31 * _hash + delta;
        delta = photoCard == null ? 0 : photoCard.hashCode();
        _hash = 31 * _hash + delta;
        delta = galleryCard == null ? 0 : galleryCard.hashCode();
        _hash = 31 * _hash + delta;
        delta = appCard == null ? 0 : appCard.hashCode();
        _hash = 31 * _hash + delta;
        delta = playerCard == null ? 0 : playerCard.hashCode();
        _hash = 31 * _hash + delta;
        delta = productCard == null ? 0 : productCard.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
