package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.data.RobotsTextGroupDataObject;
import com.pagesynopsis.ws.data.RobotsTextFileDataObject;

public class RobotsTextFileBean extends BeanBase implements RobotsTextFile
{
    private static final Logger log = Logger.getLogger(RobotsTextFileBean.class.getName());

    // Embedded data object.
    private RobotsTextFileDataObject dobj = null;

    public RobotsTextFileBean()
    {
        this(new RobotsTextFileDataObject());
    }
    public RobotsTextFileBean(String guid)
    {
        this(new RobotsTextFileDataObject(guid));
    }
    public RobotsTextFileBean(RobotsTextFileDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public RobotsTextFileDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
        }
    }

    public String getSiteUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getSiteUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
            return null;   // ???
        }
    }
    public void setSiteUrl(String siteUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setSiteUrl(siteUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
        }
    }

    public List<RobotsTextGroup> getGroups()
    {
        if(getDataObject() != null) {
            List<RobotsTextGroup> list = getDataObject().getGroups();
            if(list != null) {
                List<RobotsTextGroup> bean = new ArrayList<RobotsTextGroup>();
                for(RobotsTextGroup robotsTextGroup : list) {
                    RobotsTextGroupBean elem = null;
                    if(robotsTextGroup instanceof RobotsTextGroupBean) {
                        elem = (RobotsTextGroupBean) robotsTextGroup;
                    } else if(robotsTextGroup instanceof RobotsTextGroupDataObject) {
                        elem = new RobotsTextGroupBean((RobotsTextGroupDataObject) robotsTextGroup);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
            return null;
        }
    }
    public void setGroups(List<RobotsTextGroup> groups)
    {
        if(groups != null) {
            if(getDataObject() != null) {
                List<RobotsTextGroup> dataObj = new ArrayList<RobotsTextGroup>();
                for(RobotsTextGroup robotsTextGroup : groups) {
                    RobotsTextGroupDataObject elem = null;
                    if(robotsTextGroup instanceof RobotsTextGroupBean) {
                        elem = ((RobotsTextGroupBean) robotsTextGroup).toDataObject();
                    } else if(robotsTextGroup instanceof RobotsTextGroupDataObject) {
                        elem = (RobotsTextGroupDataObject) robotsTextGroup;
                    } else if(robotsTextGroup instanceof RobotsTextGroup) {
                        elem = new RobotsTextGroupDataObject(robotsTextGroup.getUuid(), robotsTextGroup.getUserAgent(), robotsTextGroup.getAllows(), robotsTextGroup.getDisallows());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setGroups(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setGroups(groups);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
            }
        }
    }

    public List<String> getSitemaps()
    {
        if(getDataObject() != null) {
            return getDataObject().getSitemaps();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
            return null;   // ???
        }
    }
    public void setSitemaps(List<String> sitemaps)
    {
        if(getDataObject() != null) {
            getDataObject().setSitemaps(sitemaps);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
        }
    }

    public String getContent()
    {
        if(getDataObject() != null) {
            return getDataObject().getContent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
            return null;   // ???
        }
    }
    public void setContent(String content)
    {
        if(getDataObject() != null) {
            getDataObject().setContent(content);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
        }
    }

    public String getContentHash()
    {
        if(getDataObject() != null) {
            return getDataObject().getContentHash();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
            return null;   // ???
        }
    }
    public void setContentHash(String contentHash)
    {
        if(getDataObject() != null) {
            getDataObject().setContentHash(contentHash);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
        }
    }

    public Long getLastCheckedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastCheckedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastCheckedTime(lastCheckedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
        }
    }

    public Long getLastUpdatedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastUpdatedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastUpdatedTime(lastUpdatedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextFileDataObject is null!");
        }
    }


    // TBD
    public RobotsTextFileDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
