package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogImageStruct")
@XmlType(propOrder = {"uuid", "url", "secureUrl", "type", "width", "height"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgImageStructStub implements OgImageStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgImageStructStub.class.getName());

    private String uuid;
    private String url;
    private String secureUrl;
    private String type;
    private Integer width;
    private Integer height;

    public OgImageStructStub()
    {
        this(null);
    }
    public OgImageStructStub(OgImageStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.url = bean.getUrl();
            this.secureUrl = bean.getSecureUrl();
            this.type = bean.getType();
            this.width = bean.getWidth();
            this.height = bean.getHeight();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getUrl()
    {
        return this.url;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    @XmlElement
    public String getSecureUrl()
    {
        return this.secureUrl;
    }
    public void setSecureUrl(String secureUrl)
    {
        this.secureUrl = secureUrl;
    }

    @XmlElement
    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    @XmlElement
    public Integer getWidth()
    {
        return this.width;
    }
    public void setWidth(Integer width)
    {
        this.width = width;
    }

    @XmlElement
    public Integer getHeight()
    {
        return this.height;
    }
    public void setHeight(Integer height)
    {
        this.height = height;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecureUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("url", this.url);
        dataMap.put("secureUrl", this.secureUrl);
        dataMap.put("type", this.type);
        dataMap.put("width", this.width);
        dataMap.put("height", this.height);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = url == null ? 0 : url.hashCode();
        _hash = 31 * _hash + delta;
        delta = secureUrl == null ? 0 : secureUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = width == null ? 0 : width.hashCode();
        _hash = 31 * _hash + delta;
        delta = height == null ? 0 : height.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static OgImageStructStub convertBeanToStub(OgImageStruct bean)
    {
        OgImageStructStub stub = null;
        if(bean instanceof OgImageStructStub) {
            stub = (OgImageStructStub) bean;
        } else {
            if(bean != null) {
                stub = new OgImageStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgImageStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of OgImageStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgImageStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgImageStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgImageStructStub object as a string.", e);
        }
        
        return null;
    }
    public static OgImageStructStub fromJsonString(String jsonStr)
    {
        try {
            OgImageStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgImageStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgImageStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgImageStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgImageStructStub object.", e);
        }
        
        return null;
    }

}
