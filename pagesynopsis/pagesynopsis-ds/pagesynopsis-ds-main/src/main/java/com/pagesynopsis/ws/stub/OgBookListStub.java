package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogBooks")
@XmlType(propOrder = {"ogBook", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgBookListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgBookListStub.class.getName());

    private List<OgBookStub> ogBooks = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgBookListStub()
    {
        this(new ArrayList<OgBookStub>());
    }
    public OgBookListStub(List<OgBookStub> ogBooks)
    {
        this(ogBooks, null);
    }
    public OgBookListStub(List<OgBookStub> ogBooks, String forwardCursor)
    {
        this.ogBooks = ogBooks;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogBooks == null) {
            return true;
        } else {
            return ogBooks.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogBooks == null) {
            return 0;
        } else {
            return ogBooks.size();
        }
    }


    @XmlElement(name = "ogBook")
    public List<OgBookStub> getOgBook()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgBookStub> getList()
    {
        return ogBooks;
    }
    public void setList(List<OgBookStub> ogBooks)
    {
        this.ogBooks = ogBooks;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgBookStub> it = this.ogBooks.iterator();
        while(it.hasNext()) {
            OgBookStub ogBook = it.next();
            sb.append(ogBook.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgBookListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgBookListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgBookListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgBookListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgBookListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgBookListStub fromJsonString(String jsonStr)
    {
        try {
            OgBookListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgBookListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgBookListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgBookListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgBookListStub object.", e);
        }
        
        return null;
    }

}
