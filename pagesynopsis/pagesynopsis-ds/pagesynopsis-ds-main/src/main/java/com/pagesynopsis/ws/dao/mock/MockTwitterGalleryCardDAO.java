package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.TwitterGalleryCardDAO;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;


// MockTwitterGalleryCardDAO is a decorator.
// It can be used as a base class to mock TwitterGalleryCardDAO objects.
public abstract class MockTwitterGalleryCardDAO implements TwitterGalleryCardDAO
{
    private static final Logger log = Logger.getLogger(MockTwitterGalleryCardDAO.class.getName()); 

    // MockTwitterGalleryCardDAO uses the decorator design pattern.
    private TwitterGalleryCardDAO decoratedDAO;

    public MockTwitterGalleryCardDAO(TwitterGalleryCardDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected TwitterGalleryCardDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(TwitterGalleryCardDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public TwitterGalleryCardDataObject getTwitterGalleryCard(String guid) throws BaseException
    {
        return decoratedDAO.getTwitterGalleryCard(guid);
	}

    @Override
    public List<TwitterGalleryCardDataObject> getTwitterGalleryCards(List<String> guids) throws BaseException
    {
        return decoratedDAO.getTwitterGalleryCards(guids);
    }

    @Override
    public List<TwitterGalleryCardDataObject> getAllTwitterGalleryCards() throws BaseException
	{
	    return getAllTwitterGalleryCards(null, null, null);
    }


    @Override
    public List<TwitterGalleryCardDataObject> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllTwitterGalleryCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterGalleryCardDataObject> getAllTwitterGalleryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllTwitterGalleryCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterGalleryCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllTwitterGalleryCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<TwitterGalleryCardDataObject> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTwitterGalleryCards(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TwitterGalleryCardDataObject> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<TwitterGalleryCardDataObject> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<TwitterGalleryCardDataObject> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterGalleryCard(TwitterGalleryCardDataObject twitterGalleryCard) throws BaseException
    {
        return decoratedDAO.createTwitterGalleryCard( twitterGalleryCard);
    }

    @Override
	public Boolean updateTwitterGalleryCard(TwitterGalleryCardDataObject twitterGalleryCard) throws BaseException
	{
        return decoratedDAO.updateTwitterGalleryCard(twitterGalleryCard);
	}
	
    @Override
    public Boolean deleteTwitterGalleryCard(TwitterGalleryCardDataObject twitterGalleryCard) throws BaseException
    {
        return decoratedDAO.deleteTwitterGalleryCard(twitterGalleryCard);
    }

    @Override
    public Boolean deleteTwitterGalleryCard(String guid) throws BaseException
    {
        return decoratedDAO.deleteTwitterGalleryCard(guid);
	}

    @Override
    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteTwitterGalleryCards(filter, params, values);
    }

}
