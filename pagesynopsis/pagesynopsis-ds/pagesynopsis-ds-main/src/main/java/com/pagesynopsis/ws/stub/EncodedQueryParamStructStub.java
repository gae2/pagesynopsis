package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.EncodedQueryParamStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "encodedQueryParamStruct")
@XmlType(propOrder = {"paramType", "originalString", "encodedString", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EncodedQueryParamStructStub implements EncodedQueryParamStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(EncodedQueryParamStructStub.class.getName());

    private String paramType;
    private String originalString;
    private String encodedString;
    private String note;

    public EncodedQueryParamStructStub()
    {
        this(null);
    }
    public EncodedQueryParamStructStub(EncodedQueryParamStruct bean)
    {
        if(bean != null) {
            this.paramType = bean.getParamType();
            this.originalString = bean.getOriginalString();
            this.encodedString = bean.getEncodedString();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getParamType()
    {
        return this.paramType;
    }
    public void setParamType(String paramType)
    {
        this.paramType = paramType;
    }

    @XmlElement
    public String getOriginalString()
    {
        return this.originalString;
    }
    public void setOriginalString(String originalString)
    {
        this.originalString = originalString;
    }

    @XmlElement
    public String getEncodedString()
    {
        return this.encodedString;
    }
    public void setEncodedString(String encodedString)
    {
        this.encodedString = encodedString;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getParamType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getOriginalString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getEncodedString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("paramType", this.paramType);
        dataMap.put("originalString", this.originalString);
        dataMap.put("encodedString", this.encodedString);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = paramType == null ? 0 : paramType.hashCode();
        _hash = 31 * _hash + delta;
        delta = originalString == null ? 0 : originalString.hashCode();
        _hash = 31 * _hash + delta;
        delta = encodedString == null ? 0 : encodedString.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static EncodedQueryParamStructStub convertBeanToStub(EncodedQueryParamStruct bean)
    {
        EncodedQueryParamStructStub stub = null;
        if(bean instanceof EncodedQueryParamStructStub) {
            stub = (EncodedQueryParamStructStub) bean;
        } else {
            if(bean != null) {
                stub = new EncodedQueryParamStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static EncodedQueryParamStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of EncodedQueryParamStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write EncodedQueryParamStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write EncodedQueryParamStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write EncodedQueryParamStructStub object as a string.", e);
        }
        
        return null;
    }
    public static EncodedQueryParamStructStub fromJsonString(String jsonStr)
    {
        try {
            EncodedQueryParamStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, EncodedQueryParamStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into EncodedQueryParamStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into EncodedQueryParamStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into EncodedQueryParamStructStub object.", e);
        }
        
        return null;
    }

}
