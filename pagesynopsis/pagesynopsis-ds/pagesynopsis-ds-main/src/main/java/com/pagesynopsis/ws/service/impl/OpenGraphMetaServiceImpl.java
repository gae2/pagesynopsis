package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.bean.UrlStructBean;
import com.pagesynopsis.ws.bean.ImageStructBean;
import com.pagesynopsis.ws.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.bean.AnchorStructBean;
import com.pagesynopsis.ws.bean.KeyValuePairStructBean;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.bean.AudioStructBean;
import com.pagesynopsis.ws.bean.VideoStructBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.bean.OpenGraphMetaBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.OpenGraphMetaDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OpenGraphMetaService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OpenGraphMetaServiceImpl implements OpenGraphMetaService
{
    private static final Logger log = Logger.getLogger(OpenGraphMetaServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // OpenGraphMeta related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OpenGraphMeta getOpenGraphMeta(String guid) throws BaseException
    {
        log.finer("BEGIN");

        OpenGraphMetaDataObject dataObj = getDAOFactory().getOpenGraphMetaDAO().getOpenGraphMeta(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OpenGraphMetaDataObject for guid = " + guid);
            return null;  // ????
        }
        OpenGraphMetaBean bean = new OpenGraphMetaBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOpenGraphMeta(String guid, String field) throws BaseException
    {
        OpenGraphMetaDataObject dataObj = getDAOFactory().getOpenGraphMetaDAO().getOpenGraphMeta(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OpenGraphMetaDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("fetchRequest")) {
            return dataObj.getFetchRequest();
        } else if(field.equals("targetUrl")) {
            return dataObj.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return dataObj.getPageUrl();
        } else if(field.equals("queryString")) {
            return dataObj.getQueryString();
        } else if(field.equals("queryParams")) {
            return dataObj.getQueryParams();
        } else if(field.equals("lastFetchResult")) {
            return dataObj.getLastFetchResult();
        } else if(field.equals("responseCode")) {
            return dataObj.getResponseCode();
        } else if(field.equals("contentType")) {
            return dataObj.getContentType();
        } else if(field.equals("contentLength")) {
            return dataObj.getContentLength();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("redirect")) {
            return dataObj.getRedirect();
        } else if(field.equals("location")) {
            return dataObj.getLocation();
        } else if(field.equals("pageTitle")) {
            return dataObj.getPageTitle();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("deferred")) {
            return dataObj.isDeferred();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("refreshStatus")) {
            return dataObj.getRefreshStatus();
        } else if(field.equals("refreshInterval")) {
            return dataObj.getRefreshInterval();
        } else if(field.equals("nextRefreshTime")) {
            return dataObj.getNextRefreshTime();
        } else if(field.equals("lastCheckedTime")) {
            return dataObj.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return dataObj.getLastUpdatedTime();
        } else if(field.equals("ogType")) {
            return dataObj.getOgType();
        } else if(field.equals("ogProfile")) {
            return dataObj.getOgProfile();
        } else if(field.equals("ogWebsite")) {
            return dataObj.getOgWebsite();
        } else if(field.equals("ogBlog")) {
            return dataObj.getOgBlog();
        } else if(field.equals("ogArticle")) {
            return dataObj.getOgArticle();
        } else if(field.equals("ogBook")) {
            return dataObj.getOgBook();
        } else if(field.equals("ogVideo")) {
            return dataObj.getOgVideo();
        } else if(field.equals("ogMovie")) {
            return dataObj.getOgMovie();
        } else if(field.equals("ogTvShow")) {
            return dataObj.getOgTvShow();
        } else if(field.equals("ogTvEpisode")) {
            return dataObj.getOgTvEpisode();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OpenGraphMeta> getOpenGraphMetas(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<OpenGraphMeta> list = new ArrayList<OpenGraphMeta>();
        List<OpenGraphMetaDataObject> dataObjs = getDAOFactory().getOpenGraphMetaDAO().getOpenGraphMetas(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OpenGraphMetaDataObject list.");
        } else {
            Iterator<OpenGraphMetaDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OpenGraphMetaDataObject dataObj = (OpenGraphMetaDataObject) it.next();
                list.add(new OpenGraphMetaBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas() throws BaseException
    {
        return getAllOpenGraphMetas(null, null, null);
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOpenGraphMetas(ordering, offset, count, null);
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOpenGraphMetas(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OpenGraphMeta> list = new ArrayList<OpenGraphMeta>();
        List<OpenGraphMetaDataObject> dataObjs = getDAOFactory().getOpenGraphMetaDAO().getAllOpenGraphMetas(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OpenGraphMetaDataObject list.");
        } else {
            Iterator<OpenGraphMetaDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OpenGraphMetaDataObject dataObj = (OpenGraphMetaDataObject) it.next();
                list.add(new OpenGraphMetaBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOpenGraphMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllOpenGraphMetaKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOpenGraphMetaDAO().getAllOpenGraphMetaKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OpenGraphMeta key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOpenGraphMetas(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OpenGraphMetaServiceImpl.findOpenGraphMetas(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OpenGraphMeta> list = new ArrayList<OpenGraphMeta>();
        List<OpenGraphMetaDataObject> dataObjs = getDAOFactory().getOpenGraphMetaDAO().findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find openGraphMetas for the given criterion.");
        } else {
            Iterator<OpenGraphMetaDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OpenGraphMetaDataObject dataObj = (OpenGraphMetaDataObject) it.next();
                list.add(new OpenGraphMetaBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OpenGraphMetaServiceImpl.findOpenGraphMetaKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOpenGraphMetaDAO().findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OpenGraphMeta keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OpenGraphMetaServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getOpenGraphMetaDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createOpenGraphMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        OgProfileDataObject ogProfileDobj = null;
        if(ogProfile instanceof OgProfileBean) {
            ogProfileDobj = ((OgProfileBean) ogProfile).toDataObject();
        } else if(ogProfile instanceof OgProfile) {
            ogProfileDobj = new OgProfileDataObject(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender(), ogProfile.getCreatedTime(), ogProfile.getModifiedTime());
        } else {
            ogProfileDobj = null;   // ????
        }
        OgWebsiteDataObject ogWebsiteDobj = null;
        if(ogWebsite instanceof OgWebsiteBean) {
            ogWebsiteDobj = ((OgWebsiteBean) ogWebsite).toDataObject();
        } else if(ogWebsite instanceof OgWebsite) {
            ogWebsiteDobj = new OgWebsiteDataObject(ogWebsite.getGuid(), ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote(), ogWebsite.getCreatedTime(), ogWebsite.getModifiedTime());
        } else {
            ogWebsiteDobj = null;   // ????
        }
        OgBlogDataObject ogBlogDobj = null;
        if(ogBlog instanceof OgBlogBean) {
            ogBlogDobj = ((OgBlogBean) ogBlog).toDataObject();
        } else if(ogBlog instanceof OgBlog) {
            ogBlogDobj = new OgBlogDataObject(ogBlog.getGuid(), ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote(), ogBlog.getCreatedTime(), ogBlog.getModifiedTime());
        } else {
            ogBlogDobj = null;   // ????
        }
        OgArticleDataObject ogArticleDobj = null;
        if(ogArticle instanceof OgArticleBean) {
            ogArticleDobj = ((OgArticleBean) ogArticle).toDataObject();
        } else if(ogArticle instanceof OgArticle) {
            ogArticleDobj = new OgArticleDataObject(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate(), ogArticle.getCreatedTime(), ogArticle.getModifiedTime());
        } else {
            ogArticleDobj = null;   // ????
        }
        OgBookDataObject ogBookDobj = null;
        if(ogBook instanceof OgBookBean) {
            ogBookDobj = ((OgBookBean) ogBook).toDataObject();
        } else if(ogBook instanceof OgBook) {
            ogBookDobj = new OgBookDataObject(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate(), ogBook.getCreatedTime(), ogBook.getModifiedTime());
        } else {
            ogBookDobj = null;   // ????
        }
        OgVideoDataObject ogVideoDobj = null;
        if(ogVideo instanceof OgVideoBean) {
            ogVideoDobj = ((OgVideoBean) ogVideo).toDataObject();
        } else if(ogVideo instanceof OgVideo) {
            ogVideoDobj = new OgVideoDataObject(ogVideo.getGuid(), ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate(), ogVideo.getCreatedTime(), ogVideo.getModifiedTime());
        } else {
            ogVideoDobj = null;   // ????
        }
        OgMovieDataObject ogMovieDobj = null;
        if(ogMovie instanceof OgMovieBean) {
            ogMovieDobj = ((OgMovieBean) ogMovie).toDataObject();
        } else if(ogMovie instanceof OgMovie) {
            ogMovieDobj = new OgMovieDataObject(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate(), ogMovie.getCreatedTime(), ogMovie.getModifiedTime());
        } else {
            ogMovieDobj = null;   // ????
        }
        OgTvShowDataObject ogTvShowDobj = null;
        if(ogTvShow instanceof OgTvShowBean) {
            ogTvShowDobj = ((OgTvShowBean) ogTvShow).toDataObject();
        } else if(ogTvShow instanceof OgTvShow) {
            ogTvShowDobj = new OgTvShowDataObject(ogTvShow.getGuid(), ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate(), ogTvShow.getCreatedTime(), ogTvShow.getModifiedTime());
        } else {
            ogTvShowDobj = null;   // ????
        }
        OgTvEpisodeDataObject ogTvEpisodeDobj = null;
        if(ogTvEpisode instanceof OgTvEpisodeBean) {
            ogTvEpisodeDobj = ((OgTvEpisodeBean) ogTvEpisode).toDataObject();
        } else if(ogTvEpisode instanceof OgTvEpisode) {
            ogTvEpisodeDobj = new OgTvEpisodeDataObject(ogTvEpisode.getGuid(), ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate(), ogTvEpisode.getCreatedTime(), ogTvEpisode.getModifiedTime());
        } else {
            ogTvEpisodeDobj = null;   // ????
        }
        
        OpenGraphMetaDataObject dataObj = new OpenGraphMetaDataObject(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfileDobj, ogWebsiteDobj, ogBlogDobj, ogArticleDobj, ogBookDobj, ogVideoDobj, ogMovieDobj, ogTvShowDobj, ogTvEpisodeDobj);
        return createOpenGraphMeta(dataObj);
    }

    @Override
    public String createOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param openGraphMeta cannot be null.....
        if(openGraphMeta == null) {
            log.log(Level.INFO, "Param openGraphMeta is null!");
            throw new BadRequestException("Param openGraphMeta object is null!");
        }
        OpenGraphMetaDataObject dataObj = null;
        if(openGraphMeta instanceof OpenGraphMetaDataObject) {
            dataObj = (OpenGraphMetaDataObject) openGraphMeta;
        } else if(openGraphMeta instanceof OpenGraphMetaBean) {
            dataObj = ((OpenGraphMetaBean) openGraphMeta).toDataObject();
        } else {  // if(openGraphMeta instanceof OpenGraphMeta)
            //dataObj = new OpenGraphMetaDataObject(null, openGraphMeta.getUser(), openGraphMeta.getFetchRequest(), openGraphMeta.getTargetUrl(), openGraphMeta.getPageUrl(), openGraphMeta.getQueryString(), openGraphMeta.getQueryParams(), openGraphMeta.getLastFetchResult(), openGraphMeta.getResponseCode(), openGraphMeta.getContentType(), openGraphMeta.getContentLength(), openGraphMeta.getLanguage(), openGraphMeta.getRedirect(), openGraphMeta.getLocation(), openGraphMeta.getPageTitle(), openGraphMeta.getNote(), openGraphMeta.isDeferred(), openGraphMeta.getStatus(), openGraphMeta.getRefreshStatus(), openGraphMeta.getRefreshInterval(), openGraphMeta.getNextRefreshTime(), openGraphMeta.getLastCheckedTime(), openGraphMeta.getLastUpdatedTime(), openGraphMeta.getOgType(), (OgProfileDataObject) openGraphMeta.getOgProfile(), (OgWebsiteDataObject) openGraphMeta.getOgWebsite(), (OgBlogDataObject) openGraphMeta.getOgBlog(), (OgArticleDataObject) openGraphMeta.getOgArticle(), (OgBookDataObject) openGraphMeta.getOgBook(), (OgVideoDataObject) openGraphMeta.getOgVideo(), (OgMovieDataObject) openGraphMeta.getOgMovie(), (OgTvShowDataObject) openGraphMeta.getOgTvShow(), (OgTvEpisodeDataObject) openGraphMeta.getOgTvEpisode());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new OpenGraphMetaDataObject(openGraphMeta.getGuid(), openGraphMeta.getUser(), openGraphMeta.getFetchRequest(), openGraphMeta.getTargetUrl(), openGraphMeta.getPageUrl(), openGraphMeta.getQueryString(), openGraphMeta.getQueryParams(), openGraphMeta.getLastFetchResult(), openGraphMeta.getResponseCode(), openGraphMeta.getContentType(), openGraphMeta.getContentLength(), openGraphMeta.getLanguage(), openGraphMeta.getRedirect(), openGraphMeta.getLocation(), openGraphMeta.getPageTitle(), openGraphMeta.getNote(), openGraphMeta.isDeferred(), openGraphMeta.getStatus(), openGraphMeta.getRefreshStatus(), openGraphMeta.getRefreshInterval(), openGraphMeta.getNextRefreshTime(), openGraphMeta.getLastCheckedTime(), openGraphMeta.getLastUpdatedTime(), openGraphMeta.getOgType(), (OgProfileDataObject) openGraphMeta.getOgProfile(), (OgWebsiteDataObject) openGraphMeta.getOgWebsite(), (OgBlogDataObject) openGraphMeta.getOgBlog(), (OgArticleDataObject) openGraphMeta.getOgArticle(), (OgBookDataObject) openGraphMeta.getOgBook(), (OgVideoDataObject) openGraphMeta.getOgVideo(), (OgMovieDataObject) openGraphMeta.getOgMovie(), (OgTvShowDataObject) openGraphMeta.getOgTvShow(), (OgTvEpisodeDataObject) openGraphMeta.getOgTvEpisode());
        }
        String guid = getDAOFactory().getOpenGraphMetaDAO().createOpenGraphMeta(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOpenGraphMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfile ogProfile, OgWebsite ogWebsite, OgBlog ogBlog, OgArticle ogArticle, OgBook ogBook, OgVideo ogVideo, OgMovie ogMovie, OgTvShow ogTvShow, OgTvEpisode ogTvEpisode) throws BaseException
    {
        OgProfileDataObject ogProfileDobj = null;
        if(ogProfile instanceof OgProfileBean) {
            ogProfileDobj = ((OgProfileBean) ogProfile).toDataObject();            
        } else if(ogProfile instanceof OgProfile) {
            ogProfileDobj = new OgProfileDataObject(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender(), ogProfile.getCreatedTime(), ogProfile.getModifiedTime());
        } else {
            ogProfileDobj = null;   // ????
        }
        OgWebsiteDataObject ogWebsiteDobj = null;
        if(ogWebsite instanceof OgWebsiteBean) {
            ogWebsiteDobj = ((OgWebsiteBean) ogWebsite).toDataObject();            
        } else if(ogWebsite instanceof OgWebsite) {
            ogWebsiteDobj = new OgWebsiteDataObject(ogWebsite.getGuid(), ogWebsite.getUrl(), ogWebsite.getType(), ogWebsite.getSiteName(), ogWebsite.getTitle(), ogWebsite.getDescription(), ogWebsite.getFbAdmins(), ogWebsite.getFbAppId(), ogWebsite.getImage(), ogWebsite.getAudio(), ogWebsite.getVideo(), ogWebsite.getLocale(), ogWebsite.getLocaleAlternate(), ogWebsite.getNote(), ogWebsite.getCreatedTime(), ogWebsite.getModifiedTime());
        } else {
            ogWebsiteDobj = null;   // ????
        }
        OgBlogDataObject ogBlogDobj = null;
        if(ogBlog instanceof OgBlogBean) {
            ogBlogDobj = ((OgBlogBean) ogBlog).toDataObject();            
        } else if(ogBlog instanceof OgBlog) {
            ogBlogDobj = new OgBlogDataObject(ogBlog.getGuid(), ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote(), ogBlog.getCreatedTime(), ogBlog.getModifiedTime());
        } else {
            ogBlogDobj = null;   // ????
        }
        OgArticleDataObject ogArticleDobj = null;
        if(ogArticle instanceof OgArticleBean) {
            ogArticleDobj = ((OgArticleBean) ogArticle).toDataObject();            
        } else if(ogArticle instanceof OgArticle) {
            ogArticleDobj = new OgArticleDataObject(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate(), ogArticle.getCreatedTime(), ogArticle.getModifiedTime());
        } else {
            ogArticleDobj = null;   // ????
        }
        OgBookDataObject ogBookDobj = null;
        if(ogBook instanceof OgBookBean) {
            ogBookDobj = ((OgBookBean) ogBook).toDataObject();            
        } else if(ogBook instanceof OgBook) {
            ogBookDobj = new OgBookDataObject(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate(), ogBook.getCreatedTime(), ogBook.getModifiedTime());
        } else {
            ogBookDobj = null;   // ????
        }
        OgVideoDataObject ogVideoDobj = null;
        if(ogVideo instanceof OgVideoBean) {
            ogVideoDobj = ((OgVideoBean) ogVideo).toDataObject();            
        } else if(ogVideo instanceof OgVideo) {
            ogVideoDobj = new OgVideoDataObject(ogVideo.getGuid(), ogVideo.getUrl(), ogVideo.getType(), ogVideo.getSiteName(), ogVideo.getTitle(), ogVideo.getDescription(), ogVideo.getFbAdmins(), ogVideo.getFbAppId(), ogVideo.getImage(), ogVideo.getAudio(), ogVideo.getVideo(), ogVideo.getLocale(), ogVideo.getLocaleAlternate(), ogVideo.getDirector(), ogVideo.getWriter(), ogVideo.getActor(), ogVideo.getDuration(), ogVideo.getTag(), ogVideo.getReleaseDate(), ogVideo.getCreatedTime(), ogVideo.getModifiedTime());
        } else {
            ogVideoDobj = null;   // ????
        }
        OgMovieDataObject ogMovieDobj = null;
        if(ogMovie instanceof OgMovieBean) {
            ogMovieDobj = ((OgMovieBean) ogMovie).toDataObject();            
        } else if(ogMovie instanceof OgMovie) {
            ogMovieDobj = new OgMovieDataObject(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate(), ogMovie.getCreatedTime(), ogMovie.getModifiedTime());
        } else {
            ogMovieDobj = null;   // ????
        }
        OgTvShowDataObject ogTvShowDobj = null;
        if(ogTvShow instanceof OgTvShowBean) {
            ogTvShowDobj = ((OgTvShowBean) ogTvShow).toDataObject();            
        } else if(ogTvShow instanceof OgTvShow) {
            ogTvShowDobj = new OgTvShowDataObject(ogTvShow.getGuid(), ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate(), ogTvShow.getCreatedTime(), ogTvShow.getModifiedTime());
        } else {
            ogTvShowDobj = null;   // ????
        }
        OgTvEpisodeDataObject ogTvEpisodeDobj = null;
        if(ogTvEpisode instanceof OgTvEpisodeBean) {
            ogTvEpisodeDobj = ((OgTvEpisodeBean) ogTvEpisode).toDataObject();            
        } else if(ogTvEpisode instanceof OgTvEpisode) {
            ogTvEpisodeDobj = new OgTvEpisodeDataObject(ogTvEpisode.getGuid(), ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate(), ogTvEpisode.getCreatedTime(), ogTvEpisode.getModifiedTime());
        } else {
            ogTvEpisodeDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OpenGraphMetaDataObject dataObj = new OpenGraphMetaDataObject(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, ogProfileDobj, ogWebsiteDobj, ogBlogDobj, ogArticleDobj, ogBookDobj, ogVideoDobj, ogMovieDobj, ogTvShowDobj, ogTvEpisodeDobj);
        return updateOpenGraphMeta(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param openGraphMeta cannot be null.....
        if(openGraphMeta == null || openGraphMeta.getGuid() == null) {
            log.log(Level.INFO, "Param openGraphMeta or its guid is null!");
            throw new BadRequestException("Param openGraphMeta object or its guid is null!");
        }
        OpenGraphMetaDataObject dataObj = null;
        if(openGraphMeta instanceof OpenGraphMetaDataObject) {
            dataObj = (OpenGraphMetaDataObject) openGraphMeta;
        } else if(openGraphMeta instanceof OpenGraphMetaBean) {
            dataObj = ((OpenGraphMetaBean) openGraphMeta).toDataObject();
        } else {  // if(openGraphMeta instanceof OpenGraphMeta)
            dataObj = new OpenGraphMetaDataObject(openGraphMeta.getGuid(), openGraphMeta.getUser(), openGraphMeta.getFetchRequest(), openGraphMeta.getTargetUrl(), openGraphMeta.getPageUrl(), openGraphMeta.getQueryString(), openGraphMeta.getQueryParams(), openGraphMeta.getLastFetchResult(), openGraphMeta.getResponseCode(), openGraphMeta.getContentType(), openGraphMeta.getContentLength(), openGraphMeta.getLanguage(), openGraphMeta.getRedirect(), openGraphMeta.getLocation(), openGraphMeta.getPageTitle(), openGraphMeta.getNote(), openGraphMeta.isDeferred(), openGraphMeta.getStatus(), openGraphMeta.getRefreshStatus(), openGraphMeta.getRefreshInterval(), openGraphMeta.getNextRefreshTime(), openGraphMeta.getLastCheckedTime(), openGraphMeta.getLastUpdatedTime(), openGraphMeta.getOgType(), openGraphMeta.getOgProfile(), openGraphMeta.getOgWebsite(), openGraphMeta.getOgBlog(), openGraphMeta.getOgArticle(), openGraphMeta.getOgBook(), openGraphMeta.getOgVideo(), openGraphMeta.getOgMovie(), openGraphMeta.getOgTvShow(), openGraphMeta.getOgTvEpisode());
        }
        Boolean suc = getDAOFactory().getOpenGraphMetaDAO().updateOpenGraphMeta(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteOpenGraphMeta(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getOpenGraphMetaDAO().deleteOpenGraphMeta(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param openGraphMeta cannot be null.....
        if(openGraphMeta == null || openGraphMeta.getGuid() == null) {
            log.log(Level.INFO, "Param openGraphMeta or its guid is null!");
            throw new BadRequestException("Param openGraphMeta object or its guid is null!");
        }
        OpenGraphMetaDataObject dataObj = null;
        if(openGraphMeta instanceof OpenGraphMetaDataObject) {
            dataObj = (OpenGraphMetaDataObject) openGraphMeta;
        } else if(openGraphMeta instanceof OpenGraphMetaBean) {
            dataObj = ((OpenGraphMetaBean) openGraphMeta).toDataObject();
        } else {  // if(openGraphMeta instanceof OpenGraphMeta)
            dataObj = new OpenGraphMetaDataObject(openGraphMeta.getGuid(), openGraphMeta.getUser(), openGraphMeta.getFetchRequest(), openGraphMeta.getTargetUrl(), openGraphMeta.getPageUrl(), openGraphMeta.getQueryString(), openGraphMeta.getQueryParams(), openGraphMeta.getLastFetchResult(), openGraphMeta.getResponseCode(), openGraphMeta.getContentType(), openGraphMeta.getContentLength(), openGraphMeta.getLanguage(), openGraphMeta.getRedirect(), openGraphMeta.getLocation(), openGraphMeta.getPageTitle(), openGraphMeta.getNote(), openGraphMeta.isDeferred(), openGraphMeta.getStatus(), openGraphMeta.getRefreshStatus(), openGraphMeta.getRefreshInterval(), openGraphMeta.getNextRefreshTime(), openGraphMeta.getLastCheckedTime(), openGraphMeta.getLastUpdatedTime(), openGraphMeta.getOgType(), openGraphMeta.getOgProfile(), openGraphMeta.getOgWebsite(), openGraphMeta.getOgBlog(), openGraphMeta.getOgArticle(), openGraphMeta.getOgBook(), openGraphMeta.getOgVideo(), openGraphMeta.getOgMovie(), openGraphMeta.getOgTvShow(), openGraphMeta.getOgTvEpisode());
        }
        Boolean suc = getDAOFactory().getOpenGraphMetaDAO().deleteOpenGraphMeta(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteOpenGraphMetas(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getOpenGraphMetaDAO().deleteOpenGraphMetas(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
