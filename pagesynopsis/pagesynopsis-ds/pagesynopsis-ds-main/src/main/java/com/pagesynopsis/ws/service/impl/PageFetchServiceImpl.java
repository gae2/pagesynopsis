package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.bean.UrlStructBean;
import com.pagesynopsis.ws.bean.ImageStructBean;
import com.pagesynopsis.ws.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.bean.AnchorStructBean;
import com.pagesynopsis.ws.bean.KeyValuePairStructBean;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.bean.AudioStructBean;
import com.pagesynopsis.ws.bean.VideoStructBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.bean.PageFetchBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.PageFetchDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.PageFetchService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class PageFetchServiceImpl implements PageFetchService
{
    private static final Logger log = Logger.getLogger(PageFetchServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // PageFetch related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public PageFetch getPageFetch(String guid) throws BaseException
    {
        log.finer("BEGIN");

        PageFetchDataObject dataObj = getDAOFactory().getPageFetchDAO().getPageFetch(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve PageFetchDataObject for guid = " + guid);
            return null;  // ????
        }
        PageFetchBean bean = new PageFetchBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getPageFetch(String guid, String field) throws BaseException
    {
        PageFetchDataObject dataObj = getDAOFactory().getPageFetchDAO().getPageFetch(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve PageFetchDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("fetchRequest")) {
            return dataObj.getFetchRequest();
        } else if(field.equals("targetUrl")) {
            return dataObj.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return dataObj.getPageUrl();
        } else if(field.equals("queryString")) {
            return dataObj.getQueryString();
        } else if(field.equals("queryParams")) {
            return dataObj.getQueryParams();
        } else if(field.equals("lastFetchResult")) {
            return dataObj.getLastFetchResult();
        } else if(field.equals("responseCode")) {
            return dataObj.getResponseCode();
        } else if(field.equals("contentType")) {
            return dataObj.getContentType();
        } else if(field.equals("contentLength")) {
            return dataObj.getContentLength();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("redirect")) {
            return dataObj.getRedirect();
        } else if(field.equals("location")) {
            return dataObj.getLocation();
        } else if(field.equals("pageTitle")) {
            return dataObj.getPageTitle();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("deferred")) {
            return dataObj.isDeferred();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("refreshStatus")) {
            return dataObj.getRefreshStatus();
        } else if(field.equals("refreshInterval")) {
            return dataObj.getRefreshInterval();
        } else if(field.equals("nextRefreshTime")) {
            return dataObj.getNextRefreshTime();
        } else if(field.equals("lastCheckedTime")) {
            return dataObj.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return dataObj.getLastUpdatedTime();
        } else if(field.equals("inputMaxRedirects")) {
            return dataObj.getInputMaxRedirects();
        } else if(field.equals("resultRedirectCount")) {
            return dataObj.getResultRedirectCount();
        } else if(field.equals("redirectPages")) {
            return dataObj.getRedirectPages();
        } else if(field.equals("destinationUrl")) {
            return dataObj.getDestinationUrl();
        } else if(field.equals("pageAuthor")) {
            return dataObj.getPageAuthor();
        } else if(field.equals("pageSummary")) {
            return dataObj.getPageSummary();
        } else if(field.equals("favicon")) {
            return dataObj.getFavicon();
        } else if(field.equals("faviconUrl")) {
            return dataObj.getFaviconUrl();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<PageFetch> getPageFetches(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<PageFetch> list = new ArrayList<PageFetch>();
        List<PageFetchDataObject> dataObjs = getDAOFactory().getPageFetchDAO().getPageFetches(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve PageFetchDataObject list.");
        } else {
            Iterator<PageFetchDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                PageFetchDataObject dataObj = (PageFetchDataObject) it.next();
                list.add(new PageFetchBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<PageFetch> getAllPageFetches() throws BaseException
    {
        return getAllPageFetches(null, null, null);
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageFetches(ordering, offset, count, null);
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllPageFetches(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<PageFetch> list = new ArrayList<PageFetch>();
        List<PageFetchDataObject> dataObjs = getDAOFactory().getPageFetchDAO().getAllPageFetches(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve PageFetchDataObject list.");
        } else {
            Iterator<PageFetchDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                PageFetchDataObject dataObj = (PageFetchDataObject) it.next();
                list.add(new PageFetchBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllPageFetchKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getPageFetchDAO().getAllPageFetchKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve PageFetch key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findPageFetches(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("PageFetchServiceImpl.findPageFetches(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<PageFetch> list = new ArrayList<PageFetch>();
        List<PageFetchDataObject> dataObjs = getDAOFactory().getPageFetchDAO().findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find pageFetches for the given criterion.");
        } else {
            Iterator<PageFetchDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                PageFetchDataObject dataObj = (PageFetchDataObject) it.next();
                list.add(new PageFetchBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("PageFetchServiceImpl.findPageFetchKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getPageFetchDAO().findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find PageFetch keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("PageFetchServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getPageFetchDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createPageFetch(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        PageFetchDataObject dataObj = new PageFetchDataObject(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
        return createPageFetch(dataObj);
    }

    @Override
    public String createPageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("BEGIN");

        // Param pageFetch cannot be null.....
        if(pageFetch == null) {
            log.log(Level.INFO, "Param pageFetch is null!");
            throw new BadRequestException("Param pageFetch object is null!");
        }
        PageFetchDataObject dataObj = null;
        if(pageFetch instanceof PageFetchDataObject) {
            dataObj = (PageFetchDataObject) pageFetch;
        } else if(pageFetch instanceof PageFetchBean) {
            dataObj = ((PageFetchBean) pageFetch).toDataObject();
        } else {  // if(pageFetch instanceof PageFetch)
            //dataObj = new PageFetchDataObject(null, pageFetch.getUser(), pageFetch.getFetchRequest(), pageFetch.getTargetUrl(), pageFetch.getPageUrl(), pageFetch.getQueryString(), pageFetch.getQueryParams(), pageFetch.getLastFetchResult(), pageFetch.getResponseCode(), pageFetch.getContentType(), pageFetch.getContentLength(), pageFetch.getLanguage(), pageFetch.getRedirect(), pageFetch.getLocation(), pageFetch.getPageTitle(), pageFetch.getNote(), pageFetch.isDeferred(), pageFetch.getStatus(), pageFetch.getRefreshStatus(), pageFetch.getRefreshInterval(), pageFetch.getNextRefreshTime(), pageFetch.getLastCheckedTime(), pageFetch.getLastUpdatedTime(), pageFetch.getInputMaxRedirects(), pageFetch.getResultRedirectCount(), pageFetch.getRedirectPages(), pageFetch.getDestinationUrl(), pageFetch.getPageAuthor(), pageFetch.getPageSummary(), pageFetch.getFavicon(), pageFetch.getFaviconUrl());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new PageFetchDataObject(pageFetch.getGuid(), pageFetch.getUser(), pageFetch.getFetchRequest(), pageFetch.getTargetUrl(), pageFetch.getPageUrl(), pageFetch.getQueryString(), pageFetch.getQueryParams(), pageFetch.getLastFetchResult(), pageFetch.getResponseCode(), pageFetch.getContentType(), pageFetch.getContentLength(), pageFetch.getLanguage(), pageFetch.getRedirect(), pageFetch.getLocation(), pageFetch.getPageTitle(), pageFetch.getNote(), pageFetch.isDeferred(), pageFetch.getStatus(), pageFetch.getRefreshStatus(), pageFetch.getRefreshInterval(), pageFetch.getNextRefreshTime(), pageFetch.getLastCheckedTime(), pageFetch.getLastUpdatedTime(), pageFetch.getInputMaxRedirects(), pageFetch.getResultRedirectCount(), pageFetch.getRedirectPages(), pageFetch.getDestinationUrl(), pageFetch.getPageAuthor(), pageFetch.getPageSummary(), pageFetch.getFavicon(), pageFetch.getFaviconUrl());
        }
        String guid = getDAOFactory().getPageFetchDAO().createPageFetch(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updatePageFetch(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        PageFetchDataObject dataObj = new PageFetchDataObject(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
        return updatePageFetch(dataObj);
    }
        
    // ???
    @Override
    public Boolean updatePageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("BEGIN");

        // Param pageFetch cannot be null.....
        if(pageFetch == null || pageFetch.getGuid() == null) {
            log.log(Level.INFO, "Param pageFetch or its guid is null!");
            throw new BadRequestException("Param pageFetch object or its guid is null!");
        }
        PageFetchDataObject dataObj = null;
        if(pageFetch instanceof PageFetchDataObject) {
            dataObj = (PageFetchDataObject) pageFetch;
        } else if(pageFetch instanceof PageFetchBean) {
            dataObj = ((PageFetchBean) pageFetch).toDataObject();
        } else {  // if(pageFetch instanceof PageFetch)
            dataObj = new PageFetchDataObject(pageFetch.getGuid(), pageFetch.getUser(), pageFetch.getFetchRequest(), pageFetch.getTargetUrl(), pageFetch.getPageUrl(), pageFetch.getQueryString(), pageFetch.getQueryParams(), pageFetch.getLastFetchResult(), pageFetch.getResponseCode(), pageFetch.getContentType(), pageFetch.getContentLength(), pageFetch.getLanguage(), pageFetch.getRedirect(), pageFetch.getLocation(), pageFetch.getPageTitle(), pageFetch.getNote(), pageFetch.isDeferred(), pageFetch.getStatus(), pageFetch.getRefreshStatus(), pageFetch.getRefreshInterval(), pageFetch.getNextRefreshTime(), pageFetch.getLastCheckedTime(), pageFetch.getLastUpdatedTime(), pageFetch.getInputMaxRedirects(), pageFetch.getResultRedirectCount(), pageFetch.getRedirectPages(), pageFetch.getDestinationUrl(), pageFetch.getPageAuthor(), pageFetch.getPageSummary(), pageFetch.getFavicon(), pageFetch.getFaviconUrl());
        }
        Boolean suc = getDAOFactory().getPageFetchDAO().updatePageFetch(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deletePageFetch(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getPageFetchDAO().deletePageFetch(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deletePageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("BEGIN");

        // Param pageFetch cannot be null.....
        if(pageFetch == null || pageFetch.getGuid() == null) {
            log.log(Level.INFO, "Param pageFetch or its guid is null!");
            throw new BadRequestException("Param pageFetch object or its guid is null!");
        }
        PageFetchDataObject dataObj = null;
        if(pageFetch instanceof PageFetchDataObject) {
            dataObj = (PageFetchDataObject) pageFetch;
        } else if(pageFetch instanceof PageFetchBean) {
            dataObj = ((PageFetchBean) pageFetch).toDataObject();
        } else {  // if(pageFetch instanceof PageFetch)
            dataObj = new PageFetchDataObject(pageFetch.getGuid(), pageFetch.getUser(), pageFetch.getFetchRequest(), pageFetch.getTargetUrl(), pageFetch.getPageUrl(), pageFetch.getQueryString(), pageFetch.getQueryParams(), pageFetch.getLastFetchResult(), pageFetch.getResponseCode(), pageFetch.getContentType(), pageFetch.getContentLength(), pageFetch.getLanguage(), pageFetch.getRedirect(), pageFetch.getLocation(), pageFetch.getPageTitle(), pageFetch.getNote(), pageFetch.isDeferred(), pageFetch.getStatus(), pageFetch.getRefreshStatus(), pageFetch.getRefreshInterval(), pageFetch.getNextRefreshTime(), pageFetch.getLastCheckedTime(), pageFetch.getLastUpdatedTime(), pageFetch.getInputMaxRedirects(), pageFetch.getResultRedirectCount(), pageFetch.getRedirectPages(), pageFetch.getDestinationUrl(), pageFetch.getPageAuthor(), pageFetch.getPageSummary(), pageFetch.getFavicon(), pageFetch.getFaviconUrl());
        }
        Boolean suc = getDAOFactory().getPageFetchDAO().deletePageFetch(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deletePageFetches(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getPageFetchDAO().deletePageFetches(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
