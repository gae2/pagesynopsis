package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;


public interface RobotsTextFile 
{
    String  getGuid();
    String  getSiteUrl();
    List<RobotsTextGroup>  getGroups();
    List<String>  getSitemaps();
    String  getContent();
    String  getContentHash();
    Long  getLastCheckedTime();
    Long  getLastUpdatedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
