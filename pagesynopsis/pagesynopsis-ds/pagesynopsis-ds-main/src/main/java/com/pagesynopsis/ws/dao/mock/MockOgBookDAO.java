package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OgBookDAO;
import com.pagesynopsis.ws.data.OgBookDataObject;


// MockOgBookDAO is a decorator.
// It can be used as a base class to mock OgBookDAO objects.
public abstract class MockOgBookDAO implements OgBookDAO
{
    private static final Logger log = Logger.getLogger(MockOgBookDAO.class.getName()); 

    // MockOgBookDAO uses the decorator design pattern.
    private OgBookDAO decoratedDAO;

    public MockOgBookDAO(OgBookDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected OgBookDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(OgBookDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public OgBookDataObject getOgBook(String guid) throws BaseException
    {
        return decoratedDAO.getOgBook(guid);
	}

    @Override
    public List<OgBookDataObject> getOgBooks(List<String> guids) throws BaseException
    {
        return decoratedDAO.getOgBooks(guids);
    }

    @Override
    public List<OgBookDataObject> getAllOgBooks() throws BaseException
	{
	    return getAllOgBooks(null, null, null);
    }


    @Override
    public List<OgBookDataObject> getAllOgBooks(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOgBooks(ordering, offset, count, null);
    }

    @Override
    public List<OgBookDataObject> getAllOgBooks(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllOgBooks(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBookKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllOgBookKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<OgBookDataObject> findOgBooks(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOgBooks(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OgBookDataObject> findOgBooks(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOgBooks(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OgBookDataObject> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<OgBookDataObject> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgBook(OgBookDataObject ogBook) throws BaseException
    {
        return decoratedDAO.createOgBook( ogBook);
    }

    @Override
	public Boolean updateOgBook(OgBookDataObject ogBook) throws BaseException
	{
        return decoratedDAO.updateOgBook(ogBook);
	}
	
    @Override
    public Boolean deleteOgBook(OgBookDataObject ogBook) throws BaseException
    {
        return decoratedDAO.deleteOgBook(ogBook);
    }

    @Override
    public Boolean deleteOgBook(String guid) throws BaseException
    {
        return decoratedDAO.deleteOgBook(guid);
	}

    @Override
    public Long deleteOgBooks(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteOgBooks(filter, params, values);
    }

}
