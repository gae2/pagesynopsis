package com.pagesynopsis.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ResourceAlreadyPresentException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.DataStoreRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.GeoPointStruct;
import com.pagesynopsis.ws.StreetAddressStruct;
import com.pagesynopsis.ws.GaeAppStruct;
import com.pagesynopsis.ws.FullNameStruct;
import com.pagesynopsis.ws.GaeUserStruct;
import com.pagesynopsis.ws.User;
import com.pagesynopsis.ws.bean.UserBean;
import com.pagesynopsis.ws.stub.UserListStub;
import com.pagesynopsis.ws.stub.UserStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.UserResource;
import com.pagesynopsis.ws.resource.util.GeoPointStructResourceUtil;
import com.pagesynopsis.ws.resource.util.StreetAddressStructResourceUtil;
import com.pagesynopsis.ws.resource.util.GaeAppStructResourceUtil;
import com.pagesynopsis.ws.resource.util.FullNameStructResourceUtil;
import com.pagesynopsis.ws.resource.util.GaeUserStructResourceUtil;

// MockUserResource is a decorator.
// It can be used as a base class to mock UserResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/users/")
public abstract class MockUserResource implements UserResource
{
    private static final Logger log = Logger.getLogger(MockUserResource.class.getName());

    // MockUserResource uses the decorator design pattern.
    private UserResource decoratedResource;

    public MockUserResource(UserResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected UserResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(UserResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUsers(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUserKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getUserKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getUserKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getUser(String guid) throws BaseResourceException
    {
        return decoratedResource.getUser(guid);
    }

    @Override
    public Response getUser(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getUser(guid, field);
    }

    @Override
    public Response createUser(UserStub user) throws BaseResourceException
    {
        return decoratedResource.createUser(user);
    }

    @Override
    public Response updateUser(String guid, UserStub user) throws BaseResourceException
    {
        return decoratedResource.updateUser(guid, user);
    }

    @Override
    public Response updateUser(String guid, String managerApp, Long appAcl, String gaeApp, String aeryId, String sessionId, String ancestorGuid, String name, String usercode, String username, String nickname, String avatar, String email, String openId, String gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, String streetAddress, String geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseResourceException
    {
        return decoratedResource.updateUser(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, ancestorGuid, name, usercode, username, nickname, avatar, email, openId, gaeUser, entityType, surrogate, obsolete, timeZone, location, streetAddress, geoPoint, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public Response deleteUser(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteUser(guid);
    }

    @Override
    public Response deleteUsers(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteUsers(filter, params, values);
    }


}
