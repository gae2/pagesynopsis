package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.FetchRequestDAO;
import com.pagesynopsis.ws.data.FetchRequestDataObject;


// MockFetchRequestDAO is a decorator.
// It can be used as a base class to mock FetchRequestDAO objects.
public abstract class MockFetchRequestDAO implements FetchRequestDAO
{
    private static final Logger log = Logger.getLogger(MockFetchRequestDAO.class.getName()); 

    // MockFetchRequestDAO uses the decorator design pattern.
    private FetchRequestDAO decoratedDAO;

    public MockFetchRequestDAO(FetchRequestDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected FetchRequestDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(FetchRequestDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public FetchRequestDataObject getFetchRequest(String guid) throws BaseException
    {
        return decoratedDAO.getFetchRequest(guid);
	}

    @Override
    public List<FetchRequestDataObject> getFetchRequests(List<String> guids) throws BaseException
    {
        return decoratedDAO.getFetchRequests(guids);
    }

    @Override
    public List<FetchRequestDataObject> getAllFetchRequests() throws BaseException
	{
	    return getAllFetchRequests(null, null, null);
    }


    @Override
    public List<FetchRequestDataObject> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllFetchRequests(ordering, offset, count, null);
    }

    @Override
    public List<FetchRequestDataObject> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllFetchRequests(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<FetchRequestDataObject> findFetchRequests(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findFetchRequests(filter, ordering, params, values, null, null);
    }

    @Override
	public List<FetchRequestDataObject> findFetchRequests(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findFetchRequests(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<FetchRequestDataObject> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<FetchRequestDataObject> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createFetchRequest(FetchRequestDataObject fetchRequest) throws BaseException
    {
        return decoratedDAO.createFetchRequest( fetchRequest);
    }

    @Override
	public Boolean updateFetchRequest(FetchRequestDataObject fetchRequest) throws BaseException
	{
        return decoratedDAO.updateFetchRequest(fetchRequest);
	}
	
    @Override
    public Boolean deleteFetchRequest(FetchRequestDataObject fetchRequest) throws BaseException
    {
        return decoratedDAO.deleteFetchRequest(fetchRequest);
    }

    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        return decoratedDAO.deleteFetchRequest(guid);
	}

    @Override
    public Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteFetchRequests(filter, params, values);
    }

}
