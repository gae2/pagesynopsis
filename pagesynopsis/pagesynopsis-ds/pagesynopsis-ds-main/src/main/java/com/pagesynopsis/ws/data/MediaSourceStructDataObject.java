package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class MediaSourceStructDataObject implements MediaSourceStruct, Serializable
{
    private static final Logger log = Logger.getLogger(MediaSourceStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _mediasourcestruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String src;

    @Persistent(defaultFetchGroup = "true")
    private String srcUrl;

    @Persistent(defaultFetchGroup = "true")
    private String type;

    @Persistent(defaultFetchGroup = "true")
    private String remark;

    public MediaSourceStructDataObject()
    {
        // ???
        // this(null, null, null, null, null);
    }
    public MediaSourceStructDataObject(String uuid, String src, String srcUrl, String type, String remark)
    {
        setUuid(uuid);
        setSrc(src);
        setSrcUrl(srcUrl);
        setType(type);
        setRemark(remark);
    }

    private void resetEncodedKey()
    {
        if(_mediasourcestruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _mediasourcestruct_encoded_key = KeyFactory.createKeyString(MediaSourceStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _mediasourcestruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _mediasourcestruct_encoded_key = KeyFactory.createKeyString(MediaSourceStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getSrc()
    {
        return this.src;
    }
    public void setSrc(String src)
    {
        this.src = src;
        if(this.src != null) {
            resetEncodedKey();
        }
    }

    public String getSrcUrl()
    {
        return this.srcUrl;
    }
    public void setSrcUrl(String srcUrl)
    {
        this.srcUrl = srcUrl;
        if(this.srcUrl != null) {
            resetEncodedKey();
        }
    }

    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
        if(this.type != null) {
            resetEncodedKey();
        }
    }

    public String getRemark()
    {
        return this.remark;
    }
    public void setRemark(String remark)
    {
        this.remark = remark;
        if(this.remark != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getSrc() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrcUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRemark() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("src", this.src);
        dataMap.put("srcUrl", this.srcUrl);
        dataMap.put("type", this.type);
        dataMap.put("remark", this.remark);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        MediaSourceStruct thatObj = (MediaSourceStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.src == null && thatObj.getSrc() != null)
            || (this.src != null && thatObj.getSrc() == null)
            || !this.src.equals(thatObj.getSrc()) ) {
            return false;
        }
        if( (this.srcUrl == null && thatObj.getSrcUrl() != null)
            || (this.srcUrl != null && thatObj.getSrcUrl() == null)
            || !this.srcUrl.equals(thatObj.getSrcUrl()) ) {
            return false;
        }
        if( (this.type == null && thatObj.getType() != null)
            || (this.type != null && thatObj.getType() == null)
            || !this.type.equals(thatObj.getType()) ) {
            return false;
        }
        if( (this.remark == null && thatObj.getRemark() != null)
            || (this.remark != null && thatObj.getRemark() == null)
            || !this.remark.equals(thatObj.getRemark()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = src == null ? 0 : src.hashCode();
        _hash = 31 * _hash + delta;
        delta = srcUrl == null ? 0 : srcUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = remark == null ? 0 : remark.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
