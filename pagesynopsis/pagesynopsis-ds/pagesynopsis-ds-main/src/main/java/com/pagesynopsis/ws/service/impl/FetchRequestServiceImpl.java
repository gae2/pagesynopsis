package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.bean.NotificationStructBean;
import com.pagesynopsis.ws.bean.KeyValuePairStructBean;
import com.pagesynopsis.ws.bean.ReferrerInfoStructBean;
import com.pagesynopsis.ws.bean.FetchRequestBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.NotificationStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.ReferrerInfoStructDataObject;
import com.pagesynopsis.ws.data.FetchRequestDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.FetchRequestService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FetchRequestServiceImpl implements FetchRequestService
{
    private static final Logger log = Logger.getLogger(FetchRequestServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // FetchRequest related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        log.finer("BEGIN");

        FetchRequestDataObject dataObj = getDAOFactory().getFetchRequestDAO().getFetchRequest(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FetchRequestDataObject for guid = " + guid);
            return null;  // ????
        }
        FetchRequestBean bean = new FetchRequestBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        FetchRequestDataObject dataObj = getDAOFactory().getFetchRequestDAO().getFetchRequest(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FetchRequestDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("targetUrl")) {
            return dataObj.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return dataObj.getPageUrl();
        } else if(field.equals("queryString")) {
            return dataObj.getQueryString();
        } else if(field.equals("queryParams")) {
            return dataObj.getQueryParams();
        } else if(field.equals("version")) {
            return dataObj.getVersion();
        } else if(field.equals("fetchObject")) {
            return dataObj.getFetchObject();
        } else if(field.equals("fetchStatus")) {
            return dataObj.getFetchStatus();
        } else if(field.equals("result")) {
            return dataObj.getResult();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("referrerInfo")) {
            return dataObj.getReferrerInfo();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("nextPageUrl")) {
            return dataObj.getNextPageUrl();
        } else if(field.equals("followPages")) {
            return dataObj.getFollowPages();
        } else if(field.equals("followDepth")) {
            return dataObj.getFollowDepth();
        } else if(field.equals("createVersion")) {
            return dataObj.isCreateVersion();
        } else if(field.equals("deferred")) {
            return dataObj.isDeferred();
        } else if(field.equals("alert")) {
            return dataObj.isAlert();
        } else if(field.equals("notificationPref")) {
            return dataObj.getNotificationPref();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<FetchRequest> list = new ArrayList<FetchRequest>();
        List<FetchRequestDataObject> dataObjs = getDAOFactory().getFetchRequestDAO().getFetchRequests(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve FetchRequestDataObject list.");
        } else {
            Iterator<FetchRequestDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FetchRequestDataObject dataObj = (FetchRequestDataObject) it.next();
                list.add(new FetchRequestBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return getAllFetchRequests(null, null, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequests(ordering, offset, count, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFetchRequests(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FetchRequest> list = new ArrayList<FetchRequest>();
        List<FetchRequestDataObject> dataObjs = getDAOFactory().getFetchRequestDAO().getAllFetchRequests(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve FetchRequestDataObject list.");
        } else {
            Iterator<FetchRequestDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FetchRequestDataObject dataObj = (FetchRequestDataObject) it.next();
                list.add(new FetchRequestBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllFetchRequestKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getFetchRequestDAO().getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve FetchRequest key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FetchRequestServiceImpl.findFetchRequests(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FetchRequest> list = new ArrayList<FetchRequest>();
        List<FetchRequestDataObject> dataObjs = getDAOFactory().getFetchRequestDAO().findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find fetchRequests for the given criterion.");
        } else {
            Iterator<FetchRequestDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                FetchRequestDataObject dataObj = (FetchRequestDataObject) it.next();
                list.add(new FetchRequestBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FetchRequestServiceImpl.findFetchRequestKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getFetchRequestDAO().findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find FetchRequest keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("FetchRequestServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getFetchRequestDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createFetchRequest(String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }
        NotificationStructDataObject notificationPrefDobj = null;
        if(notificationPref instanceof NotificationStructBean) {
            notificationPrefDobj = ((NotificationStructBean) notificationPref).toDataObject();
        } else if(notificationPref instanceof NotificationStruct) {
            notificationPrefDobj = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getTwitterUsername(), notificationPref.getFacebookId(), notificationPref.getLinkedinId(), notificationPref.getNote());
        } else {
            notificationPrefDobj = null;   // ????
        }
        
        FetchRequestDataObject dataObj = new FetchRequestDataObject(null, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfoDobj, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPrefDobj);
        return createFetchRequest(dataObj);
    }

    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        // Param fetchRequest cannot be null.....
        if(fetchRequest == null) {
            log.log(Level.INFO, "Param fetchRequest is null!");
            throw new BadRequestException("Param fetchRequest object is null!");
        }
        FetchRequestDataObject dataObj = null;
        if(fetchRequest instanceof FetchRequestDataObject) {
            dataObj = (FetchRequestDataObject) fetchRequest;
        } else if(fetchRequest instanceof FetchRequestBean) {
            dataObj = ((FetchRequestBean) fetchRequest).toDataObject();
        } else {  // if(fetchRequest instanceof FetchRequest)
            //dataObj = new FetchRequestDataObject(null, fetchRequest.getUser(), fetchRequest.getTargetUrl(), fetchRequest.getPageUrl(), fetchRequest.getQueryString(), fetchRequest.getQueryParams(), fetchRequest.getVersion(), fetchRequest.getFetchObject(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getNote(), (ReferrerInfoStructDataObject) fetchRequest.getReferrerInfo(), fetchRequest.getStatus(), fetchRequest.getNextPageUrl(), fetchRequest.getFollowPages(), fetchRequest.getFollowDepth(), fetchRequest.isCreateVersion(), fetchRequest.isDeferred(), fetchRequest.isAlert(), (NotificationStructDataObject) fetchRequest.getNotificationPref());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new FetchRequestDataObject(fetchRequest.getGuid(), fetchRequest.getUser(), fetchRequest.getTargetUrl(), fetchRequest.getPageUrl(), fetchRequest.getQueryString(), fetchRequest.getQueryParams(), fetchRequest.getVersion(), fetchRequest.getFetchObject(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getNote(), (ReferrerInfoStructDataObject) fetchRequest.getReferrerInfo(), fetchRequest.getStatus(), fetchRequest.getNextPageUrl(), fetchRequest.getFollowPages(), fetchRequest.getFollowDepth(), fetchRequest.isCreateVersion(), fetchRequest.isDeferred(), fetchRequest.isAlert(), (NotificationStructDataObject) fetchRequest.getNotificationPref());
        }
        String guid = getDAOFactory().getFetchRequestDAO().createFetchRequest(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateFetchRequest(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        ReferrerInfoStructDataObject referrerInfoDobj = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoDobj = ((ReferrerInfoStructBean) referrerInfo).toDataObject();            
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoDobj = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoDobj = null;   // ????
        }
        NotificationStructDataObject notificationPrefDobj = null;
        if(notificationPref instanceof NotificationStructBean) {
            notificationPrefDobj = ((NotificationStructBean) notificationPref).toDataObject();            
        } else if(notificationPref instanceof NotificationStruct) {
            notificationPrefDobj = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getTwitterUsername(), notificationPref.getFacebookId(), notificationPref.getLinkedinId(), notificationPref.getNote());
        } else {
            notificationPrefDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        FetchRequestDataObject dataObj = new FetchRequestDataObject(guid, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfoDobj, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPrefDobj);
        return updateFetchRequest(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        // Param fetchRequest cannot be null.....
        if(fetchRequest == null || fetchRequest.getGuid() == null) {
            log.log(Level.INFO, "Param fetchRequest or its guid is null!");
            throw new BadRequestException("Param fetchRequest object or its guid is null!");
        }
        FetchRequestDataObject dataObj = null;
        if(fetchRequest instanceof FetchRequestDataObject) {
            dataObj = (FetchRequestDataObject) fetchRequest;
        } else if(fetchRequest instanceof FetchRequestBean) {
            dataObj = ((FetchRequestBean) fetchRequest).toDataObject();
        } else {  // if(fetchRequest instanceof FetchRequest)
            dataObj = new FetchRequestDataObject(fetchRequest.getGuid(), fetchRequest.getUser(), fetchRequest.getTargetUrl(), fetchRequest.getPageUrl(), fetchRequest.getQueryString(), fetchRequest.getQueryParams(), fetchRequest.getVersion(), fetchRequest.getFetchObject(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getNote(), fetchRequest.getReferrerInfo(), fetchRequest.getStatus(), fetchRequest.getNextPageUrl(), fetchRequest.getFollowPages(), fetchRequest.getFollowDepth(), fetchRequest.isCreateVersion(), fetchRequest.isDeferred(), fetchRequest.isAlert(), fetchRequest.getNotificationPref());
        }
        Boolean suc = getDAOFactory().getFetchRequestDAO().updateFetchRequest(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getFetchRequestDAO().deleteFetchRequest(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        // Param fetchRequest cannot be null.....
        if(fetchRequest == null || fetchRequest.getGuid() == null) {
            log.log(Level.INFO, "Param fetchRequest or its guid is null!");
            throw new BadRequestException("Param fetchRequest object or its guid is null!");
        }
        FetchRequestDataObject dataObj = null;
        if(fetchRequest instanceof FetchRequestDataObject) {
            dataObj = (FetchRequestDataObject) fetchRequest;
        } else if(fetchRequest instanceof FetchRequestBean) {
            dataObj = ((FetchRequestBean) fetchRequest).toDataObject();
        } else {  // if(fetchRequest instanceof FetchRequest)
            dataObj = new FetchRequestDataObject(fetchRequest.getGuid(), fetchRequest.getUser(), fetchRequest.getTargetUrl(), fetchRequest.getPageUrl(), fetchRequest.getQueryString(), fetchRequest.getQueryParams(), fetchRequest.getVersion(), fetchRequest.getFetchObject(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getNote(), fetchRequest.getReferrerInfo(), fetchRequest.getStatus(), fetchRequest.getNextPageUrl(), fetchRequest.getFollowPages(), fetchRequest.getFollowDepth(), fetchRequest.isCreateVersion(), fetchRequest.isDeferred(), fetchRequest.isAlert(), fetchRequest.getNotificationPref());
        }
        Boolean suc = getDAOFactory().getFetchRequestDAO().deleteFetchRequest(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getFetchRequestDAO().deleteFetchRequests(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
