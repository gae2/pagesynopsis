package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OgBlogDAO;
import com.pagesynopsis.ws.data.OgBlogDataObject;


// MockOgBlogDAO is a decorator.
// It can be used as a base class to mock OgBlogDAO objects.
public abstract class MockOgBlogDAO implements OgBlogDAO
{
    private static final Logger log = Logger.getLogger(MockOgBlogDAO.class.getName()); 

    // MockOgBlogDAO uses the decorator design pattern.
    private OgBlogDAO decoratedDAO;

    public MockOgBlogDAO(OgBlogDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected OgBlogDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(OgBlogDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public OgBlogDataObject getOgBlog(String guid) throws BaseException
    {
        return decoratedDAO.getOgBlog(guid);
	}

    @Override
    public List<OgBlogDataObject> getOgBlogs(List<String> guids) throws BaseException
    {
        return decoratedDAO.getOgBlogs(guids);
    }

    @Override
    public List<OgBlogDataObject> getAllOgBlogs() throws BaseException
	{
	    return getAllOgBlogs(null, null, null);
    }


    @Override
    public List<OgBlogDataObject> getAllOgBlogs(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOgBlogs(ordering, offset, count, null);
    }

    @Override
    public List<OgBlogDataObject> getAllOgBlogs(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllOgBlogs(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBlogKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllOgBlogKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<OgBlogDataObject> findOgBlogs(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOgBlogs(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OgBlogDataObject> findOgBlogs(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOgBlogs(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OgBlogDataObject> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<OgBlogDataObject> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgBlog(OgBlogDataObject ogBlog) throws BaseException
    {
        return decoratedDAO.createOgBlog( ogBlog);
    }

    @Override
	public Boolean updateOgBlog(OgBlogDataObject ogBlog) throws BaseException
	{
        return decoratedDAO.updateOgBlog(ogBlog);
	}
	
    @Override
    public Boolean deleteOgBlog(OgBlogDataObject ogBlog) throws BaseException
    {
        return decoratedDAO.deleteOgBlog(ogBlog);
    }

    @Override
    public Boolean deleteOgBlog(String guid) throws BaseException
    {
        return decoratedDAO.deleteOgBlog(guid);
	}

    @Override
    public Long deleteOgBlogs(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteOgBlogs(filter, params, values);
    }

}
