package com.pagesynopsis.ws;



public interface TwitterPhotoCard extends TwitterCardBase
{
    String  getImage();
    Integer  getImageWidth();
    Integer  getImageHeight();
}
