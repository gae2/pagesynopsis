package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "robotsTextFiles")
@XmlType(propOrder = {"robotsTextFile", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RobotsTextFileListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextFileListStub.class.getName());

    private List<RobotsTextFileStub> robotsTextFiles = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public RobotsTextFileListStub()
    {
        this(new ArrayList<RobotsTextFileStub>());
    }
    public RobotsTextFileListStub(List<RobotsTextFileStub> robotsTextFiles)
    {
        this(robotsTextFiles, null);
    }
    public RobotsTextFileListStub(List<RobotsTextFileStub> robotsTextFiles, String forwardCursor)
    {
        this.robotsTextFiles = robotsTextFiles;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(robotsTextFiles == null) {
            return true;
        } else {
            return robotsTextFiles.isEmpty();
        }
    }
    public int getSize()
    {
        if(robotsTextFiles == null) {
            return 0;
        } else {
            return robotsTextFiles.size();
        }
    }


    @XmlElement(name = "robotsTextFile")
    public List<RobotsTextFileStub> getRobotsTextFile()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<RobotsTextFileStub> getList()
    {
        return robotsTextFiles;
    }
    public void setList(List<RobotsTextFileStub> robotsTextFiles)
    {
        this.robotsTextFiles = robotsTextFiles;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<RobotsTextFileStub> it = this.robotsTextFiles.iterator();
        while(it.hasNext()) {
            RobotsTextFileStub robotsTextFile = it.next();
            sb.append(robotsTextFile.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RobotsTextFileListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of RobotsTextFileListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextFileListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextFileListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextFileListStub object as a string.", e);
        }
        
        return null;
    }
    public static RobotsTextFileListStub fromJsonString(String jsonStr)
    {
        try {
            RobotsTextFileListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RobotsTextFileListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextFileListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextFileListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextFileListStub object.", e);
        }
        
        return null;
    }

}
