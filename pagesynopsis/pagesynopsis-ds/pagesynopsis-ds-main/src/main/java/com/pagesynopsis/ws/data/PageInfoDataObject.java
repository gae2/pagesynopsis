package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class PageInfoDataObject extends PageBaseDataObject implements PageInfo
{
    private static final Logger log = Logger.getLogger(PageInfoDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(PageInfoDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(PageInfoDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String pageAuthor;

    @Persistent(defaultFetchGroup = "true")
    private List<String> pageDescription;

    @Persistent(defaultFetchGroup = "true")
    private String favicon;

    @Persistent(defaultFetchGroup = "true")
    private String faviconUrl;

    public PageInfoDataObject()
    {
        this(null);
    }
    public PageInfoDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public PageInfoDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String pageAuthor, String pageDescription, String favicon, String faviconUrl)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, pageAuthor, pageDescription, favicon, faviconUrl, null, null);
    }
    public PageInfoDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String pageAuthor, String pageDescription, String favicon, String faviconUrl, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);
        this.pageAuthor = pageAuthor;
        setPageDescription(pageDescription);
        this.favicon = favicon;
        this.faviconUrl = faviconUrl;
    }

//    @Override
//    protected Key createKey()
//    {
//        return PageInfoDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return PageInfoDataObject.composeKey(getGuid());
    }

    public String getPageAuthor()
    {
        return this.pageAuthor;
    }
    public void setPageAuthor(String pageAuthor)
    {
        this.pageAuthor = pageAuthor;
    }

    public String getPageDescription()
    {
        if(this.pageDescription == null) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for(String v : this.pageDescription) {
            sb.append(v);
        }
        return sb.toString(); 
    }
    public void setPageDescription(String pageDescription)
    {
        if(pageDescription == null) {
            this.pageDescription = null;
        } else {
            List<String> _list = new ArrayList<String>();
            int _beg = 0;
            int _rem = pageDescription.length();
            while(_rem > 0) {
                int _sz;
                if((_rem - 500) >= 0) {
                    _sz = 500;
                    _rem -= 500;
                } else {
                    _sz = _rem;
                    _rem = 0;
                }
                _list.add(pageDescription.substring(_beg, _beg+_sz));
                _beg += _sz;
            }
            this.pageDescription = _list;
        }
    }

    public String getFavicon()
    {
        return this.favicon;
    }
    public void setFavicon(String favicon)
    {
        this.favicon = favicon;
    }

    public String getFaviconUrl()
    {
        return this.faviconUrl;
    }
    public void setFaviconUrl(String faviconUrl)
    {
        this.faviconUrl = faviconUrl;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("pageAuthor", this.pageAuthor);
        dataMap.put("pageDescription", this.pageDescription);
        dataMap.put("favicon", this.favicon);
        dataMap.put("faviconUrl", this.faviconUrl);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        PageInfo thatObj = (PageInfo) obj;
        if( (this.pageAuthor == null && thatObj.getPageAuthor() != null)
            || (this.pageAuthor != null && thatObj.getPageAuthor() == null)
            || !this.pageAuthor.equals(thatObj.getPageAuthor()) ) {
            return false;
        }
        if( (this.pageDescription == null && thatObj.getPageDescription() != null)
            || (this.pageDescription != null && thatObj.getPageDescription() == null)
            || !this.pageDescription.equals(thatObj.getPageDescription()) ) {
            return false;
        }
        if( (this.favicon == null && thatObj.getFavicon() != null)
            || (this.favicon != null && thatObj.getFavicon() == null)
            || !this.favicon.equals(thatObj.getFavicon()) ) {
            return false;
        }
        if( (this.faviconUrl == null && thatObj.getFaviconUrl() != null)
            || (this.faviconUrl != null && thatObj.getFaviconUrl() == null)
            || !this.faviconUrl.equals(thatObj.getFaviconUrl()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = pageAuthor == null ? 0 : pageAuthor.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageDescription == null ? 0 : pageDescription.hashCode();
        _hash = 31 * _hash + delta;
        delta = favicon == null ? 0 : favicon.hashCode();
        _hash = 31 * _hash + delta;
        delta = faviconUrl == null ? 0 : faviconUrl.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
