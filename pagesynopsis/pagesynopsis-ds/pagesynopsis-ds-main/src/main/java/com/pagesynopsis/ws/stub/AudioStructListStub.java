package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "audioStructs")
@XmlType(propOrder = {"audioStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AudioStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AudioStructListStub.class.getName());

    private List<AudioStructStub> audioStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public AudioStructListStub()
    {
        this(new ArrayList<AudioStructStub>());
    }
    public AudioStructListStub(List<AudioStructStub> audioStructs)
    {
        this(audioStructs, null);
    }
    public AudioStructListStub(List<AudioStructStub> audioStructs, String forwardCursor)
    {
        this.audioStructs = audioStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(audioStructs == null) {
            return true;
        } else {
            return audioStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(audioStructs == null) {
            return 0;
        } else {
            return audioStructs.size();
        }
    }


    @XmlElement(name = "audioStruct")
    public List<AudioStructStub> getAudioStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<AudioStructStub> getList()
    {
        return audioStructs;
    }
    public void setList(List<AudioStructStub> audioStructs)
    {
        this.audioStructs = audioStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<AudioStructStub> it = this.audioStructs.iterator();
        while(it.hasNext()) {
            AudioStructStub audioStruct = it.next();
            sb.append(audioStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AudioStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AudioStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AudioStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AudioStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AudioStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static AudioStructListStub fromJsonString(String jsonStr)
    {
        try {
            AudioStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AudioStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AudioStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AudioStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AudioStructListStub object.", e);
        }
        
        return null;
    }

}
