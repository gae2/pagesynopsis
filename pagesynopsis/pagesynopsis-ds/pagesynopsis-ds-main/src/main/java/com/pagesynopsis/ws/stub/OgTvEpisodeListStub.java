package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogTvEpisodes")
@XmlType(propOrder = {"ogTvEpisode", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgTvEpisodeListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgTvEpisodeListStub.class.getName());

    private List<OgTvEpisodeStub> ogTvEpisodes = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgTvEpisodeListStub()
    {
        this(new ArrayList<OgTvEpisodeStub>());
    }
    public OgTvEpisodeListStub(List<OgTvEpisodeStub> ogTvEpisodes)
    {
        this(ogTvEpisodes, null);
    }
    public OgTvEpisodeListStub(List<OgTvEpisodeStub> ogTvEpisodes, String forwardCursor)
    {
        this.ogTvEpisodes = ogTvEpisodes;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogTvEpisodes == null) {
            return true;
        } else {
            return ogTvEpisodes.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogTvEpisodes == null) {
            return 0;
        } else {
            return ogTvEpisodes.size();
        }
    }


    @XmlElement(name = "ogTvEpisode")
    public List<OgTvEpisodeStub> getOgTvEpisode()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgTvEpisodeStub> getList()
    {
        return ogTvEpisodes;
    }
    public void setList(List<OgTvEpisodeStub> ogTvEpisodes)
    {
        this.ogTvEpisodes = ogTvEpisodes;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgTvEpisodeStub> it = this.ogTvEpisodes.iterator();
        while(it.hasNext()) {
            OgTvEpisodeStub ogTvEpisode = it.next();
            sb.append(ogTvEpisode.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgTvEpisodeListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgTvEpisodeListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgTvEpisodeListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgTvEpisodeListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgTvEpisodeListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgTvEpisodeListStub fromJsonString(String jsonStr)
    {
        try {
            OgTvEpisodeListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgTvEpisodeListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgTvEpisodeListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgTvEpisodeListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgTvEpisodeListStub object.", e);
        }
        
        return null;
    }

}
