package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.bean.RobotsTextRefreshBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.RobotsTextRefreshDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.RobotsTextRefreshService;


// RobotsTextRefreshMockService is a decorator.
// It can be used as a base class to mock RobotsTextRefreshService objects.
public abstract class RobotsTextRefreshMockService implements RobotsTextRefreshService
{
    private static final Logger log = Logger.getLogger(RobotsTextRefreshMockService.class.getName());

    // RobotsTextRefreshMockService uses the decorator design pattern.
    private RobotsTextRefreshService decoratedService;

    public RobotsTextRefreshMockService(RobotsTextRefreshService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected RobotsTextRefreshService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(RobotsTextRefreshService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // RobotsTextRefresh related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public RobotsTextRefresh getRobotsTextRefresh(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getRobotsTextRefresh(): guid = " + guid);
        RobotsTextRefresh bean = decoratedService.getRobotsTextRefresh(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getRobotsTextRefresh(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getRobotsTextRefresh(guid, field);
        return obj;
    }

    @Override
    public List<RobotsTextRefresh> getRobotsTextRefreshes(List<String> guids) throws BaseException
    {
        log.fine("getRobotsTextRefreshes()");
        List<RobotsTextRefresh> robotsTextRefreshes = decoratedService.getRobotsTextRefreshes(guids);
        log.finer("END");
        return robotsTextRefreshes;
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes() throws BaseException
    {
        return getAllRobotsTextRefreshes(null, null, null);
    }


    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextRefreshes(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRobotsTextRefreshes(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<RobotsTextRefresh> robotsTextRefreshes = decoratedService.getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
        log.finer("END");
        return robotsTextRefreshes;
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextRefreshKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRobotsTextRefreshKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextRefreshMockService.findRobotsTextRefreshes(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<RobotsTextRefresh> robotsTextRefreshes = decoratedService.findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return robotsTextRefreshes;
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextRefreshMockService.findRobotsTextRefreshKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("RobotsTextRefreshMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createRobotsTextRefresh(String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        return decoratedService.createRobotsTextRefresh(robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
    }

    @Override
    public String createRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createRobotsTextRefresh(robotsTextRefresh);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateRobotsTextRefresh(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        return decoratedService.updateRobotsTextRefresh(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
    }
        
    @Override
    public Boolean updateRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateRobotsTextRefresh(robotsTextRefresh);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteRobotsTextRefresh(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteRobotsTextRefresh(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteRobotsTextRefresh(robotsTextRefresh);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteRobotsTextRefreshes(filter, params, values);
        return count;
    }

}
