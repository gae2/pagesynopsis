package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OgVideoDAO;
import com.pagesynopsis.ws.data.OgVideoDataObject;


// MockOgVideoDAO is a decorator.
// It can be used as a base class to mock OgVideoDAO objects.
public abstract class MockOgVideoDAO implements OgVideoDAO
{
    private static final Logger log = Logger.getLogger(MockOgVideoDAO.class.getName()); 

    // MockOgVideoDAO uses the decorator design pattern.
    private OgVideoDAO decoratedDAO;

    public MockOgVideoDAO(OgVideoDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected OgVideoDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(OgVideoDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public OgVideoDataObject getOgVideo(String guid) throws BaseException
    {
        return decoratedDAO.getOgVideo(guid);
	}

    @Override
    public List<OgVideoDataObject> getOgVideos(List<String> guids) throws BaseException
    {
        return decoratedDAO.getOgVideos(guids);
    }

    @Override
    public List<OgVideoDataObject> getAllOgVideos() throws BaseException
	{
	    return getAllOgVideos(null, null, null);
    }


    @Override
    public List<OgVideoDataObject> getAllOgVideos(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOgVideos(ordering, offset, count, null);
    }

    @Override
    public List<OgVideoDataObject> getAllOgVideos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllOgVideos(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgVideoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllOgVideoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<OgVideoDataObject> findOgVideos(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOgVideos(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OgVideoDataObject> findOgVideos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOgVideos(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OgVideoDataObject> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<OgVideoDataObject> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgVideo(OgVideoDataObject ogVideo) throws BaseException
    {
        return decoratedDAO.createOgVideo( ogVideo);
    }

    @Override
	public Boolean updateOgVideo(OgVideoDataObject ogVideo) throws BaseException
	{
        return decoratedDAO.updateOgVideo(ogVideo);
	}
	
    @Override
    public Boolean deleteOgVideo(OgVideoDataObject ogVideo) throws BaseException
    {
        return decoratedDAO.deleteOgVideo(ogVideo);
    }

    @Override
    public Boolean deleteOgVideo(String guid) throws BaseException
    {
        return decoratedDAO.deleteOgVideo(guid);
	}

    @Override
    public Long deleteOgVideos(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteOgVideos(filter, params, values);
    }

}
