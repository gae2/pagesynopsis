package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class UrlStructDataObject implements UrlStruct, Serializable
{
    private static final Logger log = Logger.getLogger(UrlStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _urlstruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private Integer statusCode;

    @Persistent(defaultFetchGroup = "true")
    private String redirectUrl;

    @Persistent(defaultFetchGroup = "true")
    private String absoluteUrl;

    @Persistent(defaultFetchGroup = "true")
    private String hashFragment;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public UrlStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null);
    }
    public UrlStructDataObject(String uuid, Integer statusCode, String redirectUrl, String absoluteUrl, String hashFragment, String note)
    {
        setUuid(uuid);
        setStatusCode(statusCode);
        setRedirectUrl(redirectUrl);
        setAbsoluteUrl(absoluteUrl);
        setHashFragment(hashFragment);
        setNote(note);
    }

    private void resetEncodedKey()
    {
        if(_urlstruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _urlstruct_encoded_key = KeyFactory.createKeyString(UrlStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _urlstruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _urlstruct_encoded_key = KeyFactory.createKeyString(UrlStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public Integer getStatusCode()
    {
        return this.statusCode;
    }
    public void setStatusCode(Integer statusCode)
    {
        this.statusCode = statusCode;
        if(this.statusCode != null) {
            resetEncodedKey();
        }
    }

    public String getRedirectUrl()
    {
        return this.redirectUrl;
    }
    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
        if(this.redirectUrl != null) {
            resetEncodedKey();
        }
    }

    public String getAbsoluteUrl()
    {
        return this.absoluteUrl;
    }
    public void setAbsoluteUrl(String absoluteUrl)
    {
        this.absoluteUrl = absoluteUrl;
        if(this.absoluteUrl != null) {
            resetEncodedKey();
        }
    }

    public String getHashFragment()
    {
        return this.hashFragment;
    }
    public void setHashFragment(String hashFragment)
    {
        this.hashFragment = hashFragment;
        if(this.hashFragment != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStatusCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRedirectUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAbsoluteUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHashFragment() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("statusCode", this.statusCode);
        dataMap.put("redirectUrl", this.redirectUrl);
        dataMap.put("absoluteUrl", this.absoluteUrl);
        dataMap.put("hashFragment", this.hashFragment);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        UrlStruct thatObj = (UrlStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.statusCode == null && thatObj.getStatusCode() != null)
            || (this.statusCode != null && thatObj.getStatusCode() == null)
            || !this.statusCode.equals(thatObj.getStatusCode()) ) {
            return false;
        }
        if( (this.redirectUrl == null && thatObj.getRedirectUrl() != null)
            || (this.redirectUrl != null && thatObj.getRedirectUrl() == null)
            || !this.redirectUrl.equals(thatObj.getRedirectUrl()) ) {
            return false;
        }
        if( (this.absoluteUrl == null && thatObj.getAbsoluteUrl() != null)
            || (this.absoluteUrl != null && thatObj.getAbsoluteUrl() == null)
            || !this.absoluteUrl.equals(thatObj.getAbsoluteUrl()) ) {
            return false;
        }
        if( (this.hashFragment == null && thatObj.getHashFragment() != null)
            || (this.hashFragment != null && thatObj.getHashFragment() == null)
            || !this.hashFragment.equals(thatObj.getHashFragment()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = statusCode == null ? 0 : statusCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirectUrl == null ? 0 : redirectUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = absoluteUrl == null ? 0 : absoluteUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = hashFragment == null ? 0 : hashFragment.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
