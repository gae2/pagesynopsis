package com.pagesynopsis.ws;



public interface OgQuantityStruct 
{
    String  getUuid();
    Float  getValue();
    String  getUnits();
    boolean isEmpty();
}
