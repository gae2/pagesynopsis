package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;


// TBD: Add offset/count to getAllOgWebsites() and findOgWebsites(), etc.
public interface OgWebsiteDAO
{
    OgWebsiteDataObject getOgWebsite(String guid) throws BaseException;
    List<OgWebsiteDataObject> getOgWebsites(List<String> guids) throws BaseException;
    List<OgWebsiteDataObject> getAllOgWebsites() throws BaseException;
    /* @Deprecated */ List<OgWebsiteDataObject> getAllOgWebsites(String ordering, Long offset, Integer count) throws BaseException;
    List<OgWebsiteDataObject> getAllOgWebsites(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgWebsiteDataObject> findOgWebsites(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgWebsiteDataObject> findOgWebsites(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<OgWebsiteDataObject> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgWebsiteDataObject> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createOgWebsite(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgWebsiteDataObject?)
    String createOgWebsite(OgWebsiteDataObject ogWebsite) throws BaseException;          // Returns Guid.  (Return OgWebsiteDataObject?)
    //Boolean updateOgWebsite(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgWebsite(OgWebsiteDataObject ogWebsite) throws BaseException;
    Boolean deleteOgWebsite(String guid) throws BaseException;
    Boolean deleteOgWebsite(OgWebsiteDataObject ogWebsite) throws BaseException;
    Long deleteOgWebsites(String filter, String params, List<String> values) throws BaseException;
}
