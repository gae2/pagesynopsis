package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.OgBlogDataObject;


// TBD: Add offset/count to getAllOgBlogs() and findOgBlogs(), etc.
public interface OgBlogDAO
{
    OgBlogDataObject getOgBlog(String guid) throws BaseException;
    List<OgBlogDataObject> getOgBlogs(List<String> guids) throws BaseException;
    List<OgBlogDataObject> getAllOgBlogs() throws BaseException;
    /* @Deprecated */ List<OgBlogDataObject> getAllOgBlogs(String ordering, Long offset, Integer count) throws BaseException;
    List<OgBlogDataObject> getAllOgBlogs(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgBlogDataObject> findOgBlogs(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgBlogDataObject> findOgBlogs(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<OgBlogDataObject> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgBlogDataObject> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createOgBlog(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgBlogDataObject?)
    String createOgBlog(OgBlogDataObject ogBlog) throws BaseException;          // Returns Guid.  (Return OgBlogDataObject?)
    //Boolean updateOgBlog(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgBlog(OgBlogDataObject ogBlog) throws BaseException;
    Boolean deleteOgBlog(String guid) throws BaseException;
    Boolean deleteOgBlog(OgBlogDataObject ogBlog) throws BaseException;
    Long deleteOgBlogs(String filter, String params, List<String> values) throws BaseException;
}
