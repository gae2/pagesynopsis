package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.TwitterCardMetaDAO;
import com.pagesynopsis.ws.data.TwitterCardMetaDataObject;


// MockTwitterCardMetaDAO is a decorator.
// It can be used as a base class to mock TwitterCardMetaDAO objects.
public abstract class MockTwitterCardMetaDAO implements TwitterCardMetaDAO
{
    private static final Logger log = Logger.getLogger(MockTwitterCardMetaDAO.class.getName()); 

    // MockTwitterCardMetaDAO uses the decorator design pattern.
    private TwitterCardMetaDAO decoratedDAO;

    public MockTwitterCardMetaDAO(TwitterCardMetaDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected TwitterCardMetaDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(TwitterCardMetaDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public TwitterCardMetaDataObject getTwitterCardMeta(String guid) throws BaseException
    {
        return decoratedDAO.getTwitterCardMeta(guid);
	}

    @Override
    public List<TwitterCardMetaDataObject> getTwitterCardMetas(List<String> guids) throws BaseException
    {
        return decoratedDAO.getTwitterCardMetas(guids);
    }

    @Override
    public List<TwitterCardMetaDataObject> getAllTwitterCardMetas() throws BaseException
	{
	    return getAllTwitterCardMetas(null, null, null);
    }


    @Override
    public List<TwitterCardMetaDataObject> getAllTwitterCardMetas(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllTwitterCardMetas(ordering, offset, count, null);
    }

    @Override
    public List<TwitterCardMetaDataObject> getAllTwitterCardMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllTwitterCardMetas(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterCardMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllTwitterCardMetaKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<TwitterCardMetaDataObject> findTwitterCardMetas(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTwitterCardMetas(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TwitterCardMetaDataObject> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTwitterCardMetas(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<TwitterCardMetaDataObject> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<TwitterCardMetaDataObject> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterCardMeta(TwitterCardMetaDataObject twitterCardMeta) throws BaseException
    {
        return decoratedDAO.createTwitterCardMeta( twitterCardMeta);
    }

    @Override
	public Boolean updateTwitterCardMeta(TwitterCardMetaDataObject twitterCardMeta) throws BaseException
	{
        return decoratedDAO.updateTwitterCardMeta(twitterCardMeta);
	}
	
    @Override
    public Boolean deleteTwitterCardMeta(TwitterCardMetaDataObject twitterCardMeta) throws BaseException
    {
        return decoratedDAO.deleteTwitterCardMeta(twitterCardMeta);
    }

    @Override
    public Boolean deleteTwitterCardMeta(String guid) throws BaseException
    {
        return decoratedDAO.deleteTwitterCardMeta(guid);
	}

    @Override
    public Long deleteTwitterCardMetas(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteTwitterCardMetas(filter, params, values);
    }

}
