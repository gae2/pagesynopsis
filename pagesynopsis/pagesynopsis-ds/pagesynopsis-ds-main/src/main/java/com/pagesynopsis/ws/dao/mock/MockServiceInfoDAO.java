package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.ServiceInfoDAO;
import com.pagesynopsis.ws.data.ServiceInfoDataObject;


// MockServiceInfoDAO is a decorator.
// It can be used as a base class to mock ServiceInfoDAO objects.
public abstract class MockServiceInfoDAO implements ServiceInfoDAO
{
    private static final Logger log = Logger.getLogger(MockServiceInfoDAO.class.getName()); 

    // MockServiceInfoDAO uses the decorator design pattern.
    private ServiceInfoDAO decoratedDAO;

    public MockServiceInfoDAO(ServiceInfoDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected ServiceInfoDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(ServiceInfoDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public ServiceInfoDataObject getServiceInfo(String guid) throws BaseException
    {
        return decoratedDAO.getServiceInfo(guid);
	}

    @Override
    public List<ServiceInfoDataObject> getServiceInfos(List<String> guids) throws BaseException
    {
        return decoratedDAO.getServiceInfos(guids);
    }

    @Override
    public List<ServiceInfoDataObject> getAllServiceInfos() throws BaseException
	{
	    return getAllServiceInfos(null, null, null);
    }


    @Override
    public List<ServiceInfoDataObject> getAllServiceInfos(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllServiceInfos(ordering, offset, count, null);
    }

    @Override
    public List<ServiceInfoDataObject> getAllServiceInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllServiceInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllServiceInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllServiceInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllServiceInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<ServiceInfoDataObject> findServiceInfos(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findServiceInfos(filter, ordering, params, values, null, null);
    }

    @Override
	public List<ServiceInfoDataObject> findServiceInfos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findServiceInfos(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<ServiceInfoDataObject> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<ServiceInfoDataObject> findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createServiceInfo(ServiceInfoDataObject serviceInfo) throws BaseException
    {
        return decoratedDAO.createServiceInfo( serviceInfo);
    }

    @Override
	public Boolean updateServiceInfo(ServiceInfoDataObject serviceInfo) throws BaseException
	{
        return decoratedDAO.updateServiceInfo(serviceInfo);
	}
	
    @Override
    public Boolean deleteServiceInfo(ServiceInfoDataObject serviceInfo) throws BaseException
    {
        return decoratedDAO.deleteServiceInfo(serviceInfo);
    }

    @Override
    public Boolean deleteServiceInfo(String guid) throws BaseException
    {
        return decoratedDAO.deleteServiceInfo(guid);
	}

    @Override
    public Long deleteServiceInfos(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteServiceInfos(filter, params, values);
    }

}
