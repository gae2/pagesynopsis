package com.pagesynopsis.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.bean.NotificationStructBean;
import com.pagesynopsis.ws.bean.KeyValuePairStructBean;
import com.pagesynopsis.ws.bean.ReferrerInfoStructBean;
import com.pagesynopsis.ws.bean.FetchRequestBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.NotificationStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.ReferrerInfoStructDataObject;
import com.pagesynopsis.ws.data.FetchRequestDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.FetchRequestService;


// FetchRequestMockService is a decorator.
// It can be used as a base class to mock FetchRequestService objects.
public abstract class FetchRequestMockService implements FetchRequestService
{
    private static final Logger log = Logger.getLogger(FetchRequestMockService.class.getName());

    // FetchRequestMockService uses the decorator design pattern.
    private FetchRequestService decoratedService;

    public FetchRequestMockService(FetchRequestService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected FetchRequestService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(FetchRequestService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // FetchRequest related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFetchRequest(): guid = " + guid);
        FetchRequest bean = decoratedService.getFetchRequest(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getFetchRequest(guid, field);
        return obj;
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        log.fine("getFetchRequests()");
        List<FetchRequest> fetchRequests = decoratedService.getFetchRequests(guids);
        log.finer("END");
        return fetchRequests;
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return getAllFetchRequests(null, null, null);
    }


    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequests(ordering, offset, count, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFetchRequests(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<FetchRequest> fetchRequests = decoratedService.getAllFetchRequests(ordering, offset, count, forwardCursor);
        log.finer("END");
        return fetchRequests;
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFetchRequestKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FetchRequestMockService.findFetchRequests(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<FetchRequest> fetchRequests = decoratedService.findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return fetchRequests;
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FetchRequestMockService.findFetchRequestKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FetchRequestMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createFetchRequest(String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        return decoratedService.createFetchRequest(user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfo, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPref);
    }

    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createFetchRequest(fetchRequest);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateFetchRequest(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        return decoratedService.updateFetchRequest(guid, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfo, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPref);
    }
        
    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateFetchRequest(fetchRequest);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteFetchRequest(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteFetchRequest(fetchRequest);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteFetchRequests(filter, params, values);
        return count;
    }

}
