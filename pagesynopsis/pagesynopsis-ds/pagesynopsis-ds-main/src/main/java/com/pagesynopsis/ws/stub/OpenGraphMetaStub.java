package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "openGraphMeta")
@XmlType(propOrder = {"guid", "user", "fetchRequest", "targetUrl", "pageUrl", "queryString", "queryParamsStub", "lastFetchResult", "responseCode", "contentType", "contentLength", "language", "redirect", "location", "pageTitle", "note", "deferred", "status", "refreshStatus", "refreshInterval", "nextRefreshTime", "lastCheckedTime", "lastUpdatedTime", "ogType", "ogProfileStub", "ogWebsiteStub", "ogBlogStub", "ogArticleStub", "ogBookStub", "ogVideoStub", "ogMovieStub", "ogTvShowStub", "ogTvEpisodeStub", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenGraphMetaStub extends PageBaseStub implements OpenGraphMeta, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OpenGraphMetaStub.class.getName());

    private String ogType;
    private OgProfileStub ogProfile;
    private OgWebsiteStub ogWebsite;
    private OgBlogStub ogBlog;
    private OgArticleStub ogArticle;
    private OgBookStub ogBook;
    private OgVideoStub ogVideo;
    private OgMovieStub ogMovie;
    private OgTvShowStub ogTvShow;
    private OgTvEpisodeStub ogTvEpisode;

    public OpenGraphMetaStub()
    {
        this(null);
    }
    public OpenGraphMetaStub(OpenGraphMeta bean)
    {
        super(bean);
        if(bean != null) {
            this.ogType = bean.getOgType();
            this.ogProfile = OgProfileStub.convertBeanToStub(bean.getOgProfile());
            this.ogWebsite = OgWebsiteStub.convertBeanToStub(bean.getOgWebsite());
            this.ogBlog = OgBlogStub.convertBeanToStub(bean.getOgBlog());
            this.ogArticle = OgArticleStub.convertBeanToStub(bean.getOgArticle());
            this.ogBook = OgBookStub.convertBeanToStub(bean.getOgBook());
            this.ogVideo = OgVideoStub.convertBeanToStub(bean.getOgVideo());
            this.ogMovie = OgMovieStub.convertBeanToStub(bean.getOgMovie());
            this.ogTvShow = OgTvShowStub.convertBeanToStub(bean.getOgTvShow());
            this.ogTvEpisode = OgTvEpisodeStub.convertBeanToStub(bean.getOgTvEpisode());
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    @XmlElement
    public String getFetchRequest()
    {
        return super.getFetchRequest();
    }
    public void setFetchRequest(String fetchRequest)
    {
        super.setFetchRequest(fetchRequest);
    }

    @XmlElement
    public String getTargetUrl()
    {
        return super.getTargetUrl();
    }
    public void setTargetUrl(String targetUrl)
    {
        super.setTargetUrl(targetUrl);
    }

    @XmlElement
    public String getPageUrl()
    {
        return super.getPageUrl();
    }
    public void setPageUrl(String pageUrl)
    {
        super.setPageUrl(pageUrl);
    }

    @XmlElement
    public String getQueryString()
    {
        return super.getQueryString();
    }
    public void setQueryString(String queryString)
    {
        super.setQueryString(queryString);
    }

    @XmlElement(name = "queryParams")
    @JsonIgnore
    public List<KeyValuePairStructStub> getQueryParamsStub()
    {
        return super.getQueryParamsStub();
    }
    public void setQueryParamsStub(List<KeyValuePairStructStub> queryParams)
    {
        super.setQueryParamsStub(queryParams);
    }
    @XmlTransient
    @JsonDeserialize(contentAs=KeyValuePairStructStub.class)
    public List<KeyValuePairStruct> getQueryParams()
    {  
        return super.getQueryParams();
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        super.setQueryParams(queryParams);
    }

    @XmlElement
    public String getLastFetchResult()
    {
        return super.getLastFetchResult();
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        super.setLastFetchResult(lastFetchResult);
    }

    @XmlElement
    public Integer getResponseCode()
    {
        return super.getResponseCode();
    }
    public void setResponseCode(Integer responseCode)
    {
        super.setResponseCode(responseCode);
    }

    @XmlElement
    public String getContentType()
    {
        return super.getContentType();
    }
    public void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }

    @XmlElement
    public Integer getContentLength()
    {
        return super.getContentLength();
    }
    public void setContentLength(Integer contentLength)
    {
        super.setContentLength(contentLength);
    }

    @XmlElement
    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    @XmlElement
    public String getRedirect()
    {
        return super.getRedirect();
    }
    public void setRedirect(String redirect)
    {
        super.setRedirect(redirect);
    }

    @XmlElement
    public String getLocation()
    {
        return super.getLocation();
    }
    public void setLocation(String location)
    {
        super.setLocation(location);
    }

    @XmlElement
    public String getPageTitle()
    {
        return super.getPageTitle();
    }
    public void setPageTitle(String pageTitle)
    {
        super.setPageTitle(pageTitle);
    }

    @XmlElement
    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    @XmlElement
    public Boolean isDeferred()
    {
        return super.isDeferred();
    }
    public void setDeferred(Boolean deferred)
    {
        super.setDeferred(deferred);
    }

    @XmlElement
    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    @XmlElement
    public Integer getRefreshStatus()
    {
        return super.getRefreshStatus();
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        super.setRefreshStatus(refreshStatus);
    }

    @XmlElement
    public Long getRefreshInterval()
    {
        return super.getRefreshInterval();
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        super.setRefreshInterval(refreshInterval);
    }

    @XmlElement
    public Long getNextRefreshTime()
    {
        return super.getNextRefreshTime();
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        super.setNextRefreshTime(nextRefreshTime);
    }

    @XmlElement
    public Long getLastCheckedTime()
    {
        return super.getLastCheckedTime();
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        super.setLastCheckedTime(lastCheckedTime);
    }

    @XmlElement
    public Long getLastUpdatedTime()
    {
        return super.getLastUpdatedTime();
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        super.setLastUpdatedTime(lastUpdatedTime);
    }

    @XmlElement
    public String getOgType()
    {
        return this.ogType;
    }
    public void setOgType(String ogType)
    {
        this.ogType = ogType;
    }

    @XmlElement(name = "ogProfile")
    @JsonIgnore
    public OgProfileStub getOgProfileStub()
    {
        return this.ogProfile;
    }
    public void setOgProfileStub(OgProfileStub ogProfile)
    {
        this.ogProfile = ogProfile;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=OgProfileStub.class)
    public OgProfile getOgProfile()
    {  
        return getOgProfileStub();
    }
    public void setOgProfile(OgProfile ogProfile)
    {
        if((ogProfile == null) || (ogProfile instanceof OgProfileStub)) {
            setOgProfileStub((OgProfileStub) ogProfile);
        } else {
            // TBD
            setOgProfileStub(OgProfileStub.convertBeanToStub(ogProfile));
        }
    }

    @XmlElement(name = "ogWebsite")
    @JsonIgnore
    public OgWebsiteStub getOgWebsiteStub()
    {
        return this.ogWebsite;
    }
    public void setOgWebsiteStub(OgWebsiteStub ogWebsite)
    {
        this.ogWebsite = ogWebsite;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=OgWebsiteStub.class)
    public OgWebsite getOgWebsite()
    {  
        return getOgWebsiteStub();
    }
    public void setOgWebsite(OgWebsite ogWebsite)
    {
        if((ogWebsite == null) || (ogWebsite instanceof OgWebsiteStub)) {
            setOgWebsiteStub((OgWebsiteStub) ogWebsite);
        } else {
            // TBD
            setOgWebsiteStub(OgWebsiteStub.convertBeanToStub(ogWebsite));
        }
    }

    @XmlElement(name = "ogBlog")
    @JsonIgnore
    public OgBlogStub getOgBlogStub()
    {
        return this.ogBlog;
    }
    public void setOgBlogStub(OgBlogStub ogBlog)
    {
        this.ogBlog = ogBlog;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=OgBlogStub.class)
    public OgBlog getOgBlog()
    {  
        return getOgBlogStub();
    }
    public void setOgBlog(OgBlog ogBlog)
    {
        if((ogBlog == null) || (ogBlog instanceof OgBlogStub)) {
            setOgBlogStub((OgBlogStub) ogBlog);
        } else {
            // TBD
            setOgBlogStub(OgBlogStub.convertBeanToStub(ogBlog));
        }
    }

    @XmlElement(name = "ogArticle")
    @JsonIgnore
    public OgArticleStub getOgArticleStub()
    {
        return this.ogArticle;
    }
    public void setOgArticleStub(OgArticleStub ogArticle)
    {
        this.ogArticle = ogArticle;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=OgArticleStub.class)
    public OgArticle getOgArticle()
    {  
        return getOgArticleStub();
    }
    public void setOgArticle(OgArticle ogArticle)
    {
        if((ogArticle == null) || (ogArticle instanceof OgArticleStub)) {
            setOgArticleStub((OgArticleStub) ogArticle);
        } else {
            // TBD
            setOgArticleStub(OgArticleStub.convertBeanToStub(ogArticle));
        }
    }

    @XmlElement(name = "ogBook")
    @JsonIgnore
    public OgBookStub getOgBookStub()
    {
        return this.ogBook;
    }
    public void setOgBookStub(OgBookStub ogBook)
    {
        this.ogBook = ogBook;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=OgBookStub.class)
    public OgBook getOgBook()
    {  
        return getOgBookStub();
    }
    public void setOgBook(OgBook ogBook)
    {
        if((ogBook == null) || (ogBook instanceof OgBookStub)) {
            setOgBookStub((OgBookStub) ogBook);
        } else {
            // TBD
            setOgBookStub(OgBookStub.convertBeanToStub(ogBook));
        }
    }

    @XmlElement(name = "ogVideo")
    @JsonIgnore
    public OgVideoStub getOgVideoStub()
    {
        return this.ogVideo;
    }
    public void setOgVideoStub(OgVideoStub ogVideo)
    {
        this.ogVideo = ogVideo;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=OgVideoStub.class)
    public OgVideo getOgVideo()
    {  
        return getOgVideoStub();
    }
    public void setOgVideo(OgVideo ogVideo)
    {
        if((ogVideo == null) || (ogVideo instanceof OgVideoStub)) {
            setOgVideoStub((OgVideoStub) ogVideo);
        } else {
            // TBD
            setOgVideoStub(OgVideoStub.convertBeanToStub(ogVideo));
        }
    }

    @XmlElement(name = "ogMovie")
    @JsonIgnore
    public OgMovieStub getOgMovieStub()
    {
        return this.ogMovie;
    }
    public void setOgMovieStub(OgMovieStub ogMovie)
    {
        this.ogMovie = ogMovie;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=OgMovieStub.class)
    public OgMovie getOgMovie()
    {  
        return getOgMovieStub();
    }
    public void setOgMovie(OgMovie ogMovie)
    {
        if((ogMovie == null) || (ogMovie instanceof OgMovieStub)) {
            setOgMovieStub((OgMovieStub) ogMovie);
        } else {
            // TBD
            setOgMovieStub(OgMovieStub.convertBeanToStub(ogMovie));
        }
    }

    @XmlElement(name = "ogTvShow")
    @JsonIgnore
    public OgTvShowStub getOgTvShowStub()
    {
        return this.ogTvShow;
    }
    public void setOgTvShowStub(OgTvShowStub ogTvShow)
    {
        this.ogTvShow = ogTvShow;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=OgTvShowStub.class)
    public OgTvShow getOgTvShow()
    {  
        return getOgTvShowStub();
    }
    public void setOgTvShow(OgTvShow ogTvShow)
    {
        if((ogTvShow == null) || (ogTvShow instanceof OgTvShowStub)) {
            setOgTvShowStub((OgTvShowStub) ogTvShow);
        } else {
            // TBD
            setOgTvShowStub(OgTvShowStub.convertBeanToStub(ogTvShow));
        }
    }

    @XmlElement(name = "ogTvEpisode")
    @JsonIgnore
    public OgTvEpisodeStub getOgTvEpisodeStub()
    {
        return this.ogTvEpisode;
    }
    public void setOgTvEpisodeStub(OgTvEpisodeStub ogTvEpisode)
    {
        this.ogTvEpisode = ogTvEpisode;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=OgTvEpisodeStub.class)
    public OgTvEpisode getOgTvEpisode()
    {  
        return getOgTvEpisodeStub();
    }
    public void setOgTvEpisode(OgTvEpisode ogTvEpisode)
    {
        if((ogTvEpisode == null) || (ogTvEpisode instanceof OgTvEpisodeStub)) {
            setOgTvEpisodeStub((OgTvEpisodeStub) ogTvEpisode);
        } else {
            // TBD
            setOgTvEpisodeStub(OgTvEpisodeStub.convertBeanToStub(ogTvEpisode));
        }
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("ogType", this.ogType);
        dataMap.put("ogProfile", this.ogProfile);
        dataMap.put("ogWebsite", this.ogWebsite);
        dataMap.put("ogBlog", this.ogBlog);
        dataMap.put("ogArticle", this.ogArticle);
        dataMap.put("ogBook", this.ogBook);
        dataMap.put("ogVideo", this.ogVideo);
        dataMap.put("ogMovie", this.ogMovie);
        dataMap.put("ogTvShow", this.ogTvShow);
        dataMap.put("ogTvEpisode", this.ogTvEpisode);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = ogType == null ? 0 : ogType.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogProfile == null ? 0 : ogProfile.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogWebsite == null ? 0 : ogWebsite.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogBlog == null ? 0 : ogBlog.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogArticle == null ? 0 : ogArticle.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogBook == null ? 0 : ogBook.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogVideo == null ? 0 : ogVideo.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogMovie == null ? 0 : ogMovie.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogTvShow == null ? 0 : ogTvShow.hashCode();
        _hash = 31 * _hash + delta;
        delta = ogTvEpisode == null ? 0 : ogTvEpisode.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static OpenGraphMetaStub convertBeanToStub(OpenGraphMeta bean)
    {
        OpenGraphMetaStub stub = null;
        if(bean instanceof OpenGraphMetaStub) {
            stub = (OpenGraphMetaStub) bean;
        } else {
            if(bean != null) {
                stub = new OpenGraphMetaStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OpenGraphMetaStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of OpenGraphMetaStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OpenGraphMetaStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OpenGraphMetaStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OpenGraphMetaStub object as a string.", e);
        }
        
        return null;
    }
    public static OpenGraphMetaStub fromJsonString(String jsonStr)
    {
        try {
            OpenGraphMetaStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OpenGraphMetaStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OpenGraphMetaStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OpenGraphMetaStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OpenGraphMetaStub object.", e);
        }
        
        return null;
    }

}
