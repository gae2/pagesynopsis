package com.pagesynopsis.ws;



public interface AudioStruct 
{
    String  getUuid();
    String  getId();
    String  getControls();
    Boolean  isAutoplayEnabled();
    Boolean  isLoopEnabled();
    Boolean  isPreloadEnabled();
    String  getRemark();
    MediaSourceStruct  getSource();
    MediaSourceStruct  getSource1();
    MediaSourceStruct  getSource2();
    MediaSourceStruct  getSource3();
    MediaSourceStruct  getSource4();
    MediaSourceStruct  getSource5();
    String  getNote();
    boolean isEmpty();
}
