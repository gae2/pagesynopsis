package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;


public interface PageFetch extends PageBase
{
    Integer  getInputMaxRedirects();
    Integer  getResultRedirectCount();
    List<UrlStruct>  getRedirectPages();
    String  getDestinationUrl();
    String  getPageAuthor();
    String  getPageSummary();
    String  getFavicon();
    String  getFaviconUrl();
}
