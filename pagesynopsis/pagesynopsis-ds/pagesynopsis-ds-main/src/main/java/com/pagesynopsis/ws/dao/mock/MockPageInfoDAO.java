package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.PageInfoDAO;
import com.pagesynopsis.ws.data.PageInfoDataObject;


// MockPageInfoDAO is a decorator.
// It can be used as a base class to mock PageInfoDAO objects.
public abstract class MockPageInfoDAO implements PageInfoDAO
{
    private static final Logger log = Logger.getLogger(MockPageInfoDAO.class.getName()); 

    // MockPageInfoDAO uses the decorator design pattern.
    private PageInfoDAO decoratedDAO;

    public MockPageInfoDAO(PageInfoDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected PageInfoDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(PageInfoDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public PageInfoDataObject getPageInfo(String guid) throws BaseException
    {
        return decoratedDAO.getPageInfo(guid);
	}

    @Override
    public List<PageInfoDataObject> getPageInfos(List<String> guids) throws BaseException
    {
        return decoratedDAO.getPageInfos(guids);
    }

    @Override
    public List<PageInfoDataObject> getAllPageInfos() throws BaseException
	{
	    return getAllPageInfos(null, null, null);
    }


    @Override
    public List<PageInfoDataObject> getAllPageInfos(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllPageInfos(ordering, offset, count, null);
    }

    @Override
    public List<PageInfoDataObject> getAllPageInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllPageInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllPageInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<PageInfoDataObject> findPageInfos(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findPageInfos(filter, ordering, params, values, null, null);
    }

    @Override
	public List<PageInfoDataObject> findPageInfos(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findPageInfos(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<PageInfoDataObject> findPageInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findPageInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<PageInfoDataObject> findPageInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findPageInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createPageInfo(PageInfoDataObject pageInfo) throws BaseException
    {
        return decoratedDAO.createPageInfo( pageInfo);
    }

    @Override
	public Boolean updatePageInfo(PageInfoDataObject pageInfo) throws BaseException
	{
        return decoratedDAO.updatePageInfo(pageInfo);
	}
	
    @Override
    public Boolean deletePageInfo(PageInfoDataObject pageInfo) throws BaseException
    {
        return decoratedDAO.deletePageInfo(pageInfo);
    }

    @Override
    public Boolean deletePageInfo(String guid) throws BaseException
    {
        return decoratedDAO.deletePageInfo(guid);
	}

    @Override
    public Long deletePageInfos(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deletePageInfos(filter, params, values);
    }

}
