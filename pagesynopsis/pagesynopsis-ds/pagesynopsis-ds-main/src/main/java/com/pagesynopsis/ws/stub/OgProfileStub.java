package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogProfile")
@XmlType(propOrder = {"guid", "url", "type", "siteName", "title", "description", "fbAdmins", "fbAppId", "imageStub", "audioStub", "videoStub", "locale", "localeAlternate", "profileId", "firstName", "lastName", "username", "gender", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgProfileStub extends OgObjectBaseStub implements OgProfile, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgProfileStub.class.getName());

    private String profileId;
    private String firstName;
    private String lastName;
    private String username;
    private String gender;

    public OgProfileStub()
    {
        this(null);
    }
    public OgProfileStub(OgProfile bean)
    {
        super(bean);
        if(bean != null) {
            this.profileId = bean.getProfileId();
            this.firstName = bean.getFirstName();
            this.lastName = bean.getLastName();
            this.username = bean.getUsername();
            this.gender = bean.getGender();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    @XmlElement
    public String getType()
    {
        return super.getType();
    }
    public void setType(String type)
    {
        super.setType(type);
    }

    @XmlElement
    public String getSiteName()
    {
        return super.getSiteName();
    }
    public void setSiteName(String siteName)
    {
        super.setSiteName(siteName);
    }

    @XmlElement
    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    @XmlElement
    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    @XmlElement
    public List<String> getFbAdmins()
    {
        return super.getFbAdmins();
    }
    public void setFbAdmins(List<String> fbAdmins)
    {
        super.setFbAdmins(fbAdmins);
    }

    @XmlElement
    public List<String> getFbAppId()
    {
        return super.getFbAppId();
    }
    public void setFbAppId(List<String> fbAppId)
    {
        super.setFbAppId(fbAppId);
    }

    @XmlElement(name = "image")
    @JsonIgnore
    public List<OgImageStructStub> getImageStub()
    {
        return super.getImageStub();
    }
    public void setImageStub(List<OgImageStructStub> image)
    {
        super.setImageStub(image);
    }
    @XmlTransient
    @JsonDeserialize(contentAs=OgImageStructStub.class)
    public List<OgImageStruct> getImage()
    {  
        return super.getImage();
    }
    public void setImage(List<OgImageStruct> image)
    {
        super.setImage(image);
    }

    @XmlElement(name = "audio")
    @JsonIgnore
    public List<OgAudioStructStub> getAudioStub()
    {
        return super.getAudioStub();
    }
    public void setAudioStub(List<OgAudioStructStub> audio)
    {
        super.setAudioStub(audio);
    }
    @XmlTransient
    @JsonDeserialize(contentAs=OgAudioStructStub.class)
    public List<OgAudioStruct> getAudio()
    {  
        return super.getAudio();
    }
    public void setAudio(List<OgAudioStruct> audio)
    {
        super.setAudio(audio);
    }

    @XmlElement(name = "video")
    @JsonIgnore
    public List<OgVideoStructStub> getVideoStub()
    {
        return super.getVideoStub();
    }
    public void setVideoStub(List<OgVideoStructStub> video)
    {
        super.setVideoStub(video);
    }
    @XmlTransient
    @JsonDeserialize(contentAs=OgVideoStructStub.class)
    public List<OgVideoStruct> getVideo()
    {  
        return super.getVideo();
    }
    public void setVideo(List<OgVideoStruct> video)
    {
        super.setVideo(video);
    }

    @XmlElement
    public String getLocale()
    {
        return super.getLocale();
    }
    public void setLocale(String locale)
    {
        super.setLocale(locale);
    }

    @XmlElement
    public List<String> getLocaleAlternate()
    {
        return super.getLocaleAlternate();
    }
    public void setLocaleAlternate(List<String> localeAlternate)
    {
        super.setLocaleAlternate(localeAlternate);
    }

    @XmlElement
    public String getProfileId()
    {
        return this.profileId;
    }
    public void setProfileId(String profileId)
    {
        this.profileId = profileId;
    }

    @XmlElement
    public String getFirstName()
    {
        return this.firstName;
    }
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    @XmlElement
    public String getLastName()
    {
        return this.lastName;
    }
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    @XmlElement
    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    @XmlElement
    public String getGender()
    {
        return this.gender;
    }
    public void setGender(String gender)
    {
        this.gender = gender;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("profileId", this.profileId);
        dataMap.put("firstName", this.firstName);
        dataMap.put("lastName", this.lastName);
        dataMap.put("username", this.username);
        dataMap.put("gender", this.gender);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = profileId == null ? 0 : profileId.hashCode();
        _hash = 31 * _hash + delta;
        delta = firstName == null ? 0 : firstName.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastName == null ? 0 : lastName.hashCode();
        _hash = 31 * _hash + delta;
        delta = username == null ? 0 : username.hashCode();
        _hash = 31 * _hash + delta;
        delta = gender == null ? 0 : gender.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static OgProfileStub convertBeanToStub(OgProfile bean)
    {
        OgProfileStub stub = null;
        if(bean instanceof OgProfileStub) {
            stub = (OgProfileStub) bean;
        } else {
            if(bean != null) {
                stub = new OgProfileStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgProfileStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of OgProfileStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgProfileStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgProfileStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgProfileStub object as a string.", e);
        }
        
        return null;
    }
    public static OgProfileStub fromJsonString(String jsonStr)
    {
        try {
            OgProfileStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgProfileStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgProfileStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgProfileStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgProfileStub object.", e);
        }
        
        return null;
    }

}
