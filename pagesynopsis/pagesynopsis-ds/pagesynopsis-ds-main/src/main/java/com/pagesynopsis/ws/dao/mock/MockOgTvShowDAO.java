package com.pagesynopsis.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;
import java.util.ArrayList;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.config.Config;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.dao.OgTvShowDAO;
import com.pagesynopsis.ws.data.OgTvShowDataObject;


// MockOgTvShowDAO is a decorator.
// It can be used as a base class to mock OgTvShowDAO objects.
public abstract class MockOgTvShowDAO implements OgTvShowDAO
{
    private static final Logger log = Logger.getLogger(MockOgTvShowDAO.class.getName()); 

    // MockOgTvShowDAO uses the decorator design pattern.
    private OgTvShowDAO decoratedDAO;

    public MockOgTvShowDAO(OgTvShowDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected OgTvShowDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(OgTvShowDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public OgTvShowDataObject getOgTvShow(String guid) throws BaseException
    {
        return decoratedDAO.getOgTvShow(guid);
	}

    @Override
    public List<OgTvShowDataObject> getOgTvShows(List<String> guids) throws BaseException
    {
        return decoratedDAO.getOgTvShows(guids);
    }

    @Override
    public List<OgTvShowDataObject> getAllOgTvShows() throws BaseException
	{
	    return getAllOgTvShows(null, null, null);
    }


    @Override
    public List<OgTvShowDataObject> getAllOgTvShows(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllOgTvShows(ordering, offset, count, null);
    }

    @Override
    public List<OgTvShowDataObject> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllOgTvShows(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShowKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllOgTvShowKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<OgTvShowDataObject> findOgTvShows(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findOgTvShows(filter, ordering, params, values, null, null);
    }

    @Override
	public List<OgTvShowDataObject> findOgTvShows(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findOgTvShows(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<OgTvShowDataObject> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<OgTvShowDataObject> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgTvShow(OgTvShowDataObject ogTvShow) throws BaseException
    {
        return decoratedDAO.createOgTvShow( ogTvShow);
    }

    @Override
	public Boolean updateOgTvShow(OgTvShowDataObject ogTvShow) throws BaseException
	{
        return decoratedDAO.updateOgTvShow(ogTvShow);
	}
	
    @Override
    public Boolean deleteOgTvShow(OgTvShowDataObject ogTvShow) throws BaseException
    {
        return decoratedDAO.deleteOgTvShow(ogTvShow);
    }

    @Override
    public Boolean deleteOgTvShow(String guid) throws BaseException
    {
        return decoratedDAO.deleteOgTvShow(guid);
	}

    @Override
    public Long deleteOgTvShows(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteOgTvShows(filter, params, values);
    }

}
