package com.pagesynopsis.ws;



public interface OgGeoPointStruct 
{
    String  getUuid();
    Float  getLatitude();
    Float  getLongitude();
    Float  getAltitude();
    boolean isEmpty();
}
