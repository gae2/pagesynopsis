# Page Synopsis

Cloud-based Web page information extraction Web services.
It is a simple Web content scraping app,
and it can be used to get a title/description, and other metadata, of any public Web page/URL.


The project consists of three Web services and their client SDKs.

* `pagesynopsis-ds:` Data storage/access web services and its client SDKs.
* `pagesynopsis-fe:` Web app/api and its client SDKs. Includes the "business logic".
* `pagesynopsis-wc:` Web-based client app for `pagesynopsis-fe`.




_Note: This was mostly developed ca 2010 as part of the "microservices framework" on GAE._
