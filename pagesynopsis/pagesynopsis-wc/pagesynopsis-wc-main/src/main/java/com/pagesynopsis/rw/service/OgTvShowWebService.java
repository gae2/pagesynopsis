package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgTvShowWebService // implements OgTvShowService
{
    private static final Logger log = Logger.getLogger(OgTvShowWebService.class.getName());
     
    // Af service interface.
    private OgTvShowService mService = null;

    public OgTvShowWebService()
    {
        this(ServiceProxyFactory.getInstance().getOgTvShowServiceProxy());
    }
    public OgTvShowWebService(OgTvShowService service)
    {
        mService = service;
    }
    
    protected OgTvShowService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getOgTvShowServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(OgTvShowService service)
    {
        mService = service;
    }
    
    
    public OgTvShowJsBean getOgTvShow(String guid) throws WebException
    {
        try {
            OgTvShow ogTvShow = getServiceProxy().getOgTvShow(guid);
            OgTvShowJsBean bean = convertOgTvShowToJsBean(ogTvShow);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOgTvShow(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getOgTvShow(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgTvShowJsBean> getOgTvShows(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgTvShowJsBean> jsBeans = new ArrayList<OgTvShowJsBean>();
            List<OgTvShow> ogTvShows = getServiceProxy().getOgTvShows(guids);
            if(ogTvShows != null) {
                for(OgTvShow ogTvShow : ogTvShows) {
                    jsBeans.add(convertOgTvShowToJsBean(ogTvShow));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgTvShowJsBean> getAllOgTvShows() throws WebException
    {
        return getAllOgTvShows(null, null, null);
    }

    // @Deprecated
    public List<OgTvShowJsBean> getAllOgTvShows(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgTvShows(ordering, offset, count, null);
    }

    public List<OgTvShowJsBean> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgTvShowJsBean> jsBeans = new ArrayList<OgTvShowJsBean>();
            List<OgTvShow> ogTvShows = getServiceProxy().getAllOgTvShows(ordering, offset, count, forwardCursor);
            if(ogTvShows != null) {
                for(OgTvShow ogTvShow : ogTvShows) {
                    jsBeans.add(convertOgTvShowToJsBean(ogTvShow));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgTvShowKeys(ordering, offset, count, null);
    }

    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllOgTvShowKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OgTvShowJsBean> findOgTvShows(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOgTvShows(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OgTvShowJsBean> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OgTvShowJsBean> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgTvShowJsBean> jsBeans = new ArrayList<OgTvShowJsBean>();
            List<OgTvShow> ogTvShows = getServiceProxy().findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(ogTvShows != null) {
                for(OgTvShow ogTvShow : ogTvShows) {
                    jsBeans.add(convertOgTvShowToJsBean(ogTvShow));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgTvShow(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getServiceProxy().createOgTvShow(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgTvShow(OgTvShowJsBean jsBean) throws WebException
    {
        try {
            OgTvShow ogTvShow = convertOgTvShowJsBeanToBean(jsBean);
            return getServiceProxy().createOgTvShow(ogTvShow);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgTvShowJsBean constructOgTvShow(OgTvShowJsBean jsBean) throws WebException
    {
        try {
            OgTvShow ogTvShow = convertOgTvShowJsBeanToBean(jsBean);
            ogTvShow = getServiceProxy().constructOgTvShow(ogTvShow);
            jsBean = convertOgTvShowToJsBean(ogTvShow);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOgTvShow(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getServiceProxy().updateOgTvShow(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOgTvShow(OgTvShowJsBean jsBean) throws WebException
    {
        try {
            OgTvShow ogTvShow = convertOgTvShowJsBeanToBean(jsBean);
            return getServiceProxy().updateOgTvShow(ogTvShow);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgTvShowJsBean refreshOgTvShow(OgTvShowJsBean jsBean) throws WebException
    {
        try {
            OgTvShow ogTvShow = convertOgTvShowJsBeanToBean(jsBean);
            ogTvShow = getServiceProxy().refreshOgTvShow(ogTvShow);
            jsBean = convertOgTvShowToJsBean(ogTvShow);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgTvShow(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteOgTvShow(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgTvShow(OgTvShowJsBean jsBean) throws WebException
    {
        try {
            OgTvShow ogTvShow = convertOgTvShowJsBeanToBean(jsBean);
            return getServiceProxy().deleteOgTvShow(ogTvShow);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOgTvShows(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteOgTvShows(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static OgTvShowJsBean convertOgTvShowToJsBean(OgTvShow ogTvShow)
    {
        OgTvShowJsBean jsBean = null;
        if(ogTvShow != null) {
            jsBean = new OgTvShowJsBean();
            jsBean.setGuid(ogTvShow.getGuid());
            jsBean.setUrl(ogTvShow.getUrl());
            jsBean.setType(ogTvShow.getType());
            jsBean.setSiteName(ogTvShow.getSiteName());
            jsBean.setTitle(ogTvShow.getTitle());
            jsBean.setDescription(ogTvShow.getDescription());
            jsBean.setFbAdmins(ogTvShow.getFbAdmins());
            jsBean.setFbAppId(ogTvShow.getFbAppId());
            List<OgImageStructJsBean> imageJsBeans = new ArrayList<OgImageStructJsBean>();
            List<OgImageStruct> imageBeans = ogTvShow.getImage();
            if(imageBeans != null) {
                for(OgImageStruct ogImageStruct : imageBeans) {
                    OgImageStructJsBean jB = OgImageStructWebService.convertOgImageStructToJsBean(ogImageStruct);
                    imageJsBeans.add(jB); 
                }
            }
            jsBean.setImage(imageJsBeans);
            List<OgAudioStructJsBean> audioJsBeans = new ArrayList<OgAudioStructJsBean>();
            List<OgAudioStruct> audioBeans = ogTvShow.getAudio();
            if(audioBeans != null) {
                for(OgAudioStruct ogAudioStruct : audioBeans) {
                    OgAudioStructJsBean jB = OgAudioStructWebService.convertOgAudioStructToJsBean(ogAudioStruct);
                    audioJsBeans.add(jB); 
                }
            }
            jsBean.setAudio(audioJsBeans);
            List<OgVideoStructJsBean> videoJsBeans = new ArrayList<OgVideoStructJsBean>();
            List<OgVideoStruct> videoBeans = ogTvShow.getVideo();
            if(videoBeans != null) {
                for(OgVideoStruct ogVideoStruct : videoBeans) {
                    OgVideoStructJsBean jB = OgVideoStructWebService.convertOgVideoStructToJsBean(ogVideoStruct);
                    videoJsBeans.add(jB); 
                }
            }
            jsBean.setVideo(videoJsBeans);
            jsBean.setLocale(ogTvShow.getLocale());
            jsBean.setLocaleAlternate(ogTvShow.getLocaleAlternate());
            jsBean.setDirector(ogTvShow.getDirector());
            jsBean.setWriter(ogTvShow.getWriter());
            List<OgActorStructJsBean> actorJsBeans = new ArrayList<OgActorStructJsBean>();
            List<OgActorStruct> actorBeans = ogTvShow.getActor();
            if(actorBeans != null) {
                for(OgActorStruct ogActorStruct : actorBeans) {
                    OgActorStructJsBean jB = OgActorStructWebService.convertOgActorStructToJsBean(ogActorStruct);
                    actorJsBeans.add(jB); 
                }
            }
            jsBean.setActor(actorJsBeans);
            jsBean.setDuration(ogTvShow.getDuration());
            jsBean.setTag(ogTvShow.getTag());
            jsBean.setReleaseDate(ogTvShow.getReleaseDate());
            jsBean.setCreatedTime(ogTvShow.getCreatedTime());
            jsBean.setModifiedTime(ogTvShow.getModifiedTime());
        }
        return jsBean;
    }

    public static OgTvShow convertOgTvShowJsBeanToBean(OgTvShowJsBean jsBean)
    {
        OgTvShowBean ogTvShow = null;
        if(jsBean != null) {
            ogTvShow = new OgTvShowBean();
            ogTvShow.setGuid(jsBean.getGuid());
            ogTvShow.setUrl(jsBean.getUrl());
            ogTvShow.setType(jsBean.getType());
            ogTvShow.setSiteName(jsBean.getSiteName());
            ogTvShow.setTitle(jsBean.getTitle());
            ogTvShow.setDescription(jsBean.getDescription());
            ogTvShow.setFbAdmins(jsBean.getFbAdmins());
            ogTvShow.setFbAppId(jsBean.getFbAppId());
            List<OgImageStruct> imageBeans = new ArrayList<OgImageStruct>();
            List<OgImageStructJsBean> imageJsBeans = jsBean.getImage();
            if(imageJsBeans != null) {
                for(OgImageStructJsBean ogImageStruct : imageJsBeans) {
                    OgImageStruct b = OgImageStructWebService.convertOgImageStructJsBeanToBean(ogImageStruct);
                    imageBeans.add(b); 
                }
            }
            ogTvShow.setImage(imageBeans);
            List<OgAudioStruct> audioBeans = new ArrayList<OgAudioStruct>();
            List<OgAudioStructJsBean> audioJsBeans = jsBean.getAudio();
            if(audioJsBeans != null) {
                for(OgAudioStructJsBean ogAudioStruct : audioJsBeans) {
                    OgAudioStruct b = OgAudioStructWebService.convertOgAudioStructJsBeanToBean(ogAudioStruct);
                    audioBeans.add(b); 
                }
            }
            ogTvShow.setAudio(audioBeans);
            List<OgVideoStruct> videoBeans = new ArrayList<OgVideoStruct>();
            List<OgVideoStructJsBean> videoJsBeans = jsBean.getVideo();
            if(videoJsBeans != null) {
                for(OgVideoStructJsBean ogVideoStruct : videoJsBeans) {
                    OgVideoStruct b = OgVideoStructWebService.convertOgVideoStructJsBeanToBean(ogVideoStruct);
                    videoBeans.add(b); 
                }
            }
            ogTvShow.setVideo(videoBeans);
            ogTvShow.setLocale(jsBean.getLocale());
            ogTvShow.setLocaleAlternate(jsBean.getLocaleAlternate());
            ogTvShow.setDirector(jsBean.getDirector());
            ogTvShow.setWriter(jsBean.getWriter());
            List<OgActorStruct> actorBeans = new ArrayList<OgActorStruct>();
            List<OgActorStructJsBean> actorJsBeans = jsBean.getActor();
            if(actorJsBeans != null) {
                for(OgActorStructJsBean ogActorStruct : actorJsBeans) {
                    OgActorStruct b = OgActorStructWebService.convertOgActorStructJsBeanToBean(ogActorStruct);
                    actorBeans.add(b); 
                }
            }
            ogTvShow.setActor(actorBeans);
            ogTvShow.setDuration(jsBean.getDuration());
            ogTvShow.setTag(jsBean.getTag());
            ogTvShow.setReleaseDate(jsBean.getReleaseDate());
            ogTvShow.setCreatedTime(jsBean.getCreatedTime());
            ogTvShow.setModifiedTime(jsBean.getModifiedTime());
        }
        return ogTvShow;
    }

}
