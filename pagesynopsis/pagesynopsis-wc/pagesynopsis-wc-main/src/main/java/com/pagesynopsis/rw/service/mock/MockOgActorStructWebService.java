package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgActorStructWebService;


// MockOgActorStructWebService is a mock object.
// It can be used as a base class to mock OgActorStructWebService objects.
public abstract class MockOgActorStructWebService extends OgActorStructWebService  // implements OgActorStructService
{
    private static final Logger log = Logger.getLogger(MockOgActorStructWebService.class.getName());
     

}
