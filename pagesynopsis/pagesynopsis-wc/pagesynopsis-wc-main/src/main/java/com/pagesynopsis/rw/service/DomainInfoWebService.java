package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.af.bean.DomainInfoBean;
import com.pagesynopsis.af.service.DomainInfoService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.DomainInfoJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DomainInfoWebService // implements DomainInfoService
{
    private static final Logger log = Logger.getLogger(DomainInfoWebService.class.getName());
     
    // Af service interface.
    private DomainInfoService mService = null;

    public DomainInfoWebService()
    {
        this(ServiceProxyFactory.getInstance().getDomainInfoServiceProxy());
    }
    public DomainInfoWebService(DomainInfoService service)
    {
        mService = service;
    }
    
    protected DomainInfoService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getDomainInfoServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(DomainInfoService service)
    {
        mService = service;
    }
    
    
    public DomainInfoJsBean getDomainInfo(String guid) throws WebException
    {
        try {
            DomainInfo domainInfo = getServiceProxy().getDomainInfo(guid);
            DomainInfoJsBean bean = convertDomainInfoToJsBean(domainInfo);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getDomainInfo(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getDomainInfo(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<DomainInfoJsBean> getDomainInfos(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<DomainInfoJsBean> jsBeans = new ArrayList<DomainInfoJsBean>();
            List<DomainInfo> domainInfos = getServiceProxy().getDomainInfos(guids);
            if(domainInfos != null) {
                for(DomainInfo domainInfo : domainInfos) {
                    jsBeans.add(convertDomainInfoToJsBean(domainInfo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<DomainInfoJsBean> getAllDomainInfos() throws WebException
    {
        return getAllDomainInfos(null, null, null);
    }

    // @Deprecated
    public List<DomainInfoJsBean> getAllDomainInfos(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllDomainInfos(ordering, offset, count, null);
    }

    public List<DomainInfoJsBean> getAllDomainInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<DomainInfoJsBean> jsBeans = new ArrayList<DomainInfoJsBean>();
            List<DomainInfo> domainInfos = getServiceProxy().getAllDomainInfos(ordering, offset, count, forwardCursor);
            if(domainInfos != null) {
                for(DomainInfo domainInfo : domainInfos) {
                    jsBeans.add(convertDomainInfoToJsBean(domainInfo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllDomainInfoKeys(ordering, offset, count, null);
    }

    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllDomainInfoKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<DomainInfoJsBean> findDomainInfos(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findDomainInfos(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<DomainInfoJsBean> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<DomainInfoJsBean> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<DomainInfoJsBean> jsBeans = new ArrayList<DomainInfoJsBean>();
            List<DomainInfo> domainInfos = getServiceProxy().findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(domainInfos != null) {
                for(DomainInfo domainInfo : domainInfos) {
                    jsBeans.add(convertDomainInfoToJsBean(domainInfo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createDomainInfo(String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws WebException
    {
        try {
            return getServiceProxy().createDomainInfo(domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createDomainInfo(DomainInfoJsBean jsBean) throws WebException
    {
        try {
            DomainInfo domainInfo = convertDomainInfoJsBeanToBean(jsBean);
            return getServiceProxy().createDomainInfo(domainInfo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public DomainInfoJsBean constructDomainInfo(DomainInfoJsBean jsBean) throws WebException
    {
        try {
            DomainInfo domainInfo = convertDomainInfoJsBeanToBean(jsBean);
            domainInfo = getServiceProxy().constructDomainInfo(domainInfo);
            jsBean = convertDomainInfoToJsBean(domainInfo);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateDomainInfo(String guid, String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws WebException
    {
        try {
            return getServiceProxy().updateDomainInfo(guid, domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateDomainInfo(DomainInfoJsBean jsBean) throws WebException
    {
        try {
            DomainInfo domainInfo = convertDomainInfoJsBeanToBean(jsBean);
            return getServiceProxy().updateDomainInfo(domainInfo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public DomainInfoJsBean refreshDomainInfo(DomainInfoJsBean jsBean) throws WebException
    {
        try {
            DomainInfo domainInfo = convertDomainInfoJsBeanToBean(jsBean);
            domainInfo = getServiceProxy().refreshDomainInfo(domainInfo);
            jsBean = convertDomainInfoToJsBean(domainInfo);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteDomainInfo(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteDomainInfo(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteDomainInfo(DomainInfoJsBean jsBean) throws WebException
    {
        try {
            DomainInfo domainInfo = convertDomainInfoJsBeanToBean(jsBean);
            return getServiceProxy().deleteDomainInfo(domainInfo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteDomainInfos(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteDomainInfos(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static DomainInfoJsBean convertDomainInfoToJsBean(DomainInfo domainInfo)
    {
        DomainInfoJsBean jsBean = null;
        if(domainInfo != null) {
            jsBean = new DomainInfoJsBean();
            jsBean.setGuid(domainInfo.getGuid());
            jsBean.setDomain(domainInfo.getDomain());
            jsBean.setExcluded(domainInfo.isExcluded());
            jsBean.setCategory(domainInfo.getCategory());
            jsBean.setReputation(domainInfo.getReputation());
            jsBean.setAuthority(domainInfo.getAuthority());
            jsBean.setNote(domainInfo.getNote());
            jsBean.setStatus(domainInfo.getStatus());
            jsBean.setLastCheckedTime(domainInfo.getLastCheckedTime());
            jsBean.setLastUpdatedTime(domainInfo.getLastUpdatedTime());
            jsBean.setCreatedTime(domainInfo.getCreatedTime());
            jsBean.setModifiedTime(domainInfo.getModifiedTime());
        }
        return jsBean;
    }

    public static DomainInfo convertDomainInfoJsBeanToBean(DomainInfoJsBean jsBean)
    {
        DomainInfoBean domainInfo = null;
        if(jsBean != null) {
            domainInfo = new DomainInfoBean();
            domainInfo.setGuid(jsBean.getGuid());
            domainInfo.setDomain(jsBean.getDomain());
            domainInfo.setExcluded(jsBean.isExcluded());
            domainInfo.setCategory(jsBean.getCategory());
            domainInfo.setReputation(jsBean.getReputation());
            domainInfo.setAuthority(jsBean.getAuthority());
            domainInfo.setNote(jsBean.getNote());
            domainInfo.setStatus(jsBean.getStatus());
            domainInfo.setLastCheckedTime(jsBean.getLastCheckedTime());
            domainInfo.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            domainInfo.setCreatedTime(jsBean.getCreatedTime());
            domainInfo.setModifiedTime(jsBean.getModifiedTime());
        }
        return domainInfo;
    }

}
