package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.GeoPointStruct;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.GeoPointStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class GeoPointStructWebService // implements GeoPointStructService
{
    private static final Logger log = Logger.getLogger(GeoPointStructWebService.class.getName());
     
    public static GeoPointStructJsBean convertGeoPointStructToJsBean(GeoPointStruct geoPointStruct)
    {
        GeoPointStructJsBean jsBean = null;
        if(geoPointStruct != null) {
            jsBean = new GeoPointStructJsBean();
            jsBean.setUuid(geoPointStruct.getUuid());
            jsBean.setLatitude(geoPointStruct.getLatitude());
            jsBean.setLongitude(geoPointStruct.getLongitude());
            jsBean.setAltitude(geoPointStruct.getAltitude());
            jsBean.setSensorUsed(geoPointStruct.isSensorUsed());
        }
        return jsBean;
    }

    public static GeoPointStruct convertGeoPointStructJsBeanToBean(GeoPointStructJsBean jsBean)
    {
        GeoPointStructBean geoPointStruct = null;
        if(jsBean != null) {
            geoPointStruct = new GeoPointStructBean();
            geoPointStruct.setUuid(jsBean.getUuid());
            geoPointStruct.setLatitude(jsBean.getLatitude());
            geoPointStruct.setLongitude(jsBean.getLongitude());
            geoPointStruct.setAltitude(jsBean.getAltitude());
            geoPointStruct.setSensorUsed(jsBean.isSensorUsed());
        }
        return geoPointStruct;
    }

}
