package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.service.TwitterPhotoCardService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterPhotoCardWebService // implements TwitterPhotoCardService
{
    private static final Logger log = Logger.getLogger(TwitterPhotoCardWebService.class.getName());
     
    // Af service interface.
    private TwitterPhotoCardService mService = null;

    public TwitterPhotoCardWebService()
    {
        this(ServiceProxyFactory.getInstance().getTwitterPhotoCardServiceProxy());
    }
    public TwitterPhotoCardWebService(TwitterPhotoCardService service)
    {
        mService = service;
    }
    
    protected TwitterPhotoCardService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getTwitterPhotoCardServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(TwitterPhotoCardService service)
    {
        mService = service;
    }
    
    
    public TwitterPhotoCardJsBean getTwitterPhotoCard(String guid) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = getServiceProxy().getTwitterPhotoCard(guid);
            TwitterPhotoCardJsBean bean = convertTwitterPhotoCardToJsBean(twitterPhotoCard);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTwitterPhotoCard(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getTwitterPhotoCard(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterPhotoCardJsBean> getTwitterPhotoCards(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterPhotoCardJsBean> jsBeans = new ArrayList<TwitterPhotoCardJsBean>();
            List<TwitterPhotoCard> twitterPhotoCards = getServiceProxy().getTwitterPhotoCards(guids);
            if(twitterPhotoCards != null) {
                for(TwitterPhotoCard twitterPhotoCard : twitterPhotoCards) {
                    jsBeans.add(convertTwitterPhotoCardToJsBean(twitterPhotoCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterPhotoCardJsBean> getAllTwitterPhotoCards() throws WebException
    {
        return getAllTwitterPhotoCards(null, null, null);
    }

    // @Deprecated
    public List<TwitterPhotoCardJsBean> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTwitterPhotoCards(ordering, offset, count, null);
    }

    public List<TwitterPhotoCardJsBean> getAllTwitterPhotoCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TwitterPhotoCardJsBean> jsBeans = new ArrayList<TwitterPhotoCardJsBean>();
            List<TwitterPhotoCard> twitterPhotoCards = getServiceProxy().getAllTwitterPhotoCards(ordering, offset, count, forwardCursor);
            if(twitterPhotoCards != null) {
                for(TwitterPhotoCard twitterPhotoCard : twitterPhotoCards) {
                    jsBeans.add(convertTwitterPhotoCardToJsBean(twitterPhotoCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTwitterPhotoCardKeys(ordering, offset, count, null);
    }

    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllTwitterPhotoCardKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<TwitterPhotoCardJsBean> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<TwitterPhotoCardJsBean> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<TwitterPhotoCardJsBean> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TwitterPhotoCardJsBean> jsBeans = new ArrayList<TwitterPhotoCardJsBean>();
            List<TwitterPhotoCard> twitterPhotoCards = getServiceProxy().findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(twitterPhotoCards != null) {
                for(TwitterPhotoCard twitterPhotoCard : twitterPhotoCards) {
                    jsBeans.add(convertTwitterPhotoCardToJsBean(twitterPhotoCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterPhotoCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws WebException
    {
        try {
            return getServiceProxy().createTwitterPhotoCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterPhotoCard(TwitterPhotoCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = convertTwitterPhotoCardJsBeanToBean(jsBean);
            return getServiceProxy().createTwitterPhotoCard(twitterPhotoCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterPhotoCardJsBean constructTwitterPhotoCard(TwitterPhotoCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = convertTwitterPhotoCardJsBeanToBean(jsBean);
            twitterPhotoCard = getServiceProxy().constructTwitterPhotoCard(twitterPhotoCard);
            jsBean = convertTwitterPhotoCardToJsBean(twitterPhotoCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTwitterPhotoCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws WebException
    {
        try {
            return getServiceProxy().updateTwitterPhotoCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTwitterPhotoCard(TwitterPhotoCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = convertTwitterPhotoCardJsBeanToBean(jsBean);
            return getServiceProxy().updateTwitterPhotoCard(twitterPhotoCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterPhotoCardJsBean refreshTwitterPhotoCard(TwitterPhotoCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = convertTwitterPhotoCardJsBeanToBean(jsBean);
            twitterPhotoCard = getServiceProxy().refreshTwitterPhotoCard(twitterPhotoCard);
            jsBean = convertTwitterPhotoCardToJsBean(twitterPhotoCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterPhotoCard(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteTwitterPhotoCard(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterPhotoCard(TwitterPhotoCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = convertTwitterPhotoCardJsBeanToBean(jsBean);
            return getServiceProxy().deleteTwitterPhotoCard(twitterPhotoCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteTwitterPhotoCards(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static TwitterPhotoCardJsBean convertTwitterPhotoCardToJsBean(TwitterPhotoCard twitterPhotoCard)
    {
        TwitterPhotoCardJsBean jsBean = null;
        if(twitterPhotoCard != null) {
            jsBean = new TwitterPhotoCardJsBean();
            jsBean.setGuid(twitterPhotoCard.getGuid());
            jsBean.setCard(twitterPhotoCard.getCard());
            jsBean.setUrl(twitterPhotoCard.getUrl());
            jsBean.setTitle(twitterPhotoCard.getTitle());
            jsBean.setDescription(twitterPhotoCard.getDescription());
            jsBean.setSite(twitterPhotoCard.getSite());
            jsBean.setSiteId(twitterPhotoCard.getSiteId());
            jsBean.setCreator(twitterPhotoCard.getCreator());
            jsBean.setCreatorId(twitterPhotoCard.getCreatorId());
            jsBean.setImage(twitterPhotoCard.getImage());
            jsBean.setImageWidth(twitterPhotoCard.getImageWidth());
            jsBean.setImageHeight(twitterPhotoCard.getImageHeight());
            jsBean.setCreatedTime(twitterPhotoCard.getCreatedTime());
            jsBean.setModifiedTime(twitterPhotoCard.getModifiedTime());
        }
        return jsBean;
    }

    public static TwitterPhotoCard convertTwitterPhotoCardJsBeanToBean(TwitterPhotoCardJsBean jsBean)
    {
        TwitterPhotoCardBean twitterPhotoCard = null;
        if(jsBean != null) {
            twitterPhotoCard = new TwitterPhotoCardBean();
            twitterPhotoCard.setGuid(jsBean.getGuid());
            twitterPhotoCard.setCard(jsBean.getCard());
            twitterPhotoCard.setUrl(jsBean.getUrl());
            twitterPhotoCard.setTitle(jsBean.getTitle());
            twitterPhotoCard.setDescription(jsBean.getDescription());
            twitterPhotoCard.setSite(jsBean.getSite());
            twitterPhotoCard.setSiteId(jsBean.getSiteId());
            twitterPhotoCard.setCreator(jsBean.getCreator());
            twitterPhotoCard.setCreatorId(jsBean.getCreatorId());
            twitterPhotoCard.setImage(jsBean.getImage());
            twitterPhotoCard.setImageWidth(jsBean.getImageWidth());
            twitterPhotoCard.setImageHeight(jsBean.getImageHeight());
            twitterPhotoCard.setCreatedTime(jsBean.getCreatedTime());
            twitterPhotoCard.setModifiedTime(jsBean.getModifiedTime());
        }
        return twitterPhotoCard;
    }

}
