package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.af.bean.DomainInfoBean;
import com.pagesynopsis.af.service.DomainInfoService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.DomainInfoJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.DomainInfoWebService;


// MockDomainInfoWebService is a mock object.
// It can be used as a base class to mock DomainInfoWebService objects.
public abstract class MockDomainInfoWebService extends DomainInfoWebService  // implements DomainInfoService
{
    private static final Logger log = Logger.getLogger(MockDomainInfoWebService.class.getName());
     
    // Af service interface.
    private DomainInfoWebService mService = null;

    public MockDomainInfoWebService()
    {
        this(MockServiceProxyFactory.getInstance().getDomainInfoServiceProxy());
    }
    public MockDomainInfoWebService(DomainInfoService service)
    {
        super(service);
    }


}
