package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.ImageStructWebService;


// MockImageStructWebService is a mock object.
// It can be used as a base class to mock ImageStructWebService objects.
public abstract class MockImageStructWebService extends ImageStructWebService  // implements ImageStructService
{
    private static final Logger log = Logger.getLogger(MockImageStructWebService.class.getName());
     

}
