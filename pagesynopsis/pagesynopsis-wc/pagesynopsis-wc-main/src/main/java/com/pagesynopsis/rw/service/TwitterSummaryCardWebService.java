package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.service.TwitterSummaryCardService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterSummaryCardWebService // implements TwitterSummaryCardService
{
    private static final Logger log = Logger.getLogger(TwitterSummaryCardWebService.class.getName());
     
    // Af service interface.
    private TwitterSummaryCardService mService = null;

    public TwitterSummaryCardWebService()
    {
        this(ServiceProxyFactory.getInstance().getTwitterSummaryCardServiceProxy());
    }
    public TwitterSummaryCardWebService(TwitterSummaryCardService service)
    {
        mService = service;
    }
    
    protected TwitterSummaryCardService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getTwitterSummaryCardServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(TwitterSummaryCardService service)
    {
        mService = service;
    }
    
    
    public TwitterSummaryCardJsBean getTwitterSummaryCard(String guid) throws WebException
    {
        try {
            TwitterSummaryCard twitterSummaryCard = getServiceProxy().getTwitterSummaryCard(guid);
            TwitterSummaryCardJsBean bean = convertTwitterSummaryCardToJsBean(twitterSummaryCard);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTwitterSummaryCard(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getTwitterSummaryCard(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterSummaryCardJsBean> getTwitterSummaryCards(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterSummaryCardJsBean> jsBeans = new ArrayList<TwitterSummaryCardJsBean>();
            List<TwitterSummaryCard> twitterSummaryCards = getServiceProxy().getTwitterSummaryCards(guids);
            if(twitterSummaryCards != null) {
                for(TwitterSummaryCard twitterSummaryCard : twitterSummaryCards) {
                    jsBeans.add(convertTwitterSummaryCardToJsBean(twitterSummaryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterSummaryCardJsBean> getAllTwitterSummaryCards() throws WebException
    {
        return getAllTwitterSummaryCards(null, null, null);
    }

    // @Deprecated
    public List<TwitterSummaryCardJsBean> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTwitterSummaryCards(ordering, offset, count, null);
    }

    public List<TwitterSummaryCardJsBean> getAllTwitterSummaryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TwitterSummaryCardJsBean> jsBeans = new ArrayList<TwitterSummaryCardJsBean>();
            List<TwitterSummaryCard> twitterSummaryCards = getServiceProxy().getAllTwitterSummaryCards(ordering, offset, count, forwardCursor);
            if(twitterSummaryCards != null) {
                for(TwitterSummaryCard twitterSummaryCard : twitterSummaryCards) {
                    jsBeans.add(convertTwitterSummaryCardToJsBean(twitterSummaryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTwitterSummaryCardKeys(ordering, offset, count, null);
    }

    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllTwitterSummaryCardKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<TwitterSummaryCardJsBean> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTwitterSummaryCards(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<TwitterSummaryCardJsBean> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<TwitterSummaryCardJsBean> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TwitterSummaryCardJsBean> jsBeans = new ArrayList<TwitterSummaryCardJsBean>();
            List<TwitterSummaryCard> twitterSummaryCards = getServiceProxy().findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(twitterSummaryCards != null) {
                for(TwitterSummaryCard twitterSummaryCard : twitterSummaryCards) {
                    jsBeans.add(convertTwitterSummaryCardToJsBean(twitterSummaryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterSummaryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws WebException
    {
        try {
            return getServiceProxy().createTwitterSummaryCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterSummaryCard(TwitterSummaryCardJsBean jsBean) throws WebException
    {
        try {
            TwitterSummaryCard twitterSummaryCard = convertTwitterSummaryCardJsBeanToBean(jsBean);
            return getServiceProxy().createTwitterSummaryCard(twitterSummaryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterSummaryCardJsBean constructTwitterSummaryCard(TwitterSummaryCardJsBean jsBean) throws WebException
    {
        try {
            TwitterSummaryCard twitterSummaryCard = convertTwitterSummaryCardJsBeanToBean(jsBean);
            twitterSummaryCard = getServiceProxy().constructTwitterSummaryCard(twitterSummaryCard);
            jsBean = convertTwitterSummaryCardToJsBean(twitterSummaryCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTwitterSummaryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws WebException
    {
        try {
            return getServiceProxy().updateTwitterSummaryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTwitterSummaryCard(TwitterSummaryCardJsBean jsBean) throws WebException
    {
        try {
            TwitterSummaryCard twitterSummaryCard = convertTwitterSummaryCardJsBeanToBean(jsBean);
            return getServiceProxy().updateTwitterSummaryCard(twitterSummaryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterSummaryCardJsBean refreshTwitterSummaryCard(TwitterSummaryCardJsBean jsBean) throws WebException
    {
        try {
            TwitterSummaryCard twitterSummaryCard = convertTwitterSummaryCardJsBeanToBean(jsBean);
            twitterSummaryCard = getServiceProxy().refreshTwitterSummaryCard(twitterSummaryCard);
            jsBean = convertTwitterSummaryCardToJsBean(twitterSummaryCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterSummaryCard(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteTwitterSummaryCard(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterSummaryCard(TwitterSummaryCardJsBean jsBean) throws WebException
    {
        try {
            TwitterSummaryCard twitterSummaryCard = convertTwitterSummaryCardJsBeanToBean(jsBean);
            return getServiceProxy().deleteTwitterSummaryCard(twitterSummaryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteTwitterSummaryCards(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static TwitterSummaryCardJsBean convertTwitterSummaryCardToJsBean(TwitterSummaryCard twitterSummaryCard)
    {
        TwitterSummaryCardJsBean jsBean = null;
        if(twitterSummaryCard != null) {
            jsBean = new TwitterSummaryCardJsBean();
            jsBean.setGuid(twitterSummaryCard.getGuid());
            jsBean.setCard(twitterSummaryCard.getCard());
            jsBean.setUrl(twitterSummaryCard.getUrl());
            jsBean.setTitle(twitterSummaryCard.getTitle());
            jsBean.setDescription(twitterSummaryCard.getDescription());
            jsBean.setSite(twitterSummaryCard.getSite());
            jsBean.setSiteId(twitterSummaryCard.getSiteId());
            jsBean.setCreator(twitterSummaryCard.getCreator());
            jsBean.setCreatorId(twitterSummaryCard.getCreatorId());
            jsBean.setImage(twitterSummaryCard.getImage());
            jsBean.setImageWidth(twitterSummaryCard.getImageWidth());
            jsBean.setImageHeight(twitterSummaryCard.getImageHeight());
            jsBean.setCreatedTime(twitterSummaryCard.getCreatedTime());
            jsBean.setModifiedTime(twitterSummaryCard.getModifiedTime());
        }
        return jsBean;
    }

    public static TwitterSummaryCard convertTwitterSummaryCardJsBeanToBean(TwitterSummaryCardJsBean jsBean)
    {
        TwitterSummaryCardBean twitterSummaryCard = null;
        if(jsBean != null) {
            twitterSummaryCard = new TwitterSummaryCardBean();
            twitterSummaryCard.setGuid(jsBean.getGuid());
            twitterSummaryCard.setCard(jsBean.getCard());
            twitterSummaryCard.setUrl(jsBean.getUrl());
            twitterSummaryCard.setTitle(jsBean.getTitle());
            twitterSummaryCard.setDescription(jsBean.getDescription());
            twitterSummaryCard.setSite(jsBean.getSite());
            twitterSummaryCard.setSiteId(jsBean.getSiteId());
            twitterSummaryCard.setCreator(jsBean.getCreator());
            twitterSummaryCard.setCreatorId(jsBean.getCreatorId());
            twitterSummaryCard.setImage(jsBean.getImage());
            twitterSummaryCard.setImageWidth(jsBean.getImageWidth());
            twitterSummaryCard.setImageHeight(jsBean.getImageHeight());
            twitterSummaryCard.setCreatedTime(jsBean.getCreatedTime());
            twitterSummaryCard.setModifiedTime(jsBean.getModifiedTime());
        }
        return twitterSummaryCard;
    }

}
