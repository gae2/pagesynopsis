package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.FullNameStruct;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.FullNameStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.FullNameStructWebService;


// MockFullNameStructWebService is a mock object.
// It can be used as a base class to mock FullNameStructWebService objects.
public abstract class MockFullNameStructWebService extends FullNameStructWebService  // implements FullNameStructService
{
    private static final Logger log = Logger.getLogger(MockFullNameStructWebService.class.getName());
     

}
