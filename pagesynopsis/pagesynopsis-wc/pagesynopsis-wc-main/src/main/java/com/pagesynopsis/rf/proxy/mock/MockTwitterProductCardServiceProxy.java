package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoListStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataListStub;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.TwitterProductCardListStub;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.service.TwitterProductCardService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.TwitterProductCardServiceProxy;


// MockTwitterProductCardServiceProxy is a decorator.
// It can be used as a base class to mock TwitterProductCardService objects.
public abstract class MockTwitterProductCardServiceProxy extends TwitterProductCardServiceProxy implements TwitterProductCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterProductCardServiceProxy.class.getName());

    // MockTwitterProductCardServiceProxy uses the decorator design pattern.
    private TwitterProductCardService decoratedProxy;

    public MockTwitterProductCardServiceProxy(TwitterProductCardService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected TwitterProductCardService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(TwitterProductCardService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public TwitterProductCard getTwitterProductCard(String guid) throws BaseException
    {
        return decoratedProxy.getTwitterProductCard(guid);
    }

    @Override
    public Object getTwitterProductCard(String guid, String field) throws BaseException
    {
        return decoratedProxy.getTwitterProductCard(guid, field);
    }

    @Override
    public List<TwitterProductCard> getTwitterProductCards(List<String> guids) throws BaseException
    {
        return decoratedProxy.getTwitterProductCards(guids);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards() throws BaseException
    {
        return getAllTwitterProductCards(null, null, null);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterProductCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterProductCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterProductCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterProductCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterProductCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterProductCards(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterProductCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException
    {
        return decoratedProxy.createTwitterProductCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1, data2);
    }

    @Override
    public String createTwitterProductCard(TwitterProductCard bean) throws BaseException
    {
        return decoratedProxy.createTwitterProductCard(bean);
    }

    @Override
    public TwitterProductCard constructTwitterProductCard(TwitterProductCard bean) throws BaseException
    {
        return decoratedProxy.constructTwitterProductCard(bean);
    }

    @Override
    public Boolean updateTwitterProductCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductData data1, TwitterCardProductData data2) throws BaseException
    {
        return decoratedProxy.updateTwitterProductCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, data1, data2);
    }

    @Override
    public Boolean updateTwitterProductCard(TwitterProductCard bean) throws BaseException
    {
        return decoratedProxy.updateTwitterProductCard(bean);
    }

    @Override
    public TwitterProductCard refreshTwitterProductCard(TwitterProductCard bean) throws BaseException
    {
        return decoratedProxy.refreshTwitterProductCard(bean);
    }

    @Override
    public Boolean deleteTwitterProductCard(String guid) throws BaseException
    {
        return decoratedProxy.deleteTwitterProductCard(guid);
    }

    @Override
    public Boolean deleteTwitterProductCard(TwitterProductCard bean) throws BaseException
    {
        return decoratedProxy.deleteTwitterProductCard(bean);
    }

    @Override
    public Long deleteTwitterProductCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteTwitterProductCards(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterProductCards(List<TwitterProductCard> twitterProductCards) throws BaseException
    {
        return decoratedProxy.createTwitterProductCards(twitterProductCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterProductCards(List<TwitterProductCard> twitterProductCards) throws BaseException
    //{
    //}

}
