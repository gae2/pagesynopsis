package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.HelpNotice;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.HelpNoticeJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.HelpNoticeWebService;


// MockHelpNoticeWebService is a mock object.
// It can be used as a base class to mock HelpNoticeWebService objects.
public abstract class MockHelpNoticeWebService extends HelpNoticeWebService  // implements HelpNoticeService
{
    private static final Logger log = Logger.getLogger(MockHelpNoticeWebService.class.getName());
     

}
