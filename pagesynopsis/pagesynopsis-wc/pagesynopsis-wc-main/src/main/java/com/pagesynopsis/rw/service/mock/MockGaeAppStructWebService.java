package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.GaeAppStruct;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.GaeAppStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.GaeAppStructWebService;


// MockGaeAppStructWebService is a mock object.
// It can be used as a base class to mock GaeAppStructWebService objects.
public abstract class MockGaeAppStructWebService extends GaeAppStructWebService  // implements GaeAppStructService
{
    private static final Logger log = Logger.getLogger(MockGaeAppStructWebService.class.getName());
     

}
