package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.GeoCoordinateStruct;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.GeoCoordinateStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.GeoCoordinateStructWebService;


// MockGeoCoordinateStructWebService is a mock object.
// It can be used as a base class to mock GeoCoordinateStructWebService objects.
public abstract class MockGeoCoordinateStructWebService extends GeoCoordinateStructWebService  // implements GeoCoordinateStructService
{
    private static final Logger log = Logger.getLogger(MockGeoCoordinateStructWebService.class.getName());
     

}
