package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.GaeUserStruct;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.GaeUserStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class GaeUserStructWebService // implements GaeUserStructService
{
    private static final Logger log = Logger.getLogger(GaeUserStructWebService.class.getName());
     
    public static GaeUserStructJsBean convertGaeUserStructToJsBean(GaeUserStruct gaeUserStruct)
    {
        GaeUserStructJsBean jsBean = null;
        if(gaeUserStruct != null) {
            jsBean = new GaeUserStructJsBean();
            jsBean.setAuthDomain(gaeUserStruct.getAuthDomain());
            jsBean.setFederatedIdentity(gaeUserStruct.getFederatedIdentity());
            jsBean.setNickname(gaeUserStruct.getNickname());
            jsBean.setUserId(gaeUserStruct.getUserId());
            jsBean.setEmail(gaeUserStruct.getEmail());
            jsBean.setNote(gaeUserStruct.getNote());
        }
        return jsBean;
    }

    public static GaeUserStruct convertGaeUserStructJsBeanToBean(GaeUserStructJsBean jsBean)
    {
        GaeUserStructBean gaeUserStruct = null;
        if(jsBean != null) {
            gaeUserStruct = new GaeUserStructBean();
            gaeUserStruct.setAuthDomain(jsBean.getAuthDomain());
            gaeUserStruct.setFederatedIdentity(jsBean.getFederatedIdentity());
            gaeUserStruct.setNickname(jsBean.getNickname());
            gaeUserStruct.setUserId(jsBean.getUserId());
            gaeUserStruct.setEmail(jsBean.getEmail());
            gaeUserStruct.setNote(jsBean.getNote());
        }
        return gaeUserStruct;
    }

}
