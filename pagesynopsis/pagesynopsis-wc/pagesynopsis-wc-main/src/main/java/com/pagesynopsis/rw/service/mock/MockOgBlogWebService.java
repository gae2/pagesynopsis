package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.service.OgBlogService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgBlogWebService;


// MockOgBlogWebService is a mock object.
// It can be used as a base class to mock OgBlogWebService objects.
public abstract class MockOgBlogWebService extends OgBlogWebService  // implements OgBlogService
{
    private static final Logger log = Logger.getLogger(MockOgBlogWebService.class.getName());
     
    // Af service interface.
    private OgBlogWebService mService = null;

    public MockOgBlogWebService()
    {
        this(MockServiceProxyFactory.getInstance().getOgBlogServiceProxy());
    }
    public MockOgBlogWebService(OgBlogService service)
    {
        super(service);
    }


}
