package com.pagesynopsis.web.service;

import java.util.logging.Logger;

import com.pagesynopsis.af.service.PageInfoService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.PageInfoJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;
import com.pagesynopsis.rw.service.PageInfoWebService;
import com.pagesynopsis.web.proxy.PageInfoWebServiceProxy;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.PageInfo;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class PageInfoProxyWebService
{
    private static final Logger log = Logger.getLogger(PageInfoProxyWebService.class.getName());
     
    // Af service interface.
    private PageInfoWebServiceProxy mServiceProxy = null;

    public PageInfoProxyWebService()
    {
        //this(new PageInfoWebServiceProxy());
        this(null);
    }
    public PageInfoProxyWebService(PageInfoWebServiceProxy serviceProxy)
    {
        mServiceProxy = serviceProxy;
    }
    
    private PageInfoWebServiceProxy getServiceProxy()
    {
        if(mServiceProxy == null) {
            mServiceProxy = new PageInfoWebServiceProxy();
        }
        return mServiceProxy;
    }
    

//  "Get" is better than "Find" for this: Get returns a full object, whereas Find returns only an object with "default fetch". 
    public PageInfoJsBean findPageInfoByTargetUrl(String targetUrl, Boolean fetch) throws WebException
    {
        return findPageInfoByTargetUrl(targetUrl, fetch, null);
    }
    public PageInfoJsBean findPageInfoByTargetUrl(String targetUrl, Boolean fetch, Integer refresh) throws WebException
    {
        try {
            PageInfoJsBean jsBean = null;
            PageInfo pageInfo = getServiceProxy().findPageInfoByTargetUrl(targetUrl, fetch, refresh);
            if(pageInfo != null) {
                jsBean = PageInfoWebService.convertPageInfoToJsBean(pageInfo);
            }
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    
}
