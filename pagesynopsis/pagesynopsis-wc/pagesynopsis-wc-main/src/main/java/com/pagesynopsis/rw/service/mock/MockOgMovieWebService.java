package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgMovieWebService;


// MockOgMovieWebService is a mock object.
// It can be used as a base class to mock OgMovieWebService objects.
public abstract class MockOgMovieWebService extends OgMovieWebService  // implements OgMovieService
{
    private static final Logger log = Logger.getLogger(MockOgMovieWebService.class.getName());
     
    // Af service interface.
    private OgMovieWebService mService = null;

    public MockOgMovieWebService()
    {
        this(MockServiceProxyFactory.getInstance().getOgMovieServiceProxy());
    }
    public MockOgMovieWebService(OgMovieService service)
    {
        super(service);
    }


}
