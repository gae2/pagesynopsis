package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.DecodedQueryParamStruct;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.DecodedQueryParamStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.DecodedQueryParamStructWebService;


// MockDecodedQueryParamStructWebService is a mock object.
// It can be used as a base class to mock DecodedQueryParamStructWebService objects.
public abstract class MockDecodedQueryParamStructWebService extends DecodedQueryParamStructWebService  // implements DecodedQueryParamStructService
{
    private static final Logger log = Logger.getLogger(MockDecodedQueryParamStructWebService.class.getName());
     

}
