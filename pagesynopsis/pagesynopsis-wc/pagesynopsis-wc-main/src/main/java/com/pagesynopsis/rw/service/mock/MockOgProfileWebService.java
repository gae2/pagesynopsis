package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.service.OgProfileService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgProfileWebService;


// MockOgProfileWebService is a mock object.
// It can be used as a base class to mock OgProfileWebService objects.
public abstract class MockOgProfileWebService extends OgProfileWebService  // implements OgProfileService
{
    private static final Logger log = Logger.getLogger(MockOgProfileWebService.class.getName());
     
    // Af service interface.
    private OgProfileWebService mService = null;

    public MockOgProfileWebService()
    {
        this(MockServiceProxyFactory.getInstance().getOgProfileServiceProxy());
    }
    public MockOgProfileWebService(OgProfileService service)
    {
        super(service);
    }


}
