package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoListStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataListStub;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.TwitterAppCardListStub;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.service.TwitterAppCardService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.TwitterAppCardServiceProxy;


// MockTwitterAppCardServiceProxy is a decorator.
// It can be used as a base class to mock TwitterAppCardService objects.
public abstract class MockTwitterAppCardServiceProxy extends TwitterAppCardServiceProxy implements TwitterAppCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterAppCardServiceProxy.class.getName());

    // MockTwitterAppCardServiceProxy uses the decorator design pattern.
    private TwitterAppCardService decoratedProxy;

    public MockTwitterAppCardServiceProxy(TwitterAppCardService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected TwitterAppCardService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(TwitterAppCardService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public TwitterAppCard getTwitterAppCard(String guid) throws BaseException
    {
        return decoratedProxy.getTwitterAppCard(guid);
    }

    @Override
    public Object getTwitterAppCard(String guid, String field) throws BaseException
    {
        return decoratedProxy.getTwitterAppCard(guid, field);
    }

    @Override
    public List<TwitterAppCard> getTwitterAppCards(List<String> guids) throws BaseException
    {
        return decoratedProxy.getTwitterAppCards(guids);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards() throws BaseException
    {
        return getAllTwitterAppCards(null, null, null);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterAppCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterAppCard> getAllTwitterAppCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterAppCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterAppCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterAppCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterAppCard> findTwitterAppCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterAppCards(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterAppCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        return decoratedProxy.createTwitterAppCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp);
    }

    @Override
    public String createTwitterAppCard(TwitterAppCard bean) throws BaseException
    {
        return decoratedProxy.createTwitterAppCard(bean);
    }

    @Override
    public TwitterAppCard constructTwitterAppCard(TwitterAppCard bean) throws BaseException
    {
        return decoratedProxy.constructTwitterAppCard(bean);
    }

    @Override
    public Boolean updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfo iphoneApp, TwitterCardAppInfo ipadApp, TwitterCardAppInfo googlePlayApp) throws BaseException
    {
        return decoratedProxy.updateTwitterAppCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, iphoneApp, ipadApp, googlePlayApp);
    }

    @Override
    public Boolean updateTwitterAppCard(TwitterAppCard bean) throws BaseException
    {
        return decoratedProxy.updateTwitterAppCard(bean);
    }

    @Override
    public TwitterAppCard refreshTwitterAppCard(TwitterAppCard bean) throws BaseException
    {
        return decoratedProxy.refreshTwitterAppCard(bean);
    }

    @Override
    public Boolean deleteTwitterAppCard(String guid) throws BaseException
    {
        return decoratedProxy.deleteTwitterAppCard(guid);
    }

    @Override
    public Boolean deleteTwitterAppCard(TwitterAppCard bean) throws BaseException
    {
        return decoratedProxy.deleteTwitterAppCard(bean);
    }

    @Override
    public Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteTwitterAppCards(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException
    {
        return decoratedProxy.createTwitterAppCards(twitterAppCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterAppCards(List<TwitterAppCard> twitterAppCards) throws BaseException
    //{
    //}

}
