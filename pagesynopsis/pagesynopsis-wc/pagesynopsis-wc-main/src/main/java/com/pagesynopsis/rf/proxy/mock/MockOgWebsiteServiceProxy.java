package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgAudioStructListStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgImageStructListStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgActorStructListStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructListStub;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgWebsiteListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.service.OgWebsiteService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.OgWebsiteServiceProxy;


// MockOgWebsiteServiceProxy is a decorator.
// It can be used as a base class to mock OgWebsiteService objects.
public abstract class MockOgWebsiteServiceProxy extends OgWebsiteServiceProxy implements OgWebsiteService
{
    private static final Logger log = Logger.getLogger(MockOgWebsiteServiceProxy.class.getName());

    // MockOgWebsiteServiceProxy uses the decorator design pattern.
    private OgWebsiteService decoratedProxy;

    public MockOgWebsiteServiceProxy(OgWebsiteService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected OgWebsiteService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(OgWebsiteService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public OgWebsite getOgWebsite(String guid) throws BaseException
    {
        return decoratedProxy.getOgWebsite(guid);
    }

    @Override
    public Object getOgWebsite(String guid, String field) throws BaseException
    {
        return decoratedProxy.getOgWebsite(guid, field);
    }

    @Override
    public List<OgWebsite> getOgWebsites(List<String> guids) throws BaseException
    {
        return decoratedProxy.getOgWebsites(guids);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites() throws BaseException
    {
        return getAllOgWebsites(null, null, null);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgWebsites(ordering, offset, count, null);
    }

    @Override
    public List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgWebsites(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgWebsiteKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgWebsiteKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgWebsites(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgWebsite> findOgWebsites(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgWebsites(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgWebsiteKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgWebsite(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return decoratedProxy.createOgWebsite(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public String createOgWebsite(OgWebsite bean) throws BaseException
    {
        return decoratedProxy.createOgWebsite(bean);
    }

    @Override
    public OgWebsite constructOgWebsite(OgWebsite bean) throws BaseException
    {
        return decoratedProxy.constructOgWebsite(bean);
    }

    @Override
    public Boolean updateOgWebsite(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        return decoratedProxy.updateOgWebsite(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
    }

    @Override
    public Boolean updateOgWebsite(OgWebsite bean) throws BaseException
    {
        return decoratedProxy.updateOgWebsite(bean);
    }

    @Override
    public OgWebsite refreshOgWebsite(OgWebsite bean) throws BaseException
    {
        return decoratedProxy.refreshOgWebsite(bean);
    }

    @Override
    public Boolean deleteOgWebsite(String guid) throws BaseException
    {
        return decoratedProxy.deleteOgWebsite(guid);
    }

    @Override
    public Boolean deleteOgWebsite(OgWebsite bean) throws BaseException
    {
        return decoratedProxy.deleteOgWebsite(bean);
    }

    @Override
    public Long deleteOgWebsites(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteOgWebsites(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgWebsites(List<OgWebsite> ogWebsites) throws BaseException
    {
        return decoratedProxy.createOgWebsites(ogWebsites);
    }

    // TBD
    //@Override
    //public Boolean updateOgWebsites(List<OgWebsite> ogWebsites) throws BaseException
    //{
    //}

}
