package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.UserWebsiteStruct;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.UserWebsiteStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.UserWebsiteStructWebService;


// MockUserWebsiteStructWebService is a mock object.
// It can be used as a base class to mock UserWebsiteStructWebService objects.
public abstract class MockUserWebsiteStructWebService extends UserWebsiteStructWebService  // implements UserWebsiteStructService
{
    private static final Logger log = Logger.getLogger(MockUserWebsiteStructWebService.class.getName());
     

}
