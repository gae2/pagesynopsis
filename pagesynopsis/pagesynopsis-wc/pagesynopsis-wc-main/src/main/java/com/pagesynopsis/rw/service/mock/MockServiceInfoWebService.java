package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.ServiceInfo;
import com.pagesynopsis.af.bean.ServiceInfoBean;
import com.pagesynopsis.af.service.ServiceInfoService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.ServiceInfoJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.ServiceInfoWebService;


// MockServiceInfoWebService is a mock object.
// It can be used as a base class to mock ServiceInfoWebService objects.
public abstract class MockServiceInfoWebService extends ServiceInfoWebService  // implements ServiceInfoService
{
    private static final Logger log = Logger.getLogger(MockServiceInfoWebService.class.getName());
     
    // Af service interface.
    private ServiceInfoWebService mService = null;

    public MockServiceInfoWebService()
    {
        this(MockServiceProxyFactory.getInstance().getServiceInfoServiceProxy());
    }
    public MockServiceInfoWebService(ServiceInfoService service)
    {
        super(service);
    }


}
