package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgBookWebService;


// MockOgBookWebService is a mock object.
// It can be used as a base class to mock OgBookWebService objects.
public abstract class MockOgBookWebService extends OgBookWebService  // implements OgBookService
{
    private static final Logger log = Logger.getLogger(MockOgBookWebService.class.getName());
     
    // Af service interface.
    private OgBookWebService mService = null;

    public MockOgBookWebService()
    {
        this(MockServiceProxyFactory.getInstance().getOgBookServiceProxy());
    }
    public MockOgBookWebService(OgBookService service)
    {
        super(service);
    }


}
