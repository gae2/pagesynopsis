package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgTvShowWebService;


// MockOgTvShowWebService is a mock object.
// It can be used as a base class to mock OgTvShowWebService objects.
public abstract class MockOgTvShowWebService extends OgTvShowWebService  // implements OgTvShowService
{
    private static final Logger log = Logger.getLogger(MockOgTvShowWebService.class.getName());
     
    // Af service interface.
    private OgTvShowWebService mService = null;

    public MockOgTvShowWebService()
    {
        this(MockServiceProxyFactory.getInstance().getOgTvShowServiceProxy());
    }
    public MockOgTvShowWebService(OgTvShowService service)
    {
        super(service);
    }


}
