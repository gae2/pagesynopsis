package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.RobotsTextRefreshJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.RobotsTextRefreshWebService;


// MockRobotsTextRefreshWebService is a mock object.
// It can be used as a base class to mock RobotsTextRefreshWebService objects.
public abstract class MockRobotsTextRefreshWebService extends RobotsTextRefreshWebService  // implements RobotsTextRefreshService
{
    private static final Logger log = Logger.getLogger(MockRobotsTextRefreshWebService.class.getName());
     
    // Af service interface.
    private RobotsTextRefreshWebService mService = null;

    public MockRobotsTextRefreshWebService()
    {
        this(MockServiceProxyFactory.getInstance().getRobotsTextRefreshServiceProxy());
    }
    public MockRobotsTextRefreshWebService(RobotsTextRefreshService service)
    {
        super(service);
    }


}
