package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgAudioStructListStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgImageStructListStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgActorStructListStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructListStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.stub.OgMovieListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.OgMovieServiceProxy;


// MockOgMovieServiceProxy is a decorator.
// It can be used as a base class to mock OgMovieService objects.
public abstract class MockOgMovieServiceProxy extends OgMovieServiceProxy implements OgMovieService
{
    private static final Logger log = Logger.getLogger(MockOgMovieServiceProxy.class.getName());

    // MockOgMovieServiceProxy uses the decorator design pattern.
    private OgMovieService decoratedProxy;

    public MockOgMovieServiceProxy(OgMovieService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected OgMovieService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(OgMovieService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public OgMovie getOgMovie(String guid) throws BaseException
    {
        return decoratedProxy.getOgMovie(guid);
    }

    @Override
    public Object getOgMovie(String guid, String field) throws BaseException
    {
        return decoratedProxy.getOgMovie(guid, field);
    }

    @Override
    public List<OgMovie> getOgMovies(List<String> guids) throws BaseException
    {
        return decoratedProxy.getOgMovies(guids);
    }

    @Override
    public List<OgMovie> getAllOgMovies() throws BaseException
    {
        return getAllOgMovies(null, null, null);
    }

    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgMovies(ordering, offset, count, null);
    }

    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgMovies(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgMovieKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgMovieKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgMovies(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgMovieKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgMovie(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedProxy.createOgMovie(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public String createOgMovie(OgMovie bean) throws BaseException
    {
        return decoratedProxy.createOgMovie(bean);
    }

    @Override
    public OgMovie constructOgMovie(OgMovie bean) throws BaseException
    {
        return decoratedProxy.constructOgMovie(bean);
    }

    @Override
    public Boolean updateOgMovie(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedProxy.updateOgMovie(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }

    @Override
    public Boolean updateOgMovie(OgMovie bean) throws BaseException
    {
        return decoratedProxy.updateOgMovie(bean);
    }

    @Override
    public OgMovie refreshOgMovie(OgMovie bean) throws BaseException
    {
        return decoratedProxy.refreshOgMovie(bean);
    }

    @Override
    public Boolean deleteOgMovie(String guid) throws BaseException
    {
        return decoratedProxy.deleteOgMovie(guid);
    }

    @Override
    public Boolean deleteOgMovie(OgMovie bean) throws BaseException
    {
        return decoratedProxy.deleteOgMovie(bean);
    }

    @Override
    public Long deleteOgMovies(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteOgMovies(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgMovies(List<OgMovie> ogMovies) throws BaseException
    {
        return decoratedProxy.createOgMovies(ogMovies);
    }

    // TBD
    //@Override
    //public Boolean updateOgMovies(List<OgMovie> ogMovies) throws BaseException
    //{
    //}

}
