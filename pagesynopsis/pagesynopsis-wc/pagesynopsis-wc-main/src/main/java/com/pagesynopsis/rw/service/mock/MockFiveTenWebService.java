package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.FiveTen;
import com.pagesynopsis.af.bean.FiveTenBean;
import com.pagesynopsis.af.service.FiveTenService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.FiveTenJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.FiveTenWebService;


// MockFiveTenWebService is a mock object.
// It can be used as a base class to mock FiveTenWebService objects.
public abstract class MockFiveTenWebService extends FiveTenWebService  // implements FiveTenService
{
    private static final Logger log = Logger.getLogger(MockFiveTenWebService.class.getName());
     
    // Af service interface.
    private FiveTenWebService mService = null;

    public MockFiveTenWebService()
    {
        this(MockServiceProxyFactory.getInstance().getFiveTenServiceProxy());
    }
    public MockFiveTenWebService(FiveTenService service)
    {
        super(service);
    }


}
