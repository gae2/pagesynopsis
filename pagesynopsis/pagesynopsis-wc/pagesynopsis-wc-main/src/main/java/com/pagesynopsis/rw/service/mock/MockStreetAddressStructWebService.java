package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.StreetAddressStruct;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.StreetAddressStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.StreetAddressStructWebService;


// MockStreetAddressStructWebService is a mock object.
// It can be used as a base class to mock StreetAddressStructWebService objects.
public abstract class MockStreetAddressStructWebService extends StreetAddressStructWebService  // implements StreetAddressStructService
{
    private static final Logger log = Logger.getLogger(MockStreetAddressStructWebService.class.getName());
     

}
