package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.GeoPointStruct;
import com.pagesynopsis.ws.StreetAddressStruct;
import com.pagesynopsis.ws.GaeAppStruct;
import com.pagesynopsis.ws.FullNameStruct;
import com.pagesynopsis.ws.GaeUserStruct;
import com.pagesynopsis.ws.User;
import com.pagesynopsis.af.bean.UserBean;
import com.pagesynopsis.af.service.UserService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.GeoPointStructJsBean;
import com.pagesynopsis.fe.bean.StreetAddressStructJsBean;
import com.pagesynopsis.fe.bean.GaeAppStructJsBean;
import com.pagesynopsis.fe.bean.FullNameStructJsBean;
import com.pagesynopsis.fe.bean.GaeUserStructJsBean;
import com.pagesynopsis.fe.bean.UserJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserWebService // implements UserService
{
    private static final Logger log = Logger.getLogger(UserWebService.class.getName());
     
    // Af service interface.
    private UserService mService = null;

    public UserWebService()
    {
        this(ServiceProxyFactory.getInstance().getUserServiceProxy());
    }
    public UserWebService(UserService service)
    {
        mService = service;
    }
    
    protected UserService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getUserServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(UserService service)
    {
        mService = service;
    }
    
    
    public UserJsBean getUser(String guid) throws WebException
    {
        try {
            User user = getServiceProxy().getUser(guid);
            UserJsBean bean = convertUserToJsBean(user);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUser(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getUser(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserJsBean> getUsers(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserJsBean> jsBeans = new ArrayList<UserJsBean>();
            List<User> users = getServiceProxy().getUsers(guids);
            if(users != null) {
                for(User user : users) {
                    jsBeans.add(convertUserToJsBean(user));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserJsBean> getAllUsers() throws WebException
    {
        return getAllUsers(null, null, null);
    }

    // @Deprecated
    public List<UserJsBean> getAllUsers(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllUsers(ordering, offset, count, null);
    }

    public List<UserJsBean> getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<UserJsBean> jsBeans = new ArrayList<UserJsBean>();
            List<User> users = getServiceProxy().getAllUsers(ordering, offset, count, forwardCursor);
            if(users != null) {
                for(User user : users) {
                    jsBeans.add(convertUserToJsBean(user));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllUserKeys(ordering, offset, count, null);
    }

    public List<String> getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllUserKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<UserJsBean> findUsers(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<UserJsBean> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findUsers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<UserJsBean> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<UserJsBean> jsBeans = new ArrayList<UserJsBean>();
            List<User> users = getServiceProxy().findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(users != null) {
                for(User user : users) {
                    jsBeans.add(convertUserToJsBean(user));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUser(String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStructJsBean name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStructJsBean gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStructJsBean streetAddress, GeoPointStructJsBean geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws WebException
    {
        try {
            return getServiceProxy().createUser(managerApp, appAcl, GaeAppStructWebService.convertGaeAppStructJsBeanToBean(gaeApp), aeryId, sessionId, ancestorGuid, FullNameStructWebService.convertFullNameStructJsBeanToBean(name), usercode, username, nickname, avatar, email, openId, GaeUserStructWebService.convertGaeUserStructJsBeanToBean(gaeUser), entityType, surrogate, obsolete, timeZone, location, StreetAddressStructWebService.convertStreetAddressStructJsBeanToBean(streetAddress), GeoPointStructWebService.convertGeoPointStructJsBeanToBean(geoPoint), ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUser(UserJsBean jsBean) throws WebException
    {
        try {
            User user = convertUserJsBeanToBean(jsBean);
            return getServiceProxy().createUser(user);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserJsBean constructUser(UserJsBean jsBean) throws WebException
    {
        try {
            User user = convertUserJsBeanToBean(jsBean);
            user = getServiceProxy().constructUser(user);
            jsBean = convertUserToJsBean(user);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStructJsBean gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStructJsBean name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStructJsBean gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStructJsBean streetAddress, GeoPointStructJsBean geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws WebException
    {
        try {
            return getServiceProxy().updateUser(guid, managerApp, appAcl, GaeAppStructWebService.convertGaeAppStructJsBeanToBean(gaeApp), aeryId, sessionId, ancestorGuid, FullNameStructWebService.convertFullNameStructJsBeanToBean(name), usercode, username, nickname, avatar, email, openId, GaeUserStructWebService.convertGaeUserStructJsBeanToBean(gaeUser), entityType, surrogate, obsolete, timeZone, location, StreetAddressStructWebService.convertStreetAddressStructJsBeanToBean(streetAddress), GeoPointStructWebService.convertGeoPointStructJsBeanToBean(geoPoint), ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUser(UserJsBean jsBean) throws WebException
    {
        try {
            User user = convertUserJsBeanToBean(jsBean);
            return getServiceProxy().updateUser(user);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserJsBean refreshUser(UserJsBean jsBean) throws WebException
    {
        try {
            User user = convertUserJsBeanToBean(jsBean);
            user = getServiceProxy().refreshUser(user);
            jsBean = convertUserToJsBean(user);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUser(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteUser(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUser(UserJsBean jsBean) throws WebException
    {
        try {
            User user = convertUserJsBeanToBean(jsBean);
            return getServiceProxy().deleteUser(user);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUsers(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteUsers(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static UserJsBean convertUserToJsBean(User user)
    {
        UserJsBean jsBean = null;
        if(user != null) {
            jsBean = new UserJsBean();
            jsBean.setGuid(user.getGuid());
            jsBean.setManagerApp(user.getManagerApp());
            jsBean.setAppAcl(user.getAppAcl());
            jsBean.setGaeApp(GaeAppStructWebService.convertGaeAppStructToJsBean(user.getGaeApp()));
            jsBean.setAeryId(user.getAeryId());
            jsBean.setSessionId(user.getSessionId());
            jsBean.setAncestorGuid(user.getAncestorGuid());
            jsBean.setName(FullNameStructWebService.convertFullNameStructToJsBean(user.getName()));
            jsBean.setUsercode(user.getUsercode());
            jsBean.setUsername(user.getUsername());
            jsBean.setNickname(user.getNickname());
            jsBean.setAvatar(user.getAvatar());
            jsBean.setEmail(user.getEmail());
            jsBean.setOpenId(user.getOpenId());
            jsBean.setGaeUser(GaeUserStructWebService.convertGaeUserStructToJsBean(user.getGaeUser()));
            jsBean.setEntityType(user.getEntityType());
            jsBean.setSurrogate(user.isSurrogate());
            jsBean.setObsolete(user.isObsolete());
            jsBean.setTimeZone(user.getTimeZone());
            jsBean.setLocation(user.getLocation());
            jsBean.setStreetAddress(StreetAddressStructWebService.convertStreetAddressStructToJsBean(user.getStreetAddress()));
            jsBean.setGeoPoint(GeoPointStructWebService.convertGeoPointStructToJsBean(user.getGeoPoint()));
            jsBean.setIpAddress(user.getIpAddress());
            jsBean.setReferer(user.getReferer());
            jsBean.setStatus(user.getStatus());
            jsBean.setEmailVerifiedTime(user.getEmailVerifiedTime());
            jsBean.setOpenIdVerifiedTime(user.getOpenIdVerifiedTime());
            jsBean.setAuthenticatedTime(user.getAuthenticatedTime());
            jsBean.setCreatedTime(user.getCreatedTime());
            jsBean.setModifiedTime(user.getModifiedTime());
        }
        return jsBean;
    }

    public static User convertUserJsBeanToBean(UserJsBean jsBean)
    {
        UserBean user = null;
        if(jsBean != null) {
            user = new UserBean();
            user.setGuid(jsBean.getGuid());
            user.setManagerApp(jsBean.getManagerApp());
            user.setAppAcl(jsBean.getAppAcl());
            user.setGaeApp(GaeAppStructWebService.convertGaeAppStructJsBeanToBean(jsBean.getGaeApp()));
            user.setAeryId(jsBean.getAeryId());
            user.setSessionId(jsBean.getSessionId());
            user.setAncestorGuid(jsBean.getAncestorGuid());
            user.setName(FullNameStructWebService.convertFullNameStructJsBeanToBean(jsBean.getName()));
            user.setUsercode(jsBean.getUsercode());
            user.setUsername(jsBean.getUsername());
            user.setNickname(jsBean.getNickname());
            user.setAvatar(jsBean.getAvatar());
            user.setEmail(jsBean.getEmail());
            user.setOpenId(jsBean.getOpenId());
            user.setGaeUser(GaeUserStructWebService.convertGaeUserStructJsBeanToBean(jsBean.getGaeUser()));
            user.setEntityType(jsBean.getEntityType());
            user.setSurrogate(jsBean.isSurrogate());
            user.setObsolete(jsBean.isObsolete());
            user.setTimeZone(jsBean.getTimeZone());
            user.setLocation(jsBean.getLocation());
            user.setStreetAddress(StreetAddressStructWebService.convertStreetAddressStructJsBeanToBean(jsBean.getStreetAddress()));
            user.setGeoPoint(GeoPointStructWebService.convertGeoPointStructJsBeanToBean(jsBean.getGeoPoint()));
            user.setIpAddress(jsBean.getIpAddress());
            user.setReferer(jsBean.getReferer());
            user.setStatus(jsBean.getStatus());
            user.setEmailVerifiedTime(jsBean.getEmailVerifiedTime());
            user.setOpenIdVerifiedTime(jsBean.getOpenIdVerifiedTime());
            user.setAuthenticatedTime(jsBean.getAuthenticatedTime());
            user.setCreatedTime(jsBean.getCreatedTime());
            user.setModifiedTime(jsBean.getModifiedTime());
        }
        return user;
    }

}
