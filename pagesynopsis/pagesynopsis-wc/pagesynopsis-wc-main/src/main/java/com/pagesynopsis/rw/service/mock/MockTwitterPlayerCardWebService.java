package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.service.TwitterPlayerCardService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.fe.bean.TwitterPlayerCardJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.TwitterPlayerCardWebService;


// MockTwitterPlayerCardWebService is a mock object.
// It can be used as a base class to mock TwitterPlayerCardWebService objects.
public abstract class MockTwitterPlayerCardWebService extends TwitterPlayerCardWebService  // implements TwitterPlayerCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterPlayerCardWebService.class.getName());
     
    // Af service interface.
    private TwitterPlayerCardWebService mService = null;

    public MockTwitterPlayerCardWebService()
    {
        this(MockServiceProxyFactory.getInstance().getTwitterPlayerCardServiceProxy());
    }
    public MockTwitterPlayerCardWebService(TwitterPlayerCardService service)
    {
        super(service);
    }


}
