package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.af.bean.PageFetchBean;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.fe.bean.TwitterProductCardJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.fe.bean.TwitterPlayerCardJsBean;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.fe.bean.PageFetchJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.PageFetchWebService;


// MockPageFetchWebService is a mock object.
// It can be used as a base class to mock PageFetchWebService objects.
public abstract class MockPageFetchWebService extends PageFetchWebService  // implements PageFetchService
{
    private static final Logger log = Logger.getLogger(MockPageFetchWebService.class.getName());
     
    // Af service interface.
    private PageFetchWebService mService = null;

    public MockPageFetchWebService()
    {
        this(MockServiceProxyFactory.getInstance().getPageFetchServiceProxy());
    }
    public MockPageFetchWebService(PageFetchService service)
    {
        super(service);
    }


}
