package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.TwitterCardAppInfoWebService;


// MockTwitterCardAppInfoWebService is a mock object.
// It can be used as a base class to mock TwitterCardAppInfoWebService objects.
public abstract class MockTwitterCardAppInfoWebService extends TwitterCardAppInfoWebService  // implements TwitterCardAppInfoService
{
    private static final Logger log = Logger.getLogger(MockTwitterCardAppInfoWebService.class.getName());
     

}
