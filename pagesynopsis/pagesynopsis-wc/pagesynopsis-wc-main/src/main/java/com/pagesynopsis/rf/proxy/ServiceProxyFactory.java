package com.pagesynopsis.rf.proxy;

import java.util.logging.Logger;
import java.util.logging.Level;


public class ServiceProxyFactory
{
    private static final Logger log = Logger.getLogger(ServiceProxyFactory.class.getName());

    private UserServiceProxy userService = null;
    private RobotsTextFileServiceProxy robotsTextFileService = null;
    private RobotsTextRefreshServiceProxy robotsTextRefreshService = null;
    private OgProfileServiceProxy ogProfileService = null;
    private OgWebsiteServiceProxy ogWebsiteService = null;
    private OgBlogServiceProxy ogBlogService = null;
    private OgArticleServiceProxy ogArticleService = null;
    private OgBookServiceProxy ogBookService = null;
    private OgVideoServiceProxy ogVideoService = null;
    private OgMovieServiceProxy ogMovieService = null;
    private OgTvShowServiceProxy ogTvShowService = null;
    private OgTvEpisodeServiceProxy ogTvEpisodeService = null;
    private TwitterSummaryCardServiceProxy twitterSummaryCardService = null;
    private TwitterPhotoCardServiceProxy twitterPhotoCardService = null;
    private TwitterGalleryCardServiceProxy twitterGalleryCardService = null;
    private TwitterAppCardServiceProxy twitterAppCardService = null;
    private TwitterPlayerCardServiceProxy twitterPlayerCardService = null;
    private TwitterProductCardServiceProxy twitterProductCardService = null;
    private FetchRequestServiceProxy fetchRequestService = null;
    private PageInfoServiceProxy pageInfoService = null;
    private PageFetchServiceProxy pageFetchService = null;
    private LinkListServiceProxy linkListService = null;
    private ImageSetServiceProxy imageSetService = null;
    private AudioSetServiceProxy audioSetService = null;
    private VideoSetServiceProxy videoSetService = null;
    private OpenGraphMetaServiceProxy openGraphMetaService = null;
    private TwitterCardMetaServiceProxy twitterCardMetaService = null;
    private DomainInfoServiceProxy domainInfoService = null;
    private UrlRatingServiceProxy urlRatingService = null;
    private ServiceInfoServiceProxy serviceInfoService = null;
    private FiveTenServiceProxy fiveTenService = null;

    protected ServiceProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ServiceProxyFactoryHolder
    {
        private static final ServiceProxyFactory INSTANCE = new ServiceProxyFactory();
    }

    // Singleton method
    public static ServiceProxyFactory getInstance()
    {
        return ServiceProxyFactoryHolder.INSTANCE;
    }

    public UserServiceProxy getUserServiceProxy()
    {
        if(userService == null) {
            userService = new UserServiceProxy();
        }
        return userService;
    }

    public RobotsTextFileServiceProxy getRobotsTextFileServiceProxy()
    {
        if(robotsTextFileService == null) {
            robotsTextFileService = new RobotsTextFileServiceProxy();
        }
        return robotsTextFileService;
    }

    public RobotsTextRefreshServiceProxy getRobotsTextRefreshServiceProxy()
    {
        if(robotsTextRefreshService == null) {
            robotsTextRefreshService = new RobotsTextRefreshServiceProxy();
        }
        return robotsTextRefreshService;
    }

    public OgProfileServiceProxy getOgProfileServiceProxy()
    {
        if(ogProfileService == null) {
            ogProfileService = new OgProfileServiceProxy();
        }
        return ogProfileService;
    }

    public OgWebsiteServiceProxy getOgWebsiteServiceProxy()
    {
        if(ogWebsiteService == null) {
            ogWebsiteService = new OgWebsiteServiceProxy();
        }
        return ogWebsiteService;
    }

    public OgBlogServiceProxy getOgBlogServiceProxy()
    {
        if(ogBlogService == null) {
            ogBlogService = new OgBlogServiceProxy();
        }
        return ogBlogService;
    }

    public OgArticleServiceProxy getOgArticleServiceProxy()
    {
        if(ogArticleService == null) {
            ogArticleService = new OgArticleServiceProxy();
        }
        return ogArticleService;
    }

    public OgBookServiceProxy getOgBookServiceProxy()
    {
        if(ogBookService == null) {
            ogBookService = new OgBookServiceProxy();
        }
        return ogBookService;
    }

    public OgVideoServiceProxy getOgVideoServiceProxy()
    {
        if(ogVideoService == null) {
            ogVideoService = new OgVideoServiceProxy();
        }
        return ogVideoService;
    }

    public OgMovieServiceProxy getOgMovieServiceProxy()
    {
        if(ogMovieService == null) {
            ogMovieService = new OgMovieServiceProxy();
        }
        return ogMovieService;
    }

    public OgTvShowServiceProxy getOgTvShowServiceProxy()
    {
        if(ogTvShowService == null) {
            ogTvShowService = new OgTvShowServiceProxy();
        }
        return ogTvShowService;
    }

    public OgTvEpisodeServiceProxy getOgTvEpisodeServiceProxy()
    {
        if(ogTvEpisodeService == null) {
            ogTvEpisodeService = new OgTvEpisodeServiceProxy();
        }
        return ogTvEpisodeService;
    }

    public TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy()
    {
        if(twitterSummaryCardService == null) {
            twitterSummaryCardService = new TwitterSummaryCardServiceProxy();
        }
        return twitterSummaryCardService;
    }

    public TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy()
    {
        if(twitterPhotoCardService == null) {
            twitterPhotoCardService = new TwitterPhotoCardServiceProxy();
        }
        return twitterPhotoCardService;
    }

    public TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy()
    {
        if(twitterGalleryCardService == null) {
            twitterGalleryCardService = new TwitterGalleryCardServiceProxy();
        }
        return twitterGalleryCardService;
    }

    public TwitterAppCardServiceProxy getTwitterAppCardServiceProxy()
    {
        if(twitterAppCardService == null) {
            twitterAppCardService = new TwitterAppCardServiceProxy();
        }
        return twitterAppCardService;
    }

    public TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy()
    {
        if(twitterPlayerCardService == null) {
            twitterPlayerCardService = new TwitterPlayerCardServiceProxy();
        }
        return twitterPlayerCardService;
    }

    public TwitterProductCardServiceProxy getTwitterProductCardServiceProxy()
    {
        if(twitterProductCardService == null) {
            twitterProductCardService = new TwitterProductCardServiceProxy();
        }
        return twitterProductCardService;
    }

    public FetchRequestServiceProxy getFetchRequestServiceProxy()
    {
        if(fetchRequestService == null) {
            fetchRequestService = new FetchRequestServiceProxy();
        }
        return fetchRequestService;
    }

    public PageInfoServiceProxy getPageInfoServiceProxy()
    {
        if(pageInfoService == null) {
            pageInfoService = new PageInfoServiceProxy();
        }
        return pageInfoService;
    }

    public PageFetchServiceProxy getPageFetchServiceProxy()
    {
        if(pageFetchService == null) {
            pageFetchService = new PageFetchServiceProxy();
        }
        return pageFetchService;
    }

    public LinkListServiceProxy getLinkListServiceProxy()
    {
        if(linkListService == null) {
            linkListService = new LinkListServiceProxy();
        }
        return linkListService;
    }

    public ImageSetServiceProxy getImageSetServiceProxy()
    {
        if(imageSetService == null) {
            imageSetService = new ImageSetServiceProxy();
        }
        return imageSetService;
    }

    public AudioSetServiceProxy getAudioSetServiceProxy()
    {
        if(audioSetService == null) {
            audioSetService = new AudioSetServiceProxy();
        }
        return audioSetService;
    }

    public VideoSetServiceProxy getVideoSetServiceProxy()
    {
        if(videoSetService == null) {
            videoSetService = new VideoSetServiceProxy();
        }
        return videoSetService;
    }

    public OpenGraphMetaServiceProxy getOpenGraphMetaServiceProxy()
    {
        if(openGraphMetaService == null) {
            openGraphMetaService = new OpenGraphMetaServiceProxy();
        }
        return openGraphMetaService;
    }

    public TwitterCardMetaServiceProxy getTwitterCardMetaServiceProxy()
    {
        if(twitterCardMetaService == null) {
            twitterCardMetaService = new TwitterCardMetaServiceProxy();
        }
        return twitterCardMetaService;
    }

    public DomainInfoServiceProxy getDomainInfoServiceProxy()
    {
        if(domainInfoService == null) {
            domainInfoService = new DomainInfoServiceProxy();
        }
        return domainInfoService;
    }

    public UrlRatingServiceProxy getUrlRatingServiceProxy()
    {
        if(urlRatingService == null) {
            urlRatingService = new UrlRatingServiceProxy();
        }
        return urlRatingService;
    }

    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new ServiceInfoServiceProxy();
        }
        return serviceInfoService;
    }

    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenService == null) {
            fiveTenService = new FiveTenServiceProxy();
        }
        return fiveTenService;
    }

}
