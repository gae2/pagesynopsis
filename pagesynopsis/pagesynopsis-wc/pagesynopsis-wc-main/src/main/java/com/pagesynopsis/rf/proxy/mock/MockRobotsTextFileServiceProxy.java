package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.RobotsTextGroupStub;
import com.pagesynopsis.ws.stub.RobotsTextGroupListStub;
import com.pagesynopsis.ws.stub.RobotsTextFileStub;
import com.pagesynopsis.ws.stub.RobotsTextFileListStub;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.RobotsTextFileServiceProxy;


// MockRobotsTextFileServiceProxy is a decorator.
// It can be used as a base class to mock RobotsTextFileService objects.
public abstract class MockRobotsTextFileServiceProxy extends RobotsTextFileServiceProxy implements RobotsTextFileService
{
    private static final Logger log = Logger.getLogger(MockRobotsTextFileServiceProxy.class.getName());

    // MockRobotsTextFileServiceProxy uses the decorator design pattern.
    private RobotsTextFileService decoratedProxy;

    public MockRobotsTextFileServiceProxy(RobotsTextFileService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected RobotsTextFileService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(RobotsTextFileService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public RobotsTextFile getRobotsTextFile(String guid) throws BaseException
    {
        return decoratedProxy.getRobotsTextFile(guid);
    }

    @Override
    public Object getRobotsTextFile(String guid, String field) throws BaseException
    {
        return decoratedProxy.getRobotsTextFile(guid, field);
    }

    @Override
    public List<RobotsTextFile> getRobotsTextFiles(List<String> guids) throws BaseException
    {
        return decoratedProxy.getRobotsTextFiles(guids);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles() throws BaseException
    {
        return getAllRobotsTextFiles(null, null, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextFiles(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllRobotsTextFiles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextFileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllRobotsTextFileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findRobotsTextFiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findRobotsTextFiles(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createRobotsTextFile(String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return decoratedProxy.createRobotsTextFile(siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public String createRobotsTextFile(RobotsTextFile bean) throws BaseException
    {
        return decoratedProxy.createRobotsTextFile(bean);
    }

    @Override
    public RobotsTextFile constructRobotsTextFile(RobotsTextFile bean) throws BaseException
    {
        return decoratedProxy.constructRobotsTextFile(bean);
    }

    @Override
    public Boolean updateRobotsTextFile(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return decoratedProxy.updateRobotsTextFile(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public Boolean updateRobotsTextFile(RobotsTextFile bean) throws BaseException
    {
        return decoratedProxy.updateRobotsTextFile(bean);
    }

    @Override
    public RobotsTextFile refreshRobotsTextFile(RobotsTextFile bean) throws BaseException
    {
        return decoratedProxy.refreshRobotsTextFile(bean);
    }

    @Override
    public Boolean deleteRobotsTextFile(String guid) throws BaseException
    {
        return decoratedProxy.deleteRobotsTextFile(guid);
    }

    @Override
    public Boolean deleteRobotsTextFile(RobotsTextFile bean) throws BaseException
    {
        return decoratedProxy.deleteRobotsTextFile(bean);
    }

    @Override
    public Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteRobotsTextFiles(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createRobotsTextFiles(List<RobotsTextFile> robotsTextFiles) throws BaseException
    {
        return decoratedProxy.createRobotsTextFiles(robotsTextFiles);
    }

    // TBD
    //@Override
    //public Boolean updateRobotsTextFiles(List<RobotsTextFile> robotsTextFiles) throws BaseException
    //{
    //}

}
