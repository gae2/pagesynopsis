package com.pagesynopsis.web.service;

import java.util.logging.Logger;

import com.pagesynopsis.af.service.ImageSetService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.ImageSetJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;
import com.pagesynopsis.rw.service.ImageSetWebService;
import com.pagesynopsis.web.proxy.ImageSetWebServiceProxy;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.ImageSet;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ImageSetProxyWebService
{
    private static final Logger log = Logger.getLogger(ImageSetProxyWebService.class.getName());
     
    // Af service interface.
    private ImageSetWebServiceProxy mServiceProxy = null;

    public ImageSetProxyWebService()
    {
        //this(new ImageSetWebServiceProxy());
        this(null);
    }
    public ImageSetProxyWebService(ImageSetWebServiceProxy serviceProxy)
    {
        mServiceProxy = serviceProxy;
    }
    
    private ImageSetWebServiceProxy getServiceProxy()
    {
        if(mServiceProxy == null) {
            mServiceProxy = new ImageSetWebServiceProxy();
        }
        return mServiceProxy;
    }
    

//  "Get" is better than "Find" for this: Get returns a full object, whereas Find returns only an object with "default fetch". 
    public ImageSetJsBean findImageSetByTargetUrl(String targetUrl, Boolean fetch) throws WebException
    {
        return findImageSetByTargetUrl(targetUrl, fetch, null);
    }
    public ImageSetJsBean findImageSetByTargetUrl(String targetUrl, Boolean fetch, Integer refresh) throws WebException
    {
        try {
            ImageSetJsBean jsBean = null;
            ImageSet imageSet = getServiceProxy().findImageSetByTargetUrl(targetUrl, fetch, refresh);
            if(imageSet != null) {
                jsBean = ImageSetWebService.convertImageSetToJsBean(imageSet);
            }
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    
}
