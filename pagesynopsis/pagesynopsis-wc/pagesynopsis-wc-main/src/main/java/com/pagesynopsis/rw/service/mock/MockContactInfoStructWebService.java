package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.ContactInfoStruct;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.ContactInfoStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.ContactInfoStructWebService;


// MockContactInfoStructWebService is a mock object.
// It can be used as a base class to mock ContactInfoStructWebService objects.
public abstract class MockContactInfoStructWebService extends ContactInfoStructWebService  // implements ContactInfoStructService
{
    private static final Logger log = Logger.getLogger(MockContactInfoStructWebService.class.getName());
     

}
