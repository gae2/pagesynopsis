package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.service.TwitterSummaryCardService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.TwitterSummaryCardWebService;


// MockTwitterSummaryCardWebService is a mock object.
// It can be used as a base class to mock TwitterSummaryCardWebService objects.
public abstract class MockTwitterSummaryCardWebService extends TwitterSummaryCardWebService  // implements TwitterSummaryCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterSummaryCardWebService.class.getName());
     
    // Af service interface.
    private TwitterSummaryCardWebService mService = null;

    public MockTwitterSummaryCardWebService()
    {
        this(MockServiceProxyFactory.getInstance().getTwitterSummaryCardServiceProxy());
    }
    public MockTwitterSummaryCardWebService(TwitterSummaryCardService service)
    {
        super(service);
    }


}
