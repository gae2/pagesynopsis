package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.MediaSourceStructJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.AudioStructWebService;


// MockAudioStructWebService is a mock object.
// It can be used as a base class to mock AudioStructWebService objects.
public abstract class MockAudioStructWebService extends AudioStructWebService  // implements AudioStructService
{
    private static final Logger log = Logger.getLogger(MockAudioStructWebService.class.getName());
     

}
