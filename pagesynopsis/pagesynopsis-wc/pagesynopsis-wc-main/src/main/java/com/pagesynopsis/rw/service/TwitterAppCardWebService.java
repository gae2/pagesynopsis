package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.service.TwitterAppCardService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterAppCardWebService // implements TwitterAppCardService
{
    private static final Logger log = Logger.getLogger(TwitterAppCardWebService.class.getName());
     
    // Af service interface.
    private TwitterAppCardService mService = null;

    public TwitterAppCardWebService()
    {
        this(ServiceProxyFactory.getInstance().getTwitterAppCardServiceProxy());
    }
    public TwitterAppCardWebService(TwitterAppCardService service)
    {
        mService = service;
    }
    
    protected TwitterAppCardService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getTwitterAppCardServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(TwitterAppCardService service)
    {
        mService = service;
    }
    
    
    public TwitterAppCardJsBean getTwitterAppCard(String guid) throws WebException
    {
        try {
            TwitterAppCard twitterAppCard = getServiceProxy().getTwitterAppCard(guid);
            TwitterAppCardJsBean bean = convertTwitterAppCardToJsBean(twitterAppCard);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTwitterAppCard(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getTwitterAppCard(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterAppCardJsBean> getTwitterAppCards(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterAppCardJsBean> jsBeans = new ArrayList<TwitterAppCardJsBean>();
            List<TwitterAppCard> twitterAppCards = getServiceProxy().getTwitterAppCards(guids);
            if(twitterAppCards != null) {
                for(TwitterAppCard twitterAppCard : twitterAppCards) {
                    jsBeans.add(convertTwitterAppCardToJsBean(twitterAppCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterAppCardJsBean> getAllTwitterAppCards() throws WebException
    {
        return getAllTwitterAppCards(null, null, null);
    }

    // @Deprecated
    public List<TwitterAppCardJsBean> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTwitterAppCards(ordering, offset, count, null);
    }

    public List<TwitterAppCardJsBean> getAllTwitterAppCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TwitterAppCardJsBean> jsBeans = new ArrayList<TwitterAppCardJsBean>();
            List<TwitterAppCard> twitterAppCards = getServiceProxy().getAllTwitterAppCards(ordering, offset, count, forwardCursor);
            if(twitterAppCards != null) {
                for(TwitterAppCard twitterAppCard : twitterAppCards) {
                    jsBeans.add(convertTwitterAppCardToJsBean(twitterAppCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTwitterAppCardKeys(ordering, offset, count, null);
    }

    public List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllTwitterAppCardKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<TwitterAppCardJsBean> findTwitterAppCards(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTwitterAppCards(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<TwitterAppCardJsBean> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<TwitterAppCardJsBean> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TwitterAppCardJsBean> jsBeans = new ArrayList<TwitterAppCardJsBean>();
            List<TwitterAppCard> twitterAppCards = getServiceProxy().findTwitterAppCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(twitterAppCards != null) {
                for(TwitterAppCard twitterAppCard : twitterAppCards) {
                    jsBeans.add(convertTwitterAppCardToJsBean(twitterAppCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findTwitterAppCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterAppCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfoJsBean iphoneApp, TwitterCardAppInfoJsBean ipadApp, TwitterCardAppInfoJsBean googlePlayApp) throws WebException
    {
        try {
            return getServiceProxy().createTwitterAppCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, TwitterCardAppInfoWebService.convertTwitterCardAppInfoJsBeanToBean(iphoneApp), TwitterCardAppInfoWebService.convertTwitterCardAppInfoJsBeanToBean(ipadApp), TwitterCardAppInfoWebService.convertTwitterCardAppInfoJsBeanToBean(googlePlayApp));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterAppCard(TwitterAppCardJsBean jsBean) throws WebException
    {
        try {
            TwitterAppCard twitterAppCard = convertTwitterAppCardJsBeanToBean(jsBean);
            return getServiceProxy().createTwitterAppCard(twitterAppCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterAppCardJsBean constructTwitterAppCard(TwitterAppCardJsBean jsBean) throws WebException
    {
        try {
            TwitterAppCard twitterAppCard = convertTwitterAppCardJsBeanToBean(jsBean);
            twitterAppCard = getServiceProxy().constructTwitterAppCard(twitterAppCard);
            jsBean = convertTwitterAppCardToJsBean(twitterAppCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTwitterAppCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardAppInfoJsBean iphoneApp, TwitterCardAppInfoJsBean ipadApp, TwitterCardAppInfoJsBean googlePlayApp) throws WebException
    {
        try {
            return getServiceProxy().updateTwitterAppCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, TwitterCardAppInfoWebService.convertTwitterCardAppInfoJsBeanToBean(iphoneApp), TwitterCardAppInfoWebService.convertTwitterCardAppInfoJsBeanToBean(ipadApp), TwitterCardAppInfoWebService.convertTwitterCardAppInfoJsBeanToBean(googlePlayApp));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTwitterAppCard(TwitterAppCardJsBean jsBean) throws WebException
    {
        try {
            TwitterAppCard twitterAppCard = convertTwitterAppCardJsBeanToBean(jsBean);
            return getServiceProxy().updateTwitterAppCard(twitterAppCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterAppCardJsBean refreshTwitterAppCard(TwitterAppCardJsBean jsBean) throws WebException
    {
        try {
            TwitterAppCard twitterAppCard = convertTwitterAppCardJsBeanToBean(jsBean);
            twitterAppCard = getServiceProxy().refreshTwitterAppCard(twitterAppCard);
            jsBean = convertTwitterAppCardToJsBean(twitterAppCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterAppCard(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteTwitterAppCard(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterAppCard(TwitterAppCardJsBean jsBean) throws WebException
    {
        try {
            TwitterAppCard twitterAppCard = convertTwitterAppCardJsBeanToBean(jsBean);
            return getServiceProxy().deleteTwitterAppCard(twitterAppCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTwitterAppCards(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteTwitterAppCards(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static TwitterAppCardJsBean convertTwitterAppCardToJsBean(TwitterAppCard twitterAppCard)
    {
        TwitterAppCardJsBean jsBean = null;
        if(twitterAppCard != null) {
            jsBean = new TwitterAppCardJsBean();
            jsBean.setGuid(twitterAppCard.getGuid());
            jsBean.setCard(twitterAppCard.getCard());
            jsBean.setUrl(twitterAppCard.getUrl());
            jsBean.setTitle(twitterAppCard.getTitle());
            jsBean.setDescription(twitterAppCard.getDescription());
            jsBean.setSite(twitterAppCard.getSite());
            jsBean.setSiteId(twitterAppCard.getSiteId());
            jsBean.setCreator(twitterAppCard.getCreator());
            jsBean.setCreatorId(twitterAppCard.getCreatorId());
            jsBean.setImage(twitterAppCard.getImage());
            jsBean.setImageWidth(twitterAppCard.getImageWidth());
            jsBean.setImageHeight(twitterAppCard.getImageHeight());
            jsBean.setIphoneApp(TwitterCardAppInfoWebService.convertTwitterCardAppInfoToJsBean(twitterAppCard.getIphoneApp()));
            jsBean.setIpadApp(TwitterCardAppInfoWebService.convertTwitterCardAppInfoToJsBean(twitterAppCard.getIpadApp()));
            jsBean.setGooglePlayApp(TwitterCardAppInfoWebService.convertTwitterCardAppInfoToJsBean(twitterAppCard.getGooglePlayApp()));
            jsBean.setCreatedTime(twitterAppCard.getCreatedTime());
            jsBean.setModifiedTime(twitterAppCard.getModifiedTime());
        }
        return jsBean;
    }

    public static TwitterAppCard convertTwitterAppCardJsBeanToBean(TwitterAppCardJsBean jsBean)
    {
        TwitterAppCardBean twitterAppCard = null;
        if(jsBean != null) {
            twitterAppCard = new TwitterAppCardBean();
            twitterAppCard.setGuid(jsBean.getGuid());
            twitterAppCard.setCard(jsBean.getCard());
            twitterAppCard.setUrl(jsBean.getUrl());
            twitterAppCard.setTitle(jsBean.getTitle());
            twitterAppCard.setDescription(jsBean.getDescription());
            twitterAppCard.setSite(jsBean.getSite());
            twitterAppCard.setSiteId(jsBean.getSiteId());
            twitterAppCard.setCreator(jsBean.getCreator());
            twitterAppCard.setCreatorId(jsBean.getCreatorId());
            twitterAppCard.setImage(jsBean.getImage());
            twitterAppCard.setImageWidth(jsBean.getImageWidth());
            twitterAppCard.setImageHeight(jsBean.getImageHeight());
            twitterAppCard.setIphoneApp(TwitterCardAppInfoWebService.convertTwitterCardAppInfoJsBeanToBean(jsBean.getIphoneApp()));
            twitterAppCard.setIpadApp(TwitterCardAppInfoWebService.convertTwitterCardAppInfoJsBeanToBean(jsBean.getIpadApp()));
            twitterAppCard.setGooglePlayApp(TwitterCardAppInfoWebService.convertTwitterCardAppInfoJsBeanToBean(jsBean.getGooglePlayApp()));
            twitterAppCard.setCreatedTime(jsBean.getCreatedTime());
            twitterAppCard.setModifiedTime(jsBean.getModifiedTime());
        }
        return twitterAppCard;
    }

}
