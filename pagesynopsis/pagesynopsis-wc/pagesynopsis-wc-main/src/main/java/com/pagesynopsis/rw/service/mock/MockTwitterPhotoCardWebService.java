package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.service.TwitterPhotoCardService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.TwitterPhotoCardWebService;


// MockTwitterPhotoCardWebService is a mock object.
// It can be used as a base class to mock TwitterPhotoCardWebService objects.
public abstract class MockTwitterPhotoCardWebService extends TwitterPhotoCardWebService  // implements TwitterPhotoCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterPhotoCardWebService.class.getName());
     
    // Af service interface.
    private TwitterPhotoCardWebService mService = null;

    public MockTwitterPhotoCardWebService()
    {
        this(MockServiceProxyFactory.getInstance().getTwitterPhotoCardServiceProxy());
    }
    public MockTwitterPhotoCardWebService(TwitterPhotoCardService service)
    {
        super(service);
    }


}
