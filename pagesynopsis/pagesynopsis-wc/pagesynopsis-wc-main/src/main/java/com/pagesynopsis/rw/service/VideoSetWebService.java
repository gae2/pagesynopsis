package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.af.bean.VideoSetBean;
import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.fe.bean.TwitterProductCardJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.fe.bean.TwitterPlayerCardJsBean;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.fe.bean.VideoSetJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class VideoSetWebService // implements VideoSetService
{
    private static final Logger log = Logger.getLogger(VideoSetWebService.class.getName());
     
    // Af service interface.
    private VideoSetService mService = null;

    public VideoSetWebService()
    {
        this(ServiceProxyFactory.getInstance().getVideoSetServiceProxy());
    }
    public VideoSetWebService(VideoSetService service)
    {
        mService = service;
    }
    
    protected VideoSetService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getVideoSetServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(VideoSetService service)
    {
        mService = service;
    }
    
    
    public VideoSetJsBean getVideoSet(String guid) throws WebException
    {
        try {
            VideoSet videoSet = getServiceProxy().getVideoSet(guid);
            VideoSetJsBean bean = convertVideoSetToJsBean(videoSet);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getVideoSet(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getVideoSet(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<VideoSetJsBean> getVideoSets(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<VideoSetJsBean> jsBeans = new ArrayList<VideoSetJsBean>();
            List<VideoSet> videoSets = getServiceProxy().getVideoSets(guids);
            if(videoSets != null) {
                for(VideoSet videoSet : videoSets) {
                    jsBeans.add(convertVideoSetToJsBean(videoSet));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<VideoSetJsBean> getAllVideoSets() throws WebException
    {
        return getAllVideoSets(null, null, null);
    }

    // @Deprecated
    public List<VideoSetJsBean> getAllVideoSets(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllVideoSets(ordering, offset, count, null);
    }

    public List<VideoSetJsBean> getAllVideoSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<VideoSetJsBean> jsBeans = new ArrayList<VideoSetJsBean>();
            List<VideoSet> videoSets = getServiceProxy().getAllVideoSets(ordering, offset, count, forwardCursor);
            if(videoSets != null) {
                for(VideoSet videoSet : videoSets) {
                    jsBeans.add(convertVideoSetToJsBean(videoSet));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllVideoSetKeys(ordering, offset, count, null);
    }

    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllVideoSetKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<VideoSetJsBean> findVideoSets(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findVideoSets(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<VideoSetJsBean> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findVideoSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<VideoSetJsBean> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<VideoSetJsBean> jsBeans = new ArrayList<VideoSetJsBean>();
            List<VideoSet> videoSets = getServiceProxy().findVideoSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(videoSets != null) {
                for(VideoSet videoSet : videoSets) {
                    jsBeans.add(convertVideoSetToJsBean(videoSet));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createVideoSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<VideoStruct> pageVideos) throws WebException
    {
        try {
            return getServiceProxy().createVideoSet(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageVideos);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createVideoSet(VideoSetJsBean jsBean) throws WebException
    {
        try {
            VideoSet videoSet = convertVideoSetJsBeanToBean(jsBean);
            return getServiceProxy().createVideoSet(videoSet);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public VideoSetJsBean constructVideoSet(VideoSetJsBean jsBean) throws WebException
    {
        try {
            VideoSet videoSet = convertVideoSetJsBeanToBean(jsBean);
            videoSet = getServiceProxy().constructVideoSet(videoSet);
            jsBean = convertVideoSetToJsBean(videoSet);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateVideoSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<VideoStruct> pageVideos) throws WebException
    {
        try {
            return getServiceProxy().updateVideoSet(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageVideos);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateVideoSet(VideoSetJsBean jsBean) throws WebException
    {
        try {
            VideoSet videoSet = convertVideoSetJsBeanToBean(jsBean);
            return getServiceProxy().updateVideoSet(videoSet);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public VideoSetJsBean refreshVideoSet(VideoSetJsBean jsBean) throws WebException
    {
        try {
            VideoSet videoSet = convertVideoSetJsBeanToBean(jsBean);
            videoSet = getServiceProxy().refreshVideoSet(videoSet);
            jsBean = convertVideoSetToJsBean(videoSet);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteVideoSet(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteVideoSet(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteVideoSet(VideoSetJsBean jsBean) throws WebException
    {
        try {
            VideoSet videoSet = convertVideoSetJsBeanToBean(jsBean);
            return getServiceProxy().deleteVideoSet(videoSet);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteVideoSets(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteVideoSets(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static VideoSetJsBean convertVideoSetToJsBean(VideoSet videoSet)
    {
        VideoSetJsBean jsBean = null;
        if(videoSet != null) {
            jsBean = new VideoSetJsBean();
            jsBean.setGuid(videoSet.getGuid());
            jsBean.setUser(videoSet.getUser());
            jsBean.setFetchRequest(videoSet.getFetchRequest());
            jsBean.setTargetUrl(videoSet.getTargetUrl());
            jsBean.setPageUrl(videoSet.getPageUrl());
            jsBean.setQueryString(videoSet.getQueryString());
            List<KeyValuePairStructJsBean> queryParamsJsBeans = new ArrayList<KeyValuePairStructJsBean>();
            List<KeyValuePairStruct> queryParamsBeans = videoSet.getQueryParams();
            if(queryParamsBeans != null) {
                for(KeyValuePairStruct keyValuePairStruct : queryParamsBeans) {
                    KeyValuePairStructJsBean jB = KeyValuePairStructWebService.convertKeyValuePairStructToJsBean(keyValuePairStruct);
                    queryParamsJsBeans.add(jB); 
                }
            }
            jsBean.setQueryParams(queryParamsJsBeans);
            jsBean.setLastFetchResult(videoSet.getLastFetchResult());
            jsBean.setResponseCode(videoSet.getResponseCode());
            jsBean.setContentType(videoSet.getContentType());
            jsBean.setContentLength(videoSet.getContentLength());
            jsBean.setLanguage(videoSet.getLanguage());
            jsBean.setRedirect(videoSet.getRedirect());
            jsBean.setLocation(videoSet.getLocation());
            jsBean.setPageTitle(videoSet.getPageTitle());
            jsBean.setNote(videoSet.getNote());
            jsBean.setDeferred(videoSet.isDeferred());
            jsBean.setStatus(videoSet.getStatus());
            jsBean.setRefreshStatus(videoSet.getRefreshStatus());
            jsBean.setRefreshInterval(videoSet.getRefreshInterval());
            jsBean.setNextRefreshTime(videoSet.getNextRefreshTime());
            jsBean.setLastCheckedTime(videoSet.getLastCheckedTime());
            jsBean.setLastUpdatedTime(videoSet.getLastUpdatedTime());
            jsBean.setMediaTypeFilter(videoSet.getMediaTypeFilter());
            Set<VideoStructJsBean> pageVideosJsBeans = new HashSet<VideoStructJsBean>();
            Set<VideoStruct> pageVideosBeans = videoSet.getPageVideos();
            if(pageVideosBeans != null) {
                for(VideoStruct videoStruct : pageVideosBeans) {
                    VideoStructJsBean jB = VideoStructWebService.convertVideoStructToJsBean(videoStruct);
                    pageVideosJsBeans.add(jB); 
                }
            }
            jsBean.setPageVideos(pageVideosJsBeans);
            jsBean.setCreatedTime(videoSet.getCreatedTime());
            jsBean.setModifiedTime(videoSet.getModifiedTime());
        }
        return jsBean;
    }

    public static VideoSet convertVideoSetJsBeanToBean(VideoSetJsBean jsBean)
    {
        VideoSetBean videoSet = null;
        if(jsBean != null) {
            videoSet = new VideoSetBean();
            videoSet.setGuid(jsBean.getGuid());
            videoSet.setUser(jsBean.getUser());
            videoSet.setFetchRequest(jsBean.getFetchRequest());
            videoSet.setTargetUrl(jsBean.getTargetUrl());
            videoSet.setPageUrl(jsBean.getPageUrl());
            videoSet.setQueryString(jsBean.getQueryString());
            List<KeyValuePairStruct> queryParamsBeans = new ArrayList<KeyValuePairStruct>();
            List<KeyValuePairStructJsBean> queryParamsJsBeans = jsBean.getQueryParams();
            if(queryParamsJsBeans != null) {
                for(KeyValuePairStructJsBean keyValuePairStruct : queryParamsJsBeans) {
                    KeyValuePairStruct b = KeyValuePairStructWebService.convertKeyValuePairStructJsBeanToBean(keyValuePairStruct);
                    queryParamsBeans.add(b); 
                }
            }
            videoSet.setQueryParams(queryParamsBeans);
            videoSet.setLastFetchResult(jsBean.getLastFetchResult());
            videoSet.setResponseCode(jsBean.getResponseCode());
            videoSet.setContentType(jsBean.getContentType());
            videoSet.setContentLength(jsBean.getContentLength());
            videoSet.setLanguage(jsBean.getLanguage());
            videoSet.setRedirect(jsBean.getRedirect());
            videoSet.setLocation(jsBean.getLocation());
            videoSet.setPageTitle(jsBean.getPageTitle());
            videoSet.setNote(jsBean.getNote());
            videoSet.setDeferred(jsBean.isDeferred());
            videoSet.setStatus(jsBean.getStatus());
            videoSet.setRefreshStatus(jsBean.getRefreshStatus());
            videoSet.setRefreshInterval(jsBean.getRefreshInterval());
            videoSet.setNextRefreshTime(jsBean.getNextRefreshTime());
            videoSet.setLastCheckedTime(jsBean.getLastCheckedTime());
            videoSet.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            videoSet.setMediaTypeFilter(jsBean.getMediaTypeFilter());
            Set<VideoStruct> pageVideosBeans = new HashSet<VideoStruct>();
            Set<VideoStructJsBean> pageVideosJsBeans = jsBean.getPageVideos();
            if(pageVideosJsBeans != null) {
                for(VideoStructJsBean videoStruct : pageVideosJsBeans) {
                    VideoStruct b = VideoStructWebService.convertVideoStructJsBeanToBean(videoStruct);
                    pageVideosBeans.add(b); 
                }
            }
            videoSet.setPageVideos(pageVideosBeans);
            videoSet.setCreatedTime(jsBean.getCreatedTime());
            videoSet.setModifiedTime(jsBean.getModifiedTime());
        }
        return videoSet;
    }

}
