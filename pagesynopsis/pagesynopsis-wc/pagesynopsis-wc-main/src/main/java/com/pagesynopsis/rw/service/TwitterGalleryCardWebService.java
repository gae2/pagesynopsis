package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.service.TwitterGalleryCardService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterGalleryCardWebService // implements TwitterGalleryCardService
{
    private static final Logger log = Logger.getLogger(TwitterGalleryCardWebService.class.getName());
     
    // Af service interface.
    private TwitterGalleryCardService mService = null;

    public TwitterGalleryCardWebService()
    {
        this(ServiceProxyFactory.getInstance().getTwitterGalleryCardServiceProxy());
    }
    public TwitterGalleryCardWebService(TwitterGalleryCardService service)
    {
        mService = service;
    }
    
    protected TwitterGalleryCardService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getTwitterGalleryCardServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(TwitterGalleryCardService service)
    {
        mService = service;
    }
    
    
    public TwitterGalleryCardJsBean getTwitterGalleryCard(String guid) throws WebException
    {
        try {
            TwitterGalleryCard twitterGalleryCard = getServiceProxy().getTwitterGalleryCard(guid);
            TwitterGalleryCardJsBean bean = convertTwitterGalleryCardToJsBean(twitterGalleryCard);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTwitterGalleryCard(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getTwitterGalleryCard(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterGalleryCardJsBean> getTwitterGalleryCards(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterGalleryCardJsBean> jsBeans = new ArrayList<TwitterGalleryCardJsBean>();
            List<TwitterGalleryCard> twitterGalleryCards = getServiceProxy().getTwitterGalleryCards(guids);
            if(twitterGalleryCards != null) {
                for(TwitterGalleryCard twitterGalleryCard : twitterGalleryCards) {
                    jsBeans.add(convertTwitterGalleryCardToJsBean(twitterGalleryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterGalleryCardJsBean> getAllTwitterGalleryCards() throws WebException
    {
        return getAllTwitterGalleryCards(null, null, null);
    }

    // @Deprecated
    public List<TwitterGalleryCardJsBean> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTwitterGalleryCards(ordering, offset, count, null);
    }

    public List<TwitterGalleryCardJsBean> getAllTwitterGalleryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TwitterGalleryCardJsBean> jsBeans = new ArrayList<TwitterGalleryCardJsBean>();
            List<TwitterGalleryCard> twitterGalleryCards = getServiceProxy().getAllTwitterGalleryCards(ordering, offset, count, forwardCursor);
            if(twitterGalleryCards != null) {
                for(TwitterGalleryCard twitterGalleryCard : twitterGalleryCards) {
                    jsBeans.add(convertTwitterGalleryCardToJsBean(twitterGalleryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTwitterGalleryCardKeys(ordering, offset, count, null);
    }

    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllTwitterGalleryCardKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<TwitterGalleryCardJsBean> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<TwitterGalleryCardJsBean> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<TwitterGalleryCardJsBean> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TwitterGalleryCardJsBean> jsBeans = new ArrayList<TwitterGalleryCardJsBean>();
            List<TwitterGalleryCard> twitterGalleryCards = getServiceProxy().findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(twitterGalleryCards != null) {
                for(TwitterGalleryCard twitterGalleryCard : twitterGalleryCards) {
                    jsBeans.add(convertTwitterGalleryCardToJsBean(twitterGalleryCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterGalleryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws WebException
    {
        try {
            return getServiceProxy().createTwitterGalleryCard(card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterGalleryCard(TwitterGalleryCardJsBean jsBean) throws WebException
    {
        try {
            TwitterGalleryCard twitterGalleryCard = convertTwitterGalleryCardJsBeanToBean(jsBean);
            return getServiceProxy().createTwitterGalleryCard(twitterGalleryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterGalleryCardJsBean constructTwitterGalleryCard(TwitterGalleryCardJsBean jsBean) throws WebException
    {
        try {
            TwitterGalleryCard twitterGalleryCard = convertTwitterGalleryCardJsBeanToBean(jsBean);
            twitterGalleryCard = getServiceProxy().constructTwitterGalleryCard(twitterGalleryCard);
            jsBean = convertTwitterGalleryCardToJsBean(twitterGalleryCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws WebException
    {
        try {
            return getServiceProxy().updateTwitterGalleryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTwitterGalleryCard(TwitterGalleryCardJsBean jsBean) throws WebException
    {
        try {
            TwitterGalleryCard twitterGalleryCard = convertTwitterGalleryCardJsBeanToBean(jsBean);
            return getServiceProxy().updateTwitterGalleryCard(twitterGalleryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterGalleryCardJsBean refreshTwitterGalleryCard(TwitterGalleryCardJsBean jsBean) throws WebException
    {
        try {
            TwitterGalleryCard twitterGalleryCard = convertTwitterGalleryCardJsBeanToBean(jsBean);
            twitterGalleryCard = getServiceProxy().refreshTwitterGalleryCard(twitterGalleryCard);
            jsBean = convertTwitterGalleryCardToJsBean(twitterGalleryCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterGalleryCard(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteTwitterGalleryCard(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterGalleryCard(TwitterGalleryCardJsBean jsBean) throws WebException
    {
        try {
            TwitterGalleryCard twitterGalleryCard = convertTwitterGalleryCardJsBeanToBean(jsBean);
            return getServiceProxy().deleteTwitterGalleryCard(twitterGalleryCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteTwitterGalleryCards(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static TwitterGalleryCardJsBean convertTwitterGalleryCardToJsBean(TwitterGalleryCard twitterGalleryCard)
    {
        TwitterGalleryCardJsBean jsBean = null;
        if(twitterGalleryCard != null) {
            jsBean = new TwitterGalleryCardJsBean();
            jsBean.setGuid(twitterGalleryCard.getGuid());
            jsBean.setCard(twitterGalleryCard.getCard());
            jsBean.setUrl(twitterGalleryCard.getUrl());
            jsBean.setTitle(twitterGalleryCard.getTitle());
            jsBean.setDescription(twitterGalleryCard.getDescription());
            jsBean.setSite(twitterGalleryCard.getSite());
            jsBean.setSiteId(twitterGalleryCard.getSiteId());
            jsBean.setCreator(twitterGalleryCard.getCreator());
            jsBean.setCreatorId(twitterGalleryCard.getCreatorId());
            jsBean.setImage0(twitterGalleryCard.getImage0());
            jsBean.setImage1(twitterGalleryCard.getImage1());
            jsBean.setImage2(twitterGalleryCard.getImage2());
            jsBean.setImage3(twitterGalleryCard.getImage3());
            jsBean.setCreatedTime(twitterGalleryCard.getCreatedTime());
            jsBean.setModifiedTime(twitterGalleryCard.getModifiedTime());
        }
        return jsBean;
    }

    public static TwitterGalleryCard convertTwitterGalleryCardJsBeanToBean(TwitterGalleryCardJsBean jsBean)
    {
        TwitterGalleryCardBean twitterGalleryCard = null;
        if(jsBean != null) {
            twitterGalleryCard = new TwitterGalleryCardBean();
            twitterGalleryCard.setGuid(jsBean.getGuid());
            twitterGalleryCard.setCard(jsBean.getCard());
            twitterGalleryCard.setUrl(jsBean.getUrl());
            twitterGalleryCard.setTitle(jsBean.getTitle());
            twitterGalleryCard.setDescription(jsBean.getDescription());
            twitterGalleryCard.setSite(jsBean.getSite());
            twitterGalleryCard.setSiteId(jsBean.getSiteId());
            twitterGalleryCard.setCreator(jsBean.getCreator());
            twitterGalleryCard.setCreatorId(jsBean.getCreatorId());
            twitterGalleryCard.setImage0(jsBean.getImage0());
            twitterGalleryCard.setImage1(jsBean.getImage1());
            twitterGalleryCard.setImage2(jsBean.getImage2());
            twitterGalleryCard.setImage3(jsBean.getImage3());
            twitterGalleryCard.setCreatedTime(jsBean.getCreatedTime());
            twitterGalleryCard.setModifiedTime(jsBean.getModifiedTime());
        }
        return twitterGalleryCard;
    }

}
