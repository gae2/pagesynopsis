package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgTvEpisodeWebService;


// MockOgTvEpisodeWebService is a mock object.
// It can be used as a base class to mock OgTvEpisodeWebService objects.
public abstract class MockOgTvEpisodeWebService extends OgTvEpisodeWebService  // implements OgTvEpisodeService
{
    private static final Logger log = Logger.getLogger(MockOgTvEpisodeWebService.class.getName());
     
    // Af service interface.
    private OgTvEpisodeWebService mService = null;

    public MockOgTvEpisodeWebService()
    {
        this(MockServiceProxyFactory.getInstance().getOgTvEpisodeServiceProxy());
    }
    public MockOgTvEpisodeWebService(OgTvEpisodeService service)
    {
        super(service);
    }


}
