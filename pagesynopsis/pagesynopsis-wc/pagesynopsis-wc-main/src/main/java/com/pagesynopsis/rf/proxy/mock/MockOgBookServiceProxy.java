package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgAudioStructListStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgImageStructListStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgActorStructListStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructListStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgBookListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.OgBookServiceProxy;


// MockOgBookServiceProxy is a decorator.
// It can be used as a base class to mock OgBookService objects.
public abstract class MockOgBookServiceProxy extends OgBookServiceProxy implements OgBookService
{
    private static final Logger log = Logger.getLogger(MockOgBookServiceProxy.class.getName());

    // MockOgBookServiceProxy uses the decorator design pattern.
    private OgBookService decoratedProxy;

    public MockOgBookServiceProxy(OgBookService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected OgBookService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(OgBookService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public OgBook getOgBook(String guid) throws BaseException
    {
        return decoratedProxy.getOgBook(guid);
    }

    @Override
    public Object getOgBook(String guid, String field) throws BaseException
    {
        return decoratedProxy.getOgBook(guid, field);
    }

    @Override
    public List<OgBook> getOgBooks(List<String> guids) throws BaseException
    {
        return decoratedProxy.getOgBooks(guids);
    }

    @Override
    public List<OgBook> getAllOgBooks() throws BaseException
    {
        return getAllOgBooks(null, null, null);
    }

    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBooks(ordering, offset, count, null);
    }

    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgBooks(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBookKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgBookKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgBooks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgBooks(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgBookKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgBook(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedProxy.createOgBook(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
    }

    @Override
    public String createOgBook(OgBook bean) throws BaseException
    {
        return decoratedProxy.createOgBook(bean);
    }

    @Override
    public OgBook constructOgBook(OgBook bean) throws BaseException
    {
        return decoratedProxy.constructOgBook(bean);
    }

    @Override
    public Boolean updateOgBook(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException
    {
        return decoratedProxy.updateOgBook(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
    }

    @Override
    public Boolean updateOgBook(OgBook bean) throws BaseException
    {
        return decoratedProxy.updateOgBook(bean);
    }

    @Override
    public OgBook refreshOgBook(OgBook bean) throws BaseException
    {
        return decoratedProxy.refreshOgBook(bean);
    }

    @Override
    public Boolean deleteOgBook(String guid) throws BaseException
    {
        return decoratedProxy.deleteOgBook(guid);
    }

    @Override
    public Boolean deleteOgBook(OgBook bean) throws BaseException
    {
        return decoratedProxy.deleteOgBook(bean);
    }

    @Override
    public Long deleteOgBooks(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteOgBooks(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgBooks(List<OgBook> ogBooks) throws BaseException
    {
        return decoratedProxy.createOgBooks(ogBooks);
    }

    // TBD
    //@Override
    //public Boolean updateOgBooks(List<OgBook> ogBooks) throws BaseException
    //{
    //}

}
