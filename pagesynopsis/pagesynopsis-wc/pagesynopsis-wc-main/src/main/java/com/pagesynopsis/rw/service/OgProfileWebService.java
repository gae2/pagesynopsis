package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.service.OgProfileService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgProfileWebService // implements OgProfileService
{
    private static final Logger log = Logger.getLogger(OgProfileWebService.class.getName());
     
    // Af service interface.
    private OgProfileService mService = null;

    public OgProfileWebService()
    {
        this(ServiceProxyFactory.getInstance().getOgProfileServiceProxy());
    }
    public OgProfileWebService(OgProfileService service)
    {
        mService = service;
    }
    
    protected OgProfileService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getOgProfileServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(OgProfileService service)
    {
        mService = service;
    }
    
    
    public OgProfileJsBean getOgProfile(String guid) throws WebException
    {
        try {
            OgProfile ogProfile = getServiceProxy().getOgProfile(guid);
            OgProfileJsBean bean = convertOgProfileToJsBean(ogProfile);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOgProfile(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getOgProfile(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgProfileJsBean> getOgProfiles(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgProfileJsBean> jsBeans = new ArrayList<OgProfileJsBean>();
            List<OgProfile> ogProfiles = getServiceProxy().getOgProfiles(guids);
            if(ogProfiles != null) {
                for(OgProfile ogProfile : ogProfiles) {
                    jsBeans.add(convertOgProfileToJsBean(ogProfile));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgProfileJsBean> getAllOgProfiles() throws WebException
    {
        return getAllOgProfiles(null, null, null);
    }

    // @Deprecated
    public List<OgProfileJsBean> getAllOgProfiles(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgProfiles(ordering, offset, count, null);
    }

    public List<OgProfileJsBean> getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgProfileJsBean> jsBeans = new ArrayList<OgProfileJsBean>();
            List<OgProfile> ogProfiles = getServiceProxy().getAllOgProfiles(ordering, offset, count, forwardCursor);
            if(ogProfiles != null) {
                for(OgProfile ogProfile : ogProfiles) {
                    jsBeans.add(convertOgProfileToJsBean(ogProfile));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgProfileKeys(ordering, offset, count, null);
    }

    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllOgProfileKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OgProfileJsBean> findOgProfiles(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOgProfiles(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OgProfileJsBean> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OgProfileJsBean> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgProfileJsBean> jsBeans = new ArrayList<OgProfileJsBean>();
            List<OgProfile> ogProfiles = getServiceProxy().findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(ogProfiles != null) {
                for(OgProfile ogProfile : ogProfiles) {
                    jsBeans.add(convertOgProfileToJsBean(ogProfile));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgProfile(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws WebException
    {
        try {
            return getServiceProxy().createOgProfile(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgProfile(OgProfileJsBean jsBean) throws WebException
    {
        try {
            OgProfile ogProfile = convertOgProfileJsBeanToBean(jsBean);
            return getServiceProxy().createOgProfile(ogProfile);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgProfileJsBean constructOgProfile(OgProfileJsBean jsBean) throws WebException
    {
        try {
            OgProfile ogProfile = convertOgProfileJsBeanToBean(jsBean);
            ogProfile = getServiceProxy().constructOgProfile(ogProfile);
            jsBean = convertOgProfileToJsBean(ogProfile);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOgProfile(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws WebException
    {
        try {
            return getServiceProxy().updateOgProfile(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOgProfile(OgProfileJsBean jsBean) throws WebException
    {
        try {
            OgProfile ogProfile = convertOgProfileJsBeanToBean(jsBean);
            return getServiceProxy().updateOgProfile(ogProfile);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgProfileJsBean refreshOgProfile(OgProfileJsBean jsBean) throws WebException
    {
        try {
            OgProfile ogProfile = convertOgProfileJsBeanToBean(jsBean);
            ogProfile = getServiceProxy().refreshOgProfile(ogProfile);
            jsBean = convertOgProfileToJsBean(ogProfile);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgProfile(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteOgProfile(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgProfile(OgProfileJsBean jsBean) throws WebException
    {
        try {
            OgProfile ogProfile = convertOgProfileJsBeanToBean(jsBean);
            return getServiceProxy().deleteOgProfile(ogProfile);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOgProfiles(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteOgProfiles(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static OgProfileJsBean convertOgProfileToJsBean(OgProfile ogProfile)
    {
        OgProfileJsBean jsBean = null;
        if(ogProfile != null) {
            jsBean = new OgProfileJsBean();
            jsBean.setGuid(ogProfile.getGuid());
            jsBean.setUrl(ogProfile.getUrl());
            jsBean.setType(ogProfile.getType());
            jsBean.setSiteName(ogProfile.getSiteName());
            jsBean.setTitle(ogProfile.getTitle());
            jsBean.setDescription(ogProfile.getDescription());
            jsBean.setFbAdmins(ogProfile.getFbAdmins());
            jsBean.setFbAppId(ogProfile.getFbAppId());
            List<OgImageStructJsBean> imageJsBeans = new ArrayList<OgImageStructJsBean>();
            List<OgImageStruct> imageBeans = ogProfile.getImage();
            if(imageBeans != null) {
                for(OgImageStruct ogImageStruct : imageBeans) {
                    OgImageStructJsBean jB = OgImageStructWebService.convertOgImageStructToJsBean(ogImageStruct);
                    imageJsBeans.add(jB); 
                }
            }
            jsBean.setImage(imageJsBeans);
            List<OgAudioStructJsBean> audioJsBeans = new ArrayList<OgAudioStructJsBean>();
            List<OgAudioStruct> audioBeans = ogProfile.getAudio();
            if(audioBeans != null) {
                for(OgAudioStruct ogAudioStruct : audioBeans) {
                    OgAudioStructJsBean jB = OgAudioStructWebService.convertOgAudioStructToJsBean(ogAudioStruct);
                    audioJsBeans.add(jB); 
                }
            }
            jsBean.setAudio(audioJsBeans);
            List<OgVideoStructJsBean> videoJsBeans = new ArrayList<OgVideoStructJsBean>();
            List<OgVideoStruct> videoBeans = ogProfile.getVideo();
            if(videoBeans != null) {
                for(OgVideoStruct ogVideoStruct : videoBeans) {
                    OgVideoStructJsBean jB = OgVideoStructWebService.convertOgVideoStructToJsBean(ogVideoStruct);
                    videoJsBeans.add(jB); 
                }
            }
            jsBean.setVideo(videoJsBeans);
            jsBean.setLocale(ogProfile.getLocale());
            jsBean.setLocaleAlternate(ogProfile.getLocaleAlternate());
            jsBean.setProfileId(ogProfile.getProfileId());
            jsBean.setFirstName(ogProfile.getFirstName());
            jsBean.setLastName(ogProfile.getLastName());
            jsBean.setUsername(ogProfile.getUsername());
            jsBean.setGender(ogProfile.getGender());
            jsBean.setCreatedTime(ogProfile.getCreatedTime());
            jsBean.setModifiedTime(ogProfile.getModifiedTime());
        }
        return jsBean;
    }

    public static OgProfile convertOgProfileJsBeanToBean(OgProfileJsBean jsBean)
    {
        OgProfileBean ogProfile = null;
        if(jsBean != null) {
            ogProfile = new OgProfileBean();
            ogProfile.setGuid(jsBean.getGuid());
            ogProfile.setUrl(jsBean.getUrl());
            ogProfile.setType(jsBean.getType());
            ogProfile.setSiteName(jsBean.getSiteName());
            ogProfile.setTitle(jsBean.getTitle());
            ogProfile.setDescription(jsBean.getDescription());
            ogProfile.setFbAdmins(jsBean.getFbAdmins());
            ogProfile.setFbAppId(jsBean.getFbAppId());
            List<OgImageStruct> imageBeans = new ArrayList<OgImageStruct>();
            List<OgImageStructJsBean> imageJsBeans = jsBean.getImage();
            if(imageJsBeans != null) {
                for(OgImageStructJsBean ogImageStruct : imageJsBeans) {
                    OgImageStruct b = OgImageStructWebService.convertOgImageStructJsBeanToBean(ogImageStruct);
                    imageBeans.add(b); 
                }
            }
            ogProfile.setImage(imageBeans);
            List<OgAudioStruct> audioBeans = new ArrayList<OgAudioStruct>();
            List<OgAudioStructJsBean> audioJsBeans = jsBean.getAudio();
            if(audioJsBeans != null) {
                for(OgAudioStructJsBean ogAudioStruct : audioJsBeans) {
                    OgAudioStruct b = OgAudioStructWebService.convertOgAudioStructJsBeanToBean(ogAudioStruct);
                    audioBeans.add(b); 
                }
            }
            ogProfile.setAudio(audioBeans);
            List<OgVideoStruct> videoBeans = new ArrayList<OgVideoStruct>();
            List<OgVideoStructJsBean> videoJsBeans = jsBean.getVideo();
            if(videoJsBeans != null) {
                for(OgVideoStructJsBean ogVideoStruct : videoJsBeans) {
                    OgVideoStruct b = OgVideoStructWebService.convertOgVideoStructJsBeanToBean(ogVideoStruct);
                    videoBeans.add(b); 
                }
            }
            ogProfile.setVideo(videoBeans);
            ogProfile.setLocale(jsBean.getLocale());
            ogProfile.setLocaleAlternate(jsBean.getLocaleAlternate());
            ogProfile.setProfileId(jsBean.getProfileId());
            ogProfile.setFirstName(jsBean.getFirstName());
            ogProfile.setLastName(jsBean.getLastName());
            ogProfile.setUsername(jsBean.getUsername());
            ogProfile.setGender(jsBean.getGender());
            ogProfile.setCreatedTime(jsBean.getCreatedTime());
            ogProfile.setModifiedTime(jsBean.getModifiedTime());
        }
        return ogProfile;
    }

}
