package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.GaeUserStruct;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.GaeUserStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.GaeUserStructWebService;


// MockGaeUserStructWebService is a mock object.
// It can be used as a base class to mock GaeUserStructWebService objects.
public abstract class MockGaeUserStructWebService extends GaeUserStructWebService  // implements GaeUserStructService
{
    private static final Logger log = Logger.getLogger(MockGaeUserStructWebService.class.getName());
     

}
