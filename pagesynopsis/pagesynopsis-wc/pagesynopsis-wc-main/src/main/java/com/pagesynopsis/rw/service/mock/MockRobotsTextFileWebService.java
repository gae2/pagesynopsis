package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.RobotsTextGroupJsBean;
import com.pagesynopsis.fe.bean.RobotsTextFileJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.RobotsTextFileWebService;


// MockRobotsTextFileWebService is a mock object.
// It can be used as a base class to mock RobotsTextFileWebService objects.
public abstract class MockRobotsTextFileWebService extends RobotsTextFileWebService  // implements RobotsTextFileService
{
    private static final Logger log = Logger.getLogger(MockRobotsTextFileWebService.class.getName());
     
    // Af service interface.
    private RobotsTextFileWebService mService = null;

    public MockRobotsTextFileWebService()
    {
        this(MockServiceProxyFactory.getInstance().getRobotsTextFileServiceProxy());
    }
    public MockRobotsTextFileWebService(RobotsTextFileService service)
    {
        super(service);
    }


}
