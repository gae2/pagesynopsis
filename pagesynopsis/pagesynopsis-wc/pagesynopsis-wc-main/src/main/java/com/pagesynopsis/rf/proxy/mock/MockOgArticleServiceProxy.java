package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgAudioStructListStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgImageStructListStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgActorStructListStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructListStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgArticleListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.OgArticleServiceProxy;


// MockOgArticleServiceProxy is a decorator.
// It can be used as a base class to mock OgArticleService objects.
public abstract class MockOgArticleServiceProxy extends OgArticleServiceProxy implements OgArticleService
{
    private static final Logger log = Logger.getLogger(MockOgArticleServiceProxy.class.getName());

    // MockOgArticleServiceProxy uses the decorator design pattern.
    private OgArticleService decoratedProxy;

    public MockOgArticleServiceProxy(OgArticleService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected OgArticleService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(OgArticleService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public OgArticle getOgArticle(String guid) throws BaseException
    {
        return decoratedProxy.getOgArticle(guid);
    }

    @Override
    public Object getOgArticle(String guid, String field) throws BaseException
    {
        return decoratedProxy.getOgArticle(guid, field);
    }

    @Override
    public List<OgArticle> getOgArticles(List<String> guids) throws BaseException
    {
        return decoratedProxy.getOgArticles(guids);
    }

    @Override
    public List<OgArticle> getAllOgArticles() throws BaseException
    {
        return getAllOgArticles(null, null, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticles(ordering, offset, count, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgArticles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticleKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllOgArticleKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgArticles(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findOgArticleKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgArticle(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        return decoratedProxy.createOgArticle(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
    }

    @Override
    public String createOgArticle(OgArticle bean) throws BaseException
    {
        return decoratedProxy.createOgArticle(bean);
    }

    @Override
    public OgArticle constructOgArticle(OgArticle bean) throws BaseException
    {
        return decoratedProxy.constructOgArticle(bean);
    }

    @Override
    public Boolean updateOgArticle(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        return decoratedProxy.updateOgArticle(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
    }

    @Override
    public Boolean updateOgArticle(OgArticle bean) throws BaseException
    {
        return decoratedProxy.updateOgArticle(bean);
    }

    @Override
    public OgArticle refreshOgArticle(OgArticle bean) throws BaseException
    {
        return decoratedProxy.refreshOgArticle(bean);
    }

    @Override
    public Boolean deleteOgArticle(String guid) throws BaseException
    {
        return decoratedProxy.deleteOgArticle(guid);
    }

    @Override
    public Boolean deleteOgArticle(OgArticle bean) throws BaseException
    {
        return decoratedProxy.deleteOgArticle(bean);
    }

    @Override
    public Long deleteOgArticles(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteOgArticles(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgArticles(List<OgArticle> ogArticles) throws BaseException
    {
        return decoratedProxy.createOgArticles(ogArticles);
    }

    // TBD
    //@Override
    //public Boolean updateOgArticles(List<OgArticle> ogArticles) throws BaseException
    //{
    //}

}
