package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.MediaSourceStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class VideoStructWebService // implements VideoStructService
{
    private static final Logger log = Logger.getLogger(VideoStructWebService.class.getName());
     
    public static VideoStructJsBean convertVideoStructToJsBean(VideoStruct videoStruct)
    {
        VideoStructJsBean jsBean = null;
        if(videoStruct != null) {
            jsBean = new VideoStructJsBean();
            jsBean.setUuid(videoStruct.getUuid());
            jsBean.setId(videoStruct.getId());
            jsBean.setWidth(videoStruct.getWidth());
            jsBean.setHeight(videoStruct.getHeight());
            jsBean.setControls(videoStruct.getControls());
            jsBean.setAutoplayEnabled(videoStruct.isAutoplayEnabled());
            jsBean.setLoopEnabled(videoStruct.isLoopEnabled());
            jsBean.setPreloadEnabled(videoStruct.isPreloadEnabled());
            jsBean.setMuted(videoStruct.isMuted());
            jsBean.setRemark(videoStruct.getRemark());
            jsBean.setSource(MediaSourceStructWebService.convertMediaSourceStructToJsBean(videoStruct.getSource()));
            jsBean.setSource1(MediaSourceStructWebService.convertMediaSourceStructToJsBean(videoStruct.getSource1()));
            jsBean.setSource2(MediaSourceStructWebService.convertMediaSourceStructToJsBean(videoStruct.getSource2()));
            jsBean.setSource3(MediaSourceStructWebService.convertMediaSourceStructToJsBean(videoStruct.getSource3()));
            jsBean.setSource4(MediaSourceStructWebService.convertMediaSourceStructToJsBean(videoStruct.getSource4()));
            jsBean.setSource5(MediaSourceStructWebService.convertMediaSourceStructToJsBean(videoStruct.getSource5()));
            jsBean.setNote(videoStruct.getNote());
        }
        return jsBean;
    }

    public static VideoStruct convertVideoStructJsBeanToBean(VideoStructJsBean jsBean)
    {
        VideoStructBean videoStruct = null;
        if(jsBean != null) {
            videoStruct = new VideoStructBean();
            videoStruct.setUuid(jsBean.getUuid());
            videoStruct.setId(jsBean.getId());
            videoStruct.setWidth(jsBean.getWidth());
            videoStruct.setHeight(jsBean.getHeight());
            videoStruct.setControls(jsBean.getControls());
            videoStruct.setAutoplayEnabled(jsBean.isAutoplayEnabled());
            videoStruct.setLoopEnabled(jsBean.isLoopEnabled());
            videoStruct.setPreloadEnabled(jsBean.isPreloadEnabled());
            videoStruct.setMuted(jsBean.isMuted());
            videoStruct.setRemark(jsBean.getRemark());
            videoStruct.setSource(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource()));
            videoStruct.setSource1(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource1()));
            videoStruct.setSource2(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource2()));
            videoStruct.setSource3(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource3()));
            videoStruct.setSource4(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource4()));
            videoStruct.setSource5(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource5()));
            videoStruct.setNote(jsBean.getNote());
        }
        return videoStruct;
    }

}
