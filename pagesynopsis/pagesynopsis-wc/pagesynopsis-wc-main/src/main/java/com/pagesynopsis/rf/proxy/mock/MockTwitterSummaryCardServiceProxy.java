package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoListStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataListStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardListStub;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.service.TwitterSummaryCardService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.TwitterSummaryCardServiceProxy;


// MockTwitterSummaryCardServiceProxy is a decorator.
// It can be used as a base class to mock TwitterSummaryCardService objects.
public abstract class MockTwitterSummaryCardServiceProxy extends TwitterSummaryCardServiceProxy implements TwitterSummaryCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterSummaryCardServiceProxy.class.getName());

    // MockTwitterSummaryCardServiceProxy uses the decorator design pattern.
    private TwitterSummaryCardService decoratedProxy;

    public MockTwitterSummaryCardServiceProxy(TwitterSummaryCardService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected TwitterSummaryCardService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(TwitterSummaryCardService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public TwitterSummaryCard getTwitterSummaryCard(String guid) throws BaseException
    {
        return decoratedProxy.getTwitterSummaryCard(guid);
    }

    @Override
    public Object getTwitterSummaryCard(String guid, String field) throws BaseException
    {
        return decoratedProxy.getTwitterSummaryCard(guid, field);
    }

    @Override
    public List<TwitterSummaryCard> getTwitterSummaryCards(List<String> guids) throws BaseException
    {
        return decoratedProxy.getTwitterSummaryCards(guids);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards() throws BaseException
    {
        return getAllTwitterSummaryCards(null, null, null);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterSummaryCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterSummaryCard> getAllTwitterSummaryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterSummaryCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterSummaryCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterSummaryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterSummaryCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterSummaryCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterSummaryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterSummaryCard> findTwitterSummaryCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterSummaryCards(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterSummaryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterSummaryCardKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterSummaryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return decoratedProxy.createTwitterSummaryCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public String createTwitterSummaryCard(TwitterSummaryCard bean) throws BaseException
    {
        return decoratedProxy.createTwitterSummaryCard(bean);
    }

    @Override
    public TwitterSummaryCard constructTwitterSummaryCard(TwitterSummaryCard bean) throws BaseException
    {
        return decoratedProxy.constructTwitterSummaryCard(bean);
    }

    @Override
    public Boolean updateTwitterSummaryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return decoratedProxy.updateTwitterSummaryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public Boolean updateTwitterSummaryCard(TwitterSummaryCard bean) throws BaseException
    {
        return decoratedProxy.updateTwitterSummaryCard(bean);
    }

    @Override
    public TwitterSummaryCard refreshTwitterSummaryCard(TwitterSummaryCard bean) throws BaseException
    {
        return decoratedProxy.refreshTwitterSummaryCard(bean);
    }

    @Override
    public Boolean deleteTwitterSummaryCard(String guid) throws BaseException
    {
        return decoratedProxy.deleteTwitterSummaryCard(guid);
    }

    @Override
    public Boolean deleteTwitterSummaryCard(TwitterSummaryCard bean) throws BaseException
    {
        return decoratedProxy.deleteTwitterSummaryCard(bean);
    }

    @Override
    public Long deleteTwitterSummaryCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteTwitterSummaryCards(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterSummaryCards(List<TwitterSummaryCard> twitterSummaryCards) throws BaseException
    {
        return decoratedProxy.createTwitterSummaryCards(twitterSummaryCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterSummaryCards(List<TwitterSummaryCard> twitterSummaryCards) throws BaseException
    //{
    //}

}
