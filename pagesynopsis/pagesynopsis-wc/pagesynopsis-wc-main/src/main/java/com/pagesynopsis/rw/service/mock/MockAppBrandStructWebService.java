package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.AppBrandStruct;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.AppBrandStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.AppBrandStructWebService;


// MockAppBrandStructWebService is a mock object.
// It can be used as a base class to mock AppBrandStructWebService objects.
public abstract class MockAppBrandStructWebService extends AppBrandStructWebService  // implements AppBrandStructService
{
    private static final Logger log = Logger.getLogger(MockAppBrandStructWebService.class.getName());
     

}
