package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.service.TwitterGalleryCardService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.TwitterGalleryCardWebService;


// MockTwitterGalleryCardWebService is a mock object.
// It can be used as a base class to mock TwitterGalleryCardWebService objects.
public abstract class MockTwitterGalleryCardWebService extends TwitterGalleryCardWebService  // implements TwitterGalleryCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterGalleryCardWebService.class.getName());
     
    // Af service interface.
    private TwitterGalleryCardWebService mService = null;

    public MockTwitterGalleryCardWebService()
    {
        this(MockServiceProxyFactory.getInstance().getTwitterGalleryCardServiceProxy());
    }
    public MockTwitterGalleryCardWebService(TwitterGalleryCardService service)
    {
        super(service);
    }


}
