package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.UrlStructWebService;


// MockUrlStructWebService is a mock object.
// It can be used as a base class to mock UrlStructWebService objects.
public abstract class MockUrlStructWebService extends UrlStructWebService  // implements UrlStructService
{
    private static final Logger log = Logger.getLogger(MockUrlStructWebService.class.getName());
     

}
