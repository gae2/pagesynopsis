package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.AnchorStructWebService;


// MockAnchorStructWebService is a mock object.
// It can be used as a base class to mock AnchorStructWebService objects.
public abstract class MockAnchorStructWebService extends AnchorStructWebService  // implements AnchorStructService
{
    private static final Logger log = Logger.getLogger(MockAnchorStructWebService.class.getName());
     

}
