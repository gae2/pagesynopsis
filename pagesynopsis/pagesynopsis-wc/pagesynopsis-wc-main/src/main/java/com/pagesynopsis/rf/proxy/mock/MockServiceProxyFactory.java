package com.pagesynopsis.rf.proxy.mock;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.rf.proxy.ServiceProxyFactory;
import com.pagesynopsis.rf.proxy.UserServiceProxy;
import com.pagesynopsis.rf.proxy.RobotsTextFileServiceProxy;
import com.pagesynopsis.rf.proxy.RobotsTextRefreshServiceProxy;
import com.pagesynopsis.rf.proxy.OgProfileServiceProxy;
import com.pagesynopsis.rf.proxy.OgWebsiteServiceProxy;
import com.pagesynopsis.rf.proxy.OgBlogServiceProxy;
import com.pagesynopsis.rf.proxy.OgArticleServiceProxy;
import com.pagesynopsis.rf.proxy.OgBookServiceProxy;
import com.pagesynopsis.rf.proxy.OgVideoServiceProxy;
import com.pagesynopsis.rf.proxy.OgMovieServiceProxy;
import com.pagesynopsis.rf.proxy.OgTvShowServiceProxy;
import com.pagesynopsis.rf.proxy.OgTvEpisodeServiceProxy;
import com.pagesynopsis.rf.proxy.TwitterSummaryCardServiceProxy;
import com.pagesynopsis.rf.proxy.TwitterPhotoCardServiceProxy;
import com.pagesynopsis.rf.proxy.TwitterGalleryCardServiceProxy;
import com.pagesynopsis.rf.proxy.TwitterAppCardServiceProxy;
import com.pagesynopsis.rf.proxy.TwitterPlayerCardServiceProxy;
import com.pagesynopsis.rf.proxy.TwitterProductCardServiceProxy;
import com.pagesynopsis.rf.proxy.FetchRequestServiceProxy;
import com.pagesynopsis.rf.proxy.PageInfoServiceProxy;
import com.pagesynopsis.rf.proxy.PageFetchServiceProxy;
import com.pagesynopsis.rf.proxy.LinkListServiceProxy;
import com.pagesynopsis.rf.proxy.ImageSetServiceProxy;
import com.pagesynopsis.rf.proxy.AudioSetServiceProxy;
import com.pagesynopsis.rf.proxy.VideoSetServiceProxy;
import com.pagesynopsis.rf.proxy.OpenGraphMetaServiceProxy;
import com.pagesynopsis.rf.proxy.TwitterCardMetaServiceProxy;
import com.pagesynopsis.rf.proxy.DomainInfoServiceProxy;
import com.pagesynopsis.rf.proxy.UrlRatingServiceProxy;
import com.pagesynopsis.rf.proxy.ServiceInfoServiceProxy;
import com.pagesynopsis.rf.proxy.FiveTenServiceProxy;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Create your own mock object factory using MockServiceProxyFactory as a template.
public class MockServiceProxyFactory extends ServiceProxyFactory
{
    private static final Logger log = Logger.getLogger(MockServiceProxyFactory.class.getName());

    private UserServiceProxy userService = null;
    private RobotsTextFileServiceProxy robotsTextFileService = null;
    private RobotsTextRefreshServiceProxy robotsTextRefreshService = null;
    private OgProfileServiceProxy ogProfileService = null;
    private OgWebsiteServiceProxy ogWebsiteService = null;
    private OgBlogServiceProxy ogBlogService = null;
    private OgArticleServiceProxy ogArticleService = null;
    private OgBookServiceProxy ogBookService = null;
    private OgVideoServiceProxy ogVideoService = null;
    private OgMovieServiceProxy ogMovieService = null;
    private OgTvShowServiceProxy ogTvShowService = null;
    private OgTvEpisodeServiceProxy ogTvEpisodeService = null;
    private TwitterSummaryCardServiceProxy twitterSummaryCardService = null;
    private TwitterPhotoCardServiceProxy twitterPhotoCardService = null;
    private TwitterGalleryCardServiceProxy twitterGalleryCardService = null;
    private TwitterAppCardServiceProxy twitterAppCardService = null;
    private TwitterPlayerCardServiceProxy twitterPlayerCardService = null;
    private TwitterProductCardServiceProxy twitterProductCardService = null;
    private FetchRequestServiceProxy fetchRequestService = null;
    private PageInfoServiceProxy pageInfoService = null;
    private PageFetchServiceProxy pageFetchService = null;
    private LinkListServiceProxy linkListService = null;
    private ImageSetServiceProxy imageSetService = null;
    private AudioSetServiceProxy audioSetService = null;
    private VideoSetServiceProxy videoSetService = null;
    private OpenGraphMetaServiceProxy openGraphMetaService = null;
    private TwitterCardMetaServiceProxy twitterCardMetaService = null;
    private DomainInfoServiceProxy domainInfoService = null;
    private UrlRatingServiceProxy urlRatingService = null;
    private ServiceInfoServiceProxy serviceInfoService = null;
    private FiveTenServiceProxy fiveTenService = null;

    // Using the Decorator pattern.
    private ServiceProxyFactory decoratedProxyFactory;
    private MockServiceProxyFactory()
    {
        this(null);   // ????
    }
    private MockServiceProxyFactory(ServiceProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }

    // Initialization-on-demand holder.
    private static class MockServiceProxyFactoryHolder
    {
        private static final MockServiceProxyFactory INSTANCE = new MockServiceProxyFactory();
    }

    // Singleton method
    public static MockServiceProxyFactory getInstance()
    {
        return MockServiceProxyFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public ServiceProxyFactory getDecoratedProxyFactory()
    {
        return decoratedProxyFactory;
    }
    public void setDecoratedProxyFactory(ServiceProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }


    public UserServiceProxy getUserServiceProxy()
    {
        if(userService == null) {
            userService = new MockUserServiceProxy(decoratedProxyFactory.getUserServiceProxy()) {};
        }
        return userService;
    }

    public RobotsTextFileServiceProxy getRobotsTextFileServiceProxy()
    {
        if(robotsTextFileService == null) {
            robotsTextFileService = new MockRobotsTextFileServiceProxy(decoratedProxyFactory.getRobotsTextFileServiceProxy()) {};
        }
        return robotsTextFileService;
    }

    public RobotsTextRefreshServiceProxy getRobotsTextRefreshServiceProxy()
    {
        if(robotsTextRefreshService == null) {
            robotsTextRefreshService = new MockRobotsTextRefreshServiceProxy(decoratedProxyFactory.getRobotsTextRefreshServiceProxy()) {};
        }
        return robotsTextRefreshService;
    }

    public OgProfileServiceProxy getOgProfileServiceProxy()
    {
        if(ogProfileService == null) {
            ogProfileService = new MockOgProfileServiceProxy(decoratedProxyFactory.getOgProfileServiceProxy()) {};
        }
        return ogProfileService;
    }

    public OgWebsiteServiceProxy getOgWebsiteServiceProxy()
    {
        if(ogWebsiteService == null) {
            ogWebsiteService = new MockOgWebsiteServiceProxy(decoratedProxyFactory.getOgWebsiteServiceProxy()) {};
        }
        return ogWebsiteService;
    }

    public OgBlogServiceProxy getOgBlogServiceProxy()
    {
        if(ogBlogService == null) {
            ogBlogService = new MockOgBlogServiceProxy(decoratedProxyFactory.getOgBlogServiceProxy()) {};
        }
        return ogBlogService;
    }

    public OgArticleServiceProxy getOgArticleServiceProxy()
    {
        if(ogArticleService == null) {
            ogArticleService = new MockOgArticleServiceProxy(decoratedProxyFactory.getOgArticleServiceProxy()) {};
        }
        return ogArticleService;
    }

    public OgBookServiceProxy getOgBookServiceProxy()
    {
        if(ogBookService == null) {
            ogBookService = new MockOgBookServiceProxy(decoratedProxyFactory.getOgBookServiceProxy()) {};
        }
        return ogBookService;
    }

    public OgVideoServiceProxy getOgVideoServiceProxy()
    {
        if(ogVideoService == null) {
            ogVideoService = new MockOgVideoServiceProxy(decoratedProxyFactory.getOgVideoServiceProxy()) {};
        }
        return ogVideoService;
    }

    public OgMovieServiceProxy getOgMovieServiceProxy()
    {
        if(ogMovieService == null) {
            ogMovieService = new MockOgMovieServiceProxy(decoratedProxyFactory.getOgMovieServiceProxy()) {};
        }
        return ogMovieService;
    }

    public OgTvShowServiceProxy getOgTvShowServiceProxy()
    {
        if(ogTvShowService == null) {
            ogTvShowService = new MockOgTvShowServiceProxy(decoratedProxyFactory.getOgTvShowServiceProxy()) {};
        }
        return ogTvShowService;
    }

    public OgTvEpisodeServiceProxy getOgTvEpisodeServiceProxy()
    {
        if(ogTvEpisodeService == null) {
            ogTvEpisodeService = new MockOgTvEpisodeServiceProxy(decoratedProxyFactory.getOgTvEpisodeServiceProxy()) {};
        }
        return ogTvEpisodeService;
    }

    public TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy()
    {
        if(twitterSummaryCardService == null) {
            twitterSummaryCardService = new MockTwitterSummaryCardServiceProxy(decoratedProxyFactory.getTwitterSummaryCardServiceProxy()) {};
        }
        return twitterSummaryCardService;
    }

    public TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy()
    {
        if(twitterPhotoCardService == null) {
            twitterPhotoCardService = new MockTwitterPhotoCardServiceProxy(decoratedProxyFactory.getTwitterPhotoCardServiceProxy()) {};
        }
        return twitterPhotoCardService;
    }

    public TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy()
    {
        if(twitterGalleryCardService == null) {
            twitterGalleryCardService = new MockTwitterGalleryCardServiceProxy(decoratedProxyFactory.getTwitterGalleryCardServiceProxy()) {};
        }
        return twitterGalleryCardService;
    }

    public TwitterAppCardServiceProxy getTwitterAppCardServiceProxy()
    {
        if(twitterAppCardService == null) {
            twitterAppCardService = new MockTwitterAppCardServiceProxy(decoratedProxyFactory.getTwitterAppCardServiceProxy()) {};
        }
        return twitterAppCardService;
    }

    public TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy()
    {
        if(twitterPlayerCardService == null) {
            twitterPlayerCardService = new MockTwitterPlayerCardServiceProxy(decoratedProxyFactory.getTwitterPlayerCardServiceProxy()) {};
        }
        return twitterPlayerCardService;
    }

    public TwitterProductCardServiceProxy getTwitterProductCardServiceProxy()
    {
        if(twitterProductCardService == null) {
            twitterProductCardService = new MockTwitterProductCardServiceProxy(decoratedProxyFactory.getTwitterProductCardServiceProxy()) {};
        }
        return twitterProductCardService;
    }

    public FetchRequestServiceProxy getFetchRequestServiceProxy()
    {
        if(fetchRequestService == null) {
            fetchRequestService = new MockFetchRequestServiceProxy(decoratedProxyFactory.getFetchRequestServiceProxy()) {};
        }
        return fetchRequestService;
    }

    public PageInfoServiceProxy getPageInfoServiceProxy()
    {
        if(pageInfoService == null) {
            pageInfoService = new MockPageInfoServiceProxy(decoratedProxyFactory.getPageInfoServiceProxy()) {};
        }
        return pageInfoService;
    }

    public PageFetchServiceProxy getPageFetchServiceProxy()
    {
        if(pageFetchService == null) {
            pageFetchService = new MockPageFetchServiceProxy(decoratedProxyFactory.getPageFetchServiceProxy()) {};
        }
        return pageFetchService;
    }

    public LinkListServiceProxy getLinkListServiceProxy()
    {
        if(linkListService == null) {
            linkListService = new MockLinkListServiceProxy(decoratedProxyFactory.getLinkListServiceProxy()) {};
        }
        return linkListService;
    }

    public ImageSetServiceProxy getImageSetServiceProxy()
    {
        if(imageSetService == null) {
            imageSetService = new MockImageSetServiceProxy(decoratedProxyFactory.getImageSetServiceProxy()) {};
        }
        return imageSetService;
    }

    public AudioSetServiceProxy getAudioSetServiceProxy()
    {
        if(audioSetService == null) {
            audioSetService = new MockAudioSetServiceProxy(decoratedProxyFactory.getAudioSetServiceProxy()) {};
        }
        return audioSetService;
    }

    public VideoSetServiceProxy getVideoSetServiceProxy()
    {
        if(videoSetService == null) {
            videoSetService = new MockVideoSetServiceProxy(decoratedProxyFactory.getVideoSetServiceProxy()) {};
        }
        return videoSetService;
    }

    public OpenGraphMetaServiceProxy getOpenGraphMetaServiceProxy()
    {
        if(openGraphMetaService == null) {
            openGraphMetaService = new MockOpenGraphMetaServiceProxy(decoratedProxyFactory.getOpenGraphMetaServiceProxy()) {};
        }
        return openGraphMetaService;
    }

    public TwitterCardMetaServiceProxy getTwitterCardMetaServiceProxy()
    {
        if(twitterCardMetaService == null) {
            twitterCardMetaService = new MockTwitterCardMetaServiceProxy(decoratedProxyFactory.getTwitterCardMetaServiceProxy()) {};
        }
        return twitterCardMetaService;
    }

    public DomainInfoServiceProxy getDomainInfoServiceProxy()
    {
        if(domainInfoService == null) {
            domainInfoService = new MockDomainInfoServiceProxy(decoratedProxyFactory.getDomainInfoServiceProxy()) {};
        }
        return domainInfoService;
    }

    public UrlRatingServiceProxy getUrlRatingServiceProxy()
    {
        if(urlRatingService == null) {
            urlRatingService = new MockUrlRatingServiceProxy(decoratedProxyFactory.getUrlRatingServiceProxy()) {};
        }
        return urlRatingService;
    }

    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new MockServiceInfoServiceProxy(decoratedProxyFactory.getServiceInfoServiceProxy()) {};
        }
        return serviceInfoService;
    }

    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenService == null) {
            fiveTenService = new MockFiveTenServiceProxy(decoratedProxyFactory.getFiveTenServiceProxy()) {};
        }
        return fiveTenService;
    }

}
