package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.af.bean.UrlRatingBean;
import com.pagesynopsis.af.service.UrlRatingService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.UrlRatingJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.UrlRatingWebService;


// MockUrlRatingWebService is a mock object.
// It can be used as a base class to mock UrlRatingWebService objects.
public abstract class MockUrlRatingWebService extends UrlRatingWebService  // implements UrlRatingService
{
    private static final Logger log = Logger.getLogger(MockUrlRatingWebService.class.getName());
     
    // Af service interface.
    private UrlRatingWebService mService = null;

    public MockUrlRatingWebService()
    {
        this(MockServiceProxyFactory.getInstance().getUrlRatingServiceProxy());
    }
    public MockUrlRatingWebService(UrlRatingService service)
    {
        super(service);
    }


}
