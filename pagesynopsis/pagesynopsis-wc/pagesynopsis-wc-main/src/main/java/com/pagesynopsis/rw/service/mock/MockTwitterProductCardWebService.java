package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.service.TwitterProductCardService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.fe.bean.TwitterProductCardJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.TwitterProductCardWebService;


// MockTwitterProductCardWebService is a mock object.
// It can be used as a base class to mock TwitterProductCardWebService objects.
public abstract class MockTwitterProductCardWebService extends TwitterProductCardWebService  // implements TwitterProductCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterProductCardWebService.class.getName());
     
    // Af service interface.
    private TwitterProductCardWebService mService = null;

    public MockTwitterProductCardWebService()
    {
        this(MockServiceProxyFactory.getInstance().getTwitterProductCardServiceProxy());
    }
    public MockTwitterProductCardWebService(TwitterProductCardService service)
    {
        super(service);
    }


}
