package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgGeoPointStruct;
import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgGeoPointStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgGeoPointStructWebService;


// MockOgGeoPointStructWebService is a mock object.
// It can be used as a base class to mock OgGeoPointStructWebService objects.
public abstract class MockOgGeoPointStructWebService extends OgGeoPointStructWebService  // implements OgGeoPointStructService
{
    private static final Logger log = Logger.getLogger(MockOgGeoPointStructWebService.class.getName());
     

}
