package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.EncodedQueryParamStruct;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.EncodedQueryParamStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.EncodedQueryParamStructWebService;


// MockEncodedQueryParamStructWebService is a mock object.
// It can be used as a base class to mock EncodedQueryParamStructWebService objects.
public abstract class MockEncodedQueryParamStructWebService extends EncodedQueryParamStructWebService  // implements EncodedQueryParamStructService
{
    private static final Logger log = Logger.getLogger(MockEncodedQueryParamStructWebService.class.getName());
     

}
