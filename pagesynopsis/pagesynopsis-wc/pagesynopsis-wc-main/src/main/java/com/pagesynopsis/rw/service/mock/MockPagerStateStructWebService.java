package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.PagerStateStruct;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.PagerStateStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.PagerStateStructWebService;


// MockPagerStateStructWebService is a mock object.
// It can be used as a base class to mock PagerStateStructWebService objects.
public abstract class MockPagerStateStructWebService extends PagerStateStructWebService  // implements PagerStateStructService
{
    private static final Logger log = Logger.getLogger(MockPagerStateStructWebService.class.getName());
     

}
