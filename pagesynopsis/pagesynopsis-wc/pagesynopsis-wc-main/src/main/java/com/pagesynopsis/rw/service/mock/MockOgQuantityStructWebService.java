package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgQuantityStruct;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgQuantityStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgQuantityStructWebService;


// MockOgQuantityStructWebService is a mock object.
// It can be used as a base class to mock OgQuantityStructWebService objects.
public abstract class MockOgQuantityStructWebService extends OgQuantityStructWebService  // implements OgQuantityStructService
{
    private static final Logger log = Logger.getLogger(MockOgQuantityStructWebService.class.getName());
     

}
