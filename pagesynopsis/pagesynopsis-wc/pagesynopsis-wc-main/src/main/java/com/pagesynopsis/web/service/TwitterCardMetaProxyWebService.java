package com.pagesynopsis.web.service;

import java.util.logging.Logger;

import com.pagesynopsis.af.service.TwitterCardMetaService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardMetaJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;
import com.pagesynopsis.rw.service.TwitterCardMetaWebService;
import com.pagesynopsis.web.proxy.TwitterCardMetaWebServiceProxy;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.TwitterCardMeta;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterCardMetaProxyWebService
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaProxyWebService.class.getName());
     
    // Af service interface.
    private TwitterCardMetaWebServiceProxy mServiceProxy = null;

    public TwitterCardMetaProxyWebService()
    {
        //this(new TwitterCardMetaWebServiceProxy());
        this(null);
    }
    public TwitterCardMetaProxyWebService(TwitterCardMetaWebServiceProxy serviceProxy)
    {
        mServiceProxy = serviceProxy;
    }
    
    private TwitterCardMetaWebServiceProxy getServiceProxy()
    {
        if(mServiceProxy == null) {
            mServiceProxy = new TwitterCardMetaWebServiceProxy();
        }
        return mServiceProxy;
    }
    

//  "Get" is better than "Find" for this: Get returns a full object, whereas Find returns only an object with "default fetch". 
    public TwitterCardMetaJsBean findTwitterCardMetaByTargetUrl(String targetUrl, Boolean fetch) throws WebException
    {
        return findTwitterCardMetaByTargetUrl(targetUrl, fetch, null);
    }
    public TwitterCardMetaJsBean findTwitterCardMetaByTargetUrl(String targetUrl, Boolean fetch, Integer refresh) throws WebException
    {
        try {
            TwitterCardMetaJsBean jsBean = null;
            TwitterCardMeta twitterCardMeta = getServiceProxy().findTwitterCardMetaByTargetUrl(targetUrl, fetch, refresh);
            if(twitterCardMeta != null) {
                jsBean = TwitterCardMetaWebService.convertTwitterCardMetaToJsBean(twitterCardMeta);
            }
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    
}
