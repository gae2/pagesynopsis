package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.service.OgWebsiteService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgWebsiteWebService;


// MockOgWebsiteWebService is a mock object.
// It can be used as a base class to mock OgWebsiteWebService objects.
public abstract class MockOgWebsiteWebService extends OgWebsiteWebService  // implements OgWebsiteService
{
    private static final Logger log = Logger.getLogger(MockOgWebsiteWebService.class.getName());
     
    // Af service interface.
    private OgWebsiteWebService mService = null;

    public MockOgWebsiteWebService()
    {
        this(MockServiceProxyFactory.getInstance().getOgWebsiteServiceProxy());
    }
    public MockOgWebsiteWebService(OgWebsiteService service)
    {
        super(service);
    }


}
