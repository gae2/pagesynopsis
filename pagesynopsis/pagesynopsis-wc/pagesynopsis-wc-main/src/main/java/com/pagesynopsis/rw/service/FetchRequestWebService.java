package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.af.bean.FetchRequestBean;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.NotificationStructJsBean;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.ReferrerInfoStructJsBean;
import com.pagesynopsis.fe.bean.FetchRequestJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FetchRequestWebService // implements FetchRequestService
{
    private static final Logger log = Logger.getLogger(FetchRequestWebService.class.getName());
     
    // Af service interface.
    private FetchRequestService mService = null;

    public FetchRequestWebService()
    {
        this(ServiceProxyFactory.getInstance().getFetchRequestServiceProxy());
    }
    public FetchRequestWebService(FetchRequestService service)
    {
        mService = service;
    }
    
    protected FetchRequestService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getFetchRequestServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(FetchRequestService service)
    {
        mService = service;
    }
    
    
    public FetchRequestJsBean getFetchRequest(String guid) throws WebException
    {
        try {
            FetchRequest fetchRequest = getServiceProxy().getFetchRequest(guid);
            FetchRequestJsBean bean = convertFetchRequestToJsBean(fetchRequest);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getFetchRequest(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getFetchRequest(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FetchRequestJsBean> getFetchRequests(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<FetchRequestJsBean> jsBeans = new ArrayList<FetchRequestJsBean>();
            List<FetchRequest> fetchRequests = getServiceProxy().getFetchRequests(guids);
            if(fetchRequests != null) {
                for(FetchRequest fetchRequest : fetchRequests) {
                    jsBeans.add(convertFetchRequestToJsBean(fetchRequest));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<FetchRequestJsBean> getAllFetchRequests() throws WebException
    {
        return getAllFetchRequests(null, null, null);
    }

    // @Deprecated
    public List<FetchRequestJsBean> getAllFetchRequests(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllFetchRequests(ordering, offset, count, null);
    }

    public List<FetchRequestJsBean> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<FetchRequestJsBean> jsBeans = new ArrayList<FetchRequestJsBean>();
            List<FetchRequest> fetchRequests = getServiceProxy().getAllFetchRequests(ordering, offset, count, forwardCursor);
            if(fetchRequests != null) {
                for(FetchRequest fetchRequest : fetchRequests) {
                    jsBeans.add(convertFetchRequestToJsBean(fetchRequest));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<FetchRequestJsBean> findFetchRequests(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findFetchRequests(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<FetchRequestJsBean> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<FetchRequestJsBean> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<FetchRequestJsBean> jsBeans = new ArrayList<FetchRequestJsBean>();
            List<FetchRequest> fetchRequests = getServiceProxy().findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(fetchRequests != null) {
                for(FetchRequest fetchRequest : fetchRequests) {
                    jsBeans.add(convertFetchRequestToJsBean(fetchRequest));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFetchRequest(String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStructJsBean referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStructJsBean notificationPref) throws WebException
    {
        try {
            return getServiceProxy().createFetchRequest(user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, NotificationStructWebService.convertNotificationStructJsBeanToBean(notificationPref));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createFetchRequest(FetchRequestJsBean jsBean) throws WebException
    {
        try {
            FetchRequest fetchRequest = convertFetchRequestJsBeanToBean(jsBean);
            return getServiceProxy().createFetchRequest(fetchRequest);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FetchRequestJsBean constructFetchRequest(FetchRequestJsBean jsBean) throws WebException
    {
        try {
            FetchRequest fetchRequest = convertFetchRequestJsBeanToBean(jsBean);
            fetchRequest = getServiceProxy().constructFetchRequest(fetchRequest);
            jsBean = convertFetchRequestToJsBean(fetchRequest);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateFetchRequest(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStructJsBean referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStructJsBean notificationPref) throws WebException
    {
        try {
            return getServiceProxy().updateFetchRequest(guid, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, NotificationStructWebService.convertNotificationStructJsBeanToBean(notificationPref));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateFetchRequest(FetchRequestJsBean jsBean) throws WebException
    {
        try {
            FetchRequest fetchRequest = convertFetchRequestJsBeanToBean(jsBean);
            return getServiceProxy().updateFetchRequest(fetchRequest);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public FetchRequestJsBean refreshFetchRequest(FetchRequestJsBean jsBean) throws WebException
    {
        try {
            FetchRequest fetchRequest = convertFetchRequestJsBeanToBean(jsBean);
            fetchRequest = getServiceProxy().refreshFetchRequest(fetchRequest);
            jsBean = convertFetchRequestToJsBean(fetchRequest);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFetchRequest(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteFetchRequest(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteFetchRequest(FetchRequestJsBean jsBean) throws WebException
    {
        try {
            FetchRequest fetchRequest = convertFetchRequestJsBeanToBean(jsBean);
            return getServiceProxy().deleteFetchRequest(fetchRequest);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteFetchRequests(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteFetchRequests(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static FetchRequestJsBean convertFetchRequestToJsBean(FetchRequest fetchRequest)
    {
        FetchRequestJsBean jsBean = null;
        if(fetchRequest != null) {
            jsBean = new FetchRequestJsBean();
            jsBean.setGuid(fetchRequest.getGuid());
            jsBean.setUser(fetchRequest.getUser());
            jsBean.setTargetUrl(fetchRequest.getTargetUrl());
            jsBean.setPageUrl(fetchRequest.getPageUrl());
            jsBean.setQueryString(fetchRequest.getQueryString());
            List<KeyValuePairStructJsBean> queryParamsJsBeans = new ArrayList<KeyValuePairStructJsBean>();
            List<KeyValuePairStruct> queryParamsBeans = fetchRequest.getQueryParams();
            if(queryParamsBeans != null) {
                for(KeyValuePairStruct keyValuePairStruct : queryParamsBeans) {
                    KeyValuePairStructJsBean jB = KeyValuePairStructWebService.convertKeyValuePairStructToJsBean(keyValuePairStruct);
                    queryParamsJsBeans.add(jB); 
                }
            }
            jsBean.setQueryParams(queryParamsJsBeans);
            jsBean.setVersion(fetchRequest.getVersion());
            jsBean.setFetchObject(fetchRequest.getFetchObject());
            jsBean.setFetchStatus(fetchRequest.getFetchStatus());
            jsBean.setResult(fetchRequest.getResult());
            jsBean.setNote(fetchRequest.getNote());
            jsBean.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructToJsBean(fetchRequest.getReferrerInfo()));
            jsBean.setStatus(fetchRequest.getStatus());
            jsBean.setNextPageUrl(fetchRequest.getNextPageUrl());
            jsBean.setFollowPages(fetchRequest.getFollowPages());
            jsBean.setFollowDepth(fetchRequest.getFollowDepth());
            jsBean.setCreateVersion(fetchRequest.isCreateVersion());
            jsBean.setDeferred(fetchRequest.isDeferred());
            jsBean.setAlert(fetchRequest.isAlert());
            jsBean.setNotificationPref(NotificationStructWebService.convertNotificationStructToJsBean(fetchRequest.getNotificationPref()));
            jsBean.setCreatedTime(fetchRequest.getCreatedTime());
            jsBean.setModifiedTime(fetchRequest.getModifiedTime());
        }
        return jsBean;
    }

    public static FetchRequest convertFetchRequestJsBeanToBean(FetchRequestJsBean jsBean)
    {
        FetchRequestBean fetchRequest = null;
        if(jsBean != null) {
            fetchRequest = new FetchRequestBean();
            fetchRequest.setGuid(jsBean.getGuid());
            fetchRequest.setUser(jsBean.getUser());
            fetchRequest.setTargetUrl(jsBean.getTargetUrl());
            fetchRequest.setPageUrl(jsBean.getPageUrl());
            fetchRequest.setQueryString(jsBean.getQueryString());
            List<KeyValuePairStruct> queryParamsBeans = new ArrayList<KeyValuePairStruct>();
            List<KeyValuePairStructJsBean> queryParamsJsBeans = jsBean.getQueryParams();
            if(queryParamsJsBeans != null) {
                for(KeyValuePairStructJsBean keyValuePairStruct : queryParamsJsBeans) {
                    KeyValuePairStruct b = KeyValuePairStructWebService.convertKeyValuePairStructJsBeanToBean(keyValuePairStruct);
                    queryParamsBeans.add(b); 
                }
            }
            fetchRequest.setQueryParams(queryParamsBeans);
            fetchRequest.setVersion(jsBean.getVersion());
            fetchRequest.setFetchObject(jsBean.getFetchObject());
            fetchRequest.setFetchStatus(jsBean.getFetchStatus());
            fetchRequest.setResult(jsBean.getResult());
            fetchRequest.setNote(jsBean.getNote());
            fetchRequest.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(jsBean.getReferrerInfo()));
            fetchRequest.setStatus(jsBean.getStatus());
            fetchRequest.setNextPageUrl(jsBean.getNextPageUrl());
            fetchRequest.setFollowPages(jsBean.getFollowPages());
            fetchRequest.setFollowDepth(jsBean.getFollowDepth());
            fetchRequest.setCreateVersion(jsBean.isCreateVersion());
            fetchRequest.setDeferred(jsBean.isDeferred());
            fetchRequest.setAlert(jsBean.isAlert());
            fetchRequest.setNotificationPref(NotificationStructWebService.convertNotificationStructJsBeanToBean(jsBean.getNotificationPref()));
            fetchRequest.setCreatedTime(jsBean.getCreatedTime());
            fetchRequest.setModifiedTime(jsBean.getModifiedTime());
        }
        return fetchRequest;
    }

}
