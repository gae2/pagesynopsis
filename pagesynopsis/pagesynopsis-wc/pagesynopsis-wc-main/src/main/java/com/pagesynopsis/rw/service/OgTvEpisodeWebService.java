package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgTvEpisodeWebService // implements OgTvEpisodeService
{
    private static final Logger log = Logger.getLogger(OgTvEpisodeWebService.class.getName());
     
    // Af service interface.
    private OgTvEpisodeService mService = null;

    public OgTvEpisodeWebService()
    {
        this(ServiceProxyFactory.getInstance().getOgTvEpisodeServiceProxy());
    }
    public OgTvEpisodeWebService(OgTvEpisodeService service)
    {
        mService = service;
    }
    
    protected OgTvEpisodeService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getOgTvEpisodeServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(OgTvEpisodeService service)
    {
        mService = service;
    }
    
    
    public OgTvEpisodeJsBean getOgTvEpisode(String guid) throws WebException
    {
        try {
            OgTvEpisode ogTvEpisode = getServiceProxy().getOgTvEpisode(guid);
            OgTvEpisodeJsBean bean = convertOgTvEpisodeToJsBean(ogTvEpisode);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOgTvEpisode(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getOgTvEpisode(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgTvEpisodeJsBean> getOgTvEpisodes(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgTvEpisodeJsBean> jsBeans = new ArrayList<OgTvEpisodeJsBean>();
            List<OgTvEpisode> ogTvEpisodes = getServiceProxy().getOgTvEpisodes(guids);
            if(ogTvEpisodes != null) {
                for(OgTvEpisode ogTvEpisode : ogTvEpisodes) {
                    jsBeans.add(convertOgTvEpisodeToJsBean(ogTvEpisode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgTvEpisodeJsBean> getAllOgTvEpisodes() throws WebException
    {
        return getAllOgTvEpisodes(null, null, null);
    }

    // @Deprecated
    public List<OgTvEpisodeJsBean> getAllOgTvEpisodes(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgTvEpisodes(ordering, offset, count, null);
    }

    public List<OgTvEpisodeJsBean> getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgTvEpisodeJsBean> jsBeans = new ArrayList<OgTvEpisodeJsBean>();
            List<OgTvEpisode> ogTvEpisodes = getServiceProxy().getAllOgTvEpisodes(ordering, offset, count, forwardCursor);
            if(ogTvEpisodes != null) {
                for(OgTvEpisode ogTvEpisode : ogTvEpisodes) {
                    jsBeans.add(convertOgTvEpisodeToJsBean(ogTvEpisode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgTvEpisodeKeys(ordering, offset, count, null);
    }

    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllOgTvEpisodeKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OgTvEpisodeJsBean> findOgTvEpisodes(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOgTvEpisodes(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OgTvEpisodeJsBean> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OgTvEpisodeJsBean> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgTvEpisodeJsBean> jsBeans = new ArrayList<OgTvEpisodeJsBean>();
            List<OgTvEpisode> ogTvEpisodes = getServiceProxy().findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(ogTvEpisodes != null) {
                for(OgTvEpisode ogTvEpisode : ogTvEpisodes) {
                    jsBeans.add(convertOgTvEpisodeToJsBean(ogTvEpisode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgTvEpisode(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getServiceProxy().createOgTvEpisode(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgTvEpisode(OgTvEpisodeJsBean jsBean) throws WebException
    {
        try {
            OgTvEpisode ogTvEpisode = convertOgTvEpisodeJsBeanToBean(jsBean);
            return getServiceProxy().createOgTvEpisode(ogTvEpisode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgTvEpisodeJsBean constructOgTvEpisode(OgTvEpisodeJsBean jsBean) throws WebException
    {
        try {
            OgTvEpisode ogTvEpisode = convertOgTvEpisodeJsBeanToBean(jsBean);
            ogTvEpisode = getServiceProxy().constructOgTvEpisode(ogTvEpisode);
            jsBean = convertOgTvEpisodeToJsBean(ogTvEpisode);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOgTvEpisode(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getServiceProxy().updateOgTvEpisode(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOgTvEpisode(OgTvEpisodeJsBean jsBean) throws WebException
    {
        try {
            OgTvEpisode ogTvEpisode = convertOgTvEpisodeJsBeanToBean(jsBean);
            return getServiceProxy().updateOgTvEpisode(ogTvEpisode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgTvEpisodeJsBean refreshOgTvEpisode(OgTvEpisodeJsBean jsBean) throws WebException
    {
        try {
            OgTvEpisode ogTvEpisode = convertOgTvEpisodeJsBeanToBean(jsBean);
            ogTvEpisode = getServiceProxy().refreshOgTvEpisode(ogTvEpisode);
            jsBean = convertOgTvEpisodeToJsBean(ogTvEpisode);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgTvEpisode(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteOgTvEpisode(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgTvEpisode(OgTvEpisodeJsBean jsBean) throws WebException
    {
        try {
            OgTvEpisode ogTvEpisode = convertOgTvEpisodeJsBeanToBean(jsBean);
            return getServiceProxy().deleteOgTvEpisode(ogTvEpisode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOgTvEpisodes(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteOgTvEpisodes(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static OgTvEpisodeJsBean convertOgTvEpisodeToJsBean(OgTvEpisode ogTvEpisode)
    {
        OgTvEpisodeJsBean jsBean = null;
        if(ogTvEpisode != null) {
            jsBean = new OgTvEpisodeJsBean();
            jsBean.setGuid(ogTvEpisode.getGuid());
            jsBean.setUrl(ogTvEpisode.getUrl());
            jsBean.setType(ogTvEpisode.getType());
            jsBean.setSiteName(ogTvEpisode.getSiteName());
            jsBean.setTitle(ogTvEpisode.getTitle());
            jsBean.setDescription(ogTvEpisode.getDescription());
            jsBean.setFbAdmins(ogTvEpisode.getFbAdmins());
            jsBean.setFbAppId(ogTvEpisode.getFbAppId());
            List<OgImageStructJsBean> imageJsBeans = new ArrayList<OgImageStructJsBean>();
            List<OgImageStruct> imageBeans = ogTvEpisode.getImage();
            if(imageBeans != null) {
                for(OgImageStruct ogImageStruct : imageBeans) {
                    OgImageStructJsBean jB = OgImageStructWebService.convertOgImageStructToJsBean(ogImageStruct);
                    imageJsBeans.add(jB); 
                }
            }
            jsBean.setImage(imageJsBeans);
            List<OgAudioStructJsBean> audioJsBeans = new ArrayList<OgAudioStructJsBean>();
            List<OgAudioStruct> audioBeans = ogTvEpisode.getAudio();
            if(audioBeans != null) {
                for(OgAudioStruct ogAudioStruct : audioBeans) {
                    OgAudioStructJsBean jB = OgAudioStructWebService.convertOgAudioStructToJsBean(ogAudioStruct);
                    audioJsBeans.add(jB); 
                }
            }
            jsBean.setAudio(audioJsBeans);
            List<OgVideoStructJsBean> videoJsBeans = new ArrayList<OgVideoStructJsBean>();
            List<OgVideoStruct> videoBeans = ogTvEpisode.getVideo();
            if(videoBeans != null) {
                for(OgVideoStruct ogVideoStruct : videoBeans) {
                    OgVideoStructJsBean jB = OgVideoStructWebService.convertOgVideoStructToJsBean(ogVideoStruct);
                    videoJsBeans.add(jB); 
                }
            }
            jsBean.setVideo(videoJsBeans);
            jsBean.setLocale(ogTvEpisode.getLocale());
            jsBean.setLocaleAlternate(ogTvEpisode.getLocaleAlternate());
            jsBean.setDirector(ogTvEpisode.getDirector());
            jsBean.setWriter(ogTvEpisode.getWriter());
            List<OgActorStructJsBean> actorJsBeans = new ArrayList<OgActorStructJsBean>();
            List<OgActorStruct> actorBeans = ogTvEpisode.getActor();
            if(actorBeans != null) {
                for(OgActorStruct ogActorStruct : actorBeans) {
                    OgActorStructJsBean jB = OgActorStructWebService.convertOgActorStructToJsBean(ogActorStruct);
                    actorJsBeans.add(jB); 
                }
            }
            jsBean.setActor(actorJsBeans);
            jsBean.setSeries(ogTvEpisode.getSeries());
            jsBean.setDuration(ogTvEpisode.getDuration());
            jsBean.setTag(ogTvEpisode.getTag());
            jsBean.setReleaseDate(ogTvEpisode.getReleaseDate());
            jsBean.setCreatedTime(ogTvEpisode.getCreatedTime());
            jsBean.setModifiedTime(ogTvEpisode.getModifiedTime());
        }
        return jsBean;
    }

    public static OgTvEpisode convertOgTvEpisodeJsBeanToBean(OgTvEpisodeJsBean jsBean)
    {
        OgTvEpisodeBean ogTvEpisode = null;
        if(jsBean != null) {
            ogTvEpisode = new OgTvEpisodeBean();
            ogTvEpisode.setGuid(jsBean.getGuid());
            ogTvEpisode.setUrl(jsBean.getUrl());
            ogTvEpisode.setType(jsBean.getType());
            ogTvEpisode.setSiteName(jsBean.getSiteName());
            ogTvEpisode.setTitle(jsBean.getTitle());
            ogTvEpisode.setDescription(jsBean.getDescription());
            ogTvEpisode.setFbAdmins(jsBean.getFbAdmins());
            ogTvEpisode.setFbAppId(jsBean.getFbAppId());
            List<OgImageStruct> imageBeans = new ArrayList<OgImageStruct>();
            List<OgImageStructJsBean> imageJsBeans = jsBean.getImage();
            if(imageJsBeans != null) {
                for(OgImageStructJsBean ogImageStruct : imageJsBeans) {
                    OgImageStruct b = OgImageStructWebService.convertOgImageStructJsBeanToBean(ogImageStruct);
                    imageBeans.add(b); 
                }
            }
            ogTvEpisode.setImage(imageBeans);
            List<OgAudioStruct> audioBeans = new ArrayList<OgAudioStruct>();
            List<OgAudioStructJsBean> audioJsBeans = jsBean.getAudio();
            if(audioJsBeans != null) {
                for(OgAudioStructJsBean ogAudioStruct : audioJsBeans) {
                    OgAudioStruct b = OgAudioStructWebService.convertOgAudioStructJsBeanToBean(ogAudioStruct);
                    audioBeans.add(b); 
                }
            }
            ogTvEpisode.setAudio(audioBeans);
            List<OgVideoStruct> videoBeans = new ArrayList<OgVideoStruct>();
            List<OgVideoStructJsBean> videoJsBeans = jsBean.getVideo();
            if(videoJsBeans != null) {
                for(OgVideoStructJsBean ogVideoStruct : videoJsBeans) {
                    OgVideoStruct b = OgVideoStructWebService.convertOgVideoStructJsBeanToBean(ogVideoStruct);
                    videoBeans.add(b); 
                }
            }
            ogTvEpisode.setVideo(videoBeans);
            ogTvEpisode.setLocale(jsBean.getLocale());
            ogTvEpisode.setLocaleAlternate(jsBean.getLocaleAlternate());
            ogTvEpisode.setDirector(jsBean.getDirector());
            ogTvEpisode.setWriter(jsBean.getWriter());
            List<OgActorStruct> actorBeans = new ArrayList<OgActorStruct>();
            List<OgActorStructJsBean> actorJsBeans = jsBean.getActor();
            if(actorJsBeans != null) {
                for(OgActorStructJsBean ogActorStruct : actorJsBeans) {
                    OgActorStruct b = OgActorStructWebService.convertOgActorStructJsBeanToBean(ogActorStruct);
                    actorBeans.add(b); 
                }
            }
            ogTvEpisode.setActor(actorBeans);
            ogTvEpisode.setSeries(jsBean.getSeries());
            ogTvEpisode.setDuration(jsBean.getDuration());
            ogTvEpisode.setTag(jsBean.getTag());
            ogTvEpisode.setReleaseDate(jsBean.getReleaseDate());
            ogTvEpisode.setCreatedTime(jsBean.getCreatedTime());
            ogTvEpisode.setModifiedTime(jsBean.getModifiedTime());
        }
        return ogTvEpisode;
    }

}
