package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgImageStructWebService;


// MockOgImageStructWebService is a mock object.
// It can be used as a base class to mock OgImageStructWebService objects.
public abstract class MockOgImageStructWebService extends OgImageStructWebService  // implements OgImageStructService
{
    private static final Logger log = Logger.getLogger(MockOgImageStructWebService.class.getName());
     

}
