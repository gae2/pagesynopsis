package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.service.OgVideoService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgVideoWebService;


// MockOgVideoWebService is a mock object.
// It can be used as a base class to mock OgVideoWebService objects.
public abstract class MockOgVideoWebService extends OgVideoWebService  // implements OgVideoService
{
    private static final Logger log = Logger.getLogger(MockOgVideoWebService.class.getName());
     
    // Af service interface.
    private OgVideoWebService mService = null;

    public MockOgVideoWebService()
    {
        this(MockServiceProxyFactory.getInstance().getOgVideoServiceProxy());
    }
    public MockOgVideoWebService(OgVideoService service)
    {
        super(service);
    }


}
