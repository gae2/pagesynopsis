package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.OgVideoStub;
import com.pagesynopsis.ws.stub.OgVideoListStub;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.TwitterProductCardListStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardListStub;
import com.pagesynopsis.ws.stub.OgBlogStub;
import com.pagesynopsis.ws.stub.OgBlogListStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardListStub;
import com.pagesynopsis.ws.stub.UrlStructStub;
import com.pagesynopsis.ws.stub.UrlStructListStub;
import com.pagesynopsis.ws.stub.ImageStructStub;
import com.pagesynopsis.ws.stub.ImageStructListStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardListStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardListStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgTvShowListStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgBookListStub;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgWebsiteListStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.stub.OgMovieListStub;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.TwitterAppCardListStub;
import com.pagesynopsis.ws.stub.AnchorStructStub;
import com.pagesynopsis.ws.stub.AnchorStructListStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructListStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgArticleListStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeListStub;
import com.pagesynopsis.ws.stub.AudioStructStub;
import com.pagesynopsis.ws.stub.AudioStructListStub;
import com.pagesynopsis.ws.stub.VideoStructStub;
import com.pagesynopsis.ws.stub.VideoStructListStub;
import com.pagesynopsis.ws.stub.OgProfileStub;
import com.pagesynopsis.ws.stub.OgProfileListStub;
import com.pagesynopsis.ws.stub.LinkListStub;
import com.pagesynopsis.ws.stub.LinkListListStub;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.LinkListBean;
import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.LinkListServiceProxy;


// MockLinkListServiceProxy is a decorator.
// It can be used as a base class to mock LinkListService objects.
public abstract class MockLinkListServiceProxy extends LinkListServiceProxy implements LinkListService
{
    private static final Logger log = Logger.getLogger(MockLinkListServiceProxy.class.getName());

    // MockLinkListServiceProxy uses the decorator design pattern.
    private LinkListService decoratedProxy;

    public MockLinkListServiceProxy(LinkListService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected LinkListService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(LinkListService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public LinkList getLinkList(String guid) throws BaseException
    {
        return decoratedProxy.getLinkList(guid);
    }

    @Override
    public Object getLinkList(String guid, String field) throws BaseException
    {
        return decoratedProxy.getLinkList(guid, field);
    }

    @Override
    public List<LinkList> getLinkLists(List<String> guids) throws BaseException
    {
        return decoratedProxy.getLinkLists(guids);
    }

    @Override
    public List<LinkList> getAllLinkLists() throws BaseException
    {
        return getAllLinkLists(null, null, null);
    }

    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllLinkLists(ordering, offset, count, null);
    }

    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllLinkLists(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllLinkListKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllLinkListKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findLinkLists(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findLinkListKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createLinkList(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        return decoratedProxy.createLinkList(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
    }

    @Override
    public String createLinkList(LinkList bean) throws BaseException
    {
        return decoratedProxy.createLinkList(bean);
    }

    @Override
    public LinkList constructLinkList(LinkList bean) throws BaseException
    {
        return decoratedProxy.constructLinkList(bean);
    }

    @Override
    public Boolean updateLinkList(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        return decoratedProxy.updateLinkList(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
    }

    @Override
    public Boolean updateLinkList(LinkList bean) throws BaseException
    {
        return decoratedProxy.updateLinkList(bean);
    }

    @Override
    public LinkList refreshLinkList(LinkList bean) throws BaseException
    {
        return decoratedProxy.refreshLinkList(bean);
    }

    @Override
    public Boolean deleteLinkList(String guid) throws BaseException
    {
        return decoratedProxy.deleteLinkList(guid);
    }

    @Override
    public Boolean deleteLinkList(LinkList bean) throws BaseException
    {
        return decoratedProxy.deleteLinkList(bean);
    }

    @Override
    public Long deleteLinkLists(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteLinkLists(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createLinkLists(List<LinkList> linkLists) throws BaseException
    {
        return decoratedProxy.createLinkLists(linkLists);
    }

    // TBD
    //@Override
    //public Boolean updateLinkLists(List<LinkList> linkLists) throws BaseException
    //{
    //}

}
