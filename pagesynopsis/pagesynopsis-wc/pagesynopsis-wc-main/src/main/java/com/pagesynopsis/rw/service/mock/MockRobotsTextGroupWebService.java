package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.RobotsTextGroupJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.RobotsTextGroupWebService;


// MockRobotsTextGroupWebService is a mock object.
// It can be used as a base class to mock RobotsTextGroupWebService objects.
public abstract class MockRobotsTextGroupWebService extends RobotsTextGroupWebService  // implements RobotsTextGroupService
{
    private static final Logger log = Logger.getLogger(MockRobotsTextGroupWebService.class.getName());
     

}
