package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.ReferrerInfoStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.ReferrerInfoStructWebService;


// MockReferrerInfoStructWebService is a mock object.
// It can be used as a base class to mock ReferrerInfoStructWebService objects.
public abstract class MockReferrerInfoStructWebService extends ReferrerInfoStructWebService  // implements ReferrerInfoStructService
{
    private static final Logger log = Logger.getLogger(MockReferrerInfoStructWebService.class.getName());
     

}
