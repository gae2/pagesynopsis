package com.pagesynopsis.rf.proxy;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgAudioStructListStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgImageStructListStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgActorStructListStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructListStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgBookListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;

import com.pagesynopsis.rf.config.Config;


public class OgBookServiceProxy extends AbstractBaseServiceProxy implements OgBookService
{
    private static final Logger log = Logger.getLogger(OgBookServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding ws.resource.impl class.
    private final static String RESOURCE_OGBOOK = "ogBooks";


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    public OgBookServiceProxy()
    {
        initCache();
    }


    protected WebResource getOgBookWebResource()
    {
        return getWebResource(RESOURCE_OGBOOK);
    }
    protected WebResource getOgBookWebResource(String path)
    {
        return getWebResource(RESOURCE_OGBOOK, path);
    }
    protected WebResource getOgBookWebResourceByGuid(String guid)
    {
        return getOgBookWebResource(guid);
    }


    @Override
    public OgBook getOgBook(String guid) throws BaseException
    {
        OgBook bean = null;

        String key = getResourcePath(RESOURCE_OGBOOK, guid);
        CacheEntry entry = null;
        if(mCache != null) {
            entry = mCache.getCacheEntry(key);
            if(entry != null) {
                // TBD: eTag, lastModified, expires, etc....
            }
        }

        WebResource webResource = getOgBookWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                OgBookStub stub = clientResponse.getEntity(OgBookStub.class);
                bean = MarshalHelper.convertOgBookToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgBook bean = " + bean);
                break;
            // case StatusCode.NOT_MODIFIED:
            //     // TBD
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Object getOgBook(String guid, String field) throws BaseException
    {
        OgBook bean = getOgBook(guid);
        if(bean == null) {
            return null;
        }

        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return bean.getUrl();
        } else if(field.equals("type")) {
            return bean.getType();
        } else if(field.equals("siteName")) {
            return bean.getSiteName();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("fbAdmins")) {
            return bean.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return bean.getFbAppId();
        } else if(field.equals("image")) {
            return bean.getImage();
        } else if(field.equals("audio")) {
            return bean.getAudio();
        } else if(field.equals("video")) {
            return bean.getVideo();
        } else if(field.equals("locale")) {
            return bean.getLocale();
        } else if(field.equals("localeAlternate")) {
            return bean.getLocaleAlternate();
        } else if(field.equals("author")) {
            return bean.getAuthor();
        } else if(field.equals("isbn")) {
            return bean.getIsbn();
        } else if(field.equals("tag")) {
            return bean.getTag();
        } else if(field.equals("releaseDate")) {
            return bean.getReleaseDate();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgBook> getOgBooks(List<String> guids) throws BaseException
    {
        throw new NotImplementedException("To be implemented.");
    }

    @Override
    public List<OgBook> getAllOgBooks() throws BaseException
    {
        return getAllOgBooks(null, null, null);
    }

    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBooks(ordering, offset, count, null);
    }

    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<OgBook> list = null;
    	
        WebResource webResource = getOgBookWebResource(RESOURCE_PATH_ALL);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                OgBookListStub stub = clientResponse.getEntity(OgBookListStub.class);
                list = MarshalHelper.convertOgBookListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgBook list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBookKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<String> list = null;
    	
        WebResource webResource = getOgBookWebResource(RESOURCE_PATH_ALLKEYS);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgBook key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgBooks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<OgBook> list = null;
    	
//        ClientResponse clientResponse = getOgBookWebResource()
//        	.queryParam("filter", filter)
//        	.queryParam("ordering", ordering)
//        	.queryParam("params", params)
//        	//.queryParam("values", values)  // ???
//        	.accept(getOutputMediaType())
//        	.get(ClientResponse.class);

    	WebResource webResource = getOgBookWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                OgBookListStub stub = clientResponse.getEntity(OgBookListStub.class);
                list = MarshalHelper.convertOgBookListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgBook list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<String> list = null;

    	WebResource webResource = getOgBookWebResource(RESOURCE_PATH_KEYS);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgBook key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getOgBookWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(aggregate != null) {
            webResource = webResource.queryParam("aggregate", aggregate);
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgBook count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }

    @Override
    public String createOgBook(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException
    {
        OgBookBean bean = new OgBookBean(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
        return createOgBook(bean);        
    }

    @Override
    public String createOgBook(OgBook bean) throws BaseException
    {
        String guid = null;
        OgBookStub stub = MarshalHelper.convertOgBookToStub(bean);
        WebResource webResource = getOgBookWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                guid = clientResponse.getEntity(String.class);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New OgBook guid = " + guid);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New OgBook resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return guid;
    }

    @Override
    public OgBook constructOgBook(OgBook bean) throws BaseException
    {
        OgBookStub stub = MarshalHelper.convertOgBookToStub(bean);
        WebResource webResource = getOgBookWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                stub = clientResponse.getEntity(OgBookStub.class);
                bean = MarshalHelper.convertOgBookToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New OgBook bean = " + bean);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New OgBook resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean updateOgBook(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgBook guid is invalid.");
        	throw new BaseException("OgBook guid is invalid.");
        }
        OgBookBean bean = new OgBookBean(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
        return updateOgBook(bean);        
    }

    @Override
    public Boolean updateOgBook(OgBook bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgBook object is invalid.");
        	throw new BaseException("OgBook object is invalid.");
        }
        OgBookStub stub = MarshalHelper.convertOgBookToStub(bean);

        WebResource webResource = getOgBookWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        //ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).put(ClientResponse.class, stub);
        ClientResponse clientResponse = webResource.type(getInputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully updated the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public OgBook refreshOgBook(OgBook bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgBook object is invalid.");
        	throw new BaseException("OgBook object is invalid.");
        }
        OgBookStub stub = MarshalHelper.convertOgBookToStub(bean);

        WebResource webResource = getOgBookWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                stub = clientResponse.getEntity(OgBookStub.class);
                bean = MarshalHelper.convertOgBookToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully refreshed the OgBook bean = " + bean);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean deleteOgBook(String guid) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgBook guid is invalid.");
        	throw new BaseException("OgBook guid is invalid.");
        }

        WebResource webResource = getOgBookWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully deleted the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public Boolean deleteOgBook(OgBook bean) throws BaseException
    {
        String guid = bean.getGuid();
        return deleteOgBook(guid);
    }

    @Override
    public Long deleteOgBooks(String filter, String params, List<String> values) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getOgBookWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // Currently, the data access layer throws exception if the filter is empty.
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface. Or, try comma-separated list???
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        // Order of the mime types ???
        ClientResponse clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Deleted OgBooks: count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGBOOK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgBooks(List<OgBook> ogBooks) throws BaseException
    {
        log.finer("BEGIN");

        if(ogBooks == null) {
            log.log(Level.WARNING, "createOgBooks() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = ogBooks.size();
        if(size == 0) {
            log.log(Level.WARNING, "createOgBooks() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(OgBook ogBook : ogBooks) {
            String guid = createOgBook(ogBook);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createOgBooks() failed for at least one ogBook. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateOgBooks(List<OgBook> ogBooks) throws BaseException
    //{
    //}

}
