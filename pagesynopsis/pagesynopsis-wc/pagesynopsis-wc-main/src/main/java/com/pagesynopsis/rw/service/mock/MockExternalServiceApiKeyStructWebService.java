package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.ExternalServiceApiKeyStruct;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.ExternalServiceApiKeyStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.ExternalServiceApiKeyStructWebService;


// MockExternalServiceApiKeyStructWebService is a mock object.
// It can be used as a base class to mock ExternalServiceApiKeyStructWebService objects.
public abstract class MockExternalServiceApiKeyStructWebService extends ExternalServiceApiKeyStructWebService  // implements ExternalServiceApiKeyStructService
{
    private static final Logger log = Logger.getLogger(MockExternalServiceApiKeyStructWebService.class.getName());
     

}
