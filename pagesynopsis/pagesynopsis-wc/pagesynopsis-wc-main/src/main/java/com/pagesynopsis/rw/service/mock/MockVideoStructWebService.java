package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.MediaSourceStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.VideoStructWebService;


// MockVideoStructWebService is a mock object.
// It can be used as a base class to mock VideoStructWebService objects.
public abstract class MockVideoStructWebService extends VideoStructWebService  // implements VideoStructService
{
    private static final Logger log = Logger.getLogger(MockVideoStructWebService.class.getName());
     

}
