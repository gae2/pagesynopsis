package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgVideoStructWebService;


// MockOgVideoStructWebService is a mock object.
// It can be used as a base class to mock OgVideoStructWebService objects.
public abstract class MockOgVideoStructWebService extends OgVideoStructWebService  // implements OgVideoStructService
{
    private static final Logger log = Logger.getLogger(MockOgVideoStructWebService.class.getName());
     

}
