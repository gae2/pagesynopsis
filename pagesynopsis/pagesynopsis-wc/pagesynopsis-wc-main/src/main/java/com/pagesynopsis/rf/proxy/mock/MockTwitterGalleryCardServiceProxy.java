package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoListStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataListStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardListStub;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.service.TwitterGalleryCardService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.TwitterGalleryCardServiceProxy;


// MockTwitterGalleryCardServiceProxy is a decorator.
// It can be used as a base class to mock TwitterGalleryCardService objects.
public abstract class MockTwitterGalleryCardServiceProxy extends TwitterGalleryCardServiceProxy implements TwitterGalleryCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterGalleryCardServiceProxy.class.getName());

    // MockTwitterGalleryCardServiceProxy uses the decorator design pattern.
    private TwitterGalleryCardService decoratedProxy;

    public MockTwitterGalleryCardServiceProxy(TwitterGalleryCardService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected TwitterGalleryCardService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(TwitterGalleryCardService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public TwitterGalleryCard getTwitterGalleryCard(String guid) throws BaseException
    {
        return decoratedProxy.getTwitterGalleryCard(guid);
    }

    @Override
    public Object getTwitterGalleryCard(String guid, String field) throws BaseException
    {
        return decoratedProxy.getTwitterGalleryCard(guid, field);
    }

    @Override
    public List<TwitterGalleryCard> getTwitterGalleryCards(List<String> guids) throws BaseException
    {
        return decoratedProxy.getTwitterGalleryCards(guids);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards() throws BaseException
    {
        return getAllTwitterGalleryCards(null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterGalleryCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterGalleryCard> getAllTwitterGalleryCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterGalleryCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterGalleryCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterGalleryCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterGalleryCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterGalleryCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterGalleryCard> findTwitterGalleryCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterGalleryCards(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterGalleryCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterGalleryCardKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterGalleryCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        return decoratedProxy.createTwitterGalleryCard(card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
    }

    @Override
    public String createTwitterGalleryCard(TwitterGalleryCard bean) throws BaseException
    {
        return decoratedProxy.createTwitterGalleryCard(bean);
    }

    @Override
    public TwitterGalleryCard constructTwitterGalleryCard(TwitterGalleryCard bean) throws BaseException
    {
        return decoratedProxy.constructTwitterGalleryCard(bean);
    }

    @Override
    public Boolean updateTwitterGalleryCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image0, String image1, String image2, String image3) throws BaseException
    {
        return decoratedProxy.updateTwitterGalleryCard(guid, card, url, title, description, site, siteId, creator, creatorId, image0, image1, image2, image3);
    }

    @Override
    public Boolean updateTwitterGalleryCard(TwitterGalleryCard bean) throws BaseException
    {
        return decoratedProxy.updateTwitterGalleryCard(bean);
    }

    @Override
    public TwitterGalleryCard refreshTwitterGalleryCard(TwitterGalleryCard bean) throws BaseException
    {
        return decoratedProxy.refreshTwitterGalleryCard(bean);
    }

    @Override
    public Boolean deleteTwitterGalleryCard(String guid) throws BaseException
    {
        return decoratedProxy.deleteTwitterGalleryCard(guid);
    }

    @Override
    public Boolean deleteTwitterGalleryCard(TwitterGalleryCard bean) throws BaseException
    {
        return decoratedProxy.deleteTwitterGalleryCard(bean);
    }

    @Override
    public Long deleteTwitterGalleryCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteTwitterGalleryCards(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException
    {
        return decoratedProxy.createTwitterGalleryCards(twitterGalleryCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterGalleryCards(List<TwitterGalleryCard> twitterGalleryCards) throws BaseException
    //{
    //}

}
