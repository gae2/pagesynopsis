package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.KeyValueRelationStruct;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.KeyValueRelationStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.KeyValueRelationStructWebService;


// MockKeyValueRelationStructWebService is a mock object.
// It can be used as a base class to mock KeyValueRelationStructWebService objects.
public abstract class MockKeyValueRelationStructWebService extends KeyValueRelationStructWebService  // implements KeyValueRelationStructService
{
    private static final Logger log = Logger.getLogger(MockKeyValueRelationStructWebService.class.getName());
     

}
