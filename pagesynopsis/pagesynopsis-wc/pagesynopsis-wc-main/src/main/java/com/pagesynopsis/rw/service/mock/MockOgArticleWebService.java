package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgArticleWebService;


// MockOgArticleWebService is a mock object.
// It can be used as a base class to mock OgArticleWebService objects.
public abstract class MockOgArticleWebService extends OgArticleWebService  // implements OgArticleService
{
    private static final Logger log = Logger.getLogger(MockOgArticleWebService.class.getName());
     
    // Af service interface.
    private OgArticleWebService mService = null;

    public MockOgArticleWebService()
    {
        this(MockServiceProxyFactory.getInstance().getOgArticleServiceProxy());
    }
    public MockOgArticleWebService(OgArticleService service)
    {
        super(service);
    }


}
