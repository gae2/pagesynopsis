package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.af.bean.UrlRatingBean;
import com.pagesynopsis.af.service.UrlRatingService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.UrlRatingJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UrlRatingWebService // implements UrlRatingService
{
    private static final Logger log = Logger.getLogger(UrlRatingWebService.class.getName());
     
    // Af service interface.
    private UrlRatingService mService = null;

    public UrlRatingWebService()
    {
        this(ServiceProxyFactory.getInstance().getUrlRatingServiceProxy());
    }
    public UrlRatingWebService(UrlRatingService service)
    {
        mService = service;
    }
    
    protected UrlRatingService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getUrlRatingServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(UrlRatingService service)
    {
        mService = service;
    }
    
    
    public UrlRatingJsBean getUrlRating(String guid) throws WebException
    {
        try {
            UrlRating urlRating = getServiceProxy().getUrlRating(guid);
            UrlRatingJsBean bean = convertUrlRatingToJsBean(urlRating);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUrlRating(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getUrlRating(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UrlRatingJsBean> getUrlRatings(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UrlRatingJsBean> jsBeans = new ArrayList<UrlRatingJsBean>();
            List<UrlRating> urlRatings = getServiceProxy().getUrlRatings(guids);
            if(urlRatings != null) {
                for(UrlRating urlRating : urlRatings) {
                    jsBeans.add(convertUrlRatingToJsBean(urlRating));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UrlRatingJsBean> getAllUrlRatings() throws WebException
    {
        return getAllUrlRatings(null, null, null);
    }

    // @Deprecated
    public List<UrlRatingJsBean> getAllUrlRatings(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllUrlRatings(ordering, offset, count, null);
    }

    public List<UrlRatingJsBean> getAllUrlRatings(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<UrlRatingJsBean> jsBeans = new ArrayList<UrlRatingJsBean>();
            List<UrlRating> urlRatings = getServiceProxy().getAllUrlRatings(ordering, offset, count, forwardCursor);
            if(urlRatings != null) {
                for(UrlRating urlRating : urlRatings) {
                    jsBeans.add(convertUrlRatingToJsBean(urlRating));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllUrlRatingKeys(ordering, offset, count, null);
    }

    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllUrlRatingKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<UrlRatingJsBean> findUrlRatings(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUrlRatings(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<UrlRatingJsBean> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<UrlRatingJsBean> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<UrlRatingJsBean> jsBeans = new ArrayList<UrlRatingJsBean>();
            List<UrlRating> urlRatings = getServiceProxy().findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(urlRatings != null) {
                for(UrlRating urlRating : urlRatings) {
                    jsBeans.add(convertUrlRatingToJsBean(urlRating));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUrlRating(String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws WebException
    {
        try {
            return getServiceProxy().createUrlRating(domain, pageUrl, preview, flag, rating, note, status, ratedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUrlRating(UrlRatingJsBean jsBean) throws WebException
    {
        try {
            UrlRating urlRating = convertUrlRatingJsBeanToBean(jsBean);
            return getServiceProxy().createUrlRating(urlRating);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UrlRatingJsBean constructUrlRating(UrlRatingJsBean jsBean) throws WebException
    {
        try {
            UrlRating urlRating = convertUrlRatingJsBeanToBean(jsBean);
            urlRating = getServiceProxy().constructUrlRating(urlRating);
            jsBean = convertUrlRatingToJsBean(urlRating);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUrlRating(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws WebException
    {
        try {
            return getServiceProxy().updateUrlRating(guid, domain, pageUrl, preview, flag, rating, note, status, ratedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUrlRating(UrlRatingJsBean jsBean) throws WebException
    {
        try {
            UrlRating urlRating = convertUrlRatingJsBeanToBean(jsBean);
            return getServiceProxy().updateUrlRating(urlRating);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UrlRatingJsBean refreshUrlRating(UrlRatingJsBean jsBean) throws WebException
    {
        try {
            UrlRating urlRating = convertUrlRatingJsBeanToBean(jsBean);
            urlRating = getServiceProxy().refreshUrlRating(urlRating);
            jsBean = convertUrlRatingToJsBean(urlRating);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUrlRating(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteUrlRating(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUrlRating(UrlRatingJsBean jsBean) throws WebException
    {
        try {
            UrlRating urlRating = convertUrlRatingJsBeanToBean(jsBean);
            return getServiceProxy().deleteUrlRating(urlRating);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUrlRatings(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteUrlRatings(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static UrlRatingJsBean convertUrlRatingToJsBean(UrlRating urlRating)
    {
        UrlRatingJsBean jsBean = null;
        if(urlRating != null) {
            jsBean = new UrlRatingJsBean();
            jsBean.setGuid(urlRating.getGuid());
            jsBean.setDomain(urlRating.getDomain());
            jsBean.setPageUrl(urlRating.getPageUrl());
            jsBean.setPreview(urlRating.getPreview());
            jsBean.setFlag(urlRating.getFlag());
            jsBean.setRating(urlRating.getRating());
            jsBean.setNote(urlRating.getNote());
            jsBean.setStatus(urlRating.getStatus());
            jsBean.setRatedTime(urlRating.getRatedTime());
            jsBean.setCreatedTime(urlRating.getCreatedTime());
            jsBean.setModifiedTime(urlRating.getModifiedTime());
        }
        return jsBean;
    }

    public static UrlRating convertUrlRatingJsBeanToBean(UrlRatingJsBean jsBean)
    {
        UrlRatingBean urlRating = null;
        if(jsBean != null) {
            urlRating = new UrlRatingBean();
            urlRating.setGuid(jsBean.getGuid());
            urlRating.setDomain(jsBean.getDomain());
            urlRating.setPageUrl(jsBean.getPageUrl());
            urlRating.setPreview(jsBean.getPreview());
            urlRating.setFlag(jsBean.getFlag());
            urlRating.setRating(jsBean.getRating());
            urlRating.setNote(jsBean.getNote());
            urlRating.setStatus(jsBean.getStatus());
            urlRating.setRatedTime(jsBean.getRatedTime());
            urlRating.setCreatedTime(jsBean.getCreatedTime());
            urlRating.setModifiedTime(jsBean.getModifiedTime());
        }
        return urlRating;
    }

}
