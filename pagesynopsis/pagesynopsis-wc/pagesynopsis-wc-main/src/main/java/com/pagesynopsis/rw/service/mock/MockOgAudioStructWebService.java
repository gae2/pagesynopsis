package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgAudioStructWebService;


// MockOgAudioStructWebService is a mock object.
// It can be used as a base class to mock OgAudioStructWebService objects.
public abstract class MockOgAudioStructWebService extends OgAudioStructWebService  // implements OgAudioStructService
{
    private static final Logger log = Logger.getLogger(MockOgAudioStructWebService.class.getName());
     

}
