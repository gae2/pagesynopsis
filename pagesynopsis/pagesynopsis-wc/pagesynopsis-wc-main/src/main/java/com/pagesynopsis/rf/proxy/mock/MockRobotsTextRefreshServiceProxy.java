package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.RobotsTextRefreshStub;
import com.pagesynopsis.ws.stub.RobotsTextRefreshListStub;
import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.RobotsTextRefreshServiceProxy;


// MockRobotsTextRefreshServiceProxy is a decorator.
// It can be used as a base class to mock RobotsTextRefreshService objects.
public abstract class MockRobotsTextRefreshServiceProxy extends RobotsTextRefreshServiceProxy implements RobotsTextRefreshService
{
    private static final Logger log = Logger.getLogger(MockRobotsTextRefreshServiceProxy.class.getName());

    // MockRobotsTextRefreshServiceProxy uses the decorator design pattern.
    private RobotsTextRefreshService decoratedProxy;

    public MockRobotsTextRefreshServiceProxy(RobotsTextRefreshService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected RobotsTextRefreshService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(RobotsTextRefreshService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public RobotsTextRefresh getRobotsTextRefresh(String guid) throws BaseException
    {
        return decoratedProxy.getRobotsTextRefresh(guid);
    }

    @Override
    public Object getRobotsTextRefresh(String guid, String field) throws BaseException
    {
        return decoratedProxy.getRobotsTextRefresh(guid, field);
    }

    @Override
    public List<RobotsTextRefresh> getRobotsTextRefreshes(List<String> guids) throws BaseException
    {
        return decoratedProxy.getRobotsTextRefreshes(guids);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes() throws BaseException
    {
        return getAllRobotsTextRefreshes(null, null, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextRefreshes(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextRefreshKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findRobotsTextRefreshes(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createRobotsTextRefresh(String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        return decoratedProxy.createRobotsTextRefresh(robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
    }

    @Override
    public String createRobotsTextRefresh(RobotsTextRefresh bean) throws BaseException
    {
        return decoratedProxy.createRobotsTextRefresh(bean);
    }

    @Override
    public RobotsTextRefresh constructRobotsTextRefresh(RobotsTextRefresh bean) throws BaseException
    {
        return decoratedProxy.constructRobotsTextRefresh(bean);
    }

    @Override
    public Boolean updateRobotsTextRefresh(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        return decoratedProxy.updateRobotsTextRefresh(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
    }

    @Override
    public Boolean updateRobotsTextRefresh(RobotsTextRefresh bean) throws BaseException
    {
        return decoratedProxy.updateRobotsTextRefresh(bean);
    }

    @Override
    public RobotsTextRefresh refreshRobotsTextRefresh(RobotsTextRefresh bean) throws BaseException
    {
        return decoratedProxy.refreshRobotsTextRefresh(bean);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(String guid) throws BaseException
    {
        return decoratedProxy.deleteRobotsTextRefresh(guid);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(RobotsTextRefresh bean) throws BaseException
    {
        return decoratedProxy.deleteRobotsTextRefresh(bean);
    }

    @Override
    public Long deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteRobotsTextRefreshes(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createRobotsTextRefreshes(List<RobotsTextRefresh> robotsTextRefreshes) throws BaseException
    {
        return decoratedProxy.createRobotsTextRefreshes(robotsTextRefreshes);
    }

    // TBD
    //@Override
    //public Boolean updateRobotsTextRefreshes(List<RobotsTextRefresh> robotsTextRefreshes) throws BaseException
    //{
    //}

}
