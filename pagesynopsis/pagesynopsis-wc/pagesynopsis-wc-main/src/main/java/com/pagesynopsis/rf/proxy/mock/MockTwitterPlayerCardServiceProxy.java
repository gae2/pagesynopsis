package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoListStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataListStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardListStub;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.service.TwitterPlayerCardService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.TwitterPlayerCardServiceProxy;


// MockTwitterPlayerCardServiceProxy is a decorator.
// It can be used as a base class to mock TwitterPlayerCardService objects.
public abstract class MockTwitterPlayerCardServiceProxy extends TwitterPlayerCardServiceProxy implements TwitterPlayerCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterPlayerCardServiceProxy.class.getName());

    // MockTwitterPlayerCardServiceProxy uses the decorator design pattern.
    private TwitterPlayerCardService decoratedProxy;

    public MockTwitterPlayerCardServiceProxy(TwitterPlayerCardService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected TwitterPlayerCardService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(TwitterPlayerCardService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public TwitterPlayerCard getTwitterPlayerCard(String guid) throws BaseException
    {
        return decoratedProxy.getTwitterPlayerCard(guid);
    }

    @Override
    public Object getTwitterPlayerCard(String guid, String field) throws BaseException
    {
        return decoratedProxy.getTwitterPlayerCard(guid, field);
    }

    @Override
    public List<TwitterPlayerCard> getTwitterPlayerCards(List<String> guids) throws BaseException
    {
        return decoratedProxy.getTwitterPlayerCards(guids);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards() throws BaseException
    {
        return getAllTwitterPlayerCards(null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPlayerCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterPlayerCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPlayerCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterPlayerCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterPlayerCards(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterPlayerCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        return decoratedProxy.createTwitterPlayerCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
    }

    @Override
    public String createTwitterPlayerCard(TwitterPlayerCard bean) throws BaseException
    {
        return decoratedProxy.createTwitterPlayerCard(bean);
    }

    @Override
    public TwitterPlayerCard constructTwitterPlayerCard(TwitterPlayerCard bean) throws BaseException
    {
        return decoratedProxy.constructTwitterPlayerCard(bean);
    }

    @Override
    public Boolean updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws BaseException
    {
        return decoratedProxy.updateTwitterPlayerCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
    }

    @Override
    public Boolean updateTwitterPlayerCard(TwitterPlayerCard bean) throws BaseException
    {
        return decoratedProxy.updateTwitterPlayerCard(bean);
    }

    @Override
    public TwitterPlayerCard refreshTwitterPlayerCard(TwitterPlayerCard bean) throws BaseException
    {
        return decoratedProxy.refreshTwitterPlayerCard(bean);
    }

    @Override
    public Boolean deleteTwitterPlayerCard(String guid) throws BaseException
    {
        return decoratedProxy.deleteTwitterPlayerCard(guid);
    }

    @Override
    public Boolean deleteTwitterPlayerCard(TwitterPlayerCard bean) throws BaseException
    {
        return decoratedProxy.deleteTwitterPlayerCard(bean);
    }

    @Override
    public Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteTwitterPlayerCards(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException
    {
        return decoratedProxy.createTwitterPlayerCards(twitterPlayerCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException
    //{
    //}

}
