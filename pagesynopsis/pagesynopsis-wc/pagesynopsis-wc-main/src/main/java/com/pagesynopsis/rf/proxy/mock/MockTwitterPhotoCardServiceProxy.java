package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoListStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataListStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardListStub;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.service.TwitterPhotoCardService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.TwitterPhotoCardServiceProxy;


// MockTwitterPhotoCardServiceProxy is a decorator.
// It can be used as a base class to mock TwitterPhotoCardService objects.
public abstract class MockTwitterPhotoCardServiceProxy extends TwitterPhotoCardServiceProxy implements TwitterPhotoCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterPhotoCardServiceProxy.class.getName());

    // MockTwitterPhotoCardServiceProxy uses the decorator design pattern.
    private TwitterPhotoCardService decoratedProxy;

    public MockTwitterPhotoCardServiceProxy(TwitterPhotoCardService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected TwitterPhotoCardService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(TwitterPhotoCardService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public TwitterPhotoCard getTwitterPhotoCard(String guid) throws BaseException
    {
        return decoratedProxy.getTwitterPhotoCard(guid);
    }

    @Override
    public Object getTwitterPhotoCard(String guid, String field) throws BaseException
    {
        return decoratedProxy.getTwitterPhotoCard(guid, field);
    }

    @Override
    public List<TwitterPhotoCard> getTwitterPhotoCards(List<String> guids) throws BaseException
    {
        return decoratedProxy.getTwitterPhotoCards(guids);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards() throws BaseException
    {
        return getAllTwitterPhotoCards(null, null, null);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPhotoCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPhotoCard> getAllTwitterPhotoCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterPhotoCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPhotoCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTwitterPhotoCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterPhotoCard> findTwitterPhotoCards(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterPhotoCards(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTwitterPhotoCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return decoratedProxy.createTwitterPhotoCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public String createTwitterPhotoCard(TwitterPhotoCard bean) throws BaseException
    {
        return decoratedProxy.createTwitterPhotoCard(bean);
    }

    @Override
    public TwitterPhotoCard constructTwitterPhotoCard(TwitterPhotoCard bean) throws BaseException
    {
        return decoratedProxy.constructTwitterPhotoCard(bean);
    }

    @Override
    public Boolean updateTwitterPhotoCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws BaseException
    {
        return decoratedProxy.updateTwitterPhotoCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
    }

    @Override
    public Boolean updateTwitterPhotoCard(TwitterPhotoCard bean) throws BaseException
    {
        return decoratedProxy.updateTwitterPhotoCard(bean);
    }

    @Override
    public TwitterPhotoCard refreshTwitterPhotoCard(TwitterPhotoCard bean) throws BaseException
    {
        return decoratedProxy.refreshTwitterPhotoCard(bean);
    }

    @Override
    public Boolean deleteTwitterPhotoCard(String guid) throws BaseException
    {
        return decoratedProxy.deleteTwitterPhotoCard(guid);
    }

    @Override
    public Boolean deleteTwitterPhotoCard(TwitterPhotoCard bean) throws BaseException
    {
        return decoratedProxy.deleteTwitterPhotoCard(bean);
    }

    @Override
    public Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteTwitterPhotoCards(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterPhotoCards(List<TwitterPhotoCard> twitterPhotoCards) throws BaseException
    {
        return decoratedProxy.createTwitterPhotoCards(twitterPhotoCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterPhotoCards(List<TwitterPhotoCard> twitterPhotoCards) throws BaseException
    //{
    //}

}
