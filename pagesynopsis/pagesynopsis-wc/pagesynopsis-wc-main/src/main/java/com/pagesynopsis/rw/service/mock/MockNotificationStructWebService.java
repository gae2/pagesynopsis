package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.NotificationStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.NotificationStructWebService;


// MockNotificationStructWebService is a mock object.
// It can be used as a base class to mock NotificationStructWebService objects.
public abstract class MockNotificationStructWebService extends NotificationStructWebService  // implements NotificationStructService
{
    private static final Logger log = Logger.getLogger(MockNotificationStructWebService.class.getName());
     

}
