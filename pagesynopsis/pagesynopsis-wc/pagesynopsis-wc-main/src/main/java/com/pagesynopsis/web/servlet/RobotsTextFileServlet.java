package com.pagesynopsis.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.RobotsTextFileJsBean;
import com.pagesynopsis.rw.service.RobotsTextFileWebService;
import com.pagesynopsis.rw.service.UserWebService;
import com.pagesynopsis.web.service.RobotsTextFileProxyWebService;
import com.pagesynopsis.ws.core.StatusCode;


public class RobotsTextFileServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextFileServlet.class.getName());
    
    // temporary
    private static final String QUERY_PARAM_SITE_URL = "siteUrl";
    
    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private RobotsTextFileWebService robotsTextFileWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private RobotsTextFileWebService getRobotsTextFileService()
    {
        if(robotsTextFileWebService == null) {
            robotsTextFileWebService = new RobotsTextFileWebService();
        }
        return robotsTextFileWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();        
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        //throw new ServletException("Not implemented.");
        log.info("doGet(): TOP");
   
        // TBD:
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        String siteUrl = null;
        String[] siteUrls = req.getParameterValues(QUERY_PARAM_SITE_URL);
        if(siteUrls != null && siteUrls.length > 0) {
            // TBD: Support multiple target urls???
            siteUrl = URLDecoder.decode(siteUrls[0], "UTF-8");  // ?????
        } else {
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        log.fine("siteUrl = " + siteUrl);
                
        RobotsTextFileProxyWebService proxyWebService = new RobotsTextFileProxyWebService();
        RobotsTextFileJsBean robotsTextFile = null;
        try {
            robotsTextFile = proxyWebService.findRobotsTextFileBySiteUrl(siteUrl);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve robotsTextFile", e);
        }
       
        if(robotsTextFile != null) {
            String jsonStr = robotsTextFile.toJsonString();
            resp.setContentType("application/json");  // ????
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
}
