package com.pagesynopsis.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.UrlRatingStub;
import com.pagesynopsis.ws.stub.UrlRatingListStub;
import com.pagesynopsis.af.bean.UrlRatingBean;
import com.pagesynopsis.af.service.UrlRatingService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.config.Config;
import com.pagesynopsis.rf.proxy.UrlRatingServiceProxy;


// MockUrlRatingServiceProxy is a decorator.
// It can be used as a base class to mock UrlRatingService objects.
public abstract class MockUrlRatingServiceProxy extends UrlRatingServiceProxy implements UrlRatingService
{
    private static final Logger log = Logger.getLogger(MockUrlRatingServiceProxy.class.getName());

    // MockUrlRatingServiceProxy uses the decorator design pattern.
    private UrlRatingService decoratedProxy;

    public MockUrlRatingServiceProxy(UrlRatingService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected UrlRatingService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(UrlRatingService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public UrlRating getUrlRating(String guid) throws BaseException
    {
        return decoratedProxy.getUrlRating(guid);
    }

    @Override
    public Object getUrlRating(String guid, String field) throws BaseException
    {
        return decoratedProxy.getUrlRating(guid, field);
    }

    @Override
    public List<UrlRating> getUrlRatings(List<String> guids) throws BaseException
    {
        return decoratedProxy.getUrlRatings(guids);
    }

    @Override
    public List<UrlRating> getAllUrlRatings() throws BaseException
    {
        return getAllUrlRatings(null, null, null);
    }

    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUrlRatings(ordering, offset, count, null);
    }

    @Override
    public List<UrlRating> getAllUrlRatings(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUrlRatings(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUrlRatingKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUrlRatingKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUrlRatings(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<UrlRating> findUrlRatings(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUrlRatings(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUrlRatingKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUrlRating(String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException
    {
        return decoratedProxy.createUrlRating(domain, pageUrl, preview, flag, rating, note, status, ratedTime);
    }

    @Override
    public String createUrlRating(UrlRating bean) throws BaseException
    {
        return decoratedProxy.createUrlRating(bean);
    }

    @Override
    public UrlRating constructUrlRating(UrlRating bean) throws BaseException
    {
        return decoratedProxy.constructUrlRating(bean);
    }

    @Override
    public Boolean updateUrlRating(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime) throws BaseException
    {
        return decoratedProxy.updateUrlRating(guid, domain, pageUrl, preview, flag, rating, note, status, ratedTime);
    }

    @Override
    public Boolean updateUrlRating(UrlRating bean) throws BaseException
    {
        return decoratedProxy.updateUrlRating(bean);
    }

    @Override
    public UrlRating refreshUrlRating(UrlRating bean) throws BaseException
    {
        return decoratedProxy.refreshUrlRating(bean);
    }

    @Override
    public Boolean deleteUrlRating(String guid) throws BaseException
    {
        return decoratedProxy.deleteUrlRating(guid);
    }

    @Override
    public Boolean deleteUrlRating(UrlRating bean) throws BaseException
    {
        return decoratedProxy.deleteUrlRating(bean);
    }

    @Override
    public Long deleteUrlRatings(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteUrlRatings(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createUrlRatings(List<UrlRating> urlRatings) throws BaseException
    {
        return decoratedProxy.createUrlRatings(urlRatings);
    }

    // TBD
    //@Override
    //public Boolean updateUrlRatings(List<UrlRating> urlRatings) throws BaseException
    //{
    //}

}
