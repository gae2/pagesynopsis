package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.af.bean.OpenGraphMetaBean;
import com.pagesynopsis.af.service.OpenGraphMetaService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.fe.bean.TwitterProductCardJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.fe.bean.TwitterPlayerCardJsBean;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.fe.bean.OpenGraphMetaJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OpenGraphMetaWebService // implements OpenGraphMetaService
{
    private static final Logger log = Logger.getLogger(OpenGraphMetaWebService.class.getName());
     
    // Af service interface.
    private OpenGraphMetaService mService = null;

    public OpenGraphMetaWebService()
    {
        this(ServiceProxyFactory.getInstance().getOpenGraphMetaServiceProxy());
    }
    public OpenGraphMetaWebService(OpenGraphMetaService service)
    {
        mService = service;
    }
    
    protected OpenGraphMetaService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getOpenGraphMetaServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(OpenGraphMetaService service)
    {
        mService = service;
    }
    
    
    public OpenGraphMetaJsBean getOpenGraphMeta(String guid) throws WebException
    {
        try {
            OpenGraphMeta openGraphMeta = getServiceProxy().getOpenGraphMeta(guid);
            OpenGraphMetaJsBean bean = convertOpenGraphMetaToJsBean(openGraphMeta);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOpenGraphMeta(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getOpenGraphMeta(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OpenGraphMetaJsBean> getOpenGraphMetas(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OpenGraphMetaJsBean> jsBeans = new ArrayList<OpenGraphMetaJsBean>();
            List<OpenGraphMeta> openGraphMetas = getServiceProxy().getOpenGraphMetas(guids);
            if(openGraphMetas != null) {
                for(OpenGraphMeta openGraphMeta : openGraphMetas) {
                    jsBeans.add(convertOpenGraphMetaToJsBean(openGraphMeta));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OpenGraphMetaJsBean> getAllOpenGraphMetas() throws WebException
    {
        return getAllOpenGraphMetas(null, null, null);
    }

    // @Deprecated
    public List<OpenGraphMetaJsBean> getAllOpenGraphMetas(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOpenGraphMetas(ordering, offset, count, null);
    }

    public List<OpenGraphMetaJsBean> getAllOpenGraphMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OpenGraphMetaJsBean> jsBeans = new ArrayList<OpenGraphMetaJsBean>();
            List<OpenGraphMeta> openGraphMetas = getServiceProxy().getAllOpenGraphMetas(ordering, offset, count, forwardCursor);
            if(openGraphMetas != null) {
                for(OpenGraphMeta openGraphMeta : openGraphMetas) {
                    jsBeans.add(convertOpenGraphMetaToJsBean(openGraphMeta));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOpenGraphMetaKeys(ordering, offset, count, null);
    }

    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllOpenGraphMetaKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OpenGraphMetaJsBean> findOpenGraphMetas(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOpenGraphMetas(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OpenGraphMetaJsBean> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OpenGraphMetaJsBean> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OpenGraphMetaJsBean> jsBeans = new ArrayList<OpenGraphMetaJsBean>();
            List<OpenGraphMeta> openGraphMetas = getServiceProxy().findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(openGraphMetas != null) {
                for(OpenGraphMeta openGraphMeta : openGraphMetas) {
                    jsBeans.add(convertOpenGraphMetaToJsBean(openGraphMeta));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOpenGraphMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfileJsBean ogProfile, OgWebsiteJsBean ogWebsite, OgBlogJsBean ogBlog, OgArticleJsBean ogArticle, OgBookJsBean ogBook, OgVideoJsBean ogVideo, OgMovieJsBean ogMovie, OgTvShowJsBean ogTvShow, OgTvEpisodeJsBean ogTvEpisode) throws WebException
    {
        try {
            return getServiceProxy().createOpenGraphMeta(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, OgProfileWebService.convertOgProfileJsBeanToBean(ogProfile), OgWebsiteWebService.convertOgWebsiteJsBeanToBean(ogWebsite), OgBlogWebService.convertOgBlogJsBeanToBean(ogBlog), OgArticleWebService.convertOgArticleJsBeanToBean(ogArticle), OgBookWebService.convertOgBookJsBeanToBean(ogBook), OgVideoWebService.convertOgVideoJsBeanToBean(ogVideo), OgMovieWebService.convertOgMovieJsBeanToBean(ogMovie), OgTvShowWebService.convertOgTvShowJsBeanToBean(ogTvShow), OgTvEpisodeWebService.convertOgTvEpisodeJsBeanToBean(ogTvEpisode));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOpenGraphMeta(OpenGraphMetaJsBean jsBean) throws WebException
    {
        try {
            OpenGraphMeta openGraphMeta = convertOpenGraphMetaJsBeanToBean(jsBean);
            return getServiceProxy().createOpenGraphMeta(openGraphMeta);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OpenGraphMetaJsBean constructOpenGraphMeta(OpenGraphMetaJsBean jsBean) throws WebException
    {
        try {
            OpenGraphMeta openGraphMeta = convertOpenGraphMetaJsBeanToBean(jsBean);
            openGraphMeta = getServiceProxy().constructOpenGraphMeta(openGraphMeta);
            jsBean = convertOpenGraphMetaToJsBean(openGraphMeta);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOpenGraphMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String ogType, OgProfileJsBean ogProfile, OgWebsiteJsBean ogWebsite, OgBlogJsBean ogBlog, OgArticleJsBean ogArticle, OgBookJsBean ogBook, OgVideoJsBean ogVideo, OgMovieJsBean ogMovie, OgTvShowJsBean ogTvShow, OgTvEpisodeJsBean ogTvEpisode) throws WebException
    {
        try {
            return getServiceProxy().updateOpenGraphMeta(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, ogType, OgProfileWebService.convertOgProfileJsBeanToBean(ogProfile), OgWebsiteWebService.convertOgWebsiteJsBeanToBean(ogWebsite), OgBlogWebService.convertOgBlogJsBeanToBean(ogBlog), OgArticleWebService.convertOgArticleJsBeanToBean(ogArticle), OgBookWebService.convertOgBookJsBeanToBean(ogBook), OgVideoWebService.convertOgVideoJsBeanToBean(ogVideo), OgMovieWebService.convertOgMovieJsBeanToBean(ogMovie), OgTvShowWebService.convertOgTvShowJsBeanToBean(ogTvShow), OgTvEpisodeWebService.convertOgTvEpisodeJsBeanToBean(ogTvEpisode));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOpenGraphMeta(OpenGraphMetaJsBean jsBean) throws WebException
    {
        try {
            OpenGraphMeta openGraphMeta = convertOpenGraphMetaJsBeanToBean(jsBean);
            return getServiceProxy().updateOpenGraphMeta(openGraphMeta);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OpenGraphMetaJsBean refreshOpenGraphMeta(OpenGraphMetaJsBean jsBean) throws WebException
    {
        try {
            OpenGraphMeta openGraphMeta = convertOpenGraphMetaJsBeanToBean(jsBean);
            openGraphMeta = getServiceProxy().refreshOpenGraphMeta(openGraphMeta);
            jsBean = convertOpenGraphMetaToJsBean(openGraphMeta);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOpenGraphMeta(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteOpenGraphMeta(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOpenGraphMeta(OpenGraphMetaJsBean jsBean) throws WebException
    {
        try {
            OpenGraphMeta openGraphMeta = convertOpenGraphMetaJsBeanToBean(jsBean);
            return getServiceProxy().deleteOpenGraphMeta(openGraphMeta);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOpenGraphMetas(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteOpenGraphMetas(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static OpenGraphMetaJsBean convertOpenGraphMetaToJsBean(OpenGraphMeta openGraphMeta)
    {
        OpenGraphMetaJsBean jsBean = null;
        if(openGraphMeta != null) {
            jsBean = new OpenGraphMetaJsBean();
            jsBean.setGuid(openGraphMeta.getGuid());
            jsBean.setUser(openGraphMeta.getUser());
            jsBean.setFetchRequest(openGraphMeta.getFetchRequest());
            jsBean.setTargetUrl(openGraphMeta.getTargetUrl());
            jsBean.setPageUrl(openGraphMeta.getPageUrl());
            jsBean.setQueryString(openGraphMeta.getQueryString());
            List<KeyValuePairStructJsBean> queryParamsJsBeans = new ArrayList<KeyValuePairStructJsBean>();
            List<KeyValuePairStruct> queryParamsBeans = openGraphMeta.getQueryParams();
            if(queryParamsBeans != null) {
                for(KeyValuePairStruct keyValuePairStruct : queryParamsBeans) {
                    KeyValuePairStructJsBean jB = KeyValuePairStructWebService.convertKeyValuePairStructToJsBean(keyValuePairStruct);
                    queryParamsJsBeans.add(jB); 
                }
            }
            jsBean.setQueryParams(queryParamsJsBeans);
            jsBean.setLastFetchResult(openGraphMeta.getLastFetchResult());
            jsBean.setResponseCode(openGraphMeta.getResponseCode());
            jsBean.setContentType(openGraphMeta.getContentType());
            jsBean.setContentLength(openGraphMeta.getContentLength());
            jsBean.setLanguage(openGraphMeta.getLanguage());
            jsBean.setRedirect(openGraphMeta.getRedirect());
            jsBean.setLocation(openGraphMeta.getLocation());
            jsBean.setPageTitle(openGraphMeta.getPageTitle());
            jsBean.setNote(openGraphMeta.getNote());
            jsBean.setDeferred(openGraphMeta.isDeferred());
            jsBean.setStatus(openGraphMeta.getStatus());
            jsBean.setRefreshStatus(openGraphMeta.getRefreshStatus());
            jsBean.setRefreshInterval(openGraphMeta.getRefreshInterval());
            jsBean.setNextRefreshTime(openGraphMeta.getNextRefreshTime());
            jsBean.setLastCheckedTime(openGraphMeta.getLastCheckedTime());
            jsBean.setLastUpdatedTime(openGraphMeta.getLastUpdatedTime());
            jsBean.setOgType(openGraphMeta.getOgType());
            jsBean.setOgProfile(OgProfileWebService.convertOgProfileToJsBean(openGraphMeta.getOgProfile()));
            jsBean.setOgWebsite(OgWebsiteWebService.convertOgWebsiteToJsBean(openGraphMeta.getOgWebsite()));
            jsBean.setOgBlog(OgBlogWebService.convertOgBlogToJsBean(openGraphMeta.getOgBlog()));
            jsBean.setOgArticle(OgArticleWebService.convertOgArticleToJsBean(openGraphMeta.getOgArticle()));
            jsBean.setOgBook(OgBookWebService.convertOgBookToJsBean(openGraphMeta.getOgBook()));
            jsBean.setOgVideo(OgVideoWebService.convertOgVideoToJsBean(openGraphMeta.getOgVideo()));
            jsBean.setOgMovie(OgMovieWebService.convertOgMovieToJsBean(openGraphMeta.getOgMovie()));
            jsBean.setOgTvShow(OgTvShowWebService.convertOgTvShowToJsBean(openGraphMeta.getOgTvShow()));
            jsBean.setOgTvEpisode(OgTvEpisodeWebService.convertOgTvEpisodeToJsBean(openGraphMeta.getOgTvEpisode()));
            jsBean.setCreatedTime(openGraphMeta.getCreatedTime());
            jsBean.setModifiedTime(openGraphMeta.getModifiedTime());
        }
        return jsBean;
    }

    public static OpenGraphMeta convertOpenGraphMetaJsBeanToBean(OpenGraphMetaJsBean jsBean)
    {
        OpenGraphMetaBean openGraphMeta = null;
        if(jsBean != null) {
            openGraphMeta = new OpenGraphMetaBean();
            openGraphMeta.setGuid(jsBean.getGuid());
            openGraphMeta.setUser(jsBean.getUser());
            openGraphMeta.setFetchRequest(jsBean.getFetchRequest());
            openGraphMeta.setTargetUrl(jsBean.getTargetUrl());
            openGraphMeta.setPageUrl(jsBean.getPageUrl());
            openGraphMeta.setQueryString(jsBean.getQueryString());
            List<KeyValuePairStruct> queryParamsBeans = new ArrayList<KeyValuePairStruct>();
            List<KeyValuePairStructJsBean> queryParamsJsBeans = jsBean.getQueryParams();
            if(queryParamsJsBeans != null) {
                for(KeyValuePairStructJsBean keyValuePairStruct : queryParamsJsBeans) {
                    KeyValuePairStruct b = KeyValuePairStructWebService.convertKeyValuePairStructJsBeanToBean(keyValuePairStruct);
                    queryParamsBeans.add(b); 
                }
            }
            openGraphMeta.setQueryParams(queryParamsBeans);
            openGraphMeta.setLastFetchResult(jsBean.getLastFetchResult());
            openGraphMeta.setResponseCode(jsBean.getResponseCode());
            openGraphMeta.setContentType(jsBean.getContentType());
            openGraphMeta.setContentLength(jsBean.getContentLength());
            openGraphMeta.setLanguage(jsBean.getLanguage());
            openGraphMeta.setRedirect(jsBean.getRedirect());
            openGraphMeta.setLocation(jsBean.getLocation());
            openGraphMeta.setPageTitle(jsBean.getPageTitle());
            openGraphMeta.setNote(jsBean.getNote());
            openGraphMeta.setDeferred(jsBean.isDeferred());
            openGraphMeta.setStatus(jsBean.getStatus());
            openGraphMeta.setRefreshStatus(jsBean.getRefreshStatus());
            openGraphMeta.setRefreshInterval(jsBean.getRefreshInterval());
            openGraphMeta.setNextRefreshTime(jsBean.getNextRefreshTime());
            openGraphMeta.setLastCheckedTime(jsBean.getLastCheckedTime());
            openGraphMeta.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            openGraphMeta.setOgType(jsBean.getOgType());
            openGraphMeta.setOgProfile(OgProfileWebService.convertOgProfileJsBeanToBean(jsBean.getOgProfile()));
            openGraphMeta.setOgWebsite(OgWebsiteWebService.convertOgWebsiteJsBeanToBean(jsBean.getOgWebsite()));
            openGraphMeta.setOgBlog(OgBlogWebService.convertOgBlogJsBeanToBean(jsBean.getOgBlog()));
            openGraphMeta.setOgArticle(OgArticleWebService.convertOgArticleJsBeanToBean(jsBean.getOgArticle()));
            openGraphMeta.setOgBook(OgBookWebService.convertOgBookJsBeanToBean(jsBean.getOgBook()));
            openGraphMeta.setOgVideo(OgVideoWebService.convertOgVideoJsBeanToBean(jsBean.getOgVideo()));
            openGraphMeta.setOgMovie(OgMovieWebService.convertOgMovieJsBeanToBean(jsBean.getOgMovie()));
            openGraphMeta.setOgTvShow(OgTvShowWebService.convertOgTvShowJsBeanToBean(jsBean.getOgTvShow()));
            openGraphMeta.setOgTvEpisode(OgTvEpisodeWebService.convertOgTvEpisodeJsBeanToBean(jsBean.getOgTvEpisode()));
            openGraphMeta.setCreatedTime(jsBean.getCreatedTime());
            openGraphMeta.setModifiedTime(jsBean.getModifiedTime());
        }
        return openGraphMeta;
    }

}
