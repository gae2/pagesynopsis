package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgArticleWebService // implements OgArticleService
{
    private static final Logger log = Logger.getLogger(OgArticleWebService.class.getName());
     
    // Af service interface.
    private OgArticleService mService = null;

    public OgArticleWebService()
    {
        this(ServiceProxyFactory.getInstance().getOgArticleServiceProxy());
    }
    public OgArticleWebService(OgArticleService service)
    {
        mService = service;
    }
    
    protected OgArticleService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getOgArticleServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(OgArticleService service)
    {
        mService = service;
    }
    
    
    public OgArticleJsBean getOgArticle(String guid) throws WebException
    {
        try {
            OgArticle ogArticle = getServiceProxy().getOgArticle(guid);
            OgArticleJsBean bean = convertOgArticleToJsBean(ogArticle);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOgArticle(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getOgArticle(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgArticleJsBean> getOgArticles(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgArticleJsBean> jsBeans = new ArrayList<OgArticleJsBean>();
            List<OgArticle> ogArticles = getServiceProxy().getOgArticles(guids);
            if(ogArticles != null) {
                for(OgArticle ogArticle : ogArticles) {
                    jsBeans.add(convertOgArticleToJsBean(ogArticle));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgArticleJsBean> getAllOgArticles() throws WebException
    {
        return getAllOgArticles(null, null, null);
    }

    // @Deprecated
    public List<OgArticleJsBean> getAllOgArticles(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgArticles(ordering, offset, count, null);
    }

    public List<OgArticleJsBean> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgArticleJsBean> jsBeans = new ArrayList<OgArticleJsBean>();
            List<OgArticle> ogArticles = getServiceProxy().getAllOgArticles(ordering, offset, count, forwardCursor);
            if(ogArticles != null) {
                for(OgArticle ogArticle : ogArticles) {
                    jsBeans.add(convertOgArticleToJsBean(ogArticle));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgArticleKeys(ordering, offset, count, null);
    }

    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllOgArticleKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OgArticleJsBean> findOgArticles(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOgArticles(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OgArticleJsBean> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OgArticleJsBean> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgArticleJsBean> jsBeans = new ArrayList<OgArticleJsBean>();
            List<OgArticle> ogArticles = getServiceProxy().findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(ogArticles != null) {
                for(OgArticle ogArticle : ogArticles) {
                    jsBeans.add(convertOgArticleToJsBean(ogArticle));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgArticle(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws WebException
    {
        try {
            return getServiceProxy().createOgArticle(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgArticle(OgArticleJsBean jsBean) throws WebException
    {
        try {
            OgArticle ogArticle = convertOgArticleJsBeanToBean(jsBean);
            return getServiceProxy().createOgArticle(ogArticle);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgArticleJsBean constructOgArticle(OgArticleJsBean jsBean) throws WebException
    {
        try {
            OgArticle ogArticle = convertOgArticleJsBeanToBean(jsBean);
            ogArticle = getServiceProxy().constructOgArticle(ogArticle);
            jsBean = convertOgArticleToJsBean(ogArticle);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOgArticle(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws WebException
    {
        try {
            return getServiceProxy().updateOgArticle(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOgArticle(OgArticleJsBean jsBean) throws WebException
    {
        try {
            OgArticle ogArticle = convertOgArticleJsBeanToBean(jsBean);
            return getServiceProxy().updateOgArticle(ogArticle);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgArticleJsBean refreshOgArticle(OgArticleJsBean jsBean) throws WebException
    {
        try {
            OgArticle ogArticle = convertOgArticleJsBeanToBean(jsBean);
            ogArticle = getServiceProxy().refreshOgArticle(ogArticle);
            jsBean = convertOgArticleToJsBean(ogArticle);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgArticle(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteOgArticle(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgArticle(OgArticleJsBean jsBean) throws WebException
    {
        try {
            OgArticle ogArticle = convertOgArticleJsBeanToBean(jsBean);
            return getServiceProxy().deleteOgArticle(ogArticle);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOgArticles(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteOgArticles(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static OgArticleJsBean convertOgArticleToJsBean(OgArticle ogArticle)
    {
        OgArticleJsBean jsBean = null;
        if(ogArticle != null) {
            jsBean = new OgArticleJsBean();
            jsBean.setGuid(ogArticle.getGuid());
            jsBean.setUrl(ogArticle.getUrl());
            jsBean.setType(ogArticle.getType());
            jsBean.setSiteName(ogArticle.getSiteName());
            jsBean.setTitle(ogArticle.getTitle());
            jsBean.setDescription(ogArticle.getDescription());
            jsBean.setFbAdmins(ogArticle.getFbAdmins());
            jsBean.setFbAppId(ogArticle.getFbAppId());
            List<OgImageStructJsBean> imageJsBeans = new ArrayList<OgImageStructJsBean>();
            List<OgImageStruct> imageBeans = ogArticle.getImage();
            if(imageBeans != null) {
                for(OgImageStruct ogImageStruct : imageBeans) {
                    OgImageStructJsBean jB = OgImageStructWebService.convertOgImageStructToJsBean(ogImageStruct);
                    imageJsBeans.add(jB); 
                }
            }
            jsBean.setImage(imageJsBeans);
            List<OgAudioStructJsBean> audioJsBeans = new ArrayList<OgAudioStructJsBean>();
            List<OgAudioStruct> audioBeans = ogArticle.getAudio();
            if(audioBeans != null) {
                for(OgAudioStruct ogAudioStruct : audioBeans) {
                    OgAudioStructJsBean jB = OgAudioStructWebService.convertOgAudioStructToJsBean(ogAudioStruct);
                    audioJsBeans.add(jB); 
                }
            }
            jsBean.setAudio(audioJsBeans);
            List<OgVideoStructJsBean> videoJsBeans = new ArrayList<OgVideoStructJsBean>();
            List<OgVideoStruct> videoBeans = ogArticle.getVideo();
            if(videoBeans != null) {
                for(OgVideoStruct ogVideoStruct : videoBeans) {
                    OgVideoStructJsBean jB = OgVideoStructWebService.convertOgVideoStructToJsBean(ogVideoStruct);
                    videoJsBeans.add(jB); 
                }
            }
            jsBean.setVideo(videoJsBeans);
            jsBean.setLocale(ogArticle.getLocale());
            jsBean.setLocaleAlternate(ogArticle.getLocaleAlternate());
            jsBean.setAuthor(ogArticle.getAuthor());
            jsBean.setSection(ogArticle.getSection());
            jsBean.setTag(ogArticle.getTag());
            jsBean.setPublishedDate(ogArticle.getPublishedDate());
            jsBean.setModifiedDate(ogArticle.getModifiedDate());
            jsBean.setExpirationDate(ogArticle.getExpirationDate());
            jsBean.setCreatedTime(ogArticle.getCreatedTime());
            jsBean.setModifiedTime(ogArticle.getModifiedTime());
        }
        return jsBean;
    }

    public static OgArticle convertOgArticleJsBeanToBean(OgArticleJsBean jsBean)
    {
        OgArticleBean ogArticle = null;
        if(jsBean != null) {
            ogArticle = new OgArticleBean();
            ogArticle.setGuid(jsBean.getGuid());
            ogArticle.setUrl(jsBean.getUrl());
            ogArticle.setType(jsBean.getType());
            ogArticle.setSiteName(jsBean.getSiteName());
            ogArticle.setTitle(jsBean.getTitle());
            ogArticle.setDescription(jsBean.getDescription());
            ogArticle.setFbAdmins(jsBean.getFbAdmins());
            ogArticle.setFbAppId(jsBean.getFbAppId());
            List<OgImageStruct> imageBeans = new ArrayList<OgImageStruct>();
            List<OgImageStructJsBean> imageJsBeans = jsBean.getImage();
            if(imageJsBeans != null) {
                for(OgImageStructJsBean ogImageStruct : imageJsBeans) {
                    OgImageStruct b = OgImageStructWebService.convertOgImageStructJsBeanToBean(ogImageStruct);
                    imageBeans.add(b); 
                }
            }
            ogArticle.setImage(imageBeans);
            List<OgAudioStruct> audioBeans = new ArrayList<OgAudioStruct>();
            List<OgAudioStructJsBean> audioJsBeans = jsBean.getAudio();
            if(audioJsBeans != null) {
                for(OgAudioStructJsBean ogAudioStruct : audioJsBeans) {
                    OgAudioStruct b = OgAudioStructWebService.convertOgAudioStructJsBeanToBean(ogAudioStruct);
                    audioBeans.add(b); 
                }
            }
            ogArticle.setAudio(audioBeans);
            List<OgVideoStruct> videoBeans = new ArrayList<OgVideoStruct>();
            List<OgVideoStructJsBean> videoJsBeans = jsBean.getVideo();
            if(videoJsBeans != null) {
                for(OgVideoStructJsBean ogVideoStruct : videoJsBeans) {
                    OgVideoStruct b = OgVideoStructWebService.convertOgVideoStructJsBeanToBean(ogVideoStruct);
                    videoBeans.add(b); 
                }
            }
            ogArticle.setVideo(videoBeans);
            ogArticle.setLocale(jsBean.getLocale());
            ogArticle.setLocaleAlternate(jsBean.getLocaleAlternate());
            ogArticle.setAuthor(jsBean.getAuthor());
            ogArticle.setSection(jsBean.getSection());
            ogArticle.setTag(jsBean.getTag());
            ogArticle.setPublishedDate(jsBean.getPublishedDate());
            ogArticle.setModifiedDate(jsBean.getModifiedDate());
            ogArticle.setExpirationDate(jsBean.getExpirationDate());
            ogArticle.setCreatedTime(jsBean.getCreatedTime());
            ogArticle.setModifiedTime(jsBean.getModifiedTime());
        }
        return ogArticle;
    }

}
