package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.af.bean.FetchRequestBean;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.NotificationStructJsBean;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.ReferrerInfoStructJsBean;
import com.pagesynopsis.fe.bean.FetchRequestJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.FetchRequestWebService;


// MockFetchRequestWebService is a mock object.
// It can be used as a base class to mock FetchRequestWebService objects.
public abstract class MockFetchRequestWebService extends FetchRequestWebService  // implements FetchRequestService
{
    private static final Logger log = Logger.getLogger(MockFetchRequestWebService.class.getName());
     
    // Af service interface.
    private FetchRequestWebService mService = null;

    public MockFetchRequestWebService()
    {
        this(MockServiceProxyFactory.getInstance().getFetchRequestServiceProxy());
    }
    public MockFetchRequestWebService(FetchRequestService service)
    {
        super(service);
    }


}
