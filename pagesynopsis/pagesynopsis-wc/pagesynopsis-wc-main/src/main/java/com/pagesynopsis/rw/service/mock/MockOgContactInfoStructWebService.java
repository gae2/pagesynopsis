package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgContactInfoStruct;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgContactInfoStructJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.OgContactInfoStructWebService;


// MockOgContactInfoStructWebService is a mock object.
// It can be used as a base class to mock OgContactInfoStructWebService objects.
public abstract class MockOgContactInfoStructWebService extends OgContactInfoStructWebService  // implements OgContactInfoStructService
{
    private static final Logger log = Logger.getLogger(MockOgContactInfoStructWebService.class.getName());
     

}
