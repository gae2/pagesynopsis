package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.GeoPointStruct;
import com.pagesynopsis.ws.StreetAddressStruct;
import com.pagesynopsis.ws.GaeAppStruct;
import com.pagesynopsis.ws.FullNameStruct;
import com.pagesynopsis.ws.GaeUserStruct;
import com.pagesynopsis.ws.User;
import com.pagesynopsis.af.bean.UserBean;
import com.pagesynopsis.af.service.UserService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.GeoPointStructJsBean;
import com.pagesynopsis.fe.bean.StreetAddressStructJsBean;
import com.pagesynopsis.fe.bean.GaeAppStructJsBean;
import com.pagesynopsis.fe.bean.FullNameStructJsBean;
import com.pagesynopsis.fe.bean.GaeUserStructJsBean;
import com.pagesynopsis.fe.bean.UserJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.UserWebService;


// MockUserWebService is a mock object.
// It can be used as a base class to mock UserWebService objects.
public abstract class MockUserWebService extends UserWebService  // implements UserService
{
    private static final Logger log = Logger.getLogger(MockUserWebService.class.getName());
     
    // Af service interface.
    private UserWebService mService = null;

    public MockUserWebService()
    {
        this(MockServiceProxyFactory.getInstance().getUserServiceProxy());
    }
    public MockUserWebService(UserService service)
    {
        super(service);
    }


}
