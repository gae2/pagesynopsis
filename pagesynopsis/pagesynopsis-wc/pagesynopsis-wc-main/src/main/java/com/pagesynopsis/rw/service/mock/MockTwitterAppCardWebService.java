package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.service.TwitterAppCardService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardAppInfoJsBean;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.TwitterAppCardWebService;


// MockTwitterAppCardWebService is a mock object.
// It can be used as a base class to mock TwitterAppCardWebService objects.
public abstract class MockTwitterAppCardWebService extends TwitterAppCardWebService  // implements TwitterAppCardService
{
    private static final Logger log = Logger.getLogger(MockTwitterAppCardWebService.class.getName());
     
    // Af service interface.
    private TwitterAppCardWebService mService = null;

    public MockTwitterAppCardWebService()
    {
        this(MockServiceProxyFactory.getInstance().getTwitterAppCardServiceProxy());
    }
    public MockTwitterAppCardWebService(TwitterAppCardService service)
    {
        super(service);
    }


}
