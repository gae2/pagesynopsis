package com.pagesynopsis.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.rf.proxy.mock.MockServiceProxyFactory;
import com.pagesynopsis.rw.service.TwitterCardProductDataWebService;


// MockTwitterCardProductDataWebService is a mock object.
// It can be used as a base class to mock TwitterCardProductDataWebService objects.
public abstract class MockTwitterCardProductDataWebService extends TwitterCardProductDataWebService  // implements TwitterCardProductDataService
{
    private static final Logger log = Logger.getLogger(MockTwitterCardProductDataWebService.class.getName());
     

}
